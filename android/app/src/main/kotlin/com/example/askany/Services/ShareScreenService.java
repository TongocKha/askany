package com.example.askany.Services;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import com.askany.R;

public class ShareScreenService extends Service {
    private static String TAG = "FlutterForegroundService";
    int NOTIFICATION_ID = 201;
    public static String CHANNEL_ID = "share_screen";
    private boolean userStopForegroundService = false;
    private boolean isForegroundRunning = false;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            return START_NOT_STICKY;
        }
        if (intent.getAction() == null) {
            return START_NOT_STICKY;
        }

        final String action = intent.getAction();

        switch (action) {
            case "START":
                startForegroundService();
                break;
            case "STOP":
                stopFlutterForegroundService();
                break;
            default: break;
        }

        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        if (!userStopForegroundService) {
            Log.d(TAG, "User close app, kill current process to avoid memory leak in other plugin.");
            android.os.Process.killProcess(android.os.Process.myPid());
        }
    }

    private void startForegroundService() {
        if (!isForegroundRunning) {
            isForegroundRunning = true;
            PackageManager pm = getApplicationContext().getPackageManager();
            Intent notificationIntent = pm.getLaunchIntentForPackage(getApplicationContext().getPackageName());
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                    notificationIntent, 0);


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setColor(Color.RED)
                        .setContentTitle("Bạn đang trong cuộc gọi Askany")
                        .setContentText("Bạn đang chia sẻ màn hình...")
                        .setCategory(NotificationCompat.CATEGORY_SERVICE)
                        .setContentIntent(pendingIntent)
                        .setUsesChronometer(true)
                        .setOngoing(true);

                startForeground(NOTIFICATION_ID, builder.build());
            }


        }
    }

    private void stopFlutterForegroundService() {
        if (isForegroundRunning) {
            stopForeground(true);
            stopSelf();
            isForegroundRunning = false;
            userStopForegroundService = true;
        }
    }
}
