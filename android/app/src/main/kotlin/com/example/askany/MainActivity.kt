package com.askany

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.ContentResolver
import android.content.Intent
import android.media.AudioAttributes
import android.net.Uri
import android.os.Build
import android.os.Bundle
import androidx.annotation.NonNull
import androidx.annotation.RequiresApi
import com.example.askany.Services.IncomingService
import com.example.askany.Services.ShareScreenService
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel


class MainActivity: FlutterActivity() {
    private val INITIAL_APP_CHANNEL: String = "com.askany.startArguments"
    private val SHARE_SCREEN_CHANNEL: String = "com.askany.shareScreen"

    private val ASKANY_CNID: String = "askany_notification_normal"

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        stopForeGround()
        createNotificationIncomingChannel()
        createNotificationShareScreenChannel()
        createNotificationChannel()
    }

    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)

        // Method Channel check start app from incoming call
        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, INITIAL_APP_CHANNEL).setMethodCallHandler { call, result ->
            if (call.method == "arguments") {
                val action: String? = intent.action;
                if (action == IncomingService.CHANNEL_ID) {
                    result.success(true)
                } else {
                    result.success(false)
                }
            }
        }

        // Method Channel foreground share screen
        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, SHARE_SCREEN_CHANNEL).setMethodCallHandler { call, result ->
            val intent = Intent(this, ShareScreenService::class.java)
            if (call.method == "START") {
                intent.action = "START"
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
                    context.startForegroundService(intent)
                    result.success(true);
                } else {
                    context.startService(intent)
                    result.success(true);
                }
            } else if (call.method == "STOP") {
                intent.action = "STOP"
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
                    context.startForegroundService(intent)
                    result.success(true);
                } else {
                    context.startService(intent)
                    result.success(true);
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        stopForeGround()
    }

    override fun onRestart() {
        super.onRestart()
        stopForeGround()
    }

    private fun stopForeGround() {
        IncomingService.roomId = ""
        val intent = Intent(context, IncomingService::class.java)
        intent.action = "STOP"
        context.startService(intent)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationIncomingChannel() {
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        @SuppressLint("WrongConstant") val notificationChannel = NotificationChannel(IncomingService.CHANNEL_ID, IncomingService.CHANNEL_ID,
                NotificationManager.IMPORTANCE_MAX)

        notificationChannel.description = IncomingService.CHANNEL_ID
        notificationChannel.enableLights(true)
        notificationChannel.vibrationPattern = longArrayOf(0, 1000, 500, 1000)
        notificationChannel.enableVibration(true)

        notificationManager.createNotificationChannel(notificationChannel)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationShareScreenChannel() {
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        @SuppressLint("WrongConstant") val notificationChannel = NotificationChannel(ShareScreenService.CHANNEL_ID, ShareScreenService.CHANNEL_ID,
                NotificationManager.IMPORTANCE_DEFAULT)

        notificationChannel.description = ShareScreenService.CHANNEL_ID
        notificationManager.createNotificationChannel(notificationChannel)
    }

    private fun createNotificationChannel() {
        val sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + applicationContext!!.packageName + "/" + R.raw.alarm)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create the NotificationChannel
            val name = ASKANY_CNID
            val descriptionText = ASKANY_CNID
            val audioAttributes: AudioAttributes = AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setUsage(AudioAttributes.USAGE_ALARM)
                    .build()
            val importance = NotificationManager.IMPORTANCE_HIGH
            val mChannel = NotificationChannel(ASKANY_CNID, name, importance)
            mChannel.description = descriptionText
            mChannel.setSound(sound, audioAttributes)
            mChannel.enableVibration(false)
            mChannel.enableLights(true)
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(mChannel)
        }
    }
}

