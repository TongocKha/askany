package com.example.askany.Services;
import android.app.KeyguardManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.Drawable;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.os.Vibrator;
import android.provider.Settings;
import android.util.Log;
import android.widget.RemoteViews;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.askany.MainActivity;
import com.askany.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.Timer;
import java.util.TimerTask;
import io.socket.client.IO;
import io.socket.client.Socket;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;

public class IncomingService extends Service {
    private RemoteViews mRemoteViews;
    public static String CHANNEL_ID = "incoming";
    int NOTIFICATION_ID = 101;
    private boolean isForegroundRunning = false;
    public static String roomId = "";
    private Vibrator mVibrator;
    private Ringtone mRington;
    private String defautImage = "https://firebasestorage.googleapis.com/v0/b/appchat-d8e22.appspot.com/o/avatar_default.png?alt=media&token=9c54c56b-6c98-4dd4-a403-dd1982029433";
    public static final int MAX_WIDTH= 200;
    public static final int MAX_HEIGHT= 200;
    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            return START_NOT_STICKY;
        }
        if (intent.getAction() == null) {
            return START_NOT_STICKY;
        }

        final String action = intent.getAction();

        switch (action) {
            case "START":
                String fullName = intent.getStringExtra("fullName");
                String urlToImage = intent.getStringExtra("urlToImage");
                roomId = intent.getStringExtra("roomId");
                startForegroundService(fullName, urlToImage);
                break;
            case "STOP":
                stopFlutterForegroundService();
                break;
            default: break;
        }

        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void startForegroundService(String fullName, String urlToImage) {
        if (!isForegroundRunning) {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){
                mRemoteViews = new RemoteViews(getPackageName(), R.layout.notification_incoming_call);
                mRemoteViews.setTextViewText(R.id.incoming_call_notification_content_text, fullName);
                if(!exists(urlToImage)){
                    urlToImage = defautImage;
                }
                 Picasso.get().load(urlToImage).resize(MAX_WIDTH,MAX_HEIGHT).error(R.drawable.ic_error)
                        .into(new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                               //UINotification(true,bitmap,defautImage);
                                  Intent fullScreenIntent = new Intent(IncomingService.this, MainActivity.class);
                                fullScreenIntent.setAction(CHANNEL_ID);
                                fullScreenIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(IncomingService.this, 0,
                                        fullScreenIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                                Intent fullScreenLockIntent = new Intent(IncomingService.this, MainActivity.class);
                                fullScreenIntent.setAction(Intent.ACTION_MAIN);
                                fullScreenIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                PendingIntent fullScreenLockPendingIntent = PendingIntent.getActivity(IncomingService.this, 0,
                                        fullScreenLockIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                                Intent stopSelf = new Intent(IncomingService.this, IncomingService.class);
                                stopSelf.setAction("STOP");
                                PendingIntent pStopSelf = PendingIntent.getService(IncomingService.this, 0, stopSelf,0);

                                mRemoteViews.setImageViewBitmap(R.id.imgAvatar, getRoundedCornerBitmap(bitmap));
                                mRemoteViews.setOnClickPendingIntent(R.id.answer_call_button, fullScreenPendingIntent);
                                mRemoteViews.setOnClickPendingIntent(R.id.hang_up_button, pStopSelf);

                                KeyguardManager keyguardManager = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
                                // Vibration
                                mVibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                long[] pattern = {0, 500, 1000};

                                // Sound
                                try {
                                    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
                                    mRington = RingtoneManager.getRingtone(getApplicationContext(), notification);
                                    mRington.setLooping(true);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                if(!keyguardManager.isDeviceLocked()) {
                                    mVibrator.vibrate(pattern,0);
                                    mRington.play();
                                }

                                NotificationCompat.Builder notificationBuilder =
                                        new NotificationCompat.Builder(IncomingService.this, CHANNEL_ID)
                                                .setSmallIcon(R.drawable.launch_background)
                                                .setPriority(NotificationCompat.PRIORITY_MAX)
                                                .setCategory(NotificationCompat.CATEGORY_CALL)
                                                .setCustomContentView(mRemoteViews)
                                                .setFullScreenIntent(fullScreenLockPendingIntent, true);

                                Notification incomingCallNotification = notificationBuilder.build();
                                startForeground(NOTIFICATION_ID, incomingCallNotification);
                                scheduleStopIncomingCall();
                            }

                            @Override
                            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                                 UINotification(false,null,defautImage);
                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {

                            }
                        });

            }
        }
    }
     public static boolean exists(String URLName){
        try {
            HttpURLConnection.setFollowRedirects(false);
            HttpURLConnection con =  (HttpURLConnection) new URL(URLName).openConnection();
            con.setRequestMethod("HEAD");
            return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private void UINotification(boolean checkImage,Bitmap bitmap,String Image){
         Intent fullScreenIntent = new Intent(IncomingService.this, MainActivity.class);
                                fullScreenIntent.setAction(CHANNEL_ID);
                                fullScreenIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(IncomingService.this, 0,
                                        fullScreenIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                                Intent fullScreenLockIntent = new Intent(IncomingService.this, MainActivity.class);
                                fullScreenIntent.setAction(Intent.ACTION_MAIN);
                                fullScreenIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                PendingIntent fullScreenLockPendingIntent = PendingIntent.getActivity(IncomingService.this, 0,
                                        fullScreenLockIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                                Intent stopSelf = new Intent(IncomingService.this, IncomingService.class);
                                stopSelf.setAction("STOP");
                                PendingIntent pStopSelf = PendingIntent.getService(IncomingService.this, 0, stopSelf,0);

                                if(checkImage){
                                     mRemoteViews.setImageViewBitmap(R.id.imgAvatar, getRoundedCornerBitmap(bitmap));
                                }
                                else{
                                    mRemoteViews.setImageViewBitmap(R.id.imgAvatar, getBitmapFromURL(defautImage));
                                }
                              
                                mRemoteViews.setOnClickPendingIntent(R.id.answer_call_button, fullScreenPendingIntent);
                                mRemoteViews.setOnClickPendingIntent(R.id.hang_up_button, pStopSelf);

                                KeyguardManager keyguardManager = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
                                // Vibration
                                mVibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                long[] pattern = {0, 500, 1000};

                                // Sound
                                try {
                                    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
                                    mRington = RingtoneManager.getRingtone(getApplicationContext(), notification);
                                    mRington.setLooping(true);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                if(!keyguardManager.isDeviceLocked()) {
                                    mVibrator.vibrate(pattern,0);
                                    mRington.play();
                                }

                                NotificationCompat.Builder notificationBuilder =
                                        new NotificationCompat.Builder(IncomingService.this, CHANNEL_ID)
                                                .setSmallIcon(R.drawable.launch_background)
                                                .setPriority(NotificationCompat.PRIORITY_MAX)
                                                .setCategory(NotificationCompat.CATEGORY_CALL)
                                                .setCustomContentView(mRemoteViews)
                                                .setFullScreenIntent(fullScreenLockPendingIntent, true);

                                Notification incomingCallNotification = notificationBuilder.build();
                                startForeground(NOTIFICATION_ID, incomingCallNotification);
                                scheduleStopIncomingCall();
    }
    private void scheduleStopIncomingCall() {
        isForegroundRunning = true;
        TimerTask myTask = new TimerTask()
        {
            public void run()
            {
                stopFlutterForegroundService();
            }
        };

        //Timer that will make the runnable run.
        Timer myTimer = new Timer();

        //the amount of time after which you want to stop the service
        long SECOND = 1000;
        long DELAY_END_CALL_IN_MINUTES = 30;
        long INTERVAL = DELAY_END_CALL_IN_MINUTES * SECOND; // I choose 5 seconds

        myTimer.schedule(myTask, INTERVAL);
    }

    private void stopFlutterForegroundService() {
        if (isForegroundRunning) {
            mVibrator.cancel();
            if (mRington.isPlaying()) {
                mRington.stop();
            }
            if (roomId.length() > 0) {
                new Thread(new ClientThread()).start();
            } else {
                stopForeground(true);
                stopSelf();
                roomId = "";
                isForegroundRunning = false;
            }
        }
    }

    class ClientThread implements Runnable {

        @Override
        public void run() {
            Socket mSocket;
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("room", roomId);
                mSocket = IO.socket("https://call.damdev.tk");
                mSocket.connect();

                TimerTask myTask = new TimerTask()
                {
                    public void run()
                    {
                        if (mSocket.connected()) {
                            mSocket.emit("LEAVE_ROOM_CSS", jsonObject);
                            mSocket.disconnect();
                            stopForeground(true);
//                            stopSelf();
                            roomId = "";
                            isForegroundRunning = false;
                        }
                    }
                };

                //Timer that will make the runnable run.
                Timer myTimer = new Timer();

                //the amount of time after which you want to stop the service
                long HAlF_SECOND = 500;
                myTimer.schedule(myTask, 0, HAlF_SECOND);

            } catch (URISyntaxException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }
    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        int radius = Math.min(h / 2, w / 2);
        Bitmap output = Bitmap.createBitmap(w + 8, h + 8, Bitmap.Config.ARGB_8888);

        Paint p = new Paint();
        p.setAntiAlias(true);

        Canvas c = new Canvas(output);
        c.drawARGB(0, 0, 0, 0);
        p.setStyle(Paint.Style.FILL);

        c.drawCircle((w / 2) + 4, (h / 2) + 4, radius, p);

        p.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));

        c.drawBitmap(bitmap, 4, 4, p);
        p.setXfermode(null);
        p.setStyle(Paint.Style.STROKE);
        p.setColor(Color.WHITE);
        p.setStrokeWidth(3);
        c.drawCircle((w / 2) + 4, (h / 2) + 4, radius, p);

        return output;
    }
}
