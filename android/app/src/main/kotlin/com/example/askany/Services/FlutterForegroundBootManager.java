package com.example.askany.Services;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

public class FlutterForegroundBootManager extends BroadcastReceiver {

    public static final String TAG = "FlutterForegroundBootManager";

    @Override
    public void onReceive(Context context, Intent intent) {
        // just make sure we are getting the right intent (better safe than sorry)
        if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
            ComponentName comp = new ComponentName(context.getPackageName(), FlutterForegroundBootManager.class.getName());
            ComponentName service = context.startService(new Intent().setComponent(comp));
            if (null == service) {
                // something really wrong here
                System.out.println("Could not start service " + comp.toString());
            }
        } else {
            System.out.println("Received unexpected intent ");
        }

    }
}