package com.example.askany.Services;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.PowerManager;
import android.util.Log;

import androidx.annotation.NonNull;

import com.askany.MainActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import static android.os.PowerManager.ACQUIRE_CAUSES_WAKEUP;
import static android.os.PowerManager.FULL_WAKE_LOCK;
import static android.os.PowerManager.ON_AFTER_RELEASE;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class FirebaseInstanceIdService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        if (remoteMessage.getData() != null && remoteMessage.getData().get("action") != null) {
            String MISSING_CALL = "missing";

            String fullName = "Hồng Vinh";
            String urlToImage = "https://images.pexels.com.qwejhjdfhasda/photos/1624496/pexels-photo-1624496.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500";
            Log.d("ASKANY",remoteMessage.getData().toString());

            if (remoteMessage.getData().get("action").equals(IncomingService.CHANNEL_ID)) {
                
                String roomId = remoteMessage.getData().get("roomId");
                fullName = remoteMessage.getData().get("name");
                urlToImage ="https://askany.damdev.tk/users/" + remoteMessage.getData().get("avatar");
                
                PowerManager pm = (PowerManager) getApplicationContext()
                        .getSystemService(Context.POWER_SERVICE);

                @SuppressLint("InvalidWakeLockTag") PowerManager.WakeLock wl1 = pm.newWakeLock(
                        ACQUIRE_CAUSES_WAKEUP | ON_AFTER_RELEASE | FULL_WAKE_LOCK, "wl1");
                wl1.acquire(10000);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    Intent intent = new Intent(this, IncomingService.class);
                    intent.setAction("START");
                    intent.putExtra("fullName", fullName);

                    intent.putExtra("urlToImage", urlToImage);
                    intent.putExtra("roomId", roomId);
                    startForegroundService(intent);
                } else {
                    Intent i = new Intent(this, MainActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.setAction(Intent.ACTION_MAIN);
                    i.addCategory(Intent.CATEGORY_LAUNCHER);
                    i.putExtra("foreground", true);
                    i.putExtra("incoming_call", true);

                    startActivity(i);
                }
            } else if (remoteMessage.getData().get("action").equals(MISSING_CALL)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    Intent intent = new Intent(this, IncomingService.class);
                    intent.setAction("STOP");
                    startForegroundService(intent);
                }
            }
        }
    }
}