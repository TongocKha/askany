import Flutter
import UIKit
import Photos
import CallKit
import PushKit
import Firebase
import flutter_callkit_incoming
import AVFAudio
//import momo_vn

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  var deviceTokentVoip: String!
  var uuidCall: String!
    
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
//      self.registerForPushNotifications()
      self.voipRegistration()

      if #available(iOS 10.0, *) {
        UNUserNotificationCenter.current().delegate = self as UNUserNotificationCenterDelegate
      }
//      UIApplication.shared.registerForRemoteNotifications()
      FirebaseApp.configure()
      ///
      let controller : FlutterViewController = window?.rootViewController as! FlutterViewController
      let deviceTokentVoipChannel = FlutterMethodChannel(name: "com.askany.ndn/getVoipDeviceTokent",binaryMessenger: controller.binaryMessenger)
      let isCheckLockChannel = FlutterMethodChannel(name: "com.askany.ndn/checkLock",binaryMessenger: controller.binaryMessenger)
      let uuidCallPartner = FlutterMethodChannel(name: "com.askany.ndn/uuidCall",binaryMessenger: controller.binaryMessenger)
      let enableSound = FlutterMethodChannel(name: "com.askany.ndn/enableSoundSpeaker",binaryMessenger: controller.binaryMessenger)
      let saveImageNative = FlutterMethodChannel(name: "com.askany.ndn/ios_save_img",binaryMessenger: controller.binaryMessenger)

      isCheckLockChannel.setMethodCallHandler({
            (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
                switch (call.method) {
                        case "isLockScreen":
                            return result(UIScreen.main.brightness == 0.0)
                        default:
                            return result(FlutterMethodNotImplemented)
                        }
            
      })
      uuidCallPartner.setMethodCallHandler({
                      (call: FlutterMethodCall, result: @escaping  FlutterResult)  -> Void in
          guard call.method == "getUuidCall" else {
             result(FlutterMethodNotImplemented)
             return
           }
          self.getUuidCallPartner(result: result, call: call)
      })
      deviceTokentVoipChannel.setMethodCallHandler({
            (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
          guard call.method == "getDeviceTokentVoip" else {
             result(FlutterMethodNotImplemented)
             return
           }
          self.receiveDeviceTokentVoip(result:result)
                                       })
      enableSound.setMethodCallHandler({
                      (call: FlutterMethodCall, result: @escaping  FlutterResult)  -> Void in
          guard call.method == "enableSound" else {
             result(FlutterMethodNotImplemented)
             return
           }
          self.enableSound(result: result)
      })
      saveImageNative.setMethodCallHandler({
                      (call: FlutterMethodCall, result: @escaping  FlutterResult)  -> Void in
          guard call.method == "saveImageToGallery" else {
             result(FlutterMethodNotImplemented)
             return
           }
          let arguments = call.arguments as? [String: Any] ?? [String: Any]()
          guard let imageData = (arguments["imageBytes"] as? FlutterStandardTypedData)?.data,
                let image = UIImage(data: imageData),
                let quality = arguments["quality"] as? Int,
                let _ = arguments["name"],
                let isReturnImagePath = arguments["isReturnImagePathOfIOS"] as? Bool
                else { return }
                let newImage = image.jpegData(compressionQuality: CGFloat(quality / 100))!
          self.saveImage(UIImage(data: newImage) ?? image, isReturnImagePath: isReturnImagePath, result: result)
      })
      ///
      GeneratedPluginRegistrant.register(with: self)
      application.registerForRemoteNotifications()
      return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
    
    func saveImage(_ image: UIImage, isReturnImagePath: Bool, result: @escaping  FlutterResult) {
            if !isReturnImagePath {
                UIImageWriteToSavedPhotosAlbum(image, self, #selector(didFinishSavingImage(image:error:contextInfo:)), nil)
                return
            }
            
            var imageIds: [String] = []
            
            PHPhotoLibrary.shared().performChanges( {
                let req = PHAssetChangeRequest.creationRequestForAsset(from: image)
                if let imageId = req.placeholderForCreatedAsset?.localIdentifier {
                    imageIds.append(imageId)
                }
            }, completionHandler: { [unowned self] (success, error) in
                DispatchQueue.main.async {
                    if (success && imageIds.count > 0) {
                        let assetResult = PHAsset.fetchAssets(withLocalIdentifiers: imageIds, options: nil)
                        if (assetResult.count > 0) {
                            let imageAsset = assetResult[0]
                            let options = PHContentEditingInputRequestOptions()
                            options.canHandleAdjustmentData = { (adjustmeta)
                                -> Bool in true }
                            imageAsset.requestContentEditingInput(with: options) { [unowned self] (contentEditingInput, info) in
                                if let urlStr = contentEditingInput?.fullSizeImageURL?.absoluteString {
                                    self.saveResult(isSuccess: true, filePath: urlStr)
                                }
                            }
                        }
                    } else {
                        self.saveResult(isSuccess: false, error: "")
                    }
                    
                    result(true)
                }
            })
        }
    
    @objc func didFinishSavingImage(image: UIImage, error: NSError?, contextInfo: UnsafeMutableRawPointer?) {
            saveResult(isSuccess: error == nil, error: error?.description)
        }
    
    func saveResult(isSuccess: Bool, error: String? = nil, filePath: String? = nil) {
            var saveResult = SaveResultModel()
            saveResult.isSuccess = error == nil
            saveResult.errorMessage = error?.description
            saveResult.filePath = filePath
//            result(saveResult.toDic())
        }
    
    func getUuidCallPartner(result:FlutterResult, call: FlutterMethodCall){
        switch (call.method) {
            case "getUuidCall":
                return result(uuidCall)
            default:
                return result(FlutterMethodNotImplemented)
            }
    }
    func receiveDeviceTokentVoip(result: FlutterResult) {
        if deviceTokentVoip == nil {
            result(FlutterError(code: "UNAVAILABLE",
                                message: "Token voip is null∂",
                                details: nil))
        }else{
            result(deviceTokentVoip)
        }
   }
    func enableSound(result: FlutterResult) {
        let audioSession = AVAudioSession.sharedInstance()
        do{
            try audioSession.overrideOutputAudioPort(AVAudioSession.PortOverride.speaker)
        } catch let error as NSError {
            print("audioSession error:  \(error.localizedDescription)")
        }
        result("ok")
    }
    // Register for VoIP notifications
        func voipRegistration() {

            // Create a push registry object
            let mainQueue = DispatchQueue.main
           // let mainQueue = DispatchQueue(label: "voipQueue")
            let voipRegistry: PKPushRegistry = PKPushRegistry(queue: mainQueue)
            voipRegistry.delegate = self
            voipRegistry.desiredPushTypes = [PKPushType.voIP]
        }
    
//    override func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
//            MoMoPayment.handleOpenUrl(url: url, sourceApp: sourceApplication!)
//            return true
//        }
//
//      override func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
//            MoMoPayment.handleOpenUrl(url: url, sourceApp: "")
//            return true
//        }
}

//MARK: - PKPushRegistryDelegate
extension AppDelegate :PKPushRegistryDelegate{

    // Handle updated push credentials
    func pushRegistry(_ registry: PKPushRegistry, didUpdate credentials: PKPushCredentials, for type: PKPushType) {
        print(credentials.token)
        let deviceToken = credentials.token.map { String(format: "%02x", $0) }.joined()
        print("pushRegistry -> deviceToken :\(deviceToken)")
        deviceTokentVoip = deviceToken
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didInvalidatePushTokenFor type: PKPushType) {
        print("pushRegistry:didInvalidatePushTokenForType:")
    }
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType)  {
        print(payload.dictionaryPayload)
        let handle = payload.dictionaryPayload as? [String: Any]
//        let alert = handle?["alert"] as? [String: String]
        let uuidCallPartner = handle?["uuidCall"]
        let nameCall = handle?["nameCaller"]
        uuidCall = uuidCallPartner as? String ?? ""
        var info = [String:Any?]()
//        info["id"] = "aad915e1-5ff4-4bed-bf13-c423048ec97a"
        info["id"] = uuidCallPartner
        info["nameCaller"] = nameCall
        info["handle"] = "0123456789"
        info["ios"] = [
            "audioSessionMode": "videoChat"
        ]
        SwiftFlutterCallkitIncomingPlugin.sharedInstance?.showCallkitIncoming(flutter_callkit_incoming.Data(args: info), fromPushKit: true)
    }


}

public struct SaveResultModel: Encodable {
    var isSuccess: Bool!
    var filePath: String?
    var errorMessage: String?
    
    func toDic() -> [String:Any]? {
        let encoder = JSONEncoder()
        guard let data = try? encoder.encode(self) else { return nil }
        if (!JSONSerialization.isValidJSONObject(data)) {
            return try? JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:Any]
        }
        return nil
    }
}

// curl -v -d '{"aps":{"uuidCall":"aad915e1-5ff4-4bed-bf13-c423048ec97a","nameCaller":"Bao Pham",}}' --http2 --cert VOIP.pem:1234 https://api.development.push.apple.com/3/device/fc69f1f8b8ca704e9154676da34281451506e62305918578ea73a749017c4f83
//class Data {
//    let uuid: String
//    let nameCaller: String
//    let appName: String
//    let handle: String
//    let avatar: String	
//    let type: Int
//    let duration: Int
//    let extra: [String: Any?]
//
//    //iOS
//    let iconName: String
//    let handleType: String
//    let supportsVideo: Bool
//    let maximumCallGroups: Int
//    let maximumCallsPerCallGroup: Int
//    let supportsDTMF: Bool
//    let supportsHolding: Bool
//    let supportsGrouping: Bool
//    let supportsUngrouping: Bool
//    let includesCallsInRecents: Bool
//    let ringtonePath: String
//    let audioSessionMode: String
//    let audioSessionActive: Bool
//    let audioSessionPreferredSampleRate: Double
//    let audioSessionPreferredIOBufferDuration: Double
//
//
//    init(args: [String: Any?]) {
//        self.uuid = args["id"] as? String ?? ""
//        self.nameCaller = args["nameCaller"] as? String ?? ""
//        self.appName = args["appName"] as? String ?? "Callkit"
//        self.handle = args["handle"] as? String ?? ""
//        self.avatar = args["avatar"] as? String ?? ""
//        self.type = args["type"] as? Int ?? 0
//        self.duration = args["duration"] as? Int ?? 30000
//        self.extra = args["extra"] as? [String: Any?] ?? [:]
//
//
//        if let ios = args["ios"] as? [String: Any] {
//            self.iconName = ios["iconName"] as? String ?? ""
//            self.handleType = ios["handleType"] as? String ?? ""
//            self.supportsVideo = ios["supportsVideo"] as? Bool ?? true
//            self.maximumCallGroups = ios["maximumCallGroups"] as? Int ?? 2
//            self.maximumCallsPerCallGroup = ios["maximumCallsPerCallGroup"] as? Int ?? 1
//            self.supportsDTMF = ios["supportsDTMF"] as? Bool ?? true
//            self.supportsHolding = ios["supportsHolding"] as? Bool ?? true
//            self.supportsGrouping = ios["supportsGrouping"] as? Bool ?? true
//            self.supportsUngrouping = ios["supportsUngrouping"] as? Bool ?? true
//            self.includesCallsInRecents = ios["includesCallsInRecents"] as? Bool ?? true
//            self.ringtonePath = ios["ringtonePath"] as? String ?? ""
//            self.audioSessionMode = ios["audioSessionMode"] as? String ?? ""
//            self.audioSessionActive = ios["audioSessionActive"] as? Bool ?? true
//            self.audioSessionPreferredSampleRate = ios["audioSessionPreferredSampleRate"] as? Double ?? 44100.0
//            self.audioSessionPreferredIOBufferDuration = ios["audioSessionPreferredIOBufferDuration"] as? Double ?? 0.005
//        }else {
//            self.iconName = ""
//            self.handleType = ""
//            self.supportsVideo = true
//            self.maximumCallGroups = 2
//            self.maximumCallsPerCallGroup = 1
//            self.supportsDTMF = true
//            self.supportsHolding = true
//            self.supportsGrouping = true
//            self.supportsUngrouping = true
//            self.includesCallsInRecents = true
//            self.ringtonePath = ""
//            self.audioSessionMode = ""
//            self.audioSessionActive = true
//            self.audioSessionPreferredSampleRate = 44100.0
//            self.audioSessionPreferredIOBufferDuration = 0.005
//        }
//    }
//
//    func toJSON() -> [String: Any?] {
//        let ios = [
//            "iconName": iconName,
//            "handleType": handleType,
//            "supportsVideo": supportsVideo,
//            "maximumCallGroups": maximumCallGroups,
//            "maximumCallsPerCallGroup": maximumCallsPerCallGroup,
//            "supportsDTMF": supportsDTMF,
//            "supportsHolding": supportsHolding,
//            "supportsGrouping": supportsGrouping,
//            "supportsUngrouping": supportsUngrouping,
//            "includesCallsInRecents": includesCallsInRecents,
//            "ringtonePath": ringtonePath,
//            "audioSessionMode": audioSessionMode,
//            "audioSessionActive": audioSessionActive,
//            "audioSessionPreferredSampleRate": audioSessionPreferredSampleRate,
//            "audioSessionPreferredIOBufferDuration": audioSessionPreferredIOBufferDuration
//        ] as [String : Any?]
//        let map = [
//            "uuid": uuid,
//            "nameCaller": nameCaller,
//            "appName": appName,
//            "handle": handle,
//            "avatar": avatar,
//            "type": type,
//            "duration": duration,
//            "extra": extra,
//            "ios": ios
//        ] as [String : Any?]
//        return map
//    }
//
//}
