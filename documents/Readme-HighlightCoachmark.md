# Hướng dẫn sử dụng Highlight Coachmark (Dùng để tạo screen hướng dẫn)

> _Highlight Coachmark usage tutorial_

# 1. CoachMark

Đây là class chính trong phần highlight coachmark, chúng ta sẽ sử dụng nó để show screen tutorial với phần highlight.

Đối với tham số truyển vào class này thì chỉ cần quan tâm đến _bgColor_ dùng để set màu background cho screen tutorial

Thứ cần được quan tâm nhiều hơn là hàm _show()_ của class này. Hàm này có nhiệm vụ show ra screen tutorial, với các tham số truyền vào như sau:

- targetContext: tham số thuộc kiểu BuildContext, là nơi mà tutorial screen trỏ tới
- children: Những thứ sẽ hiển thị trên tutorial screen (mũi tên, hình ảnh, chú thích,...), nên set position cho chúng bằng cách bọc widget **Positioned** ở ngoài
- markRect: Vùng được chỉ định sẽ được highlight
- markShape: Hình dạng của vùng highlight, nếu bỏ trống sẽ mặc định là hình tròn
- onClose: Hàm này kích hoạt khi đóng screen tutorial

## 2. Hướng dẫn sử dụng

- Đầu tiên, chúng ta phải tạo CoachMark trước, cú pháp tạo CoachMark như sau:

```
CoachMark _coachMark = CoachMark();
```

**Lưu ý**: _Với mỗi screen tutorial thì phải sử dụng 1 coach mark riêng biệt, nếu không sẽ báo lỗi trùng key._

- Tiếp theo, xác định target mà tutorial screen đó sẽ chỉ tới (phần phát sáng hay highlight). Ở đây, chúng ta sẽ sử dụng key của widget mà ta định chỉ tới để lấy currentContext, sau đó dùng currentContext để tìm ra render object.

**Lưu ý**: _RenderObject phải được chuyển sang dạng RenderBox để tiện cho việc xác định vị trí, hình dạng, kích thước của phần highlight._

```
RenderBox _target = key.currentContext!.findRenderObject() as RenderBox;
```

- Tiếp theo, sử dụng kết hợp vị trí và kích thước của target để tạo hình dạng của phần highlight, cú pháp cụ thể tham khảo dưới đây

```
Rect _markRect = _target.localToGlobal(Offset.zero) & _target.size;
_markRect = Rect.fromCircle(
    center: _markRect.center,
    radius: _markRect.height,
);
```

- Cuối cùng, gọi hàm coachmark.show() và truyền vào các tham số để hiện ra tutorial screen

## 3. Code mẫu:

### 3.1. Ví dụ 1:

```
final CoachMark _coachMark = CoachMark();
RenderBox _target =
    _popUpKey.currentContext!.findRenderObject() as RenderBox;
Rect _markRect = _target.localToGlobal(Offset.zero) & _target.size;
_markRect = Rect.fromCircle(
    center: _markRect.center,
    radius: _markRect.height / 3,
);
_coachMark.show(
  targetContext: _popUpKey.currentContext!,
  children: [
    Positioned(
      top: _markRect.top - _markRect.height / 3,
      right: (100.w - _markRect.center.dx) + _markRect.width / 2,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 18.sp),
        height: 43.sp,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              width: 46.4.w,
              child: Text(
                "Chọn để xem lịch theo tháng hoặc tuần",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 13.sp,
                  fontWeight: FontWeight.w600,
                  height: 1.625,
                  color: colorWhiteCard,
                ),
              ),
            ),
            SizedBox(width: 13.sp),
            Image.asset(imageArrowTutorial, width: 12.w),
          ],
        ),
      ),
    ),
  ],
  markRect: _markRect,
  onClose: () {},
);
```

![Kết quả ví dụ 1](./readme_images/highlight_coachmark_1.png)

### 3.2. Ví dụ 2

```
final CoachMark _coachMark = CoachMark();
RenderBox _target = _calendarKey.currentContext!.findRenderObject() as RenderBox;
Rect _markRect = _target.localToGlobal(Offset.zero) & _target.size;
_markRect = Rect.fromCenter(
  center: _markRect.center,
  height: 0,
  width: 0,
);
_coachMark.show(
  targetContext: _calendarKey.currentContext!,
  children: [
    Positioned(
      top: _markRect.center.dy - (6.7.h + 43.sp + 23.sp) / 2,
      left: _markRect.center.dx - 31.2.w,
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset(imageSwipeTutorial, width: 52.w, height: 6.7.h),
            SizedBox(height: 23.sp),
            SizedBox(
              width: 62.4.w,
              height: 43.sp,
              child: Text(
                "Lướt lịch sang trái hoặc phải để xem những tháng khác",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 13.sp,
                  fontWeight: FontWeight.w600,
                  height: 1.625,
                  color: colorWhiteCard,
                ),
              ),
            ),
          ],
        ),
      ),
    ),
  ],
  markRect: _markRect,
  onClose: () {
    _showPopUpMenuTutorial();
  },
);
```

![Kết quả ví dụ 2](./readme_images/highlight_coachmark_2.png)
