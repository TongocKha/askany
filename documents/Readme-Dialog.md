# Hướng dẫn sử dụng các dialogs dùng chung trong project Askany Mobile

> _Đây mới chỉ là những dialog có thể tái sử dụng nhiều tính tới thời điểm này, nếu sau này còn dialog nào có thể tái sử dụng sẽ có cập nhật sau_

## 1. DialogWithImageAndText

### Danh sách thuộc tính

- text: Phần chữ của dialog (_Không thể để trống_)
- image: Phần hình ảnh của dialog (_Không thể để trống_)
- padding: Padding của phần nội dung của dialog (_Có thể để trống, đang set mặc định là EdgeInsets.symmetric(vertical: 24.sp, horizontal: 50.sp)_)
- textColor: Màu chữ của text (_Có thể để trống, đang set mặc định là colorBlack1_)
- textFontSize: Cỡ chữ của text (_Có thể để trống, đang set mặc định là 13.sp_)

### Hình ảnh mẫu

![Dialog with image and text](./readme_images/dialog_with_image_and_text.png)

### Code mẫu

```
DialogWithImageAndText(
    image: Image.asset(
        imageRegisterSuccess,
        width: 80.sp,
        height: 80.sp,
    ),
    padding: EdgeInsets.symmetric(
        vertical: 26.sp,
        horizontal: 20.sp,
    ),
    text: 'Chúc mừng bạn đã đăng ký thành công!',
    textFontSize: 12.5.sp,
),
```

## 2. DialogWithTextAndPopButton

### Danh sách thuộc tính

- title: Phần title của dialog (_Có thể để trống, nếu để trống sẽ có dạng giống mẫu 3_)
- bodyBefore: Phần chữ body của dialog trước phần highlight (_Có thể để trống, nhưng không thể cùng đồng thời để trống với bodyAfter_)
- highlightText: Phần chữ được highlight của body (_Có thể để trống_)
- bodyAfter: Phần chữ body của dialog sau phần highlight (_Có thể để trống, nhưng không thể cùng đồng thời để trống với bodyBefore_)
- buttonText: Phần chữ của button (_Có thể để trống, đang set mặc định là "ĐÃ HIỂU"_)
- titleColor: Màu chữ của title (_Có thể để trống, đang set mặc định là colorBlack1_)
- bodyColor: Màu chữ của body không bao gồm phần highlight (_Có thể để trống, đang set mặc định là colorBlack2_)
- highlightColor: Màu chữ của phần highlight (_Có thể để trống, đang set mặc định là colorGreen2_)
- titleFontSize: Cỡ chữ của title (_Có thể để trống, đang set mặc định là 15.sp_)
- bodyFontSize: Cỡ chữ của body (_Có thể để trống, đang set mặc định là 12.sp_)
- bodyAlign: Text alignment của phần body (_Có thể để trống, đang set mặc định là TextAlign.justify_)
- titleAlign: Text alignment của phần title (_Có thể để trống, đang set mặc định là TextAlign.center_)
- padding: Padding của phần nội dung (_Có thể để trống, đang set mặc định là EdgeInsets.only(left: 16.sp, right: 16.sp, top: 22.sp, bottom: 18.sp)_)

### Hình ảnh mẫu

![Dialog with text and pop button sample 1](./readme_images/dialog_with_text_and_pop_button_1.png)
![Dialog with text and pop button sample 2](./readme_images/dialog_with_text_and_pop_button_2.png)
![Dialog with text and pop button sample 3](./readme_images/dialog_with_text_and_pop_button_3.png)

### Code mẫu

```
DialogWithTextAndPopButton(
    title: 'Xác thực email',
    bodyBefore: 'Vui lòng truy cập',
    highlightText: 'hoanglam345@gmail.com',
    bodyAfter:
        'và làm theo hướng dẫn để xác thực email. (Lưu ý kiểm tra mục Spam nếu không nhận được email xác thực)',
),
```

## 3. DialogNotifyError

### Danh sách thuộc tính

- title: Phần chữ title (_Có thể để trống, nếu để trống sẽ giống với mẫu 2_)
- bodyBefore: Phần chữ body của dialog trước phần highlight (_Có thể để trống, nhưng không thể cùng đồng thời để trống với bodyAfter_)
- highlightText: Phần chữ được highlight của body (_Có thể để trống_)
- bodyAfter: Phần chữ body của dialog sau phần highlight (_Có thể để trống, nhưng không thể cùng đồng thời để trống với bodyBefore_)
- cancelText: Phần chữ của button cancel bên trái dùng để pop dialog (_Có thể để trống, nếu để trống sẽ giống với mẫu 2_)
- confirmText: Phần chữ của button confirm bên phải (_Có thể để trống, đang để mặc định là "XÁC NHẬN"_)
- titleColor: Màu chữ của title (_Có thể để trống, đang để mặc định là colorBlack1_)
- bodyColor: Màu chữ của body không bao gồm phần highlight (_Có thể để trống, đang để mặc định là colorBlack1_)
- highlightColor: Màu chữ của phần highlight (_Có thể để trống, đang để mặc định là colorGreen2_)
- onConfirmed: Hàm thực hiện khi người dùng chọn button confirm bên phải (_Không thể để trống_)

### Hình ảnh mẫu

![Dialog confirm cancel sample 1](./readme_images/dialog_confirm_cancel_1.png)
![Dialog confirm cancel sample 2](./readme_images/dialog_confirm_cancel_2.png)

### Code mẫu

```
DialogConfirmCancel(
    bodyBefore: 'Vui lòng đăng ký tài khoản trên website Askany.com',
    confirmText: 'Chuyển đến website',
    onConfirmed: () {
        AppNavigator.pop();
        UrlLauncherHelper.launchUrl(
            'https://askanydev.tk/dang-ky',
        );
    },
),
```
### Danh sách thuộc tính

- title: Phần chữ title (_Không thể để trống_)
- body: Phần chữ body của dialog (_Không thể để trống_)

### Hình ảnh mẫu

![Dialog notify error](./readme_images/dialog_notify_error.png)

### Code mẫu

```
DialogNotifyError(
    title: 'Server lỗi!',
    body: 'Vui lòng thử lại sau.',
),
```
# ------------------------------------------------------------

Written by: Lê Thành Luân
