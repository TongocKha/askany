# Document về luồng Chat của app Askany

> _Giải thích về luồng của chat và các tính năng của phần chat_

## 1. Màn hình hiển thị các cuộc trò chuyện(ChatScreen)

**Chèn hình ChatScreen**

Màn hình này hiển thị danh sách các trò chuyện. Các list này có dạng **PaginationListView**, mỗi item là 1 **ChatCard** nhận vào 1 **ConversationModel**.

 Khi user chọn Tin nhắn, sẽ gọi event **GetConversationEvent** của ChatBloc. Nếu state trả về **GetDoneConversation**  thì hiển thị danh sách trò chuyện.

 Bên trong **ChatCard** còn có 1 local variable là _isSeenLatestMessage, xác định user đã xem tin nhắn cuối cùng hay chưa để hiện thị giao diện phù hợp (chi tiết xem bản desgin), không cần set value cho variable này, vì giá trị của nó được lấy từ **widget.conversationModel.latestMessage.isSeen**

### 1.1. ChatCard

Nhận vào 1 **ConversationModel**, hiển thị phần thông tin về cuộc trò chuyện (chi tiết xem bản design). Mỗi cuộc trò chuyện có 2 cách giữ kéo, với mỗi cách kéo user có button chỉnh sửa cuộc trò chuyện khác nhau:

- Kéo từ phải sang trái: Hiển thị button Thêm và button Gọi (Sử dụng **ButtonSlidable**)
- Kéo từ trái sang phải: Hiện thị button Xóa (Sử dụng **ButtonSlidable**)

**(*) Danh sách thuộc tính ButtonSlidable**

 **Chèn hình ButtonSlidable**
- title: Phần chữ button (Không thể để trống)
- backgroundColor: phần font nền của button (Không thể để trống)
- colorIcon: Màu icon trong button (Không thể để trống)
- imageAsset: Phần Icon của button (Không thể để trống)
- colorText : Màu chữ của button (Có thể để trống, đang để mặc định là Colors.white)
- handlePressed : Hàm thực hiện khi người dùng chọn button (Không thể để trống)


#### 1.1.1 Button Thêm

Khi nhấn vào button sẽ hiển thị 1 **BottomChatOptions**.

BottomSheet có các button (Đổi tên, Thông tin chuyên gia,Đánh dấu chưa đọc, Tắt thông báo, Xóa trò chuyện, Hủy).

##### 1.1.1.1 Button Đổi tên

Khi nhấn button sẽ hiển thị 1 **DialogInput**, khi đó sẽ gọi event **ChangeConversationNameEvent** của chatBloc, user có thể đổi trường **ConversationName** trong **ConversationModel**.


##### 1.1.1.2 Button Thông tin chuyên gia

Khi nhấn vào Button sẽ hiển thị màn hình chi tiết thông tin chuyên gia

##### 1.1.1.3 Button Đánh dấu chưa đọc
Button sẽ được hiển thị nếu tin nhắn cuối cùng là tin nhắn expert gửi cho user

Khi nhấn vào button, khi đó sẽ gọi event **MarkReadEvent** của chatBloc. Nếu thành công, thực hiện **GetDoneConversation** sẽ được cập nhật lại field **isSeen** trong **LastMessage**.


##### 1.1.1.4 Button Tắt thông báo
##### 1.1.1.5 Button Xóa trò chuyện

Khi nhấn Button sẽ hiển thị 1 **BottomDelete** (Xác nhận muốn xóa hay không).

Nếu xác nhận muốn xóa sẽ gọi event **DeleteConversationEvent** của chatBloc. Nếu thành công, thực hiện **GetDoneConversation** sẽ xóa cuộc trò chuyện được chọn trong List Conversation của user.                           
##### 1.1.1.6 Button Hủy

Khi nhấn button sẽ đóng bottomSheet.

#### 1.1.2 Button Call
Khi nhấn button sẽ gọi event **RequestVideoCallEvent** của videoBloc. Nếu thành công, thực hiện gọi expert.

### 1.1.3 Button Delete 
Khi nhấn Button sẽ hiển thị 1 **BottomDelete** (Xác nhận muốn xóa hay không).

Nếu xác nhận muốn xóa sẽ gọi event **DeleteConversationEvent** của chatBloc. Nếu thành công, thực hiện **GetDoneConversation** sẽ xóa cuộc trò chuyện được chọn trong List Conversation của user.             







