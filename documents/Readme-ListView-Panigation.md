# Hướng dẫn sử dụng Listview Panigation dùng chung trong project Askany Mobile

> _Đây mới chỉ là Listview Panigation có thể tái sử dụng nhiều tính tới thời điểm này, nếu sau này có thể tái sử dụng sẽ có cập nhật sau_

### Code mẫu

```
PaginationListView(
            itemCount: skills.length + 1,
            callBackLoadMore: () {
              AppBloc.skillBloc.add(GetSkillEvent());
            },
            padding: EdgeInsets.only(bottom: 48.sp),
            isLoadMore: state is GettingSkillFinished,
            childShimmer: Container(),
            itemBuilder: (context, index) {
              return index == skills.length
                  ? TouchableOpacity(
                      onTap: () {
                        AppNavigator.push(Routes.ADD_SKILL);
                      },
                      child: Container(
                        margin: EdgeInsets.only(top: 12.sp),
                        child: Text(
                          "+ Thêm kỹ năng",
                          style: TextStyle(
                            color: colorFontGreen,
                            fontSize: 13.sp,
                          ),
                          textAlign: TextAlign.left,
                        ),
                      ),
                    )
                  : SkillCard(
                      title: skills[index].name,
                      price: skills[index].callPrice!.costString,
                      time: skills[index].meetPrice != null
                          ? skills[index].meetPrice!.totalMinutes.toString()
                          : skills[index].callPrice!.totalMinutes.toString(),
                      bullet1: skills[index].expExperience.toString() + " năm kinh nghiệm",
                      bullet2: skills[index].highestPosition != null
                          ? skills[index].highestPosition!.getLocaleTitle!
                          : '',
                      bullet3: skills[index].companyName,
                      onTap: () {
                        AppNavigator.push(Routes.ADD_SKILL, arguments: {
                          'skillModel': skills[index],
                        });
                      },
                    );
            },
          );
```

### Danh sách thuộc tính
    required this.itemCount,
    required this.itemBuilder,
    required this.childShimmer,
    this.callBackLoadMore,
    this.padding = EdgeInsets.zero,
    this.scrollDirection = Axis.vertical,
    this.isLoadMore = false,

Lưu ý nếu có required bắt buộc phải nhập không được để trống

- required this.itemCount: số phần tử của item list

- required this.itemBuilder: item dùng để build thành list của bạn

- required this.childShimmer: widget cardshimmer add vào đây để cho phép cộng card shimmer  hiệu ứng load  ở cuối list  khi vẫn còn data chưa load xong

- this.callBackLoadMore: lệnh chạy hàm add event bloc call data 

- this.padding = EdgeInsets.zero: padding cho list view builder

- this.scrollDirection = Axis.vertical: hướng load list view  mặc định load dọc, có thể chỉnh thêm thành ngang  nếu nhập giá trị vào chỗ này

- this.isLoadMore = false: Mặc định là false không cộng thêm shimmer vào nếu true thì cộng thêm shimmer card vào




## STEP BY STEP LIST VIEW PANIGATION HOW TO BUILD

## 1. Tạo shimmer card theo cấu trúc UI item của case cần giải quyết 
	
### Mẫu code

```
Container(
      margin: EdgeInsets.symmetric(vertical: 7.5.sp),
      padding: EdgeInsets.symmetric(vertical: 16.sp, horizontal: 8.sp),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(6.5.sp),
        border: Border.all(
          color: colorGray2, // Set border color
          width: 0.5.sp,
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                margin: EdgeInsets.fromLTRB(0, 0, 7.sp, 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    FadeShimmer(
                      height: 12.5.sp,
                      width: 60.w,
                      fadeTheme: FadeTheme.lightReverse,
                    ),
                    SizedBox(height: 4.sp),
                    FadeShimmer(
                      height: 12.sp,
                      width: 80.sp,
                      fadeTheme: FadeTheme.lightReverse,
                    ),
                  ],
                ),
              ),
              FadeShimmer(
                width: 24.sp,
                height: 24.sp,
                fadeTheme: FadeTheme.lightReverse,
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 10.sp),
            child: dividerChat,
          ),
          FadeShimmer(
            width: 40.w,
            height: 11.sp,
            fadeTheme: FadeTheme.lightReverse,
          ),
          SizedBox(height: 6.sp),
          FadeShimmer(
            width: 40.w,
            height: 11.sp,
            fadeTheme: FadeTheme.lightReverse,
          )
        ],
      ),
    )

```

FadeShimmer:
- radius: Chỉnh bo góc shimmer,
- fadeTheme: Phần màu mặc định của máy ,
- highlightColor: Màu của shimmer,
- baseColor: Màu của shimmer,
- required width: Chiều dài shimmer,
- required height: Chiều rộng shimmer,
- millisecondsDelay: Chỉnh giây hiển thị shimmer,

## 2. Tạo bloc cho case cần giải quyết

### bước 2.1 Khai báo thuộc tính trong bloc

```
  List<SkillModel> skills = [];
  bool isOverSkills = false;

```

### bước 2.2 Tạo hàm get list ra 

 ```
 // Private methods
  Future<void> _getListSkills() async {
    List<SkillModel> _skills = await SkillRepository().getListSkill(
      //skip bỏ qua các phần tử đã có trong list
      skip: skills.length,
    );
    //_skills  nếu list trống thì chứng tỏ đã lấy hết list ra không cần call api nữa
    if (_skills.isEmpty) {
      isOverSkills = true;
    } else {
      skills.addAll(_skills);
    }
  }

 ```


### bước 2.3 Tạo 1 reposity cho getlist gọi data ra
tạo class repository trong folder remote data source

```
tạo 1 endpoit trong file enpoint ở constants
  static const String GET_SKILL = 'skills';
```

```
import 'base_repository.dart';

class SkillRepository {
Future<List<SkillModel>> getListSkill({
    required int skip,
    int status = 1,
    int limit = 10,
  }) async {
    Response response = await BaseRepository().getRoute(

      Endpoints.GET_SKILL + '/' + AppBloc.userBloc.accountModel!.id!,
      query: 'status=$status&skip=$skip&limit=$limit',
        // status  1 - hoat dong , 0 - khóa, -1 xóa
	// skip bỏ qua số phần tử đã có trong list rồi
	// limit giới hạn số phần tử được lấy từ api
    );

    if (response.statusCode == StatusCode.OK) {
      final skills = response.data['data'] as List;
      return skills.map((skill) => SkillModel.fromMap(skill)).toList();
     // tạo model get từ json convert ra model
    }

    return [];
  }
}

```


### bước 2.4 Tạo hàm return list ra lưu vào state

```

  GettingSkillFinished get _gettingSkills => GettingSkillFinished(skills: skills);

  GetDoneSkillFinished get _getDoneSkills => GetDoneSkillFinished(skills: skills);


```

### bước 2.5 Tạo gàm clean list
hàm này dùng để bỏ vào dipose

```
  _cleanBloc() {
    skills = [];
    isOverSkills = false;
  }


```

### bước 2.6 Tạo state lưu list lại hiển thị bên widget list

class GettingSkillFinished extends SkillState {
  final List<SkillModel> skills;
  GettingSkillFinished({required this.skills});

  @override
  List get props => [skills];
}

class GetDoneSkillFinished extends SkillState {
  final List<SkillModel> skills;
  GetDoneSkillFinished({required this.skills});

  @override
  List get props => [skills];
}

### bước 2.7 Add event cho bloc

```

 if (event is GetSkillEvent) {
      if (skills.isEmpty) {
        yield SkillInitial();
      } else {
        yield _gettingSkills;
      }

      if (!isOverSkills) {
        await _getListSkills();
      }

      yield _getDoneSkills;
    }


```

## 3. Tạo 1 list của case cần giải quyết

### bước 3.1 Add event cho lần đầu tiên để load list lần đầu

- Mẫu Code

```
  @override
  void initState() {
    super.initState();
    AppBloc.skillBloc.add(GetSkillEvent());
  }

```

### bước 3.2 Add Bloc kèm theo PanigationListView đã viết sẵn

```
BlocBuilder<SkillBloc, SkillState>(
      builder: (context, state) {
	//GettingSkillFinished đóng vai trò là event load và hiện shimmer khi load 
	//GetDoneSkillFinished đóng vai trò là event load done và kill shimmer khi load xong

        if (state is GettingSkillFinished || state is GetDoneSkillFinished) {
          List<SkillModel> skills = state.props[0] as List<SkillModel>;
          return PaginationListView(
            itemCount: skills.length + 1,
            callBackLoadMore: () {
              AppBloc.skillBloc.add(GetSkillEvent())
	      // call lệnh load tiếp list khi kéo về cuối list item
            },
            padding: EdgeInsets.only(bottom: 48.sp),
            isLoadMore: state is GettingSkillFinished,
  	    // isLoadMore là lệnh check khi nào thì được hiện shimmer
            childShimmer: Container(),
            itemBuilder: (context, index) {
              return index == skills.length
                  ? TouchableOpacity(
                      onTap: () {
                        AppNavigator.push(Routes.ADD_SKILL);
                      },
                      child: Container(
                        margin: EdgeInsets.only(top: 12.sp),
                        child: Text(
                          "+ Thêm kỹ năng",
                          style: TextStyle(
                            color: colorFontGreen,
                            fontSize: 13.sp,
                          ),
                          textAlign: TextAlign.left,
                        ),
                      ),
                    )
                  : SkillCard(
                      title: skills[index].name,
                      price: skills[index].callPrice!.costString,
                      time: skills[index].meetPrice != null
                          ? skills[index].meetPrice!.totalMinutes.toString()
                          : skills[index].callPrice!.totalMinutes.toString(),
                      bullet1: skills[index].expExperience.toString() + " năm kinh nghiệm",
                      bullet2: skills[index].highestPosition != null
                          ? skills[index].highestPosition!.getLocaleTitle!
                          : '',
                      bullet3: skills[index].companyName,
                      onTap: () {
                        AppNavigator.push(Routes.ADD_SKILL, arguments: {
                          'skillModel': skills[index],
                        });
                      },
                    );
            },
          );
        }

        // Show shimmer list khi chưa bắt được event load list
        return ListView.builder(
          physics: BouncingScrollPhysics(),
          padding: EdgeInsets.only(bottom: 30.sp),
          itemCount: ITEM_COUNT_SHIMMER,
          itemBuilder: (context, index) {
            return SkillShimmerCard();
          },
        );
      },
    );

```



# ------------------------------------------------------------
