# Hướng dẫn sử dụng Incomming call IOS trong project Askany Mobile

> _Đây mới chỉ là khởi đầu tính tới thời điểm này, nếu sau này rule Incomming call IOS có thay đổi sẽ có cập nhật sau_

## 1. Device(UI) gửi gì lên server?

### Mô tả bằng lời
- Device A: Device get push token voip gửi lên server. 
- Device B: Device get push token voip gửi lên server. 
- Server: Server save lại token đó.
- Server: Rồi trả về uuid. 
(_Lưu ý: Mỗi 1 device sẽ phải khởi tạo uuid unique nó như mã định danh và xác định device đó _)
- Device A: Call Device B.
- Server: Server gửi uuid của A cho B thông qua payload của apns voip service.
- Device B: Sau khi nhận được thông báo B dùng cái uuid unique của A để show call incoming.
(_Lưu ý: Nếu lần sau call lại thì sẽ được lưu là device định danh uuid đó IOS lưu history cùng 1 user .Mỗi cái uuid như là unique sdt _)
### Tổng kết
- Tổng cộng server phải save 2 token trên mỗi user là fcm Firebase và apns voip service và tự tạo ra UUID.

## 2. Server push apns voip service như nào?

### Để push apns voip service cho device IOS server cần làm những việc như sau:
- Đầu tiên phải có file VOIPservice vậy nó lấy từ Apple develop đăng ký dịch vụ voip service
(_ Hiện file này đã được tải về _)
- Sau đó import vào máy mac và khởi chạy để export ra file p12 từ key chain access
- Sau đó khởi chạy lệnh convert sang file pem để chạy lệnh voip service 
- Khởi chạy lệnh sau ta sẽ push
- Done việc còn lại front end set up trên native push voip service lắng nghe và khởi động incoming call

### Command line mẫu push 1 apns

```
- (_ curl -v -d '{"aps":{"alert":"This is a test notification"}}' --http2 --cert VOIP.pem:1234 https://api.development.push.apple.com/3/device/8bc7c5dd0dcf77ef1c6970d3c0a45212ed97bebb46a8c7f4f79df8b83441605b _)
```

### Command line mẫu push 1 apns voip service

```
- (_ curl -v -d '{"aps":{"alert":"aad915e1-5ff4-4bed-bf13-c423048ec97a {uuid ở đây xóa uuid này đi}"}}' --http2 --cert VOIP.pem:1234 https://api.development.push.apple.com/3/device/{device token voip service ở đây} _)
```

# ------------------------------------------------------------

