## PRJECT BACKLOG - Scrum Master [lambiengcode](https://github.com/hongvinhmobile)

### [Before - 28/2/2021]
- Done UI phase 1 Askany ✅
- Done Calendar ✅
- Done Request ✅
- Done Chat ✅
- Done Account ✅
- Video Call
  - Android: ✅
  - iOS:
    - Share Screen ❌
    - Incoming Call(Sound) ❌

### [28/2/2021 - 4/3/2021]

- UI Group Home Screen Askany 
  - Home Screen ✅
  - Discover Screen ✅
  - Search Screen ✅
  - Filter Result Screen ✅
  - Intro Expert Screen ✅
  - Dialog Info Expert ✅
  - Details Video Discover ✅


- UI Group Category
  - Category Screen ✅
  - Sub Category Screen ✅


- UI Register Expert
  - Step 1 ✅
  - Step 2 ✅
  - Step 3 ✅
  - Step 4 ✅

### [7/3/2021 - 11/3/2021]

- Home Screen
  - Video Player
  - Toggle Header When Scroll
  - Map API:
    - API fetch category
    - API search
    - API fetch video
    - API fetch info expert & reviews

- UI Category
  - Details Category Screen
  - Profile Expert Screen
  - Skills Screen

- UI Account
  - Account Screen
  - Account Expert Screen
  - Wallet Screen
  - Transactions Screen
  - Experience Expert Screen
  - Edit Profile Expert Screen