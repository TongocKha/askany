# Document về luồng request của app Askany

> _Giải thích về luồng của request và các tính năng của phần request_

## 1. Màn hình hiển thị các yêu cầu (RequestScreen)

Màn hình này hiển thị danh sách các yêu cầu, chia làm 2 loại là **HappeningList** (các yêu cầu đang diễn ra) và **FinishedList** (các yêu cầu đã kết thúc). Các list này có dạng **PaginationListView**, mỗi item là 1 **RequestCard** nhận vào 1 **RequestModel**.

Khi user chọn **HappeningList**, sẽ gọi event **OnHappeningEvent** của **RequestBloc**. Nếu state trả về **GettingRequestHappening** hoặc **GetDoneRequestHappening** thì hiển thị danh sách request. Tương tự đối với **FinishedList**.

Bên trong **RequestCard** còn có 1 local variable là _\_isExpert_, xác định account đang đăng nhập có phải chuyên gia không để hiển thị giao diện phù hợp (chi tiết xem bản design), không cần set value cho variable này, vì giá trị của nó được lấy từ **AppBloc.userBloc.accountModel!.isExpert**. Khi tap vào **RequestCard** sẽ dẫn đến screen **DetailsRequestScreen**.

## 2. Màn hình chi tiết yêu cầu (DetailsRequestScreen)

Màn hình này hiển thị chi tiết dịch vụ, nhận vào 1 **RequestModel**, có 1 local variable _\_isExpert_ (vai trò cũng giống như ở **RequestCard**). Màn hình này bao gồm các phần sau:

### 2.1. RequestDetailsCard

Nhận vào 1 **RequestModel**, hiển thị phần thông tin chi tiết về yêu cầu (chi tiết xem bản design). Khi nhấn vào button chỉnh sửa thì có 2 trường hợp:

- Request đã có offer: Không cho phép chỉnh sửa, hỏi người dùng nếu muốn xóa, nếu người dùng xác nhận xóa thì gọi event **DeleteRequestEvent**
- Request chưa có offer: Cho phép chỉnh sửa, đưa người dùng đến giao diện **CreateRequestScreen**

### 2.2. Phần dưới chia làm 2 case

#### 2.2.1. Expert

Hiển thị **\_MyOfferResult** nếu request có offer được chọn, hoặc status của request là expired. Trong **\_MyOfferResult** có button "Hủy chào giá", nếu người dùng nhấn vào sẽ gọi dialog xác nhận hủy, người dùng chọn "Xác nhận" thì gọi event **ExpertCancelOfferEvent**

Hiển thị **\_MyOffer** nếu request chưa có offer được chọn và status không phải là expired.

#### 2.2.2. Không phải Expert

Nếu request không có offer, hiển thị "Hiện chưa có lượt chào giá nào!", ngược lại, hiển thị **\_ListOffers** gồm các **OfferRequestCard**.

**OfferRequestCard** nhận vào 1 **OfferModel** và 1 **RequestModel**. Có button hiển thị chữ "Lựa chọn" hoặc "Được chọn" tùy vào status của request và offer. Khi button đang có chữ "Lựa chọn", tức là có thể chọn offer cho request đó, lúc này nếu user tap vào button sẽ gọi event **UserChooseOfferEvent** của **RequestBloc**, và chuyển hướng tới screen **OrderServiceScreen**. Khi button có chữ "Được chọn" nghĩa là request có offer đó được chọn.

## 3. OrderServiceScreen

Screen này nhận vào 1 **RequestModel** request và 1 **OfferModel** offer. Ngoài ra còn có các local variables sau:

Screen này được chia làm 3 thành phần

### 3.1. StepOrerService

Hiển thị cây progress các step, chi tiết xem design

### 3.2. Giao diện các step

#### 3.2.1. OrderServiceStepOne

Nhận vào các tham số:

- formKey: key của Form, dùng để check validation của form
- authorNameController: TextEditingController của TextField tên người dùng
- authorPhoneController: TextEditingController của TextField số điện thoại người dùng
- noteController: TextEditingController của TextField note

#### 3.2.2. OrderServiceStepTwo

Nhận vào các tham số:

- onDateSelected: Function kích hoạt khi chọn ngày trên **DialogDatePicker**
- onTimeSelected: Function kích hoạt khi chọn giờ trên time picker scroll view.
- initialStartTime: startTime khởi tạo, để lưu lại thời gian mà người dùng đã chọn nếu có chuyển đổi qua lại giữa các step
- initialEndTime: endTime khởi tạo, vai trò giống _initialStartTime_
- totalMinus: Độ dài cuộc hẹn, tính theo phút

Các local variables:

- \_scrollController: ScrollController cho time picker scroll view.
- \_startDateTime: Biến cục bộ lưu lại thời gian bắt đầu người dùng chọn
- \_endDateTime: Biến cục bộ lưu lại thời gian kết thúc người dùng chọn
- \_timeOfDate: Biến cục bộ lưu lại thời gian trong ngày người dùng đã pick
- \_isMorning: Biến detect khi người dùng scroll đến khoảng thời gian chiều hoặc sáng để thay đổi giao diện cho phù hợp
- \_listBusy: List này là danh sách thời gian bận của chuyên gia

Các functions:

- \_buildTimePicker(): Tạo nên time picker scroll view
- \_buildTimePickerTitle(): Tạo nên title "Sáng" - "Chiều" của time picker
- \_buildTimePickerButton(**bool** _isBusy_, **DateTime** _thisDateTimeValue_, **TimeOfDay** _thisTimeValue_, **int** _index_): Tạo nên time picker button cho time picker scroll view. Nhận vào _isBusy_ là thời gian đó chuyên gia có bận không, _thisDateTimeValue_ là giá trị DateTime của button đó, _thisTimeValue_ là giá trị thời gian trong ngày của button đó, _index_ là chỉ số của button trong list.
- \_buildDatePicker(): Tạo nên date picker
- _isStartTimeValid(**DateTime** \_startTime_): Check xem _startTime_ người dùng chọn có hợp lệ không
- \_concatListTimeBusy(): Nối những khoảng thời gian chết giữa các khung giờ expert bận với nhau và tạo thành list thời gian bận mới. Thời gian chết là những khoảng thời gian expert rảnh nhưng user không thể pick vì không đủ duration.
- \_isExpertBusy(**DateTime** _time_): Check xem thời gian đó expert có bận không
- \_isInTimeRange(**DateTime** _time_, **DateTime** _timeStart_, **DateTime?** _timeEnd_): Check xem _time_ đấy có trong khoảng thời gian user pick không, để cập nhật lại ui cho time picker.
- \_calculateEndDateTime(**DateTime** _startDateTime_): Tính thời gian kết thúc dựa trên _startTime_ và _totalMinus_
- \_convertStringToTime(**String** _timeString_): Chuyển đổi String thành dạng TimeOfDay
- \_addTimeToDate(**TimeOfDay** _time_, **DateTime** _date_): Thêm _time_ vào _date_, trả về _date_ mới với _time_

#### 3.2.3. OrderServiceStepThree

Nhận vào các tham số:

- request: Request đang thanh toán
- offer: Offer được pick
- onSelectedValueChanged: Function kích hoạt khi chọn phương thức thanh toán thay đổi

Các local variables:

- \_paymentMethod: Phương thức thanh toán được chọn
- \_wallet: Biến lưu lượng tiền trong ví của người dùng
- \_subTotal: Biến lưu tổng tiền tạm tính
- \_extraFee: Biến lưu phí thêm
- \_total: Biến lưu tổng tiền thanh toán

Các functions:

- \_buildPaymentInfo(): Tạo nên phần info của giao diện thanh toán
- \_buildPaymentMethod(): Tạo nên phần chọn phương thức thanh toán
- convertMoneyToString(**double** _money_, **String** _currency_): Chuyển đổi số thực _money_ thành dạng chuỗi tiền, dựa theo đơn vị tiền _currency_
- calculateTotal(**RequestModel** _request_, **OfferModel** _offer_): Tính tổng tiền
- calculateSubTotal(**RequestModel** _request_, **OfferModel** _offer_): Tính tổng tạm tính
- calculateExtraFee(**double** _subTotal_, **int** _numParticipants_): Tính phí cộng thêm

### 3.3. Button chuyển tiếp giữa các step

- Nếu ở step 1: Check validation của form thông tin user, nếu không vi phạm thì chuyển sang step 2
- Nếu ở step 2: Kiểm tra _startTime_ và _endTime_, nếu thời gian không hợp lệ thì hiển thị dialog thông báo lỗi, yêu cầu người dùng chọn lại thời gian. Nếu hợp lệ thì chuyển sang step 3
- Nếu ở step 3: Gọi event **UserPayOfferEvent**

## 4. Màn hình tạo request (CreateRequestScreen)

Nhận vào **RequestModel** request

Các local variables:

- \_formKey: Key của form, để check validation
- \_titleController: TextEditingController của TextField title
- \_contentController: TextEditingController của TextField content
- \_priceController: TextEditingController của TextField price
- \_quantityController: TextEditingController của TextField quantity
- \_locationNameController: TextEditingController của TextField location
- \_locationAddressController: TextEditingController của TextField location address
- \_isCreating: Biến detect xem có phải đang tạo request hay đang chỉnh sửa request để cập nhật UI
- \_title: Biến bắt text trong TextField title
- \_content: Biến bắt text trong TextField content
- \_currency: Biến bắt text trong TextField currency
- \_price: Biến bắt text trong TextField price
- \_quantity: Biến bắt text trong TextField quantity
- \_contactForm: Biến bắt value trong dropdown box contact form
- \_locationName: Biến bắt text trong TextField location name
- \_locationAddress: Biến bắt text trong TextField location address
- \_specialty: Biến bắt value trong dropdown box specialty
- \_selectedDate: Biến bắt giá trị DateTime trong date picker
- specialtiesTitle: Danh sách các specialties
- \_contactFormString: Lưu lại cách thức liên hệ làm giá trị mặc định khi edit request
- \_contactForms: Danh sách các cách thức liên hệ

Khi screen đang ở trạng thái tạo yêu cầu, nhấn button "Đăng yêu cầu" sẽ gọi event **CreateRequestEvent**

Khi screen đang ở trạng thái chỉnh sửa yêu cầu, nhấn button "Lưu" sẽ gọi event **UpdateRequestEvent**

## 5. Màn hình thanh toán WebView với VNPay (WebViewPaymentScreen)

Screen này được tạo nên sử dụng thư viện webview_flutter, nhận vào các tham số:

- url: Link dẫn tới trang thanh toán
- onPaymentDone: Function kích hoạt khi thanh toán xong

Local variables:

- \_key: Key của WebView
- \_gestureRecognizers: Xử lý để có thể thao tác cuộn, chạm trên WebView

## 6. Những lưu ý về các event

### 6.1. UserChooseOfferEvent

Thực hiện gọi API chọn offer trước, sau đó, nếu kết quả trả về status code 200, thực hiện cập nhật lại list request trên UI của app, đổi status của request đang xét là _REQUEST_PICKED_EXPERT_, offer được chọn là _OFFER_CHOSEN_.

### 6.2. UserPayOfferEvent

Thực hiện gọi API update timeline và user info. Nếu cập nhật thất bại thì return, ngắt thực hiện những dòng lệnh tiếp theo. Sau đó thì có 2 trường hợp:

- Phương pháp thanh toán bằng ví Askany: Gọi API thanh toán bằng ví Askany
- Phương pháp thanh toán bằng VNPay: Gọi API lấy link thanh toán bằng VNPay, sau đó đưa người dùng đến WebView với đường link được trả về. Nếu người dùng thoát ra không thanh toán thì return, ngắt thực hiện những dòng lệnh tiếp theo. Nếu người dùng thanh toán thì trả về kết quả thanh toán thành công hay thất bại thông qua hàm _onPaymenDone_.

Nếu người dùng thực hiện thanh toán thành công, tiến hành cập nhật lại list request trên UI của app, đổi status của request đang xét là _REQUEST_PAYMENTED_, offer được chọn là _OFFER_PAID_.

Sau đó, chuyển người dùng đến giao diện thông báo kết quả thanh toán.

### 6.3. CreateRequestEvent

Thực hiện gọi API tạo request, sau đó thêm request mới vào list request để cập nhật lại UI

### 6.4. UpdateRequestEvent

Thực hiện gọi API update request, sau đó thay request được update bằng request mới trong list request để cập nhật lại UI

### 6.5. DeleteRequestEvent

Thực hiện gọi API xóa request, sau đó xóa request được chọn ra khỏi list request để cập nhật lại UI

### 6.6. ExpertCancelOfferEvent

Khi expert cancel offer, thực hiện gọi API cancel offer, sau đó xóa request mà chứa offer đó khỏi list request để cập nhật lại UI
