## Askany Mobile Project (Readme version 1.1.0) :beer: :beer: :beer: 

### How to run? (Flutter version: 2.2.3)

Step 1:

Download or clone this repo by using the link below:

```terminal
git clone https://github.com/hongvinhmobile/askany_mobile.git
```

Step 2:

Go to project root and execute the following command in console to get the required dependencies and start:

```terminal
flutter pub get
flutter run
```

### Libraries & Tools Used
- [dio]()
- [local storage]()
- [bloc]()
- [i18n]()
- [sizer]()
- [intl]()

### Folder Structure

Here is the core folder structure which flutter bloc.

```terminal
flutter-app/
|- android
|- build
|- ios
|- lib
|- test
```

Here is the folder structure we have been using in this project

```terminal
lib/
|- src/
  |- bloc/
  |- configs/
  |- constants/
  |- data/
  |- helpers/
  |- models/
  |- routes/
  |- services/
  |- ui/
|- main.dart
```

Now, lets dive into the lib folder which has the main code for the application.

```terminal
1- bloc - All bloc in project
2- configs - Contains the config for application, such as: theme, languages,...
3- constants - All the application level constants are defined in this directory with-in their respective files.
4- data - Contains the data layer of your project, includes directories for local, network and shared pref/cache.
5- helpers - Contains the utilities/common functions of your application.
6- models - Define model using in application, support parse raw data to object
7- routes - Define route name and map screen with route
8- services - Contains all services using in application, such as: socket, firebase messaging, in app review, webrtc, etc..
9- ui - Contains all the ui of your project, contains sub directory for each screen.
```

### How can I extend the project? (Including Unit Test)

#### Documents of Project

- ******Dialog******: [Document](https://gitlab.com/lambiengcode/askany_mobile/-/blob/master/documents/Readme-Dialog.md)
- ******Incoming Call iOS******: [Document](https://gitlab.com/lambiengcode/askany_mobile/-/blob/master/documents/Readme-IncomingCall.md)
- ******Bloc & Repository******: [Document]()
- ******Network Image******: [Document]()
- ******Style******: [Document]()
- ******Local Storage for Cache Data Offline******: [Document]()

- ******Flow App******:
  + ***Home***: [Document]()
  + ***Request***: [Document]()
  + ***Category***: [Document]()
  + ***Message***: [Document]()
  + ***Notification***: [Document]()
  + ***User***: [Document]()
  + ***Authentication***: [Document]()
  + ***Video Call***: [Document]()

### How can I maintain the project?
[Update Later]()

# :beer:--------------- GIT CONVENTION ---------------:beer:


### Quy tắc commit code

( Làm sao commit code cho hiệu quả)

#### Cấu trúc

[type: optional] description


#### Một số type phổ biến:
    1.1. feat: Thêm mới một feature

    1.2. fix: fix bug cho hệ thống

    1.3. refactor: sửa code nhưng không fix bug cũng không thêm feature hoặc đôi khi bug cũng được fix từ việc refacto

    1.4. docs: thêm/thay đổi document

    1.5. chore: những sửa đổi nhỏ nhặt không liên quan tới code

    1.6. style: những thay đổi không làm thay đổi ý nghĩa của code như thay đổi css/ui chẳng hạn

    1.7. perf: code cải tiến về mặt hiệu năng xử lý

    1.8. vendor: cập nhật version cho các dependencies, packages

#### Tóm tắt các quy tắc:

Các quy tắc cũng khá đơn giản và dễ nhớ. Khi viết commit theo cấu trúc bên trên là chúng ta đã tuân thủ gần hết bộ quy tắc này rồi, điểm qua bao gồm:

Commit message phải có prefix là một type (dạng danh từ) như feat, fix.. Theo ngay sau là scoped (nếu có) và một dấu hai chấm và khoảng trắng như chúng ta vừa đề cập ở trên. VD: feat:, fix:.
feat type: này thì bắt buộc phải sử dụng khi thêm một feature
fix type: này bắt buộc phải sử dụng khi fix bug
Nếu có scope, scope phải là một danh từ mô tả về vùng code thay đổi và phải đặt ngay sau type. VD: feat(authentication).
Một description: phải là mô tả ngắn về các thay đổi trong commit và phải ở sau một khoảng trắng sau type/scope:
Một commit dài thì có thể có phần body ngay sau description, cung cấp ngữ cảnh về các thay đổi. Phải có một dòng trắng giữa description và body.
Phần footer có thể được đặt ngay sau body, phải có một dòng trắng giữa body và footer. Footer phải chứ các thông tin mở rộng về commit như các pull request liên quan, các người review, breaking changes. Mỗi thông tin trên một dòng.
Các type khác feat và fix có thể được sử dụng trong commit message.
Nhìn chung mấy cái bên trên này chúng ta không phải nhớ nhiều vì khi viết commit theo cấu trúc ở phần một là nó đã đúng hết luôn rồi. 😄 Ngoài ra còn một vài quy tắc khác như:

Commit breaking changes thì phải chỉ rõ ngay khi bắt đầu body hoặc footer với từ khóa BREAKING CHANGE viết hoa. Theo sau là dấu hai chấm và một khoảng trắng.
Example:

feat(oauth): add scopes for oauth apps

BREAKING CHANGE: environment variables now take precedence over config files.


Một description nữa phải được cung cấp ngay sau BREAKING CHANGE, mô tả những thay đổi của API.
Example:

BREAKING CHANGE: environment variables now take precedence over config files.

:beer: **Push Code Flow for Member**

Step 1: commit your code
- git add .
- git commit -m "\<Message>"
- git push

Step 2: merge code branch dev
- git pull origin dev

Step 3: if pull dev have conflict, lets resolve them and commit
- git add .
- git commit -m "fix(Conflict)"

Step 4: Push again code to your branch
- git push