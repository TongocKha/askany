// import 'dart:io';
// import 'dart:typed_data';
// import 'package:askany/src/data/remote_data_source/image_repository.dart';
// import 'package:askany/src/helpers/path_helper.dart';
// import 'package:askany/src/routes/app_pages.dart';
// import 'package:intl/intl.dart';

// class DownloaderHelper {
//   void downloadImage(String urlToImage) async {
//     Uint8List? imageBuffer = await ImageRepository().getByteArrayFromUrl(
//       urlToImage,
//     );

//     if (imageBuffer != null) {
//       String time = DateFormat('yyyyMMdd-HHmmss').format(DateTime.now());
//       Directory? downloadsDirectory = await PathHelper.downloadsDir;
//       final pathOfTheFileToWrite = (downloadsDirectory?.path ?? '') + "/$time.jpg";
//       File file = File(pathOfTheFileToWrite);
//       await file.writeAsBytes(imageBuffer);
//     }

//     AppNavigator.pop();
//   }
// }
