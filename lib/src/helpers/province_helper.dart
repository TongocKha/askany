import 'package:askany/src/constants/provinces.dart';

class ProvinceHelper {
  String? getProvinceByCode(int code) {
    return Province().getProvinceByCode(code);
  }

  int? getCodeByProvince(String provice) {
    return Province().getCodeOfProvince(provice);
  }

  static String convertStringToSlug(str) {
    final _dupeSpaceRegExp = RegExp(r'\s{2,}');
    final _punctuationRegExp = RegExp(r'[^\w\s-]');
    str = str.trim(); // trim
    str = str.toLowerCase();
    String from =
        'àáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđ·/_,:;';
    String to =
        'aaaaaaaaaaaaaaaaaeeeeeeeeeeeiiiiiooooooooooooooooouuuuuuuuuuuyyyyyd------';
    // remove accents, swap ñ for n, etc
    from.split('').forEach((char) {
      int index = from.indexOf(char);
      str = str.replaceAll(char, to.split('')[index]);
    });
    str = str
        .replaceAll(_dupeSpaceRegExp, ' ')
        // Remove punctuation.
        .replaceAll(_punctuationRegExp, '')
        // // Replace space with the delimiter.
        .replaceAll(' ', '-');

    return str;
  }

  static filterList(String text) {
    List filteredList = [];
    provinceList.forEach((element) {
      int index = provinceList.indexOf(element);
      if (convertStringToSlug(text).split('-').every((element) {
        return convertStringToSlug(provinceList[index]).contains(element);
      })) filteredList.add(element);
    });
    return filteredList;
  }
}

final provinceList = Province().renderProvinceArray();
