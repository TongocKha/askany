import 'package:askany/src/helpers/emoji/emoji.dart';
import 'package:flutter/services.dart';

class MessageFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    String newText = replaceWithEmojis(newValue.text);
    return TextEditingValue(
      text: newText,
      selection: TextSelection.fromPosition(
        TextPosition(offset: newText.length),
      ),
    );
  }
}
