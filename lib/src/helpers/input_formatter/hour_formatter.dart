import 'package:askany/src/helpers/date_time_helper.dart';
import 'package:flutter/services.dart';

class HourFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    // Format: 00:00 - 00:00
    // Cursor: 0123456789
    int oldPositionCursor = oldValue.selection.base.offset;
    int currentPositionCursor = newValue.selection.base.offset;
    String newText = oldValue.text;
    // Insert
    if (currentPositionCursor > oldPositionCursor) {
      newText = newText.replaceRange(
        0,
        currentPositionCursor,
        newValue.text.substring(
          0,
          currentPositionCursor,
        ),
      );

      if ([2, 10].contains(currentPositionCursor)) {
        currentPositionCursor++;
      } else if (currentPositionCursor == 5) {
        currentPositionCursor = 8;
      }
    } else {
      newText = newText.replaceRange(
          currentPositionCursor, currentPositionCursor + 1, '0');
      if ([3, 11].contains(currentPositionCursor)) {
        currentPositionCursor--;
      } else if (currentPositionCursor == 8) {
        currentPositionCursor = 5;
      }
    }

    // Reformatter
    List<String> values = newText.split(' - ');
    List<String> result = [];
    values.forEach((value) {
      List<String> time = value.split(':');
      int hour = int.parse(time[0]);
      int minute = int.parse(time[1]);

      if (hour > 23) {
        hour = 23;
      }

      if (minute > 59) {
        minute = 59;
      }

      result.add('${addZeroPrefix(hour)}:${addZeroPrefix(minute)}');
    });

    newText = result.join(' - ');

    return TextEditingValue(
      text: newText,
      selection: TextSelection.fromPosition(
        TextPosition(offset: currentPositionCursor),
      ),
    );
  }
}
