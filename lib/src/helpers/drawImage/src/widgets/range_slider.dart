import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

///
class RangedSlider extends StatelessWidget {
  ///Range Slider widget for strokeWidth
  const RangedSlider({
    Key? key,
    required this.value,
    required this.onChanged,
    required this.isCheck,
  }) : super(key: key);

  ///Default value of strokewidth.
  final double value;
  final bool isCheck;

  /// Callback for value change.
  final ValueChanged<double> onChanged;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onChanged(value);
      },
      child: Container(
        height: 42.sp,
        width: 42.sp,
        child: Center(
          child: Container(
            height: value,
            width: value,
            decoration: BoxDecoration(
              color: !isCheck ? colorWhite : Colors.amber,
              borderRadius: BorderRadius.circular(32.sp),
            ),
          ),
        ),
      ),
    );
  }
}

List<double> size = [5, 11, 16, 21, 26, 31, 40];
List<bool> isCheck = [true, false, false, false, false, false, false];

clearIsCheck(int index) {
  isCheck.asMap().forEach((i, item) {
    if (i != index) {
      isCheck[i] = false;
    } else {
      isCheck[i] = true;
    }
  });
}
