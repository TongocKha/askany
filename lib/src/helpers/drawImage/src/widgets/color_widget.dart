import 'package:askany/src/configs/themes/app_colors.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ColorItem extends StatelessWidget {
  const ColorItem({Key? key, this.onTap, this.isSelected, this.color})
      : super(key: key);
  final VoidCallback? onTap;
  final bool? isSelected;
  final Color? color;

  @override
  Widget build(BuildContext context) {
    return TouchableOpacity(
      onTap: onTap,
      child: Padding(
        padding: EdgeInsets.all(8.0.sp),
        child: Container(
          padding: EdgeInsets.all(2.sp),
          decoration: BoxDecoration(
            color: isSelected! ? Colors.white70 : Colors.transparent,
            shape: BoxShape.circle,
            border: Border.all(color: isSelected! ? Colors.amber : colorBlack),
          ),
          child: CircleAvatar(
              radius: isSelected! ? 16.sp : 12.sp, backgroundColor: color),
        ),
      ),
    );
  }
}

const List<Color> editorColors = [
  Colors.white,
  Colors.red,
  Colors.blueAccent,
  Colors.greenAccent,
  Colors.green,
  Colors.pink,
  Colors.yellow,
  Colors.grey,
  Colors.teal,
  Colors.cyan,
  Colors.blue,
  Colors.orange,
  Colors.black,
  Colors.brown,
];
