import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';

import '../../image_painter.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class SelectionItems extends StatelessWidget {
  final bool? isSelected;
  final ModeData? data;
  final VoidCallback? onTap;

  const SelectionItems(
      {Key? key, this.isSelected = false, this.data, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TouchableOpacity(
      onTap: onTap,
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 2.0.sp),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(7.0.sp),
        ),
        child: Row(
          children: [
            IconTheme(
              data: IconThemeData(opacity: 1.0.sp),
              child: Icon(data!.icon,
                  size: 32.sp, color: isSelected! ? Colors.amber : colorWhite),
            ),
          ],
        ),
      ),
    );
  }
}

List<ModeData> paintModes(TextDelegate textDelegate) => [
      const ModeData(
        icon: Icons.zoom_out_map,
        mode: PaintMode.none,
      ),
      const ModeData(
        icon: Icons.edit,
        mode: PaintMode.freeStyle,
      ),
    ];

@immutable
class ModeData {
  final IconData? icon;
  final PaintMode? mode;
  const ModeData({
    this.icon,
    this.mode,
  });
}
