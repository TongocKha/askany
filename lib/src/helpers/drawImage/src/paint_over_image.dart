import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:askany/src/helpers/drawImage/src/ported_interactive_viewer.dart';
import 'package:askany/src/helpers/drawImage/src/widgets/color_widget.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/account_style.dart';
import 'package:askany/src/ui/style/calendar_style.dart';
import 'package:askany/src/ui/style/chat_style.dart';
import 'package:askany/src/ui/style/dialog_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';

import 'image_painter.dart';
import 'delegates/text_delegate.dart';
import 'widgets/mode_widget.dart';
import 'widgets/range_slider.dart';

export 'image_painter.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

///[ImagePainter] widget.
@immutable
class ImagePainter extends StatefulWidget {
  const ImagePainter._(
      {Key? key,
      this.handlePress,
      this.assetPath,
      this.networkUrl,
      this.byteArray,
      this.file,
      this.height,
      this.width,
      this.placeHolder,
      this.isScalable,
      this.brushIcon,
      this.clearAllIcon,
      this.colorIcon,
      this.undoIcon,
      this.isSignature = false,
      this.controlsAtTop = true,
      this.signatureBackgroundColor,
      this.colors,
      this.initialPaintMode,
      this.initialStrokeWidth,
      this.initialColor,
      this.onColorChanged,
      this.onStrokeWidthChanged,
      this.onPaintModeChanged,
      this.textDelegate})
      : super(key: key);

  ///Constructor for loading image from network url.
  factory ImagePainter.network(
    String url, {
    required Key key,
    double? height,
    double? width,
    Widget? placeholderWidget,
    bool? scalable,
    List<Color>? colors,
    Widget? brushIcon,
    Widget? undoIcon,
    Widget? clearAllIcon,
    Widget? colorIcon,
    PaintMode? initialPaintMode,
    double? initialStrokeWidth,
    Color? initialColor,
    ValueChanged<PaintMode>? onPaintModeChanged,
    ValueChanged<Color>? onColorChanged,
    ValueChanged<double>? onStrokeWidthChanged,
    TextDelegate? textDelegate,
  }) {
    return ImagePainter._(
      key: key,
      networkUrl: url,
      height: height,
      width: width,
      placeHolder: placeholderWidget,
      isScalable: scalable,
      colors: colors,
      brushIcon: brushIcon,
      undoIcon: undoIcon,
      colorIcon: colorIcon,
      clearAllIcon: clearAllIcon,
      initialPaintMode: initialPaintMode,
      initialColor: initialColor,
      initialStrokeWidth: initialStrokeWidth,
      onPaintModeChanged: onPaintModeChanged,
      onColorChanged: onColorChanged,
      onStrokeWidthChanged: onStrokeWidthChanged,
      textDelegate: textDelegate,
    );
  }

  ///Constructor for loading image from assetPath.
  factory ImagePainter.asset(
    String path, {
    required Key key,
    double? height,
    double? width,
    bool? scalable,
    Widget? placeholderWidget,
    List<Color>? colors,
    Widget? brushIcon,
    Widget? undoIcon,
    Widget? clearAllIcon,
    Widget? colorIcon,
    PaintMode? initialPaintMode,
    double? initialStrokeWidth,
    Color? initialColor,
    ValueChanged<PaintMode>? onPaintModeChanged,
    ValueChanged<Color>? onColorChanged,
    ValueChanged<double>? onStrokeWidthChanged,
    TextDelegate? textDelegate,
  }) {
    return ImagePainter._(
      key: key,
      assetPath: path,
      height: height,
      width: width,
      isScalable: scalable ?? false,
      placeHolder: placeholderWidget,
      colors: colors,
      brushIcon: brushIcon,
      undoIcon: undoIcon,
      colorIcon: colorIcon,
      clearAllIcon: clearAllIcon,
      initialPaintMode: initialPaintMode,
      initialColor: initialColor,
      initialStrokeWidth: initialStrokeWidth,
      onPaintModeChanged: onPaintModeChanged,
      onColorChanged: onColorChanged,
      onStrokeWidthChanged: onStrokeWidthChanged,
      textDelegate: textDelegate,
    );
  }

  ///Constructor for loading image from [File].
  factory ImagePainter.file(
    File file, {
    required Key key,
    Function? handlePress,
    double? height,
    double? width,
    bool? scalable,
    Widget? placeholderWidget,
    List<Color>? colors,
    Widget? brushIcon,
    Widget? undoIcon,
    Widget? clearAllIcon,
    Widget? colorIcon,
    PaintMode? initialPaintMode,
    double? initialStrokeWidth,
    Color? initialColor,
    ValueChanged<PaintMode>? onPaintModeChanged,
    ValueChanged<Color>? onColorChanged,
    ValueChanged<double>? onStrokeWidthChanged,
    TextDelegate? textDelegate,
  }) {
    return ImagePainter._(
      handlePress: handlePress,
      key: key,
      file: file,
      height: height,
      width: width,
      placeHolder: placeholderWidget,
      colors: colors,
      isScalable: scalable ?? false,
      brushIcon: brushIcon,
      undoIcon: undoIcon,
      colorIcon: colorIcon,
      clearAllIcon: clearAllIcon,
      initialPaintMode: initialPaintMode,
      initialColor: initialColor,
      initialStrokeWidth: initialStrokeWidth,
      onPaintModeChanged: onPaintModeChanged,
      onColorChanged: onColorChanged,
      onStrokeWidthChanged: onStrokeWidthChanged,
      textDelegate: textDelegate,
    );
  }

  ///Constructor for loading image from memory.
  factory ImagePainter.memory(
    Uint8List byteArray, {
    required Key key,
    double? height,
    double? width,
    bool? scalable,
    Widget? placeholderWidget,
    List<Color>? colors,
    Widget? brushIcon,
    Widget? undoIcon,
    Widget? clearAllIcon,
    Widget? colorIcon,
    PaintMode? initialPaintMode,
    double? initialStrokeWidth,
    Color? initialColor,
    ValueChanged<PaintMode>? onPaintModeChanged,
    ValueChanged<Color>? onColorChanged,
    ValueChanged<double>? onStrokeWidthChanged,
    TextDelegate? textDelegate,
  }) {
    return ImagePainter._(
      key: key,
      byteArray: byteArray,
      height: height,
      width: width,
      placeHolder: placeholderWidget,
      isScalable: scalable ?? false,
      colors: colors,
      brushIcon: brushIcon,
      undoIcon: undoIcon,
      colorIcon: colorIcon,
      clearAllIcon: clearAllIcon,
      initialPaintMode: initialPaintMode,
      initialColor: initialColor,
      initialStrokeWidth: initialStrokeWidth,
      onPaintModeChanged: onPaintModeChanged,
      onColorChanged: onColorChanged,
      onStrokeWidthChanged: onStrokeWidthChanged,
      textDelegate: textDelegate,
    );
  }

  ///Constructor for signature painting.
  factory ImagePainter.signature({
    required Key key,
    Color? signatureBgColor,
    double? height,
    double? width,
    List<Color>? colors,
    Widget? brushIcon,
    Widget? undoIcon,
    Widget? clearAllIcon,
    Widget? colorIcon,
    ValueChanged<PaintMode>? onPaintModeChanged,
    ValueChanged<Color>? onColorChanged,
    ValueChanged<double>? onStrokeWidthChanged,
    TextDelegate? textDelegate,
  }) {
    return ImagePainter._(
      key: key,
      height: height,
      width: width,
      isSignature: true,
      isScalable: false,
      colors: colors,
      signatureBackgroundColor: signatureBgColor ?? Colors.white,
      brushIcon: brushIcon,
      undoIcon: undoIcon,
      colorIcon: colorIcon,
      clearAllIcon: clearAllIcon,
      onPaintModeChanged: onPaintModeChanged,
      onColorChanged: onColorChanged,
      onStrokeWidthChanged: onStrokeWidthChanged,
      textDelegate: textDelegate,
    );
  }

  ///Only accessible through [ImagePainter.network] constructor.
  final String? networkUrl;

  final Function? handlePress;

  ///Only accessible through [ImagePainter.memory] constructor.
  final Uint8List? byteArray;

  ///Only accessible through [ImagePainter.file] constructor.
  final File? file;

  ///Only accessible through [ImagePainter.asset] constructor.
  final String? assetPath;

  ///Height of the Widget. Image is subjected to fit within the given height.
  final double? height;

  ///Width of the widget. Image is subjected to fit within the given width.
  final double? width;

  ///Widget to be shown during the conversion of provided image to [ui.Image].
  final Widget? placeHolder;

  ///Defines whether the widget should be scaled or not. Defaults to [false].
  final bool? isScalable;

  ///Flag to determine signature or image;
  final bool isSignature;

  ///Signature mode background color
  final Color? signatureBackgroundColor;

  ///List of colors for color selection
  ///If not provided, default colors are used.
  final List<Color>? colors;

  ///Icon Widget of strokeWidth.
  final Widget? brushIcon;

  ///Widget of Color Icon in control bar.
  final Widget? colorIcon;

  ///Widget for Undo last action on control bar.
  final Widget? undoIcon;

  ///Widget for clearing all actions on control bar.
  final Widget? clearAllIcon;

  ///Define where the controls is located.
  ///`true` represents top.
  final bool controlsAtTop;

  ///Initial PaintMode.
  final PaintMode? initialPaintMode;

  //the initial stroke width
  final double? initialStrokeWidth;

  //the initial color
  final Color? initialColor;

  final ValueChanged<Color>? onColorChanged;

  final ValueChanged<double>? onStrokeWidthChanged;

  final ValueChanged<PaintMode>? onPaintModeChanged;

  //the text delegate
  final TextDelegate? textDelegate;

  @override
  ImagePainterState createState() => ImagePainterState();
}

///
class ImagePainterState extends State<ImagePainter> {
  final _repaintKey = GlobalKey();
  ui.Image? _image;
  bool _inDrag = false;
  final _paintHistory = <PaintInfo>[];
  final _points = <Offset?>[];
  late final ValueNotifier<Controller> _controller;
  late final ValueNotifier<bool> _isLoaded;
  late final TextEditingController _textController;
  Offset? _start, _end;
  int _strokeMultiplier = 1;
  late TextDelegate textDelegate;
  @override
  void initState() {
    super.initState();
    _isLoaded = ValueNotifier<bool>(false);
    _resolveAndConvertImage();
    if (widget.isSignature) {
      _controller = ValueNotifier(
          const Controller(mode: PaintMode.freeStyle, color: Colors.black));
    } else {
      _controller = ValueNotifier(
        const Controller().copyWith(
          mode: widget.initialPaintMode,
          strokeWidth: widget.initialStrokeWidth,
          color: widget.initialColor,
        ),
      );
    }
    _textController = TextEditingController();
    textDelegate = widget.textDelegate ?? TextDelegate();
  }

  @override
  void dispose() {
    _controller.dispose();
    _isLoaded.dispose();
    _textController.dispose();
    super.dispose();
  }

  Paint get _painter => Paint()
    ..color = _controller.value.color
    ..strokeWidth = _controller.value.strokeWidth * _strokeMultiplier
    ..style = _controller.value.mode == PaintMode.dashLine
        ? PaintingStyle.stroke
        : _controller.value.paintStyle;

  ///Converts the incoming image type from constructor to [ui.Image]
  Future<void> _resolveAndConvertImage() async {
    if (widget.networkUrl != null) {
      _image = await _loadNetworkImage(widget.networkUrl!);
      if (_image == null) {
        throw ("${widget.networkUrl} couldn't be resolved.");
      } else {
        _setStrokeMultiplier();
      }
    } else if (widget.assetPath != null) {
      final img = await rootBundle.load(widget.assetPath!);
      _image = await _convertImage(Uint8List.view(img.buffer));
      if (_image == null) {
        throw ("${widget.assetPath} couldn't be resolved.");
      } else {
        _setStrokeMultiplier();
      }
    } else if (widget.file != null) {
      final img = await widget.file!.readAsBytes();
      _image = await _convertImage(img);
      if (_image == null) {
        throw ("Image couldn't be resolved from provided file.");
      } else {
        _setStrokeMultiplier();
      }
    } else if (widget.byteArray != null) {
      _image = await _convertImage(widget.byteArray!);
      if (_image == null) {
        throw ("Image couldn't be resolved from provided byteArray.");
      } else {
        _setStrokeMultiplier();
      }
    } else {
      _isLoaded.value = true;
    }
  }

  ///Dynamically sets stroke multiplier on the basis of widget size.
  ///Implemented to avoid thin stroke on high res images.
  _setStrokeMultiplier() {
    if ((_image!.height + _image!.width) > 1000) {
      _strokeMultiplier = (_image!.height + _image!.width) ~/ 1000;
    }
  }

  ///Completer function to convert asset or file image to [ui.Image] before drawing on custompainter.
  Future<ui.Image> _convertImage(Uint8List img) async {
    final completer = Completer<ui.Image>();
    ui.decodeImageFromList(img, (image) {
      _isLoaded.value = true;
      return completer.complete(image);
    });
    return completer.future;
  }

  ///Completer function to convert network image to [ui.Image] before drawing on custompainter.
  Future<ui.Image> _loadNetworkImage(String path) async {
    final completer = Completer<ImageInfo>();
    var img = NetworkImage(path);
    img.resolve(const ImageConfiguration()).addListener(
        ImageStreamListener((info, _) => completer.complete(info)));
    final imageInfo = await completer.future;
    _isLoaded.value = true;
    return imageInfo.image;
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<bool>(
      valueListenable: _isLoaded,
      builder: (_, loaded, __) {
        if (loaded) {
          return widget.isSignature
              ? _paintSignature()
              : _paintImage(widget.handlePress!);
        } else {
          return Container(
            height: widget.height ?? double.maxFinite,
            width: widget.width ?? double.maxFinite,
            child: Center(
              child: widget.placeHolder ??
                  const CircularProgressIndicator(
                    color: colorGreen2,
                  ),
            ),
          );
        }
      },
    );
  }

  ///paints image on given constrains for drawing if image is not null.
  Widget _paintImage(Function handlePress) {
    return Stack(
      children: [
        Column(
          children: [
            Expanded(
              child: FittedBox(
                alignment: FractionalOffset.center,
                fit: BoxFit.fitWidth,
                child: ClipRect(
                  child: ValueListenableBuilder<Controller>(
                    valueListenable: _controller,
                    builder: (_, controller, __) {
                      return ImagePainterTransformer(
                        maxScale: 2.4,
                        minScale: 1,
                        panEnabled: controller.mode == PaintMode.none,
                        scaleEnabled: widget.isScalable!,
                        onInteractionUpdate: (details) =>
                            _scaleUpdateGesture(details, controller),
                        onInteractionEnd: (details) =>
                            _scaleEndGesture(details, controller),
                        child: CustomPaint(
                          size: Size(_image!.width.toDouble(),
                              _image!.height.toDouble()),
                          willChange: true,
                          isComplex: true,
                          painter: DrawImage(
                            image: _image,
                            points: _points,
                            paintHistory: _paintHistory,
                            isDragging: _inDrag,
                            update: UpdatePoints(
                              start: _start,
                              end: _end,
                              painter: _painter,
                              mode: controller.mode,
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ),
            ),
            SizedBox(height: 5.sp),
            ValueListenableBuilder<Controller>(
              valueListenable: _controller,
              builder: (_, controller, __) {
                return _custom(
                  controller: controller,
                  visible: true,
                  handlePress: handlePress,
                );
              },
            ),
          ],
        ),
        Align(
          alignment: Alignment.topRight,
          child: _buildControls(),
        ),
      ],
    );
  }

  Widget _paintSignature() {
    return Stack(
      children: [
        RepaintBoundary(
          key: _repaintKey,
          child: ClipRect(
            child: Container(
              child: ValueListenableBuilder<Controller>(
                valueListenable: _controller,
                builder: (_, controller, __) {
                  return ImagePainterTransformer(
                    panEnabled: false,
                    scaleEnabled: false,
                    onInteractionStart: _scaleStartGesture,
                    onInteractionUpdate: (details) =>
                        _scaleUpdateGesture(details, controller),
                    onInteractionEnd: (details) =>
                        _scaleEndGesture(details, controller),
                    child: CustomPaint(
                      willChange: true,
                      isComplex: true,
                      painter: DrawImage(
                        isSignature: true,
                        backgroundColor: widget.signatureBackgroundColor,
                        points: _points,
                        paintHistory: _paintHistory,
                        isDragging: _inDrag,
                        update: UpdatePoints(
                          start: _start,
                          end: _end,
                          painter: _painter,
                          mode: controller.mode,
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        ),
      ],
    );
  }

  _scaleStartGesture(ScaleStartDetails onStart) {
    if (!widget.isSignature) {
      setState(() {
        _start = onStart.focalPoint;
        _points.add(_start);
      });
    }
  }

  ///Fires while user is interacting with the screen to record painting.
  void _scaleUpdateGesture(ScaleUpdateDetails onUpdate, Controller ctrl) {
    setState(
      () {
        _inDrag = true;
        _start ??= onUpdate.focalPoint;
        _end = onUpdate.focalPoint;
        if (ctrl.mode == PaintMode.freeStyle) _points.add(_end);
        if (ctrl.mode == PaintMode.text &&
            _paintHistory
                .where((element) => element.mode == PaintMode.text)
                .isNotEmpty) {
          _paintHistory
              .lastWhere((element) => element.mode == PaintMode.text)
              .offset = [_end];
        }
      },
    );
  }

  ///Fires when user stops interacting with the screen.
  void _scaleEndGesture(ScaleEndDetails onEnd, Controller controller) {
    setState(() {
      _inDrag = false;
      if (_start != null &&
          _end != null &&
          (controller.mode == PaintMode.freeStyle)) {
        _points.add(null);
        _addFreeStylePoints();
        _points.clear();
      } else if (_start != null &&
          _end != null &&
          controller.mode != PaintMode.text) {
        _addEndPoints();
      }
      _start = null;
      _end = null;
    });
  }

  void _addEndPoints() => _addPaintHistory(
        PaintInfo(
          offset: <Offset?>[_start, _end],
          painter: _painter,
          mode: _controller.value.mode,
        ),
      );

  void _addFreeStylePoints() => _addPaintHistory(
        PaintInfo(
          offset: <Offset?>[..._points],
          painter: _painter,
          mode: PaintMode.freeStyle,
        ),
      );

  ///Provides [ui.Image] of the recorded canvas to perform action.
  Future<ui.Image> _renderImage() async {
    final recorder = ui.PictureRecorder();
    final canvas = Canvas(recorder);
    final painter = DrawImage(image: _image, paintHistory: _paintHistory);
    final size = Size(_image!.width.toDouble(), _image!.height.toDouble());
    painter.paint(canvas, size);
    return recorder
        .endRecording()
        .toImage(size.width.floor(), size.height.floor());
  }

  ///Generates [Uint8List] of the [ui.Image] generated by the [renderImage()] method.
  ///Can be converted to image file by writing as bytes.
  Future<Uint8List?> exportImage() async {
    late ui.Image _convertedImage;
    if (widget.isSignature) {
      final _boundary = _repaintKey.currentContext!.findRenderObject()
          as RenderRepaintBoundary;
      _convertedImage = await _boundary.toImage(pixelRatio: 3);
    } else if (widget.byteArray != null && _paintHistory.isEmpty) {
      return widget.byteArray;
    } else {
      _convertedImage = await _renderImage();
    }
    final byteData =
        await _convertedImage.toByteData(format: ui.ImageByteFormat.png);
    return byteData?.buffer.asUint8List();
  }

  void _addPaintHistory(PaintInfo info) {
    if (info.mode != PaintMode.none) {
      _paintHistory.add(info);
    }
  }

  List<bool> listIcon = [true, false];
  void clearItem(int index) {
    listIcon[index] = true;
    listIcon.asMap().forEach((i, bool) {
      if (i != index) {
        listIcon[i] = false;
      }
    });
  }

  Widget _custom({
    required bool visible,
    required Controller controller,
    required Function handlePress,
  }) {
    return Visibility(
      visible: visible,
      child: Container(
        height: 20.h,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 16.sp),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  TouchableOpacity(
                    onTap: () => setState(_paintHistory.clear),
                    child: Container(
                      height: 18.sp,
                      width: 18.sp,
                      color: Colors.transparent,
                      margin: EdgeInsets.all(7.sp),
                      child: Image.asset(
                        iconRevertImage,
                        color: colorWhite,
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ),
                  Container(
                    child: Text(
                      'Paint',
                      style: TextStyle(color: Colors.white, fontSize: 13.sp),
                    ),
                  ),
                  TouchableOpacity(
                    onTap: handlePress,
                    child: Container(
                      height: 18.sp,
                      width: 18.sp,
                      color: Colors.transparent,
                      margin: EdgeInsets.all(7.sp),
                      child: Image.asset(
                        iconCheckInBox,
                        color: colorWhite,
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Column(
                children: [
                  OptionColor(
                    visible: listIcon[0],
                    controller: controller,
                    controllers: _controller,
                    colors: widget.colors,
                    onColorChanged: widget.onColorChanged,
                  ),
                  OptionRangeSlider(
                      visible: listIcon[1],
                      controller: controller,
                      controllers: _controller)
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  bool edit = false;

  Widget _buildControls() {
    return Container(
      height: 25.h,
      padding: EdgeInsets.only(top: 4.sp, left: 16.sp, right: 16.sp),
      color: Colors.transparent,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          ValueListenableBuilder<Controller>(
            valueListenable: _controller,
            builder: (_, _ctrl, __) {
              return TouchableOpacity(
                onTap: () {
                  setState(() {
                    edit = !edit;
                    if (edit) {
                      _controller.value = _ctrl.copyWith(
                          mode: paintModes(textDelegate)[0].mode);
                    } else {
                      _controller.value = _ctrl.copyWith(
                          mode: paintModes(textDelegate)[1].mode);
                    }
                  });
                },
                child: Container(
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(color: colorGray1),
                    color: edit ? colorBlack1.withOpacity(0.6) : colorGreen4,
                  ),
                  padding: EdgeInsets.all(6.sp),
                  child: Image.asset(
                    imgPaint,
                    height: 15.sp,
                    width: 15.sp,
                    color: edit ? colorWhite : colorBlack1,
                    fit: BoxFit.fitWidth,
                  ),
                ),
              );
            },
          ),
          ValueListenableBuilder<Controller>(
              valueListenable: _controller,
              builder: (_, controller, __) {
                return TouchableOpacity(
                  onTap: () {
                    setState(() {
                      clearItem(0);
                    });
                  },
                  child: widget.colorIcon ??
                      Container(
                        height: 30.sp,
                        width: 30.sp,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: colorGray1),
                          color: controller.color,
                        ),
                      ),
                );
              }),
          TouchableOpacity(
            onTap: () {
              setState(() {
                clearItem(1);
              });
            },
            child: Container(
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(color: colorGray1),
                color:
                    !listIcon[1] ? colorBlack1.withOpacity(0.3) : colorGreen4,
              ),
              padding: EdgeInsets.all(6.sp),
              child: Image.asset(
                imgPencil,
                height: 15.sp,
                width: 15.sp,
                color: !listIcon[1] ? colorWhite : colorBlack1,
                fit: BoxFit.fitWidth,
              ),
            ),
          ),
          TouchableOpacity(
              child: widget.undoIcon ??
                  Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(color: colorGray1),
                      color: colorBlack1.withOpacity(0.3),
                    ),
                    padding: EdgeInsets.all(6.sp),
                    child: Image.asset(
                      imgUndo,
                      color: colorWhite,
                      height: 15.sp,
                      width: 15.sp,
                    ),
                  ),
              onTap: () {
                if (_paintHistory.isNotEmpty) {
                  setState(_paintHistory.removeLast);
                }
              }),
        ],
      ),
    );
  }
}

///Gives access to manipulate the essential components like [strokeWidth], [Color] and [PaintMode].
@immutable
class Controller {
  ///Tracks [strokeWidth] of the [Paint] method.
  final double strokeWidth;

  ///Tracks [Color] of the [Paint] method.
  final Color color;

  ///Tracks [PaintingStyle] of the [Paint] method.
  final PaintingStyle paintStyle;

  ///Tracks [PaintMode] of the current [Paint] method.
  final PaintMode? mode;

  ///Any text.
  final String text;

  ///Constructor of the [Controller] class.
  const Controller(
      {this.strokeWidth = 4.0,
      this.color = Colors.red,
      this.mode = PaintMode.line,
      this.paintStyle = PaintingStyle.stroke,
      this.text = ""});

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is Controller &&
        o.strokeWidth == strokeWidth &&
        o.color == color &&
        o.paintStyle == paintStyle &&
        o.mode == mode &&
        o.text == text;
  }

  @override
  int get hashCode {
    return strokeWidth.hashCode ^
        color.hashCode ^
        paintStyle.hashCode ^
        mode.hashCode ^
        text.hashCode;
  }

  ///copyWith Method to access immutable controller.
  Controller copyWith(
      {double? strokeWidth,
      Color? color,
      PaintMode? mode,
      PaintingStyle? paintingStyle,
      String? text}) {
    return Controller(
        strokeWidth: strokeWidth ?? this.strokeWidth,
        color: color ?? this.color,
        mode: mode ?? this.mode,
        paintStyle: paintingStyle ?? paintStyle,
        text: text ?? this.text);
  }
}

// ignore: must_be_immutable
class OptionColor extends StatefulWidget {
  final bool visible;
  List<Color>? colors;
  Controller controller;
  late final ValueNotifier<Controller> controllers;
  final ValueChanged<Color>? onColorChanged;
  OptionColor({
    Key? key,
    required this.visible,
    this.colors,
    required this.controller,
    required this.controllers,
    this.onColorChanged,
  }) : super(key: key);

  @override
  State<OptionColor> createState() => _OptionColorState();
}

class _OptionColorState extends State<OptionColor> {
  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: widget.visible,
      child: Center(
        child: Container(
          height: 80.sp,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: (widget.colors?.length ?? editorColors.length),
            padding: EdgeInsets.symmetric(horizontal: 16.sp),
            shrinkWrap: true,
            itemBuilder: (context, index) {
              return ColorItem(
                isSelected: (widget.colors?[index] ?? editorColors[index]) ==
                    widget.controller.color,
                color: (widget.colors?[index] ?? editorColors[index]),
                onTap: () {
                  widget.controllers.value = widget.controller.copyWith(
                      color: (widget.colors?[index] ?? editorColors[index]));
                  if (widget.onColorChanged != null) {
                    widget.onColorChanged!(
                        (widget.colors?[index] ?? editorColors[index]));
                  }
                },
              );
            },
          ),
        ),
      ),
    );
  }
}

// ignore: must_be_immutable
class OptionRangeSlider extends StatefulWidget {
  final bool visible;
  List<Color>? colors;
  Controller controller;
  late final ValueNotifier<Controller> controllers;
  final ValueChanged<double>? onStrokeWidthChanged;
  OptionRangeSlider({
    Key? key,
    required this.visible,
    this.colors,
    required this.controller,
    required this.controllers,
    this.onStrokeWidthChanged,
  }) : super(key: key);

  @override
  State<OptionRangeSlider> createState() => _OptionRangeSliderState();
}

class _OptionRangeSliderState extends State<OptionRangeSlider> {
  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: widget.visible,
      child: Center(
        child: Container(
          height: 80.sp,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: size.length,
            padding: EdgeInsets.symmetric(horizontal: 16.sp),
            shrinkWrap: true,
            itemBuilder: (context, index) {
              return Row(
                children: [
                  RangedSlider(
                    value: size[index],
                    isCheck: isCheck[index],
                    // value: widget.controller.strokeWidth,
                    onChanged: (value) {
                      setState(() {
                        clearIsCheck(index);
                      });
                      widget.controllers.value =
                          widget.controller.copyWith(strokeWidth: value);
                      if (widget.onStrokeWidthChanged != null) {
                        widget.onStrokeWidthChanged!(value);
                      }
                    },
                  ),
                  SizedBox(width: 5.sp),
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}
