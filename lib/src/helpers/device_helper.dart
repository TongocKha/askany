import 'dart:io';

import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/models/device_model.dart';
import 'package:askany/src/services/firebase_messaging/handle_messaging.dart';
import 'package:askany/src/services/incomingcall_ios/incoming_call.dart';
import 'package:device_info/device_info.dart';
import 'package:just_audio/just_audio.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:flutter/services.dart';
import 'package:vibration/vibration.dart';

class DeviceHelper {
  static DeviceModel? deviceModel;
  Future<void> getDeviceInfo() async {
    deviceModel = await DeviceHelper().getDeviceDetails();
  }

  Future<DeviceModel> getDeviceDetails() async {
    late String deviceName;
    // late String deviceVersion;
    late String identifier;
    late String appVersion;
    final String? fcmToken = await getFirebaseMessagingToken();
    print(fcmToken);
    String? apnsToken = '';
    if (Platform.isIOS) {
      apnsToken = await getVoipServiceTokenDevice();
    }
    final PackageInfo packageInfo = await PackageInfo.fromPlatform();
    final DeviceInfoPlugin deviceInfoPlugin = new DeviceInfoPlugin();
    appVersion = packageInfo.version;
    try {
      if (Platform.isAndroid) {
        var build = await deviceInfoPlugin.androidInfo;
        deviceName = build.model;
        identifier = build.androidId; //UUID for Android
      } else if (Platform.isIOS) {
        var data = await deviceInfoPlugin.iosInfo;
        deviceName = data.utsname.machine;
        identifier = data.identifierForVendor; //UUID for iOS
      }
    } on PlatformException {
      print('Failed to get platform version');
    }

    return DeviceModel(
      appVersion: appVersion,
      deviceModel: deviceName,
      deviceUuid: identifier,
      fcmToken: fcmToken!,
      apnsToken: apnsToken ?? '',
      createdBy: UserLocal().getUser().id ?? '',
    );
  }

  Future<void> soundAndRing() async {
    final _player = AudioPlayer();
    try {
      if (_player.playing) {
        _player.stop();
      }
      await _player.setAudioSource(
        AudioSource.uri(
          Uri.parse("asset:///assets/audio/alarm.mp3"),
        ),
      );
      _player.setVolume(1);
      _player.play();
      _player.setLoopMode(LoopMode.off);
      Vibration.vibrate(
        pattern: [300, 300],
      );
    } catch (error) {
      print('audio error: ${error.toString()}');
    }
  }

  Future<void> typingSound(AudioPlayer player, bool isTyping) async {
    if (isTyping == false) {
      await player.dispose();
    } else {
      try {
        {
          await player.setAudioSource(
            AudioSource.uri(
              Uri.parse("asset:///assets/audio/typing_sound.mp3"),
            ),
          );
          player.setVolume(1);
          player.setLoopMode(LoopMode.all);
          player.play();
        }
      } catch (error) {
        print('audio error: ${error.toString()}');
      }
    }
  }
}
