import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

List<String> month = List.generate(12, (index) {
  return addZeroPrefix(index + 1);
});

List<String> year =
    List.generate(50, (index) => (DateTime.now().year - index).toString()).reversed.toList();

List<int> dayCountForMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
List<String> monthNames = [
  Strings.monthNames[0].i18n,
  Strings.monthNames[1].i18n,
  Strings.monthNames[2].i18n,
  Strings.monthNames[3].i18n,
  Strings.monthNames[4].i18n,
  Strings.monthNames[5].i18n,
  Strings.monthNames[6].i18n,
  Strings.monthNames[7].i18n,
  Strings.monthNames[8].i18n,
  Strings.monthNames[9].i18n,
  Strings.monthNames[10].i18n,
  Strings.monthNames[11].i18n,
];
List<String> dayNames = [
  Strings.dayNames[0].i18n,
  Strings.dayNames[1].i18n,
  Strings.dayNames[2].i18n,
  Strings.dayNames[3].i18n,
  Strings.dayNames[4].i18n,
  Strings.dayNames[5].i18n,
  Strings.dayNames[6].i18n,
];
List hours() {
  var hours = [];
  for (var i = 0; i <= 23; i++) {
    hours.add(addZeroPrefix(i));
  }
  return hours;
}

List minutes() {
  var minutes = [];
  for (var i = 0; i < 60; i++) {
    minutes.add(addZeroPrefix(i));
  }
  return minutes;
}

int relatedWeekday({required int month, required int year, required int day}) {
  var _monthToString = addZeroPrefix(month);
  var _dayToString = addZeroPrefix(day);
  return DateTime.parse('$year-$_monthToString-$_dayToString').weekday;
  // thứ hai là số 1
}

String addZeroPrefix(number) => '00'.substring(number.toString().length) + '$number';

int dayCountForFebruary(year) {
  return year % 4 == 0 ? 29 : 28;
}

List dayToWeekday({required int month, required int year}) {
  final dayToWeekday = [];
  var _dayCountForMonth = month == 2 ? dayCountForFebruary(year) : dayCountForMonth[month - 1];
  // vì hiển thị lịch từ chủ nhật, ta thêm 0 vào những ngày trong tuần trước ngày 1
  var fistDayRelatedWeekday = relatedWeekday(day: 1, month: month, year: year);
  if (fistDayRelatedWeekday != 7) {
    // nếu ngày đầu tháng là chủ nhật thì không thêm
    for (var i = 0; i < fistDayRelatedWeekday; i++) {
      dayToWeekday.add(0);
    }
  }

  for (var i = 1; i <= _dayCountForMonth; i++) {
    dayToWeekday.add(i);
  }
  return dayToWeekday;
}

List generateDays(dayCountForMonth) {
  var dayList = [];
  for (var i = 1; i <= dayCountForMonth; i++) {
    dayList.add(i);
  }
  return dayList;
}

List allowedAppointmentDates() {
  var thisMonth = DateTime.now().month;
  var thisMonthList = [];
  var nextMonthList = [];
  for (var i = 5; i <= 21; i++) {
    if (DateTime.now().add(Duration(days: i)).month == thisMonth) {
      thisMonthList.add(DateTime.now().add(Duration(days: i)).day);
    } else {
      nextMonthList.add(DateTime.now().add(Duration(days: i)).day);
    }
  }
  return [thisMonthList, nextMonthList];
}

List<List<DateTime>> getCalendarThisMonth({DateTime? dateTime}) {
  var inputDate = dateTime ?? DateTime.now();
  var thisMonth = inputDate.month;
  var thisYear = inputDate.year;
  int firstDayOfMonth = DateTime(inputDate.year, thisMonth).weekday;
  List<List<DateTime>> thisMonthList = [];
  int date = 1;
  while (date <= dayCountForMonth[thisMonth - 1]) {
    List<DateTime> week = [];
    for (int i = 1; i < 8; i++) {
      if (date == 1) {
        if (i < firstDayOfMonth) {
          week.add(
            DateTime(thisYear, thisMonth).subtract(Duration(days: firstDayOfMonth - i)),
          );
        } else {
          week.add(DateTime(thisYear, thisMonth, date));
          date++;
        }
      } else {
        if (date <= dayCountForMonth[thisMonth - 1]) {
          week.add(DateTime(thisYear, thisMonth, date));
        } else {
          week.add(
            week[week.length - 1].add(Duration(days: 1)),
          );
        }
        date++;
      }
    }
    thisMonthList.add(week);
  }

  return thisMonthList;
}

List<DateTime> getCalendarThisWeek({DateTime? dateTime}) {
  DateTime nowDate = dateTime ?? DateTime.now();
  int year = nowDate.year;
  int month = nowDate.month;
  int day = nowDate.day;
  List<DateTime> thisWeek = [];
  List thisMonth = getCalendarThisMonth(dateTime: nowDate);
  thisMonth.forEach((week) {
    if (week.contains(DateTime(year, month, day))) {
      thisWeek = week;
    }
  });

  return thisWeek;
}

String getDayName(DateTime date) {
  if (isEqualTwoDate(DateTime.now(), date)) {
    return 'Hôm nay';
  }

  return '${calendarTitle[date.weekday - 1]}\n${addZeroPrefix(date.day)}';
}

String getTimeString(DateTime date) {
  return DateFormat('HH:mm').format(date);
}

String getDatetimeString(DateTime time) {
  return DateFormat('dd/MM/yyyy HH:mm').format(time);
}

String getDateString(DateTime time) {
  return DateFormat('dd/MM/yyyy').format(time);
}

String getTimelineTitleCard(DateTime startTime, DateTime endTime) {
  return '${getTimeString(startTime)} - ${getTimeString(endTime)} | ${DateFormat('dd/MM/yyyy').format(startTime)}'
      '';
}

int getStatusTimeline(DateTime startTime, DateTime endTime) {
  if (DateTime.now().isAfter(startTime) && DateTime.now().isBefore(endTime)) {
    return 0;
  } else if (DateTime.now().isAfter(endTime)) {
    return 1;
  } else {
    return 2;
  }
}

bool isEqualTwoDate(DateTime date1, DateTime date2) {
  return date1.day == date2.day && date1.month == date2.month && date1.year == date2.year;
}

bool locatedInThisMonth(DateTime date, {DateTime? compareDate}) {
  DateTime compare = compareDate ?? DateTime.now();
  return date.month == compare.month;
}

bool isShowTime(DateTime currentTime, DateTime prevTime) {
  return currentTime.difference(prevTime).inMinutes >= SPACE_TIME_MESSAGE;
}

String getDurationCallHistory(DateTime startTime, DateTime endTime) {
  var duration = endTime.difference(startTime);
  var hour = duration.inHours;
  var minute = duration.inMinutes - (hour * 60);
  var second = duration.inSeconds - (hour * 3600) - (minute * 60);

  if (hour > 0) {
    return '${addZeroPrefix(hour)} giờ ${addZeroPrefix(minute)} phút';
  }

  if (minute > 0) {
    return '${addZeroPrefix(minute)} phút ${addZeroPrefix(second)} giây';
  }

  return ' ${addZeroPrefix(second)} giây';
}

String getDurationCalling({required int durationSeconds, bool isShowHours = true}) {
  Duration duration = Duration(seconds: durationSeconds);
  var hour = duration.inHours;
  var minute = duration.inMinutes - (hour * 60);
  var second = duration.inSeconds - (hour * 3600) - (minute * 60);

  if (!isShowHours && hour == 0) {
    return '${addZeroPrefix(minute)}:${addZeroPrefix(second)}';
  }

  return '${addZeroPrefix(hour)}:${addZeroPrefix(minute)}:${addZeroPrefix(second)}';
}

String formatTimeToUTC(DateTime date) {
  return date.toUtc().toString().split(' ').join('T');
}

DateTime? fromStringToDate(String stringDate, {String splitter = "/"}) {
  try {
    final _listTime = stringDate.split(splitter);
    final _newStringDate = _listTime[2] + '-' + _listTime[1] + '-' + _listTime[0];
    return DateTime.tryParse(_newStringDate);
  } catch (error) {
    return null;
  }
}

final List<TextInputFormatter> dateTimeInputFormat = [
  _DateFormatter(),
  FilteringTextInputFormatter.allow(RegExp("[0-9/]")),
  LengthLimitingTextInputFormatter(10),
];

class _DateFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
    TextEditingValue oldValue,
    TextEditingValue newValue,
  ) {
    // int _selectionIndex;
    String _oldText = oldValue.text;
    String _newText = newValue.text;
    int _oldSelection = oldValue.selection.start;
    int _newSelection = newValue.selection.start;
    final int _selectionIndexFromTheRight = newValue.text.length - newValue.selection.end;

    if (_newSelection == 2) {
      final _day = int.tryParse(_newText.substring(0, 2));
      if (_day != null) {
        _newText = _getValidDay(_day, null, null) + _newText.substring(2);
      }
    }

    if (_newSelection == 5) {
      final _month = int.tryParse(_newText.substring(3, 5));
      final _day = int.tryParse(_newText.substring(0, 2));
      if (_month != null && _day != null) {
        _newText =
            _getValidDay(_day, _month, null) + "/" + _getValidMonth(_month) + _newText.substring(5);
      }
    }

    if (_newText.length == 10) {
      final _day = int.tryParse(_newText.substring(0, 2));
      final _month = int.tryParse(_newText.substring(3, 5));
      final _year = int.tryParse(_newText.substring(6, 10));
      if (_day != null && _month != null && _year != null) {
        _newText = _getValidDay(_day, _month, _year) + "/" + _getValidMonth(_month) + "/$_year";
      }
      final _newDateTime = fromStringToDate(_newText);
      if (_newDateTime != null) {
        if (_newDateTime.isAfter(DateTime.now())) {
          _newText = DateFormat("dd/MM/yyyy").format(_newDateTime);
        } else {
          _newText = DateFormat("dd/MM/yyyy").format(DateTime.now());
        }
      } else {
        _newText = DateFormat("dd/MM/yyyy").format(DateTime.now());
      }
      return TextEditingValue(
        text: _newText,
        selection: newValue.selection,
      );
    }

    if ((_newSelection >= 1 && _newSelection <= 2) ||
        (_newSelection >= 4 && _newSelection <= 5) ||
        (_newSelection >= 7 && _newSelection <= 10)) {
      if (_newText[_newSelection - 1] == "/") {
        return TextEditingValue(
          text: _oldText,
          selection: oldValue.selection,
        );
      }
    }

    if (_newText.length > _oldText.length) {
      if (_newSelection == 2 || _newSelection == 5) {
        _newText = _newText.substring(0, _newSelection) + "/" + _newText.substring(_newSelection);
      } else if (_oldSelection == 2 || _oldSelection == 5) {
        _newText = _newText.substring(0, _oldSelection) + "/" + _newText.substring(_oldSelection);
      }
    }

    if ("/".allMatches(_newText).length > 2) {
      _newText = _newText.replaceAll("//", "/");
    }

    return TextEditingValue(
      text: _newText,
      selection: TextSelection.collapsed(
        offset: _newText.length - _selectionIndexFromTheRight,
      ),
    );
  }

  String _getValidDay(int day, int? month, int? year) {
    if (day < 10 && day >= 1) {
      return "0$day";
    }
    if (day >= 10 && day < 29) {
      return "$day";
    }
    if (day >= 29) {
      if (month != null) {
        final _maxDay = _maxDayInMonth(int.parse(_getValidMonth(month)), year);
        if (day <= _maxDay) {
          return day.toString();
        } else {
          return _maxDay.toString();
        }
      } else {
        return day > 31 ? "31" : day.toString();
      }
    }
    return "01";
  }

  String _getValidMonth(int month) {
    if (month < 10 && month > 0) {
      return "0$month";
    }
    if (month >= 10 && month <= 12) {
      return "$month";
    }
    if (month > 12) {
      return "12";
    } else {
      return "01";
    }
  }

  int _maxDayInMonth(int month, int? year) {
    switch (month) {
      case 1:
      case 3:
      case 5:
      case 7:
      case 8:
      case 10:
      case 12:
        return 31;
      case 4:
      case 6:
      case 9:
      case 11:
        return 30;
      case 2:
        return _checkLeapYear(year) ? 29 : 28;
      default:
        return 31;
    }
  }

  bool _checkLeapYear(int? year) {
    if (year != null) {
      if (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0)) {
        return true;
      } else {
        return false;
      }
    }
    return true;
  }
}
