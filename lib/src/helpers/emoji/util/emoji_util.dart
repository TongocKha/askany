import '../data/emoji.dart';
import '../data/emoticons.dart';

/// Emojifies and returns the given text.
///
/// We first replace known emoticons with corresponding emoji codes,
/// then replace all known emoji codes with corresponding glyphs.
String replaceWithEmojis(String text) =>
    replaceEmojiCodesWithGlyphs(replaceEmoticonsWithEmojiCodes(text));

/// Replaces known emoticons with corresponding emoji codes.
String replaceEmoticonsWithEmojiCodes(String text) {
  var emoticonPattern = emoticonToEmojiCode.keys.map(escapeRegex).join('|');
  return text.replaceAllMapped(
      new RegExp('\\B($emoticonPattern)'), (Match m) => _toEmojiCode(m.group(1)!));
}

/// Replaces known emoji codes with corresponding glyphs.
String replaceEmojiCodesWithGlyphs(String text) {
  List<String> words = text.split(' ');
  words.asMap().forEach((index, w) {
    if (listEmojiAndCode.contains(w)) {
      words[index] = _toGlyph(w);
    }
  });
  return words.join(' ');
}

_toEmojiCode(String emoticon) => ':${emoticonToEmojiCode[emoticon]}:';

_toGlyph(String code) {
  int indexOfEmoji = listEmojiAndCode.indexWhere((element) => element == code);
  if (indexOfEmoji != -1) {
    return emojiCode[indexOfEmoji]['unicode'];
  }

  return code;
}

List<String> get listEmojiAndCode => emojiCode.map((e) => e['emoji']!).toList();

/// From the PatternCharacter rule here:
/// http://ecma-international.org/ecma-262/5.1/#sec-15.10
final _specialChars = new RegExp(r'([\\\^\$\.\|\+\[\]\(\)\{\}\/\*])');

/// Escapes special regex characters in [str] so that it can be used as a
/// literal match inside of a [RegExp].
///
/// The special characters are: \ ^ $ . | + [ ] ( ) { }
/// as defined here: http://ecma-international.org/ecma-262/5.1/#sec-15.10
String escapeRegex(String str) =>
    str.splitMapJoin(_specialChars, onMatch: (Match m) => '\\${m.group(0)}', onNonMatch: (s) => s);
