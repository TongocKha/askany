import 'package:askany/src/constants/constants.dart';
import 'package:url_launcher/url_launcher.dart';

class UrlLauncherHelper {
  static Future<void> launchUrl(String urlString) async {
    if (await canLaunch(urlString)) {
      await launch(
        urlString,
        forceSafariVC: false,
      );
    } else {
      print("Can\'t Launch Url");
    }
  }

  bool isWebsite(String website) {
    return website.trim().startsWith(HTTP_URL) || website.trim().startsWith(HTTPS_URL);
  }
}
