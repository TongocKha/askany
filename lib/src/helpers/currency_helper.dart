import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

class CurrencyHelper {
  static List<TextInputFormatter> vndInputFormat = [
    FilteringTextInputFormatter.digitsOnly,
    TextInputFormatter.withFunction((oldValue, newValue) {
      if (newValue.text.isEmpty) {
        return newValue.copyWith(text: '');
      } else if (newValue.text.compareTo(oldValue.text) != 0) {
        final int selectionIndexFromTheRight =
            newValue.text.length - newValue.selection.end;
        final f = NumberFormat("#,###");
        final number =
            int.parse(newValue.text.replaceAll(f.symbols.GROUP_SEP, ''));
        final newString = f.format(number);
        return TextEditingValue(
          text: newString,
          selection: TextSelection.collapsed(
            offset: newString.length - selectionIndexFromTheRight,
          ),
        );
      } else {
        return newValue;
      }
    }),
  ];

  static List<TextInputFormatter> usdInputFormat = [
    FilteringTextInputFormatter.allow(RegExp("[0-9.]")),
    DecimalTextInputFormatter(decimalRange: 2),
  ];

  double convertMoney(double money, String from, String to) {
    if (from == 'usd' && to == 'vnd') {
      return 25000 * money;
    } else if (from == 'vnd' && to == 'usd') {
      return money / 25000;
    }
    return money;
  }
}

class DecimalTextInputFormatter extends TextInputFormatter {
  DecimalTextInputFormatter({this.decimalRange = 2});

  final int decimalRange;

  @override
  TextEditingValue formatEditUpdate(
    TextEditingValue oldValue, // unused.
    TextEditingValue newValue,
  ) {
    String newString = newValue.text;

    if (newString.isEmpty) {
      return newValue.copyWith(text: '');
    }

    String value = newValue.text;

    if (value.contains(".")) {
      if (value.substring(value.indexOf(".") + 1).length > decimalRange ||
          value.indexOf(".") != value.lastIndexOf(".")) {
        return TextEditingValue(
          text: oldValue.text,
          selection: oldValue.selection,
        );
      } else {
        final natural = value.split(".")[0];
        final decimal = value.split(".")[1];
        final int selectionIndexFromTheRight =
            newValue.text.length - newValue.selection.end;
        final f = NumberFormat("#,###");
        final number = int.parse(natural.replaceAll(f.symbols.GROUP_SEP, ''));
        newString = f.format(number) + '.' + decimal;
        return TextEditingValue(
          text: newString,
          selection: TextSelection.collapsed(
            offset: newString.length - selectionIndexFromTheRight,
          ),
        );
      }
    }

    final int selectionIndexFromTheRight =
        newValue.text.length - newValue.selection.end;
    final f = NumberFormat("#,###");
    final number = int.parse(newString.replaceAll(f.symbols.GROUP_SEP, ''));
    newString = f.format(number);
    return TextEditingValue(
      text: newString,
      selection: TextSelection.collapsed(
        offset: newString.length - selectionIndexFromTheRight,
      ),
    );
  }
}

class ThousandsSeparatorInputFormatter extends TextInputFormatter {
  static const separator = ','; // Change this to '.' for other locales
  // isDot = 2 = fail
  @override
  TextEditingValue formatEditUpdate(
    TextEditingValue oldValue,
    TextEditingValue newValue,
  ) {
    if (newValue.text.length == 0) {
      return newValue.copyWith(text: '');
    }
    String oldValueText = oldValue.text.replaceAll(separator, '');
    String newValueText = newValue.text.replaceAll(separator, '');
    if (oldValue.text.endsWith(separator) &&
        oldValue.text.length == newValue.text.length + 1) {
      newValueText = newValueText.substring(0, newValueText.length - 1);
    }
    if (oldValueText != newValueText) {
      int selectionIndex =
          newValue.text.length - newValue.selection.extentOffset;

      int checkseeni = 0;
      for (int i = newValueText.length - 1; i >= 0; i--) {
        if (newValueText[i] == '.') {
          checkseeni = i;
          break;
        }
      }
      if (checkseeni > 0) {
        newValueText =
            int.parse(newValueText.substring(0, checkseeni)).toString() +
                newValueText.substring(checkseeni, newValueText.length);
      } else {
        newValueText = newValueText;
      }
      newValueText.replaceAll(',', '');
      final chars = newValueText.split('');

      String newString = '';

      int isDot = 0;
      int seeni = checkseeni;
      for (int i = chars.length - 1; i >= 0; i--) {
        if (chars[i] != '.') {
          if (isDot == 0 && seeni == 0) {
            if ((chars.length - 1 - i) % 3 == 0 && i != chars.length - 1) {
              newString = separator + newString;
            }
          } else if (isDot == 1 || seeni > 0) {
            if (i < seeni) {
              if (seeni == chars.length - 1) {
                if ((chars.length - 1 - 1 - i) % 3 == 0 &&
                    i != chars.length - 1 - 1) {
                  newString = separator + newString;
                }
              } else {
                if ((seeni - 1 - i) % 3 == 0 && i != seeni - 1) {
                  newString = separator + newString;
                }
              }
            }
          }
        } else {
          isDot++;
          if (isDot == 1) {
            seeni = i;
          } else {
            break;
          }
        }
        newString = chars[i] + newString;
      }
      if (isDot == 2) newString = chars.removeAt(chars.length);
      return TextEditingValue(
        text: newString.toString(),
        selection: TextSelection.collapsed(
          offset: newString.length - selectionIndex,
        ),
      );
    }

    // If the new value and old value are the same, just return as-is
    return newValue;
  }
}
