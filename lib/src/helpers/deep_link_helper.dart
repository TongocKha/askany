import 'dart:async';

import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/category/category_bloc.dart';
import 'package:askany/src/bloc/category_info/category_info_bloc.dart';
import 'package:askany/src/bloc/request_info/request_info_bloc.dart';
import 'package:askany/src/bloc/skill_info/skill_info_bloc.dart';
import 'package:askany/src/constants/constants.dart';

import 'package:askany/src/routes/app_navigator_observer.dart';

import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:uni_links/uni_links.dart';

class DeepLinkHelper {
  static Future<void> initUniUrls() async {
    uriLinkStream.listen((Uri? uri) {
      if (uri != null) {
        if (AppNavigatorObserver.currentRouteName != Routes.ROOT) {
          AppNavigator.popUntil(Routes.ROOT);
        }
        if (uri.path.isNotEmpty) {
          List<String> path = uri.path.split('/');
          switch (path[1]) {
            case DetailsExpert:
              if (path[2].isNotEmpty) {
                AppNavigator.push(
                  Routes.DETAILS_SPECIALIST,
                  arguments: {
                    'expertId': path[2],
                  },
                );
              }
              break;
            case DetailsService:
              if (path[2].isNotEmpty) {
                AppBloc.requestInfoBloc.add(
                  GetRequestInfoEvent(
                    requestId: path[2],
                  ),
                );
              }
              break;
            case DetailsSkill:
              if (path[2].isNotEmpty) {
                AppBloc.skillInfoBloc.add(
                  GetSkillInfoEvent(
                    skillId: path[2],
                  ),
                );
              }
              break;
            case Category:
              if (path[2].isNotEmpty) {
                AppBloc.categoryInfoBloc.add(GetInfoCategoryEvent(
                    slug: path[2], slugSpecialty: path[3]));
              }
              break;

            case RequestService:
              if (uri.queryParameters.isNotEmpty) {
                AppBloc.categoryBloc.add(
                  GetCategoryInfoEvent(
                    categoryId: uri.queryParameters['category'] ?? '',
                  ),
                );
              }
              break;
          }
        }
      }
    }, onError: (err) {});
  }
}
