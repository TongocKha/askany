import 'dart:async';

import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/badge/badge_bloc.dart';
import 'package:askany/src/bloc/home/home_bloc.dart';
import 'package:askany/src/bloc/video_call/video_call_bloc.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/helpers/permission_helper.dart';

import 'package:askany/src/services/firebase_messaging/handle_messaging.dart';
import 'package:askany/src/ui/account/account_screen.dart';
import 'package:askany/src/ui/category/category_screen.dart';
import 'package:askany/src/ui/common/widgets/badges/badge.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/call/calling_bar.dart';
import 'package:askany/src/ui/common/widgets/text_ui/text_ui.dart';
import 'package:askany/src/ui/home/home_screen.dart';
import 'package:askany/src/ui/notification/notification_screen.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:askany/src/ui/request/request_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'bloc/timer/timer_bloc.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<Widget> _tabs = [
    HomeScreen(),
    RequestScreen(),
    CategoryScreen(),
    NotificationScreen(),
    AccountScreen(),
  ];

  @override
  void initState() {
    super.initState();
    AppBloc.initialHomeBlocWithoutAuth();
    Future.delayed(Duration(milliseconds: DELAY_HALF_SECOND), () async {
      await requestPermission();
      await PermissionHelper().checkPermissionAndRequest(Permission.camera);
      await PermissionHelper().checkPermissionAndRequest(Permission.microphone);
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomAppBar(
        clipBehavior: Clip.antiAliasWithSaveLayer,
        color: Theme.of(context).scaffoldBackgroundColor,
        elevation: .0,
        child: Container(
          height: 50.sp,
          padding: EdgeInsets.symmetric(horizontal: 6.5.sp),
          alignment: Alignment.center,
          decoration: BoxDecoration(
            border: Border(
              top: BorderSide(
                color: Theme.of(context).dividerColor,
                width: .2,
              ),
            ),
          ),
          child: Row(
            children: [
              _buildItemBottomBar(
                inActiveIcon: iconHome,
                activeIcon: iconHomeSelected,
                index: 0,
                title: Strings.home.i18n,
              ),
              _buildItemBottomBar(
                inActiveIcon: iconRequest,
                activeIcon: iconRequestSelected,
                index: 1,
                title: Strings.request.i18n,
              ),
              _buildItemBottomBar(
                inActiveIcon: iconCategory,
                activeIcon: iconCategorySelected,
                index: 2,
                title: Strings.category.i18n,
              ),
              BlocBuilder<BadgesBloc, BadgesState>(
                builder: (context, state) {
                  return _buildItemBottomBar(
                    inActiveIcon: iconNotification,
                    activeIcon: iconNotificationSelected,
                    index: 3,
                    title: Strings.notification.i18n,
                    quantity: state is GetBadgesDone ? state.badgesNotification : 0,
                  );
                },
              ),
              _buildItemBottomBar(
                inActiveIcon: iconAccount,
                activeIcon: iconAccountSelected,
                index: 4,
                title: Strings.account.i18n,
              ),
            ],
          ),
        ),
      ),
      body: Stack(
        children: [
          BlocBuilder<HomeBloc, HomeState>(
            builder: (context, state) {
              return _tabs[state.props[0]];
            },
          ),
          BlocBuilder<VideoCallBloc, VideoCallState>(
            builder: (context, videoCall) {
              return BlocBuilder<TimerBloc, TimerState>(
                builder: (context, state) {
                  return Visibility(
                    visible: videoCall is VideoCalling,
                    child: CallingBar(
                      state: state,
                    ),
                  );
                },
              );
            },
          ),
        ],
      ),
    );
  }

  Widget _buildItemBottomBar({inActiveIcon, activeIcon, index, title, int quantity = 0}) {
    return Expanded(
      child: TouchableOpacity(
        onTap: () {
          AppBloc.homeBloc.add(OnChangeIndexEvent(
            index: index,
          ));
        },
        child: BlocBuilder<HomeBloc, HomeState>(
          builder: (context, state) {
            int _currentIndex = state.props[0];
            return Container(
              color: Colors.transparent,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Badge(
                    badgeContent: TextUI(
                      '$quantity',
                      color: colorGreen2,
                      fontSize: 8.5.sp,
                      fontWeight: FontWeight.w700,
                    ),
                    showBadge: quantity > 0,
                    badgeColor: colorGreen5,
                    child: Container(
                      color: Colors.transparent,
                      child: Image.asset(
                        _currentIndex == index ? activeIcon : inActiveIcon,
                        width: 21.sp,
                        height: 21.sp,
                        color: _currentIndex == index ? null : colorIconInactive,
                      ),
                    ),
                  ),
                  SizedBox(height: 4.5.sp),
                  Text(
                    title,
                    style: TextStyle(
                      fontSize: 8.5.sp,
                      color: _currentIndex == index ? colorGreen2 : colorGray1,
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
