import 'dart:convert';

class CancelModel {
  CancelModel({
    required this.id,
    required this.reason,
    required this.content,
  });

  final String id;
  final Reason reason;
  final String content;

  factory CancelModel.fromJson(String str) => CancelModel.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory CancelModel.fromMap(Map<String, dynamic> json) => CancelModel(
        id: json["_id"],
        reason: Reason.fromMap(json["reason"]),
        content: json["content"] ?? '',
      );

  Map<String, dynamic> toMap() => {
        "_id": id,
        "reason": reason.toMap(),
        "content": content,
      };
}

class Reason {
  Reason({
    required this.id,
    this.vi,
    this.createdAt,
    this.modifiedAt,
    this.v,
  });

  final String id;
  final Vi? vi;
  final DateTime? createdAt;
  final DateTime? modifiedAt;
  final int? v;

  factory Reason.fromJson(String str) => Reason.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Reason.fromMap(Map<String, dynamic> json) => Reason(
        id: json["_id"],
        vi: Vi.fromMap(json["vi"]),
        createdAt: DateTime.parse(json["createdAt"]),
        modifiedAt: DateTime.parse(json["modifiedAt"]),
        v: json["__v"],
      );

  Map<String, dynamic> toMap() => {
        "_id": id,
        "vi": vi,
        "createdAt": createdAt?.toIso8601String(),
        "modifiedAt": modifiedAt?.toIso8601String(),
        "__v": v,
      };
}

class Vi {
  Vi({
    this.id,
    this.name,
    this.slug,
    this.createdAt,
    this.modifiedAt,
    this.v,
  });

  final String? id;
  final String? name;
  final String? slug;
  final DateTime? createdAt;
  final DateTime? modifiedAt;
  final int? v;

  factory Vi.fromJson(String str) => Vi.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Vi.fromMap(Map<String, dynamic> json) => Vi(
        id: json["_id"],
        name: json["name"],
        slug: json["slug"],
        createdAt: DateTime.parse(json["createdAt"]),
        modifiedAt: DateTime.parse(json["modifiedAt"]),
        v: json["__v"],
      );

  Map<String, dynamic> toMap() => {
        "_id": id,
        "name": name,
        "slug": slug,
        "createdAt": createdAt?.toIso8601String(),
        "modifiedAt": modifiedAt?.toIso8601String(),
        "__v": v,
      };
}
