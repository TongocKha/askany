import 'dart:convert';

import 'package:askany/src/helpers/date_time_helper.dart';
import 'package:askany/src/ui/style/request_style.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class ExpertTimelineModel {
  final List<TimelineModel> busyTimes;
  final List<TimelineModel> scheduleTime;
  List<int> dateAvailableInMonth;
  ExpertTimelineModel({
    required this.busyTimes,
    required this.scheduleTime,
    required this.dateAvailableInMonth,
  });

  ExpertTimelineModel copyWith({
    List<TimelineModel>? busyTimes,
    List<TimelineModel>? scheduleTime,
    List<int>? dateAvailableInMonth,
  }) {
    return ExpertTimelineModel(
      busyTimes: busyTimes ?? this.busyTimes,
      scheduleTime: scheduleTime ?? this.scheduleTime,
      dateAvailableInMonth: dateAvailableInMonth ?? this.dateAvailableInMonth,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'busyTimes': busyTimes.map((x) => x.toMap()).toList(),
      'scheduleTime': scheduleTime.map((x) => x.toMap()).toList(),
    };
  }

  factory ExpertTimelineModel.fromMap(Map<String, dynamic> map) {
    return ExpertTimelineModel(
      busyTimes: List<TimelineModel>.from(
          map['busyTimes']?.map((x) => TimelineModel.fromExpertTimeline(x))),
      scheduleTime: List<TimelineModel>.from(
          map['scheduleTime']?.map((x) => TimelineModel.fromExpertTimeline(x))),
      dateAvailableInMonth: ((map['dateAvailableInMonth'] ?? []) as List)
          .map((e) => int.parse(
                e.toString(),
              ))
          .toList(),
    );
  }

  String toJson() => json.encode(toMap());

  factory ExpertTimelineModel.fromJson(String source) =>
      ExpertTimelineModel.fromMap(json.decode(source));

  @override
  String toString() => 'ExpertTimelineModel(busyTimes: $busyTimes, scheduleTime: $scheduleTime)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is ExpertTimelineModel &&
        listEquals(other.busyTimes, busyTimes) &&
        listEquals(other.scheduleTime, scheduleTime);
  }

  @override
  int get hashCode => busyTimes.hashCode ^ scheduleTime.hashCode;

  List<List<DateTime>> getBusyTime({DateTime? date}) {
    List<List<DateTime>> result = busyTimes.map((e) => e.timeline(date: date)).toList();
    List<List<DateTime>> schedule = getScheduleTime();
    DateTime? startTime;
    bool flag = false;
    List<dynamic> listTimeForCheck = [];
    listTimeForCheck.addAll(listTime);
    listTimeForCheck.insert(0, listTime.first);
    listTimeForCheck.add(listTime.last);
    listTimeForCheck.asMap().forEach((indexOfTime, time) {
      DateTime timeOfDay = _convertStringToTime(time);
      bool flagSchedule = false;

      schedule.asMap().forEach((index, element) {
        bool inRangeTime = _isInTimeRange(timeOfDay, element.first, element.last);
        if (!flagSchedule) {
          if (!inRangeTime && index == schedule.length - 1) {
            if (!flag) {
              startTime = timeOfDay;
              flag = true;
            } else if (indexOfTime == listTimeForCheck.length - 1) {
              result.add([startTime!, _convertStringToTime(listTimeForCheck[indexOfTime - 1])]);
            }
          } else {
            if (inRangeTime) {
              flagSchedule = true;
            }

            if (flag) {
              flag = false;
              result.add([startTime!, _convertStringToTime(listTimeForCheck[indexOfTime - 1])]);
            }
          }
        }
      });
    });

    return result;
  }

  List<List<DateTime>> getScheduleTime({DateTime? date}) {
    List<List<DateTime>> result = scheduleTime.map((e) => e.timeline(date: date)).toList();
    result.sort((a, b) => a.first.compareTo(b.first));

    return result;
  }

  bool _isInTimeRange(DateTime time, DateTime timeStart, DateTime? timeEnd) {
    if (timeEnd == null) return false;
    return (time.isAfter(timeStart) || time.isAtSameMomentAs(timeStart)) &&
        (time.isBefore(timeEnd) || time.isAtSameMomentAs(timeEnd));
  }

  DateTime _convertStringToTime(String timeString) {
    return DateTime(
      DateTime.now().year,
      DateTime.now().month,
      DateTime.now().day,
      int.parse(timeString.split(':')[0]),
      int.parse(timeString.split(':')[1]),
    );
  }
}

class TimelineModel {
  final TimeOfDay startTime;
  final TimeOfDay endTime;
  TimelineModel({
    required this.startTime,
    required this.endTime,
  });

  TimelineModel copyWith({
    TimeOfDay? startTime,
    TimeOfDay? endTime,
  }) {
    return TimelineModel(
      startTime: startTime ?? this.startTime,
      endTime: endTime ?? this.endTime,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'startTime': {
        'hours': startTime.hour.toString(),
        'minutes': startTime.minute.toString(),
      },
      'endTime': {
        'hours': endTime.hour.toString(),
        'minutes': endTime.minute.toString(),
      },
    };
  }

  factory TimelineModel.fromExpertTimeline(Map<String, dynamic> map) {
    return TimelineModel(
      startTime: TimeOfDay(
        hour: int.parse((map['startTime']['hours'] ?? 0).toString()),
        minute: int.parse((map['startTime']['minutes'] ?? 0).toString()),
      ),
      endTime: TimeOfDay(
        hour: int.parse((map['endTime']['hours'] ?? 0).toString()),
        minute: int.parse((map['endTime']['minutes'] ?? 0).toString()),
      ),
    );
  }

  String toJson() => json.encode(toMap());

  factory TimelineModel.fromJson(String source) =>
      TimelineModel.fromExpertTimeline(json.decode(source));

  @override
  String toString() => 'TimelineModel(startTime: $startTime, endTime: $endTime)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is TimelineModel && other.startTime == startTime && other.endTime == endTime;
  }

  @override
  int get hashCode => startTime.hashCode ^ endTime.hashCode;

  List<DateTime> timeline({DateTime? date}) {
    DateTime dateFormat = date ?? DateTime.now();

    return [_addTimeToDate(startTime, dateFormat), _addTimeToDate(endTime, dateFormat)];
  }

  DateTime _addTimeToDate(TimeOfDay time, DateTime date) {
    return DateTime(date.year, date.month, date.day, time.hour, time.minute);
  }

  String get timeString =>
      '${addZeroPrefix(startTime.hour)}:${addZeroPrefix(startTime.minute)} - ${addZeroPrefix(endTime.hour)}:${addZeroPrefix(endTime.minute)}';
}
