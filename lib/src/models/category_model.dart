import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:askany/src/models/avatar_model.dart';

class CategoryModel {
  final String id;
  final String name;
  final String content;
  final String slug;
  final AvatarModel? thumbnail;
  final AvatarModel? icon;
  final String? iconId;
  CategoryModel({
    required this.id,
    required this.name,
    required this.content,
    required this.slug,
    this.thumbnail,
    this.icon,
    this.iconId,
  });

  CategoryModel copyWith({
    String? id,
    String? name,
    String? content,
    String? slug,
    AvatarModel? thumbnail,
    AvatarModel? icon,
  }) {
    return CategoryModel(
      id: id ?? this.id,
      name: name ?? this.name,
      content: content ?? this.content,
      slug: slug ?? this.slug,
      thumbnail: thumbnail ?? this.thumbnail,
      icon: icon ?? this.icon,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'name': name,
      'content': content,
      'slug': slug,
      'thumbnail': thumbnail?.toMap(),
      'icon': icon?.toMap(),
    };
  }

  factory CategoryModel.fromMap(Map<String, dynamic> map) {
    return CategoryModel(
      id: map['_id'] ?? '',
      name: map['name'] ?? '',
      content: map['content'] ?? '',
      slug: map['slug'] ?? '',
      thumbnail: map['thumbnail'] != null
          ? AvatarModel.fromMap(map['thumbnail'])
          : null,
      icon: map['icon'] != null ? AvatarModel.fromMap(map['icon']) : null,
    );
  }

  factory CategoryModel.fromMapHome(Map<String, dynamic> map) {
    return CategoryModel(
      id: map['_id'] ?? '',
      name: map['name'] ?? '',
      content: '',
      slug: map['slug'] ?? '',
      thumbnail: map['thumbnail'] != null
          ? AvatarModel.fromMap(map['thumbnail'])
          : null,
      icon: null,
    );
  }

  String toJson() => json.encode(toMap());

  factory CategoryModel.fromJson(String source) =>
      CategoryModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'CategoryModel(id: $id, name: $name, content: $content, slug: $slug, thumbnail: $thumbnail, icon: $icon)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is CategoryModel &&
        other.id == id &&
        other.name == name &&
        other.content == content &&
        other.slug == slug &&
        other.thumbnail == thumbnail &&
        other.icon == icon;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        name.hashCode ^
        content.hashCode ^
        slug.hashCode ^
        thumbnail.hashCode ^
        icon.hashCode;
  }
}

class CategoryInfoModel {
  final CategoryModel categoryInfo;
  final List<CategoryModel> listSubCates;
  CategoryInfoModel({
    required this.categoryInfo,
    required this.listSubCates,
  });

  CategoryInfoModel copyWith({
    CategoryModel? categoryInfo,
    List<CategoryModel>? listSubCates,
  }) {
    return CategoryInfoModel(
      categoryInfo: categoryInfo ?? this.categoryInfo,
      listSubCates: listSubCates ?? this.listSubCates,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'categoryInfo': categoryInfo.toMap(),
      'listSubCates': listSubCates.map((x) => x.toMap()).toList(),
    };
  }

  factory CategoryInfoModel.fromMap(Map<String, dynamic> map) {
    return CategoryInfoModel(
      categoryInfo: CategoryModel.fromMap(map['categoryInfo']),
      listSubCates: List<CategoryModel>.from(
          map['listSubCates']?.map((x) => CategoryModel.fromMap(x))),
    );
  }

  factory CategoryInfoModel.fromInfoCategory(Map<String, dynamic> map) {
    return CategoryInfoModel(
      categoryInfo: CategoryModel.fromMap(map['categoryInfo']),
      listSubCates: List<CategoryModel>.from(map['subCategoriesWithSpecialties']
          ?.map((x) => CategoryModel.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory CategoryInfoModel.fromJson(String source) =>
      CategoryInfoModel.fromMap(json.decode(source));

  @override
  String toString() =>
      'CategoryInfoModel(categoryInfo: $categoryInfo, listSubCates: $listSubCates)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is CategoryInfoModel &&
        other.categoryInfo == categoryInfo &&
        listEquals(other.listSubCates, listSubCates);
  }

  @override
  int get hashCode => categoryInfo.hashCode ^ listSubCates.hashCode;
}
