import 'dart:convert';

import 'package:askany/src/models/account_model.dart';

class NotificationModel {
  final String id;
  final AccountModel user;
  final AccountModel expert;
  dynamic admin;
  int status;
  final int type;
  final bool fromSystem;
  final String message;
  final RequestNotiModel? request;
  final NotificationOffer? offer;
  final dynamic transaction;
  final DateTime createdAt;
  final DateTime modifiedAt;
  NotificationModel({
    required this.id,
    required this.user,
    required this.expert,
    required this.admin,
    required this.status,
    required this.type,
    required this.fromSystem,
    required this.message,
    required this.request,
    required this.offer,
    required this.transaction,
    required this.createdAt,
    required this.modifiedAt,
  });

  NotificationModel copyWith({
    String? id,
    AccountModel? user,
    AccountModel? expert,
    dynamic admin,
    int? status,
    int? type,
    bool? fromSystem,
    String? message,
    RequestNotiModel? request,
    dynamic offer,
    dynamic transaction,
    DateTime? createdAt,
    DateTime? modifiedAt,
  }) {
    return NotificationModel(
      id: id ?? this.id,
      user: user ?? this.user,
      expert: expert ?? this.expert,
      admin: admin ?? this.admin,
      status: status ?? this.status,
      type: type ?? this.type,
      fromSystem: fromSystem ?? this.fromSystem,
      message: message ?? this.message,
      request: request ?? this.request,
      offer: offer ?? this.offer,
      transaction: transaction ?? this.transaction,
      createdAt: createdAt ?? this.createdAt,
      modifiedAt: modifiedAt ?? this.modifiedAt,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'user': user.toMapUserNotification(),
      'expert': expert.toMapUserNotification(),
      'admin': null,
      'status': status,
      'type': type,
      'fromSystem': fromSystem,
      'message': message,
      'request': request?.toMap(),
      'offer': offer?.toMap(),
      'transaction': null,
      'createdAt': createdAt.toString(),
      'modifiedAt': modifiedAt.toString(),
    };
  }

  factory NotificationModel.fromMap(Map<String, dynamic> map) {
    return NotificationModel(
      id: map['_id'] ?? '',
      user: AccountModel.fromMap(map['user']),
      expert: AccountModel.fromMap(map['expert']),
      admin: map['admin'] ?? null,
      status: map['status']?.toInt() ?? 0,
      type: map['type']?.toInt() ?? 0,
      fromSystem: map['fromSystem'] ?? false,
      message: map['message'] ?? '',
      request: map['request'] == null ? null : RequestNotiModel.fromMap(map['request']),
      offer: map['offer'] == null ? null : NotificationOffer.fromMap(map['offer']),

      //  ?? null,
      transaction: map['transaction'] ?? null,
      createdAt: DateTime.parse(map['createdAt']).toLocal(),
      modifiedAt: DateTime.parse(map['modifiedAt']).toLocal(),
    );
  }

  String toJson() => json.encode(toMap());

  factory NotificationModel.fromJson(String source) =>
      NotificationModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'NotificationModel(id: $id, user: $user, expert: $expert, admin: $admin, status: $status, type: $type, fromSystem: $fromSystem, message: $message, request: $request, offer: $offer, transaction: $transaction, createdAt: $createdAt, modifiedAt: $modifiedAt)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is NotificationModel &&
        other.id == id &&
        other.user == user &&
        other.expert == expert &&
        other.admin == admin &&
        other.status == status &&
        other.type == type &&
        other.fromSystem == fromSystem &&
        other.message == message &&
        other.request == request &&
        other.offer == offer &&
        other.transaction == transaction &&
        other.createdAt == createdAt &&
        other.modifiedAt == modifiedAt;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        user.hashCode ^
        expert.hashCode ^
        admin.hashCode ^
        status.hashCode ^
        type.hashCode ^
        fromSystem.hashCode ^
        message.hashCode ^
        request.hashCode ^
        offer.hashCode ^
        transaction.hashCode ^
        createdAt.hashCode ^
        modifiedAt.hashCode;
  }

  bool get isSeen => status == 1;

  AccountModel get userNotification => type % 2 != 0 ? expert : user;
}

class RequestNotiModel {
  final String id;
  final String title;
  final String slug;
  final int status;
  RequestNotiModel({
    required this.id,
    required this.title,
    required this.slug,
    required this.status,
  });

  RequestNotiModel copyWith({
    String? id,
    String? title,
    String? slug,
    int? status,
  }) {
    return RequestNotiModel(
      id: id ?? this.id,
      title: title ?? this.title,
      slug: slug ?? this.slug,
      status: status ?? this.status,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'title': title,
      'slug': slug,
      'status': status,
    };
  }

  factory RequestNotiModel.fromMap(Map<String, dynamic> map) {
    return RequestNotiModel(
      id: map['id'] ?? '',
      title: map['title'] ?? '',
      slug: map['slug'] ?? '',
      status: map['status']?.toInt() ?? 0,
    );
  }

  String toJson() => json.encode(toMap());

  factory RequestNotiModel.fromJson(String source) => RequestNotiModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'RequestNotiModel(id: $id, title: $title, slug: $slug, status: $status)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is RequestNotiModel &&
        other.id == id &&
        other.title == title &&
        other.slug == slug &&
        other.status == status;
  }

  @override
  int get hashCode {
    return id.hashCode ^ title.hashCode ^ slug.hashCode ^ status.hashCode;
  }
}

class NotificationOffer {
  NotificationOffer({
    required this.id,
    required this.status,
  });

  final String id;
  final int status;

  factory NotificationOffer.fromJson(String str) => NotificationOffer.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory NotificationOffer.fromMap(Map<String, dynamic> json) => NotificationOffer(
        id: json["_id"],
        status: json["status"],
      );

  Map<String, dynamic> toMap() => {
        "_id": id,
        "status": status,
      };
}
