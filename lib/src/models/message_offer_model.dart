import 'dart:convert';

import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/models/offer_model.dart';
import 'package:askany/src/models/request_model.dart';

class MessageOfferModel {
  final String id;
  final String requestTitle;
  final String requestSlug;
  final String authorExpert;
  final String price;
  final String content;
  final String offeredRequest;
  final String offeredUser;
  final int status;
  final String note;
  final String authorPhone;
  final String authorName;
  final DateTime startTime;
  final DateTime endTime;
  final DateTime askanyPaymentTime;
  final DateTime confirmedTime;
  final DateTime completedTime;
  final DateTime dateExpired;
  final bool isPickTime;
  final DateTime createdAt;
  final DateTime modifiedAt;
  final String? cancel;
  final String? cancelledBy;
  MessageOfferModel({
    required this.id,
    required this.requestTitle,
    required this.requestSlug,
    required this.authorExpert,
    required this.price,
    required this.content,
    required this.offeredRequest,
    required this.offeredUser,
    required this.status,
    required this.note,
    required this.authorPhone,
    required this.authorName,
    required this.startTime,
    required this.endTime,
    required this.askanyPaymentTime,
    required this.confirmedTime,
    required this.completedTime,
    required this.dateExpired,
    required this.isPickTime,
    required this.createdAt,
    required this.modifiedAt,
    this.cancel,
    this.cancelledBy,
  });

  MessageOfferModel copyWith({
    String? id,
    String? requestTitle,
    String? requestSlug,
    String? authorExpert,
    String? price,
    String? content,
    String? offeredRequest,
    String? offeredUser,
    int? status,
    String? note,
    String? authorPhone,
    String? authorName,
    DateTime? startTime,
    DateTime? endTime,
    DateTime? askanyPaymentTime,
    DateTime? confirmedTime,
    DateTime? completedTime,
    DateTime? dateExpired,
    bool? isPickTime,
    DateTime? createdAt,
    DateTime? modifiedAt,
    String? cancel,
    String? cancelledBy,
  }) {
    return MessageOfferModel(
      id: id ?? this.id,
      requestTitle: requestTitle ?? this.requestTitle,
      requestSlug: requestSlug ?? this.requestSlug,
      authorExpert: authorExpert ?? this.authorExpert,
      price: price ?? this.price,
      content: content ?? this.content,
      offeredRequest: offeredRequest ?? this.offeredRequest,
      offeredUser: offeredUser ?? this.offeredUser,
      status: status ?? this.status,
      note: note ?? this.note,
      authorPhone: authorPhone ?? this.authorPhone,
      authorName: authorName ?? this.authorName,
      startTime: startTime ?? this.startTime,
      endTime: endTime ?? this.endTime,
      askanyPaymentTime: askanyPaymentTime ?? this.askanyPaymentTime,
      confirmedTime: confirmedTime ?? this.confirmedTime,
      completedTime: completedTime ?? this.completedTime,
      dateExpired: dateExpired ?? this.dateExpired,
      isPickTime: isPickTime ?? this.isPickTime,
      createdAt: createdAt ?? this.createdAt,
      modifiedAt: modifiedAt ?? this.modifiedAt,
      cancel: cancel ?? this.cancel,
      cancelledBy: cancelledBy ?? this.cancelledBy,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'requestTitle': requestTitle,
      'requestSlug': requestSlug,
      'authorExpert': authorExpert,
      'price': price,
      'content': content,
      'offeredRequest': offeredRequest,
      'offeredUser': offeredUser,
      'status': status,
      'note': note,
      'authorPhone': authorPhone,
      'authorName': authorName,
      'startTime': startTime.toString(),
      'endTime': endTime.toString(),
      'askanyPaymentTime': askanyPaymentTime.toString(),
      'confirmedTime': confirmedTime.toString(),
      'completedTime': completedTime.toString(),
      'dateExpired': dateExpired.toString(),
      'isPickTime': isPickTime,
      'createdAt': createdAt.toString(),
      'modifiedAt': modifiedAt.toString(),
      'cancel': cancel,
      'cancelledBy': cancelledBy,
    };
  }

  factory MessageOfferModel.fromMap(Map<String, dynamic> map) {
    return MessageOfferModel(
      id: map['_id'] ?? '',
      requestTitle: map['requestTitle'] ?? '',
      requestSlug: map['requestSlug'] ?? '',
      authorExpert: map['authorExpert'] ?? '',
      price: map['price'] ?? '',
      content: map['content'] ?? '',
      offeredRequest: map['offeredRequest']['_id'] ?? '',
      offeredUser: map['offeredUser'] ?? '',
      status: map['status']?.toInt() ?? 0,
      note: map['note'] ?? '',
      authorPhone: map['authorPhone'] ?? '',
      authorName: map['authorName'] ?? '',
      startTime: map['startTime'] != null
          ? DateTime.parse(map['startTime']).toLocal()
          : DateTime.now(),
      endTime: map['endTime'] != null
          ? DateTime.parse(map['endTime']).toLocal()
          : DateTime.now(),
      askanyPaymentTime: map['askanyPaymentTime'] != null
          ? DateTime.parse(map['askanyPaymentTime']).toLocal()
          : DateTime.now(),
      confirmedTime: map['confirmedTime'] != null
          ? DateTime.parse(map['confirmedTime']).toLocal()
          : DateTime.now(),
      completedTime: map['completedTime'] != null
          ? DateTime.parse(map['completedTime']).toLocal()
          : DateTime.now(),
      dateExpired: map['dateExpired'] != null
          ? DateTime.parse(map['dateExpired']).toLocal()
          : DateTime.now(),
      isPickTime: map['isPickTime'] ?? false,
      createdAt: map['createdAt'] != null
          ? DateTime.parse(map['createdAt']).toLocal()
          : DateTime.now(),
      modifiedAt: map['modifiedAt'] != null
          ? DateTime.parse(map['modifiedAt']).toLocal()
          : DateTime.now(),
      cancel: map['cancel'] ?? '',
      cancelledBy: map['cancelledBy'] ?? '',
    );
  }

  factory MessageOfferModel.fromRequestModel(
      RequestModel request, OfferModel offer) {
    return MessageOfferModel(
      id: DateTime.now().microsecondsSinceEpoch.toString(),
      requestTitle: request.title,
      requestSlug: request.slug ?? '',
      authorExpert: offer.authorExpert.id,
      price: offer.price.cost.toString(),
      content: request.content,
      offeredRequest: offer.id,
      offeredUser:
          request.authorUser?.id ?? AppBloc.userBloc.getAccount.id ?? '',
      status: offer.status,
      note: offer.content,
      authorPhone: offer.authorPhone ?? '',
      authorName: offer.authorName ?? '',
      startTime: offer.startTime,
      endTime: offer.endTime,
      askanyPaymentTime: request.askanyPaymentTime ?? offer.startTime,
      confirmedTime: request.confirmedTime ?? offer.startTime,
      completedTime: request.completedTime ?? offer.startTime,
      dateExpired: request.dateExpired,
      isPickTime: offer.isPickTime,
      createdAt: DateTime.now(),
      modifiedAt: DateTime.now(),
      cancel: null,
      cancelledBy: null,
    );
  }

  String toJson() => json.encode(toMap());

  factory MessageOfferModel.fromJson(String source) =>
      MessageOfferModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'MessageOfferModel(id: $id, requestTitle: $requestTitle, requestSlug: $requestSlug, authorExpert: $authorExpert, price: $price, content: $content, offeredRequest: $offeredRequest, offeredUser: $offeredUser, status: $status, note: $note, authorPhone: $authorPhone, authorName: $authorName, startTime: $startTime, endTime: $endTime, askanyPaymentTime: $askanyPaymentTime, confirmedTime: $confirmedTime, completedTime: $completedTime, dateExpired: $dateExpired, isPickTime: $isPickTime, createdAt: $createdAt, modifiedAt: $modifiedAt, cancel: $cancel, cancelledBy: $cancelledBy)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is MessageOfferModel &&
        other.id == id &&
        other.requestTitle == requestTitle &&
        other.requestSlug == requestSlug &&
        other.authorExpert == authorExpert &&
        other.price == price &&
        other.content == content &&
        other.offeredRequest == offeredRequest &&
        other.offeredUser == offeredUser &&
        other.status == status &&
        other.note == note &&
        other.authorPhone == authorPhone &&
        other.authorName == authorName &&
        other.startTime == startTime &&
        other.endTime == endTime &&
        other.askanyPaymentTime == askanyPaymentTime &&
        other.confirmedTime == confirmedTime &&
        other.completedTime == completedTime &&
        other.dateExpired == dateExpired &&
        other.isPickTime == isPickTime &&
        other.createdAt == createdAt &&
        other.modifiedAt == modifiedAt &&
        other.cancel == cancel &&
        other.cancelledBy == cancelledBy;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        requestTitle.hashCode ^
        requestSlug.hashCode ^
        authorExpert.hashCode ^
        price.hashCode ^
        content.hashCode ^
        offeredRequest.hashCode ^
        offeredUser.hashCode ^
        status.hashCode ^
        note.hashCode ^
        authorPhone.hashCode ^
        authorName.hashCode ^
        startTime.hashCode ^
        endTime.hashCode ^
        askanyPaymentTime.hashCode ^
        confirmedTime.hashCode ^
        completedTime.hashCode ^
        dateExpired.hashCode ^
        isPickTime.hashCode ^
        createdAt.hashCode ^
        modifiedAt.hashCode ^
        cancel.hashCode ^
        cancelledBy.hashCode;
  }
}
