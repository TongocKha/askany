import 'dart:convert';
import 'package:flutter/foundation.dart';

class SearchModel {
  String search;
  final String? companyName;
  final int? experienceYears;
  final int? contactType;
  final int? province;
  final String? position;
  final String? cost;
  final String? specialty;
  final String? category;
  List<String>? ratings;
  final int? sortType;
  SearchModel({
    this.search = '',
    this.companyName,
    this.experienceYears,
    this.contactType,
    this.province,
    this.position,
    this.cost,
    this.specialty,
    this.category,
    this.ratings,
    this.sortType,
  });

  SearchModel copyWith({
    String? search,
    String? companyName,
    int? experienceYears,
    int? contactType,
    int? province,
    String? position,
    String? cost,
    String? specialty,
    String? category,
    List<String>? ratings,
  }) {
    return SearchModel(
      search: search ?? this.search,
      companyName: companyName ?? this.companyName,
      experienceYears: experienceYears ?? this.experienceYears,
      contactType: contactType ?? this.contactType,
      province: province ?? this.province,
      position: position ?? this.position,
      cost: cost ?? this.cost,
      specialty: specialty ?? this.specialty,
      category: category ?? this.category,
      ratings: ratings ?? this.ratings,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'search': search,
      'companyName': companyName,
      'experienceYears': experienceYears,
      'contactType': contactType,
      'province': province,
      'position': position,
      'cost': cost,
      'specialty': specialty,
      'category': category,
      'ratings': ratings,
      'sortType': sortType,
    };
  }

  factory SearchModel.fromMap(Map<String, dynamic> map) {
    return SearchModel(
      search: map['search'],
      companyName: map['companyName'],
      experienceYears: map['experienceYears']?.toInt(),
      contactType: map['contactType']?.toInt(),
      province: map['province']?.toInt(),
      position: map['position'],
      cost: map['cost'],
      specialty: map['specialty'],
      category: map['category'],
      ratings: map['ratings'],
      sortType: map['sortType']?.toInt(),
    );
  }

  String toJson() => json.encode(toMap());

  factory SearchModel.fromJson(String source) =>
      SearchModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'SearchModel(search: $search, companyName: $companyName, experienceYears: $experienceYears, contactType: $contactType, province: $province, position: $position, cost: $cost)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is SearchModel &&
        other.search == search &&
        other.companyName == companyName &&
        other.experienceYears == experienceYears &&
        other.contactType == contactType &&
        other.province == province &&
        other.position == position &&
        other.cost == cost;
  }

  @override
  int get hashCode {
    return search.hashCode ^
        companyName.hashCode ^
        experienceYears.hashCode ^
        contactType.hashCode ^
        province.hashCode ^
        position.hashCode ^
        cost.hashCode;
  }

  String get query {
    List<String> result = [];

    List<String> keys = toMap().keys.toList();
    List<dynamic> values = toMap().values.toList();

    values.asMap().forEach((index, value) {
      if (value != null && value.toString().isNotEmpty) {
        if (keys[index] != 'ratings') {
          result.add('${keys[index]}=$value');
        }
      }
    });

    if (ratings != null) {
      ratings!.forEach((val) {
        result.add('rating=$val');
      });
    }

    if (result.isEmpty) {
      return '';
    }

    result.add('');

    return result.join('&');
  }
}

class LocalSearchModel {
  final String searchKey;
  final DateTime createdAt;
  final String createdBy;
  LocalSearchModel({
    required this.searchKey,
    required this.createdAt,
    required this.createdBy,
  });

  LocalSearchModel copyWith({
    String? searchKey,
    DateTime? createdAt,
    String? createdBy,
  }) {
    return LocalSearchModel(
      searchKey: searchKey ?? this.searchKey,
      createdAt: createdAt ?? this.createdAt,
      createdBy: createdBy ?? this.createdBy,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'searchKey': searchKey,
      'createdAt': createdAt.toString(),
      'createdBy': createdBy,
    };
  }

  factory LocalSearchModel.fromMap(Map<String, dynamic> map) {
    return LocalSearchModel(
      searchKey: map['searchKey'] ?? '',
      createdAt: DateTime.parse(map['createdAt']).toLocal(),
      createdBy: map['createdBy'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory LocalSearchModel.fromJson(String source) =>
      LocalSearchModel.fromMap(json.decode(source));

  @override
  String toString() =>
      'LocalSearchModel(searchKey: $searchKey, createdAt: $createdAt, createdBy: $createdBy)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is LocalSearchModel &&
        other.searchKey == searchKey &&
        other.createdAt == createdAt &&
        other.createdBy == createdBy;
  }

  @override
  int get hashCode =>
      searchKey.hashCode ^ createdAt.hashCode ^ createdBy.hashCode;
}

class ResultSearchLocalModel {
  final bool isOver;
  final int totalResult;
  final List<Map<String, dynamic>> result;
  ResultSearchLocalModel({
    required this.isOver,
    required this.totalResult,
    required this.result,
  });

  ResultSearchLocalModel copyWith({
    bool? isOver,
    int? totalResult,
    List<Map<String, dynamic>>? result,
  }) {
    return ResultSearchLocalModel(
      isOver: isOver ?? this.isOver,
      totalResult: totalResult ?? this.totalResult,
      result: result ?? this.result,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'isOver': isOver,
      'totalResult': totalResult,
      'result': result,
    };
  }

  factory ResultSearchLocalModel.fromMap(Map<String, dynamic> map) {
    return ResultSearchLocalModel(
      isOver: map['isOver'] ?? false,
      totalResult: map['totalResult']?.toInt() ?? 0,
      result: [],
    );
  }

  String toJson() => json.encode(toMap());

  factory ResultSearchLocalModel.fromJson(String source) =>
      ResultSearchLocalModel.fromMap(json.decode(source));

  @override
  String toString() =>
      'ResultSearchLocalModel(isOver: $isOver, totalResult: $totalResult, result: $result)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is ResultSearchLocalModel &&
        other.isOver == isOver &&
        other.totalResult == totalResult &&
        listEquals(other.result, result);
  }

  @override
  int get hashCode => isOver.hashCode ^ totalResult.hashCode ^ result.hashCode;
}
