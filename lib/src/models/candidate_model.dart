import 'dart:convert';

class CandidateModel {
  final String candidate;
  final String sdpMid;
  final int sdpMLineIndex;
  CandidateModel({
    required this.candidate,
    required this.sdpMid,
    required this.sdpMLineIndex,
  });

  CandidateModel copyWith({
    String? candidate,
    String? sdpMid,
    int? sdpMLineIndex,
  }) {
    return CandidateModel(
      candidate: candidate ?? this.candidate,
      sdpMid: sdpMid ?? this.sdpMid,
      sdpMLineIndex: sdpMLineIndex ?? this.sdpMLineIndex,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'candidate': candidate,
      'sdpMid': sdpMid,
      'sdpMLineIndex': sdpMLineIndex,
    };
  }

  factory CandidateModel.fromMap(Map<String, dynamic> map) {
    return CandidateModel(
      candidate: map['candidate'] ?? '',
      sdpMid: map['sdpMid'] ?? '',
      sdpMLineIndex: map['sdpMLineIndex']?.toInt() ?? 0,
    );
  }

  String toJson() => json.encode(toMap());

  factory CandidateModel.fromJson(String source) => CandidateModel.fromMap(json.decode(source));

  @override
  String toString() =>
      'CandidateModel(candidate: $candidate, sdpMid: $sdpMid, sdpMLineIndex: $sdpMLineIndex)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is CandidateModel &&
        other.candidate == candidate &&
        other.sdpMid == sdpMid &&
        other.sdpMLineIndex == sdpMLineIndex;
  }

  @override
  int get hashCode => candidate.hashCode ^ sdpMid.hashCode ^ sdpMLineIndex.hashCode;
}
