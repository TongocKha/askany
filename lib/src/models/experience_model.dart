import 'dart:convert';
import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/helpers/date_time_helper.dart';
import 'package:askany/src/models/specialty_model.dart';
import 'package:flutter/foundation.dart';

class ExperienceModel {
  final String id;
  String companyName;
  List<String> specialties;
  String position;
  DateTime timeStartWork;
  DateTime timeEndWork;
  String description;
  final int status;
  final String author;
  final DateTime createdAt;
  ExperienceModel({
    required this.id,
    required this.companyName,
    required this.specialties,
    required this.position,
    required this.timeStartWork,
    required this.timeEndWork,
    required this.description,
    required this.status,
    required this.author,
    required this.createdAt,
  });

  ExperienceModel copyWith({
    String? id,
    String? companyName,
    List<String>? specialties,
    String? position,
    DateTime? timeStartWork,
    DateTime? timeEndWork,
    String? description,
    int? status,
    String? author,
    DateTime? createdAt,
  }) {
    return ExperienceModel(
      id: id ?? this.id,
      companyName: companyName ?? this.companyName,
      specialties: specialties ?? this.specialties,
      position: position ?? this.position,
      timeStartWork: timeStartWork ?? this.timeStartWork,
      timeEndWork: timeEndWork ?? this.timeEndWork,
      description: description ?? this.description,
      status: status ?? this.status,
      author: author ?? this.author,
      createdAt: createdAt ?? this.createdAt,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'companyName': companyName,
      'specialties': specialties,
      'position': position,
      'timeStartWork': timeStartWork.toString(),
      'timeEndWork': timeEndWork.toString(),
      'description': description,
      'status': status,
      'author': author,
      'createdAt': createdAt.toString(),
    };
  }

  Map<String, dynamic> toMapCreate() {
    return {
      'companyName': companyName,
      'specialties': specialties,
      'position': position,
      'timeStartWork': formatTimeToUTC(timeStartWork),
      'timeEndWork': formatTimeToUTC(timeEndWork),
      'description': description,
    };
  }

  factory ExperienceModel.fromMap(Map<String, dynamic> map) {
    return ExperienceModel(
      id: map['_id'] ?? '',
      companyName: map['companyName'] ?? '',
      specialties: List<String>.from(map['specialties']),
      position: map['position'] ?? '',
      timeStartWork: DateTime.parse(map['timeStartWork']).toLocal(),
      timeEndWork: DateTime.parse(map['timeEndWork']).toLocal(),
      description: map['description'] ?? '',
      status: map['status']?.toInt() ?? 0,
      author: map['author'] ?? '',
      createdAt: DateTime.parse(map['createdAt']).toLocal(),
    );
  }

  String toJson() => json.encode(toMap());

  factory ExperienceModel.fromJson(String source) => ExperienceModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'ExperienceModel(id: $id, companyName: $companyName, specialties: $specialties, position: $position, timeStartWork: $timeStartWork, timeEndWork: $timeEndWork, description: $description, status: $status, author: $author, createdAt: $createdAt)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is ExperienceModel &&
        other.id == id &&
        other.companyName == companyName &&
        listEquals(other.specialties, specialties) &&
        other.position == position &&
        other.timeStartWork == timeStartWork &&
        other.timeEndWork == timeEndWork &&
        other.description == description &&
        other.status == status &&
        other.author == author &&
        other.createdAt == createdAt;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        companyName.hashCode ^
        specialties.hashCode ^
        position.hashCode ^
        timeStartWork.hashCode ^
        timeEndWork.hashCode ^
        description.hashCode ^
        status.hashCode ^
        author.hashCode ^
        createdAt.hashCode;
  }

  String get specialtyTitle {
    if (specialties.isEmpty) {
      return '';
    }

    String result = '';

    List<SpecialtyModel> specialtiesModel = AppBloc.requestBloc.specialties;

    int indexOfSpecialty = specialtiesModel.indexWhere(
      (specialty) => specialty.id == specialties.first,
    );

    if (indexOfSpecialty != -1) {
      result = specialtiesModel[indexOfSpecialty].getLocaleTitle ?? '';
    }

    return result;
  }
}
