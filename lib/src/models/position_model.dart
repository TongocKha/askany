import 'dart:convert';

import 'package:askany/src/configs/lang/language_service.dart';
import 'package:askany/src/models/position_details_model.dart';

class PositionModel {
  final String id;
  final int status;
  final PositionDetailsModel? vi;
  final PositionDetailsModel? en;
  final DateTime createdAt;

  PositionModel({
    required this.id,
    required this.status,
    required this.vi,
    required this.en,
    required this.createdAt,
  });

  PositionModel copyWith({
    String? id,
    int? status,
    PositionDetailsModel? vi,
    PositionDetailsModel? en,
    DateTime? createdAt,
  }) {
    return PositionModel(
      id: id ?? this.id,
      status: status ?? this.status,
      vi: vi ?? this.vi,
      en: en ?? this.en,
      createdAt: createdAt ?? this.createdAt,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'status': status,
      'vi': vi?.toMap(),
      'en': en?.toMap(),
      'createdAt': createdAt.toString(),
    };
  }

  factory PositionModel.fromMapSkill() {
    return PositionModel(
        id: '', status: 0, vi: null, en: null, createdAt: DateTime.now());
  }

  factory PositionModel.fromMap(Map<String, dynamic> map) {
    return PositionModel(
      id: map['_id'] ?? '',
      status: map['status']?.toInt() ?? 0,
      vi: map['vi'] == null ? null : PositionDetailsModel.fromMap(map['vi']),
      en: map['en'] == null ? null : PositionDetailsModel.fromMap(map['en']),
      createdAt: map['createdAt'] != null
          ? DateTime.parse(map['createdAt']).toLocal()
          : DateTime.now(),
    );
  }

  String toJson() => json.encode(toMap());

  factory PositionModel.fromJson(String source) =>
      PositionModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'HighestPositionModel(id: $id, status: $status, vi: $vi, en: $en, createdAt: $createdAt)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is PositionModel &&
        other.id == id &&
        other.status == status &&
        other.vi == vi &&
        other.en == en &&
        other.createdAt == createdAt;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        status.hashCode ^
        vi.hashCode ^
        en.hashCode ^
        createdAt.hashCode;
  }

  String? get getLocaleTitle {
    if (LanguageService.getIsLanguage('vi')) {
      if (vi != null) {
        return vi!.name;
      }
    }

    if (en != null) {
      return en!.name;
    }

    return null;
  }
}
