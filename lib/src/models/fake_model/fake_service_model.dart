class FakeServiceModel {
  final String imageUrl;
  final String title;
  final String imageExpert;
  final String description;
  FakeServiceModel({
    required this.imageUrl,
    required this.title,
    required this.imageExpert,
    required this.description,
  });
}

String descriptionfake =
    'Đó là một thắc mắc vô cùng chính xác với nhiều người khi chặn đường của họ du học tới Mỹ là vô cùng dễ dàng và đạt được Visa du lịch Mỹ ngay trong lần phỏng vấn đầu tiên.\nTuy nhiên, với hơn 8 năm trên chặn đường hỗ trợ khách hàng xin Visa các nước phát triển như Mỹ, Úc, Canada, Châu Âu Visa Liên Lục Bảo chúng tôi nhận thấy rằng đã có rất nhiều hồ sơ trượt phỏng vấn hay rớt Visa du lịch Mỹ dù tài chính cực mạnh, hay lịch sử du lịch cực tốt, có chức vụ cao trong xã hội như giám đốc của một doanh nghiệp, xưởng sản xuất kinh doanh.';

List<FakeServiceModel> fakeServices = [
  FakeServiceModel(
    description: descriptionfake,
    imageUrl:
        'https://media.istockphoto.com/photos/friendly-administrator-assisting-woman-at-reception-desk-picture-id1304871477?b=1&k=20&m=1304871477&s=170667a&w=0&h=VvhS7Q91zNGIoxOQX9c9fwQiMF9cEYgR8fMbyx8WAVQ=',
    title: 'Du học Mỹ',
    imageExpert:
        'https://images.unsplash.com/photo-1618245056886-a7fce9bd79cf?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTN8fHVzZXIlMjB3b21lbnxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60',
  ),
  FakeServiceModel(
    description: descriptionfake,
    imageUrl:
        'https://media.istockphoto.com/photos/-picture-id843181596?b=1&k=20&m=843181596&s=170667a&w=0&h=g75r5M8msWBJeV2_7-AzSCy6bYszn6FNSX7ENxLZxhQ=',
    title: 'Dịch vụ SEO',
    imageExpert:
        'https://images.unsplash.com/photo-1618245056886-a7fce9bd79cf?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTN8fHVzZXIlMjB3b21lbnxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60',
  ),
  FakeServiceModel(
    description: descriptionfake,
    imageExpert:
        'https://images.unsplash.com/photo-1618245056886-a7fce9bd79cf?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTN8fHVzZXIlMjB3b21lbnxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60',
    imageUrl:
        'https://images.unsplash.com/photo-1555421689-491a97ff2040?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Nnx8c2VydmljZSUyMHNlb3xlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60',
    title: 'Tư vấn tình cảm',
  ),
  FakeServiceModel(
    description: descriptionfake,
    imageExpert:
        'https://images.unsplash.com/photo-1618245056886-a7fce9bd79cf?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTN8fHVzZXIlMjB3b21lbnxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60',
    imageUrl:
        'https://media.istockphoesk-picture-id1304871477?b=1&k=20&m=1304871477&s=170667a&w=0&h=VvhS7Q91zNGIoxOQX9c9fwQiMF9cEYgR8fMbyx8WAVQ=',
    title: 'Du học Mỹ',
  ),
];

List<FakeServiceModel> fakeServices2 = [
  FakeServiceModel(
    description: descriptionfake,
    imageUrl:
        'https://media.istockphoto.com/photos/friendly-administrator-assisting-woman-at-reception-desk-picture-id1304871477?b=1&k=20&m=1304871477&s=170667a&w=0&h=VvhS7Q91zNGIoxOQX9c9fwQiMF9cEYgR8fMbyx8WAVQ=',
    title: 'Cách xin visa du học Mỹ bao đậu 100%',
    imageExpert:
        'https://images.unsplash.com/photo-1618245056886-a7fce9bd79cf?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTN8fHVzZXIlMjB3b21lbnxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60',
  ),
  FakeServiceModel(
    description: descriptionfake,
    imageUrl:
        'https://media.istockphoto.com/photos/-picture-id843181596?b=1&k=20&m=843181596&s=170667a&w=0&h=g75r5M8msWBJeV2_7-AzSCy6bYszn6FNSX7ENxLZxhQ=',
    title: 'Xin visa du học mới nhất 2022',
    imageExpert:
        'https://images.unsplash.com/photo-1618245056886-a7fce9bd79cf?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTN8fHVzZXIlMjB3b21lbnxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60',
  ),
  FakeServiceModel(
    description: descriptionfake,
    imageExpert:
        'https://images.unsplash.com/photo-1618245056886-a7fce9bd79cf?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTN8fHVzZXIlMjB3b21lbnxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60',
    imageUrl:
        'https://images.unsplash.com/photo-1555421689-491a97ff2040?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Nnx8c2VydmljZSUyMHNlb3xlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60',
    title: 'Mẹo xin visa du học Mỹ bao đậu',
  ),
  FakeServiceModel(
    description: descriptionfake,
    imageExpert:
        'https://images.unsplash.com/photo-1618245056886-a7fce9bd79cf?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTN8fHVzZXIlMjB3b21lbnxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60',
    imageUrl:
        'https://media.istockphoesk-picture-id1304871477?b=1&k=20&m=1304871477&s=170667a&w=0&h=VvhS7Q91zNGIoxOQX9c9fwQiMF9cEYgR8fMbyx8WAVQ=',
    title: 'Xin visa du hcoj Mỹ năm 2022 có những thay đổi gì?',
  ),
];
