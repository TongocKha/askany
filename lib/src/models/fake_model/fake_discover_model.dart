class FakeDiscoverModel {
  final String title;
  final String description;
  final String image;
  final String date;
  final int duration;
  FakeDiscoverModel({
    required this.title,
    required this.description,
    required this.image,
    required this.date,
    required this.duration,
  });
}

String imageDiscover =
    'https://media.istockphoto.com/photos/diverse-coworkers-discuss-strategy-in-board-room-meeting-picture-id1344289506?b=1&k=20&m=1344289506&s=170667a&w=0&h=5XsKMe7xA3vGyzoBu0EVsmonR2tanQpptYdS_nK4SQ8=';

List<FakeDiscoverModel> fakeDiscovers = [
  FakeDiscoverModel(
      duration: 1000,
      title: 'Du lịch Mỹ và những chi phí bạn cần biết',
      description: 'Giáo sư Nguyễn Minh Châu',
      image: imageDiscover,
      date: '25/08/2022'),
  FakeDiscoverModel(
    duration: 1000,
    date: '25/08/2022',
    title: 'Marketing là gì? 10 điều Marketing làm mỗi ngày',
    description: 'Giáo sư Nguyễn Minh Châu',
    image:
        'https://images.unsplash.com/photo-1542781942-fc5121ab1a3a?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8ZGlzY292ZXIlMjBzaW5nYXBvcmV8ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=60',
  ),
  FakeDiscoverModel(
    duration: 1000,
    date: '25/08/2022',
    title: 'Những cảnh đẹp nổi tiếng của Singapore nhất định phải ghé thăm',
    description: 'Giáo sư Đào Hồng Vinh',
    image:
        'https://images.unsplash.com/photo-1472148439583-1f4cf81b80e0?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTN8fGRpc2NvdmVyJTIwc2luZ2Fwb3JlfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60',
  ),
  FakeDiscoverModel(
      duration: 1000,
      title: 'Du lịch Mỹ và những chi phí bạn cần biết',
      description: 'Giáo sư Nguyễn Minh Châu',
      image: 'https://imagespng',
      date: '25/08/2022'),
];
