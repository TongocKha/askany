class FakeAnswerModel {
  final String usernameuser;
  final String imageUser;
  final String descriptionAnswer;
  FakeAnswerModel({
    required this.usernameuser,
    required this.imageUser,
    required this.descriptionAnswer,
  });
}

List<FakeAnswerModel> fakeAnswer = [
  FakeAnswerModel(
    usernameuser: 'Đào Hồng Vinh',
    imageUser:
        'https://images.unsplash.com/photo-1618245056886-a7fce9bd79cf?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTN8fHVzZXIlMjB3b21lbnxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60',
    descriptionAnswer:
        'Cảm ơn bạn vì đã đánh giá tốt, rất vui khi được hỗ trợ bạn!',
  ),
  FakeAnswerModel(
    usernameuser: 'Đào Hồng Vinh',
    imageUser:
        'https://images.unsplash.com/photo-1618245056886-a7fce9bd79cf?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTN8fHVzZXIlMjB3b21lbnxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60',
    descriptionAnswer:
        'Cảm ơn bạn vì đã đánh giá tốt, rất vui khi được hỗ trợ bạn!',
  )
];

class FakeReviewModel {
  final String usernameuser;
  final String imageUser;
  final String descriptionReview;
  final List<FakeAnswerModel> answerReview;
  FakeReviewModel({
    required this.usernameuser,
    required this.imageUser,
    required this.descriptionReview,
    required this.answerReview,
  });
}

List<FakeReviewModel> fakeReview = [
  FakeReviewModel(
    usernameuser: 'Leader Mobile',
    imageUser:
        'https://images.unsplash.com/photo-1507003211169-0a1dd7228f2d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTB8fHVzZXIlMjBtZW58ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=60',
    descriptionReview:
        'Thật tuyệt vời! Mình có rất nhiều thắc mắc cần được giải đáp trước khi nộp hồ sơ đi du học thì những thắc mắc này đều được bạn giải đáp rất rõ ràng và cặn kẽ. Bạn cũng cực kỳ nhiệt tình chỉ cho mình những tips nhỏ khi làm hồ sơ hay xin visa. Mình cảm thấy rất hài lòng và chắc chắn sẽ còn quay lại.',
    answerReview: fakeAnswer,
  ),
  FakeReviewModel(
    usernameuser: 'hongvinhmobile',
    imageUser:
        'https://images.unsplash.com/photo-1507003211169-0a1dd7228f2d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTB8fHVzZXIlMjBtZW58ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=60',
    descriptionReview:
        'Thật tuyệt vời! Mình có rất nhiều thắc mắc cần được giải đáp trước khi nộp hồ sơ đi du học thì những thắc mắc này đều được bạn giải đáp rất rõ ràng và cặn kẽ. Bạn cũng cực kỳ nhiệt tình chỉ cho mình những tips nhỏ khi làm hồ sơ hay xin visa. Mình cảm thấy rất hài lòng và chắc chắn sẽ còn quay lại.',
    answerReview: fakeAnswer,
  ),
  FakeReviewModel(
    usernameuser: 'lambiengcode',
    imageUser:
        'https://images.unsplash.com/photo-1507003211169-0a1dd7228f2d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTB8fHVzZXIlMjBtZW58ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=60',
    descriptionReview:
        'Thật tuyệt vời! Mình có rất nhiều thắc mắc cần được giải đáp trước khi nộp hồ sơ đi du học thì những thắc mắc này đều được bạn giải đáp rất rõ ràng và cặn kẽ. Bạn cũng cực kỳ nhiệt tình chỉ cho mình những tips nhỏ khi làm hồ sơ hay xin visa. Mình cảm thấy rất hài lòng và chắc chắn sẽ còn quay lại.',
    answerReview: fakeAnswer,
  ),
];
