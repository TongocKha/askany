import 'package:askany/src/models/budget_model.dart';

class FakeHotExpertModel {
  final String imageExpert;
  final String nameExpert;
  final String description;
  final BudgetModel price;
  final String phone;
  final String? note;
  final bool pickTime;
  FakeHotExpertModel({
    required this.imageExpert,
    required this.nameExpert,
    required this.description,
    required this.price,
    required this.phone,
    this.note,
    this.pickTime = false,
  });
}

List<FakeHotExpertModel> fakeHotExpert = [
  FakeHotExpertModel(
    imageExpert:
        'https://images.unsplash.com/photo-1618245056886-a7fce9bd79cf?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTN8fHVzZXIlMjB3b21lbnxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60',
    nameExpert: 'Hoàng Oanh',
    description: 'Chuyên viên tư vấn du học',
    price: BudgetModel(
      currency: 'vnd',
      cost: 2000000,
      totalMinutes: 60,
    ),
    phone: '0909078978',
  ),
  FakeHotExpertModel(
    imageExpert:
        'https://images.unsplash.com/photo-1633332755192-727a05c4013d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8dXNlcnxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60',
    nameExpert: 'Ngô Minh Hoàng',
    description: 'Chuyên viên tư vấn du học',
    price: BudgetModel(
      currency: 'vnd',
      cost: 2000000,
      totalMinutes: 60,
    ),
    phone: '0909078978',
  ),
  FakeHotExpertModel(
    imageExpert:
        'https://images.unsplash.com/photo-1570295999919-56ceb5ecca61?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8M3x8dXNlcnxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60',
    nameExpert: 'Đặng Nguyễn Minh Hưng',
    description: 'Chuyên viên tư vấn du học',
    price: BudgetModel(
      currency: 'vnd',
      cost: 2000000,
      totalMinutes: 60,
    ),
    phone: '0909078978',
  ),
  FakeHotExpertModel(
    imageExpert: 'https://imagespng',
    nameExpert: 'Đặng Nguyễn Minh Hưng',
    description: 'Chuyên viên tư vấn du học',
    price: BudgetModel(
      currency: 'vnd',
      cost: 2000000,
      totalMinutes: 60,
    ),
    phone: '0909078978',
  ),
];
