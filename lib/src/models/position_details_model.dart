import 'dart:convert';

class PositionDetailsModel {
  final String? id;
  final String name;
  final String? slug;
  final String? lang;
  final String? position;
  PositionDetailsModel({
    this.id,
    required this.name,
    required this.slug,
    required this.lang,
    required this.position,
  });

  PositionDetailsModel copyWith({
    String? id,
    String? name,
    String? slug,
    String? lang,
    String? position,
  }) {
    return PositionDetailsModel(
      id: id ?? this.id,
      name: name ?? this.name,
      slug: slug ?? this.slug,
      lang: lang ?? this.lang,
      position: position ?? this.position,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'name': name,
      'slug': slug,
      'lang': lang,
      'position': position,
    };
  }

  factory PositionDetailsModel.fromMap(Map<String, dynamic> map) {
    return PositionDetailsModel(
      id: map['_id'],
      name: map['name'] ?? '',
      slug: map['slug'] ?? '',
      lang: map['lang'] ?? '',
      position: map['position'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory PositionDetailsModel.fromJson(String source) =>
      PositionDetailsModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'PositionDetailsModel(id: $id, name: $name, slug: $slug, lang: $lang, position: $position)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is PositionDetailsModel &&
        other.id == id &&
        other.name == name &&
        other.slug == slug &&
        other.lang == lang &&
        other.position == position;
  }

  @override
  int get hashCode {
    return id.hashCode ^ name.hashCode ^ slug.hashCode ^ lang.hashCode ^ position.hashCode;
  }
}
