import 'dart:convert';

import 'package:askany/src/configs/lang/language_service.dart';
import 'package:askany/src/models/avatar_model.dart';
import 'package:askany/src/models/specialty_details_model.dart';

class SpecialtyModel {
  final String id;
  final SpecialtyDetailsModel vi;
  final SpecialtyDetailsModel? en;
  final AvatarModel? thumbnail;
  final String? thubnailSkill;
  SpecialtyModel({
    required this.id,
    required this.vi,
    required this.en,
    required this.thumbnail,
    this.thubnailSkill,
  });

  SpecialtyModel copyWith({
    String? id,
    SpecialtyDetailsModel? vi,
    SpecialtyDetailsModel? en,
    AvatarModel? thumbnail,
  }) {
    return SpecialtyModel(
      id: id ?? this.id,
      vi: vi ?? this.vi,
      en: en ?? this.en,
      thumbnail: thumbnail ?? this.thumbnail,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'vi': vi.toMap(),
      'en': en?.toMap(),
      'thumbnail': thumbnail?.toMap(),
    };
  }

  factory SpecialtyModel.fromMap(Map<String, dynamic> map) {
    return SpecialtyModel(
      id: map['_id'] ?? '',
      vi: SpecialtyDetailsModel.fromMap(map['vi']),
      en: map['en'] == null || map['en'] is String
          ? null
          : SpecialtyDetailsModel.fromMap(map['en']),
      thumbnail: null,
    );
  }
  factory SpecialtyModel.fromMapSkill(Map<String, dynamic> map) {
    return SpecialtyModel(
      id: map['_id'] ?? '',
      vi: SpecialtyDetailsModel.fromMap(map['vi']),
      en: map['en'] == null ? null : SpecialtyDetailsModel.fromMap(map['en']),
      thubnailSkill: null,
      thumbnail: null,
    );
  }

  factory SpecialtyModel.fromMapCategory(Map<String, dynamic> map) {
    return SpecialtyModel(
      id: map['_id'] ?? '',
      vi: SpecialtyDetailsModel(
        id: '',
        name: '',
        slug: '',
        lang: '',
        content: '',
        category: '',
        specialty: '',
      ),
      en: null,
      thubnailSkill: null,
      thumbnail: map['thumbnail'] != null
          ? AvatarModel.fromMap(map['thumbnail'])
          : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory SpecialtyModel.fromJson(String source) =>
      SpecialtyModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'SpecialtyModel(id: $id, vi: $vi, en: $en, thumbnail: $thumbnail)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is SpecialtyModel &&
        other.id == id &&
        other.vi == vi &&
        other.en == en &&
        other.thumbnail == thumbnail;
  }

  @override
  int get hashCode {
    return id.hashCode ^ vi.hashCode ^ en.hashCode ^ thumbnail.hashCode;
  }

  String? get getLocaleTitle {
    if (LanguageService.getIsLanguage('vi')) {
      return vi.name;
    }

    if (en != null) {
      return en!.name;
    }

    return null;
  }
}
