import 'dart:convert';

import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/models/avatar_model.dart';
import 'package:askany/src/models/message_model.dart';
import 'package:flutter/foundation.dart';

import 'package:askany/src/models/account_model.dart';

class ConversationModel {
  final String id;
  String conversationName;
  final List<AccountModel> users;
  final AccountModel expert;
  final List<String> memberFullNames;
  final int status;
  final DateTime createdAt;
  LatestMessage latestMessage;
  final List<String> ignoreNotis;
  ConversationModel({
    required this.id,
    required this.conversationName,
    required this.users,
    required this.expert,
    required this.memberFullNames,
    required this.status,
    required this.createdAt,
    required this.latestMessage,
    required this.ignoreNotis,
  });

  ConversationModel copyWith({
    String? id,
    String? conversationName,
    List<AccountModel>? users,
    AccountModel? expert,
    List<String>? memberFullNames,
    int? status,
    DateTime? createdAt,
    LatestMessage? latestMessage,
    List<String>? ignoreNotis,
  }) {
    return ConversationModel(
      id: id ?? this.id,
      conversationName: conversationName ?? this.conversationName,
      users: users ?? this.users,
      expert: expert ?? this.expert,
      memberFullNames: memberFullNames ?? this.memberFullNames,
      status: status ?? this.status,
      createdAt: createdAt ?? this.createdAt,
      latestMessage: latestMessage ?? this.latestMessage,
      ignoreNotis: ignoreNotis ?? this.ignoreNotis,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'conversationName': conversationName,
      'users': users.map((x) => x.toMap()).toList(),
      'expert': expert.toMap(),
      'memberFullNames': memberFullNames,
      'status': status,
      'createdAt': createdAt.toString(),
      'latestMessage': latestMessage.toMap(),
    };
  }

  factory ConversationModel.fromMap(Map<String, dynamic> map) {
    return ConversationModel(
        id: map['_id'] ?? '',
        conversationName: map['conversationName'] ?? '',
        users: List<AccountModel>.from(map['users']?.map((x) => AccountModel.fromMap(x))),
        expert: AccountModel.fromMap(map['expert']),
        memberFullNames: List<String>.from(map['memberFullNames']),
        status: map['status']?.toInt() ?? 0,
        createdAt: DateTime.parse(map['createdAt'] ?? DateTime.now().toString()).toLocal(),
        latestMessage: map['latestMessage'] != null
            ? LatestMessage.fromMap(map['latestMessage'])
            : defaultLatestMessage,
        ignoreNotis: List<String>.from(map['ignoreNotis'] ?? []));
  }

  String toJson() => json.encode(toMap());

  factory ConversationModel.fromJson(String source) =>
      ConversationModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'ConversationModel(id: $id, conversationName: $conversationName, users: $users, expert: $expert, memberFullNames: $memberFullNames, status: $status, createdAt: $createdAt, latestMessage: $latestMessage)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is ConversationModel &&
        other.id == id &&
        other.conversationName == conversationName &&
        listEquals(other.users, users) &&
        other.expert == expert &&
        listEquals(other.memberFullNames, memberFullNames) &&
        other.status == status &&
        other.createdAt == createdAt &&
        other.latestMessage == latestMessage;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        conversationName.hashCode ^
        users.hashCode ^
        expert.hashCode ^
        memberFullNames.hashCode ^
        status.hashCode ^
        createdAt.hashCode ^
        latestMessage.hashCode;
  }

  String get conversationTitle => conversationName.length != 0 && isGroup
      ? conversationName
      : (UserLocal().getIsExpert()
          ? (users.isEmpty ? 'Askany User' : users.first.fullname!)
          : expert.fullname!);

  List<String?> get avatarConversations {
    return allMembers
        .where((e) => e.id != UserLocal().getUser().id)
        .toList()
        .map((e) => e.avatar?.urlToImage)
        .toList();
  }

  AvatarModel? avatarByUserSender(String userId) {
    int indexOfUser = allMembers.indexWhere((user) => user.id == userId);
    if (indexOfUser != -1) {
      return allMembers[indexOfUser].avatar;
    }

    return null;
  }

  AccountModel? userSender(String userId) {
    int indexOfUser = allMembers.indexWhere((user) => user.id == userId);
    if (indexOfUser != -1) {
      return allMembers[indexOfUser];
    }

    return null;
  }

  AvatarModel? avatarByReceiver() {
    List<AccountModel> filterUsers = allMembers
        .where(
          (user) => user.id != AppBloc.userBloc.getAccount.id,
        )
        .toList();

    if (filterUsers.isNotEmpty) {
      return filterUsers.first.avatar;
    }

    return null;
  }

  List<AccountModel> get allMembers {
    if (!users.contains(expert)) {
      users.add(expert);
    }

    return users;
  }

  AccountModel? get receiverUser {
    List<AccountModel> usersNotMe =
        allMembers.where((user) => user.id != AppBloc.userBloc.getAccount.id).toList();

    if (usersNotMe.isEmpty) {
      return null;
    }

    return allMembers.where((user) => user.id != AppBloc.userBloc.getAccount.id).first;
  }

  bool get isGroup => allMembers.length != 2;
}

class LatestMessage {
  final String id;
  final String expertSender;
  String data;
  final DateTime createdAt;
  int isSeenStatus;
  LatestMessage({
    required this.id,
    required this.expertSender,
    required this.data,
    required this.createdAt,
    this.isSeenStatus = 1,
  });

  LatestMessage copyWith({
    String? id,
    String? expertSender,
    String? data,
    DateTime? createdAt,
  }) {
    return LatestMessage(
      id: id ?? this.id,
      expertSender: expertSender ?? this.expertSender,
      data: data ?? this.data,
      createdAt: createdAt ?? this.createdAt,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'expertSender': expertSender,
      'data': data,
      'createdAt': createdAt.toString(),
    };
  }

  factory LatestMessage.fromMap(Map<String, dynamic> map) {
    bool isDeleted = map['status'] == -1;

    return LatestMessage(
      id: map['_id'] ?? '',
      expertSender: map['expertSender'] ?? map['userSender'] ?? '',
      data: isDeleted ? 'Tin nhắn đã thu hồi' : map['data'] ?? '',
      createdAt: DateTime.parse(map['createdAt']).toLocal(),
      isSeenStatus: map['isSeen'] ?? NOT_SEEN,
    );
  }

  String toJson() => json.encode(toMap());

  factory LatestMessage.fromJson(String source) => LatestMessage.fromMap(json.decode(source));

  factory LatestMessage.fromMessageModel(MessageModel message) => LatestMessage(
        id: message.id,
        data: message.data,
        expertSender: message.userSender,
        createdAt: message.createdAt,
        isSeenStatus: message.status,
      );

  @override
  String toString() {
    return 'LatestMessage(id: $id, expertSender: $expertSender, data: $data, createdAt: $createdAt)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is LatestMessage &&
        other.id == id &&
        other.expertSender == expertSender &&
        other.data == data &&
        other.createdAt == createdAt;
  }

  @override
  int get hashCode {
    return id.hashCode ^ expertSender.hashCode ^ data.hashCode ^ createdAt.hashCode;
  }

  bool get isMe => expertSender == UserLocal().getUser().id;

  bool get isSeen => isMe ? true : isSeenStatus == IS_SEEN;

  set isSeen(bool value) {
    isSeenStatus = value ? IS_SEEN : NOT_SEEN;
  }
}
