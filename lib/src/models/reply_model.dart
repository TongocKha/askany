import 'dart:convert';

class ReplyModel {
  final String id;
  final String content;
  final DateTime createdAt;
  ReplyModel({
    required this.id,
    required this.content,
    required this.createdAt,
  });

  ReplyModel copyWith({
    String? id,
    String? content,
    DateTime? createdAt,
  }) {
    return ReplyModel(
      id: id ?? this.id,
      content: content ?? this.content,
      createdAt: createdAt ?? this.createdAt,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'content': content,
      'createdAt': createdAt.millisecondsSinceEpoch,
    };
  }

  factory ReplyModel.fromMap(Map<String, dynamic> map) {
    return ReplyModel(
      id: map['_id'] ?? '',
      content: map['content'] ?? '',
      createdAt: DateTime.parse(map['createdAt']),
    );
  }

  String toJson() => json.encode(toMap());

  factory ReplyModel.fromJson(String source) =>
      ReplyModel.fromMap(json.decode(source));

  @override
  String toString() =>
      'ReplyModel(id: $id, content: $content, createdAt: $createdAt)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is ReplyModel &&
        other.id == id &&
        other.content == content &&
        other.createdAt == createdAt;
  }

  @override
  int get hashCode => id.hashCode ^ content.hashCode ^ createdAt.hashCode;
}
