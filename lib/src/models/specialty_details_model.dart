import 'dart:convert';

class SpecialtyDetailsModel {
  final String id;
  final String name;
  final String slug;
  final String lang;
  final String content;
  final String category;
  final String specialty;
  SpecialtyDetailsModel({
    required this.id,
    required this.name,
    required this.slug,
    required this.lang,
    required this.content,
    required this.category,
    required this.specialty,
  });

  SpecialtyDetailsModel copyWith({
    String? id,
    String? name,
    String? slug,
    String? lang,
    String? content,
    String? category,
    String? specialty,
  }) {
    return SpecialtyDetailsModel(
      id: id ?? this.id,
      name: name ?? this.name,
      slug: slug ?? this.slug,
      lang: lang ?? this.lang,
      content: content ?? this.content,
      category: category ?? this.category,
      specialty: specialty ?? this.specialty,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'name': name,
      'slug': slug,
      'lang': lang,
      'content': content,
      'category': category,
      'specialty': specialty,
    };
  }

  factory SpecialtyDetailsModel.fromMap(Map<String, dynamic> map) {
    return SpecialtyDetailsModel(
      id: map['_id'] ?? '',
      name: map['name'] ?? '',
      slug: map['slug'] ?? '',
      lang: map['lang'] ?? '',
      content: map['content'] ?? '',
      category: map['category'] ?? '',
      specialty: map['specialty'] ?? '',
    );
  }
  String toJson() => json.encode(toMap());

  factory SpecialtyDetailsModel.fromJson(String source) =>
      SpecialtyDetailsModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'SpecialtyDetailsModel(id: $id, name: $name, slug: $slug, lang: $lang, content: $content, category: $category, specialty: $specialty)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is SpecialtyDetailsModel &&
        other.id == id &&
        other.name == name &&
        other.slug == slug &&
        other.lang == lang &&
        other.content == content &&
        other.category == category &&
        other.specialty == specialty;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        name.hashCode ^
        slug.hashCode ^
        lang.hashCode ^
        content.hashCode ^
        category.hashCode ^
        specialty.hashCode;
  }
}
