import 'dart:convert';

import 'package:askany/src/configs/lang/language_service.dart';

class ReasonModel {
  late final String id;
  final String name;
  ReasonModel({
    required this.id,
    required this.name,
  });

  ReasonModel copyWith({
    String? id,
    String? name,
  }) {
    return ReasonModel(
      id: id ?? this.id,
      name: name ?? this.name,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'name': name,
    };
  }

  factory ReasonModel.fromMap(Map<String, dynamic> map) {
    return ReasonModel(
      id: map['_id'] ?? '',
      name: LanguageService.getIsLanguage('vi')
          ? map['vi']['name']
          : (map['en'] != null ? map['en']['name'] : ''),
    );
  }

  factory ReasonModel.fromMapLanguage(Map<String, dynamic> map) {
    return ReasonModel(
      id: map['_id'] ?? '',
      name: map['name'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory ReasonModel.fromJson(String source) =>
      ReasonModel.fromMap(json.decode(source));

  @override
  String toString() => 'ReasonModel(id: $id, name: $name)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is ReasonModel && other.id == id && other.name == name;
  }

  @override
  int get hashCode => id.hashCode ^ name.hashCode;
}
