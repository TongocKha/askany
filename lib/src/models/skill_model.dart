import 'dart:convert';

import 'package:askany/src/models/expert_model.dart';
import 'package:askany/src/models/specialty_details_model.dart';
import 'package:askany/src/models/specialty_model.dart';
import 'package:flutter/foundation.dart';

import 'package:askany/src/models/author_model.dart';
import 'package:askany/src/models/budget_model.dart';
import 'package:askany/src/models/position_model.dart';

class SkillModel {
  String? id;
  final int status;
  String description;
  String name;
  final List<String> keywords;
  String companyName;
  int expExperience;
  BudgetModel? meetPrice;
  BudgetModel? callPrice;
  PositionModel? highestPosition;
  SpecialtyModel? specialty;
  final AuthorModel? author;
  final DateTime? createdAt;
  double? callPriceCost;
  double? meetPriceCost;
  final double? stars;
  final int? ratingCount;
  final String? expertId;
  final ExpertModel? expertModel;

  SkillModel({
    this.id,
    required this.status,
    required this.description,
    required this.name,
    required this.keywords,
    required this.companyName,
    required this.expExperience,
    required this.meetPrice,
    required this.callPrice,
    required this.highestPosition,
    required this.specialty,
    this.author,
    this.createdAt,
    this.callPriceCost,
    this.meetPriceCost,
    this.ratingCount,
    this.stars,
    this.expertId,
    this.expertModel,
  });

  SkillModel copyWith({
    String? id,
    int? status,
    String? description,
    String? name,
    List<String>? keywords,
    String? companyName,
    int? expExperience,
    BudgetModel? meetPrice,
    BudgetModel? callPrice,
    PositionModel? highestPosition,
    SpecialtyModel? specialty,
    AuthorModel? author,
    DateTime? createdAt,
    double? callPriceCost,
    double? meetPriceCost,
    int? ratingCount,
    double? stars,
  }) {
    return SkillModel(
      id: id ?? this.id,
      status: status ?? this.status,
      description: description ?? this.description,
      name: name ?? this.name,
      keywords: keywords ?? this.keywords,
      companyName: companyName ?? this.companyName,
      expExperience: expExperience ?? this.expExperience,
      meetPrice: meetPrice ?? this.meetPrice,
      callPrice: callPrice ?? this.callPrice,
      highestPosition: highestPosition ?? this.highestPosition,
      specialty: specialty ?? this.specialty,
      author: author ?? this.author,
      createdAt: createdAt ?? this.createdAt,
      callPriceCost: callPriceCost ?? this.callPriceCost,
      meetPriceCost: meetPriceCost ?? this.meetPriceCost,
      ratingCount: ratingCount ?? this.ratingCount,
      stars: stars ?? this.stars,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'status': status,
      'description': description,
      'name': name,
      'keywords': keywords,
      'companyName': companyName,
      'expExperience': expExperience,
      'meetPrice': meetPrice?.toMap(),
      'callPrice': callPrice?.toMap(),
      'highestPosition': highestPosition?.toMap(),
      'specialty': specialty?.toMap(),
      'author': author?.toMap(),
      'createdAt': createdAt.toString(),
      'callPriceCost': callPriceCost,
      'meetPriceCost': meetPriceCost,
      'ratingCount': ratingCount,
      'stars': stars,
    };
  }

  Map<String, dynamic> toMapSearch() {
    return {
      '_id': id,
      'status': status,
      'description': description,
      'name': name,
      'keywords': keywords,
      'companyName': companyName,
      'expExperience': expExperience,
      'meetPrice': meetPrice?.toMap(),
      'callPrice': callPrice?.toMap(),
      'highestPosition': highestPosition?.toMap(),
      'specialty': specialty?.toMap(),
      'author': author?.toMap(),
      'createdAt': createdAt.toString(),
      'callPriceCost': callPriceCost,
      'meetPriceCost': meetPriceCost,
      'ratingCount': ratingCount,
      'stars': stars,
    };
  }

  factory SkillModel.fromMap(Map<String, dynamic> map) {
    return SkillModel(
      id: map['_id'],
      status: map['status']?.toInt() ?? 0,
      description: map['description'] ?? '',
      name: map['name'] ?? '',
      keywords: List<String>.from(map['keywords']),
      companyName: map['companyName'] ?? '',
      expExperience: map['expExperience']?.toInt() ?? 0,
      meetPrice: map['meetPrice'] != null
          ? BudgetModel.fromMap(map['meetPrice'])
          : null,
      callPrice: map['callPrice'] != null
          ? BudgetModel.fromMap(map['callPrice'])
          : null,
      highestPosition: map['highestPosition'] != null
          ? PositionModel.fromMap(map['highestPosition'])
          : null,
      specialty: map['specialty'] != null
          ? SpecialtyModel.fromMapSkill(map['specialty'])
          : null,
      // author: map['author'] != null ? AuthorModel.fromMap(map['author']) : null,
      createdAt: DateTime.parse(map['createdAt']),
      callPriceCost: double.parse(((map['callPriceCost'] ?? 0).toString())),
      meetPriceCost: double.parse(((map['meetPriceCost'] ?? 0).toString())),
      ratingCount: map['ratingCount']?.toInt() ?? 0,
      stars: double.parse(((map['stars'] ?? 0).toString())),
    );
  }

  factory SkillModel.fromMapSearch(Map<String, dynamic> map) {
    return SkillModel(
      id: map['_id'],
      status: map['status']?.toInt() ?? 0,
      description: map['description'] ?? '',
      name: map['name'] ?? '',
      keywords: List<String>.from(map['keywords']),
      companyName: map['companyName'] ?? '',
      expExperience: map['expExperience']?.toInt() ?? 0,
      meetPrice: map['meetPrice'] != null
          ? BudgetModel.fromMap(map['meetPrice'])
          : null,
      callPrice: map['callPrice'] != null
          ? BudgetModel.fromMap(map['callPrice'])
          : null,
      highestPosition: map['highestPosition'] != null
          ? PositionModel.fromMap(map['highestPosition'])
          : null,
      specialty: null,
      author: map['author'] != null ? AuthorModel.fromMap(map['author']) : null,
      createdAt: DateTime.parse(map['createdAt']),
      callPriceCost: double.parse(((map['callPriceCost'] ?? 0).toString())),
      meetPriceCost: double.parse(((map['meetPriceCost'] ?? 0).toString())),
      ratingCount: map['ratingCount']?.toInt() ?? 0,
      stars: double.parse(((map['stars'] ?? 0).toString())),
    );
  }

  factory SkillModel.fromMapInfoExpert(Map<String, dynamic> map) {
    return SkillModel(
      id: map['_id'],
      status: map['status']?.toInt() ?? 0,
      description: map['description'] ?? '',
      name: map['name'] ?? '',
      keywords: List<String>.from(map['keywords']),
      companyName: map['companyName'] ?? '',
      expExperience: map['expExperience']?.toInt() ?? 0,
      meetPrice: map['meetPrice'] != null
          ? BudgetModel.fromMap(map['meetPrice'])
          : null,
      callPrice: map['callPrice'] != null
          ? BudgetModel.fromMap(map['callPrice'])
          : null,
      highestPosition: map['highestPosition'] != null
          ? PositionModel.fromMap(map['highestPosition'])
          : null,
      specialty: null,
      author: null,
      createdAt: DateTime.parse(map['createdAt']),
      callPriceCost: double.parse(((map['callPriceCost'] ?? 0).toString())),
      meetPriceCost: double.parse(((map['meetPriceCost'] ?? 0).toString())),
      ratingCount: map['ratingCount']?.toInt() ?? 0,
      stars: double.parse(((map['stars'] ?? 0).toString())),
      expertId: map['author'] == null
          ? null
          : map['author'] is String
              ? map['author']
              : map['author']['_id'],
    );
  }

  factory SkillModel.fromMapInfoSkill(Map<String, dynamic> map) {
    return SkillModel(
        id: map['_id'],
        status: map['status']?.toInt() ?? 0,
        description: map['description'] ?? '',
        name: map['name'] ?? '',
        keywords: List<String>.from(map['keywords']),
        companyName: map['companyName'] ?? '',
        expExperience: map['expExperience']?.toInt() ?? 0,
        meetPrice: map['meetPrice'] != null
            ? BudgetModel.fromMap(map['meetPrice'])
            : null,
        callPrice: map['callPrice'] != null
            ? BudgetModel.fromMap(map['callPrice'])
            : null,
        highestPosition: map['highestPosition'] != null
            ? PositionModel.fromMap(map['highestPosition'])
            : null,
        specialty: null,
        author: null,
        createdAt: DateTime.parse(map['createdAt']),
        callPriceCost: double.parse(((map['callPriceCost'] ?? 0).toString())),
        meetPriceCost: double.parse(((map['meetPriceCost'] ?? 0).toString())),
        ratingCount: map['ratingCount']?.toInt() ?? 0,
        stars: double.parse(((map['stars'] ?? 0).toString())),
        expertId: null,
        expertModel: map['author'] == null
            ? null
            : ExpertModel.fromMapInfoExpert(map['author']));
  }

  String toJson() => json.encode(toMap());

  factory SkillModel.fromJson(String source) =>
      SkillModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'SkillModel(id: $id, status: $status, description: $description, name: $name, keywords: $keywords, companyName: $companyName, expExperience: $expExperience, meetPrice: $meetPrice, highestPosition: $highestPosition, specialty: $specialty, author: $author, createdAt: $createdAt)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is SkillModel &&
        other.id == id &&
        other.status == status &&
        other.description == description &&
        other.name == name &&
        listEquals(other.keywords, keywords) &&
        other.companyName == companyName &&
        other.expExperience == expExperience &&
        other.meetPrice == meetPrice &&
        other.highestPosition == highestPosition &&
        other.specialty == specialty &&
        other.author == author &&
        other.createdAt == createdAt;
  }

  Map<String, dynamic> toMapForCreate() {
    return {
      // '_id': id,
      // 'status': status,
      'callPrice': callPrice?.toJsonSkill(),
      'meetPrice': meetPrice?.toJsonSkill(),
      'name': name,
      'keywords': keywords,
      'companyName': companyName,
      'expExperience': expExperience,
      'description': description,
      'highestPosition': highestPosition?.id,
      'specialty': specialty?.id,
      // 'author': author?.toMap(),
      // 'createdAt': createdAt.toString(),
    };
  }

  factory SkillModel.fromCreateRequest(Map<String, dynamic> map) {
    return SkillModel(
      id: map['_id'],
      status: map['status']?.toInt() ?? 0,
      description: map['description'] ?? '',
      name: map['name'] ?? '',
      keywords: List<String>.from(map['keywords']),
      companyName: map['companyName'] ?? '',
      expExperience: map['expExperience']?.toInt() ?? 0,
      meetPrice: BudgetModel.fromMapSkill(),
      callPrice: BudgetModel.fromMapSkill(),
      highestPosition: PositionModel.fromMapSkill(),
      callPriceCost: double.parse(((map['callPriceCost'] ?? 0).toString())),
      meetPriceCost: double.parse(((map['meetPriceCost'] ?? 0).toString())),
      specialty: SpecialtyModel(
        id: map['specialty'] ?? '',
        vi: SpecialtyDetailsModel(
            id: '',
            specialty: '',
            name: '',
            category: '',
            content: '',
            lang: '',
            slug: ''),
        en: SpecialtyDetailsModel(
            id: '',
            specialty: '',
            name: '',
            category: '',
            content: '',
            lang: '',
            slug: ''),
        thubnailSkill: '',
        thumbnail: null,
      ),
      // author: map['author'] != null ? AuthorModel.fromMap(map['author']) : null,
      // createdAt: DateTime.parse(map['createdAt']),
    );
  }

  @override
  int get hashCode {
    return id.hashCode ^
        status.hashCode ^
        description.hashCode ^
        name.hashCode ^
        keywords.hashCode ^
        companyName.hashCode ^
        expExperience.hashCode ^
        meetPrice.hashCode ^
        highestPosition.hashCode ^
        specialty.hashCode ^
        author.hashCode ^
        callPriceCost.hashCode ^
        meetPriceCost.hashCode ^
        ratingCount.hashCode ^
        stars.hashCode ^
        createdAt.hashCode;
  }
}
