import 'dart:convert';

import 'package:askany/src/models/avatar_model.dart';
import 'package:askany/src/models/budget_model.dart';
import 'package:askany/src/models/position_model.dart';

class AuthorModel {
  final String id;
  final String fullname;
  final AvatarModel? avatar;
  final int province;
  AuthorModel({
    required this.id,
    required this.fullname,
    this.avatar,
    required this.province,
  });

  AuthorModel copyWith({
    String? id,
    String? fullname,
    AvatarModel? avatar,
    int? province,
  }) {
    return AuthorModel(
      id: id ?? this.id,
      fullname: fullname ?? this.fullname,
      avatar: avatar ?? this.avatar,
      province: province ?? this.province,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'fullname': fullname,
      'avatar': avatar?.toMap(),
      'province': province,
    };
  }

  factory AuthorModel.fromMap(Map<String, dynamic> map) {
    return AuthorModel(
      id: map['_id'] ?? '',
      fullname: map['fullname'] ?? '',
      avatar: map['avatar'] != null ? AvatarModel.fromMap(map['avatar']) : null,
      province: map['province']?.toInt() ?? 0,
    );
  }

  String toJson() => json.encode(toMap());

  factory AuthorModel.fromJson(String source) =>
      AuthorModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'AuthorModel(id: $id, fullname: $fullname, avatar: $avatar, province: $province)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is AuthorModel &&
        other.id == id &&
        other.fullname == fullname &&
        other.avatar == avatar &&
        other.province == province;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        fullname.hashCode ^
        avatar.hashCode ^
        province.hashCode;
  }
}

class AuthorExpertModel {
  final String id;
  final String fullname;
  final int province;
  final AvatarModel? avatar;
  final int ratingCounts;
  final double stars;
  final String? companyName;
  final double? experienceYears;
  final PositionModel? highestPosition;
  final String? introduce;
  final BudgetModel? callPrice;
  final BudgetModel? meetPrice;

  AuthorExpertModel({
    required this.id,
    required this.fullname,
    required this.province,
    this.avatar,
    this.ratingCounts = 0,
    this.stars = 0.0,
    this.companyName,
    this.experienceYears,
    this.highestPosition,
    this.introduce,
    this.callPrice,
    this.meetPrice,
  });

  AuthorExpertModel copyWith({
    String? id,
    String? fullname,
    int? province,
    AvatarModel? avatar,
    int? ratingCounts,
    double? stars,
    String? companyName,
    double? experienceYears,
    PositionModel? highestPosition,
    String? introduce,
    BudgetModel? callPrice,
    BudgetModel? meetPrice,
  }) {
    return AuthorExpertModel(
      id: id ?? this.id,
      fullname: fullname ?? this.fullname,
      province: province ?? this.province,
      avatar: avatar ?? this.avatar,
      ratingCounts: ratingCounts ?? this.ratingCounts,
      stars: stars ?? this.stars,
      companyName: companyName ?? this.companyName,
      experienceYears: experienceYears ?? this.experienceYears,
      highestPosition: highestPosition ?? this.highestPosition,
      introduce: introduce ?? this.introduce,
      callPrice: callPrice ?? this.callPrice,
      meetPrice: meetPrice ?? this.meetPrice,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'fullname': fullname,
      'province': province,
      'avatar': avatar?.toMap(),
    };
  }

  factory AuthorExpertModel.fromMap(Map<String, dynamic> map) {
    return AuthorExpertModel(
      id: map['_id'] ?? '',
      fullname: map['fullname'] ?? '',
      province: map['province']?.toInt() ?? 0,
      avatar: map['avatar'] != null
          ? AvatarModel.fromMap(map['avatar'])
          : AvatarModel(
              name: '',
              src: '',
            ),
      ratingCounts: map['ratingCount']?.toInt() ?? 0,
      stars: double.parse((map['stars'] ?? 0.0).toString()),
    );
  }

  factory AuthorExpertModel.fromMapCategoryExpert(Map<String, dynamic> map) {
    return AuthorExpertModel(
      id: map['_id'] ?? '',
      fullname: map['fullname'] ?? '',
      province: map['province']?.toInt() ?? 0,
      avatar: map['avatar'] != null
          ? AvatarModel.fromMap(map['avatar'])
          : AvatarModel(
              name: '',
              src: '',
            ),
      ratingCounts: map['ratingCount']?.toInt() ?? 0,
      stars: double.parse(
        (map['stars'] ?? 0.0).toString(),
      ),
      companyName: map['companyName'] ?? '',
      experienceYears: double.parse(((map['experienceYears'] ?? 0).toString())),
      highestPosition: map['highestPosition'] != null
          ? PositionModel.fromMap(map['highestPosition'])
          : null,
      introduce: map['introduce'] ?? '',
      meetPrice: map['meetPrice'] != null
          ? BudgetModel.fromMap(map['meetPrice'])
          : null,
      callPrice: map['callPrice'] != null
          ? BudgetModel.fromMap(map['callPrice'])
          : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory AuthorExpertModel.fromJson(String source) =>
      AuthorExpertModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'AuthorExpertModel(id: $id, fullname: $fullname, province: $province, avatar: $avatar)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is AuthorExpertModel &&
        other.id == id &&
        other.fullname == fullname &&
        other.province == province &&
        other.avatar == avatar;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        fullname.hashCode ^
        province.hashCode ^
        avatar.hashCode;
  }
}
