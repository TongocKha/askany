import 'dart:convert';

import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/models/specialty_model.dart';
import 'package:flutter/foundation.dart';

class WorkingExperience {
  final String id;
  final String companyName;
  final List<SpecialtyModel>? specialties;
  final String? position;
  final DateTime? timeStartWork;
  final DateTime? timeEndWork;
  final String description;
  final double? status;
  final String author;
  final DateTime? createdAt;
  final DateTime? modifiedAt;
  WorkingExperience({
    required this.id,
    required this.companyName,
    this.specialties,
    this.position,
    this.timeStartWork,
    this.timeEndWork,
    required this.description,
    required this.status,
    required this.author,
    this.createdAt,
    this.modifiedAt,
  });

  WorkingExperience copyWith({
    String? id,
    String? companyName,
    List<SpecialtyModel>? specialties,
    String? position,
    DateTime? timeStartWork,
    DateTime? timeEndWork,
    String? description,
    double? status,
    String? author,
    DateTime? createdAt,
    DateTime? modifiedAt,
  }) {
    return WorkingExperience(
      id: id ?? this.id,
      companyName: companyName ?? this.companyName,
      specialties: specialties ?? this.specialties,
      position: position ?? this.position,
      timeStartWork: timeStartWork ?? this.timeStartWork,
      timeEndWork: timeEndWork ?? this.timeEndWork,
      description: description ?? this.description,
      status: status ?? this.status,
      author: author ?? this.author,
      createdAt: createdAt ?? this.createdAt,
      modifiedAt: modifiedAt ?? this.modifiedAt,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'companyName': companyName,
      'specialties': specialties,
      'positionModel': position,
      'timeStartWork': timeStartWork?.millisecondsSinceEpoch,
      'timeEndWork': timeEndWork?.millisecondsSinceEpoch,
      'description': description,
      'status': status,
      'author': author,
      'createdAt': createdAt?.millisecondsSinceEpoch,
      'modifiedAt': modifiedAt?.millisecondsSinceEpoch,
    };
  }

  factory WorkingExperience.fromMap(Map<String, dynamic> map) {
    return WorkingExperience(
      id: map['_id'] ?? '',
      companyName: map['companyName'] ?? '',
      specialties:
          ((map['specialties'] ?? []) as List).map((item) => SpecialtyModel.fromMap(item)).toList(),
      position: map['positionModel'] ?? '',
      timeStartWork: map['timeStartWork'] != null ? DateTime.parse(map['createdAt']) : null,
      timeEndWork: map['timeEndWork'] != null ? DateTime.parse(map['createdAt']) : null,
      description: map['description'] ?? '',
      status: double.parse(((map['status'] ?? 0).toString())),
      author: map['author'] ?? '',
      createdAt: map['createdAt'] != null ? DateTime.parse(map['createdAt']) : null,
      modifiedAt: map['modifiedAt'] != null ? DateTime.parse(map['createdAt']) : null,
    );
  }
  factory WorkingExperience.fromMapInfoExpert(Map<String, dynamic> map) {
    return WorkingExperience(
      id: map['_id'] ?? '',
      companyName: map['companyName'] ?? '',
      specialties: null,
      position: map['positionModel'] ?? '',
      timeStartWork: map['timeStartWork'] != null ? DateTime.parse(map['createdAt']) : null,
      timeEndWork: map['timeEndWork'] != null ? DateTime.parse(map['createdAt']) : null,
      description: map['description'] ?? '',
      status: double.parse(((map['status'] ?? 0).toString())),
      author: map['author'] ?? '',
      createdAt: map['createdAt'] != null ? DateTime.parse(map['createdAt']) : null,
      modifiedAt: map['modifiedAt'] != null ? DateTime.parse(map['createdAt']) : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory WorkingExperience.fromJson(String source) =>
      WorkingExperience.fromMap(json.decode(source));

  @override
  String toString() {
    return 'WorkingExperience(id: $id, companyName: $companyName, specialties: $specialties, positionModel: $position, timeStartWork: $timeStartWork, timeEndWork: $timeEndWork, description: $description, status: $status, author: $author, createdAt: $createdAt, modifiedAt: $modifiedAt)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is WorkingExperience &&
        other.id == id &&
        other.companyName == companyName &&
        listEquals(other.specialties, specialties) &&
        other.position == position &&
        other.timeStartWork == timeStartWork &&
        other.timeEndWork == timeEndWork &&
        other.description == description &&
        other.status == status &&
        other.author == author &&
        other.createdAt == createdAt &&
        other.modifiedAt == modifiedAt;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        companyName.hashCode ^
        specialties.hashCode ^
        position.hashCode ^
        timeStartWork.hashCode ^
        timeEndWork.hashCode ^
        description.hashCode ^
        status.hashCode ^
        author.hashCode ^
        createdAt.hashCode ^
        modifiedAt.hashCode;
  }

  String get specialtyTitle {
    if (specialties!.isEmpty) {
      return '';
    }

    String result = '';

    List<SpecialtyModel> specialtiesModel = AppBloc.requestBloc.specialties;

    int indexOfSpecialty = specialtiesModel.indexWhere(
      (specialty) => specialty.id == specialties!.first.id,
    );

    if (indexOfSpecialty != -1) {
      result = specialtiesModel[indexOfSpecialty].getLocaleTitle ?? '';
    }

    return result;
  }
}
