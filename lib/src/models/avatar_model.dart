import 'dart:convert';

import 'package:askany/src/configs/application.dart';

class AvatarModel {
  final String? id;
  final String name;
  final String src;
  AvatarModel({
    this.id,
    required this.name,
    required this.src,
  });

  AvatarModel copyWith({
    String? id,
    String? name,
    String? src,
  }) {
    return AvatarModel(
      id: id ?? this.id,
      name: name ?? this.name,
      src: src ?? this.src,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'src': src,
    };
  }

  factory AvatarModel.fromMap(Map<String, dynamic> map) {
    return AvatarModel(
      id: map['_id'],
      name: map['name'] ?? '',
      src: map['src'] ?? '',
    );
  }

  factory AvatarModel.fromName(String name) {
    return AvatarModel(
      id: '',
      name: name,
      src: 'users',
    );
  }

  String toJson() => json.encode(toMap());

  factory AvatarModel.fromJson(String source) =>
      AvatarModel.fromMap(json.decode(source));

  @override
  String toString() => 'AvatarModel(id: $id, name: $name, src: $src)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is AvatarModel &&
        other.id == id &&
        other.name == name &&
        other.src == src;
  }

  @override
  int get hashCode => id.hashCode ^ name.hashCode ^ src.hashCode;

  String get urlToImage => Application.baseUrl + '$src/' + name;
}
