import 'dart:convert';
import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/helpers/date_time_helper.dart';
import 'package:askany/src/models/author_model.dart';
import 'package:askany/src/models/budget_model.dart';
import 'package:askany/src/models/cancel_service_model.dart';
import 'package:askany/src/models/offer_model.dart';
import 'package:askany/src/models/report_model.dart';
import 'package:askany/src/models/specialty_model.dart';
import 'package:askany/src/models/service_feedback_model.dart';

class RequestModel {
  String? id;
  AuthorModel? authorUser;
  AuthorExpertModel? authorExpert;
  final String title;
  final String? slug;
  final String content;
  final String specialty;
  final SpecialtyModel? specialtyModel;
  BudgetModel? budget;
  BudgetModel? price;
  final String contactForm;
  final String locationName;
  final String locationAddress;
  int? status;
  final int? type;
  final DateTime dateExpired;
  DateTime? startTime;
  DateTime? endTime;
  DateTime? createdAt;
  DateTime? askanyPaymentTime;
  DateTime? confirmedTime;
  DateTime? completedTime;
  final int participantsCount;
  final List<OfferModel> offerers;
  List<OfferModel> selectedOffers;
  String? myOfferId;
  CancelModel? cancel;
  RatingModel? rating;
  ReportModel? report;
  String? cancelledBy;
  bool? isPickTime;
  RequestModel({
    this.id,
    this.authorUser,
    required this.title,
    this.slug,
    required this.content,
    required this.specialty,
    this.budget,
    required this.contactForm,
    required this.locationName,
    required this.locationAddress,
    this.status,
    this.type,
    required this.dateExpired,
    this.startTime,
    this.endTime,
    this.createdAt,
    this.specialtyModel,
    required this.offerers,
    required this.participantsCount,
    required this.selectedOffers,
    this.myOfferId,
    this.cancel,
    this.rating,
    this.report,
    this.isPickTime,
    this.cancelledBy,
  });

  RequestModel copyWith({
    String? id,
    AuthorModel? authorModel,
    String? title,
    String? slug,
    String? content,
    String? specialty,
    BudgetModel? budget,
    String? contactForm,
    String? locationName,
    String? locationAddress,
    int? status,
    int? type,
    DateTime? dateExpired,
    DateTime? startTime,
    DateTime? endTime,
    DateTime? createdAt,
    List? offers,
    int? participantsCount,
    SpecialtyModel? specialtyModel,
    List<OfferModel>? selectedOffers,
  }) {
    return RequestModel(
      id: id ?? this.id,
      authorUser: authorModel ?? this.authorUser,
      title: title ?? this.title,
      slug: slug ?? this.slug,
      content: content ?? this.content,
      specialty: specialty ?? this.specialty,
      budget: budget ?? this.budget,
      contactForm: contactForm ?? this.contactForm,
      locationName: locationName ?? this.locationName,
      locationAddress: locationAddress ?? this.locationAddress,
      status: status ?? this.status,
      type: type ?? this.type,
      dateExpired: dateExpired ?? this.dateExpired,
      startTime: startTime ?? this.startTime,
      endTime: endTime ?? this.endTime,
      createdAt: createdAt ?? this.createdAt,
      offerers: offerers,
      participantsCount: participantsCount ?? this.participantsCount,
      specialtyModel: specialtyModel ?? this.specialtyModel,
      selectedOffers: selectedOffers ?? this.selectedOffers,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'authorUser': authorUser!.toMap(),
      'title': title,
      'slug': slug,
      'content': content,
      'specialty': specialty,
      'budget': budget!.toMap(),
      'contactForm': contactForm,
      'locationName': locationName,
      'locationAddress': locationAddress,
      'status': status,
      'type': type,
      'dateExpired': dateExpired.toString(),
      'startTime': startTime!.toString(),
      'endTime': endTime!.toString(),
      'createdAt': createdAt!.toString(),
    };
  }

  Map<String, dynamic> toMapForCreate() {
    return {
      'title': title,
      'content': content,
      'specialty': specialty,
      'budget': budget!.toJsonSkill(),
      'contactForm': contactForm,
      'locationName': locationName,
      'locationAddress': locationAddress,
      'dateExpired': formatTimeToUTC(dateExpired),
      'participantsCount': participantsCount.toString(),
    };
  }

  Map<String, dynamic> toMapForUpdateSkill() {
    return {
      'budget': budget,
      'locationName': locationName,
      'locationAddress': locationAddress,
      'participantsCount': participantsCount.toString(),
      'startTime': startTime,
      'endTime': endTime,
    };
  }

  factory RequestModel.fromMap(Map<String, dynamic> map) {
    final DateTime dateExpired =
        DateTime.parse(map['dateExpired'] ?? DATE_TIME_DEFAULT).toLocal();
    final int statusRaw = map['status']?.toInt() ?? 0;
    final int status =
        statusRaw == REQUEST_HAPPENING && dateExpired.isBefore(DateTime.now())
            ? REQUEST_FINISHED
            : statusRaw;

    return RequestModel(
      id: map['_id'] ?? '',
      authorUser: AuthorModel.fromMap(map['authorUser']),
      title: map['title'] ?? '',
      slug: map['slug'] ?? '',
      content: map['content'] ?? '',
      specialty: map['specialty']?['_id'] ?? '',
      budget: BudgetModel.fromMap(map['budget']),
      contactForm: map['contactForm'] ?? '',
      locationName: map['locationName'] ?? '',
      locationAddress: map['locationAddress'] ?? '',
      status: status,
      type: map['type']?.toInt() ?? 0,
      dateExpired: dateExpired,
      startTime:
          DateTime.parse(map['startTime'] ?? DATE_TIME_DEFAULT).toLocal(),
      endTime: DateTime.parse(map['endTime'] ?? DATE_TIME_DEFAULT).toLocal(),
      createdAt:
          DateTime.parse(map['createdAt'] ?? DATE_TIME_DEFAULT).toLocal(),
      specialtyModel:
          map['specialty'] == null || map['specialty']?['vi'] == null
              ? null
              : SpecialtyModel.fromMap(map['specialty']),
      offerers: (map['offerers'] as List<dynamic>)
          .map((offer) => OfferModel.fromMap(offer))
          .toList(),
      participantsCount: map['participantsCount'] ?? PARTICIPANTS_COUNT,
      selectedOffers: map['selectedOffers'] == null
          ? []
          : (map['selectedOffers'] as List<dynamic>)
              .where((offer) => offer['authorExpert'] != null)
              .map((offer) => OfferModel.fromMap(offer))
              .toList(),
    );
  }

  factory RequestModel.fromCreateRequest(Map<String, dynamic> map) {
    return RequestModel(
      id: map['_id'] ?? '',
      authorUser: null,
      title: map['title'] ?? '',
      slug: map['slug'] ?? '',
      content: map['content'] ?? '',
      specialty: map['specialty'] ?? '',
      budget: null,
      contactForm: map['contactForm'] ?? '',
      locationName: map['locationName'] ?? '',
      locationAddress: map['locationAddress'] ?? '',
      status: map['status']?.toInt() ?? 0,
      type: map['type']?.toInt() ?? 0,
      dateExpired:
          DateTime.parse(map['dateExpired'] ?? DATE_TIME_DEFAULT).toLocal(),
      startTime:
          DateTime.parse(map['startTime'] ?? DATE_TIME_DEFAULT).toLocal(),
      endTime: DateTime.parse(map['endTime'] ?? DATE_TIME_DEFAULT).toLocal(),
      createdAt:
          DateTime.parse(map['createdAt'] ?? DATE_TIME_DEFAULT).toLocal(),
      offerers: [],
      participantsCount: map['participantsCount'] ?? PARTICIPANTS_COUNT,
      selectedOffers: [],
    );
  }

  factory RequestModel.fromCreateRequestBySkill(Map<String, dynamic> map) {
    return RequestModel(
      id: map['_id'] ?? '',
      authorUser: null,
      title: map['title'] ?? '',
      slug: map['slug'] ?? '',
      content: map['content'] ?? '',
      specialty: map['specialty'] ?? '',
      budget: null,
      contactForm: map['contactForm'] ?? '',
      locationName: map['locationName'] ?? '',
      locationAddress: map['locationAddress'] ?? '',
      status: map['status']?.toInt() ?? 0,
      type: map['type']?.toInt() ?? 0,
      dateExpired:
          DateTime.parse(map['dateExpired'] ?? DATE_TIME_DEFAULT).toLocal(),
      startTime: null,
      endTime: null,
      createdAt:
          DateTime.parse(map['createdAt'] ?? DATE_TIME_DEFAULT).toLocal(),
      offerers: [],
      participantsCount: map['participantsCount'] ?? PARTICIPANTS_COUNT,
      selectedOffers: [],
    );
  }
  factory RequestModel.fromCreateRequestByTargetExpert(
      Map<String, dynamic> map) {
    return RequestModel(
      id: map['_id'] ?? '',
      authorUser: null,
      title: map['title'] ?? '',
      slug: map['slug'] ?? '',
      content: map['content'] ?? '',
      specialty: map['specialty'] ?? '',
      budget: null,
      contactForm: map['contactForm'] ?? '',
      locationName: map['locationName'] ?? '',
      locationAddress: map['locationAddress'] ?? '',
      status: map['status']?.toInt() ?? 0,
      type: map['type']?.toInt() ?? 0,
      dateExpired:
          DateTime.parse(map['dateExpired'] ?? DATE_TIME_DEFAULT).toLocal(),
      startTime: null,
      endTime: null,
      createdAt:
          DateTime.parse(map['createdAt'] ?? DATE_TIME_DEFAULT).toLocal(),
      offerers: [],
      participantsCount: map['participantsCount'] ?? PARTICIPANTS_COUNT,
      selectedOffers: [],
    );
  }

  factory RequestModel.fromOfferRequest(Map<String, dynamic> map) {
    RequestModel request = RequestModel.fromMap(map['offeredRequest']);

    request.myOfferId = map['_id'];
    request.price = BudgetModel.fromMap(map['price']);
    request.authorExpert = map['authorExpert'] == null
        ? null
        : AuthorExpertModel.fromMap(map['authorExpert']);
    request.startTime =
        DateTime.parse(map['startTime'] ?? DATE_TIME_DEFAULT).toLocal();
    request.endTime =
        DateTime.parse(map['endTime'] ?? DATE_TIME_DEFAULT).toLocal();
    request.createdAt =
        DateTime.parse(map['createdAt'] ?? DATE_TIME_DEFAULT).toLocal();
    request.askanyPaymentTime =
        DateTime.parse(map['askanyPaymentTime'] ?? DATE_TIME_DEFAULT).toLocal();
    request.confirmedTime =
        DateTime.parse(map['confirmedTime'] ?? DATE_TIME_DEFAULT).toLocal();
    request.completedTime =
        DateTime.parse(map['completedTime'] ?? DATE_TIME_DEFAULT).toLocal();

    if (map['cancel']?["reason"] != null) {
      request.cancel = CancelModel.fromMap(map['cancel']);
    }

    request.isPickTime = map['isPickTime'];

    if (map['report'] != null) {
      request.report = ReportModel.fromMap(map['report']);
    }
    if (map['rating'] != null) {
      request.rating = RatingModel.fromMap(map['rating']);
    }
    if (map['cancelledBy'] != null) {
      request.cancelledBy = map['cancelledBy'];
    }

    return request;
  }

  String toJson() => json.encode(toMap());

  factory RequestModel.fromJson(String source) =>
      RequestModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'RequestModel(id: $id, authorModel: $authorUser, title: $title, slug: $slug, content: $content, specialty: $specialty, budget: $budget, contactForm: $contactForm, locationName: $locationName, locationAddress: $locationAddress, status: $status, type: $type, dateExpired: $dateExpired, startTime: $startTime, endTime: $endTime, createdAt: $createdAt, selectedOffers: $selectedOffers)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is RequestModel &&
        other.id == id &&
        other.authorUser == authorUser &&
        other.title == title &&
        other.slug == slug &&
        other.content == content &&
        other.specialty == specialty &&
        other.budget == budget &&
        other.contactForm == contactForm &&
        other.locationName == locationName &&
        other.locationAddress == locationAddress &&
        other.status == status &&
        other.type == type &&
        other.dateExpired == dateExpired &&
        other.startTime == startTime &&
        other.endTime == endTime &&
        other.createdAt == createdAt;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        authorUser.hashCode ^
        title.hashCode ^
        slug.hashCode ^
        content.hashCode ^
        specialty.hashCode ^
        budget.hashCode ^
        contactForm.hashCode ^
        locationName.hashCode ^
        locationAddress.hashCode ^
        status.hashCode ^
        type.hashCode ^
        dateExpired.hashCode ^
        startTime.hashCode ^
        endTime.hashCode ^
        createdAt.hashCode;
  }

  bool get isMyAuthor =>
      authorUser!.id == AppBloc.userBloc.getAccount.id ||
      [...offerers, ...selectedOffers]
          .where((offer) =>
              offer.authorExpert.id == AppBloc.userBloc.getAccount.id)
          .isNotEmpty;

  OfferModel? get myOffer {
    if (myOfferId == null) {
      return null;
    }

    int indexOfRequest = [...offerers, ...selectedOffers].indexWhere(
      (offer) => offer.id == myOfferId,
    );

    if (indexOfRequest != -1) {
      return [...offerers, ...selectedOffers][indexOfRequest];
    }
    if (selectedOffers.length == 1) {
      return [...offerers, ...selectedOffers][0];
    }

    return null;
  }
}
