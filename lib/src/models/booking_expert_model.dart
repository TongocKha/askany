import 'dart:convert';

class OfferByTargetExpertModel {
  OfferByTargetExpertModel({
    required this.requestTitle,
    required this.requestSlug,
    required this.authorExpert,
    required this.price,
    required this.content,
    required this.offeredRequest,
    required this.offeredUser,
    required this.status,
    required this.note,
    required this.authorPhone,
    required this.authorName,
    required this.startTime,
    required this.endTime,
    required this.askanyPaymentTime,
    required this.confirmedTime,
    required this.completedTime,
    required this.dateExpired,
    required this.isPickTime,
    required this.skill,
    required this.createdAt,
    required this.modifiedAt,
    required this.id,
    required this.v,
  });

  final String requestTitle;
  final String requestSlug;
  final String authorExpert;
  final String price;
  final String content;
  final String offeredRequest;
  final String offeredUser;
  final int status;
  final String note;
  final String authorPhone;
  final String authorName;
  final DateTime startTime;
  final DateTime endTime;
  final DateTime askanyPaymentTime;
  final DateTime confirmedTime;
  final DateTime completedTime;
  final DateTime dateExpired;
  final bool isPickTime;
  final dynamic skill;
  final DateTime createdAt;
  final DateTime modifiedAt;
  final String id;
  final int v;

  factory OfferByTargetExpertModel.fromJson(String str) =>
      OfferByTargetExpertModel.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory OfferByTargetExpertModel.fromMap(Map<String, dynamic> json) =>
      OfferByTargetExpertModel(
        requestTitle: json["requestTitle"],
        requestSlug: json["requestSlug"],
        authorExpert: json["authorExpert"],
        price: json["price"],
        content: json["content"],
        offeredRequest: json["offeredRequest"],
        offeredUser: json["offeredUser"],
        status: json["status"],
        note: json["note"],
        authorPhone: json["authorPhone"],
        authorName: json["authorName"],
        startTime: DateTime.parse(json["startTime"]),
        endTime: DateTime.parse(json["endTime"]),
        askanyPaymentTime: DateTime.parse(json["askanyPaymentTime"]),
        confirmedTime: DateTime.parse(json["confirmedTime"]),
        completedTime: DateTime.parse(json["completedTime"]),
        dateExpired: DateTime.parse(json["dateExpired"]),
        isPickTime: json["isPickTime"],
        skill: json["skill"],
        createdAt: DateTime.parse(json["createdAt"]),
        modifiedAt: DateTime.parse(json["modifiedAt"]),
        id: json["_id"],
        v: json["__v"],
      );

  Map<String, dynamic> toMap() => {
        "requestTitle": requestTitle,
        "requestSlug": requestSlug,
        "authorExpert": authorExpert,
        "price": price,
        "content": content,
        "offeredRequest": offeredRequest,
        "offeredUser": offeredUser,
        "status": status,
        "note": note,
        "authorPhone": authorPhone,
        "authorName": authorName,
        "startTime": startTime.toIso8601String(),
        "endTime": endTime.toIso8601String(),
        "askanyPaymentTime": askanyPaymentTime.toIso8601String(),
        "confirmedTime": confirmedTime.toIso8601String(),
        "completedTime": completedTime.toIso8601String(),
        "dateExpired": dateExpired.toIso8601String(),
        "isPickTime": isPickTime,
        "skill": skill,
        "createdAt": createdAt.toIso8601String(),
        "modifiedAt": modifiedAt.toIso8601String(),
        "_id": id,
        "__v": v,
      };
}

class RequestByTargetExpertModel {
  RequestByTargetExpertModel({
    required this.authorUser,
    required this.title,
    required this.slug,
    required this.content,
    required this.participantsCount,
    required this.budget,
    required this.contactForm,
    required this.locationName,
    required this.note,
    required this.locationAddress,
    required this.status,
    required this.type,
    required this.offerers,
    required this.selectedOffers,
    required this.dateExpired,
    required this.createdAt,
    required this.modifiedAt,
    required this.id,
    required this.v,
  });

  final String authorUser;
  final String title;
  final String slug;
  final String content;
  final dynamic participantsCount;
  final String budget;
  final String contactForm;
  final String locationName;
  final String note;
  final String locationAddress;
  final int status;
  final int type;
  final List<dynamic> offerers;
  final List<String> selectedOffers;
  final DateTime dateExpired;
  final DateTime createdAt;
  final DateTime modifiedAt;
  final String id;
  final int v;

  factory RequestByTargetExpertModel.fromJson(String str) =>
      RequestByTargetExpertModel.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory RequestByTargetExpertModel.fromMap(Map<String, dynamic> json) =>
      RequestByTargetExpertModel(
        authorUser: json["authorUser"],
        title: json["title"],
        slug: json["slug"],
        content: json["content"],
        participantsCount: json["participantsCount"],
        budget: json["budget"],
        contactForm: json["contactForm"],
        locationName: json["locationName"],
        note: json["note"],
        locationAddress: json["locationAddress"],
        status: json["status"],
        type: json["type"],
        offerers: List<dynamic>.from(json["offerers"].map((x) => x)),
        selectedOffers: List<String>.from(json["selectedOffers"].map((x) => x)),
        dateExpired: DateTime.parse(json["dateExpired"]),
        createdAt: DateTime.parse(json["createdAt"]),
        modifiedAt: DateTime.parse(json["modifiedAt"]),
        id: json["_id"],
        v: json["__v"],
      );

  Map<String, dynamic> toMap() => {
        "authorUser": authorUser,
        "title": title,
        "slug": slug,
        "content": content,
        "participantsCount": participantsCount,
        "budget": budget,
        "contactForm": contactForm,
        "locationName": locationName,
        "note": note,
        "locationAddress": locationAddress,
        "status": status,
        "type": type,
        "offerers": List<dynamic>.from(offerers.map((x) => x)),
        "selectedOffers": List<dynamic>.from(selectedOffers.map((x) => x)),
        "dateExpired": dateExpired.toIso8601String(),
        "createdAt": createdAt.toIso8601String(),
        "modifiedAt": modifiedAt.toIso8601String(),
        "_id": id,
        "__v": v,
      };
}
