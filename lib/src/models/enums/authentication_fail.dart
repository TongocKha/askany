import 'package:askany/src/configs/lang/localization.dart';

enum AuthenticationException {
  NOT_REGISTRATION_EXPERT,
  EMAIL_NOT_EXISTS,
  WRONG_PASSWORD,
  UNCONFIRMED_ACCOUNT,
  EMAIL_EXISTS,
  SOCIAL_EXISTS,
  PHONE_EXISTS,
}

extension AuthenticationExceptionDescription on AuthenticationException {
  String? get description {
    switch (this) {
      case AuthenticationException.NOT_REGISTRATION_EXPERT:
        return Strings.notRegistrationExpertNoti.i18n;
      case AuthenticationException.EMAIL_NOT_EXISTS:
        return Strings.emailNotExistNoti.i18n;
      case AuthenticationException.WRONG_PASSWORD:
        return Strings.wrongPasswordNoti.i18n;
      case AuthenticationException.UNCONFIRMED_ACCOUNT:
        return Strings.unConfirmedAccount.i18n;
      case AuthenticationException.PHONE_EXISTS:
        return Strings.phoneExistedNoti.i18n;
      case AuthenticationException.EMAIL_EXISTS:
        return Strings.emailExistedNoti.i18n;
      case AuthenticationException.SOCIAL_EXISTS:
        return Strings.socialExistedNoti.i18n;
      default:
        return null;
    }
  }
}
