enum BlocExpertException {
  BLOC_EXPERT_EXISTS,
}

extension BlocExpertExceptionDescription on BlocExpertException {
  String? get description {
    switch (this) {
      case BlocExpertException.BLOC_EXPERT_EXISTS:
        return 'Bạn đã chặn chuyên gia này!';
      default:
        return null;
    }
  }
}
