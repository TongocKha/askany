import 'dart:io';

import 'package:askany/src/ui/style/style.dart';

enum PaymentMethods {
  vnpay,
  zalo_pay,
  momo,
  native_pay,
  askany,
}

extension PaymentMethodExtention on PaymentMethods {
  String get textDescription {
    String result = '';

    switch (this) {
      case PaymentMethods.vnpay:
        result = 'Thanh toán bằng VNPAY';
        break;
      case PaymentMethods.momo:
        result = 'Thanh toán bằng MOMO';
        break;
      case PaymentMethods.native_pay:
        result = 'Thanh toán bằng ${Platform.isIOS ? 'Apple Pay' : 'Google Pay'}';
        break;
      case PaymentMethods.zalo_pay:
        result = 'Thanh toán bằng ZaloPay';
        break;
      case PaymentMethods.askany:
        result = 'Thanh toán bằng ví Askany';
        break;
      default:
        break;
    }

    return result;
  }

  String get assetImage {
    String result = momoLogo;

    switch (this) {
      case PaymentMethods.vnpay:
        result = vnpayLogo;
        break;
      case PaymentMethods.native_pay:
        result = Platform.isIOS ? applePayLogo : googlePayLogo;
        break;
      case PaymentMethods.askany:
        result = askanyLogo;
        break;
      case PaymentMethods.zalo_pay:
        result = zaloLogo;
        break;
      default:
        break;
    }

    return result;
  }
}
