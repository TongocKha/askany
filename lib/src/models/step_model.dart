import 'dart:convert';

class StepModel {
  final String title;
  final DateTime createdAt;
  final bool isDanger;
  StepModel({
    required this.title,
    required this.createdAt,
    this.isDanger = false,
  });

  StepModel copyWith({
    String? title,
    DateTime? createdAt,
  }) {
    return StepModel(
      title: title ?? this.title,
      createdAt: createdAt ?? this.createdAt,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'title': title,
      'createdAt': createdAt.millisecondsSinceEpoch,
    };
  }

  factory StepModel.fromMap(Map<String, dynamic> map) {
    return StepModel(
      title: map['title'] ?? '',
      createdAt: DateTime.fromMillisecondsSinceEpoch(map['createdAt']),
    );
  }

  String toJson() => json.encode(toMap());

  factory StepModel.fromJson(String source) => StepModel.fromMap(json.decode(source));

  @override
  String toString() => 'StepModel(title: $title, createdAt: $createdAt)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is StepModel && other.title == title && other.createdAt == createdAt;
  }

  @override
  int get hashCode => title.hashCode ^ createdAt.hashCode;
}
