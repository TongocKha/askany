import 'dart:convert';

import 'package:askany/src/constants/constants.dart';
import 'package:flutter/foundation.dart';

import 'package:askany/src/models/weekly_schedule_model.dart';
import 'package:intl/intl.dart';

class TimelinePriceModel {
  final String id;
  int isRepeat;
  dynamic specialFreeDates;
  final int status;
  List<WeeklyScheduleModel> weeklySchedules;
  TimelinePriceModel({
    required this.id,
    required this.isRepeat,
    required this.specialFreeDates,
    required this.status,
    required this.weeklySchedules,
  });

  TimelinePriceModel copyWith({
    String? id,
    int? isRepeat,
    dynamic specialFreeDates,
    int? status,
    List<WeeklyScheduleModel>? weeklySchedules,
  }) {
    return TimelinePriceModel(
      id: id ?? this.id,
      isRepeat: isRepeat ?? this.isRepeat,
      specialFreeDates: specialFreeDates ?? this.specialFreeDates,
      status: status ?? this.status,
      weeklySchedules: weeklySchedules ?? this.weeklySchedules,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'isRepeat': isRepeat,
      'specialFreeDates': specialFreeDates,
      'status': status,
      'weeklySchedules': weeklySchedules.map((x) => x.toMap()).toList(),
    };
  }

  Map<String, dynamic> toMapUpdate() {
    return {
      'isRepeat': isRepeat == IS_REPEAT,
      'dayOfWeek': weeklySchedules.map((e) => e.dayOfWeek).toList().join(','),
      'date':
          weeklySchedules.map((e) => DateFormat('yyyy-MM-dd').format(e.date)).toList().join(','),
      'timesCall': weeklySchedules.first.timesCall.map((e) => e.toJson()).toList(),
      'timesMeet': weeklySchedules.first.timesMeet.map((e) => e.toJson()).toList(),
    };
  }

  factory TimelinePriceModel.fromMap(Map<String, dynamic> map) {
    return TimelinePriceModel(
      id: map['_id'] ?? '',
      isRepeat: map['isRepeat']?.toInt() ?? 0,
      specialFreeDates: map['specialFreeDates'] ?? null,
      status: map['status']?.toInt() ?? 0,
      weeklySchedules: ((map['weeklySchedules'] as List?) ?? [null])
          .map((item) => WeeklyScheduleModel.fromMap(item))
          .toList(),
    );
  }

  String toJson() => json.encode(toMap());
  String toJsonUpdate() => json.encode(toMapUpdate());

  factory TimelinePriceModel.fromJson(String source) =>
      TimelinePriceModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'TimelinePriceModel(id: $id, isRepeat: $isRepeat, specialFreeDates: $specialFreeDates, status: $status, weeklySchedules: $weeklySchedules)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is TimelinePriceModel &&
        other.id == id &&
        other.isRepeat == isRepeat &&
        other.specialFreeDates == specialFreeDates &&
        other.status == status &&
        listEquals(other.weeklySchedules, weeklySchedules);
  }

  @override
  int get hashCode {
    return id.hashCode ^
        isRepeat.hashCode ^
        specialFreeDates.hashCode ^
        status.hashCode ^
        weeklySchedules.hashCode;
  }
}
