import 'dart:convert';

import 'package:askany/src/models/conversation_model.dart';

class IncomingCallModel {
  final ConversationModel room;
  final String createdBy;
  final String receiverId;
  final String sdp;
  final String id;
  IncomingCallModel({
    required this.room,
    required this.createdBy,
    required this.receiverId,
    required this.sdp,
    required this.id,
  });

  IncomingCallModel copyWith({
    ConversationModel? room,
    String? createdBy,
    String? receiverId,
    String? sdp,
    String? id,
  }) {
    return IncomingCallModel(
      room: room ?? this.room,
      createdBy: createdBy ?? this.createdBy,
      receiverId: receiverId ?? this.receiverId,
      sdp: sdp ?? this.sdp,
      id: id ?? this.id,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'room': room,
      'createdBy': createdBy,
      'receiverId': receiverId,
      'sdp': sdp,
      '_id': id,
    };
  }

  factory IncomingCallModel.fromMap(Map<String, dynamic> map) {
    return IncomingCallModel(
      room: ConversationModel.fromMap(map['room']),
      createdBy: map['createdBy']?['_id'] ?? '',
      receiverId: map['receiverId']?['_id'] ?? '',
      sdp: map['sdp'] ?? '',
      id: map['_id'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory IncomingCallModel.fromJson(String source) =>
      IncomingCallModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'IncomingCallModel(roomId: $room, createdBy: $createdBy, receiverId: $receiverId, sdp: $sdp)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is IncomingCallModel &&
        other.room == room &&
        other.createdBy == createdBy &&
        other.receiverId == receiverId &&
        other.sdp == sdp;
  }

  @override
  int get hashCode {
    return room.hashCode ^ createdBy.hashCode ^ receiverId.hashCode ^ sdp.hashCode;
  }
}
