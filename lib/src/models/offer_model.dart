import 'dart:convert';

import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/models/author_model.dart';
import 'package:askany/src/models/budget_model.dart';

class OfferModel {
  final String id;
  final AuthorExpertModel authorExpert;
  final BudgetModel price;
  final String content;
  int status;
  String? authorName;
  String? authorPhone;
  String? note;
  bool isPickTime;
  DateTime startTime;
  DateTime endTime;

  OfferModel({
    required this.id,
    required this.authorExpert,
    required this.price,
    required this.content,
    required this.status,
    this.isPickTime = false,
    required this.startTime,
    required this.endTime,
    this.authorName,
    this.authorPhone,
    this.note,
  });

  OfferModel copyWith({
    String? id,
    AuthorExpertModel? authorExpert,
    BudgetModel? price,
    String? content,
    int? status,
    DateTime? startTime,
    DateTime? endTime,
  }) {
    return OfferModel(
      id: id ?? this.id,
      authorExpert: authorExpert ?? this.authorExpert,
      price: price ?? this.price,
      content: content ?? this.content,
      status: status ?? this.status,
      startTime: startTime ?? this.startTime,
      endTime: endTime ?? this.endTime,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'authorExpert': authorExpert.toMap(),
      'price': price.toMap(),
      'content': content,
      'status': status,
    };
  }

  factory OfferModel.fromMap(Map<String, dynamic> map) {
    return OfferModel(
      id: map['_id'] ?? '',
      authorExpert: AuthorExpertModel.fromMap(map['authorExpert']),
      price: BudgetModel.fromMap(map['price']),
      content: map['content'] ?? '',
      status: map['status']?.toInt() ?? 0,
      startTime:
          DateTime.parse(map['startTime'] ?? DATE_TIME_DEFAULT).toLocal(),
      endTime: DateTime.parse(map['endTime'] ?? DATE_TIME_DEFAULT).toLocal(),
      isPickTime: map['isPickTime'] ?? false,
      authorName: map['authorName'] ?? '',
      authorPhone: map['authorPhone'] ?? '',
      note: map['note'] ?? '',
    );
  }

  factory OfferModel.fromOrderSkill(Map<String, dynamic> map) {
    return OfferModel(
      id: map['_id'] ?? '',
      authorExpert: AuthorExpertModel.fromMap(map['authorExpert']),
      price: BudgetModel.fromMap(map['price']),
      content: map['content'] ?? '',
      status: map['status']?.toInt() ?? 0,
      startTime:
          DateTime.parse(map['startTime'] ?? DATE_TIME_DEFAULT).toLocal(),
      endTime: DateTime.parse(map['endTime'] ?? DATE_TIME_DEFAULT).toLocal(),
      isPickTime: map['isPickTime'] ?? false,
      authorName: map['authorName'] ?? '',
      authorPhone: map['authorPhone'] ?? '',
      note: map['note'] ?? '',
    );
  }
  factory OfferModel.fromTargetExpert(Map<String, dynamic> map) {
    return OfferModel(
      id: map['_id'] ?? '',
      authorExpert: AuthorExpertModel.fromMap(map['authorExpert']),
      price: BudgetModel.fromMap(map['price']),
      content: map['content'] ?? '',
      status: map['status']?.toInt() ?? 0,
      startTime:
          DateTime.parse(map['startTime'] ?? DATE_TIME_DEFAULT).toLocal(),
      endTime: DateTime.parse(map['endTime'] ?? DATE_TIME_DEFAULT).toLocal(),
      isPickTime: map['isPickTime'] ?? false,
      authorName: map['authorName'] ?? '',
      authorPhone: map['authorPhone'] ?? '',
      note: map['note'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory OfferModel.fromJson(String source) =>
      OfferModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'OfferModel(id: $id, authorExpert: $authorExpert, price: $price, content: $content, status: $status)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is OfferModel &&
        other.id == id &&
        other.authorExpert == authorExpert &&
        other.price == price &&
        other.content == content &&
        other.status == status;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        authorExpert.hashCode ^
        price.hashCode ^
        content.hashCode ^
        status.hashCode;
  }
}
