import 'dart:convert';

import 'package:askany/src/constants/constants.dart';

class OfferSkillModel {
  final String id;
  final String requestTitle;
  final String requestSlug;
  final String authorExpert;
  final String content;
  final String? offeredRequest;
  final String? offeredUser;
  final double status;
  final DateTime startTime;
  final DateTime endTime;
  final DateTime? askanyPaymentTime;
  final DateTime? confirmedTime;
  final DateTime? completedTime;
  final DateTime? dateExpired;
  final bool isPickTime;
  final String skill;
  final DateTime createdAt;
  final DateTime modifiedAt;
  OfferSkillModel({
    required this.id,
    required this.requestTitle,
    required this.requestSlug,
    required this.authorExpert,
    required this.content,
    this.offeredRequest,
    this.offeredUser,
    required this.status,
    required this.startTime,
    required this.endTime,
    this.askanyPaymentTime,
    this.confirmedTime,
    this.completedTime,
    this.dateExpired,
    required this.isPickTime,
    required this.skill,
    required this.createdAt,
    required this.modifiedAt,
  });

  OfferSkillModel copyWith({
    String? id,
    String? requestTitle,
    String? requestSlug,
    String? authorExpert,
    String? content,
    String? offeredRequest,
    String? offeredUser,
    double? status,
    DateTime? startTime,
    DateTime? endTime,
    DateTime? askanyPaymentTime,
    DateTime? confirmedTime,
    DateTime? completedTime,
    DateTime? dateExpired,
    bool? isPickTime,
    String? skill,
    DateTime? createdAt,
    DateTime? modifiedAt,
  }) {
    return OfferSkillModel(
      id: id ?? this.id,
      requestTitle: requestTitle ?? this.requestTitle,
      requestSlug: requestSlug ?? this.requestSlug,
      authorExpert: authorExpert ?? this.authorExpert,
      content: content ?? this.content,
      offeredRequest: offeredRequest ?? this.offeredRequest,
      offeredUser: offeredUser ?? this.offeredUser,
      status: status ?? this.status,
      startTime: startTime ?? this.startTime,
      endTime: endTime ?? this.endTime,
      askanyPaymentTime: askanyPaymentTime ?? this.askanyPaymentTime,
      confirmedTime: confirmedTime ?? this.confirmedTime,
      completedTime: completedTime ?? this.completedTime,
      dateExpired: dateExpired ?? this.dateExpired,
      isPickTime: isPickTime ?? this.isPickTime,
      skill: skill ?? this.skill,
      createdAt: createdAt ?? this.createdAt,
      modifiedAt: modifiedAt ?? this.modifiedAt,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'requestTitle': requestTitle,
      'requestSlug': requestSlug,
      'authorExpert': authorExpert,
      'content': content,
      'offeredRequest': offeredRequest,
      'offeredUser': offeredUser,
      'status': status,
      'startTime': startTime.millisecondsSinceEpoch,
      'endTime': endTime.millisecondsSinceEpoch,
      'askanyPaymentTime': askanyPaymentTime?.millisecondsSinceEpoch,
      'confirmedTime': confirmedTime?.millisecondsSinceEpoch,
      'completedTime': completedTime?.millisecondsSinceEpoch,
      'dateExpired': dateExpired?.millisecondsSinceEpoch,
      'isPickTime': isPickTime,
      'skill': skill,
      'createdAt': createdAt.millisecondsSinceEpoch,
      'modifiedAt': modifiedAt.millisecondsSinceEpoch,
    };
  }

  factory OfferSkillModel.fromMap(Map<String, dynamic> map) {
    return OfferSkillModel(
      id: map['_id'] ?? '',
      requestTitle: map['requestTitle'] ?? '',
      requestSlug: map['requestSlug'] ?? '',
      authorExpert: map['authorExpert'] ?? '',
      content: map['content'] ?? '',
      offeredRequest: map['offeredRequest'] ?? '',
      offeredUser: map['offeredUser'] ?? '',
      status: map['status']?.toDouble() ?? 0.0,
      startTime:
          DateTime.parse(map['startTime'] ?? DATE_TIME_DEFAULT).toLocal(),
      endTime: DateTime.parse(map['endTime'] ?? DATE_TIME_DEFAULT).toLocal(),
      askanyPaymentTime: null,
      confirmedTime: null,
      completedTime: null,
      dateExpired: null,
      isPickTime: map['isPickTime'] ?? false,
      skill: map['skill'] ?? '',
      createdAt:
          DateTime.parse(map['createdAt'] ?? DATE_TIME_DEFAULT).toLocal(),
      modifiedAt:
          DateTime.parse(map['modifiedAt'] ?? DATE_TIME_DEFAULT).toLocal(),
    );
  }

  String toJson() => json.encode(toMap());

  factory OfferSkillModel.fromJson(String source) =>
      OfferSkillModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'OfferSkillModel(requestTitle: $requestTitle, requestSlug: $requestSlug, authorExpert: $authorExpert, content: $content, offeredRequest: $offeredRequest, offeredUser: $offeredUser, status: $status, startTime: $startTime, endTime: $endTime, askanyPaymentTime: $askanyPaymentTime, confirmedTime: $confirmedTime, completedTime: $completedTime, dateExpired: $dateExpired, isPickTime: $isPickTime, skill: $skill, createdAt: $createdAt, modifiedAt: $modifiedAt)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is OfferSkillModel &&
        other.requestTitle == requestTitle &&
        other.requestSlug == requestSlug &&
        other.authorExpert == authorExpert &&
        other.content == content &&
        other.offeredRequest == offeredRequest &&
        other.offeredUser == offeredUser &&
        other.status == status &&
        other.startTime == startTime &&
        other.endTime == endTime &&
        other.askanyPaymentTime == askanyPaymentTime &&
        other.confirmedTime == confirmedTime &&
        other.completedTime == completedTime &&
        other.dateExpired == dateExpired &&
        other.isPickTime == isPickTime &&
        other.skill == skill &&
        other.createdAt == createdAt &&
        other.modifiedAt == modifiedAt;
  }

  @override
  int get hashCode {
    return requestTitle.hashCode ^
        requestSlug.hashCode ^
        authorExpert.hashCode ^
        content.hashCode ^
        offeredRequest.hashCode ^
        offeredUser.hashCode ^
        status.hashCode ^
        startTime.hashCode ^
        endTime.hashCode ^
        askanyPaymentTime.hashCode ^
        confirmedTime.hashCode ^
        completedTime.hashCode ^
        dateExpired.hashCode ^
        isPickTime.hashCode ^
        skill.hashCode ^
        createdAt.hashCode ^
        modifiedAt.hashCode;
  }
}
