import 'dart:convert';

class ReportModel {
  ReportModel({
    required this.id,
    this.expert,
    required this.content,
    this.authorUser,
    required this.images,
    required this.status,
    this.offer,
    this.createdAt,
    this.modifiedAt,
    this.v,
  });

  final String id;
  final String? expert;
  final String content;
  final String? authorUser;
  final List<dynamic> images;
  final int status;
  final String? offer;
  final DateTime? createdAt;
  final DateTime? modifiedAt;
  final int? v;

  factory ReportModel.fromJson(String str) =>
      ReportModel.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory ReportModel.fromMap(Map<String, dynamic> json) => ReportModel(
        id: json["_id"],
        expert: json["expert"] ?? '',
        content: json["content"],
        authorUser: json["authorUser"] ?? '',
        images: List<dynamic>.from(json["images"].map((x) => x)),
        status: json["status"],
        offer: json["offer"] ?? '',
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        modifiedAt: json["modifiedAt"] == null
            ? null
            : DateTime.parse(json["modifiedAt"]),
        v: json["__v"],
      );

  Map<String, dynamic> toMap() => {
        "_id": id,
        "expert": expert,
        "content": content,
        "authorUser": authorUser,
        "images": List<dynamic>.from(images.map((x) => x)),
        "status": status,
        "offer": offer,
        "createdAt": createdAt?.toIso8601String(),
        "modifiedAt": modifiedAt?.toIso8601String(),
        "__v": v,
      };
}
