import 'dart:convert';

enum PaymentType {
  call,
  meet,
}

class PaymentInfoModel {
  final PaymentType type;
  final int quantity;
  final String price;
  final String serviceTime;
  final String totalPrice;
  final String taxPrice;
  final String total;
  PaymentInfoModel({
    required this.type,
    required this.quantity,
    required this.price,
    required this.serviceTime,
    required this.totalPrice,
    required this.taxPrice,
    required this.total,
  });

  PaymentInfoModel copyWith({
    PaymentType? type,
    int? quantity,
    String? price,
    String? serviceTime,
    String? totalPrice,
    String? taxPrice,
    String? total,
  }) {
    return PaymentInfoModel(
      type: type ?? this.type,
      quantity: quantity ?? this.quantity,
      price: price ?? this.price,
      serviceTime: serviceTime ?? this.serviceTime,
      totalPrice: totalPrice ?? this.totalPrice,
      taxPrice: taxPrice ?? this.taxPrice,
      total: total ?? this.total,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'type': type,
      'quantity': quantity,
      'price': price,
      'serviceTime': serviceTime,
      'totalPrice': totalPrice,
      'taxPrice': taxPrice,
      'total': total,
    };
  }

  factory PaymentInfoModel.fromMap(Map<String, dynamic> map) {
    return PaymentInfoModel(
      type: map['type']?.toInt() ?? 0,
      quantity: map['quantity']?.toInt() ?? 0,
      price: map['price'] ?? '',
      serviceTime: map['serviceTime'] ?? '',
      totalPrice: map['totalPrice'] ?? '',
      taxPrice: map['taxPrice'] ?? '',
      total: map['total'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory PaymentInfoModel.fromJson(String source) => PaymentInfoModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'PaymentInfoModel(type: $type, quantity: $quantity, price: $price, serviceTime: $serviceTime, totalPrice: $totalPrice, taxPrice: $taxPrice, total: $total)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is PaymentInfoModel &&
        other.type == type &&
        other.quantity == quantity &&
        other.price == price &&
        other.serviceTime == serviceTime &&
        other.totalPrice == totalPrice &&
        other.taxPrice == taxPrice &&
        other.total == total;
  }

  @override
  int get hashCode {
    return type.hashCode ^
        quantity.hashCode ^
        price.hashCode ^
        serviceTime.hashCode ^
        totalPrice.hashCode ^
        taxPrice.hashCode ^
        total.hashCode;
  }

  String get paymentType {
    switch (type) {
      case PaymentType.call:
        return 'Gói gọi điện';
      case PaymentType.meet:
        return 'Gói gặp mặt';
      default:
        return '';
    }
  }
}
