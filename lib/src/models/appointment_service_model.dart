import 'dart:convert';

import 'package:askany/src/models/account_model.dart';
import 'package:askany/src/models/payment_info_model.dart';
import 'package:askany/src/models/step_model.dart';

class AppointmentServiceModel {
  final String id;
  final List<StepModel> steps;
  final String title;
  final DateTime startTime;
  final DateTime endTime;
  final String address;
  final AccountModel expert;
  final PaymentInfoModel paymentInfo;
  AppointmentServiceModel({
    required this.id,
    required this.title,
    required this.steps,
    required this.startTime,
    required this.endTime,
    required this.address,
    required this.expert,
    required this.paymentInfo,
  });

  AppointmentServiceModel copyWith({
    String? id,
    String? title,
    String? serviceName,
    DateTime? startTime,
    DateTime? endTime,
    String? address,
    List<StepModel>? steps,
    AccountModel? expert,
    PaymentInfoModel? paymentInfo,
  }) {
    return AppointmentServiceModel(
      id: id ?? this.id,
      title: title ?? this.title,
      steps: steps ?? this.steps,
      startTime: startTime ?? this.startTime,
      endTime: endTime ?? this.endTime,
      address: address ?? this.address,
      expert: expert ?? this.expert,
      paymentInfo: paymentInfo ?? this.paymentInfo,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'title': title,
      'address': address,
      'expert': expert.toMap(),
      'paymentInfo': paymentInfo.toMap(),
    };
  }

  factory AppointmentServiceModel.fromMap(Map<String, dynamic> map) {
    return AppointmentServiceModel(
      id: map['id'] ?? '',
      title: map['title'] ?? '',
      steps: map['steps']?.map((step) => StepModel.fromMap(step))?.toList() ?? [],
      startTime: DateTime.parse(map['startTime']),
      endTime: DateTime.parse(map['endTime']),
      address: map['address'] ?? '',
      expert: AccountModel.fromMap(map['expert']),
      paymentInfo: PaymentInfoModel.fromMap(map['paymentInfo']),
    );
  }

  String toJson() => json.encode(toMap());

  factory AppointmentServiceModel.fromJson(String source) =>
      AppointmentServiceModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'AppointmentServiceModel(id: $id, title: $title, address: $address, expert: $expert, paymentInfo: $paymentInfo)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is AppointmentServiceModel &&
        other.id == id &&
        other.title == title &&
        other.address == address &&
        other.expert == expert &&
        other.paymentInfo == paymentInfo;
  }

  @override
  int get hashCode {
    return id.hashCode ^ title.hashCode ^ address.hashCode ^ expert.hashCode ^ paymentInfo.hashCode;
  }
}
