import 'dart:convert';

import 'package:askany/src/configs/lang/language_service.dart';
import 'package:askany/src/models/cancel_service_reason_model.dart';

class LanguageModel {
  final String id;
  final double? status;
  final DateTime? createdAt;
  final DateTime? modifiedAt;
  final ReasonModel vi;
  final ReasonModel en;
  LanguageModel({
    required this.id,
    required this.status,
    this.createdAt,
    this.modifiedAt,
    required this.vi,
    required this.en,
  });

  LanguageModel copyWith({
    String? id,
    double? status,
    DateTime? createdAt,
    DateTime? modifiedAt,
    ReasonModel? vi,
    ReasonModel? en,
  }) {
    return LanguageModel(
      id: id ?? this.id,
      status: status ?? this.status,
      createdAt: createdAt ?? this.createdAt,
      modifiedAt: modifiedAt ?? this.modifiedAt,
      vi: vi ?? this.vi,
      en: en ?? this.en,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'status': status,
      'createdAt': createdAt?.millisecondsSinceEpoch,
      'modifiedAt': modifiedAt?.millisecondsSinceEpoch,
      'vi': vi.toMap(),
      'en': en.toMap(),
    };
  }

  factory LanguageModel.fromMap(Map<String, dynamic> map) {
    return LanguageModel(
      id: map['_id'] ?? '',
      status: double.parse(((map['status'] ?? 0).toString())),
      createdAt: map['createdAt'] != null ? DateTime.parse(map['createdAt']).toLocal() : null,
      modifiedAt: map['modifiedAt'] != null ? DateTime.parse(map['modifiedAt']).toLocal() : null,
      vi: ReasonModel.fromMapLanguage(map['vi']),
      en: ReasonModel.fromMapLanguage(map['en']),
    );
  }

  String toJson() => json.encode(toMap());

  factory LanguageModel.fromJson(String source) => LanguageModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'LanguageModel(id: $id, status: $status, createdAt: $createdAt, modifiedAt: $modifiedAt, vi: $vi, en: $en)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is LanguageModel &&
        other.id == id &&
        other.status == status &&
        other.createdAt == createdAt &&
        other.modifiedAt == modifiedAt &&
        other.vi == vi &&
        other.en == en;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        status.hashCode ^
        createdAt.hashCode ^
        modifiedAt.hashCode ^
        vi.hashCode ^
        en.hashCode;
  }

  String get getLocaleTitle {
    if (LanguageService.getIsLanguage('vi')) {
      return vi.name;
    }

    return en.name;
  }
}
