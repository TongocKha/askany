import 'dart:convert';
import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/models/budget_model.dart';
import 'package:askany/src/models/language_model.dart';
import 'package:askany/src/models/position_model.dart';
import 'package:askany/src/models/skill_model.dart';
import 'package:askany/src/models/specialty_model.dart';
import 'package:askany/src/models/time_line_price_model.dart';
import 'package:askany/src/models/working_experience.dart';
import 'package:flutter/foundation.dart';

import 'package:askany/src/models/avatar_model.dart';

class ExpertModel {
  final String id;
  final String expertname;
  final String fullname;
  final int province;
  final int gender;
  final int status;
  final bool isUser;
  final AvatarModel? avatar;
  final List<WorkingExperience>? workingExperience;
  final dynamic experienceConfirm;
  List<LanguageModel>? languages;
  List<SpecialtyModel> specialties;
  final List<SkillModel>? skills;
  final List<String>? timeline;
  final double wallet;
  final int stepInfo;
  final int ratingCount;
  final double stars;
  String companyName;
  int experienceYears;
  String highestPosition;
  String introduce;
  BudgetModel? callPrice;
  BudgetModel? meetPrice;
  List<TimelinePriceModel>? timelinePriceModel;
  List<int>? ratingSingleCounts;
  List<String>? specialtyId;
  ExpertModel({
    required this.id,
    required this.expertname,
    required this.fullname,
    required this.province,
    required this.gender,
    required this.status,
    required this.isUser,
    this.avatar,
    this.workingExperience,
    required this.experienceConfirm,
    this.languages,
    required this.specialties,
    this.skills,
    this.timeline,
    required this.wallet,
    required this.stepInfo,
    required this.ratingCount,
    required this.stars,
    required this.companyName,
    required this.experienceYears,
    required this.highestPosition,
    required this.introduce,
    this.callPrice,
    this.meetPrice,
    this.timelinePriceModel,
    this.ratingSingleCounts,
    this.specialtyId,
  });

  ExpertModel copyWith({
    String? id,
    String? expertname,
    String? fullname,
    int? province,
    int? gender,
    int? status,
    bool? isUser,
    AvatarModel? avatar,
    List<WorkingExperience>? workingExperience,
    dynamic experienceConfirm,
    List<LanguageModel>? languages,
    List<SpecialtyModel>? specialties,
    List<SkillModel>? skills,
    List<String>? timeline,
    double? wallet,
    int? stepInfo,
    int? ratingCount,
    double? stars,
    String? companyName,
    int? experienceYears,
    String? highestPosition,
    String? introduce,
    BudgetModel? callPrice,
    BudgetModel? meetPrice,
    List<int>? ratingSingleCounts,
  }) {
    return ExpertModel(
      id: id ?? this.id,
      expertname: expertname ?? this.expertname,
      fullname: fullname ?? this.fullname,
      province: province ?? this.province,
      gender: gender ?? this.gender,
      status: status ?? this.status,
      isUser: isUser ?? this.isUser,
      avatar: avatar ?? this.avatar,
      workingExperience: workingExperience ?? this.workingExperience,
      experienceConfirm: experienceConfirm ?? this.experienceConfirm,
      languages: languages ?? this.languages,
      specialties: specialties ?? this.specialties,
      skills: skills ?? this.skills,
      timeline: timeline ?? this.timeline,
      wallet: wallet ?? this.wallet,
      stepInfo: stepInfo ?? this.stepInfo,
      ratingCount: ratingCount ?? this.ratingCount,
      stars: stars ?? this.stars,
      companyName: companyName ?? this.companyName,
      experienceYears: experienceYears ?? this.experienceYears,
      highestPosition: highestPosition ?? this.highestPosition,
      introduce: introduce ?? this.introduce,
      callPrice: callPrice ?? this.callPrice,
      meetPrice: meetPrice ?? this.meetPrice,
      ratingSingleCounts: ratingSingleCounts ?? this.ratingSingleCounts,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'expertname': expertname,
      'fullname': fullname,
      'province': province,
      'gender': gender,
      'status': status,
      'isUser': isUser,
      'avatar': avatar?.toMap(),
      'workingExperience': workingExperience,
      'experienceConfirm': experienceConfirm,
      'languages': languages,
      'specialties': specialties,
      'skills': skills,
      'timeline': timeline,
      'wallet': wallet,
      'stepInfo': stepInfo,
      'ratingCount': ratingCount,
      'stars': stars,
      'companyName': companyName,
      'experienceYears': experienceYears,
      'highestPosition': highestPosition,
      'introduce': introduce,
      'callPrice': callPrice?.toMapSkill(),
      'meetPrice': meetPrice?.toMapSkill(),
      'ratingSingleCounts': ratingSingleCounts
    };
  }

  factory ExpertModel.fromMap(Map<String, dynamic> map) {
    return ExpertModel(
      id: map['_id'] ?? '',
      expertname: map['expertname'] ?? '',
      fullname: map['fullname'] ?? '',
      province: map['province']?.toInt() ?? 0,
      gender: map['gender']?.toInt() ?? 0,
      status: map['status']?.toInt() ?? 0,
      isUser: map['isUser'] ?? false,
      avatar: map['avatar'] != null ? AvatarModel.fromMap(map['avatar']) : null,
      workingExperience: null,
      experienceConfirm: map['experienceConfirm'] ?? null,
      languages: [],
      specialties:
          ((map['specialties'] ?? []) as List).map((item) => SpecialtyModel.fromMap(item)).toList(),
      skills: [],
      timeline: [],
      wallet: map['wallet']?.toDouble() ?? 0.0,
      stepInfo: map['stepInfo']?.toInt() ?? 0,
      ratingCount: map['ratingCount']?.toInt() ?? 0,
      stars: double.parse(double.parse((map['stars'] ?? 0).toString()).toStringAsFixed(1)),
      companyName: map['companyName'] ?? '',
      experienceYears: map['experienceYears']?.toInt() ?? 0,
      highestPosition: map['highestPosition']?['_id'] ?? '',
      introduce: map['introduce'] ?? '',
      callPrice: map['callPrice'] != null ? BudgetModel.fromMap(map['callPrice']) : null,
      meetPrice: map['meetPrice'] != null ? BudgetModel.fromMap(map['meetPrice']) : null,
      ratingSingleCounts: null,
    );
  }

  factory ExpertModel.fromMapExpertSearch(Map<String, dynamic> map) {
    return ExpertModel(
      id: map['_id'] ?? '',
      expertname: map['expertname'] ?? '',
      fullname: map['fullname'] ?? '',
      province: map['province']?.toInt() ?? 0,
      gender: map['gender']?.toInt() ?? 0,
      status: map['status']?.toInt() ?? 0,
      isUser: map['isUser'] ?? false,
      avatar: map['avatar'] != null ? AvatarModel.fromMap(map['avatar']) : null,
      workingExperience: null,
      experienceConfirm: map['experienceConfirm'] ?? null,
      languages: [],
      specialties: ((map['specialties'] ?? []) as List)
          .where((item) => item is Map)
          .toList()
          .map((item) => SpecialtyModel.fromMap(item))
          .toList(),
      specialtyId:
          ((map['specialties'] ?? []) as List).where((item) => item is String).toList().cast(),
      skills: [],
      timeline: [],
      wallet: map['wallet']?.toDouble() ?? 0.0,
      stepInfo: map['stepInfo']?.toInt() ?? 0,
      ratingCount: map['ratingCount']?.toInt() ?? 0,
      stars: double.parse(double.parse((map['stars'] ?? 0).toString()).toStringAsFixed(1)),
      companyName: map['companyName'] ?? '',
      experienceYears: map['experienceYears']?.toInt() ?? 0,
      highestPosition: map['highestPosition']?['_id'] ?? '',
      introduce: map['introduce'] ?? '',
      callPrice: map['callPrice'] != null ? BudgetModel.fromMap(map['callPrice']) : null,
      meetPrice: map['meetPrice'] != null ? BudgetModel.fromMap(map['meetPrice']) : null,
      ratingSingleCounts:
          map['ratingSingleCounts'] != null ? List<int>.from(map["ratingSingleCounts"]) : null,
    );
  }

  factory ExpertModel.fromMapProfile(Map<String, dynamic> map) {
    return ExpertModel(
      id: map['_id'] ?? '',
      expertname: map['expertname'] ?? '',
      fullname: map['fullname'] ?? '',
      province: map['province']?.toInt() ?? 0,
      gender: map['gender']?.toInt() ?? 0,
      status: map['status']?.toInt() ?? 0,
      isUser: map['isUser'] ?? false,
      avatar: map['avatar'] != null ? AvatarModel.fromMap(map['avatar']) : null,
      workingExperience: ((map['workingExperience'] ?? []) as List)
          .map((item) => WorkingExperience.fromMap(item))
          .toList(),
      experienceConfirm: map['experienceConfirm'] ?? null,
      languages:
          ((map['languages'] ?? []) as List).map((item) => LanguageModel.fromMap(item)).toList(),
      specialties:
          ((map['specialties'] ?? []) as List).map((item) => SpecialtyModel.fromMap(item)).toList(),
      skills: [],
      timeline: [],
      wallet: map['wallet']?.toDouble() ?? 0.0,
      stepInfo: map['stepInfo']?.toInt() ?? 0,
      ratingCount: map['ratingCount']?.toInt() ?? 0,
      stars: double.parse(double.parse((map['stars'] ?? 0).toString()).toStringAsFixed(1)),
      companyName: map['companyName'] ?? '',
      experienceYears: map['experienceYears']?.toInt() ?? 0,
      highestPosition: map['highestPosition']?['_id'] ?? '',
      introduce: map['introduce'] ?? '',
      callPrice: map['callPrice'] != null ? BudgetModel.fromMap(map['callPrice']) : null,
      meetPrice: map['meetPrice'] != null ? BudgetModel.fromMap(map['meetPrice']) : null,
      timelinePriceModel:
          ((map['timeline'] as List?) ?? []).map((e) => TimelinePriceModel.fromMap(e)).toList(),
    );
  }

  factory ExpertModel.fromMapInfoExpert(Map<String, dynamic> map) {
    return ExpertModel(
      id: map['_id'] ?? '',
      expertname: map['expertname'] ?? '',
      fullname: map['fullname'] ?? '',
      province: map['province']?.toInt() ?? 0,
      gender: map['gender']?.toInt() ?? 0,
      status: map['status']?.toInt() ?? 0,
      isUser: map['isUser'] ?? false,
      avatar: map['avatar'] != null ? AvatarModel.fromMap(map['avatar']) : null,
      workingExperience: ((map['workingExperience'] ?? []) as List)
          .map((item) => WorkingExperience.fromMap(item))
          .toList(),
      experienceConfirm: map['experienceConfirm'] ?? null,
      languages:
          ((map['languages'] ?? []) as List).map((item) => LanguageModel.fromMap(item)).toList(),
      specialties:
          ((map['specialties'] ?? []) as List).map((item) => SpecialtyModel.fromMap(item)).toList(),
      skills: ((map['skills'] ?? []) as List)
          .map((item) => SkillModel.fromMapInfoExpert(item))
          .toList(),
      timeline: [],
      wallet: map['wallet']?.toDouble() ?? 0.0,
      stepInfo: map['stepInfo']?.toInt() ?? 0,
      ratingCount: map['ratingCount']?.toInt() ?? 0,
      stars: double.parse(double.parse((map['stars'] ?? 0).toString()).toStringAsFixed(1)),
      companyName: map['companyName'] ?? '',
      experienceYears: map['experienceYears']?.toInt() ?? 0,
      highestPosition: map['highestPosition']?['_id'] ?? '',
      introduce: map['introduce'] ?? '',
      callPrice: map['callPrice'] != null ? BudgetModel.fromMap(map['callPrice']) : null,
      meetPrice: map['meetPrice'] != null ? BudgetModel.fromMap(map['meetPrice']) : null,
      ratingSingleCounts:
          map['ratingSingleCounts'] != null ? List<int>.from(map["ratingSingleCounts"]) : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory ExpertModel.fromJson(String source) => ExpertModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'ExpertModel(id: $id, expertname: $expertname, fullname: $fullname, province: $province, gender: $gender, status: $status, isUser: $isUser, avatar: $avatar, workingExperience: $workingExperience, experienceConfirm: $experienceConfirm, languages: $languages, specialties: $specialties, skills: $skills, timeline: $timeline, wallet: $wallet, stepInfo: $stepInfo, ratingCount: $ratingCount, stars: $stars, companyName: $companyName, experienceYears: $experienceYears, highestPosition: $highestPosition, introduce: $introduce)';
  }

  String query(String? categoryid, String? specialtyId) {
    List<String> result = [];

    List<String> keys = ['category', 'specialty'];
    List<String> values = [categoryid!, specialtyId!];

    values.asMap().forEach((index, value) {
      if (value.toString().isNotEmpty) {
        result.add('${keys[index]}=$value');
      }
    });

    if (result.isEmpty) {
      return '';
    }

    result.add('');

    return result.join('&');
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is ExpertModel &&
        other.id == id &&
        other.expertname == expertname &&
        other.fullname == fullname &&
        other.province == province &&
        other.gender == gender &&
        other.status == status &&
        other.isUser == isUser &&
        other.avatar == avatar &&
        listEquals(other.workingExperience, workingExperience) &&
        other.experienceConfirm == experienceConfirm &&
        listEquals(other.languages, languages) &&
        listEquals(other.specialties, specialties) &&
        listEquals(other.skills, skills) &&
        listEquals(other.timeline, timeline) &&
        other.wallet == wallet &&
        other.stepInfo == stepInfo &&
        other.ratingCount == ratingCount &&
        other.stars == stars &&
        other.companyName == companyName &&
        other.experienceYears == experienceYears &&
        other.highestPosition == highestPosition &&
        other.introduce == introduce;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        expertname.hashCode ^
        fullname.hashCode ^
        province.hashCode ^
        gender.hashCode ^
        status.hashCode ^
        isUser.hashCode ^
        avatar.hashCode ^
        workingExperience.hashCode ^
        experienceConfirm.hashCode ^
        languages.hashCode ^
        specialties.hashCode ^
        skills.hashCode ^
        timeline.hashCode ^
        wallet.hashCode ^
        stepInfo.hashCode ^
        ratingCount.hashCode ^
        stars.hashCode ^
        companyName.hashCode ^
        experienceYears.hashCode ^
        highestPosition.hashCode ^
        introduce.hashCode;
  }

  String get languagesTile {
    List<String> result = [];

    result = (languages ?? []).map((e) => e.getLocaleTitle).toList();

    return result.join(', ');
  }

  String get highgestPositionTile {
    String result = '';
    List<PositionModel> positions = AppBloc.skillBloc.positions;

    int indexOfPosition = positions.indexWhere((position) => position.id == highestPosition);

    if (indexOfPosition != -1) {
      result = positions[indexOfPosition].getLocaleTitle ?? '';
    }

    return result;
  }

  String get expertSpecialist {
    if (specialties.isNotEmpty) {
      return specialties.first.getLocaleTitle ?? '';
    }

    return '';
  }
}
