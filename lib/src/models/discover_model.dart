import 'dart:convert';

class DiscoverConvertedModel {
  final String title;
  final String descriptions;
  final Content content;
  final dynamic item;
  final String image;
  final DateTime date;
  final int duration;
  DiscoverConvertedModel({
    required this.title,
    required this.descriptions,
    required this.content,
    required this.item,
    required this.image,
    required this.date,
    required this.duration,
  });
}

class DiscoverOriginModel {
  DiscoverOriginModel({
    required this.id,
    required this.name,
    required this.page,
    required this.titles,
    required this.descriptions,
    required this.contents,
    required this.items,
    required this.images,
    required this.lang,
    required this.createdAt,
    required this.modifiedAt,
    required this.v,
  });

  final String id;
  final String name;
  final String page;
  final List<String> titles;
  final List<String> descriptions;
  final List<Content> contents;
  final List items;
  final List<DiscoverImage> images;
  final String lang;
  final DateTime createdAt;
  final DateTime modifiedAt;
  final int v;

  factory DiscoverOriginModel.fromJson(String str) =>
      DiscoverOriginModel.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory DiscoverOriginModel.fromMap(Map<String, dynamic> json) =>
      DiscoverOriginModel(
        id: json["_id"],
        name: json["name"],
        page: json["page"],
        titles: List<String>.from(json["titles"].map((x) => x)),
        descriptions: List<String>.from(json["descriptions"].map((x) => x)),
        contents:
            List<Content>.from(json["contents"].map((x) => Content.fromMap(x))),
        items: List<dynamic>.from(json["items"].map((x) => x)),
        images: List<DiscoverImage>.from(
            json["images"].map((x) => DiscoverImage.fromMap(x))),
        lang: json["lang"],
        createdAt: DateTime.parse(json["createdAt"]),
        modifiedAt: DateTime.parse(json["modifiedAt"]),
        v: json["__v"],
      );

  Map<String, dynamic> toMap() => {
        "_id": id,
        "name": name,
        "page": page,
        "titles": List<dynamic>.from(titles.map((x) => x)),
        "descriptions": List<dynamic>.from(descriptions.map((x) => x)),
        "contents": List<dynamic>.from(contents.map((x) => x.toMap())),
        "items": List<dynamic>.from(items.map((x) => x)),
        "images": List<dynamic>.from(images.map((x) => x.toMap())),
        "lang": lang,
        "createdAt": createdAt.toIso8601String(),
        "modifiedAt": modifiedAt.toIso8601String(),
        "__v": v,
      };
}

class Content {
  Content({
    required this.id,
    required this.fullname,
  });

  final String id;
  final String fullname;

  factory Content.fromJson(String str) => Content.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Content.fromMap(Map<String, dynamic> json) => Content(
        id: json["_id"],
        fullname: json["fullname"],
      );

  Map<String, dynamic> toMap() => {
        "_id": id,
        "fullname": fullname,
      };
}

class DiscoverImage {
  DiscoverImage({
    required this.id,
    required this.name,
    required this.src,
    required this.createdAt,
    required this.modifiedAt,
    required this.v,
  });

  final String id;
  final String name;
  final String src;
  final DateTime createdAt;
  final DateTime modifiedAt;
  final int v;

  factory DiscoverImage.fromJson(String str) =>
      DiscoverImage.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory DiscoverImage.fromMap(Map<String, dynamic> json) => DiscoverImage(
        id: json["_id"],
        name: json["name"],
        src: json["src"],
        createdAt: DateTime.parse(json["createdAt"]),
        modifiedAt: DateTime.parse(json["modifiedAt"]),
        v: json["__v"],
      );

  Map<String, dynamic> toMap() => {
        "_id": id,
        "name": name,
        "src": src,
        "createdAt": createdAt.toIso8601String(),
        "modifiedAt": modifiedAt.toIso8601String(),
        "__v": v,
      };
}
