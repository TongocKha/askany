import 'dart:convert';

import 'package:flutter/foundation.dart';

import 'package:askany/src/models/avatar_model.dart';

class MessagesImageModel {
  final String id;
  final String conversation;
  final List<AvatarModel> images;
  dynamic parent;
  final int status;
  final int index;
  MessagesImageModel({
    required this.id,
    required this.conversation,
    required this.images,
    required this.parent,
    required this.status,
    required this.index,
  });

  MessagesImageModel copyWith({
    String? id,
    String? conversation,
    List<AvatarModel>? images,
    dynamic parent,
    int? status,
    int? index,
  }) {
    return MessagesImageModel(
      id: id ?? this.id,
      conversation: conversation ?? this.conversation,
      images: images ?? this.images,
      parent: parent ?? this.parent,
      status: status ?? this.status,
      index: index ?? this.index,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'conversation': conversation,
      'images': images.map((x) => x.toMap()).toList(),
      'parent': parent,
      'status': status,
      'index': index,
    };
  }

  factory MessagesImageModel.fromMap(Map<String, dynamic> map) {
    return MessagesImageModel(
      id: map['_id'] ?? '',
      conversation: map['conversation'] ?? '',
      images: ((map['images'] as List?) ?? [])
          .where((item) => item is Map)
          .map((item) => AvatarModel.fromMap(item))
          .toList(),
      parent: map['parent'] ?? null,
      status: map['status']?.toInt() ?? 0,
      index: map['index']?.toInt() ?? 0,
    );
  }

  String toJson() => json.encode(toMap());

  factory MessagesImageModel.fromJson(String source) =>
      MessagesImageModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'MessagesImageModel(id: $id, conversation: $conversation, images: $images, parent: $parent, status: $status, index: $index)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is MessagesImageModel &&
        other.id == id &&
        other.conversation == conversation &&
        listEquals(other.images, images) &&
        other.parent == parent &&
        other.status == status &&
        other.index == index;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        conversation.hashCode ^
        images.hashCode ^
        parent.hashCode ^
        status.hashCode ^
        index.hashCode;
  }
}
