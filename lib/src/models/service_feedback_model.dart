import 'dart:convert';

class RatingModel {
  RatingModel({
    required this.id,
    required this.stars,
    this.expert,
    required this.content,
    this.authorUser,
    this.reply,
    this.parent,
    required this.createdAt,
    required this.modifiedAt,
  });

  final String id;
  final int stars;
  final Expert? expert;
  final String content;
  final AuthorUser? authorUser;
  Reply? reply;
  final dynamic parent;
  final DateTime createdAt;
  final DateTime modifiedAt;

  factory RatingModel.fromJson(String str) =>
      RatingModel.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory RatingModel.fromMap(Map<String, dynamic> json) => RatingModel(
        id: json["_id"],
        stars: json["stars"],
        expert: Expert.fromMap(json["expert"]),
        content: json["content"],
        authorUser: AuthorUser.fromMap(json["authorUser"]),
        reply: json["reply"] == null ? null : Reply.fromMap(json["reply"]),
        parent: json["parent"],
        createdAt: DateTime.parse(json["createdAt"]),
        modifiedAt: DateTime.parse(json["modifiedAt"]),
      );

  Map<String, dynamic> toMap() => {
        "_id": id,
        "stars": stars,
        "expert": expert?.toMap(),
        "content": content,
        "authorUser": authorUser?.toMap(),
        "reply": reply?.toMap(),
        "parent": parent,
        "createdAt": createdAt.toIso8601String(),
        "modifiedAt": modifiedAt.toIso8601String(),
      };
}

class AuthorUser {
  AuthorUser({
    required this.id,
    required this.fullname,
    required this.avatar,
    required this.province,
  });

  final String id;
  final String fullname;
  final Avatar avatar;
  final int province;

  factory AuthorUser.fromJson(String str) =>
      AuthorUser.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory AuthorUser.fromMap(Map<String, dynamic> json) => AuthorUser(
        id: json["_id"],
        fullname: json["fullname"],
        avatar: Avatar.fromMap(json["avatar"]),
        province: json["province"],
      );

  Map<String, dynamic> toMap() => {
        "_id": id,
        "fullname": fullname,
        "avatar": avatar.toMap(),
        "province": province,
      };
}

class Avatar {
  Avatar({
    required this.id,
    required this.name,
    required this.src,
  });

  final String id;
  final String name;
  final String src;

  factory Avatar.fromJson(String str) => Avatar.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Avatar.fromMap(Map<String, dynamic> json) => Avatar(
        id: json["_id"],
        name: json["name"],
        src: json["src"],
      );

  Map<String, dynamic> toMap() => {
        "_id": id,
        "name": name,
        "src": src,
      };
}

class Expert {
  Expert({
    required this.id,
    required this.fullname,
    required this.province,
    required this.avatar,
    required this.ratingCount,
    required this.stars,
  });

  final String id;
  final String fullname;
  final int province;
  final Avatar avatar;
  final int ratingCount;
  final String stars;

  factory Expert.fromJson(String str) => Expert.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Expert.fromMap(Map<String, dynamic> json) => Expert(
        id: json["_id"],
        fullname: json["fullname"],
        province: json["province"],
        avatar: Avatar.fromMap(json["avatar"]),
        ratingCount: json["ratingCount"],
        stars: json["stars"].toString(),
      );

  Map<String, dynamic> toMap() => {
        "_id": id,
        "fullname": fullname,
        "province": province,
        "avatar": avatar.toMap(),
        "ratingCount": ratingCount,
        "stars": stars,
      };
}

class Reply {
  Reply({
    this.id,
    this.expert,
    required this.content,
    this.parent,
    required this.createdAt,
    this.modifiedAt,
    this.v,
  });

  final String? id;
  final String? expert;
  final String content;
  final String? parent;
  final DateTime createdAt;
  final DateTime? modifiedAt;
  final int? v;

  factory Reply.fromJson(String str) => Reply.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Reply.fromMap(Map<String, dynamic> json) => Reply(
        id: json["_id"],
        expert: json["expert"],
        content: json["content"],
        parent: json["parent"],
        createdAt: DateTime.parse(json["createdAt"]),
        modifiedAt: DateTime.parse(json["modifiedAt"]),
        v: json["__v"],
      );

  Map<String, dynamic> toMap() => {
        "_id": id,
        "expert": expert,
        "content": content,
        "parent": parent,
        "createdAt": createdAt.toIso8601String(),
        "modifiedAt": modifiedAt?.toIso8601String(),
        "__v": v,
      };
}
