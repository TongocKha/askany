import 'package:askany/src/helpers/utils/logger.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class LocationWidgetModel {
  double posittionX, posittionY, posittionRight, posittionLeft, posittionTop, posittionBottom;
  int checkStatus = 0;

  // status = 1 default
  void setLocationWidgetModel(int status, double height, double width) {
    final Size maxSize = Size(100.w, 100.h);
    final Size videoCallWidgetSize = Size(25.w, 40.w);
    final double horizontalPadding = 30.sp;
    final double ip8PlusDisplayHeight = 736.0;
    if (status == 1) {
      if (checkStatus == 0 || checkStatus == 2) {
        this.posittionX = maxSize.width - videoCallWidgetSize.width - horizontalPadding;
        this.posittionY = 0;
        checkStatus = 1;
      }

      this.posittionLeft = 0;
      this.posittionTop = 0;
      posittionRight = maxSize.width - videoCallWidgetSize.width - horizontalPadding;
      this.posittionBottom = maxSize.height > ip8PlusDisplayHeight
          ? maxSize.height - videoCallWidgetSize.height - 92.sp - AppBar().preferredSize.height
          : maxSize.height - videoCallWidgetSize.height - 48.sp - AppBar().preferredSize.height;
    } else if (status == 2) {
      if (checkStatus == 1 || checkStatus == 0) {
        this.posittionX = 100.h - 40.w - 30.sp - 70.sp + 5.sp;
        this.posittionY = 0 + 5.sp;
        checkStatus = 2;
      }

      this.posittionLeft = 0 + 70.sp - 10.sp;
      this.posittionTop = 0 + 5.sp;
      this.posittionRight = 100.h - 40.w - 30.sp - 70.sp + 5.sp;
      // this.posittionBottom =  100.w - 25.w - 30.sp -20.sp -30.sp ;
      this.posittionBottom = 100.w - 40.w - 45.sp;
    } else if (status == 3) {
      //videocalling full screen portrait
      // this.posittionX = 54.w;
      // this.posittionY = 0;
      // this.posittionLeft = 0;
      // this.posittionTop = 0;
      // this.posittionRight = 54.w;
      // this.posittionBottom = 85.h;
      checkStatus = 0;
      this.posittionX = 100.w - 25.w - 30.sp;
      this.posittionY = 0 + 10.sp;
      this.posittionLeft = 0;
      this.posittionTop = 0 + 10.sp;
      this.posittionRight = 100.w - 25.w - 30.sp;
      this.posittionBottom = 100.h - 40.w - 30.sp - 5.sp;
    } else if (status == 4) {
      //videocalling full screen lanscape
      checkStatus = 0;
      this.posittionX = 100.h - 40.w - 30.sp + 5.sp;
      this.posittionY = 0 + 5.sp;

      this.posittionLeft = 0 - 10.sp;
      this.posittionTop = 0 + 5.sp;
      this.posittionRight = 100.h - 40.w - 30.sp + 5.sp;
      // this.posittionBottom =  100.w - 25.w - 30.sp -20.sp -30.sp ;
      this.posittionBottom = 100.w - 25.w - 10.sp - 15.sp - 5.sp;
    }
  }

  void toStringLocation() {
    UtilLogger.log(
        "POSITION",
        "posittionX: $posittionX      " +
            "posittionY:$posittionY      " +
            "posittionLeft:$posittionLeft     " +
            "posittionTop:$posittionTop     " +
            "posittionRight:$posittionRight     " +
            "posittionBottom:$posittionBottom     ");
  }

  LocationWidgetModel(
      {required this.posittionX,
      required this.posittionY,
      required this.posittionLeft,
      required this.posittionTop,
      required this.posittionRight,
      required this.posittionBottom});
}
