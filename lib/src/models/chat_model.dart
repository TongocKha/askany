import 'dart:convert';

class ChatModel {
  final String id;
  final bool pendingMessage;
  final String urlToImage;
  final String fullName;
  final String lastMessage;
  final DateTime createdAt;
  final bool notification;
  ChatModel({
    required this.id,
    required this.pendingMessage,
    required this.urlToImage,
    required this.fullName,
    required this.lastMessage,
    required this.createdAt,
    required this.notification,
  });

  ChatModel copyWith({
    String? id,
    bool? pendingMessage,
    String? urlToImage,
    String? fullName,
    String? lastMessage,
    DateTime? createdAt,
    bool? notification,
  }) {
    return ChatModel(
      id: id ?? this.id,
      pendingMessage: pendingMessage ?? this.pendingMessage,
      urlToImage: urlToImage ?? this.urlToImage,
      fullName: fullName ?? this.fullName,
      lastMessage: lastMessage ?? this.lastMessage,
      createdAt: createdAt ?? this.createdAt,
      notification: notification ?? this.notification,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'pendingMessage': pendingMessage,
      'urlToImage': urlToImage,
      'fullName': fullName,
      'lastMessage': lastMessage,
      'createdAt': createdAt.millisecondsSinceEpoch,
      'notification': notification,
    };
  }

  factory ChatModel.fromMap(Map<String, dynamic> map) {
    return ChatModel(
      id: map['id'] ?? '',
      pendingMessage: map['pendingMessage'] ?? false,
      urlToImage: map['urlToImage'] ?? '',
      fullName: map['fullName'] ?? '',
      lastMessage: map['lastMessage'] ?? '',
      createdAt: DateTime.fromMillisecondsSinceEpoch(map['createdAt']),
      notification: map['notification'] ?? false,
    );
  }

  String toJson() => json.encode(toMap());

  factory ChatModel.fromJson(String source) => ChatModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'ChatModel(id: $id, pendingMessage: $pendingMessage, urlToImage: $urlToImage, fullName: $fullName, lastMessage: $lastMessage, createdAt: $createdAt, notification: $notification)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is ChatModel &&
        other.id == id &&
        other.pendingMessage == pendingMessage &&
        other.urlToImage == urlToImage &&
        other.fullName == fullName &&
        other.lastMessage == lastMessage &&
        other.createdAt == createdAt &&
        other.notification == notification;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        pendingMessage.hashCode ^
        urlToImage.hashCode ^
        fullName.hashCode ^
        lastMessage.hashCode ^
        createdAt.hashCode ^
        notification.hashCode;
  }
}
