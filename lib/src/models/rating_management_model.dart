import 'dart:convert';

import 'package:flutter/foundation.dart';

import 'package:askany/src/models/rating_model.dart';

class RatingManagementModel {
  final List<RatingModel> ratings;
  bool isOver;
  RatingManagementModel({
    required this.ratings,
    required this.isOver,
  });

  RatingManagementModel copyWith({
    List<RatingModel>? ratings,
    bool? isOver,
  }) {
    return RatingManagementModel(
      ratings: ratings ?? this.ratings,
      isOver: isOver ?? this.isOver,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'ratings': ratings.map((x) => x.toMap()).toList(),
      'isOver': isOver,
    };
  }

  factory RatingManagementModel.fromMap(Map<String, dynamic> map) {
    return RatingManagementModel(
      ratings: List<RatingModel>.from(map['ratings']?.map((x) => RatingModel.fromMap(x))),
      isOver: map['isOver'] ?? false,
    );
  }

  String toJson() => json.encode(toMap());

  factory RatingManagementModel.fromJson(String source) =>
      RatingManagementModel.fromMap(json.decode(source));

  @override
  String toString() => 'RatingManagementModel(ratings: $ratings, isOver: $isOver)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is RatingManagementModel &&
        listEquals(other.ratings, ratings) &&
        other.isOver == isOver;
  }

  @override
  int get hashCode => ratings.hashCode ^ isOver.hashCode;
}

class RatingStoreModel {
  RatingManagementModel one;
  RatingManagementModel two;
  RatingManagementModel three;
  RatingManagementModel four;
  RatingManagementModel five;
  RatingStoreModel({
    required this.one,
    required this.two,
    required this.three,
    required this.four,
    required this.five,
  });

  RatingStoreModel copyWith({
    RatingManagementModel? one,
    RatingManagementModel? two,
    RatingManagementModel? three,
    RatingManagementModel? four,
    RatingManagementModel? five,
  }) {
    return RatingStoreModel(
      one: one ?? this.one,
      two: two ?? this.two,
      three: three ?? this.three,
      four: four ?? this.four,
      five: five ?? this.five,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'one': one.toMap(),
      'two': two.toMap(),
      'three': three.toMap(),
      'four': four.toMap(),
      'five': five.toMap(),
    };
  }

  factory RatingStoreModel.fromAllRatings(List<RatingModel> ratings, bool isOver) {
    List<RatingModel> one = ratings.where((e) => e.stars == 1).toList();
    List<RatingModel> two = ratings.where((e) => e.stars == 2).toList();
    List<RatingModel> three = ratings.where((e) => e.stars == 3).toList();
    List<RatingModel> four = ratings.where((e) => e.stars == 4).toList();
    List<RatingModel> five = ratings.where((e) => e.stars == 5).toList();

    return RatingStoreModel(
      one: RatingManagementModel(
        isOver: isOver,
        ratings: one,
      ),
      two: RatingManagementModel(
        isOver: isOver,
        ratings: two,
      ),
      three: RatingManagementModel(
        isOver: isOver,
        ratings: three,
      ),
      four: RatingManagementModel(
        isOver: isOver,
        ratings: four,
      ),
      five: RatingManagementModel(
        isOver: isOver,
        ratings: five,
      ),
    );
  }

  RatingManagementModel get joinRatings {
    List<RatingModel> result = [];

    result.addAll(one.ratings);
    result.addAll(two.ratings);
    result.addAll(three.ratings);
    result.addAll(four.ratings);
    result.addAll(five.ratings);

    return RatingManagementModel(
      ratings: result,
      isOver: one.isOver && two.isOver && three.isOver && four.isOver && five.isOver,
    );
  }

  RatingManagementModel getRatingsByStars(int stars) {
    switch (stars) {
      case 1:
        return one;
      case 2:
        return two;
      case 3:
        return three;
      case 4:
        return four;
      case 5:
        return five;
      default:
        return joinRatings;
    }
  }

  bool get isOver => one.isOver && two.isOver && three.isOver && four.isOver && five.isOver;

  String toJson() => json.encode(toMap());

  // factory RatingStoreModel.fromJson(String source) => RatingStoreModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'RatingStoreModel(one: $one, two: $two, three: $three, four: $four, five: $five)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is RatingStoreModel &&
        other.one == one &&
        other.two == two &&
        other.three == three &&
        other.four == four &&
        other.five == five;
  }

  @override
  int get hashCode {
    return one.hashCode ^ two.hashCode ^ three.hashCode ^ four.hashCode ^ five.hashCode;
  }
}
