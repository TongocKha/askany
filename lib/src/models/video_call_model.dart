import 'package:flutter_webrtc/flutter_webrtc.dart' as RTC;

class VideoCallModel {
  final RTC.RTCVideoRenderer localRenderer;
  final RTC.RTCVideoRenderer remoteRenderer;
  final RTC.RTCVideoRenderer localShareRenderer;
  final RTC.RTCVideoRenderer remoteShareRenderer;
  final bool isFrontCamera;
  final bool isSharingScreen;
  final bool micEnabled;
  final bool soundMyShare;
  final bool cameraEnabled;
  final bool isMyShare;
  final bool isVidCallFullScreen;
  final bool isShareFullScreen;
  final bool isMyRaisingHand;
  final bool isRemoteRaisingHand;
  final bool isRemoteOnMic;
  final bool isRemoteOnCamera;
  VideoCallModel({
    required this.localRenderer,
    required this.remoteRenderer,
    required this.localShareRenderer,
    required this.remoteShareRenderer,
    required this.isFrontCamera,
    required this.isSharingScreen,
    required this.micEnabled,
    required this.soundMyShare,
    required this.cameraEnabled,
    required this.isMyShare,
    required this.isVidCallFullScreen,
    required this.isShareFullScreen,
    required this.isMyRaisingHand,
    required this.isRemoteOnCamera,
    required this.isRemoteOnMic,
    required this.isRemoteRaisingHand,
  });
}
