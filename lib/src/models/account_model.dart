import 'dart:convert';

import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/models/avatar_model.dart';
import 'package:askany/src/models/experience_confirm_model.dart';
import 'package:askany/src/helpers/extentions/string_extention.dart';

class AccountModel {
  final String? id;
  String? phone;
  final String? username;
  String? fullname;
  String? email;
  final int? gender;
  final int? status;
  AvatarModel? avatar;
  int? province;
  final bool? isExpert;
  final String? createdAt;
  final String? modifiedAt;
  double? wallet;
  bool? isVerifyPhone;
  bool? isVerifiedCompany;
  bool? isVerifiedEmail;
  ExperienceConfirmModel? experienceConfirm;
  bool? isSocial;
  AccountModel({
    this.id,
    this.phone,
    this.username,
    this.fullname,
    this.email,
    this.gender,
    this.status,
    this.avatar,
    this.province,
    this.isExpert,
    this.createdAt,
    this.modifiedAt,
    this.wallet,
    this.isVerifyPhone,
    this.isVerifiedCompany,
    this.isVerifiedEmail,
    this.experienceConfirm,
    this.isSocial,
  });

  AccountModel copyWith({
    String? id,
    String? phone,
    String? username,
    String? fullname,
    String? email,
    String? token,
    int? gender,
    int? status,
    String? avatar,
    int? province,
    bool? isExpert,
    String? createdAt,
    String? modifiedAt,
    double? wallet,
    bool? isVerifyPhone,
    bool? isVerifiedCompany,
    bool? isVerifiedEmail,
    String? experienceConfirm,
  }) {
    return AccountModel(
      id: id ?? this.id,
      phone: phone ?? this.phone,
      username: username ?? this.username,
      fullname: fullname ?? this.fullname,
      email: email ?? this.email,
      gender: gender ?? this.gender,
      status: status ?? this.status,
      avatar: this.avatar,
      province: province ?? this.province,
      isExpert: isExpert ?? this.isExpert,
      createdAt: createdAt ?? this.createdAt,
      modifiedAt: modifiedAt ?? this.modifiedAt,
      wallet: wallet ?? this.wallet,
      isVerifyPhone: isVerifyPhone ?? this.isVerifyPhone,
      isVerifiedCompany: isVerifiedCompany ?? this.isVerifiedCompany,
      isVerifiedEmail: isVerifiedEmail ?? this.isVerifiedEmail,
      experienceConfirm: this.experienceConfirm,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'username': username,
      'phone': phone,
      'fullname': fullname,
      'email': email,
      'gender': gender,
      'status': status,
      'avatar': avatar == null ? null : avatar!.toMap(),
      'province': province,
      'isExpert': isExpert,
      'createdAt': createdAt,
      'modifiedAt': modifiedAt,
      'wallet': wallet,
      'isVerifyPhone': isVerifyPhone,
      'isVerifiedCompany': isVerifiedCompany,
      'isVerifiedEmail': isVerifiedEmail,
      'experienceConfirm': experienceConfirm == null ? null : experienceConfirm!.toMap(),
    };
  }

  Map<String, dynamic> toMapUserNotification() {
    return {
      '_id': id,
      'fullname': fullname,
      'avatar': avatar == null ? null : avatar!.toMap(),
    };
  }

  factory AccountModel.fromMap(Map<String, dynamic> map) {
    return AccountModel(
      id: map['_id'],
      phone: map['phone'] ?? '',
      username: map['username'] ?? '',
      fullname: map['fullname'] ?? '',
      email: map['email'] ?? '',
      gender: map['gender'] ?? 2,
      status: map['status'] ?? 1,
      avatar: map['avatar'] == null || map['avatar'] is String
          ? null
          : AvatarModel.fromMap(map['avatar']),
      province: map['province'] ?? 1,
      isExpert: UserLocal().getIsExpert(),
      createdAt: map['createdAt'],
      modifiedAt: map['modifiedAt'],
      wallet: double.parse((map['wallet'] ?? 0.0).toString()),
      isVerifyPhone: map['isVerifyPhone'] ?? false,
      isVerifiedCompany: map['isVerifiedCompany'] ?? false,
      isVerifiedEmail: map['isVerifiedEmail'] ?? false,
      experienceConfirm: map['experienceConfirm'] == null || map['experienceConfirm'] is String
          ? null
          : ExperienceConfirmModel.fromMap(map['experienceConfirm']),
      isSocial: map['isSocial'] ?? false,
    );
  }

  factory AccountModel.fromUpdateUser(Map<String, dynamic> map) {
    return AccountModel(
      id: map['_id'],
      phone: map['phone'] ?? '',
      username: map['username'] ?? '',
      fullname: map['fullname'] ?? '',
      email: map['email'] ?? '',
      gender: map['gender'] ?? 2,
      status: map['status'] ?? 1,
      avatar: map['avatar'] == null ? null : AvatarModel.fromMap(map['avatar']),
      province: map['province'] ?? 1,
      isExpert: UserLocal().getIsExpert(),
      createdAt: map['createdAt'],
      modifiedAt: map['modifiedAt'],
      wallet: double.parse((map['wallet'] ?? 0.0).toString()),
      isVerifyPhone: map['isVerifyPhone'] ?? false,
      isVerifiedCompany: map['isVerifiedCompany'] ?? false,
      isVerifiedEmail: map['isVerifiedEmail'] ?? false,
      experienceConfirm: map['experienceConfirm'] == null
          ? null
          : ExperienceConfirmModel.fromMap(map['experienceConfirm']),
    );
  }

  String toJson() => json.encode(toMap());

  factory AccountModel.fromJson(String source) => AccountModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'AccountModel(id: $id, phone: $phone, username: $username, fullname: $fullname, email: $email, gender: $gender, status: $status, avatar: $avatar, province: $province, isExpert: $isExpert, isVerifyPhone: $isVerifyPhone, isVerifiedCompany: $isVerifiedCompany,, isVerifiedEmail: $isVerifiedEmail createdAt: $createdAt, modifiedAt: $modifiedAt, wallet: $wallet, experienceConfirm: $experienceConfirm)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is AccountModel &&
        other.id == id &&
        other.phone == phone &&
        other.username == username &&
        other.fullname == fullname &&
        other.email == email &&
        other.gender == gender &&
        other.status == status &&
        other.avatar == avatar &&
        other.province == province &&
        other.isExpert == isExpert &&
        other.createdAt == createdAt &&
        other.modifiedAt == modifiedAt &&
        other.wallet == wallet &&
        other.isVerifyPhone == isVerifyPhone &&
        other.isVerifiedCompany == isVerifiedCompany &&
        other.isVerifiedCompany == isVerifiedEmail &&
        other.experienceConfirm == experienceConfirm;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        phone.hashCode ^
        username.hashCode ^
        fullname.hashCode ^
        email.hashCode ^
        gender.hashCode ^
        status.hashCode ^
        avatar.hashCode ^
        province.hashCode ^
        isExpert.hashCode ^
        createdAt.hashCode ^
        modifiedAt.hashCode ^
        wallet.hashCode ^
        isVerifyPhone.hashCode ^
        isVerifiedCompany.hashCode ^
        isVerifiedEmail.hashCode ^
        experienceConfirm.hashCode;
  }

  String get walletString {
    return (wallet ?? 0.0).toStringAsFixed(0).formatMoney() + 'đ';
  }
}
