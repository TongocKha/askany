import 'dart:convert';

import 'package:askany/src/helpers/extentions/string_extention.dart';
import 'package:askany/src/models/account_model.dart';

class TransactionModel {
  final String id;
  int status;
  final double amount;
  final RequestTransModel? request;
  final AccountModel? user;
  final AccountModel? expert;
  dynamic admin;
  int type;
  final DateTime createdAt;
  TransactionModel({
    required this.id,
    required this.status,
    required this.amount,
    this.request,
    this.user,
    this.expert,
    this.admin,
    required this.type,
    required this.createdAt,
  });

  String get costString {
    return amount.toStringAsFixed(0).formatMoney() + 'đ';
  }

  TransactionModel copyWith({
    String? id,
    int? status,
    double? amount,
    RequestTransModel? request,
    AccountModel? user,
    AccountModel? expert,
    dynamic admin,
    int? type,
    DateTime? createdAt,
  }) {
    return TransactionModel(
      id: id ?? this.id,
      status: status ?? this.status,
      amount: amount ?? this.amount,
      request: request ?? this.request,
      user: user ?? this.user,
      expert: expert ?? this.expert,
      admin: admin ?? this.admin,
      type: type ?? this.type,
      createdAt: createdAt ?? this.createdAt,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'status': status,
      'amount': amount,
      'request': request?.toMap(),
      'user': user?.toMap(),
      'expert': expert?.toMap(),
      'admin': admin,
      'type': type,
      'createdAt': createdAt.millisecondsSinceEpoch,
    };
  }

  factory TransactionModel.fromMap(Map<String, dynamic> map) {
    return TransactionModel(
      id: map['_id'] ?? '',
      status: map['status']?.toInt() ?? 0,
      amount: map['amount']?.toDouble() ?? 0.0,
      request: map['request'] != null ? RequestTransModel.fromMap(map['request']) : null,
      user: map['user'] != null ? AccountModel.fromMap(map['user']) : null,
      expert: map['expert'] != null ? AccountModel.fromMap(map['expert']) : null,
      admin: map['admin'] ?? null,
      type: map['type']?.toInt() ?? 0,
      createdAt: DateTime.parse(map['createdAt']).toLocal(),
    );
  }

  String toJson() => json.encode(toMap());

  factory TransactionModel.fromJson(String source) => TransactionModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'TransactionModel(id: $id, status: $status, amount: $amount, request: $request, user: $user, expert: $expert, admin: $admin, type: $type, createdAt: $createdAt)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is TransactionModel &&
        other.id == id &&
        other.status == status &&
        other.amount == amount &&
        other.request == request &&
        other.user == user &&
        other.expert == expert &&
        other.admin == admin &&
        other.type == type &&
        other.createdAt == createdAt;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        status.hashCode ^
        amount.hashCode ^
        request.hashCode ^
        user.hashCode ^
        expert.hashCode ^
        admin.hashCode ^
        type.hashCode ^
        createdAt.hashCode;
  }
}

class RequestTransModel {
  final String id;
  final String title;
  RequestTransModel({
    required this.id,
    required this.title,
  });

  RequestTransModel copyWith({
    String? id,
    String? title,
  }) {
    return RequestTransModel(
      id: id ?? this.id,
      title: title ?? this.title,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'title': title,
    };
  }

  factory RequestTransModel.fromMap(Map<String, dynamic> map) {
    return RequestTransModel(
      id: map['_id'] ?? '',
      title: map['title'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory RequestTransModel.fromJson(String source) =>
      RequestTransModel.fromMap(json.decode(source));

  @override
  String toString() => 'RequestTransModel(id: $id, title: $title)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is RequestTransModel && other.id == id && other.title == title;
  }

  @override
  int get hashCode => id.hashCode ^ title.hashCode;
}
