import 'dart:convert';

class ExperienceConfirmModel {
  final String reason;
  final int status;
  ExperienceConfirmModel({required this.reason, required this.status});
  ExperienceConfirmModel copyWith({
    String? reason,
    int? status,
  }) {
    return ExperienceConfirmModel(
      reason: reason ?? this.reason,
      status: status ?? this.status,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'reason': reason,
      'status': status,
    };
  }

  Map<String, dynamic> toMapSkill() {
    return {
      'reason': reason,
      'status': status,
    };
  }

  factory ExperienceConfirmModel.fromMap(Map<String, dynamic> map) {
    return ExperienceConfirmModel(
      reason: map['reason'] ?? '',
      status: int.parse((map['status'] ?? 0).toString()),
    );
  }

  factory ExperienceConfirmModel.fromMapSkill() {
    return ExperienceConfirmModel(
      reason: '',
      status: 2,
    );
  }
  String toJon() => json.encode(toMap());
  String toJsonSkill() => json.encode(toMapSkill());
  factory ExperienceConfirmModel.fromJson(String source) =>
      ExperienceConfirmModel.fromMap(json.decode(source));

  @override
  String toString() => 'ExperienceModel(reason: $reason, status: $status)';
  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
    return other is ExperienceConfirmModel && other.reason == reason && other.status == status;
  }

  @override
  int get hashCode => reason.hashCode ^ status.hashCode;
}
