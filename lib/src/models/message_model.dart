import 'dart:convert';
import 'dart:io';
import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/models/avatar_model.dart';
import 'package:askany/src/models/message_offer_model.dart';
import 'package:flutter/material.dart';

class MessageModel {
  final String id;
  final String conversation;
  final String userSender;
  MessageModel? parent;
  String data;
  final int isSeen;
  int status;
  final DateTime createdAt;
  final DateTime modifiedAt;
  bool isSending;
  bool isError;
  final int typeSystem;
  List<AvatarModel>? images;
  List<File>? localImages;
  final MessageOfferModel? request;
  final int? index;
  AnimationController? animationController;

  MessageModel({
    required this.id,
    required this.conversation,
    required this.userSender,
    required this.data,
    required this.isSeen,
    required this.status,
    required this.createdAt,
    required this.modifiedAt,
    this.parent,
    this.localImages,
    this.images,
    this.isSending = false,
    this.isError = false,
    this.typeSystem = 0,
    this.request,
    this.index,
  });

  MessageModel copyWith({
    String? id,
    String? conversation,
    String? userSender,
    String? data,
    int? isSeen,
    int? status,
    DateTime? createdAt,
    DateTime? modifiedAt,
    List<AvatarModel>? images,
    GlobalKey? globalKey,
    bool? isActiveAnimation,
    int? index,
  }) {
    return MessageModel(
      id: id ?? this.id,
      conversation: conversation ?? this.conversation,
      userSender: userSender ?? this.userSender,
      data: data ?? this.data,
      isSeen: isSeen ?? this.isSeen,
      status: status ?? this.status,
      createdAt: createdAt ?? this.createdAt,
      modifiedAt: modifiedAt ?? this.modifiedAt,
      images: images ?? this.images,
      index: index ?? this.index,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'conversation': conversation,
      'userSender': userSender,
      'data': data,
      'isSeen': isSeen,
      'status': status,
      'createdAt': createdAt.toString(),
      'modifiedAt': modifiedAt.toString(),
      'isSending': isSending,
      'isError': isError,
      'index': index,
    };
  }

  Map<String, dynamic> toMapPayloadNotification() {
    return {
      '_id': id,
      'conversation': conversation,
      'userSender': userSender,
      'data': data,
      'isSeen': isSeen,
      'status': status,
      'createdAt': createdAt.toString(),
      'modifiedAt': modifiedAt.toString(),
      'isSending': isSending,
      'isError': isError,
      'route': 'chat',
    };
  }

  Map<String, dynamic> toMapSendSocket() {
    Map<String, dynamic> result = {
      '_id': id,
      'conversation': conversation,
      'userSender': userSender,
      'data': data,
      'isSeen': isSeen,
      'status': status,
      'createdAt': createdAt.toString(),
      'modifiedAt': modifiedAt.toString(),
      'typeSystem': typeSystem,
      'images': images?.map((img) => img.toMap()).toList(),
      'parent': parent?.toMap(),
    };

    if (request != null) {
      result['offer'] = request!.toMap();
    }

    return result;
  }

  Map<String, dynamic> toMapSendSocketUpdate() {
    return {
      '_id': id,
      'conversation': conversation,
      'data': data,
    };
  }

  Map<String, dynamic> toMapSendSocketDelete() {
    return {
      '_id': id,
      'conversation': conversation,
    };
  }

  factory MessageModel.fromMap(Map<String, dynamic> map) {
    bool isDeleted = map['status'] == -1;

    return MessageModel(
      id: map['_id'] ?? '',
      conversation: map['conversation'] ?? '',
      userSender: map['userSender'] ?? (map['expertSender'] ?? ''),
      isSeen: map['isSeen']?.toInt() ?? 0,
      status: map['status']?.toInt() ?? 1,
      createdAt: DateTime.parse(map['createdAt']).toLocal(),
      modifiedAt: DateTime.parse(map['modifiedAt']).toLocal(),
      isError: map['isError'] ?? false,
      isSending: map['isSending'] ?? false,
      data: isDeleted ? 'Tin nhắn đã thu hồi' : map['data'] ?? '',
      request: (map['typeSystem'] ?? 0) > 0 && map['offer'] != null
          ? MessageOfferModel.fromMap(map['offer'])
          : null,
      images: ((map['images'] as List?) ?? [])
          .where((item) => item is Map)
          .map((item) => AvatarModel.fromMap(item))
          .toList(),
      parent: map['parent'] == null || map['parent'] is String
          ? null
          : MessageModel.fromMap(map['parent']),
      index: map['index']?.toInt() ?? null,
    );
  }

  String toJson() => json.encode(toMap());

  factory MessageModel.fromJson(String source) => MessageModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'MessageModel(id: $id, conversation: $conversation, userSender: $userSender, data: $data, isSeen: $isSeen, status: $status, createdAt: $createdAt, modifiedAt: $modifiedAt, index: $index)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is MessageModel &&
        other.id == id &&
        other.conversation == conversation &&
        other.userSender == userSender &&
        other.data == data &&
        other.isSeen == isSeen &&
        other.status == status &&
        other.createdAt == createdAt &&
        other.modifiedAt == modifiedAt &&
        other.index == index;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        conversation.hashCode ^
        userSender.hashCode ^
        data.hashCode ^
        isSeen.hashCode ^
        status.hashCode ^
        createdAt.hashCode ^
        modifiedAt.hashCode;
  }

  bool get isMe => userSender == UserLocal().getUser().id;

  bool get isDeleted => status == -1;

  String get fullName {
    int indexOfConversation = AppBloc.chatBloc.conversations.indexWhere(
      (chat) => chat.id == conversation,
    );

    if (indexOfConversation != -1) {
      return AppBloc.chatBloc.conversations[indexOfConversation].users.isEmpty
          ? ''
          : AppBloc.chatBloc.conversations[indexOfConversation].users.first.fullname ?? '';
    }
    return '';
  }

  bool get hasImage =>
      images != null && localImages != null && localImages!.isNotEmpty && images!.isNotEmpty;
}
