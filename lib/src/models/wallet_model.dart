import 'dart:convert';

import 'package:askany/src/models/account_model.dart';

class WalletModel {
  WalletModel({
    this.status,
    required this.amount,
    this.orderId,
    this.offer,
    this.request,
    this.user,
    this.expert,
    this.admin,
    this.images,
    this.transactionParent,
    this.type,
    this.createdAt,
    this.modifiedAt,
    this.id,
    this.v,
  });

  final int? status;
  final int amount;
  final dynamic orderId;
  final dynamic offer;
  final dynamic request;
  final AccountModel? user;
  final AccountModel? expert;
  final dynamic admin;
  final List<dynamic>? images;
  final dynamic transactionParent;
  final int? type;
  final DateTime? createdAt;
  final DateTime? modifiedAt;
  final String? id;
  final int? v;

  factory WalletModel.fromJson(String str) =>
      WalletModel.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory WalletModel.fromMap(Map<String, dynamic> json) => WalletModel(
        status: json["status"],
        amount: json["amount"],
        orderId: json["orderID"],
        offer: json["offer"],
        request: json["request"],
        user: json["user"],
        expert: json["expert"],
        admin: json["admin"],
        images: List<dynamic>.from(json["images"].map((x) => x)),
        transactionParent: json["transactionParent"],
        type: json["type"],
        createdAt: DateTime.parse(json["createdAt"]),
        modifiedAt: DateTime.parse(json["modifiedAt"]),
        id: json["_id"],
        v: json["__v"],
      );

  Map<String, dynamic> toMap() => {
        "status": status,
        "amount": amount,
        "orderID": orderId,
        "offer": offer,
        "request": request,
        "user": user,
        "expert": expert,
        "admin": admin,
        "images": List<dynamic>.from(images!.map((x) => x)),
        "transactionParent": transactionParent,
        "type": type,
        "createdAt": createdAt!.toIso8601String(),
        "modifiedAt": modifiedAt!.toIso8601String(),
        "_id": id,
        "__v": v,
      };
}

var body = {};
