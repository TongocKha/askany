import 'dart:convert';

enum MessageCallType {
  MissedCall,
  EndCall,
}

class MessageCallModel {
  final MessageCallType type;
  final DateTime startTime;
  final DateTime endTime;
  MessageCallModel({
    required this.type,
    required this.startTime,
    required this.endTime,
  });

  MessageCallModel copyWith({
    MessageCallType? type,
    DateTime? startTime,
    DateTime? endTime,
  }) {
    return MessageCallModel(
      type: type ?? this.type,
      startTime: startTime ?? this.startTime,
      endTime: endTime ?? this.endTime,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'type': type.toString(),
      'startTime': startTime.millisecondsSinceEpoch,
      'endTime': endTime.millisecondsSinceEpoch,
    };
  }

  factory MessageCallModel.fromMap(Map<String, dynamic> map) {
    return MessageCallModel(
      type: map['type'] == 1 ? MessageCallType.MissedCall : MessageCallType.EndCall,
      startTime: DateTime.fromMillisecondsSinceEpoch(map['startTime']),
      endTime: DateTime.fromMillisecondsSinceEpoch(map['endTime']),
    );
  }

  String toJson() => json.encode(toMap());

  factory MessageCallModel.fromJson(String source) => MessageCallModel.fromMap(json.decode(source));

  @override
  String toString() => 'MessageCallModel(type: $type, startTime: $startTime, endTime: $endTime)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is MessageCallModel &&
        other.type == type &&
        other.startTime == startTime &&
        other.endTime == endTime;
  }

  @override
  int get hashCode => type.hashCode ^ startTime.hashCode ^ endTime.hashCode;
}
