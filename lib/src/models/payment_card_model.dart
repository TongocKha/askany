import 'package:askany/src/models/account_model.dart';
import 'dart:convert';

class PaymentCardModel {
  PaymentCardModel({
    required this.id,
    this.authorUser,
    required this.bankCode,
    required this.cardNumber,
    this.v,
    required this.accountName,
    required this.bankName,
    this.billAddress,
    this.cardType,
    this.city,
    this.country,
    this.postOfficeCode,
    this.status,
  });

  final String id;
  final AccountModel? authorUser;
  final String bankCode;
  final String cardNumber;
  final int? v;
  final String accountName;
  final String bankName;
  final String? billAddress;
  final int? cardType;
  final String? city;
  final String? country;
  final int? postOfficeCode;
  final int? status;

  factory PaymentCardModel.fromJson(String str) => PaymentCardModel.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory PaymentCardModel.fromMap(Map<String, dynamic> json) => PaymentCardModel(
        id: json["_id"],
        authorUser: json["authorUser"] == null ? null : AccountModel.fromMap(json["authorUser"]),
        bankCode: json["bankCode"],
        cardNumber: json["cardNumber"],
        v: json["__v"],
        accountName: json["accountName"],
        bankName: json["bankName"],
        billAddress: json["billAddress"],
        cardType: json["cardType"],
        city: json["city"],
        country: json["country"],
        postOfficeCode: json["postOfficeCode"],
        status: json["status"],
      );

  Map<String, dynamic> toMap() => {
        "_id": id,
        "authorUser": authorUser!.toMap(),
        "bankCode": bankCode,
        "cardNumber": cardNumber,
        "__v": v,
        "accountName": accountName,
        "bankName": bankName,
        "billAddress": billAddress,
        "cardType": cardType,
        "city": city,
        "country": country,
        "postOfficeCode": postOfficeCode,
        "status": status,
      };
}
