
import 'package:flutter/material.dart';

class StringModel {
  final String text;
  final FontWeight fontWeight;
  StringModel({
    required this.text,
    required this.fontWeight,
  });

  StringModel copyWith({
    String? text,
    FontWeight? fontWeight,
  }) {
    return StringModel(
      text: text ?? this.text,
      fontWeight: fontWeight ?? this.fontWeight,
    );
  }
}
