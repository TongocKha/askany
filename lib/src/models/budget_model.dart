import 'dart:convert';
import 'package:askany/src/helpers/extentions/string_extention.dart';
import 'package:intl/intl.dart';

class BudgetModel {
  final String? id;
  final String currency;
  final double cost;
  final int totalMinutes;

  BudgetModel({
    this.id,
    required this.currency,
    required this.cost,
    required this.totalMinutes,
  });

  BudgetModel copyWith({
    String? currency,
    double? cost,
    int? totalMinutes,
  }) {
    return BudgetModel(
      currency: currency ?? this.currency,
      cost: cost ?? this.cost,
      totalMinutes: totalMinutes ?? this.totalMinutes,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'currency': currency,
      'cost': cost.toString(),
      'totalMinutes': totalMinutes.toString(),
      '_id': id.toString(),
    };
  }

  Map<String, dynamic> toMapSkill() {
    return {
      'currency': currency,
      'cost': cost,
      'totalMinutes': totalMinutes,
    };
  }

  Map<String, dynamic> toMapUpdate() {
    return {
      '_id': id ?? '',
      'currency': currency,
      'cost': cost,
      'totalMinutes': totalMinutes,
    };
  }

  factory BudgetModel.fromMap(Map<String, dynamic> map) {
    return BudgetModel(
      id: map['_id'] ?? '',
      currency: map['currency'] ?? '',
      cost: double.parse(((map['cost'] ?? 0).toString())),
      totalMinutes: int.parse((map['totalMinutes'] ?? 0).toString()),
    );
  }
  factory BudgetModel.fromMapSkill() {
    return BudgetModel(
      currency: '',
      cost: 0,
      totalMinutes: 0,
    );
  }
  String toJson() => json.encode(toMapSkill());
  String toJsonSkill() => json.encode(toMapSkill());
  String toJsonUpdate() => json.encode(toMapUpdate());
  factory BudgetModel.fromJson(String source) => BudgetModel.fromMap(json.decode(source));

  @override
  String toString() => 'BudgetModel(currency: $currency, cost: $cost, totalMinutes: $totalMinutes)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is BudgetModel &&
        other.currency == currency &&
        other.cost == cost &&
        other.totalMinutes == totalMinutes;
  }

  @override
  int get hashCode => currency.hashCode ^ cost.hashCode ^ totalMinutes.hashCode;

  String get costString {
    if (currency == 'usd') {
      NumberFormat format = NumberFormat('#,###.##');
      return format.format(cost) + '\$';
    }

    return cost.toStringAsFixed(0).formatMoney() + 'đ';
  }

  String get costString15Minutes {
    double costFormat = (cost / totalMinutes) * 15;

    if (currency == 'usd') {
      NumberFormat format = NumberFormat('#,###.##');
      return format.format(costFormat) + '\$';
    }

    return costFormat.toStringAsFixed(0).formatMoney() + 'đ';
  }

  String get costString60Minutes {
    double costFormat = (cost / totalMinutes) * 60;

    if (currency == 'usd') {
      NumberFormat format = NumberFormat('#,###.##');
      return format.format(costFormat) + '\$';
    }

    return costFormat.toStringAsFixed(0).formatMoney() + 'đ';
  }

  String get costStringWithoutCurrency {
    if (currency == 'usd') {
      NumberFormat format = NumberFormat('#,###.##');
      return format.format(cost);
    }

    return cost.toStringAsFixed(0).formatMoney(splitBy: ',');
  }

  String priceString(double price) {
    if (currency == 'usd') {
      NumberFormat format = NumberFormat('#,###.##');
      return format.format(price);
    }

    return price.toStringAsFixed(0).formatMoney() + 'đ';
  }
}
