import 'dart:convert';

import 'package:askany/src/constants/constants.dart';
import 'package:flutter/foundation.dart';

import 'package:askany/src/models/expert_timeline_model.dart';

class WeeklyScheduleModel {
  final String id;
  final int dayOfWeek;
  final DateTime date;
  final List<TimelineModel> timesMeet;
  final List<TimelineModel> timesCall;
  WeeklyScheduleModel({
    required this.id,
    required this.dayOfWeek,
    required this.date,
    required this.timesMeet,
    required this.timesCall,
  });

  WeeklyScheduleModel copyWith({
    String? id,
    int? dayOfWeek,
    DateTime? date,
    List<TimelineModel>? timesMeet,
    List<TimelineModel>? timesCall,
  }) {
    return WeeklyScheduleModel(
      id: id ?? this.id,
      dayOfWeek: dayOfWeek ?? this.dayOfWeek,
      date: date ?? this.date,
      timesMeet: timesMeet ?? this.timesMeet,
      timesCall: timesCall ?? this.timesCall,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'dayOfWeek': dayOfWeek,
      'date': date.toString(),
      'timesMeet': timesMeet.map((x) => x.toMap()).toList(),
      'timesCall': timesCall.map((x) => x.toMap()).toList(),
    };
  }

  factory WeeklyScheduleModel.fromMap(Map<String, dynamic>? map) {
    if (map == null) {
      DateTime date = DateTime.now();
      return WeeklyScheduleModel(
        id: '',
        dayOfWeek: date.weekday == 7 ? 0 : date.weekday,
        date: date,
        timesMeet: [],
        timesCall: [],
      );
    }

    return WeeklyScheduleModel(
      id: map['_id'] ?? '',
      dayOfWeek: map['dayOfWeek']?.toInt() ?? 0,
      date: DateTime.parse(map['date'] ?? DATE_TIME_DEFAULT).toLocal(),
      timesMeet: List<TimelineModel>.from(
          map['timesMeet']?.map((x) => TimelineModel.fromExpertTimeline(x))),
      timesCall: List<TimelineModel>.from(
          map['timesCall']?.map((x) => TimelineModel.fromExpertTimeline(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory WeeklyScheduleModel.fromJson(String source) =>
      WeeklyScheduleModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'WeeklyScheduleModel(id: $id, dayOfWeek: $dayOfWeek, date: $date, timesMeet: $timesMeet, timesCall: $timesCall)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is WeeklyScheduleModel &&
        other.id == id &&
        other.dayOfWeek == dayOfWeek &&
        other.date == date &&
        listEquals(other.timesMeet, timesMeet) &&
        listEquals(other.timesCall, timesCall);
  }

  @override
  int get hashCode {
    return id.hashCode ^
        dayOfWeek.hashCode ^
        date.hashCode ^
        timesMeet.hashCode ^
        timesCall.hashCode;
  }
}
