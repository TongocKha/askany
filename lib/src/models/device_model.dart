import 'dart:convert';

class DeviceModel {
  final String appVersion;
  final String deviceModel;
  final String deviceUuid;
  final String fcmToken;
  final String apnsToken;
  final String createdBy;
  DeviceModel({
    required this.appVersion,
    required this.deviceModel,
    required this.deviceUuid,
    required this.fcmToken,
    required this.createdBy,
    required this.apnsToken,
  });

  DeviceModel copyWith({
    String? appVersion,
    String? deviceModel,
    String? deviceUuid,
    String? fcmToken,
    String? apnsToken,
    String? createdBy,
  }) {
    return DeviceModel(
      appVersion: appVersion ?? this.appVersion,
      deviceModel: deviceModel ?? this.deviceModel,
      deviceUuid: deviceUuid ?? this.deviceUuid,
      fcmToken: fcmToken ?? this.fcmToken,
      createdBy: createdBy ?? this.createdBy,
      apnsToken: apnsToken ?? this.apnsToken,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'appVersion': appVersion,
      'deviceModel': deviceModel,
      'deviceUuid': deviceUuid,
      'fcmToken': fcmToken,
      // 'createdBy': createdBy,
      'apnsToken': apnsToken,
    };
  }

  factory DeviceModel.fromMap(Map<String, dynamic> map) {
    return DeviceModel(
      appVersion: map['appVersion'] ?? '',
      deviceModel: map['deviceModel'] ?? '',
      deviceUuid: map['deviceUuid'] ?? '',
      fcmToken: map['fcmToken'] ?? '',
      createdBy: map['createdBy'] ?? '',
      apnsToken: map['apnsToken'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory DeviceModel.fromJson(String source) => DeviceModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'DeviceModel(appVersion: $appVersion, deviceModel: $deviceModel, deviceUuid: $deviceUuid, fcmToken: $fcmToken, apnsToken: $apnsToken, createdBy: $createdBy,)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is DeviceModel &&
        other.appVersion == appVersion &&
        other.deviceModel == deviceModel &&
        other.deviceUuid == deviceUuid &&
        other.fcmToken == fcmToken &&
        other.createdBy == createdBy;
  }

  @override
  int get hashCode {
    return appVersion.hashCode ^
        deviceModel.hashCode ^
        deviceUuid.hashCode ^
        fcmToken.hashCode ^
        createdBy.hashCode;
  }
}
