import 'dart:convert';

import 'package:askany/src/models/avatar_model.dart';
import 'package:askany/src/models/specialty_model.dart';

class ContentModel {
  final String id;
  final String name;
  final String slug;
  final String lang;
  final SpecialtyModel? specialty;
  final AvatarModel? thumbnail;
  ContentModel({
    required this.id,
    required this.name,
    required this.slug,
    required this.lang,
    required this.specialty,
    this.thumbnail,
  });

  ContentModel copyWith({
    String? id,
    String? name,
    String? slug,
    String? lang,
    SpecialtyModel? specialty,
    AvatarModel? thumbnail,
  }) {
    return ContentModel(
      id: id ?? this.id,
      name: name ?? this.name,
      slug: slug ?? this.slug,
      lang: lang ?? this.lang,
      specialty: specialty ?? this.specialty,
      thumbnail: thumbnail ?? this.thumbnail,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'name': name,
      'slug': slug,
      'lang': lang,
      'specialty': specialty?.toMap(),
      'thumbnail': thumbnail?.toMap(),
    };
  }

  factory ContentModel.fromMap(Map<String, dynamic> map) {
    return ContentModel(
      id: map['_id'] ?? '',
      name: map['name'] ?? '',
      slug: map['slug'] ?? '',
      lang: map['lang'] ?? '',
      specialty: map['specialty'] != null
          ? SpecialtyModel.fromMapCategory(map['specialty'])
          : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory ContentModel.fromJson(String source) =>
      ContentModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'ContentModel(id: $id, name: $name, slug: $slug, lang: $lang, specialty: $specialty)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is ContentModel &&
        other.id == id &&
        other.name == name &&
        other.slug == slug &&
        other.lang == lang &&
        other.specialty == specialty;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        name.hashCode ^
        slug.hashCode ^
        lang.hashCode ^
        specialty.hashCode;
  }
}
