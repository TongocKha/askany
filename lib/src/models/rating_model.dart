import 'dart:convert';

import 'package:askany/src/models/author_model.dart';
import 'package:askany/src/models/reply_model.dart';

class RatingModel {
  final String id;
  final double stars;
  final AuthorModel? expert;
  final String content;
  final AuthorModel? authorUser;
  final String offer;
  final String skill;
  final ReplyModel? reply;
  final String? parent;
  final DateTime? createdAt;
  final DateTime? modifiedAt;
  RatingModel({
    required this.id,
    required this.stars,
    this.expert,
    required this.content,
    this.authorUser,
    required this.offer,
    required this.skill,
    this.reply,
    this.parent,
    this.createdAt,
    this.modifiedAt,
  });

  RatingModel copyWith({
    String? id,
    double? stars,
    AuthorModel? expert,
    String? content,
    AuthorModel? authorUser,
    String? offer,
    String? skill,
    ReplyModel? reply,
    String? parent,
    DateTime? createdAt,
    DateTime? modifiedAt,
  }) {
    return RatingModel(
      id: id ?? this.id,
      stars: stars ?? this.stars,
      expert: expert ?? this.expert,
      content: content ?? this.content,
      authorUser: authorUser ?? this.authorUser,
      offer: offer ?? this.offer,
      skill: skill ?? this.skill,
      reply: reply ?? this.reply,
      parent: parent ?? this.parent,
      createdAt: createdAt ?? this.createdAt,
      modifiedAt: modifiedAt ?? this.modifiedAt,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'stars': stars,
      'expert': expert?.toMap(),
      'content': content,
      'authorUser': authorUser?.toMap(),
      'offer': offer,
      'skill': skill,
      'reply': reply?.toMap(),
      'parent': parent,
      'createdAt': createdAt.toString(),
      'modifiedAt': modifiedAt.toString(),
    };
  }

  factory RatingModel.fromMap(Map<String, dynamic> map) {
    return RatingModel(
      id: map['_id'] ?? '',
      stars: double.parse((map['stars'] ?? 0).toString()),
      expert: map['expert'] != null ? AuthorModel.fromMap(map['expert']) : null,
      content: map['content'] ?? '',
      authorUser: map['authorUser'] != null ? AuthorModel.fromMap(map['authorUser']) : null,
      offer: map['offer'] ?? '',
      skill: map['skill'] ?? '',
      reply: map['reply'] != null ? ReplyModel.fromMap(map['reply']) : null,
      parent: map['parent'] ?? null,
      createdAt: DateTime.parse(map['createdAt']),
      modifiedAt: DateTime.parse(map['modifiedAt']),
    );
  }

  String toJson() => json.encode(toMap());

  factory RatingModel.fromJson(String source) => RatingModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'RatingModel(id: $id, stars: $stars, expert: $expert, content: $content, authorUser: $authorUser, offer: $offer, skill: $skill, reply: $reply, parent: $parent, createdAt: $createdAt, modifiedAt: $modifiedAt)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is RatingModel &&
        other.id == id &&
        other.stars == stars &&
        other.expert == expert &&
        other.content == content &&
        other.authorUser == authorUser &&
        other.offer == offer &&
        other.skill == skill &&
        other.reply == reply &&
        other.parent == parent &&
        other.createdAt == createdAt &&
        other.modifiedAt == modifiedAt;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        stars.hashCode ^
        expert.hashCode ^
        content.hashCode ^
        authorUser.hashCode ^
        offer.hashCode ^
        skill.hashCode ^
        reply.hashCode ^
        parent.hashCode ^
        createdAt.hashCode ^
        modifiedAt.hashCode;
  }
}
