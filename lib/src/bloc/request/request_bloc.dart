import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/home/home_bloc.dart';
import 'package:askany/src/bloc/message/message_bloc.dart';
import 'package:askany/src/bloc/notification/notification_bloc.dart';
import 'package:askany/src/bloc/service_managament/service_management_bloc.dart';
import 'package:askany/src/bloc/user/user_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/data/local_data_source/request_local_data.dart';
import 'package:askany/src/data/remote_data_source/booking_repository.dart';
import 'package:askany/src/data/remote_data_source/request_repository.dart';
import 'package:askany/src/data/remote_data_source/specialty_repository.dart';
import 'package:askany/src/helpers/extentions/string_extention.dart';
import 'package:askany/src/models/author_model.dart';
import 'package:askany/src/models/budget_model.dart';
import 'package:askany/src/models/category_model.dart';
import 'package:askany/src/models/expert_model.dart';
import 'package:askany/src/models/message_offer_model.dart';
import 'package:askany/src/models/offer_model.dart';
import 'package:askany/src/models/offer_skill_model.dart';
import 'package:askany/src/models/request_model.dart';
import 'package:askany/src/models/skill_model.dart';
import 'package:askany/src/models/slide_mode.dart';
import 'package:askany/src/models/specialty_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_with_text_and_pop_button.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
part 'request_event.dart';
part 'request_state.dart';

class RequestBloc extends Bloc<RequestEvent, RequestState> {
  RequestBloc() : super(RequestHappeningInitial());

  List<RequestModel> requestsHappening = [];
  List<RequestModel> requestsFinished = [];
  List<RequestModel> requestsCommon = [];

  List<CategoryModel> categories = [];
  List<SpecialtyModel> specialties = [];
  List<SpecialtyModel> mySpecialties = [];

  bool isOverRequestsHappening = false;
  bool isOverRequestsFinished = false;
  bool isOverRequestCommon = false;

  bool isOverCategories = false;
  bool isOverSpecialties = false;
  bool isMyOverSpecialties = false;

  Map<String, String> draftMessages = {};
  String searchKey = '';
  String cost = '0-1000000000';
  CategoryModel? category;

  Map<String, OfferSkillModel> offerSkillModel = {};
  Map<String, RequestModel> requestModel = {};

  @override
  Stream<RequestState> mapEventToState(RequestEvent event) async* {
    if (event is ExpertCreateOfferEvent) {
      await _expertCreateOffer(event);
      if (state is GetDoneRequestHappening ||
          state is GettingRequestHappening) {
        yield _requestDoneHappeningState;
      } else if (state is GettingRequestFinished ||
          state is GetDoneRequestFinished) {
        yield _requestDoneFinishedState;
      } else {
        yield _requestDoneCommonState;
      }
    }

    if (event is GetRequestCommonEvent) {
      if (!isOverRequestCommon) {
        yield _requestCommonState;
        await _getRequestCommon();
        yield _requestDoneCommonState;
      }
    }

    if (event is RefreshRequestFinishEvent) {
      await _refreshRequestFinish(event);
      yield _requestDoneFinishedState;
    }

    if (event is RefreshRequestHappeningEvent) {
      await _refreshRequestHappening(event);
      yield _requestDoneHappeningState;
    }

    if (event is RefreshRequestCommonEvent) {
      await _refreshRequestCommon(event);
      yield _requestDoneCommonState;
    }

    if (event is GetSpecialtyEvent) {
      specialties.clear();
      _getSpecialtiesLocal();
      if (!isOverSpecialties) {
        await _getSpecialties();
      }
    }

    if (event is GetMySpecialtyEvent) {
      await _getMySpecialties();
    }

    if (event is OnHappeningEvent) {
      yield RequestHappeningInitial();
      if (requestsHappening.isEmpty) {
        await _getRequestByStatus(REQUEST_HAPPENING_STRING);
      }
      yield _requestDoneHappeningState;
    }

    if (event is OnFinishedEvent) {
      yield RequestFinishedInitial();
      if (requestsFinished.isEmpty) {
        await _getRequestByStatus(REQUEST_FINISHED_STRING);
      }
      yield _requestDoneFinishedState;
    }

    if (event is OnCommonEvent) {
      if (requestsCommon.isEmpty) {
        yield RequestCommonInitial();
        await _getRequestCommon();
      }
      yield _requestDoneCommonState;
    }

    if (event is GetRequestHappeningEvent) {
      if (!isOverRequestsHappening) {
        yield _requestHappeningState;
        await _getRequestByStatus(REQUEST_HAPPENING_STRING);
        yield _requestDoneHappeningState;
      }
    }

    if (event is GetRequestFinishedEvent) {
      if (!isOverRequestsFinished) {
        yield _requestFinishedState;
        await _getRequestByStatus(REQUEST_FINISHED_STRING);
        yield _requestDoneFinishedState;
      }
    }

    if (event is CreateRequestEvent) {
      bool isCreateSucceed = await _createRequest(event);
      if (isCreateSucceed) {
        if (event.isNavigationFromSearch) {
          AppBloc.homeBloc.add(OnChangeIndexEvent(index: 1));
          AppNavigator.popUntil(Routes.ROOT);
        } else {
          AppNavigator.pop();
        }
      }
      if (state is GetDoneRequestHappening ||
          state is GettingRequestHappening) {
        yield _requestDoneHappeningState;
      } else if (state is GettingRequestFinished ||
          state is GetDoneRequestFinished) {
        yield _requestDoneFinishedState;
      } else {
        yield _requestDoneCommonState;
      }
    }

    if (event is UpdateRequestEvent) {
      bool isUpdateSucceed = await _updateRequest(event);
      if (isUpdateSucceed) {
        AppNavigator.pop();
      }
      if (state is GetDoneRequestHappening ||
          state is GettingRequestHappening) {
        yield _requestDoneHappeningState;
      } else if (state is GettingRequestFinished ||
          state is GetDoneRequestFinished) {
        yield _requestDoneFinishedState;
      } else {
        yield _requestDoneCommonState;
      }
    }

    if (event is DeleteRequestEvent) {
      bool isDeleteSucceed = await _deleteRequest(event);
      if (isDeleteSucceed) {
        if (state is GetDoneRequestHappening ||
            state is GettingRequestHappening) {
          yield _requestDoneHappeningState;
        } else if (state is GettingRequestFinished ||
            state is GetDoneRequestFinished) {
          yield _requestDoneFinishedState;
        } else {
          yield _requestDoneCommonState;
        }
        AppNavigator.popUntil(Routes.ROOT);
      }
    }

    if (event is ExpertCancelOfferEvent) {
      bool isCancelSucceed = await _expertCancelOffer(event);
      if (isCancelSucceed) {
        if (state is GetDoneRequestHappening ||
            state is GettingRequestHappening) {
          yield _requestDoneHappeningState;
        } else if (state is GettingRequestFinished ||
            state is GetDoneRequestFinished) {
          yield _requestDoneFinishedState;
        } else {
          yield _requestDoneCommonState;
        }
      }
    }

    if (event is UserChooseOfferEvent) {
      bool _userChooseOfferSuccess = await _userChooseOffer(event);
      if (_userChooseOfferSuccess) {
        if (state is GetDoneRequestHappening ||
            state is GettingRequestHappening) {
          yield _requestDoneHappeningState;
        } else if (state is GettingRequestFinished ||
            state is GetDoneRequestFinished) {
          yield _requestDoneFinishedState;
        } else {
          yield _requestDoneCommonState;
        }
      }
    }

    if (event is UserDoneStep1Event) {
      bool isUpdateSucceed = await _userDoneStep1(event);
      if (isUpdateSucceed) {
        if (state is GetDoneRequestHappening ||
            state is GettingRequestHappening) {
          yield _requestDoneHappeningState;
        } else if (state is GettingRequestFinished ||
            state is GetDoneRequestFinished) {
          yield _requestDoneFinishedState;
        } else {
          yield _requestDoneCommonState;
        }
      }
    }

    if (event is UserDoneStep2Event) {
      bool isUpdateSucceed = await _userDoneStep2(event);
      if (isUpdateSucceed) {
        if (state is GetDoneRequestHappening ||
            state is GettingRequestHappening) {
          yield _requestDoneHappeningState;
        } else if (state is GettingRequestFinished ||
            state is GetDoneRequestFinished) {
          yield _requestDoneFinishedState;
        } else {
          yield _requestDoneCommonState;
        }
      }
    }

    if (event is UserPayOfferEvent) {
      final bool _userPayOfferSuccess = await _userPayOffer(event);
      if (_userPayOfferSuccess) {
        if (event.paymentMethod == PaymentMethod.byAppWallet) {
          AppBloc.userBloc.add(
            UpdateUserWalletEvent(updatedWallet: event.updatedWallet),
          );
        }
        yield _requestDoneHappeningState;
      }
    }

    if (event is UserPayOfferSkillEvent) {
      final bool _userPayOfferSuccess = await _userPayOfferSkill(event);
      if (_userPayOfferSuccess) {
        if (event.paymentMethod == PaymentMethod.byAppWallet) {
          AppBloc.userBloc.add(
            UpdateUserWalletEvent(updatedWallet: event.updatedWallet),
          );
        }
        if (state is GetDoneRequestHappening ||
            state is GettingRequestHappening) {
          yield _requestDoneHappeningState;
        } else if (state is GettingRequestFinished ||
            state is GetDoneRequestFinished) {
          yield _requestDoneFinishedState;
        } else {
          yield _requestDoneCommonState;
        }
      }
    }

    if (event is UserEndRequestEvent) {
      bool isRemoveSucceed = await _userEndRequest(event);
      if (isRemoveSucceed) {
        if (state is GetDoneRequestHappening ||
            state is GettingRequestHappening) {
          yield _requestDoneHappeningState;
        } else if (state is GettingRequestFinished ||
            state is GetDoneRequestFinished) {
          yield _requestDoneFinishedState;
        } else {
          yield _requestDoneCommonState;
        }
        AppNavigator.popUntil(Routes.ROOT);
      }
    }

    if (event is ContinuePaymentOfferEvent) {
      await _continuePaymentOffer(event);
    }

    if (event is FilterRequestEvent) {
      searchKey = event.searchKey;
      category = event.category != null ? event.category : category;
      cost = event.cost;
      requestsCommon.clear();
      isOverRequestCommon = false;
      if (requestsCommon.isEmpty) {
        yield RequestCommonInitial();
      } else {
        yield _requestCommonState;
      }

      if (!isOverRequestCommon) {
        await _getRequestCommon();
      }

      yield _requestDoneCommonState;
    }

    if (event is BookingByTargetExpertEvent) {
      showDialogLoading();
      yield RequestCommonInitial();
      final bool _result = await _bookingByTargetExpert(event: event);

      if (_result) {
        if (event.paymentMethod == PaymentMethod.byAppWallet) {
          AppBloc.userBloc.add(
            UpdateUserWalletEvent(updatedWallet: event.updatedWallet),
          );
        }

        yield _requestDoneHappeningState;
      }

      yield _requestFinishedState;
    }
    if (event is MoveBackToOriginRequestListEvent) {
      AppNavigator.pop();
      yield _requestDoneCommonState;
    }

    if (event is CleanRequestEvent) {
      _cleanBloc();
      yield RequestHappeningInitial();
    }

    if (event is CleanFilterRequestEvent) {
      searchKey = '';
      cost = '0-1000000000';
      category = null;
      requestsCommon.clear();
      yield _requestDoneCommonState;
    }
  }

  // States
  GettingRequestHappening get _requestHappeningState =>
      GettingRequestHappening(requests: requestsHappening);

  GettingRequestFinished get _requestFinishedState =>
      GettingRequestFinished(requests: requestsFinished);

  GettingRequestCommon get _requestCommonState =>
      GettingRequestCommon(requests: getFilterListRequest);

  GetDoneRequestHappening get _requestDoneHappeningState =>
      GetDoneRequestHappening(requests: requestsHappening);

  GetDoneRequestFinished get _requestDoneFinishedState =>
      GetDoneRequestFinished(requests: requestsFinished);

  GetDoneRequestCommon get _requestDoneCommonState =>
      GetDoneRequestCommon(requests: getFilterListRequest);

  // Private methods
  List<RequestModel> get getFilterListRequest {
    _filterDuplicateRequest();
    requestsCommon.sort((a, b) => b.createdAt!.compareTo(a.createdAt!));

    if (searchKey.isEmpty) {
      return requestsCommon;
    }

    List searchKeys = searchKey.toLowerCase().formatVietnamese().split(' ');
    List<RequestModel> result = requestsCommon.where(
      (e) {
        bool allMatches = searchKeys.every(
          (key) => e.title.toLowerCase().formatVietnamese().contains(key),
        );

        return allMatches;
      },
    ).toList();
    return result;
  }

  _filterDuplicateRequest() {
    requestsCommon = requestsCommon.toSet().toList();
  }

  Future<void> _expertCreateOffer(ExpertCreateOfferEvent event) async {
    String? offerId = await RequestRepository().expertCreateOffer(
      request: event.requestModel,
      price: event.budgetModel,
      content: event.content,
    );

    AppNavigator.pop();

    if (offerId != null) {
      OfferModel offerModel = OfferModel(
        id: offerId,
        authorExpert: AuthorExpertModel(
          id: AppBloc.userBloc.getAccount.id!,
          fullname: AppBloc.userBloc.getAccount.fullname ?? '',
          province: AppBloc.userBloc.getAccount.province ?? 0,
        ),
        price: event.budgetModel,
        content: event.content,
        status: OFFER_ACTIVE,
        startTime: DateTime.now(),
        endTime: DateTime.now(),
      );

      int indexOfRequest = requestsCommon.indexWhere(
        (request) => request.id == event.requestModel.id,
      );

      if (indexOfRequest != -1) {
        requestsCommon[indexOfRequest].offerers.add(offerModel);
      }

      if (state is GetDoneRequestHappening) {
        event.requestModel.offerers.add(offerModel);
        requestsHappening.insert(0, event.requestModel);
      }

      AppBloc.notificationBloc.add(
        CreateNotificationEvent(
          messageType: OFFERED_REQUEST,
          userId: event.requestModel.authorUser?.id ?? '',
          requestId: event.requestModel.id,
          offerId: offerId,
        ),
      );

      dialogAnimationWrapper(
        borderRadius: 10.sp,
        slideFrom: SlideMode.bot,
        child: DialogWithTextAndPopButton(
            title: Strings.successfulBidNoti.i18n,
            bodyBefore: Strings.successfulBidCongratulations.i18n),
      );
    } else {
      dialogAnimationWrapper(
        borderRadius: 10.sp,
        slideFrom: SlideMode.bot,
        child: DialogWithTextAndPopButton(
          title: Strings.failedBid.i18n,
          bodyBefore: Strings.failedBidTryAgain.i18n,
        ),
      );
    }
  }

  Future<void> _refreshRequestHappening(
      RefreshRequestHappeningEvent event) async {
    requestsHappening.clear();
    isOverRequestsHappening = false;
    await _getRequestByStatus(REQUEST_HAPPENING_STRING);

    event.handleFinished();
  }

  Future<void> _refreshRequestCommon(RefreshRequestCommonEvent event) async {
    requestsCommon.clear();
    isOverRequestCommon = false;
    await _getRequestCommon();
    event.handleFinished();
  }

  Future<void> _refreshRequestFinish(RefreshRequestFinishEvent event) async {
    requestsFinished.clear();
    isOverRequestsFinished = false;
    await _getRequestByStatus(REQUEST_FINISHED_STRING);

    event.handleFinished();
  }

  _getSpecialtiesLocal() {
    List<SpecialtyModel> localSpecialties = RequestLocal().specialties;
    if (localSpecialties.isNotEmpty) {
      specialties = localSpecialties;
    }
  }

  Future<void> _getSpecialties() async {
    List<SpecialtyModel> _specialties =
        await SpecialtyRepository().getSpecialties(
      skip: 0,
      limit: UNLIMITED_QUANTITY,
    );

    isOverSpecialties = true;
    specialties = [];
    specialties.addAll(_specialties);
    RequestLocal().saveSpecialties(_specialties);
  }

  Future<void> _getMySpecialties() async {
    if (mySpecialties.isEmpty) {
      List<SpecialtyModel> _specialties =
          await SpecialtyRepository().getMySpecialties(
        skip: mySpecialties.length,
        limit: 100,
      );

      if (_specialties.isEmpty) {
        isMyOverSpecialties = true;
      } else {
        mySpecialties.addAll(_specialties);
      }
    }
  }

  Future<void> _getRequestCommon() async {
    List<RequestModel> _requests =
        await RequestRepository().getListRequestCommon(
      status: REQUEST_HAPPENING_STRING,
      skip: requestsCommon.length,
      search:
          searchKey.isEmpty ? '' : searchKey.toLowerCase().formatVietnamese(),
      category: category == null ? null : category!.id,
      cost: cost,
    );

    if (_requests.length < LIMIT_API_5) {
      isOverRequestCommon = true;
    }

    requestsCommon.addAll(_requests);
  }

  Future<void> _getRequestByStatus(String status) async {
    // List happening
    List<RequestModel> _requests;

    if (status == REQUEST_HAPPENING_STRING) {
      if (AppBloc.userBloc.getAccount.isExpert!) {
        _requests = await RequestRepository().getListRequestByExpert(
          status: status,
          skip: requestsHappening.length,
        );
      } else {
        _requests = await RequestRepository().getListRequest(
          status: status,
          skip: requestsHappening.length,
        );
      }

      if (_requests.isEmpty) {
        isOverRequestsHappening = true;
      } else {
        requestsHappening.addAll(_requests);
      }
    } else {
      if (AppBloc.userBloc.getAccount.isExpert!) {
        _requests = await RequestRepository().getListRequestByExpert(
          status: status,
          skip: requestsFinished.length,
        );
      } else {
        _requests = await RequestRepository().getListRequest(
          status: status,
          skip: requestsFinished.length,
        );
      }

      if (_requests.isEmpty) {
        isOverRequestsFinished = true;
      } else {
        requestsFinished.addAll(_requests);
      }
    }
  }

  Future<bool> _createRequest(CreateRequestEvent event) async {
    RequestModel? _request = await RequestRepository().createRequest(
      request: event.request,
    );
    AppNavigator.pop();
    if (_request != null) {
      _request.budget = event.request.budget;
      _request.authorUser = AuthorModel(
        id: AppBloc.userBloc.getAccount.id!,
        fullname: AppBloc.userBloc.getAccount.fullname!,
        province: AppBloc.userBloc.getAccount.province!,
        avatar: AppBloc.userBloc.getAccount.avatar,
      );
      requestsHappening.insert(0, _request);
      requestsCommon.insert(0, _request);
    }
    return _request != null;
  }

  Future<bool> _updateRequest(UpdateRequestEvent event) async {
    RequestModel? _request =
        await RequestRepository().updateRequest(request: event.request);
    AppNavigator.pop();
    if (_request != null) {
      _request.budget = event.request.budget;
      _request.authorUser = AuthorModel(
        id: AppBloc.userBloc.getAccount.id!,
        fullname: AppBloc.userBloc.getAccount.fullname!,
        province: AppBloc.userBloc.getAccount.province!,
        avatar: AppBloc.userBloc.getAccount.avatar,
      );
      int indexOfRequest =
          requestsHappening.indexWhere((item) => item.id == event.request.id);
      if (indexOfRequest != -1) {
        requestsHappening[indexOfRequest] = _request;
      }

      int indexOfCommonRequest =
          requestsCommon.indexWhere((item) => item.id == event.request.id);
      if (indexOfCommonRequest != -1) {
        requestsCommon[indexOfCommonRequest] = _request;
      }
    }
    return _request != null;
  }

  Future<bool> _deleteRequest(DeleteRequestEvent event) async {
    bool isDeleteSucceed =
        await RequestRepository().deleteRequest(event.requestId);
    if (isDeleteSucceed) {
      int indexOfRequest = requestsHappening
          .indexWhere((request) => request.id == event.requestId);
      if (indexOfRequest != -1) {
        requestsHappening.removeAt(indexOfRequest);
      }

      int indexOfCommonRequest =
          requestsCommon.indexWhere((item) => item.id == event.requestId);
      if (indexOfCommonRequest != -1) {
        requestsCommon.removeAt(indexOfCommonRequest);
      }
    }
    return isDeleteSucceed;
  }

  Future<bool> _expertCancelOffer(ExpertCancelOfferEvent event) async {
    bool isCancelSucceed =
        await RequestRepository().expertCancelOffer(event.offerId);

    AppNavigator.pop();

    if (isCancelSucceed) {
      AppBloc.notificationBloc.add(
        CreateNotificationEvent(
          messageType: CANCEL_SERVICE,
          userId: event.requestModel.authorUser?.id ?? '',
          requestId: event.requestModel.id,
          offerId: event.offerId,
        ),
      );

      int indexOfRequest = requestsHappening.indexWhere(
        (request) => request.id == event.requestModel.id,
      );
      if (indexOfRequest != -1) {
        requestsHappening.removeAt(indexOfRequest);
      }

      int indexOfRequestCommon = requestsCommon.indexWhere(
        (request) => request.id == event.requestModel.id,
      );
      if (indexOfRequestCommon != -1) {
        int indexOfOffer =
            requestsCommon[indexOfRequestCommon].offerers.indexWhere(
                  (offer) => event.offerId == offer.id,
                );

        requestsCommon[indexOfRequestCommon].offerers.removeAt(indexOfOffer);
      }
    } else {
      dialogAnimationWrapper(
        borderRadius: 10.sp,
        slideFrom: SlideMode.bot,
        child: DialogWithTextAndPopButton(
          title: Strings.cancelBidFailed.i18n,
          bodyBefore: Strings.failedBidTryAgain.i18n,
        ),
      );
    }
    return isCancelSucceed;
  }

  Future<bool> _userChooseOffer(UserChooseOfferEvent event) async {
    String? conversationId = await RequestRepository().userChooseOffer(
      requestId: event.requestId,
      offerId: event.offerId,
    );

    if (conversationId != null) {
      // Handle request bloc
      if (state is GettingRequestCommon || state is GetDoneRequestCommon) {
        final _requestIndex = requestsCommon
            .indexWhere((element) => element.id == event.requestId);

        if (_requestIndex != -1) {
          final _offerIndex = requestsCommon[_requestIndex]
              .offerers
              .indexWhere((element) => element.id == event.offerId);

          if (_offerIndex != -1) {
            final OfferModel _selectedOffer = requestsCommon[_requestIndex]
                .offerers[_offerIndex]
                .copyWith(status: OFFER_CHOSEN);

            requestsCommon[_requestIndex].offerers.removeAt(_offerIndex);
            requestsCommon[_requestIndex].selectedOffers.add(_selectedOffer);

            // Create & send notification
            AppBloc.notificationBloc.add(
              CreateNotificationEvent(
                messageType: PICKED_EXPERT,
                userId: _selectedOffer.authorExpert.id,
                requestId: event.requestId,
                offerId: event.offerId,
              ),
            );

            // Insert automatic message
            AppBloc.messageBloc.add(
              InsertAutomaticMessageEvent(
                message: MessageOfferModel.fromRequestModel(
                  requestsCommon[_requestIndex],
                  _selectedOffer,
                ),
                conversationId: conversationId,
              ),
            );
          }
        }
      } else {
        final _requestIndex = requestsHappening
            .indexWhere((element) => element.id == event.requestId);

        if (_requestIndex != -1) {
          final _offerIndex = requestsHappening[_requestIndex]
              .offerers
              .indexWhere((element) => element.id == event.offerId);

          if (_offerIndex != -1) {
            final OfferModel _selectedOffer = requestsHappening[_requestIndex]
                .offerers[_offerIndex]
                .copyWith(status: OFFER_CHOSEN);

            requestsHappening[_requestIndex].offerers.removeAt(_offerIndex);
            requestsHappening[_requestIndex].selectedOffers.add(_selectedOffer);

            // Create & send notification
            AppBloc.notificationBloc.add(
              CreateNotificationEvent(
                messageType: PICKED_EXPERT,
                userId: _selectedOffer.authorExpert.id,
                requestId: event.requestId,
                offerId: event.offerId,
              ),
            );

            // Insert automatic message
            AppBloc.messageBloc.add(
              InsertAutomaticMessageEvent(
                message: MessageOfferModel.fromRequestModel(
                  requestsCommon[_requestIndex],
                  _selectedOffer,
                ),
                conversationId: conversationId,
              ),
            );
          }
        }
      }
    }

    return conversationId != null;
  }

  Future<bool> _userDoneStep1(UserDoneStep1Event event) async {
    final _updateInfoResult = await RequestRepository().updateUserInfo(
      offerId: event.offer.id,
      authorName: event.authorName,
      authorPhone: event.authorPhone,
      note: event.note,
      locationName: event.request.locationName,
      locationAddress: event.request.locationAddress,
    );

    AppNavigator.pop();

    if (_updateInfoResult) {
      if (state is GetDoneRequestCommon || state is GettingRequestCommon) {
        int indexOfRequest = requestsCommon
            .indexWhere((request) => request.id == event.request.id);

        if (indexOfRequest != -1) {
          int indexOfOffer =
              requestsCommon[indexOfRequest].selectedOffers.indexWhere(
                    (offer) => offer.id == event.offer.id,
                  );

          if (indexOfOffer != -1) {
            requestsCommon[indexOfRequest]
                .selectedOffers[indexOfOffer]
                .authorName = event.authorName;
            requestsCommon[indexOfRequest]
                .selectedOffers[indexOfOffer]
                .authorPhone = event.authorPhone;
            requestsCommon[indexOfRequest].selectedOffers[indexOfOffer].note =
                event.note;
          }
        }
      } else if (state is GetDoneRequestHappening ||
          state is GettingRequestHappening) {
        int indexOfRequest = requestsHappening.indexWhere(
          (request) => request.id == event.request.id,
        );

        if (indexOfRequest != -1) {
          int indexOfOffer =
              requestsHappening[indexOfRequest].selectedOffers.indexWhere(
                    (offer) => offer.id == event.offer.id,
                  );

          if (indexOfOffer != -1) {
            requestsHappening[indexOfRequest]
                .selectedOffers[indexOfOffer]
                .authorName = event.authorName;
            requestsHappening[indexOfRequest]
                .selectedOffers[indexOfOffer]
                .authorPhone = event.authorPhone;
            requestsHappening[indexOfRequest]
                .selectedOffers[indexOfOffer]
                .note = event.note;
          }
        }
      }
      event.handleFinished();
    }

    return _updateInfoResult;
  }

  Future<bool> _userDoneStep2(UserDoneStep2Event event) async {
    final _updateInfoResult = await RequestRepository().updateTimeline(
      offerId: event.offer.id,
      startTime: event.startTime,
      endTime: event.endTime,
    );

    AppNavigator.pop();

    if (_updateInfoResult) {
      if (state is GetDoneRequestCommon || state is GettingRequestCommon) {
        int indexOfRequest = requestsCommon
            .indexWhere((request) => request.id == event.request.id);

        if (indexOfRequest != -1) {
          int indexOfOffer =
              requestsCommon[indexOfRequest].selectedOffers.indexWhere(
                    (offer) => offer.id == event.offer.id,
                  );

          if (indexOfOffer != -1) {
            requestsCommon[indexOfRequest]
                .selectedOffers[indexOfOffer]
                .startTime = event.startTime;
            requestsCommon[indexOfRequest]
                .selectedOffers[indexOfOffer]
                .endTime = event.endTime;
            requestsCommon[indexOfRequest]
                .selectedOffers[indexOfOffer]
                .isPickTime = true;
          }
        }
      } else if (state is GetDoneRequestHappening ||
          state is GettingRequestHappening) {
        int indexOfRequest = requestsHappening.indexWhere(
          (request) => request.id == event.request.id,
        );

        if (indexOfRequest != -1) {
          int indexOfOffer =
              requestsHappening[indexOfRequest].selectedOffers.indexWhere(
                    (offer) => offer.id == event.offer.id,
                  );

          if (indexOfOffer != -1) {
            requestsHappening[indexOfRequest]
                .selectedOffers[indexOfOffer]
                .startTime = event.startTime;
            requestsHappening[indexOfRequest]
                .selectedOffers[indexOfOffer]
                .endTime = event.endTime;
            requestsHappening[indexOfRequest]
                .selectedOffers[indexOfOffer]
                .isPickTime = true;
          }
        }
      }
      event.handleFinished();
    } else {
      // Show dialog update failure!
    }

    return _updateInfoResult;
  }

  Future<bool> _userPayOffer(UserPayOfferEvent event) async {
    bool? _payOfferResult;
    if (event.paymentMethod == PaymentMethod.byAppWallet) {
      _payOfferResult = await RequestRepository().payByPersonalWallet(
        amount: event.amount.round(),
        offerId: event.offerId,
        expertId: event.expertId,
        requestId: event.request.id,
      );
      AppNavigator.pop();
    } else if (event.paymentMethod == PaymentMethod.byVNPay) {
      final vnpayUrl = await RequestRepository().payByVNPay(
        amount: event.amount,
        offerId: event.offerId,
        requestId: event.request.id!,
      );
      AppNavigator.pop();
      AppNavigator.push(
        Routes.WEB_VIEW_PAYMENT,
        arguments: {
          'url': vnpayUrl,
          'onPaymentDone': (value) {
            _payOfferResult = value;
            _handleAfterPayment(event: event, value: value);
          },
        },
      );
      return false;
    }

    _handleAfterPayment(event: event, value: _payOfferResult);

    return _payOfferResult!;
  }

  Future<bool> _bookingByTargetExpert(
      {required BookingByTargetExpertEvent event}) async {
    final List _bookingExpertCacheData =
        await RequestRepository().bookingExpert(
      authorName: event.authorName,
      authorPhone: event.authorPhone,
      note: event.note ?? '',
      participantsCount: event.participantNumber,
      expertId: event.expert.id,
      contactForm: event.contactForm,
      locationAddress: event.locationAddress ?? '',
      locationName: event.locationName ?? '',
    );

    if (_bookingExpertCacheData[0] != null &&
        _bookingExpertCacheData[1] != null) {
      final RequestModel _requestCacheData = _bookingExpertCacheData[0];
      final OfferSkillModel _offerCacheData = _bookingExpertCacheData[1];

      final bool _updateOfferInfor = await _updateCacheOfferInfor(
        event,
        _offerCacheData.id,
      );
      final bool _updateTimeLine = await _updateOfferTimeline(
        offerId: _offerCacheData.id,
        startTime: event.startTime,
        endTime: event.endTime,
      );
      if (_updateOfferInfor == true && _updateTimeLine == true) {
        final _newService = await _addNewServiceToConfirmingList(
          askanyPaymentTime: DateTime.now().toLocal(),
          event: event,
          authorExpert: AuthorExpertModel(
            id: event.expert.id,
            fullname: event.expert.fullname,
            province: event.expert.province,
          ),
          price: event.price,
          service: _requestCacheData,
          offer: _offerCacheData,
        );

        final bool isPay = await _payAfterBookingByTargetExpert(
          event: event,
          offerCacheData: _offerCacheData,
          requestCacheData: _requestCacheData,
        );
        if (isPay) {
          AppBloc.serviceManagamentBloc.add(
            UpdateOfferStatusAfterPaymentEvent(service: _newService),
          );
          return true;
        }
      }
    }
    return false;
  }

  Future<bool> _payAfterBookingByTargetExpert({
    required RequestModel requestCacheData,
    required BookingByTargetExpertEvent event,
    required OfferSkillModel offerCacheData,
  }) async {
    bool? _payOfferResult;
    if (event.paymentMethod == PaymentMethod.byAppWallet) {
      _payOfferResult = await RequestRepository().payByPersonalWallet(
        amount: event.amount.round(),
        offerId: offerCacheData.id,
        expertId: offerCacheData.authorExpert,
        requestId: requestCacheData.id,
      );
      AppNavigator.pop();
    } else if (event.paymentMethod == PaymentMethod.byVNPay) {
      final String? url = await RequestRepository().payByVNPay(
        amount: event.amount,
        offerId: offerCacheData.id,
        requestId: requestCacheData.id.toString(),
      );

      await AppNavigator.push(
        Routes.WEB_VIEW_PAYMENT,
        arguments: {
          'url': url,
          'onPaymentDone': (value) {
            _payOfferResult = value;
            if (value) {
              _handleAfterBookingDoneExpert(
                  service: requestCacheData, result: value, event: event);
            }
          },
        },
      );
    }
    AppNavigator.pop();
    if (_payOfferResult == true) {
      _handleAfterBookingDoneExpert(
          service: requestCacheData, result: true, event: event);

      return true;
    } else {
      AppNavigator.push(
        Routes.PAYMENT_RESULT,
        arguments: {
          'isSuccess': false,
          'isCheckChatScreen': event.isCheckChatScreen
        },
      );
    }

    return _payOfferResult ?? false;
  }

  Future<void> _bookingExpert(UserPayOfferSkillEvent event) async {
    List? _list = await BookingRepository().orderSkill(
      skillId: event.skillModel.id!,
      contactForm: event.contactForm,
      authorName: event.authorName,
      authorPhone: event.authorPhone,
      locationAddress: event.localAddress,
      locationName: event.localName,
      note: event.note,
      participantsCount: event.participantsCount,
    );

    if (_list != null) {
      offerSkillModel[event.skillModel.id!] = _list[0];
      requestModel[event.skillModel.id!] = _list[1];
    }
  }

  Future<bool> _userPayOfferSkill(UserPayOfferSkillEvent event) async {
    await _bookingExpert(event);
    String offerId = offerSkillModel[event.skillModel.id!]!.id;
    bool? _payOfferResult;

    bool _updateInfoResult = await RequestRepository().updateTimeline(
      offerId: offerId,
      startTime: event.startTime,
      endTime: event.endTime,
    );

    if (_updateInfoResult == true) {
      final _newService = await _handleAfterBookingSkillDoneExpert(
        event: event,
      );
      _payOfferResult = await _payAfterBookingBySkill(
        requestCacheData: requestModel[event.skillModel.id]!,
        event: event,
        offerCacheData: offerSkillModel[event.skillModel.id!]!,
      );
      if (_payOfferResult) {
        AppBloc.serviceManagamentBloc.add(
          UpdateOfferStatusAfterPaymentEvent(service: _newService),
        );
      }
    } else {
      _updateInfoResult = false;
    }

    AppNavigator.pop();
    _handleAfterPaymentSkill(event: event, value: _payOfferResult);

    return _payOfferResult!;
  }

  Future<bool> _payAfterBookingBySkill({
    required RequestModel requestCacheData,
    required UserPayOfferSkillEvent event,
    required OfferSkillModel offerCacheData,
  }) async {
    bool? _payOfferResult;

    if (event.paymentMethod == PaymentMethod.byAppWallet) {
      _payOfferResult = await RequestRepository().payByPersonalWallet(
        amount: event.amount.round(),
        offerId: offerCacheData.id,
        expertId: offerCacheData.authorExpert,
        requestId: requestCacheData.id,
      );
      AppNavigator.pop();
    } else if (event.paymentMethod == PaymentMethod.byVNPay) {
      final String? url = await RequestRepository().payByVNPay(
        amount: event.amount,
        offerId: offerCacheData.id,
        requestId: requestCacheData.id!,
      );
      await AppNavigator.push(
        Routes.WEB_VIEW_PAYMENT,
        arguments: {
          'url': url,
          'onPaymentDone': (value) {
            _payOfferResult = value;
            _handleAfterPaymentSkill(event: event, value: value);
          },
        },
      );
      AppNavigator.pop();
      return false;
    }
    return _payOfferResult ?? false;
  }

  void _handleAfterPaymentSkill(
      {required UserPayOfferSkillEvent event, required bool? value}) {
    bool? _payOfferResult = value;
    if (_payOfferResult == null) {
      return;
    }

    if (_payOfferResult) {
      final _requestIndex = requestsHappening.indexWhere(
          (element) => element.id == requestModel[event.skillModel.id]!.id);

      if (_requestIndex != -1) {
        final int _selectedOfferIndex = requestsHappening[_requestIndex]
            .selectedOffers
            .indexWhere((element) =>
                element.id == offerSkillModel[event.skillModel.id!]!.id);
        if (_selectedOfferIndex != -1) {
          final OfferModel _selectedOffer = requestsHappening[_requestIndex]
              .selectedOffers[_selectedOfferIndex]
              .copyWith(status: OFFER_PAID);

          requestsHappening[_requestIndex].selectedOffers[_selectedOfferIndex] =
              _selectedOffer;
        }
      }
    }

    AppNavigator.push(
      Routes.PAYMENT_RESULT,
      arguments: {'isSuccess': _payOfferResult},
    );
  }

  void _handleAfterPayment(
      {required UserPayOfferEvent event, required bool? value}) {
    bool? _payOfferResult = value;
    if (_payOfferResult == null) {
      return;
    }

    if (_payOfferResult) {
      final _requestIndex = requestsHappening
          .indexWhere((element) => element.id == event.request.id);

      if (_requestIndex != -1) {
        final int _selectedOfferIndex = requestsHappening[_requestIndex]
            .selectedOffers
            .indexWhere((element) => element.id == event.offerId);
        if (_selectedOfferIndex != -1) {
          final OfferModel _selectedOffer = requestsHappening[_requestIndex]
              .selectedOffers[_selectedOfferIndex]
              .copyWith(status: OFFER_PAID);

          requestsHappening[_requestIndex].selectedOffers[_selectedOfferIndex] =
              _selectedOffer;
        }
      }
    }

    AppNavigator.push(
      Routes.PAYMENT_RESULT,
      arguments: {'isSuccess': _payOfferResult},
    );
  }

  Future<bool> _userEndRequest(UserEndRequestEvent event) async {
    final bool _result =
        await RequestRepository().userEndRequest(requestId: event.requestId);
    if (_result) {
      if (state is GettingRequestHappening ||
          state is GetDoneRequestHappening) {
        final int _requestIndex = requestsHappening
            .indexWhere((element) => element.id == event.requestId);
        if (_requestIndex != -1) {
          RequestModel _request = requestsHappening[_requestIndex]
              .copyWith(status: REQUEST_FINISHED);
          requestsHappening.removeAt(_requestIndex);
          if (requestsFinished.isNotEmpty) {
            requestsFinished.add(_request);
          }
        }
      } else {
        final int _requestIndex = requestsCommon
            .indexWhere((element) => element.id == event.requestId);
        if (_requestIndex != -1) {
          RequestModel _request =
              requestsCommon[_requestIndex].copyWith(status: REQUEST_FINISHED);
          requestsCommon.removeAt(_requestIndex);
          if (requestsFinished.isNotEmpty) {
            requestsFinished.add(_request);
          }
        }
      }
    }
    return _result;
  }

  Future<void> _continuePaymentOffer(ContinuePaymentOfferEvent event) async {
    showDialogLoading();
    OfferModel? _offerModel =
        await RequestRepository().getDetailsOffer(offerId: event.offer.id);
    OfferModel _selectedOffer = _offerModel ?? event.offer;
    AppNavigator.pop();
    AppNavigator.push(
      Routes.ORDER_SERVICE,
      arguments: {
        'request': event.request,
        'offer': _selectedOffer,
        'contactForm': event.request.contactForm,
      },
    );
  }

  Future<bool> _updateCacheOfferInfor(
      BookingByTargetExpertEvent event, String offerId) async {
    final bool _result = await RequestRepository().updateUserInfo(
        offerId: offerId,
        authorName: event.authorName,
        authorPhone: event.authorPhone,
        note: event.note ?? '',
        locationName: event.locationName ?? '',
        locationAddress: event.locationAddress ?? '');
    return _result;
  }

  Future<bool> _updateOfferTimeline({
    required String offerId,
    required DateTime startTime,
    required DateTime endTime,
  }) async {
    final bool _result = await RequestRepository().updateTimeline(
      offerId: offerId,
      startTime: startTime,
      endTime: endTime,
    );
    return _result;
  }

  void _handleAfterBookingDoneExpert({
    required RequestModel service,
    required bool result,
    required BookingByTargetExpertEvent event,
  }) async {
    AppNavigator.push(
      Routes.PAYMENT_RESULT,
      arguments: {
        'isSuccess': result,
        'isCheckChatScreen': event.isCheckChatScreen
      },
    );
  }

  Future<RequestModel> _handleAfterBookingSkillDoneExpert({
    required UserPayOfferSkillEvent event,
  }) async {
    final bookingBySkill = await _addNewServiceFromBookingSkillToConfirmingList(
      askanyPaymentTime: DateTime.now().toLocal(),
      event: event,
      authorExpert: AuthorExpertModel(
        id: event.expert.author!.id,
        fullname: event.expert.author!.fullname,
        province: event.expert.author!.province,
      ),
      price: event.price,
      service: requestModel[event.skillModel.id!]!,
      offer: offerSkillModel[event.skillModel.id!]!,
    );
    return bookingBySkill;
  }

  Future<RequestModel> _addNewServiceFromBookingSkillToConfirmingList({
    required UserPayOfferSkillEvent event,
    required AuthorExpertModel authorExpert,
    required BudgetModel price,
    required RequestModel service,
    required OfferSkillModel offer,
    required DateTime askanyPaymentTime,
  }) async {
    final OfferModel _selectedOffer = OfferModel(
      id: offer.id,
      authorExpert: authorExpert,
      price: price,
      content: offer.content,
      status: SERVICE_CONFIRMED,
      startTime: event.startTime,
      endTime: event.endTime,
    );

    final RequestModel _newService = service;
    _newService.myOfferId = offer.id;
    _newService.price = price;
    _newService.authorExpert = authorExpert;
    _newService.startTime = event.startTime;
    _newService.endTime = event.endTime;
    _newService.createdAt = DateTime.now().toLocal();
    _newService.askanyPaymentTime = DateTime.parse(DATE_TIME_DEFAULT).toLocal();

    _newService.isPickTime = true;
    _newService.selectedOffers = [_selectedOffer];
    _newService.askanyPaymentTime = askanyPaymentTime;
    _newService.confirmedTime = askanyPaymentTime;

    AppBloc.serviceManagamentBloc
        .add(AddNewServiceToListEvent(service: _newService));
    return _newService;
  }

  Future<RequestModel> _addNewServiceToConfirmingList({
    required BookingByTargetExpertEvent event,
    required AuthorExpertModel authorExpert,
    required BudgetModel price,
    required RequestModel service,
    required OfferSkillModel offer,
    required DateTime askanyPaymentTime,
  }) async {
    final OfferModel _selectedOffer = OfferModel(
      id: offer.id,
      authorExpert: authorExpert,
      price: price,
      content: offer.content,
      status: SERVICE_CONFIRMED,
      startTime: event.startTime,
      endTime: event.endTime,
    );

    final RequestModel _newService = service;
    _newService.myOfferId = offer.id;
    _newService.price = price;
    _newService.authorExpert = authorExpert;
    _newService.startTime = event.startTime;
    _newService.endTime = event.endTime;
    _newService.createdAt = DateTime.now().toLocal();
    _newService.askanyPaymentTime = DateTime.parse(DATE_TIME_DEFAULT).toLocal();

    _newService.isPickTime = true;
    _newService.selectedOffers = [_selectedOffer];
    _newService.askanyPaymentTime = askanyPaymentTime;
    _newService.confirmedTime = askanyPaymentTime;

    AppBloc.serviceManagamentBloc
        .add(AddNewServiceToListEvent(service: _newService));
    return _newService;
  }

  bool get isNotCondition =>
      searchKey == '' && cost == '0-1000000000' && category == null;

  _cleanBloc() {
    requestsHappening = [];
    requestsFinished = [];
    requestsCommon = [];

    isOverRequestsHappening = false;
    isOverRequestsFinished = false;
    isOverRequestCommon = false;
  }
}
