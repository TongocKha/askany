part of 'request_bloc.dart';

@immutable
abstract class RequestEvent {}

class CleanRequestEvent extends RequestEvent {}

class CleanFilterRequestEvent extends RequestEvent {}

class GetCategoryEvent extends RequestEvent {}

class GetSpecialtyEvent extends RequestEvent {}

class GetMySpecialtyEvent extends RequestEvent {}

class OnHappeningEvent extends RequestEvent {}

class OnFinishedEvent extends RequestEvent {}

class OnCommonEvent extends RequestEvent {}

class GetRequestHappeningEvent extends RequestEvent {}

class GetRequestFinishedEvent extends RequestEvent {}

class GetRequestCommonEvent extends RequestEvent {}

class CreateRequestEvent extends RequestEvent {
  final RequestModel request;
  final bool isNavigationFromSearch;
  CreateRequestEvent(
      {required this.request, required this.isNavigationFromSearch});
}

class UpdateRequestEvent extends RequestEvent {
  final RequestModel request;
  UpdateRequestEvent({required this.request});
}

class DeleteRequestEvent extends RequestEvent {
  final String requestId;
  DeleteRequestEvent({required this.requestId});
}

class ExpertCancelOfferEvent extends RequestEvent {
  final RequestModel requestModel;
  final String offerId;
  ExpertCancelOfferEvent({required this.offerId, required this.requestModel});
}

class UserChooseOfferEvent extends RequestEvent {
  final String requestId;
  final String offerId;

  UserChooseOfferEvent({
    required this.requestId,
    required this.offerId,
  });
}

class UserEndRequestEvent extends RequestEvent {
  final String requestId;

  UserEndRequestEvent({required this.requestId});
}

class UserDoneStep1Event extends RequestEvent {
  final OfferModel offer;
  final RequestModel request;
  final String authorName;
  final String authorPhone;
  final String note;
  final Function handleFinished;
  UserDoneStep1Event({
    required this.offer,
    required this.request,
    required this.authorName,
    required this.authorPhone,
    required this.note,
    required this.handleFinished,
  });
}

class UserDoneStep2Event extends RequestEvent {
  final OfferModel offer;
  final RequestModel request;
  final DateTime startTime;
  final DateTime endTime;
  final Function handleFinished;
  UserDoneStep2Event({
    required this.offer,
    required this.request,
    required this.startTime,
    required this.endTime,
    required this.handleFinished,
  });
}

class UserPayOfferEvent extends RequestEvent {
  final String offerId;
  final String expertId;
  final RequestModel request;
  final PaymentMethod paymentMethod;

  final double updatedWallet;
  final double amount;

  UserPayOfferEvent({
    required this.offerId,
    required this.expertId,
    required this.request,
    required this.paymentMethod,
    required this.updatedWallet,
    required this.amount,
  });
}

class BookingByTargetExpertEvent extends RequestEvent {
  final ExpertModel expert;
  final String contactForm;

  final PaymentMethod paymentMethod;
  final double updatedWallet;
  final DateTime startTime;
  final DateTime endTime;
  final String authorName;
  final String authorPhone;
  final String? locationName;
  final String? locationAddress;
  final int participantNumber;
  final String? note;
  final double cost;
  final int totalMinutes;
  final BudgetModel price;

  final double amount;
  final bool isCheckChatScreen;
  BookingByTargetExpertEvent({
    required this.expert,
    required this.contactForm,
    required this.paymentMethod,
    required this.updatedWallet,
    required this.startTime,
    required this.endTime,
    required this.authorName,
    required this.authorPhone,
    this.locationName,
    this.locationAddress,
    required this.participantNumber,
    this.note,
    required this.cost,
    required this.totalMinutes,
    required this.amount,
    required this.price,
    required this.isCheckChatScreen,
  });
}

class UserPayOfferSkillEvent extends RequestEvent {
  final PaymentMethod paymentMethod;
  final SkillModel expert;
  final BudgetModel price;

  final double updatedWallet;
  final double amount;
  final String contactForm;
  final SkillModel skillModel;
  final DateTime startTime;
  final DateTime endTime;
  final String authorName;
  final String authorPhone;
  final String note;
  final int participantsCount;
  final String localAddress;
  final String localName;

  UserPayOfferSkillEvent({
    required this.expert,
    required this.paymentMethod,
    required this.updatedWallet,
    required this.amount,
    required this.skillModel,
    required this.contactForm,
    required this.startTime,
    required this.endTime,
    required this.authorName,
    required this.authorPhone,
    required this.note,
    required this.participantsCount,
    required this.localAddress,
    required this.localName,
    required this.price,
  });
}

class ContinuePaymentOfferEvent extends RequestEvent {
  final RequestModel request;
  final OfferModel offer;
  ContinuePaymentOfferEvent({required this.request, required this.offer});
}

class RefreshRequestHappeningEvent extends RequestEvent {
  final Function handleFinished;
  RefreshRequestHappeningEvent({required this.handleFinished});
}

class RefreshRequestFinishEvent extends RequestEvent {
  final Function handleFinished;
  RefreshRequestFinishEvent({required this.handleFinished});
}

class RefreshRequestCommonEvent extends RequestEvent {
  final Function handleFinished;
  RefreshRequestCommonEvent({required this.handleFinished});
}

class ExpertCreateOfferEvent extends RequestEvent {
  final RequestModel requestModel;
  final BudgetModel budgetModel;
  final String content;
  ExpertCreateOfferEvent({
    required this.requestModel,
    required this.budgetModel,
    required this.content,
  });
}

class FilterRequestEvent extends RequestEvent {
  final String searchKey;
  final String cost;
  final CategoryModel? category;
  FilterRequestEvent({
    required this.searchKey,
    required this.cost,
    this.category,
  });

  get searchModel => null;
}

class MoveBackToOriginRequestListEvent extends RequestEvent {}
