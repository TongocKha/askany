part of 'request_bloc.dart';

@immutable
abstract class RequestState {
  List<dynamic> get props => [];
}

class RequestHappeningInitial extends RequestState {}

class RequestFinishedInitial extends RequestState {}

class RequestCommonInitial extends RequestState {}

class GettingRequestHappening extends RequestState {
  final List<RequestModel> requests;

  GettingRequestHappening({required this.requests});

  @override
  List get props => [requests];
}

class GettingRequestCommon extends RequestState {
  final List<RequestModel> requests;

  GettingRequestCommon({required this.requests});

  @override
  List get props => [requests];
}

class GetDoneRequestHappening extends RequestState {
  final List<RequestModel> requests;
  GetDoneRequestHappening({required this.requests});

  @override
  List get props => [requests];
}

class GettingRequestFinished extends RequestState {
  final List<RequestModel> requests;
  GettingRequestFinished({required this.requests});

  @override
  List get props => [requests];
}

class GetDoneRequestFinished extends RequestState {
  final List<RequestModel> requests;
  GetDoneRequestFinished({required this.requests});

  @override
  List get props => [requests];
}

class GetDoneRequestCommon extends RequestState {
  final List<RequestModel> requests;
  GetDoneRequestCommon({required this.requests});

  @override
  List get props => [requests];
}

class HandleDoneAfterBookingExxpert extends RequestState {}
