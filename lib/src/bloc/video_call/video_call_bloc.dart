import 'dart:io';
import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/timer/timer_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/data/remote_data_source/call_repository.dart';
import 'package:askany/src/helpers/utils/logger.dart';
import 'package:askany/src/models/candidate_model.dart';
import 'package:askany/src/models/conversation_model.dart';
import 'package:askany/src/models/incoming_call_model.dart';
import 'package:askany/src/models/slide_mode.dart';
import 'package:askany/src/models/video_call_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/services/incomingcall_ios/incoming_call.dart';
import 'package:askany/src/services/method_channel/foreground_service.dart';
import 'package:askany/src/services/method_channel/initial_app_channel.dart';
import 'package:askany/src/services/socket/socket_emit.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_confirm_cancel.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart' as RTC;
import 'package:meta/meta.dart';
import 'package:sdp_transform/sdp_transform.dart';
part 'video_call_event.dart';
part 'video_call_state.dart';

class VideoCallBloc extends Bloc<VideoCallEvent, VideoCallState> {
  ConversationModel? room;
  RTC.RTCPeerConnection? _peerConnection;
  RTC.RTCPeerConnection? _peerShareConnection;
  RTC.MediaStream? _localStream;
  RTC.MediaStream? _localShareStream;
  RTC.RTCVideoRenderer _localRenderer = RTC.RTCVideoRenderer();
  RTC.RTCVideoRenderer _localShareRenderer = RTC.RTCVideoRenderer();
  RTC.RTCVideoRenderer _remoteRenderer = RTC.RTCVideoRenderer();
  RTC.RTCVideoRenderer _remoteShareRenderer = RTC.RTCVideoRenderer();
  bool _isFrontCamera = true;
  bool _isSharingScreen = false;
  bool _micEnabled = true;
  bool _soundEnabled = true;
  bool _cameraEnabled = true;
  bool _offer = false;
  bool _isMyShare = false;
  bool _isShareFullScreen = false;
  bool _isVidCallFullScreen = false;
  bool _isMyRaisingHand = false;
  bool _isRemoteRaisingHand = false;
  bool _isRemoteOnMic = true;
  bool _isRemoteOnCamera = true;
  String _callId = "";
  String _receiverId = "";
  List<RTC.RTCIceCandidate> _candidates = [];
  List<RTC.RTCIceCandidate> _candidatesShare = [];
  // bool _isIncomingCall = false;
  VideoCallBloc() : super(VideoCallInitial());

  @override
  Stream<VideoCallState> mapEventToState(VideoCallEvent event) async* {
    if (event is RequestVideoCallEvent) {
      _receiverId = event.receiverId;
      room = event.conversation;
      showDialogLoading();
      bool _canCall = await _requestVideoCall();
      AppNavigator.pop();
      if (_canCall) {
        AppNavigator.push(Routes.VIDEO_CALL, arguments: {
          'slide': SlideMode.fade,
          'conversationId': room,
        });
        yield _getVideoCallingState();
      } else {
        if (UserLocal().getIsExpert()) {
          Future.delayed(Duration(milliseconds: DELAY_HALF_SECOND), () {
            dialogAnimationWrapper(
              slideFrom: SlideMode.bot,
              child: DialogConfirmCancel(
                bodyBefore: Strings.notBookedYetNoti.i18n,
                bodyColor: colorGray1,
                confirmText: Strings.underStood.i18n,
                onConfirmed: () {
                  AppNavigator.pop();
                },
              ),
            );
          });
        }
      }
    }

    if (event is SetRemoteDescriptionEvent) {
      _setRemoteDescription(event.sdp);
      if (_offer) {
        // _isIncomingCall = false;
        _candidates.forEach((e) {
          _sendCandidates(
            e.candidate.toString(),
            e.sdpMid.toString(),
            e.sdpMLineIndex!,
          );
        });
      }
      _candidates.clear();
    }

    if (event is AnswerVideoCallEvent) {
      // room = event.incomingCallModel.room;
      _callId = event.incomingCallModel.id;
      if (_offer) {
        yield _getVideoCallingState();
      } else {
        bool isNavigationFromCalling = false;

        if (!InitialAppChannel.flagCheck) {
          isNavigationFromCalling = await InitialAppChannel.checkIntentFromIncomingCall();
          InitialAppChannel.setNavigationFromIncoming(false);
        }

        if (AppNavigator.currentRoute() != Routes.VIDEO_CALL) {
          AppNavigator.push(Routes.VIDEO_CALL, arguments: {
            'slide': SlideMode.fade,
            'conversationId': event.incomingCallModel.room,
          });
        }

        if (isNavigationFromCalling) {
          await _answerVideoCall(
            sdp: event.incomingCallModel.sdp,
            roomOffer: event.incomingCallModel.room,
          );
          yield _getVideoCallingState();
        } else {
          yield _getIncomingVideoCallState(event.incomingCallModel);
        }
      }
    }

    if (event is AddCandidateEvent) {
      await _addCandidate(event.candidate);
      yield _getVideoCallingState();
    }

    if (event is StartShareScreenEvent) {
      await _startShareScreen();
      yield _getVideoCallingState();
    }

    if (event is StopShareScreenEvent) {
      await _disposeShare();
      yield _getVideoCallingState();
    }

    if (event is SetShareRemoteDescriptionEvent) {
      await _setShareRemoteDescription(event.sdp);
      if (_isMyShare) {
        _candidatesShare.forEach((e) {
          _sendShareCandidates(
            e.candidate.toString(),
            e.sdpMid.toString(),
            e.sdpMLineIndex!,
          );
        });
      }
      _candidatesShare.clear();
      yield _getVideoCallingState();
    }

    if (event is AnswerShareVideoCallEvent) {
      await _answerShareVideoCall(event.sdp);
      yield _getVideoCallingState();
    }

    if (event is AddShareCandidateEvent) {
      await _addShareCandidate(event.candidate);
      yield _getVideoCallingState();
    }

    if (event is SwitchCameraEvent) {
      await _switchCamera();
      yield _getVideoCallingState();
    }

    if (event is ToggleVideoEvent) {
      _toggleCamera();
      yield _getVideoCallingState();
    }

    if (event is ToggleMicEvent) {
      _toggleMic();
      yield _getVideoCallingState();
    }

    if (event is ToggleSoundEvent) {
      _toggleSound();
      yield _getVideoCallingState();
    }

    if (event is StartVideoCallEvent) {
      yield _getVideoCallingState();
      await _answerVideoCall(
        sdp: event.incomingCallModel.sdp,
        roomOffer: event.incomingCallModel.room,
      );
      yield _getVideoCallingState();
    }

    if (event is VideoCallingFullScreenEvent) {
      _isVidCallFullScreen = !_isVidCallFullScreen;
      yield _getVideoCallingState();
    }
    if (event is ShareFullScreenEvent) {
      _isShareFullScreen = !_isShareFullScreen;
      yield _getVideoCallingState();
    }

    if (event is SetRemoteCameraEvent) {
      _setRemoteCamera(event.isOn);
      yield _getVideoCallingState();
    }

    if (event is SetRemoteMicEvent) {
      _setRemoteMic(event.isOn);
      yield _getVideoCallingState();
    }

    if (event is ToggleMyRaiseHandEvent) {
      _raiseHand();
      yield _getVideoCallingState();
    }

    if (event is RemoteRaiseHandEvent) {
      _setRemoteRaiseHand(event.isRaising);
      yield _getVideoCallingState();
    }

    if (event is EndVideoCallEvent) {
      if (state is! VideoCallInitial) {
        await _endCall();
        await _dispose();
        yield VideoCallInitial();
      }
    }

    if (event is DisposeVideoCallEvent) {
      await _dispose();
      yield VideoCallInitial();
    }
  }

  VideoCallIncoming _getIncomingVideoCallState(IncomingCallModel incomingCallModel) {
    return VideoCallIncoming(
      incomingCallModel: incomingCallModel,
    );
  }

  VideoCalling _getVideoCallingState() {
    return VideoCalling(
      videoCallModel: VideoCallModel(
        isFrontCamera: _isFrontCamera,
        isSharingScreen: _isSharingScreen,
        micEnabled: _micEnabled,
        soundMyShare: _soundEnabled,
        cameraEnabled: _cameraEnabled,
        localRenderer: _localRenderer,
        remoteRenderer: _remoteRenderer,
        localShareRenderer: _localShareRenderer,
        remoteShareRenderer: _remoteShareRenderer,
        isMyShare: _isMyShare,
        isShareFullScreen: _isShareFullScreen,
        isVidCallFullScreen: _isVidCallFullScreen,
        isMyRaisingHand: _isMyRaisingHand,
        isRemoteRaisingHand: _isRemoteRaisingHand,
        isRemoteOnMic: _isRemoteOnMic,
        isRemoteOnCamera: _isRemoteOnCamera,
      ),
    );
  }

  Future<bool> _requestVideoCall() async {
    _initRenderers();
    _offer = true;
    _peerConnection = await _createPeerConnection();
    bool _canCall = await _createOffer();
    return _canCall;
  }

  Future<void> _startShareScreen() async {
    if (Platform.isAndroid) {
      await ForegroundService.startForegroundService();
    }
    _initShareRenderers();
    _isShareFullScreen = false;
    _isMyShare = true;
    _isSharingScreen = true;
    if (Platform.isIOS) {
      // ReplayKitLauncher.launchReplayKitBroadcast('BroadcastDemoExtension');
      _cameraEnabled = false;
      SocketEmit().sendOnCamera(roomId: room?.id ?? '', isOnCamera: false);
      await _toggleVidelTrack(enable: false);
    }
    await _createSharePeerConnection();
    await _createShareOffer();
  }

  Future<void> _answerVideoCall({required String sdp, required ConversationModel roomOffer}) async {
    _initRenderers();
    _offer = false;
    room = roomOffer;
    _peerConnection = await _createPeerConnection();
    _setRemoteDescription(sdp);
  }

  Future<void> _answerShareVideoCall(sdp) async {
    _initShareRenderers();
    _isMyShare = false;
    _peerShareConnection = await _createShareAnswerPeerConnection();
    await _setShareRemoteDescription(sdp);
  }

  Future<void> _switchCamera() async {
    if (_localStream == null) throw Exception('Stream is not initialized');

    final videoTrack = _localStream!.getVideoTracks().firstWhere((track) => track.kind == 'video');
    await RTC.Helper.switchCamera(videoTrack);
  }

  void _toggleCamera() async {
    if (!(Platform.isIOS && _isMyShare)) {
      var tracks = _localStream!.getVideoTracks();
      if (_cameraEnabled) {
        tracks.forEach((track) {
          track.enabled = false;
        });
      } else {
        tracks.forEach((track) {
          track.enabled = true;
        });
      }
      _cameraEnabled = !_cameraEnabled;
      SocketEmit().sendOnCamera(roomId: room?.id ?? '', isOnCamera: _cameraEnabled);
    }
  }

  void _toggleMic({bool? enable}) async {
    var tracks = _localStream!.getAudioTracks();

    if (enable == null) {
      if (_micEnabled) {
        tracks.forEach((track) {
          track.enabled = false;
        });
      } else {
        tracks.forEach((track) {
          track.enabled = true;
        });
      }
    } else {
      tracks.forEach((track) {
        track.enabled = enable;
      });
    }

    _micEnabled = enable ?? !_micEnabled;
    SocketEmit().sendOnMic(roomId: room?.id ?? '', isOnMic: _micEnabled);
  }

  void _toggleSound() async {
    enableSound();
    var tracks = _localStream!.getAudioTracks();
    tracks.forEach((track) {
      // if (_soundEnabled) {
      //   track.enableSpeakerphone(false);
      // } else {
      //   track.enableSpeakerphone(true);
      // }
      track.enableSpeakerphone(true);
    });
    _soundEnabled = !_soundEnabled;
  }

  void _initRenderers() async {
    _peerConnection = null;
    _localStream = null;
    _localRenderer = RTC.RTCVideoRenderer();
    _remoteRenderer = RTC.RTCVideoRenderer();
    _localShareRenderer = RTC.RTCVideoRenderer();
    _remoteShareRenderer = RTC.RTCVideoRenderer();
    _isFrontCamera = true;
    _micEnabled = true;
    _soundEnabled = true;
    _cameraEnabled = true;
    _offer = false;
    _isSharingScreen = false;
    _isMyShare = false;
    await _localRenderer.initialize();
    await _remoteRenderer.initialize();
  }

  void _initShareRenderers() async {
    _peerShareConnection = null;
    _localShareStream = null;
    _localShareRenderer = RTC.RTCVideoRenderer();
    _remoteShareRenderer = RTC.RTCVideoRenderer();
    _isMyShare = false;
    _isSharingScreen = true;
    await _localShareRenderer.initialize();
    await _remoteShareRenderer.initialize();
  }

  Future<bool> _createOffer() async {
    RTC.RTCSessionDescription description = await _peerConnection!.createOffer({
      'offerToReceiveVideo': 1,
      'offerToReceiveAudio': 1,
    });
    var session = parse(description.sdp.toString());
    String sdp = write(session, null);
    bool _canCall = await _sendSdp(sdp, "offer");
    _offer = true;

    _peerConnection!.setLocalDescription(description);
    return _canCall;
  }

  Future<void> _createShareOffer() async {
    RTC.RTCSessionDescription description = await _peerShareConnection!.createOffer({
      'offerToReceiveVideo': 1,
      'offerToReceiveAudio': 1,
    });
    var session = parse(description.sdp.toString());
    String sdp = write(session, null);
    await _sendShareSdp(sdp, "offer");
    await _peerShareConnection!.setLocalDescription(description);
  }

  void _createAnswer() async {
    RTC.RTCSessionDescription description = await _peerConnection!.createAnswer({
      'offerToReceiveVideo': 1,
      'offerToReceiveAudio': 1,
    });

    var session = parse(description.sdp!);
    String sdp = write(session, null);
    await _sendSdp(sdp, "answer");

    _peerConnection!.setLocalDescription(description);

    _peerConnection!.onIceCandidate = (event) => {
          _sendCandidates(
            event.candidate.toString(),
            event.sdpMid.toString(),
            event.sdpMLineIndex!,
          ),
        };
  }

  Future<void> _createShareAnswer() async {
    RTC.RTCSessionDescription description = await _peerShareConnection!.createAnswer({
      'offerToReceiveVideo': 1,
      'offerToReceiveAudio': 1,
    });

    var session = parse(description.sdp!);
    String sdp = write(session, null);
    await _sendShareSdp(sdp, "answer");

    _peerShareConnection!.setLocalDescription(description);

    _peerShareConnection!.onIceCandidate = (event) => {
          _sendShareCandidates(
            event.candidate.toString(),
            event.sdpMid.toString(),
            event.sdpMLineIndex!,
          ),
        };
  }

  void _setRemoteDescription(sdp) async {
    UtilLogger.log("_setRemoteDescription", sdp.length.toString());
    RTC.RTCSessionDescription description =
        new RTC.RTCSessionDescription(sdp, (_offer ? 'answer' : 'offer'));
    await _peerConnection!.setRemoteDescription(description);
    if (_offer) {
    } else {
      _createAnswer();
    }
  }

  Future<void> _setShareRemoteDescription(sdp) async {
    RTC.RTCSessionDescription description =
        new RTC.RTCSessionDescription(sdp, (_isMyShare ? 'answer' : 'offer'));
    await _peerShareConnection!.setRemoteDescription(description);
    if (_isMyShare) {
    } else {
      await _createShareAnswer();
    }
  }

  Future<RTC.RTCPeerConnection> _createPeerConnection() async {
    final Map<String, dynamic> offerSdpConstraints = {
      "mandatory": {
        "OfferToReceiveAudio": true,
        "OfferToReceiveVideo": true,
      },
      "optional": [],
    };

    _localStream = await _getUserMedia();

    RTC.RTCPeerConnection pc = await RTC.createPeerConnection(
      configuration,
      offerSdpConstraints,
    );

    pc.addStream(_localStream!);

    pc.onIceCandidate = (e) {
      if (e.candidate != null) {
        _candidates.add(e);
      }
    };

    pc.onAddStream = (stream) {
      _remoteRenderer.srcObject = stream;
    };

    return pc;
  }

  Future<void> _createSharePeerConnection() async {
    final Map<String, dynamic> offerSdpConstraints = {
      "mandatory": {
        "OfferToReceiveAudio": true,
        "OfferToReceiveVideo": true,
      },
      "optional": [],
    };

    _localShareStream = await _getScreenSharing();

    _peerShareConnection = await RTC.createPeerConnection(
      configuration,
      offerSdpConstraints,
    );

    // _localShareStream!.getTracks().forEach((element) {
    //   _peerShareConnection!.addTrack(element, _localShareStream!);
    // });

    _peerShareConnection!.addStream(_localShareStream!);

    _peerShareConnection!.onIceCandidate = (e) {
      if (e.candidate != null) {
        _candidatesShare.add(e);
      }
    };

    _peerShareConnection!.onIceConnectionState = (e) {};
  }

  Future<RTC.RTCPeerConnection> _createShareAnswerPeerConnection() async {
    final Map<String, dynamic> offerSdpConstraints = {
      "mandatory": {
        "OfferToReceiveAudio": true,
        "OfferToReceiveVideo": true,
      },
      "optional": [],
    };

    RTC.RTCPeerConnection pc = await RTC.createPeerConnection(
      Platform.isAndroid ? configurationShare : configuration,
      offerSdpConstraints,
    );

    pc.onIceCandidate = (e) {
      if (e.candidate != null) {
        _sendShareCandidates(
          e.candidate.toString(),
          e.sdpMid.toString(),
          e.sdpMLineIndex!,
        );
      }
    };

    pc.onIceConnectionState = (e) {};

    // if (Platform.isAndroid) {
    //   pc.onAddTrack = (stream, track) {
    //     _remoteShareRenderer.srcObject = stream;
    //   };
    // } else {
    //   pc.onAddStream = (stream) {
    //     _remoteShareRenderer.srcObject = stream;
    //   };
    // }

    pc.onAddStream = (stream) {
      _remoteShareRenderer.srcObject = stream;
    };

    return pc;
  }

  Future _sendCandidates(
    String candidate,
    String sdpMid,
    int sdpMLineIndex,
  ) async {
    SocketEmit().sendCandidate(
      candidate: CandidateModel(
        candidate: candidate,
        sdpMid: sdpMid,
        sdpMLineIndex: sdpMLineIndex,
      ),
      roomId: room?.id ?? '',
    );
  }

  Future<bool> _sendSdp(
    String sdp,
    String type,
  ) async {
    if (type == "offer") {
      String? callId = await CallRepository().createCall(
        conversationId: room?.id ?? '',
        sdp: sdp,
        receiverId: _receiverId,
      );
      if (callId != null) {
        SocketEmit().sendCalling(sdp: sdp, receiverId: _receiverId);
        _callId = callId;
      } else {
        add(EndVideoCallEvent());
      }
      return callId != null;
    } else {
      SocketEmit().sendAnswer(
        sdp: sdp,
        roomId: room?.id ?? '',
      );
    }
    return false;
  }

  Future _sendShareCandidates(
    String candidate,
    String sdpMid,
    int sdpMLineIndex,
  ) async {
    SocketEmit().sendShareCandidate(
      candidate: CandidateModel(
        candidate: candidate,
        sdpMid: sdpMid,
        sdpMLineIndex: sdpMLineIndex,
      ),
      roomId: room?.id ?? '',
    );
  }

  Future _sendShareSdp(
    String sdp,
    String type,
  ) async {
    if (type == "offer") {
      SocketEmit().sendShareOffer(
        sdp: sdp,
        roomId: room?.id ?? '',
      );
    } else {
      SocketEmit().sendShareAnswer(
        sdp: sdp,
        roomId: room?.id ?? '',
      );
    }
  }

  Future<RTC.MediaStream> _getUserMedia() async {
    final Map<String, dynamic> mediaConstraints = {
      'audio': true,
      'video': {
        'facingMode': 'user',
      },
    };

    RTC.MediaStream stream = await RTC.navigator.mediaDevices.getUserMedia(mediaConstraints);

    _localRenderer.srcObject = stream;

    return stream;
  }

  Future<RTC.MediaStream> _getScreenSharing() async {
    final mediaConstraints = <String, dynamic>{
      'video': true,
      'audio': true,
    };

    RTC.MediaStream stream = await RTC.navigator.mediaDevices.getDisplayMedia(mediaConstraints);

    _localShareRenderer.srcObject = stream;

    return stream;
  }

  Future<void> _replaceTrack(RTC.MediaStream? stream) async {
    List<RTC.RTCRtpSender> senders = await _peerConnection!.getSenders();
    senders.forEach(
      (sender) {
        if (sender.track!.kind != 'audio') {
          if (stream!.getVideoTracks().length > 0) {
            sender.replaceTrack(stream.getVideoTracks()[0]);
          }
        } else {
          if (stream!.getAudioTracks().length > 0) {
            sender.replaceTrack(stream.getAudioTracks()[0]);
          }
        }
      },
    );
  }

  Future<void> _addCandidate(CandidateModel candidateModel) async {
    RTC.RTCIceCandidate candidate = new RTC.RTCIceCandidate(
      candidateModel.candidate,
      candidateModel.sdpMid,
      candidateModel.sdpMLineIndex,
    );
    await _peerConnection!.addCandidate(candidate);
  }

  Future<void> _addShareCandidate(CandidateModel candidateModel) async {
    RTC.RTCIceCandidate candidate = new RTC.RTCIceCandidate(
      candidateModel.candidate,
      candidateModel.sdpMid,
      candidateModel.sdpMLineIndex,
    );
    await _peerShareConnection!.addCandidate(candidate);
  }

  void _setRemoteMic(bool value) {
    _isRemoteOnMic = value;
  }

  void _setRemoteCamera(bool value) {
    _isRemoteOnCamera = value;
  }

  void _setRemoteRaiseHand(bool value) {
    _isRemoteRaisingHand = value;
  }

  void _raiseHand() {
    _isMyRaisingHand = !_isMyRaisingHand;
    SocketEmit().sendRaiseHand(roomId: room?.id ?? '', isRaising: _isMyRaisingHand);
  }

  Future<void> _toggleVidelTrack({bool enable = true}) async {
    if (_localStream == null) throw Exception('Stream is not initialized');

    _localStream!.getVideoTracks().forEach((track) {
      if (track.kind == 'video') {
        track.enabled = enable;
      }
    });
  }

  Future<void> _endCall() async {
    await CallRepository().endCall(callId: _callId);
    SocketEmit().sendEndCall(
      roomId: room?.id ?? '',
    );
  }

  Future<void> _dispose() async {
    if (Platform.isIOS) {
      endCurrentCallIncomming();
    }

    if (AppNavigator.currentRoute() == Routes.VIDEO_CALL) {
      AppNavigator.pop();
    }

    AppBloc.timerBloc.add(EndTimerEvent());
    await _disposeShare();
    await _localStream?.dispose();
    await _peerConnection?.close();
    _callId = '';
    room = null;
    _receiverId = '';
    _peerConnection = null;
    _peerShareConnection = null;
    _localStream = null;
    _localShareStream = null;
    _localRenderer = RTC.RTCVideoRenderer();
    _localShareRenderer = RTC.RTCVideoRenderer();
    _remoteRenderer = RTC.RTCVideoRenderer();
    _remoteShareRenderer = RTC.RTCVideoRenderer();
    _isFrontCamera = true;
    _isSharingScreen = false;
    _micEnabled = true;
    _soundEnabled = true;
    _cameraEnabled = true;
    _offer = false;
    _isMyShare = false;
    _isShareFullScreen = false;
    _isVidCallFullScreen = false;
    _isMyRaisingHand = false;
    _isRemoteRaisingHand = false;
    _isRemoteOnMic = true;
    _isRemoteOnCamera = true;
    _candidates = [];
    _candidatesShare = [];
    // _isIncomingCall = false;
  }

  Future<void> _disposeShare() async {
    if (_isMyShare) {
      if (Platform.isAndroid) {
        await ForegroundService.stopForegroundService();
      }
      await _localShareStream?.dispose();
      await _peerShareConnection?.close();
      if (Platform.isIOS) {
        // ReplayKitLauncher.finishReplayKitBroadcast(
        //     'ZGFinishBroadcastUploadExtensionProcessNotification');
        _localStream = await _getUserMedia();
        await _replaceTrack(_localStream);
        if (!_micEnabled) {
          _toggleMic(enable: _micEnabled);
        }
        _cameraEnabled = true;
        SocketEmit().sendOnCamera(
          roomId: room?.id ?? '',
        );
      }
      SocketEmit().sendStopShareScreen(
        roomId: room?.id ?? '',
      );
    }
    _isSharingScreen = false;
    _isShareFullScreen = false;
    _isMyShare = false;
  }

  bool get isCalling => room != null;
}
