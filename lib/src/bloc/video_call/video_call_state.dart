part of 'video_call_bloc.dart';

@immutable
abstract class VideoCallState {}

class VideoCallInitial extends VideoCallState {}

class VideoCallIncoming extends VideoCallState {
  final IncomingCallModel incomingCallModel;
  VideoCallIncoming({required this.incomingCallModel});
}

class VideoCallIncomingCalling extends VideoCallState {
  final VideoCallModel videoCallModel;
  VideoCallIncomingCalling({required this.videoCallModel});
}

class VideoCalling extends VideoCallState {
  final VideoCallModel videoCallModel;
  VideoCalling({required this.videoCallModel});
}
