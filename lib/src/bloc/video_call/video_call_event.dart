part of 'video_call_bloc.dart';

@immutable
abstract class VideoCallEvent {}

class RequestVideoCallEvent extends VideoCallEvent {
  final String receiverId;
  final ConversationModel conversation;
  RequestVideoCallEvent({required this.receiverId, required this.conversation});
}

class SetRemoteDescriptionEvent extends VideoCallEvent {
  final String sdp;

  SetRemoteDescriptionEvent({required this.sdp});
}

class SetShareRemoteDescriptionEvent extends VideoCallEvent {
  final String sdp;

  SetShareRemoteDescriptionEvent({required this.sdp});
}

class StartVideoCallEvent extends VideoCallEvent {
  final IncomingCallModel incomingCallModel;
  StartVideoCallEvent({required this.incomingCallModel});
}

class AnswerVideoCallEvent extends VideoCallEvent {
  final IncomingCallModel incomingCallModel;
  AnswerVideoCallEvent({required this.incomingCallModel});
}

class AddCandidateEvent extends VideoCallEvent {
  final CandidateModel candidate;
  AddCandidateEvent({required this.candidate});
}

class AnswerShareVideoCallEvent extends VideoCallEvent {
  final String sdp;
  AnswerShareVideoCallEvent({required this.sdp});
}

class AddShareCandidateEvent extends VideoCallEvent {
  final CandidateModel candidate;
  AddShareCandidateEvent({required this.candidate});
}

class VideoCallingFullScreenEvent extends VideoCallEvent {}

class ShareFullScreenEvent extends VideoCallEvent {}

class ShareScreenEvent extends VideoCallEvent {}

class EndVideoCallEvent extends VideoCallEvent {}

class ToggleVideoEvent extends VideoCallEvent {}

class ToggleMicEvent extends VideoCallEvent {}

class ToggleSoundEvent extends VideoCallEvent {}

class SwitchCameraEvent extends VideoCallEvent {}

class StartShareScreenEvent extends VideoCallEvent {}

class StopShareScreenEvent extends VideoCallEvent {}

class StartRecordEvent extends VideoCallEvent {}

class ToggleMyRaiseHandEvent extends VideoCallEvent {}

class RemoteRaiseHandEvent extends VideoCallEvent {
  final bool isRaising;
  RemoteRaiseHandEvent({required this.isRaising});
}

class SetRemoteMicEvent extends VideoCallEvent {
  final bool isOn;
  SetRemoteMicEvent({required this.isOn});
}

class SetRemoteCameraEvent extends VideoCallEvent {
  final bool isOn;
  SetRemoteCameraEvent({required this.isOn});
}

class EndRecordEvent extends VideoCallEvent {}

class DisposeVideoCallEvent extends VideoCallEvent {}
