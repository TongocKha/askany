part of 'transaction_bloc.dart';

@immutable
abstract class TransactionEvent {}

class PayInVNPAYEvent extends TransactionEvent {
  final double amount;
  PayInVNPAYEvent({required this.amount});
}

class OnTransactionEvent extends TransactionEvent {}

class GetTransactionEvent extends TransactionEvent {}

class CleanTransactionEvent extends TransactionEvent {}

class RefreshTransactionEvent extends TransactionEvent {
  final Function handleFinished;
  RefreshTransactionEvent({required this.handleFinished});
}

class UpdateTransactionEvent extends TransactionEvent {
  final String transactionId;
  final int status;
  UpdateTransactionEvent({required this.transactionId, required this.status});
}

class SendRequestWithDrawEvent extends TransactionEvent {
  final String? bank;
  final double amount;
  final List<File> images;

  SendRequestWithDrawEvent({
    this.bank,
    required this.amount,
    required this.images,
  });
}
