part of 'transaction_bloc.dart';

@immutable
abstract class TransactionState {
  /// 0: List<TransactionModel>
  List get props => [];
}

class TransactionInitial extends TransactionState {
  @override
  List get props => [[]];
}

class GettingTransaction extends TransactionState {
  final List<TransactionModel> transactions;
  GettingTransaction({required this.transactions});

  @override
  List get props => [transactions];
}

class GetDoneTransaction extends TransactionState {
  final List<TransactionModel> transactions;
  GetDoneTransaction({required this.transactions});

  @override
  List get props => [transactions];
}
