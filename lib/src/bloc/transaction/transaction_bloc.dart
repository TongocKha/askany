import 'dart:io';
import 'dart:ui';
import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/user/user_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/data/remote_data_source/transaction_repository.dart';
import 'package:askany/src/models/slide_mode.dart';
import 'package:askany/src/models/transaction_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_with_text_and_pop_button.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:askany/src/helpers/extentions/string_extention.dart';
part 'transaction_event.dart';
part 'transaction_state.dart';

class TransactionBloc extends Bloc<TransactionEvent, TransactionState> {
  TransactionBloc() : super(TransactionInitial());
  List<TransactionModel> transactions = [];
  bool isOver = false;

  @override
  Stream<TransactionState> mapEventToState(TransactionEvent event) async* {
    if (event is OnTransactionEvent) {
      if (!isOver) {
        if (transactions.isEmpty) {
          yield TransactionInitial();

          await getTransactions();
        }
      }
      yield _getDoneTransactions;
    }

    if (event is GetTransactionEvent) {
      if (!isOver) {
        if (transactions.isEmpty) {
          yield TransactionInitial();
        } else {
          yield _gettingTransaction;
        }
        await getTransactions();

        yield _getDoneTransactions;
      }
    }

    if (event is RefreshTransactionEvent) {
      _cleanBloc();
      await getTransactions();
      event.handleFinished();
      yield _getDoneTransactions;
    }

    if (event is PayInVNPAYEvent) {
      String? linkRecharge =
          await TransactionRepository().getLinkRecharge(event.amount);
      AppNavigator.pop();
      if (linkRecharge != null) {
        TransactionModel _newTransaction =
            defaultTransaction(amount: event.amount);
        transactions.insert(0, _newTransaction);
        AppNavigator.push(
          Routes.WEB_VIEW_PAYMENT,
          arguments: {
            'url': linkRecharge,
            'onPaymentDone': (value) {
              bool? result = value;
              if (result != null) {
                if (result) {
                  AppBloc.userBloc.add(UpdateUserWalletEvent(
                      updatedWallet: event.amount +
                          AppBloc.userBloc.accountModel!.wallet!));
                  AppNavigator.push(Routes.SUCCESSFUL_RECHARGE_VNPAY,
                      arguments: {
                        'money': event.amount.toStringAsFixed(0).formatMoney(),
                      });
                }
                add(
                  UpdateTransactionEvent(
                    transactionId: _newTransaction.id,
                    status: result ? PAYMENT_SUCCESSFUL : PAYMENT_FAILED,
                  ),
                );
              }
            }
          },
        );
      } else {
        dialogAnimationWrapper(
          slideFrom: SlideMode.bot,
          child: DialogWithTextAndPopButton(
            title: Strings.errorOccurredNoti.i18n,
            bodyAfter: Strings.errorOccurredTryAgain.i18n,
          ),
        );
      }
      yield _getDoneTransactions;
    }
    if (event is SendRequestWithDrawEvent) {
      await _sendRequestWithDraw(event);

      yield _getDoneTransactions;
    }

    if (event is UpdateTransactionEvent) {
      int indexOfTransaction = transactions.indexWhere(
        (transaction) => transaction.id == event.transactionId,
      );

      if (indexOfTransaction != PAYMENT_FAILED) {
        transactions[indexOfTransaction].status = event.status;
      }
      yield _getDoneTransactions;
    }

    if (event is CleanTransactionEvent) {
      _cleanBloc();
      yield TransactionInitial();
    }
  }

  // MARK: Private methods

  GettingTransaction get _gettingTransaction =>
      GettingTransaction(transactions: transactions);
  GetDoneTransaction get _getDoneTransactions =>
      GetDoneTransaction(transactions: transactions);

  TransactionModel defaultTransaction({
    required double amount,
    int? type,
  }) =>
      TransactionModel(
        user: AppBloc.userBloc.getAccount,
        id: DateTime.now().microsecondsSinceEpoch.toString(),
        status: WAITING_FOR_PROGRESSING,
        amount: amount,
        type: type ??
            (UserLocal().getIsExpert()
                ? EXPERT_RECHAGE_VNPAY
                : USER_RECHARGE_VNPAY),
        createdAt: DateTime.now().toLocal(),
      );

  Future<void> getTransactions() async {
    List<TransactionModel> _transactions =
        await TransactionRepository().getTransactions(
      skip: transactions.length,
    );

    if (_transactions.length < LIMIT_API_10) {
      isOver = true;
    }
    transactions.addAll(_transactions);
  }

  Future<void> _sendRequestWithDraw(SendRequestWithDrawEvent event) async {
    bool _isSucceed = await TransactionRepository().withdrawRequest(
      bank: event.bank!,
      amount: event.amount,
      images: event.images,
    );
    AppNavigator.pop();

    if (_isSucceed) {
      TransactionModel _newTransaction = defaultTransaction(
        amount: event.amount,
        type: UserLocal().getIsExpert()
            ? EXPERT_WITHDRAW_MONEY_VNPAY
            : USER_WITHDRAW_MONEY_BANK,
      );
      transactions.insert(0, _newTransaction);
      AppNavigator.popUntil(Routes.WALLET);
      dialogAnimationWrapper(
        child: DialogWithTextAndPopButton(
          title: Strings.withdrawSuccessNoti.i18n,
          bodyAlign: TextAlign.center,
          bodyAfter: Strings.withdrawProcessIn30Days.i18n,
        ),
      );
    } else {
      dialogAnimationWrapper(
        child: DialogWithTextAndPopButton(
          title: Strings.withdrawFailedNoti.i18n,
          bodyAlign: TextAlign.center,
          bodyAfter: Strings.withdrawProcessIn30Days.i18n,
        ),
      );
    }
  }

  _cleanBloc() {
    transactions.clear();
    isOver = false;
  }
}
