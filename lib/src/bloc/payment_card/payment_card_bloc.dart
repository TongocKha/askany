import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/data/remote_data_source/transaction_repository.dart';
import 'package:askany/src/models/payment_card_model.dart';
import 'package:askany/src/models/slide_mode.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_with_text_and_next_screen.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_with_text_and_pop_button.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
part 'payment_card_event.dart';
part 'payment_card_state.dart';

class PaymentCardBloc extends Bloc<PaymentCardEvent, PaymentCardState> {
  PaymentCardBloc() : super(PaymentCardInitial());
  List<PaymentCardModel> paymentCardList = [];
  bool isOver = false;
  @override
  Stream<PaymentCardState> mapEventToState(PaymentCardEvent event) async* {
    if (event is OnPaymentCardEvent) {
      if (!isOver) {
        if (paymentCardList.isEmpty) {
          yield PaymentCardInitial();

          await _getPaymentCardList();
        }
      }
      yield _getDonePaymentCard;
    }
    if (event is GetPaymentCardEvent) {
      if (!isOver) {
        if (paymentCardList.isEmpty) {
          yield PaymentCardInitial();
        } else {
          yield _gettingPaymentCard;
        }
        await _getPaymentCardList();

        yield _getDonePaymentCard;
      }
    }
    if (event is RefreshPaymentCardEvent) {
      _cleanBloc();
      await _getPaymentCardList();
      event.handleFinished();
      yield _getDonePaymentCard;
    }

    if (event is CleanPaymentCardEvent) {
      _cleanBloc();
      yield PaymentCardInitial();
    }

    if (event is SavePaymentCardEvent) {
      await addPaymentCard(event);
      yield _getDonePaymentCard;
    }
    if (event is SavePayOutPaymentCardEvent) {
      await payOutAddPaymentCard(event);
      yield _getDonePaymentCard;
    }

    if (event is UpdatePaymentCardEvent) {
      await updatePaymentCard(event);
      yield _getDonePaymentCard;
    }
    if (event is DeletePaymentCard) {
      await _deletePaymentCard(event);
      yield _getDonePaymentCard;
    }
  }

  GettingPaymentCard get _gettingPaymentCard =>
      GettingPaymentCard(paymenCardList: paymentCardList);
  GetDonePaymentCard get _getDonePaymentCard =>
      GetDonePaymentCard(paymenCardList: paymentCardList);

  Future<void> _getPaymentCardList() async {
    List<PaymentCardModel> _paymentCard =
        await TransactionRepository().getPaymentCard();
    paymentCardList.addAll(_paymentCard);
  }

  Future<void> addPaymentCard(SavePaymentCardEvent event) async {
    String? paymentCard = await TransactionRepository().addPaymentCard(
      bankName: event.bankName,
      cardType: event.cardType,
      accountName: event.accountName,
      cardNumber: event.cardNumber,
      bankCode: event.bankCode,
      country: event.country ?? '',
      billAddress: event.billAddress ?? '',
      city: event.city ?? '',
      postOfficeCode: event.postOfficeCode ?? '',
    );
    AppNavigator.pop();
    if (paymentCard != null) {
      PaymentCardModel _newPaymentCard = PaymentCardModel(
        id: paymentCard,
        authorUser: AppBloc.userBloc.getAccount,
        bankCode: event.bankCode,
        cardNumber: event.cardNumber.replaceAll('\t\t', ''),
        accountName: event.accountName,
        bankName: event.bankName,
        cardType: event.cardType,
        country: event.country,
        city: event.city,
        billAddress: event.billAddress,
      );

      paymentCardList.add(_newPaymentCard);

      AppNavigator.popUntil(Routes.SAVE_CREDIT_CARD);
    } else {
      dialogAnimationWrapper(
        child: DialogWithTextAndPopButton(
          bodyAlign: TextAlign.center,
          bodyAfter: 'Hệ thống đang lỗi vui lòng thử lại',
        ),
      );
    }
  }

  Future<void> payOutAddPaymentCard(SavePayOutPaymentCardEvent event) async {
    String? paymentCard = await TransactionRepository().addPaymentCard(
      bankName: event.bankName,
      cardType: event.cardType,
      accountName: event.accountName,
      cardNumber: event.cardNumber,
      bankCode: event.bankCode,
      country: event.country ?? '',
      billAddress: event.billAddress ?? '',
      city: event.city ?? '',
      postOfficeCode: event.postOfficeCode ?? '',
    );
    AppNavigator.pop();

    if (event.amount.length >= 10) {
      AppNavigator.push(
        Routes.WITHDRAWOVER10,
        arguments: {
          'id': paymentCard ?? '',
          'amount': event.amount,
          'type': event.type,
        },
      );
    } else {
      AppNavigator.push(
        Routes.WITHDRAWUNDER10,
        arguments: {
          'id': paymentCard ?? '',
          'amount': event.amount,
          'type': event.type,
        },
      );
    }
    if (paymentCard != null) {
      final foundPeople =
          paymentCardList.where((element) => element.id == paymentCard);

      if (foundPeople.isEmpty) {
        PaymentCardModel _newPaymentCard = PaymentCardModel(
          id: paymentCard,
          authorUser: AppBloc.userBloc.getAccount,
          bankCode: event.bankCode,
          cardNumber: event.cardNumber.replaceAll('\t\t', ''),
          accountName: event.accountName,
          bankName: event.bankName,
          cardType: event.cardType,
          country: event.country,
          city: event.city,
          billAddress: event.billAddress,
        );

        paymentCardList.add(_newPaymentCard);
      } else {
        PaymentCardModel _newPaymentCard = PaymentCardModel(
          id: paymentCard,
          authorUser: AppBloc.userBloc.getAccount,
          bankCode: event.bankCode,
          cardNumber: event.cardNumber.replaceAll('\t\t', ''),
          accountName: event.accountName,
          bankName: event.bankName,
          cardType: event.cardType,
          country: event.country ?? '',
          city: event.city ?? '',
          billAddress: event.billAddress ?? '',
        );
        int indexOfPaymentCard =
            paymentCardList.indexWhere((id) => paymentCard == id.id);
        paymentCardList.remove(paymentCardList[indexOfPaymentCard]);
        paymentCardList.insert(0, _newPaymentCard);
      }
    } else {
      dialogAnimationWrapper(
        child: DialogWithTextAndNextScreen(
          onPressed: () {
            AppNavigator.popUntil(Routes.WITHDRAWMONEY);
          },
          bodyAlign: TextAlign.center,
          bodyAfter: 'Hệ thống đang lỗi vui lòng thử lại',
        ),
      );
    }
  }

  Future<void> updatePaymentCard(UpdatePaymentCardEvent event) async {
    String? paymentCard = await TransactionRepository().updatePaymentCard(
      id: event.id,
      bankName: event.bankName,
      cardType: event.cardType,
      accountName: event.accountName,
      cardNumber: event.cardNumber,
      bankCode: event.bankCode,
      country: event.country ?? '',
      billAddress: event.billAddress ?? '',
      city: event.city ?? '',
      postOfficeCode: event.postOfficeCode ?? '',
    );
    AppNavigator.pop();
    if (paymentCard != null) {
      PaymentCardModel _newPaymentCard = PaymentCardModel(
        id: paymentCard,
        authorUser: AppBloc.userBloc.getAccount,
        bankCode: event.bankCode,
        cardNumber: event.cardNumber.replaceAll('\t\t', ''),
        accountName: event.accountName,
        bankName: event.bankName,
        cardType: event.cardType,
        country: event.country ?? '',
        city: event.city ?? '',
        billAddress: event.billAddress ?? '',
      );
      int indexOfPaymentCard =
          paymentCardList.indexWhere((id) => event.id == id.id);
      paymentCardList.remove(paymentCardList[indexOfPaymentCard]);
      paymentCardList.insert(0, _newPaymentCard);
      AppNavigator.popUntil(Routes.SAVE_CREDIT_CARD);
      dialogAnimationWrapper(
        child: DialogWithTextAndPopButton(
          bodyAlign: TextAlign.center,
          bodyAfter: 'Cập nhật thẻ thành công',
        ),
      );
    } else {
      dialogAnimationWrapper(
        child: DialogWithTextAndPopButton(
          bodyAlign: TextAlign.center,
          bodyAfter: 'Cập nhật thẻ thất bại',
        ),
      );
    }
  }

  Future<bool> _deletePaymentCard(DeletePaymentCard event) async {
    bool _isDeleteSucceed = await TransactionRepository().deletePaymentCard(
      id: event.id,
    );

    if (!_isDeleteSucceed) {
      AppNavigator.popUntil(Routes.SAVE_CREDIT_CARD);
      dialogAnimationWrapper(
        slideFrom: SlideMode.bot,
        child: DialogWithTextAndPopButton(
          bodyAfter: "Hệ thống đang lỗi, vui lòng thử lại sau",
          bodyAlign: TextAlign.center,
        ),
      );
    } else {
      int indexOfPaymentCard =
          paymentCardList.indexWhere((id) => event.id == id.id);
      paymentCardList.remove(paymentCardList[indexOfPaymentCard]);
      AppNavigator.popUntil(Routes.SAVE_CREDIT_CARD);
      dialogAnimationWrapper(
        slideFrom: SlideMode.bot,
        child: DialogWithTextAndPopButton(
          bodyAfter: 'Xoá thẻ thành công',
          bodyAlign: TextAlign.center,
        ),
      );
    }
    return _isDeleteSucceed;
  }

  _cleanBloc() {
    paymentCardList.clear();
    isOver = false;
  }
}
