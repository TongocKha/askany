part of 'payment_card_bloc.dart';

@immutable
abstract class PaymentCardEvent {}

class OnPaymentCardEvent extends PaymentCardEvent {}

class GetPaymentCardEvent extends PaymentCardEvent {}

class CleanPaymentCardEvent extends PaymentCardEvent {}

class AddPaymentCardEvent extends PaymentCardEvent {}

class DeletePaymentCardEvent extends PaymentCardEvent {}

class RefreshPaymentCardEvent extends PaymentCardEvent {
  final Function handleFinished;
  RefreshPaymentCardEvent({required this.handleFinished});
}

class SavePaymentCardEvent extends PaymentCardEvent {
  final String bankName;
  final int cardType;
  final String accountName;
  final String cardNumber;
  final String? country;
  final String bankCode;
  final String? postOfficeCode;
  final String? billAddress;
  final String? city;
  SavePaymentCardEvent({
    required this.bankName,
    required this.cardType,
    required this.accountName,
    required this.cardNumber,
    this.country,
    required this.bankCode,
    this.billAddress,
    this.postOfficeCode,
    this.city,
  });
}

class SavePayOutPaymentCardEvent extends PaymentCardEvent {
  final String type;
  final String amount;
  final String bankName;
  final int cardType;
  final String accountName;
  final String cardNumber;
  final String? country;
  final String bankCode;
  final String? postOfficeCode;
  final String? billAddress;
  final String? city;
  SavePayOutPaymentCardEvent({
    required this.type,
    required this.amount,
    required this.bankName,
    required this.cardType,
    required this.accountName,
    required this.cardNumber,
    this.country,
    required this.bankCode,
    this.billAddress,
    this.postOfficeCode,
    this.city,
  });
}

class UpdatePaymentCardEvent extends PaymentCardEvent {
  final String id;
  final String bankName;
  final int cardType;
  final String accountName;
  final String cardNumber;
  final String? country;
  final String bankCode;
  final String? postOfficeCode;
  final String? billAddress;
  final String? city;
  UpdatePaymentCardEvent({
    required this.id,
    required this.bankName,
    required this.cardType,
    required this.accountName,
    required this.cardNumber,
    this.country,
    required this.bankCode,
    this.billAddress,
    this.postOfficeCode,
    this.city,
  });
}

class DeletePaymentCard extends PaymentCardEvent {
  final String id;
  DeletePaymentCard({required this.id});
}
