part of 'payment_card_bloc.dart';

@immutable
abstract class PaymentCardState {
  List get props => [];
}

class PaymentCardInitial extends PaymentCardState {
  @override
  List get props => [[]];
}

class GettingPaymentCard extends PaymentCardState {
  final List<PaymentCardModel> paymenCardList;
  GettingPaymentCard({required this.paymenCardList});

  @override
  List get props => [paymenCardList];
}

class GetDonePaymentCard extends PaymentCardState {
  final List<PaymentCardModel> paymenCardList;
  GetDonePaymentCard({required this.paymenCardList});

  @override
  List get props => [paymenCardList];
}
