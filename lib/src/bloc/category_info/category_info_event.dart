part of 'category_info_bloc.dart';

@immutable
abstract class CategoryInfoEvent {}

class GetInfoCategoryEvent extends CategoryInfoEvent {
  final String slug;
  final String? slugSpecialty;
  GetInfoCategoryEvent({required this.slug, this.slugSpecialty});
}

class CleanCategoryInfoEvent extends CategoryInfoEvent {}
