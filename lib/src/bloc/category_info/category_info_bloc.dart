import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/content/content_bloc.dart';
import 'package:askany/src/bloc/home/home_bloc.dart';
import 'package:askany/src/data/remote_data_source/category_repository.dart';
import 'package:askany/src/models/category_model.dart';
import 'package:askany/src/routes/app_navigator_observer.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'category_info_event.dart';
part 'category_info_state.dart';

class CategoryInfoBloc extends Bloc<CategoryInfoEvent, CategoryInfoState> {
  CategoryInfoBloc() : super(CategoryInfoInitial());

  Map<String, CategoryInfoModel> categories = {};
  @override
  Stream<CategoryInfoState> mapEventToState(CategoryInfoEvent event) async* {
    if (event is GetInfoCategoryEvent) {
      showDialogLoading();
      if (categories[event.slug] == null) {
        await _getInfo(event.slug);
      }
      AppNavigator.pop();
      AppBloc.homeBloc.add(OnChangeIndexEvent(index: 2));
      if (AppNavigatorObserver.currentRouteName != Routes.SUB_CATEGORY) {
        AppNavigator.push(Routes.SUB_CATEGORY, arguments: {
          'categoryInfo': categories[event.slug],
        });
      }

      if (event.slugSpecialty != null || event.slugSpecialty!.isNotEmpty) {
        showDialogLoading();
        AppBloc.contentBloc.add(
          OnExpertCategoryEvent(
            slug: event.slugSpecialty!,
          ),
        );
        AppNavigator.pop();

        int indexOf = categories[event.slug]!.listSubCates.indexWhere(
            (categoryModel) => categoryModel.slug == event.slugSpecialty);

        if (indexOf != -1) {
          if (AppNavigatorObserver.currentRouteName !=
              Routes.DETAILS_CATEGORY) {
            AppNavigator.push(
              Routes.DETAILS_CATEGORY,
              arguments: {
                'categoryModel': categories[event.slug]!.listSubCates[indexOf],
              },
            );
          }
        }
      }
      yield _getDoneCategoryInfo;
    }
    if (event is CleanCategoryInfoEvent) {
      yield CategoryInfoInitial();
    }
  }

  //private
  GetDoneInfoCategory get _getDoneCategoryInfo =>
      GetDoneInfoCategory(categoryInfoModel: categories);

  Future<void> _getInfo(String slug) async {
    CategoryInfoModel? categoryInfoModel =
        await CategoryRepository().getCategoriesSlug(slug: slug);
    if (categoryInfoModel != null) {
      categories[slug] = categoryInfoModel;
    }
  }
}
