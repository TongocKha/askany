part of 'category_info_bloc.dart';

@immutable
abstract class CategoryInfoState {
  List get props => [];
}

class CategoryInfoInitial extends CategoryInfoState {
  List get props => [];
}

class GetDoneInfoCategory extends CategoryInfoState {
  final Map<String, CategoryInfoModel> categoryInfoModel;
  GetDoneInfoCategory({required this.categoryInfoModel});

  @override
  List get props => [categoryInfoModel];
}
