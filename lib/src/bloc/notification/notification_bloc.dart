import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/badge/badge_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/data/remote_data_source/notification_repository.dart';
import 'package:askany/src/models/notification_model.dart';
import 'package:askany/src/models/request_model.dart';
import 'package:askany/src/models/slide_mode.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/services/firebase_messaging/local_notification.dart';
import 'package:askany/src/services/socket/socket_emit.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_with_text_and_pop_button.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:askany/src/helpers/extentions/string_extention.dart';
part 'notification_event.dart';
part 'notification_state.dart';

class NotificationBloc extends Bloc<NotificationEvent, NotificationState> {
  NotificationBloc() : super(NotificationInitial());

  List<NotificationModel> notifications = [];
  bool isOver = false;

  @override
  Stream<NotificationState> mapEventToState(NotificationEvent event) async* {
    if (event is GetNotificationEvent) {
      if (!isOver) {
        if (notifications.isEmpty) {
          yield NotificationInitial();
        } else {
          yield gettingNotification;
        }

        await _getNotifications();

        yield getDoneNotification;
      }
    }

    if (event is MarkSeenNotificationEvent) {
      await _markAsRead(event);
      yield getDoneNotification;
    }

    if (event is MarkSeenAllNotificationEvent) {
      await _markAsReadAll();
      yield getDoneNotification;
    }

    if (event is DeleteNotificationEvent) {
      await _deleteNotification(event);
      yield getDoneNotification;
    }

    if (event is CleanNotificationEvent) {
      _cleanNotification();
      yield NotificationInitial();
    }

    if (event is CreateNotificationEvent) {
      await _createNotification(event);
    }

    if (event is InsertNotificationEvent) {
      _insertNotication(event);
      yield getDoneNotification;
    }
    if (event is TouchNotificationEvent) {
      showDialogLoading();
      await _getOfferById(offerId: event.offerId);
    }

    if (event is RefreshNotificationEvent) {
      _cleanNotification();
      await _getNotifications();
      event.handleFinished();
      yield getDoneNotification;
    }
  }

  // MARK: Private methods
  GettingNotification get gettingNotification =>
      GettingNotification(notifications: notifications);
  GetDoneNotification get getDoneNotification =>
      GetDoneNotification(notifications: notifications);

  Future<void> _getNotifications() async {
    List<NotificationModel> _notifications =
        await NotificationRepository().getNotifications(
      skip: notifications.length,
    );

    if (_notifications.length < LIMIT_API_10) {
      isOver = true;
    }

    notifications.addAll(_notifications);
  }

  Future<void> _markAsRead(MarkSeenNotificationEvent event) async {
    bool isUpdateSucceed = await NotificationRepository().markAsRead(
      notificationId: event.notificationId,
    );

    if (isUpdateSucceed) {
      int indexOfNotification = notifications.indexWhere(
        (notification) => notification.id == event.notificationId,
      );

      if (indexOfNotification != -1) {
        AppBloc.badgesBloc.add(UpdateBadgesNotificationEvent(extendValue: -1));
        notifications[indexOfNotification].status = 1;
      }
    }
  }

  Future<void> _markAsReadAll() async {
    if (AppBloc.badgesBloc.badgesNotification > 0) {
      bool isUpdateSucceed = await NotificationRepository().markAsReadAll();

      if (isUpdateSucceed) {
        AppBloc.badgesBloc.add(GetBadgesNotificationEvent(badges: 0));

        notifications.forEach((notification) {
          notification.status = IS_SEEN;
        });
      }
    }
  }

  Future<void> _deleteNotification(DeleteNotificationEvent event) async {
    bool isDeleteSucceed = await NotificationRepository().deleteNotification(
      notificationId: event.notificationId,
    );

    AppNavigator.pop();

    if (isDeleteSucceed) {
      int indexOfNotification = notifications.indexWhere(
        (notification) => notification.id == event.notificationId,
      );

      if (indexOfNotification != -1) {
        notifications.removeAt(indexOfNotification);
      }
    }
  }

  Future<void> _createNotification(CreateNotificationEvent event) async {
    NotificationModel? _notification =
        await NotificationRepository().createNotification(
      typeMessage: event.messageType,
      userId: event.userId,
      requestId: event.requestId,
      offerId: event.offerId,
    );
    if (_notification != null) {
      SocketEmit().sendNotification(_notification);
    }
  }

  _insertNotication(InsertNotificationEvent event) {
    notifications.insert(0, event.notification);
    AppBloc.badgesBloc.add(UpdateBadgesNotificationEvent(extendValue: 1));
    LocalNotification().showNotificationWithSound(
      hashCode: 9798,
      title: 'Thông báo mới',
      description: ''
          .getTitleNotification(event.notification)
          .map((item) => item.text)
          .join(''),
      payload: {...event.notification.toMap(), 'route': 'notification'},
      urlToImage: null,
    );
  }

  Future<void> _getOfferById({required String offerId}) async {
    RequestModel? _newOffer = await NotificationRepository().getOfferFromId(
      offerId: offerId,
    );
    AppNavigator.pop();
    if (_newOffer != null) {
      AppNavigator.push(
        Routes.DETAILS_SERVICE,
        arguments: {
          'requestModel': _newOffer,
        },
      );
    } else {
      dialogAnimationWrapper(
        slideFrom: SlideMode.bot,
        child: DialogWithTextAndPopButton(
          title: Strings.errorOccurredNoti.i18n,
          bodyAfter: Strings.errorOccurredTryAgain.i18n,
        ),
      );
    }
  }

  void _cleanNotification() {
    notifications.clear();
    isOver = false;
  }
}
