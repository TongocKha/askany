part of 'notification_bloc.dart';

@immutable
abstract class NotificationEvent {}

class GetNotificationEvent extends NotificationEvent {}

class MarkSeenNotificationEvent extends NotificationEvent {
  final String notificationId;
  MarkSeenNotificationEvent({required this.notificationId});
}

class MarkSeenAllNotificationEvent extends NotificationEvent {
  MarkSeenAllNotificationEvent();
}

class DeleteNotificationEvent extends NotificationEvent {
  final String notificationId;
  DeleteNotificationEvent({required this.notificationId});
}

class CleanNotificationEvent extends NotificationEvent {}

class RefreshNotificationEvent extends NotificationEvent {
  final Function handleFinished;
  RefreshNotificationEvent({required this.handleFinished});
}

class CreateNotificationEvent extends NotificationEvent {
  final int messageType;
  final String userId;
  final String? requestId;
  final String? offerId;
  CreateNotificationEvent({
    required this.messageType,
    required this.userId,
    this.requestId,
    this.offerId,
  });
}

class InsertNotificationEvent extends NotificationEvent {
  final NotificationModel notification;
  InsertNotificationEvent({required this.notification});
}

class TouchNotificationEvent extends NotificationEvent {
  final String offerId;
  TouchNotificationEvent({
    required this.offerId,
  });
}
