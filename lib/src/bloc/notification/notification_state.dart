part of 'notification_bloc.dart';

@immutable
abstract class NotificationState {
  /// 0: List<NotificationModel>
  List get props => [];
}

class NotificationInitial extends NotificationState {
  @override
  List get props => [[]];
}

class GettingNotification extends NotificationState {
  final List<NotificationModel> notifications;

  GettingNotification({required this.notifications});
  @override
  List get props => [notifications];
}

class GetDoneNotification extends NotificationState {
  final List<NotificationModel> notifications;

  GetDoneNotification({required this.notifications});
  @override
  List get props => [notifications];
}
