part of 'hot_expert_home_bloc.dart';

@immutable
abstract class HotExpertHomeState {
  List get props => [[]];
}

class HotExpertHomeInitial extends HotExpertHomeState {
  List get props => [[]];
}

class GetHotExpertList extends HotExpertHomeState {
  final List<ExpertModel> experts;
  GetHotExpertList({required this.experts});

  @override
  List get props => [experts];
}
