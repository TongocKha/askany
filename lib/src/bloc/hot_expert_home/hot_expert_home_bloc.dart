import 'package:askany/src/data/remote_data_source/expert_repository.dart';
import 'package:askany/src/models/expert_model.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'hot_expert_home_event.dart';
part 'hot_expert_home_state.dart';

class HotExpertHomeBloc extends Bloc<HotExpertHomeEvent, HotExpertHomeState> {
  HotExpertHomeBloc() : super(HotExpertHomeInitial());

  List<ExpertModel> experts = [];

  @override
  Stream<HotExpertHomeState> mapEventToState(HotExpertHomeEvent event) async* {
    if (event is GetHotExpertEvent) {
      if (experts.isEmpty) {
        await _getHotExpert();
        yield _getDoneHotExperts;
      }
    }

    if (event is RefreshHotExpertEvent) {
      yield HotExpertHomeInitial();
      await _getHotExpert();
      yield _getDoneHotExperts;
    }
  }

  //private
  GetHotExpertList get _getDoneHotExperts => GetHotExpertList(experts: experts);

  Future<void> _getHotExpert() async {
    List<ExpertModel> _experts = await ExpertRepository().getHotExpert();

    if (_experts.isNotEmpty) {
      experts.addAll(_experts);
    }
  }
}
