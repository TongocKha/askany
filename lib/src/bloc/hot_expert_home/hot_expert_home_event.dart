part of 'hot_expert_home_bloc.dart';

@immutable
abstract class HotExpertHomeEvent {}

class GetHotExpertEvent extends HotExpertHomeEvent {}

class RefreshHotExpertEvent extends HotExpertHomeEvent {}
