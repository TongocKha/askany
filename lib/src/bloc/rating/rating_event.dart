part of 'rating_bloc.dart';

@immutable
abstract class RatingEvent {}

class OnRatingExpertEvent extends RatingEvent {
  final String expertId;
  OnRatingExpertEvent({required this.expertId});
}

class GetRatingExpertEvent extends RatingEvent {}

class RefreshRatingExpertEvent extends RatingEvent {
  final Function handleFinished;
  RefreshRatingExpertEvent({required this.handleFinished});
}

class FilterRatingStarEvent extends RatingEvent {
  final int stars;
  FilterRatingStarEvent({required this.stars});
}

class OnRatingSkillEvent extends RatingEvent {
  final String skillId;
  OnRatingSkillEvent({required this.skillId});
}

class GetRatingSkillEvent extends RatingEvent {}

class RefreshRatingSkillEvent extends RatingEvent {
  final Function handleFinished;
  RefreshRatingSkillEvent({required this.handleFinished});
}

class FilterRatingStarSkillEvent extends RatingEvent {
  final int stars;
  FilterRatingStarSkillEvent({required this.stars});
}

class CleanRatingExpertEvent extends RatingEvent {}
