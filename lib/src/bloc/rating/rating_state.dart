part of 'rating_bloc.dart';

@immutable
abstract class RatingState {
  List get props => [];
}

class RatingInitial extends RatingState {
  List get props => [null];
}

class GettingRatingExpert extends RatingState {
  final Map<String, RatingStoreModel> ratings;
  GettingRatingExpert({required this.ratings});

  @override
  List get props => [ratings];
}

class GetDoneRatingExpert extends RatingState {
  final Map<String, RatingStoreModel> ratings;
  GetDoneRatingExpert({required this.ratings});

  @override
  List get props => [ratings];
}

class GettingRatingSkill extends RatingState {
  final Map<String, RatingStoreModel> ratings;
  GettingRatingSkill({required this.ratings});

  @override
  List get props => [ratings];
}

class GetDoneRatingSkill extends RatingState {
  final Map<String, RatingStoreModel> ratings;
  GetDoneRatingSkill({required this.ratings});

  @override
  List get props => [ratings];
}
