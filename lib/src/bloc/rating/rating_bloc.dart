import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/data/remote_data_source/rating_repository.dart';
import 'package:askany/src/models/rating_management_model.dart';
import 'package:askany/src/models/rating_model.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'rating_event.dart';
part 'rating_state.dart';

class RatingBloc extends Bloc<RatingEvent, RatingState> {
  RatingBloc() : super(RatingInitial());

  Map<String, RatingStoreModel> ratingsExpert = {};
  Map<String, RatingStoreModel> ratingsSkill = {};
  String idExpert = '';
  String idSkill = '';
  int stars = -1;

  @override
  Stream<RatingState> mapEventToState(RatingEvent event) async* {
    if (event is OnRatingExpertEvent) {
      yield RatingInitial();
      stars = -1;
      idExpert = event.expertId;
      await _getListRatingExpert();
      yield _getDoneRatingExpert;
    }

    if (event is GetRatingExpertEvent) {
      if (!(ratingsExpert[idExpert]?.getRatingsByStars(stars).isOver ??
          false)) {
        yield _gettingRatingExpert;
        await _getListRatingExpert();
      }
      yield _getDoneRatingExpert;
    }

    if (event is RefreshRatingExpertEvent) {
      await _refreshRatingExpert(event);
      yield _getDoneRatingExpert;
    }

    if (event is FilterRatingStarEvent) {
      stars = event.stars;
      if (!(ratingsExpert[idExpert]?.getRatingsByStars(stars).isOver ??
          false)) {
        if ((ratingsExpert[idExpert]!.getRatingsByStars(stars).ratings.length) <
            LIMIT_API_10) {
          if (ratingsExpert[idExpert]!
              .getRatingsByStars(stars)
              .ratings
              .isEmpty) {
            yield RatingInitial();
          } else {
            yield _gettingRatingExpert;
          }
          await _getListRatingExpert();
        }
      }

      yield _getDoneRatingExpert;
    }

    // ========== SKILLS =============

    if (event is OnRatingSkillEvent) {
      yield RatingInitial();
      stars = -1;
      idSkill = event.skillId;
      await _getListRatingSkill();
      yield _getDoneRatingSkill;
    }

    if (event is GetRatingSkillEvent) {
      if (!(ratingsSkill[idSkill]?.getRatingsByStars(stars).isOver ?? false)) {
        yield _gettingRatingSkill;
        await _getListRatingSkill();
      }
      yield _getDoneRatingSkill;
    }

    if (event is RefreshRatingSkillEvent) {
      await _refreshRatingSkill(event);
      yield _getDoneRatingSkill;
    }

    if (event is FilterRatingStarSkillEvent) {
      stars = event.stars;
      if (!(ratingsSkill[idSkill]?.getRatingsByStars(stars).isOver ?? false)) {
        if ((ratingsSkill[idSkill]!.getRatingsByStars(stars).ratings.length) <
            LIMIT_API_10) {
          if (ratingsSkill[idSkill]!.getRatingsByStars(stars).ratings.isEmpty) {
            yield RatingInitial();
          } else {
            yield _gettingRatingSkill;
          }
          await _getListRatingSkill();
        }
      }

      yield _getDoneRatingSkill;
    }

    if (event is CleanRatingExpertEvent) {
      _cleanBloc();
      yield RatingInitial();
    }
  }

  // Private methods

  Future<void> _refreshRatingExpert(RefreshRatingExpertEvent event) async {
    ratingsExpert.remove(idExpert);
    await _getListRatingExpert();
    event.handleFinished();
  }

  Future<void> _refreshRatingSkill(RefreshRatingSkillEvent event) async {
    ratingsSkill.remove(idSkill);
    await _getListRatingSkill();
    event.handleFinished();
  }

  Future<void> _getListRatingExpert() async {
    List<RatingModel> _listRating =
        await RatingRepository().getListRatingExpert(
      expertId: idExpert,
      skip:
          ratingsExpert[idExpert]?.getRatingsByStars(stars).ratings.length ?? 0,
      stars: stars,
    );

    bool isOver = false;

    if (_listRating.length < LIMIT_API_10) {
      isOver = true;
    }

    if (ratingsExpert[idExpert] == null) {
      ratingsExpert[idExpert] =
          RatingStoreModel.fromAllRatings(_listRating, isOver);
    } else {
      ratingsExpert[idExpert] = addToStars(
        ratings: ratingsExpert[idExpert]!,
        ratingsNew: _listRating,
        stars: stars,
        isOver: isOver,
      );
    }
  }

  Future<void> _getListRatingSkill() async {
    List<RatingModel> _listRating = await RatingRepository().getListRatingSkill(
      skillId: idSkill,
      skip: ratingsSkill[idSkill]?.getRatingsByStars(stars).ratings.length ?? 0,
      stars: stars,
    );

    bool isOver = false;

    if (_listRating.length < LIMIT_API_10) {
      isOver = true;
    }

    if (ratingsSkill[idSkill] == null) {
      ratingsSkill[idSkill] =
          RatingStoreModel.fromAllRatings(_listRating, isOver);
    } else {
      ratingsSkill[idSkill] = addToStars(
        ratings: ratingsSkill[idSkill]!,
        ratingsNew: _listRating,
        stars: stars,
        isOver: isOver,
      );
    }
  }

  RatingStoreModel addToStars({
    required RatingStoreModel ratings,
    required List<RatingModel> ratingsNew,
    required int stars,
    required bool isOver,
  }) {
    RatingStoreModel ratingStore = ratings;
    switch (stars) {
      case 1:
        List<RatingModel> tempRatings = [];
        tempRatings.addAll(ratings.one.ratings);
        tempRatings.addAll(ratingsNew);
        ratingStore.one =
            RatingManagementModel(ratings: tempRatings, isOver: isOver);
        break;
      case 2:
        List<RatingModel> tempRatings = [];
        tempRatings.addAll(ratings.two.ratings);
        tempRatings.addAll(ratingsNew);
        ratingStore.two =
            RatingManagementModel(ratings: tempRatings, isOver: isOver);
        break;
      case 3:
        List<RatingModel> tempRatings = [];
        tempRatings.addAll(ratings.three.ratings);
        tempRatings.addAll(ratingsNew);
        ratingStore.three =
            RatingManagementModel(ratings: tempRatings, isOver: isOver);
        break;
      case 4:
        List<RatingModel> tempRatings = [];
        tempRatings.addAll(ratings.four.ratings);
        tempRatings.addAll(ratingsNew);
        ratingStore.four =
            RatingManagementModel(ratings: tempRatings, isOver: isOver);
        break;
      case 5:
        List<RatingModel> tempRatings = [];
        tempRatings.addAll(ratings.five.ratings);
        tempRatings.addAll(ratingsNew);
        ratingStore.five =
            RatingManagementModel(ratings: tempRatings, isOver: isOver);
        break;
      default:
        List<RatingModel> tempRatings = [];
        tempRatings.addAll(ratings.joinRatings.ratings);
        tempRatings.addAll(ratingsNew);
        ratingStore = RatingStoreModel.fromAllRatings(tempRatings, isOver);
        break;
    }
    return ratingStore;
  }

  GettingRatingExpert get _gettingRatingExpert =>
      GettingRatingExpert(ratings: ratingsExpert);

  GetDoneRatingExpert get _getDoneRatingExpert =>
      GetDoneRatingExpert(ratings: ratingsExpert);

  GettingRatingSkill get _gettingRatingSkill =>
      GettingRatingSkill(ratings: ratingsSkill);

  GetDoneRatingSkill get _getDoneRatingSkill =>
      GetDoneRatingSkill(ratings: ratingsSkill);

  _cleanBloc() {
    ratingsExpert = {};
    ratingsSkill = {};
  }
}
