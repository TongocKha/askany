part of 'category_home_bloc.dart';

@immutable
abstract class CategoryHomeState {
  List get props => [[]];
}

class CategoryHomeInitial extends CategoryHomeState {}

class GettingCategoryHome extends CategoryHomeState {
  final List<CategoryModel> categorysHome;
  GettingCategoryHome({required this.categorysHome});
  List get props => [categorysHome];
}

class GetDoneCategoryHome extends CategoryHomeState {
  final List<CategoryModel> categorysHome;
  GetDoneCategoryHome({required this.categorysHome});
  List get props => [categorysHome];
}
