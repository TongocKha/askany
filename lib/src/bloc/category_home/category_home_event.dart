part of 'category_home_bloc.dart';

@immutable
abstract class CategoryHomeEvent {}

class OnCategoryHomeEvent extends CategoryHomeEvent {}

class GetCategoryHomeEvent extends CategoryHomeEvent {}

class CleanCategoryHomeEvent extends CategoryHomeEvent {}

class RefreshCategoryHomeEvent extends CategoryHomeEvent {}
