import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/data/remote_data_source/category_repository.dart';
import 'package:askany/src/models/category_model.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'category_home_event.dart';
part 'category_home_state.dart';

class CategoryHomeBloc extends Bloc<CategoryHomeEvent, CategoryHomeState> {
  CategoryHomeBloc() : super(CategoryHomeInitial());

  List<CategoryModel> categoryHomeList = [];
  bool isOver = false;
  @override
  Stream<CategoryHomeState> mapEventToState(CategoryHomeEvent event) async* {
    if (event is OnCategoryHomeEvent) {
      if (!isOver) {
        if (categoryHomeList.isEmpty) {
          yield CategoryHomeInitial();

          await _getListCategoryHome();
        }
      }
      yield _getDoneCategoryHome;
    }

    if (event is GetCategoryHomeEvent) {
      if (!isOver) {
        if (categoryHomeList.isEmpty) {
          yield CategoryHomeInitial();
        } else {
          yield _gettingCategoryHome;
        }
        await _getListCategoryHome();

        yield _getDoneCategoryHome;
      }
    }
    if (event is RefreshCategoryHomeEvent) {
      yield CategoryHomeInitial();
      _cleanBloc();
      await _getListCategoryHome();
      yield _getDoneCategoryHome;
    }

    if (event is CleanCategoryHomeEvent) {
      _cleanBloc();
      yield CategoryHomeInitial();
    }
  }

  //private

  GettingCategoryHome get _gettingCategoryHome =>
      GettingCategoryHome(categorysHome: categoryHomeList);

  GetDoneCategoryHome get _getDoneCategoryHome =>
      GetDoneCategoryHome(categorysHome: categoryHomeList);

  Future<void> _getListCategoryHome() async {
    List<CategoryModel> _categories = await CategoryRepository().getCategoryHome();
    if (_categories.length < LIMIT_API_5) {
      isOver = true;
    }
    categoryHomeList.addAll(_categories);
  }

  _cleanBloc() {
    categoryHomeList.clear();
    isOver = false;
  }
}
