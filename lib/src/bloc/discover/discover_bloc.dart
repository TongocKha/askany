import 'package:askany/src/configs/application.dart';
import 'package:askany/src/data/remote_data_source/discover_repository.dart';
import 'package:askany/src/models/discover_model.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

part 'discover_event.dart';
part 'discover_state.dart';

class DiscoverBloc extends Bloc<DiscoverEvent, DiscoverState> {
  DiscoverBloc() : super(DiscoverInitial());

  List<DiscoverConvertedModel> listItem = [];

  @override
  Stream<DiscoverState> mapEventToState(DiscoverEvent event) async* {
    // if (event is GetOriginDiscoverEvent) {
    //   yield _gettingDiscoverItems;
    //   await _getListDiscoverItems();
    //   yield _getDoneDiscoverItems;
    // }
    if (event is OnDiscoverEvent) {
      if (listItem.isEmpty) {
        yield DiscoverInitial();
        await _getListDiscoverItems();
      }
      yield _getDoneDiscoverItems;
    }

    if (event is CleanDiscoverEvent) {
      _cleanDiscoverBloc();
      yield DiscoverInitial();
    }
    if (event is RefreshDiscoverEvent) {
      yield DiscoverInitial();
      _cleanDiscoverBloc();
      await _getListDiscoverItems();
      yield _getDoneDiscoverItems;
    }
  }

  // States:

  // GettingDiscoverItems get _gettingDiscoverItems => GettingDiscoverItems();
  GetDoneDiscoverItems get _getDoneDiscoverItems => GetDoneDiscoverItems(
        listItem: listItem,
      );

  Future<void> _getListDiscoverItems() async {
    listItem = [];

    final DiscoverOriginModel? _originItem = await DiscoverRepository().getDiscoverItems();
    if (_originItem != null) {
      final int _minItemCount = [
        _originItem.titles.length,
        _originItem.descriptions.length,
        _originItem.contents.length,
        _originItem.images.length
      ].reduce((current, next) => current < next ? current : next);

      for (int i = 0; i < _minItemCount; i++) {
        final String _imageUrl = Application.baseUrl +
            '/${_originItem.images[i].src}' +
            '/${_originItem.images[i].name}';

        listItem.add(
          DiscoverConvertedModel(
            title: _originItem.titles[i],
            descriptions: _originItem.descriptions[i],
            content: _originItem.contents[i],
            item: '',
            // empty
            image: _imageUrl,
            date: _originItem.createdAt,
            duration: 1,
            // unknown value;
          ),
        );
      }
    } else {
      return;
    }
  }

  void _cleanDiscoverBloc() {
    listItem = [];
  }
}
