part of 'discover_bloc.dart';

@immutable
abstract class DiscoverState {
  /// - prop[0] => list discover Items;
  List get props => [];
}

class DiscoverInitial extends DiscoverState {}

class GettingDiscoverItems extends DiscoverState {}

class GetDoneDiscoverItems extends DiscoverState {
  final List<DiscoverConvertedModel> listItem;
  GetDoneDiscoverItems({
    required this.listItem,
  });
  @override
  List get props => [listItem];
}
