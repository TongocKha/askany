part of 'discover_bloc.dart';

@immutable
abstract class DiscoverEvent {}

class OnDiscoverEvent extends DiscoverEvent {}

// class GetOriginDiscoverEvent extends DiscoverEvent {}

class CleanDiscoverEvent extends DiscoverEvent {}

class RefreshDiscoverEvent extends DiscoverEvent {}
