part of 'message_images_bloc.dart';

@immutable
abstract class MessageImagesState {
  List<dynamic> get props => [];
}

class MessageImagesInitial extends MessageImagesState {
  @override
  List get props => [[], []];
}

class GettingMessageImages extends MessageImagesState {
  final List<MessagesImageModel> imagesMessage;
  final List<AvatarModel> images;
  GettingMessageImages({
    required this.images,
    required this.imagesMessage,
  });
  @override
  List get props => [imagesMessage, images];
}

class GetDoneMessageImages extends MessageImagesState {
  final List<MessagesImageModel> imagesMessages;
  final List<AvatarModel> images;

  GetDoneMessageImages({
    required this.images,
    required this.imagesMessages,
  });
  @override
  List get props => [imagesMessages, images];
}
