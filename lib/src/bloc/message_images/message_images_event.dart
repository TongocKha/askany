part of 'message_images_bloc.dart';

@immutable
abstract class MessageImagesEvent {}

class OnMessagesImageEvent extends MessageImagesEvent {
  final String conversationId;
  OnMessagesImageEvent({required this.conversationId});
}

class GetMessgesImagesEvent extends MessageImagesEvent {
  final String conversationId;
  GetMessgesImagesEvent({required this.conversationId});
}

class CleanMessagesImagesEvent extends MessageImagesEvent {}

class RefreshMessgesImagesEvent extends MessageImagesEvent {
  final Function handleFinished;
  RefreshMessgesImagesEvent({required this.handleFinished});
}
