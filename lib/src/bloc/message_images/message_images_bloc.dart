import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/data/remote_data_source/message_repository.dart';
import 'package:askany/src/models/avatar_model.dart';
import 'package:askany/src/models/message_images_model.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'message_images_event.dart';
part 'message_images_state.dart';

class MessageImagesBloc extends Bloc<MessageImagesEvent, MessageImagesState> {
  Map<String, Map<String, dynamic>> messagesMapImage = {};
  String conversationId = '';
  Map<String, Map<String, List<AvatarModel>>> imagesStore = {};
  MessageImagesBloc() : super(MessageImagesInitial());

  Stream<MessageImagesState> mapEventToState(MessageImagesEvent event) async* {
    if (event is OnMessagesImageEvent) {
      yield MessageImagesInitial();
      conversationId = event.conversationId;
      if (_listMessageImagesByConversation(conversationId: conversationId)
          .isEmpty) {
        await _getMessageImagesByConversation();
      }
      yield _getDoneMessageImage;
    }
    if (event is GetMessgesImagesEvent) {
      if (_getIsOverMessageImage(conversationId: conversationId)) {
        yield _gettingMessageImage;
        await _getMessageImagesByConversation();
        yield _getDoneMessageImage;
      }
    }
    if (event is CleanMessagesImagesEvent) {
      _cleanBloc();
      yield MessageImagesInitial();
    }
  }

  // Private
  GettingMessageImages get _gettingMessageImage => GettingMessageImages(
        images: _listImagesByConversation(conversationId: conversationId),
        imagesMessage:
            _listMessageImagesByConversation(conversationId: conversationId),
      );
  GetDoneMessageImages get _getDoneMessageImage => GetDoneMessageImages(
        images: _listImagesByConversation(conversationId: conversationId),
        imagesMessages:
            _listMessageImagesByConversation(conversationId: conversationId),
      );

  Future<void> _getMessageImagesByConversation() async {
    List<MessagesImageModel> _messagesImageStore =
        _listMessageImagesByConversation(conversationId: conversationId);
    bool _isOver = _getIsOverMessageImage(conversationId: conversationId);

    List<MessagesImageModel> _messagesImage = await MessageRepository()
        .getMessagesImages(
            id: conversationId, skip: _messagesImageStore.length);
    if (_messagesImageStore.length < LIMIT_API_15) {
      _isOver = true;
    }
    _messagesImage.forEach((item) {
      final List<AvatarModel> _imageList =
          _listImagesByConversation(conversationId: conversationId);
      final List<AvatarModel> _image = item.images;
      _imageList.addAll(_image);
      _setListImagesByConversation(
        conversationId: conversationId,
        images: _imageList,
      );
    });
    _messagesImageStore.addAll(_messagesImage);
    _setListMessageImagesByConversation(
      conversationId: conversationId,
      messagesImages: _messagesImageStore,
      isOver: _isOver,
    );
  }

  void _setListMessageImagesByConversation({
    required String conversationId,
    required List<MessagesImageModel> messagesImages,
    bool isOver = false,
  }) {
    messagesMapImage[conversationId] = {
      MESSAGE_FIELD: messagesImages,
      IS_OVER_FIELD: isOver,
    };
  }

  void _setListImagesByConversation({
    required String conversationId,
    required List<AvatarModel> images,
  }) {
    imagesStore[conversationId] = {
      'images': images,
    };
  }

  bool _getIsOverMessageImage({required String conversationId}) =>
      messagesMapImage[conversationId]?[IS_OVER_FIELD] ?? false;

  List<MessagesImageModel> _listMessageImagesByConversation(
          {required String conversationId}) =>
      messagesMapImage[conversationId]?[MESSAGE_FIELD] ?? [];

  List<AvatarModel> _listImagesByConversation(
          {required String conversationId}) =>
      imagesStore[conversationId]?['images'] ?? [];

  _cleanBloc() {
    messagesMapImage = {};
    imagesStore = {};
    conversationId = '';
  }
}
