part of 'skill_info_bloc.dart';

@immutable
abstract class SkillInfoEvent {}

class GetSkillInfoEvent extends SkillInfoEvent {
  final String skillId;
  GetSkillInfoEvent({required this.skillId});
}

class ClearSkillInfoEvent extends SkillInfoEvent {}
