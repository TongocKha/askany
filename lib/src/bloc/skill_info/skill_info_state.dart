part of 'skill_info_bloc.dart';

@immutable
abstract class SkillInfoState {
  List get props => [];
}

class SkillInfoInitial extends SkillInfoState {
  List get props => [];
}

class GetDoneInfoSkill extends SkillInfoState {
  final Map<String, SkillModel> skill;
  GetDoneInfoSkill({required this.skill});

  @override
  List get props => [skill];
}
