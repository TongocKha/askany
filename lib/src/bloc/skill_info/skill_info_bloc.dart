import 'package:askany/src/data/remote_data_source/skill_repository.dart';
import 'package:askany/src/models/skill_model.dart';
import 'package:askany/src/routes/app_navigator_observer.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'skill_info_event.dart';
part 'skill_info_state.dart';

class SkillInfoBloc extends Bloc<SkillInfoEvent, SkillInfoState> {
  SkillInfoBloc() : super(SkillInfoInitial());

  Map<String, SkillModel> skills = {};

  @override
  Stream<SkillInfoState> mapEventToState(SkillInfoEvent event) async* {
    if (event is GetSkillInfoEvent) {
      showDialogLoading();
      if (skills[event.skillId] == null) {
        await _getInfo(event.skillId);
      }
      AppNavigator.pop();
      if (AppNavigatorObserver.currentRouteName != Routes.DETAIL_SKILL_SEARCH) {
        AppNavigator.push(Routes.DETAIL_SKILL_SEARCH, arguments: {
          'skillModel': skills[event.skillId],
          'expertModel': skills[event.skillId]?.expertModel ?? null,
        });
      }

      yield _getDoneInfoSkill;
    }
    if (event is ClearSkillInfoEvent) {
      yield SkillInfoInitial();
    }
  }

  //private

  GetDoneInfoSkill get _getDoneInfoSkill => GetDoneInfoSkill(skill: skills);

  Future<void> _getInfo(String skillId) async {
    SkillModel? skillInfo =
        await SkillRepository().getInfoSkill(skillId: skillId);
    if (skillInfo != null) {
      skills[skillId] = skillInfo;
    }
  }
}
