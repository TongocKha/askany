part of 'request_info_bloc.dart';

@immutable
abstract class RequestInfoEvent {}

class GetRequestInfoEvent extends RequestInfoEvent {
  final String requestId;
  GetRequestInfoEvent({required this.requestId});
}

class ClearRequestInfoEvent extends RequestInfoEvent {}
