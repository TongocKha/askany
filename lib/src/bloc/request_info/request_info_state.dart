part of 'request_info_bloc.dart';

@immutable
abstract class RequestInfoState {
  List get props => [];
}

class RequestInfoInitial extends RequestInfoState {
  List get props => [];
}

class GetDoneInfoRequest extends RequestInfoState {
  final Map<String, RequestModel> request;
  GetDoneInfoRequest({required this.request});

  @override
  List get props => [request];
}
