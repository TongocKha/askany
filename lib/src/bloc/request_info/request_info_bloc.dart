import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/home/home_bloc.dart';
import 'package:askany/src/data/remote_data_source/request_repository.dart';
import 'package:askany/src/models/request_model.dart';
import 'package:askany/src/routes/app_navigator_observer.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'request_info_event.dart';
part 'request_info_state.dart';

class RequestInfoBloc extends Bloc<RequestInfoEvent, RequestInfoState> {
  RequestInfoBloc() : super(RequestInfoInitial());

  Map<String, RequestModel> requests = {};
  @override
  Stream<RequestInfoState> mapEventToState(RequestInfoEvent event) async* {
    if (event is GetRequestInfoEvent) {
      showDialogLoading();
      if (requests[event.requestId] == null) {
        await _getInfo(event.requestId);
      }
      AppNavigator.pop();
      AppBloc.homeBloc.add(OnChangeIndexEvent(index: 1));
      if (AppNavigatorObserver.currentRouteName != Routes.DETAILS_SPECIALIST) {
        AppNavigator.push(Routes.DETAILS_REQUEST, arguments: {
          'requestModel': requests[event.requestId],
        });
      }

      yield _getDoneInfoRequest;
    }
    if (event is ClearRequestInfoEvent) {
      yield RequestInfoInitial();
    }
  }

  //private

  GetDoneInfoRequest get _getDoneInfoRequest =>
      GetDoneInfoRequest(request: requests);

  Future<void> _getInfo(String requestId) async {
    RequestModel? requestModel =
        await RequestRepository().getInfoRequest(requestId: requestId);
    if (requestModel != null) {
      requests[requestId] = requestModel;
    }
  }
}
