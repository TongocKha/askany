part of 'recommend_skill_bloc.dart';

@immutable
abstract class RecommendSkillState {
  List get props => [];
}

class RecommendSkillInitial extends RecommendSkillState {}

class GettingSkillRecommend extends RecommendSkillState {
  final List<SkillModel> skills;
  GettingSkillRecommend({required this.skills});

  @override
  List get props => [skills];
}

class GetDoneSkillRecommend extends RecommendSkillState {
  final List<SkillModel> skills;
  GetDoneSkillRecommend({required this.skills});

  @override
  List get props => [skills];
}
