part of 'recommend_skill_bloc.dart';

@immutable
abstract class RecommendSkillEvent {}

class GetListSkillRecommendEvent extends RecommendSkillEvent {
  final String keyWord;
  GetListSkillRecommendEvent({required this.keyWord});
}

class GetListSkillRecommendFinishEvent extends RecommendSkillEvent {
  final String keyWord;
  GetListSkillRecommendFinishEvent({required this.keyWord});
}

class CleanRecommendSkillEvent extends RecommendSkillEvent {}
