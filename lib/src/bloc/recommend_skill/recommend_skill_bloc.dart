import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/data/remote_data_source/skill_repository.dart';
import 'package:askany/src/models/skill_model.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'recommend_skill_event.dart';
part 'recommend_skill_state.dart';

class RecommendSkillBloc
    extends Bloc<RecommendSkillEvent, RecommendSkillState> {
  RecommendSkillBloc() : super(RecommendSkillInitial());

  List<SkillModel> recommendSkills = [];
  bool isOverSkillRecommend = false;
  @override
  Stream<RecommendSkillState> mapEventToState(
      RecommendSkillEvent event) async* {
    if (event is GetListSkillRecommendEvent) {
      _cleanBloc();
      if (recommendSkills.isNotEmpty) {
        yield RecommendSkillInitial();
      } else {
        await _getListSkillRecommend(event.keyWord);
      }
      yield _getDoneSkillRecommend;
    }
    if (event is GetListSkillRecommendFinishEvent) {
      if (recommendSkills.isNotEmpty) {
        yield _gettingSkillRecommend;
      }

      if (!isOverSkillRecommend) {
        await _getListSkillRecommend(event.keyWord);
      }

      yield _getDoneSkillRecommend;
    }

    if (event is CleanRecommendSkillEvent) {
      _cleanBloc();
      yield _getDoneSkillRecommend;
    }
  }

  //private

  GettingSkillRecommend get _gettingSkillRecommend =>
      GettingSkillRecommend(skills: recommendSkills);

  GetDoneSkillRecommend get _getDoneSkillRecommend =>
      GetDoneSkillRecommend(skills: recommendSkills);

  Future<void> _getListSkillRecommend(String keyWord) async {
    List<SkillModel>? _skills = await SkillRepository().getListSkillRecommend(
      key: keyWord,
      skip: recommendSkills.length,
    );

    if (_skills!.length < LIMIT_API_15) {
      isOverSkillRecommend = true;
    }
    recommendSkills.addAll(_skills);
  }

  _cleanBloc() {
    isOverSkillRecommend = false;
    recommendSkills.clear();
  }
}
