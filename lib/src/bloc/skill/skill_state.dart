part of 'skill_bloc.dart';

@immutable
abstract class SkillState {
  List<dynamic> get props => [];
}

class SkillInitial extends SkillState {}

class GettingSkillFinished extends SkillState {
  final List<SkillModel> skills;
  GettingSkillFinished({required this.skills});

  @override
  List get props => [skills];
}

class GetDoneSkillFinished extends SkillState {
  final List<SkillModel> skills;
  GetDoneSkillFinished({required this.skills});

  @override
  List get props => [skills];
}

class SearchingSkill extends SkillState {
  final List<SkillModel> skills;
  final int totalResult;
  SearchingSkill({required this.skills, required this.totalResult});

  @override
  List get props => [skills, totalResult];
}

class SearchDoneSkill extends SkillState {
  final List<SkillModel> skills;
  final int totalResult;
  SearchDoneSkill({required this.skills, required this.totalResult});

  @override
  List get props => [skills, totalResult];
}
