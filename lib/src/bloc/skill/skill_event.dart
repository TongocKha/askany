part of 'skill_bloc.dart';

@immutable
abstract class SkillEvent {}

class GetSkillEvent extends SkillEvent {
  final String expertId;
  GetSkillEvent({required this.expertId});
}

class GetPositionsEvent extends SkillEvent {}

class DeleteSkillEvent extends SkillEvent {
  final String id;
  DeleteSkillEvent({required this.id});
}

class GetSkillFinishedEvent extends SkillEvent {
  final String expertId;
  GetSkillFinishedEvent({required this.expertId});
}

class AddSkillEvent extends SkillEvent {
  final SkillModel skill;
  final Function? handleFinished;
  AddSkillEvent({required this.skill, this.handleFinished});
}

class UpdateSkillEvent extends SkillEvent {
  final SkillModel skill;
  UpdateSkillEvent({required this.skill});
}

class CleanSkillEvent extends SkillEvent {}

class RefreshSkillEvent extends SkillEvent {
  final Function handleFinished;
  RefreshSkillEvent({required this.handleFinished});
}

class RefreshSkillSearchEvent extends SkillEvent {
  final Function handleFinished;
  RefreshSkillSearchEvent({required this.handleFinished});
}

class SearchSkillEvent extends SkillEvent {
  final SearchModel searchModel;
  SearchSkillEvent({required this.searchModel});
}

class GetMoreSearchEvent extends SkillEvent {}
