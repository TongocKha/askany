import 'dart:ui';

import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/data/remote_data_source/skill_repository.dart';
import 'package:askany/src/models/position_model.dart';
import 'package:askany/src/models/search_model.dart';
import 'package:askany/src/models/skill_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_with_text_and_pop_button.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:askany/src/ui/style/calendar_style.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'skill_event.dart';

part 'skill_state.dart';

class SkillBloc extends Bloc<SkillEvent, SkillState> {
  SkillBloc() : super(SkillInitial());

  List<SkillModel> skills = [];
  List<SkillModel> searchSkills = [];

  List<PositionModel> positions = [];
  bool isOverSkills = false;
  bool isOverSearchSkills = false;

  int totalSkillSearch = 0;
  SearchModel? searchSkill;

  Map<String, ResultSearchLocalModel> resultSearch = {};

  String idExpert = '';

  @override
  Stream<SkillState> mapEventToState(SkillEvent event) async* {
    if (event is RefreshSkillEvent) {
      await _refreshSkill(event);
      yield _getDoneSkills;
    }

    if (event is RefreshSkillSearchEvent) {
      await _refreshSearchSkill(event);
      yield _searchDoneSkills;
    }

    if (event is GetSkillEvent) {
      yield SkillInitial();
      if (skills.isEmpty) {
        await _getListSkills(event.expertId);
      }
      yield _getDoneSkills;
    }

    if (event is GetSkillFinishedEvent) {
      if (skills.isEmpty) {
        yield SkillInitial();
      } else {
        yield _gettingSkills;
      }

      if (!isOverSkills) {
        await _getListSkills(event.expertId);
      }

      yield _getDoneSkills;
    }

    if (event is AddSkillEvent) {
      bool isCreateSucceed = await _createSkill(event);

      if (isCreateSucceed) {
        if (event.handleFinished != null) {
          event.handleFinished!();
        } else {
          AppNavigator.pop();
        }
        yield _getDoneSkills;
      }
    }

    if (event is DeleteSkillEvent) {
      if (skills.length > 1) {
        bool isCreateSucceed = await _deleteSkill(event.id);

        if (isCreateSucceed) {
          AppNavigator.pop();
          yield _getDoneSkills;
        }
      } else {
        AppNavigator.pop();
        AppNavigator.pop();
        dialogAnimationWrapper(
          child: DialogWithTextAndPopButton(
            bodyColor: colorBlack1,
            bodyAlign: TextAlign.center,
            bodyAfter: Strings.cannotDeteteThisSkillNoti.i18n,
          ),
        );
      }
    }

    if (event is UpdateSkillEvent) {
      bool isUpdateSucceed = await _updateSkill(event);
      if (isUpdateSucceed) {
        AppNavigator.pop();
        yield _getDoneSkills;
      }
    }

    if (event is GetPositionsEvent) {
      if (positions.isEmpty) {
        await _getPositions();
      }
    }

    if (event is CleanSkillEvent) {
      _cleanBloc();
      yield _getDoneSkills;
    }

    if (event is SearchSkillEvent) {
      yield SkillInitial();

      searchSkill = event.searchModel;
      searchSkills.clear();

      if (resultSearch[event.searchModel.query] != null) {
        isOverSearchSkills = resultSearch[event.searchModel.query]!.isOver;
        totalSkillSearch = resultSearch[event.searchModel.query]!.totalResult;
        searchSkills = resultSearch[event.searchModel.query]!
            .result
            .map((skill) => SkillModel.fromMapSearch(skill))
            .toList();
      } else {
        isOverSearchSkills = false;
        totalSkillSearch = 0;

        await _searchSkill(searchSkill!);
      }

      yield _searchDoneSkills;
    }

    if (event is GetMoreSearchEvent) {
      if (!isOverSearchSkills) {
        yield _searchingSkills;
        await _searchSkill(searchSkill!);
        yield _searchDoneSkills;
      }
    }
  }

  // Private methods

  Future<void> _getListSkills(String expertId) async {
    idExpert = expertId;
    List<SkillModel> _skills = await SkillRepository().getListSkill(
      skip: skills.length,
      expertId: expertId,
    );
    if (_skills.isEmpty) {
      isOverSkills = true;
    } else {
      skills.addAll(_skills);
    }
  }

  Future<void> _refreshSkill(RefreshSkillEvent event) async {
    skills.clear();
    isOverSkills = false;
    await _getListSkills(idExpert);
    event.handleFinished();
  }

  Future<void> _searchSkill(SearchModel searchModel) async {
    List _skills = await SkillRepository().searchSkill(
      skip: searchSkills.length,
      searchModel: searchModel,
    );

    if (_skills[0].length < LIMIT_API_5) {
      isOverSearchSkills = true;
    }

    searchSkills.addAll((_skills[0] as List).cast());
    totalSkillSearch = _skills[1];

    resultSearch[searchModel.query] = ResultSearchLocalModel(
      isOver: isOverSearchSkills,
      totalResult: totalSkillSearch,
      result: searchSkills.map((skill) => skill.toMapSearch()).toList(),
    );
  }

  Future<void> _refreshSearchSkill(RefreshSkillSearchEvent event) async {
    searchSkills.clear();
    isOverSearchSkills = false;
    await _searchSkill(searchSkill!);
    event.handleFinished();
  }

  Future<bool> _createSkill(AddSkillEvent event) async {
    SkillModel? _skill =
        await SkillRepository().createSkill(skill: event.skill);
    if (_skill != null) {
      _skill.highestPosition = event.skill.highestPosition;
      _skill.specialty = event.skill.specialty;
      _skill.callPrice = event.skill.callPrice;
      _skill.meetPrice = event.skill.meetPrice;
      skills.insert(0, _skill);
    }
    AppNavigator.pop();
    return _skill != null;
  }

  Future<bool> _updateSkill(UpdateSkillEvent event) async {
    SkillModel? _skill =
        await SkillRepository().updateSkill(skill: event.skill);
    AppNavigator.pop();

    if (_skill != null) {
      _skill.highestPosition = event.skill.highestPosition;
      _skill.specialty = event.skill.specialty;
      _skill.callPrice = event.skill.callPrice;
      _skill.meetPrice = event.skill.meetPrice;

      int indexOfSkill =
          skills.indexWhere((skill) => skill.id == event.skill.id);
      if (indexOfSkill != -1) {
        skills[indexOfSkill] = _skill;
      }
    }
    return _skill != null;
  }

  Future<bool> _deleteSkill(String id) async {
    bool _isDeleteSucceed = await SkillRepository().deleteSkill(id: id);
    AppNavigator.pop();

    if (_isDeleteSucceed) {
      int indexOfSkill = skills.indexWhere((skill) => skill.id == id);
      if (indexOfSkill != -1) {
        skills.removeAt(indexOfSkill);
      }
    }
    return _isDeleteSucceed;
  }

  Future<void> _getPositions() async {
    List<PositionModel> _positions = await SkillRepository().getPositions();
    positions.addAll(_positions);
  }

  GettingSkillFinished get _gettingSkills =>
      GettingSkillFinished(skills: skills);

  GetDoneSkillFinished get _getDoneSkills =>
      GetDoneSkillFinished(skills: skills);

  SearchingSkill get _searchingSkills => SearchingSkill(
        skills: searchSkills,
        totalResult: totalSkillSearch,
      );
  SearchDoneSkill get _searchDoneSkills => SearchDoneSkill(
        skills: searchSkills,
        totalResult: totalSkillSearch,
      );

  _cleanBloc() {
    skills = [];
    isOverSkills = false;
    searchSkills.clear();
    isOverSearchSkills = false;
    searchSkill = null;
  }
}
