import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/calendar/calendar_bloc.dart';
import 'package:askany/src/bloc/message/message_bloc.dart';
import 'package:askany/src/bloc/notification/notification_bloc.dart';
import 'package:askany/src/bloc/user/user_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/data/remote_data_source/request_repository.dart';
import 'package:askany/src/data/remote_data_source/service_managament_repository.dart';
import 'package:askany/src/models/cancel_service_model.dart';
import 'package:askany/src/models/cancel_service_reason_model.dart';
import 'package:askany/src/models/message_offer_model.dart';
import 'package:askany/src/models/offer_model.dart';
import 'package:askany/src/models/report_model.dart';
import 'package:askany/src/models/request_model.dart';
import 'package:askany/src/models/slide_mode.dart';
import 'package:askany/src/models/service_feedback_model.dart';
import 'package:askany/src/routes/app_navigator_observer.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/services/in_app/in_app_review.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_with_text_and_next_screen.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_with_text_and_pop_button.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
part 'service_management_event.dart';
part 'service_management_state.dart';

class CachedServiceBloc {
  bool isOver;
  List<RequestModel> services;
  CachedServiceBloc({
    this.isOver = false,
    required this.services,
  });
}

class ServiceManagamentBloc
    extends Bloc<ServiceManagamentEvent, ServiceManagamentState> {
  ServiceManagamentBloc() : super(ServiceManagamentInitial());

  Map<int, CachedServiceBloc> services = {
    SERVICE_CONFIRMING: CachedServiceBloc(
      services: [],
    ), //chờ xác nhận, đã nhận offer. Offer.status = [2,3]
    SERVICE_CONFIRMED: CachedServiceBloc(
      services: [],
    ), //đã xác nhận - expert đã xác nhận, Offer.status = 4;
    SERVICE_ADVISED: CachedServiceBloc(
      services: [],
    ), //đã tư vấn.
    SERVICE_COMPLETED: CachedServiceBloc(
      services: [],
    ), //đã hoàn thành - Offer status = 4;
    SERVICE_CANCELED: CachedServiceBloc(
      services: [],
    ), //đã huỷ - Offer status = -2;
    SERVICE_COMPLAINED: CachedServiceBloc(
      services: [],
    ), //khiếu nại - Offer status = -2;
  };

  List<ReasonModel> reasons = [];
  List<RequestModel> backUpData = [];

  int currentCustomiseStatus = -100;

  @override
  Stream<ServiceManagamentState> mapEventToState(
      ServiceManagamentEvent event) async* {
    if (event is OnChangeStatusEvent) {
      yield ServiceManagamentInitial();
    }

    if (event is OnServiceEvent) {
      _cleanData();
      _changeCurrentStatus(event);
      if (_getListByStatus.isEmpty && !_getIsOverByStatus) {
        yield ServiceManagamentInitial();
        await _getServiceByStatus();
      }
      _addAllItemToBackupList();

      yield _getDoneService;
    }

    if (event is GetServiceEvent) {
      if (!_getIsOverByStatus) {
        if (_getListByStatus.isEmpty) {
          yield ServiceManagamentInitial();
        } else {
          yield _gettingService;
        }
        await _getServiceByStatus();
      }
      _addAllItemToBackupList();

      yield _getDoneService;
    }

    if (event is RefreshServiceEvent) {
      _cleanBloc();
      await _getServiceByStatus();
      event.handleFinished();
      _addAllItemToBackupList();
      yield _getDoneService;
    }

    if (event is CleanServiceManagamentEvent) {
      _cleanBloc();
      yield ServiceManagamentInitial();
    }

    if (event is GetCancelServiceReasonsEvent) {
      _cleanReasons();

      await _getCancelServiceReasons();

      yield _getDoneService;
    }

    if (event is CancelOfferEvent) {
      final bool _cancelStatus = await _cancelOffer(event);
      dialogAnimationWrapper(
        slideFrom: SlideMode.bot,
        child: _cancelStatus
            ? DialogWithTextAndNextScreen(
                title: Strings.cancelServiceSuccessNoti.i18n,
                bodyAfter: Strings.refundIn24Hours.i18n,
                buttonText: Strings.confirm.i18n,
                onPressed: () {
                  if (AppNavigatorObserver.routeNames
                          .where((route) => route == Routes.MY_SERVICE)
                          .length ==
                      1) {
                    AppNavigator.popUntil(Routes.MY_SERVICE);
                  } else {
                    AppNavigator.pop();
                    AppNavigator.pop();
                    AppNavigator.pop();
                  }

                  _findServiceNotPaymentYet();
                },
              )
            : DialogWithTextAndPopButton(
                title: Strings.errorOccurredNoti.i18n,
                bodyAfter: Strings.errorOccurredTryAgain.i18n,
              ),
      );

      yield _getDoneService;
    }

    if (event is ExpertConfirmOfferEvent) {
      final bool _confirmStatus = await _expertConfirmOffer(event);

      if (_confirmStatus) {
        if (AppNavigatorObserver.routeNames
                .where((route) => route == Routes.MY_SERVICE)
                .length ==
            1) {
          AppNavigator.popUntil(Routes.MY_SERVICE);
        } else {
          AppNavigator.pop();
          AppNavigator.pop();
        }
      } else {
        dialogAnimationWrapper(
          slideFrom: SlideMode.bot,
          child: DialogWithTextAndPopButton(
            title: Strings.errorOccurredNoti.i18n,
            bodyAfter: Strings.errorOccurredTryAgain.i18n,
          ),
        );
      }

      yield _getDoneService;
    }

    if (event is UserCompleteOfferEvent) {
      final bool _completeStatus = await _userCompleteOffer(event);
      if (_completeStatus) {
        AppNavigator.pop();
        AppNavigator.push(Routes.SERVICE_FEEDBACK,
            arguments: {'service': event.service});
      } else {
        dialogAnimationWrapper(
          slideFrom: SlideMode.bot,
          child: DialogWithTextAndPopButton(
            title: Strings.errorOccurredNoti.i18n,
            bodyAfter: Strings.errorOccurredTryAgain.i18n,
          ),
        );
      }
      yield _getDoneService;
    }
    if (event is UserRatingAndFeedBackEvent) {
      final bool isReviewSucceed = await _userRatingAndFeedback(event);

      if (!isReviewSucceed) {
        dialogAnimationWrapper(
          slideFrom: SlideMode.bot,
          child: DialogWithTextAndPopButton(
            title: Strings.errorOccurredNoti.i18n,
            bodyAfter: Strings.errorOccurredTryAgain.i18n,
          ),
        );
      } else {
        AppNavigator.pop();
      }

      yield _getDoneService;
    }

    if (event is ReportOfferEvent) {
      final bool isReportedSucess = await _reportOffer(event);

      dialogAnimationWrapper(
        slideFrom: SlideMode.bot,
        child: isReportedSucess
            ? DialogWithTextAndNextScreen(
                title: Strings.complainSuccessNoti.i18n,
                bodyAfter: Strings.verifyComplaintIn7Days.i18n,
                buttonText: Strings.confirm.i18n,
                onPressed: () {
                  AppNavigator.popUntil(
                    Routes.DETAILS_SERVICE,
                  );
                })
            : DialogWithTextAndPopButton(
                title: Strings.errorOccurredNoti.i18n,
                bodyAfter: Strings.errorOccurredTryAgain.i18n,
              ),
      );
      yield _getDoneService;
    }

    if (event is ReplyFeedbackEvent) {
      showDialogLoading();
      yield _gettingService;

      final bool isReplySucess = await _expertReplyFeedback(event);
      if (isReplySucess) {
        yield _getDoneService;
        AppNavigator.pop();
      } else {
        AppNavigator.pop();
        dialogAnimationWrapper(
          slideFrom: SlideMode.bot,
          child: DialogWithTextAndPopButton(
            title: Strings.errorOccurredNoti.i18n,
            bodyAfter: Strings.errorOccurredTryAgain.i18n,
          ),
        );
      }
    }

    if (event is ContinuePaymentStep1Event) {
      bool isUpdateSucceed = await _userDoneStep1(event);
      if (isUpdateSucceed) {
        yield _getDoneService;
      }
    }

    if (event is ContinuePaymentStep2Event) {
      bool isUpdateSucceed = await _userDoneStep2(event);
      if (isUpdateSucceed) {
        yield _getDoneService;
      }
    }

    if (event is ContinuePaymentStep3Event) {
      final bool _userPayOfferSuccess = await _userPayOffer(event);
      if (_userPayOfferSuccess) {
        if (event.paymentMethod == PaymentMethod.byAppWallet) {
          AppBloc.userBloc.add(
            UpdateUserWalletEvent(updatedWallet: event.updatedWallet),
          );
        }
        yield _getDoneService;
      }
    }
    if (event is AddNewServiceToListEvent) {
      yield _gettingService;
      _addNewServiceToList(event);

      yield _getDoneService;
    }
    if (event is UpdateOfferStatusAfterPaymentEvent) {
      _updateOfferStatusAfterPayment(event);
      yield _getDoneService;
    }
  }

  // MARK: Private methods

  GettingService get _gettingService => GettingService(
        services: _getResultForState,
        reasons: reasons,
        backUpData: backUpData,
      );
  GetDoneService get _getDoneService => GetDoneService(
        services: _getResultForState,
        reasons: reasons,
        backUpData: backUpData,
      );

  bool get _getIsOverByStatus =>
      services[currentCustomiseStatus]?.isOver ?? false;
  List<RequestModel> get _getListByStatus =>
      _getResultForState[currentCustomiseStatus] ?? [];

  Map<int, List<RequestModel>> get _getResultForState {
    List<int> keys = services.keys.toList();
    List<CachedServiceBloc> values = services.values.toList();
    Map<int, List<RequestModel>> result = {};
    keys.asMap().forEach((index, key) {
      result.addAll({key: values[index].services});
    });
    return result;
  }

  void setListServiceByStatus(
      {required int tabsNumber, required List<RequestModel> servicesParam}) {
    services[tabsNumber]?.services = servicesParam;
  }

  void _changeCurrentStatus(OnServiceEvent event) {
    if (event.status != currentCustomiseStatus) {
      currentCustomiseStatus = event.status;
    }
  }

  Future<void> _getServiceByStatus() async {
    List<RequestModel> _currentServices = _getListByStatus;
    List<RequestModel> _services =
        await ServiceManagamentRepository().getListRequestStatus(
      status: currentCustomiseStatus,
      skip: _currentServices.length,
    );

    if (_services.length < LIMIT_API_5) {
      services[currentCustomiseStatus]?.isOver = true;
    }

    _currentServices.addAll(_services);

    setListServiceByStatus(
      tabsNumber: currentCustomiseStatus,
      servicesParam: _currentServices,
    );
  }

  Future<void> _getCancelServiceReasons() async {
    List<ReasonModel?> _reasons =
        await ServiceManagamentRepository().getCancelReasons();
    for (int i = 0; i < _reasons.length; i++) {
      if (_reasons[i] != null) {
        reasons.add(_reasons[i]!);
      }
    }
  }

  Future<bool> _cancelOffer(
    CancelOfferEvent event,
  ) async {
    String? _conversationId = await ServiceManagamentRepository().cancelOffer(
      offerId: event.offerId,
      reason: event.reason,
      content: event.content,
    );
    AppNavigator.pop();
    if (_conversationId != null) {
      AppBloc.calendarBloc.add(RemoveServiceEvent(
        requestModel: event.service,
      ));
      final List<RequestModel> confirmingOffers =
          services[event.tabNumber]?.services ?? [];
      final List<RequestModel> canceledOffers =
          services[SERVICE_CANCELED]?.services ?? [];
      int indexOfService = confirmingOffers.indexWhere(
        (offer) => offer.myOfferId == event.offerId,
      );
      if (indexOfService != -1) {
        final RequestModel currentService = confirmingOffers[indexOfService];

        confirmingOffers.remove(currentService);

        int indexOfOffer = currentService.selectedOffers.indexWhere(
          (offer) => offer.id == event.offerId,
        );

        if (indexOfOffer != -1) {
          currentService.selectedOffers[indexOfOffer].status = OFFER_CANCELED;

          // Create & send notification
          AppBloc.notificationBloc.add(
            CreateNotificationEvent(
              messageType: CANCEL_SERVICE,
              userId: UserLocal().getIsExpert()
                  ? (currentService.authorUser?.id ?? '')
                  : (currentService.myOffer?.authorExpert.id ?? ''),
              requestId: currentService.id,
              offerId: currentService.myOfferId,
            ),
          );

          // Insert automatic message
          AppBloc.messageBloc.add(
            InsertAutomaticMessageEvent(
              message: MessageOfferModel.fromRequestModel(
                currentService,
                currentService.selectedOffers[indexOfOffer],
              ),
              conversationId: _conversationId,
            ),
          );
          AppBloc.calendarBloc.add(
            RemoveServiceEvent(requestModel: event.service),
          );
        }

        currentService.cancel = CancelModel(
          id: event.offerId,
          reason: Reason(
            id: '',
            vi: Vi(name: ''),
          ),
          content: event.content,
        );

        services[event.tabNumber]?.services = confirmingOffers;

        if (canceledOffers.isNotEmpty) {
          canceledOffers.insert(0, currentService);
          services[SERVICE_CANCELED]?.services = canceledOffers;
        } else {
          services[SERVICE_CANCELED]?.isOver = false;
        }
      }
    }
    return _conversationId != null;
  }

  Future<bool> _expertConfirmOffer(ExpertConfirmOfferEvent event) async {
    bool _result = await ServiceManagamentRepository()
        .expertConfirmOffer(offerId: event.offerId);
    AppNavigator.pop();
    if (_result) {
      final List<RequestModel> confirmingOffers =
          services[SERVICE_CONFIRMING]?.services ?? [];
      final List<RequestModel> confirmedOffers =
          services[SERVICE_CONFIRMED]?.services ?? [];
      int indexOfService = confirmingOffers
          .indexWhere((request) => request.id == event.requestId);
      if (indexOfService != -1) {
        final RequestModel currentService = confirmingOffers[indexOfService];
        AppBloc.notificationBloc.add(
          CreateNotificationEvent(
            messageType: CONFIRM_ADVISE,
            userId: currentService.authorUser?.id ?? '',
            requestId: currentService.id,
            offerId: currentService.myOfferId,
          ),
        );

        confirmingOffers.remove(currentService);
        int indexOfOffer = currentService.selectedOffers.indexWhere(
          (offer) => offer.id == event.offerId,
        );
        if (indexOfOffer != -1) {
          currentService.selectedOffers[indexOfOffer].status = OFFER_CONFIRMED;
        }
        services[SERVICE_CONFIRMING]?.services = confirmingOffers;

        if (confirmedOffers.isNotEmpty) {
          confirmedOffers.insert(0, currentService);
          services[SERVICE_CONFIRMED]?.services = confirmedOffers;
        } else {
          services[SERVICE_CANCELED]?.isOver = false;
        }
      }
      InAppReviewAskany.reviewApp();
    }

    return _result;
  }

  Future<bool> _userCompleteOffer(UserCompleteOfferEvent event) async {
    bool _result = await ServiceManagamentRepository()
        .userCompleteOffer(offerId: event.service.myOfferId.toString());
    AppNavigator.pop();
    if (_result) {
      AppBloc.calendarBloc.add(RemoveServiceEvent(
        requestModel: event.service,
      ));
      final List<RequestModel> advisedOffers =
          services[SERVICE_ADVISED]?.services ?? [];
      final List<RequestModel> completedOffers =
          services[SERVICE_COMPLETED]?.services ?? [];
      int indexOfService = advisedOffers.indexWhere(
        (offer) => offer.myOfferId == event.service.myOfferId,
      );
      if (indexOfService != -1) {
        final RequestModel currentService = advisedOffers[indexOfService];

        advisedOffers.remove(currentService);

        int indexOfOffer = currentService.selectedOffers.indexWhere(
          (offer) => offer.id == event.service.myOfferId,
        );

        if (indexOfOffer != -1) {
          currentService.selectedOffers[indexOfOffer].status = OFFER_COMPLETED;
        }

        services[SERVICE_ADVISED]?.services = advisedOffers;

        if (completedOffers.isNotEmpty) {
          completedOffers.insert(0, currentService);
          services[SERVICE_COMPLETED]?.services = completedOffers;
        } else {
          services[SERVICE_CANCELED]?.isOver = false;
        }
      }
      InAppReviewAskany.reviewApp();
    }

    return _result;
  }

  Future<bool> _userRatingAndFeedback(UserRatingAndFeedBackEvent event) async {
    bool _result = await ServiceManagamentRepository().userRatingAndFeedback(
      offerId: event.service.myOfferId.toString(),
      serviceFeedbackModel: event.serviceFeedbackModel,
    );
    AppNavigator.pop();

    if (_result) {
      List<RequestModel> _currentServices =
          services[SERVICE_COMPLETED]?.services ?? [];
      int indexOfRequest = _currentServices
          .indexWhere((request) => request.id == event.service.id);

      if (indexOfRequest != -1) {
        _currentServices[indexOfRequest].rating = event.serviceFeedbackModel;
        AppBloc.notificationBloc.add(
          CreateNotificationEvent(
            messageType: USER_RATE_REQUEST,
            userId:
                _currentServices[indexOfRequest].myOffer?.authorExpert.id ?? '',
            requestId: _currentServices[indexOfRequest].id,
            offerId: _currentServices[indexOfRequest].myOfferId,
          ),
        );
      }
    }

    return _result;
  }

  Future<bool> _reportOffer(ReportOfferEvent event) async {
    ReportModel? _result = await ServiceManagamentRepository()
        .reportOffer(offerId: event.offerId, content: event.reportContent);

    if (_result != null) {
      AppBloc.calendarBloc.add(RemoveServiceEvent(
        requestModel: event.service,
      ));
      List<RequestModel> _advisedServices =
          services[SERVICE_ADVISED]?.services ?? [];

      List<RequestModel> _reportedServices =
          services[SERVICE_COMPLAINED]?.services ?? [];

      final int _indexOfService = _advisedServices
          .indexWhere((service) => service.id == event.service.id);
      if (_indexOfService != -1) {
        RequestModel _currentService = _advisedServices[_indexOfService];

        AppBloc.notificationBloc.add(
          CreateNotificationEvent(
            messageType: USER_COMPLAIN_REQUEST,
            userId: _currentService.myOffer?.authorExpert.id ?? '',
            requestId: _currentService.id,
            offerId: _currentService.myOfferId,
          ),
        );

        _advisedServices.remove(_advisedServices[_indexOfService]);
        services[SERVICE_ADVISED]?.services = _advisedServices;
        _currentService.report = _result;
        final int _indexOfOffer = _currentService.selectedOffers
            .indexWhere((offer) => offer.id == _currentService.myOffer?.id);
        if (_indexOfOffer != -1) {
          _currentService.selectedOffers[_indexOfOffer].status = REPORTED;
          if (_reportedServices.isEmpty) {
            services[SERVICE_COMPLAINED]?.isOver = false;
          }

          _reportedServices.insert(0, _currentService);
          services[SERVICE_COMPLAINED]?.services = _reportedServices;

          return true;
        }
      }
    }

    return _result != null;
  }

  Future<bool> _expertReplyFeedback(ReplyFeedbackEvent event) async {
    bool _result = await ServiceManagamentRepository().expertReplyFeedback(
      ratingId: event.ratingId,
      content: event.content,
    );
    AppNavigator.pop();
    if (_result) {
      List<RequestModel> _currentServices =
          services[SERVICE_COMPLETED]?.services ?? [];
      int indexOfRequest = _currentServices.indexWhere(
        (request) => request.rating?.id == event.ratingId,
      );

      if (indexOfRequest != -1) {
        final Reply _reply = Reply(
          id: '',
          expert: AppBloc.userBloc.getAccount.id,
          parent: event.ratingId,
          content: event.content,
          createdAt: DateTime.now(),
        );
        _currentServices[indexOfRequest].rating!.reply = _reply;
      }
    }
    return _result;
  }

  _findServiceNotPaymentYet() {
    List<RequestModel> _services = services[SERVICE_CONFIRMING]?.services ?? [];
    int indexOfNotPaymentYet = _services.indexWhere(
      (service) => (service.myOffer?.status ?? OFFER_ACTIVE) < OFFER_PAID,
    );

    if (indexOfNotPaymentYet != -1) {
      dialogAnimationWrapper(
        slideFrom: SlideMode.bot,
        child: DialogWithTextAndPopButton(
          title: Strings.unpaidServicesNoti.i18n,
          bodyBefore:
              '${Strings.unpaidSercicesDetailsP1.i18n} “${_services[indexOfNotPaymentYet].title}” ${Strings.unpaidSercicesDetailsP2.i18n}',
          bodyFontSize: 12.sp,
          bodyColor: colorBlack1,
        ),
      );
    }
  }

  Future<bool> _userDoneStep1(ContinuePaymentStep1Event event) async {
    final _updateInfoResult = await RequestRepository().updateUserInfo(
      offerId: event.offer.id,
      authorName: event.authorName,
      authorPhone: event.authorPhone,
      note: event.note,
      locationName: event.request.locationName,
      locationAddress: event.request.locationAddress,
    );

    AppNavigator.pop();

    if (_updateInfoResult) {
      int indexOfRequest =
          (services[SERVICE_CONFIRMING]?.services ?? []).indexWhere(
        (request) => request.id == event.request.id,
      );

      if (indexOfRequest != -1) {
        int indexOfOffer = services[SERVICE_CONFIRMING]!
            .services[indexOfRequest]
            .selectedOffers
            .indexWhere(
              (offer) => offer.id == event.offer.id,
            );

        if (indexOfOffer != -1) {
          services[SERVICE_CONFIRMING]!
              .services[indexOfRequest]
              .selectedOffers[indexOfOffer]
              .authorName = event.authorName;
          services[SERVICE_CONFIRMING]!
              .services[indexOfRequest]
              .selectedOffers[indexOfOffer]
              .authorPhone = event.authorPhone;
          services[SERVICE_CONFIRMING]!
              .services[indexOfRequest]
              .selectedOffers[indexOfOffer]
              .note = event.note;
        }
      }

      event.handleFinished();
    }

    return _updateInfoResult;
  }

  Future<bool> _userDoneStep2(ContinuePaymentStep2Event event) async {
    final _updateInfoResult = await RequestRepository().updateTimeline(
      offerId: event.offer.id,
      startTime: event.startTime,
      endTime: event.endTime,
    );

    AppNavigator.pop();

    if (_updateInfoResult) {
      int indexOfRequest =
          (services[SERVICE_CONFIRMING]?.services ?? []).indexWhere(
        (request) => request.id == event.request.id,
      );

      if (indexOfRequest != -1) {
        int indexOfOffer = services[SERVICE_CONFIRMING]!
            .services[indexOfRequest]
            .selectedOffers
            .indexWhere(
              (offer) => offer.id == event.offer.id,
            );

        if (indexOfOffer != -1) {
          services[SERVICE_CONFIRMING]!
              .services[indexOfRequest]
              .selectedOffers[indexOfOffer]
              .startTime = event.startTime;
          services[SERVICE_CONFIRMING]!
              .services[indexOfRequest]
              .selectedOffers[indexOfOffer]
              .endTime = event.endTime;
          services[SERVICE_CONFIRMING]!
              .services[indexOfRequest]
              .selectedOffers[indexOfOffer]
              .isPickTime = true;
        }
      }
      event.handleFinished();
    } else {
      // Show dialog update failure!
    }

    return _updateInfoResult;
  }

  Future<bool> _userPayOffer(ContinuePaymentStep3Event event) async {
    bool? _payOfferResult;
    if (event.paymentMethod == PaymentMethod.byAppWallet) {
      _payOfferResult = await RequestRepository().payByPersonalWallet(
        amount: event.amount,
        offerId: event.offerId,
        expertId: event.expertId,
        requestId: event.request.id,
      );
      AppNavigator.pop();
    } else if (event.paymentMethod == PaymentMethod.byVNPay) {
      final vnpayUrl = await RequestRepository().payByVNPay(
        amount: event.amount,
        offerId: event.offerId,
        requestId: event.request.id!,
      );
      AppNavigator.pop();
      AppNavigator.push(
        Routes.WEB_VIEW_PAYMENT,
        arguments: {
          'url': vnpayUrl,
          'onPaymentDone': (value) {
            _payOfferResult = value;
            _handleAfterPayment(event: event, value: value);
          },
        },
      );
      return false;
    }

    _handleAfterPayment(event: event, value: _payOfferResult);

    return _payOfferResult!;
  }

  void _handleAfterPayment(
      {required ContinuePaymentStep3Event event, required bool? value}) {
    bool? _payOfferResult = value;
    if (_payOfferResult == null) {
      return;
    }

    if (_payOfferResult) {
      final indexOfRequest =
          (services[SERVICE_CONFIRMING]?.services ?? []).indexWhere(
        (request) => request.id == event.request.id,
      );

      if (indexOfRequest != -1) {
        int indexOfOffer = services[SERVICE_CONFIRMING]!
            .services[indexOfRequest]
            .selectedOffers
            .indexWhere(
              (offer) => offer.id == event.offerId,
            );
        if (indexOfOffer != -1) {
          services[SERVICE_CONFIRMING]!
              .services[indexOfRequest]
              .selectedOffers[indexOfOffer]
              .status = OFFER_PAID;
        }
      }
    }

    AppNavigator.push(
      Routes.PAYMENT_RESULT,
      arguments: {'isSuccess': _payOfferResult},
    );
  }

  Future<void> _addNewServiceToList(AddNewServiceToListEvent event) async {
    final List<RequestModel> _confirmingList =
        services[SERVICE_CONFIRMING]?.services ?? [];

    if (_confirmingList.isEmpty) {
      services[SERVICE_CONFIRMING]!.isOver = false;
    } else {
      _confirmingList.insert(0, event.service);
      services[SERVICE_CONFIRMING]?.services = _confirmingList;
    }
  }

  Future<void> _updateOfferStatusAfterPayment(
      UpdateOfferStatusAfterPaymentEvent event) async {
    final int _indexOfService =
        (services[SERVICE_CONFIRMING]?.services ?? []).indexWhere(
      (request) => request.id == event.service.id,
    );
    if (_indexOfService != -1) {
      int _indexOfOffer = services[SERVICE_CONFIRMING]!
          .services[_indexOfService]
          .selectedOffers
          .indexWhere(
            (offer) => offer.id == event.service.myOfferId,
          );
      if (_indexOfOffer != -1) {
        services[SERVICE_CONFIRMING]!
            .services[_indexOfService]
            .selectedOffers[_indexOfOffer]
            .status = OFFER_PAID;
      }
    }
  }

  void _addAllItemToBackupList() {
    backUpData = [];
    services.forEach((status, serviceItems) {
      backUpData.addAll(serviceItems.services);
    });
  }

  _cleanBloc() {
    services = {
      SERVICE_CONFIRMING: CachedServiceBloc(
        services: [],
      ), //chờ xác nhận, đã nhận offer. Offer status = [2,3]
      SERVICE_CONFIRMED: CachedServiceBloc(
        services: [],
      ), //đã xác nhận - expert đã xác nhận, Offer status = 4;
      SERVICE_ADVISED: CachedServiceBloc(
        services: [],
      ), //đã hoàn thành - Offer status = 4;
      SERVICE_COMPLETED: CachedServiceBloc(
        services: [],
      ), //đã hoàn thành - Offer status = 4;
      SERVICE_CANCELED: CachedServiceBloc(
        services: [],
      ), //đã huỷ - Offer status = -2;
      SERVICE_COMPLAINED: CachedServiceBloc(
        services: [],
      ), //khiếu nại - Offer status = -2;
    };
    reasons = [];
    backUpData = [];
  }

  _cleanReasons() {
    reasons = [];
  }

  _cleanData() {
    services = {
      SERVICE_CONFIRMING: CachedServiceBloc(
        services: [],
      ),
      SERVICE_CONFIRMED: CachedServiceBloc(
        services: [],
      ),
      SERVICE_ADVISED: CachedServiceBloc(
        services: [],
      ),
      SERVICE_COMPLETED: CachedServiceBloc(
        services: [],
      ),
      SERVICE_CANCELED: CachedServiceBloc(
        services: [],
      ),
      SERVICE_COMPLAINED: CachedServiceBloc(
        services: [],
      ),
    };
  }
}
