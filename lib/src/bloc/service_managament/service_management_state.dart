part of 'service_management_bloc.dart';

@immutable
abstract class ServiceManagamentState {
  /// props: - 0: List<RequestModel> services
  /// props: = 1: List<CancelServiceReasonModel> reasons
  /// props: = 2: List<RequestModel> backUpData;
  List get props => [];
}

class ServiceManagamentInitial extends ServiceManagamentState {
  @override
  List get props => [null, [], []];
}

class GettingService extends ServiceManagamentState {
  final Map<int, List<RequestModel>> services;
  final List<ReasonModel> reasons;
  final List<RequestModel> backUpData;

  GettingService({
    required this.services,
    required this.reasons,
    required this.backUpData,
  });

  @override
  List get props => [services, reasons];
}

class GetDoneService extends ServiceManagamentState {
  final Map<int, List<RequestModel>> services;
  final List<ReasonModel> reasons;
  final List<RequestModel> backUpData;

  GetDoneService({
    required this.services,
    required this.reasons,
    required this.backUpData,
  });

  @override
  List get props => [services, reasons, backUpData];
}
