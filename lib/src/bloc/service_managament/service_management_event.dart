part of 'service_management_bloc.dart';

@immutable
abstract class ServiceManagamentEvent {}

class OnServiceEvent extends ServiceManagamentEvent {
  final int status;
  OnServiceEvent({required this.status});
}

class GetServiceEvent extends ServiceManagamentEvent {}

class RefreshServiceEvent extends ServiceManagamentEvent {
  final Function handleFinished;
  RefreshServiceEvent({
    required this.handleFinished,
  });
}

class OnChangeStatusEvent extends ServiceManagamentEvent {}

class CleanServiceManagamentEvent extends ServiceManagamentEvent {}

class GetCancelServiceReasonsEvent extends ServiceManagamentEvent {}

class CancelOfferEvent extends ServiceManagamentEvent {
  final RequestModel service;
  final int tabNumber;
  final String offerId;
  final String reason;
  final String content;
  CancelOfferEvent({
    required this.service,
    required this.tabNumber,
    required this.offerId,
    required this.reason,
    required this.content,
  });
}

class ExpertConfirmOfferEvent extends ServiceManagamentEvent {
  final String offerId;
  final String requestId;
  ExpertConfirmOfferEvent({
    required this.offerId,
    required this.requestId,
  });
}

class UserCompleteOfferEvent extends ServiceManagamentEvent {
  final RequestModel service;
  UserCompleteOfferEvent({
    required this.service,
  });
}

class UserRatingAndFeedBackEvent extends ServiceManagamentEvent {
  final RequestModel service;
  final RatingModel serviceFeedbackModel;
  UserRatingAndFeedBackEvent({
    required this.service,
    required this.serviceFeedbackModel,
  });
}

class ReportOfferEvent extends ServiceManagamentEvent {
  final RequestModel service;
  final String offerId;
  final String reportContent;
  ReportOfferEvent({
    required this.service,
    required this.offerId,
    required this.reportContent,
  });
}

class ReplyFeedbackEvent extends ServiceManagamentEvent {
  final String ratingId;
  final String content;
  ReplyFeedbackEvent({
    required this.ratingId,
    required this.content,
  });
}

class ContinuePaymentStep1Event extends ServiceManagamentEvent {
  final OfferModel offer;
  final RequestModel request;
  final String authorName;
  final String authorPhone;
  final String note;
  final Function handleFinished;
  ContinuePaymentStep1Event({
    required this.offer,
    required this.request,
    required this.authorName,
    required this.authorPhone,
    required this.note,
    required this.handleFinished,
  });
}

class ContinuePaymentStep2Event extends ServiceManagamentEvent {
  final OfferModel offer;
  final RequestModel request;
  final DateTime startTime;
  final DateTime endTime;
  final Function handleFinished;
  ContinuePaymentStep2Event({
    required this.offer,
    required this.request,
    required this.startTime,
    required this.endTime,
    required this.handleFinished,
  });
}

class ContinuePaymentStep3Event extends ServiceManagamentEvent {
  final String offerId;
  final String expertId;
  final RequestModel request;
  final PaymentMethod paymentMethod;
  final double updatedWallet;
  final double amount;

  ContinuePaymentStep3Event({
    required this.offerId,
    required this.expertId,
    required this.request,
    required this.paymentMethod,
    required this.updatedWallet,
    required this.amount,
  });
}

class AddNewServiceToListEvent extends ServiceManagamentEvent {
  final RequestModel service;

  AddNewServiceToListEvent({
    required this.service,
  });
}

class UpdateOfferStatusAfterPaymentEvent extends ServiceManagamentEvent {
  final RequestModel service;
  UpdateOfferStatusAfterPaymentEvent({
    required this.service,
  });
}
