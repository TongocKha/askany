part of 'calendar_bloc.dart';

@immutable
abstract class CalendarState {
  /// Index 0: final List<RequestModel> scheduleRequests;
  /// Index 1: final DateTime selectedDate;
  /// Index 2: final bool isWeekMode;
  /// Index 3:   final List<RequestModel> backupService;

  List<dynamic> get props => [];
}

class CalendarInitial extends CalendarState {}

class CalendarLoading extends CalendarState {
  final List<RequestModel> scheduleRequests;
  final DateTime selectedDate;
  final bool isWeekMode;
  final List<RequestModel> backupServices;
  final List<RequestModel> cachedService;

  CalendarLoading({
    required this.scheduleRequests,
    required this.selectedDate,
    required this.isWeekMode,
    required this.backupServices,
    required this.cachedService,
  });

  @override
  List get props => [
        scheduleRequests,
        selectedDate,
        isWeekMode,
        backupServices,
        cachedService
      ];
}

class CalendarLoaded extends CalendarState {
  final List<RequestModel> timeLines;
  final DateTime selectedDate;
  final bool isWeekMode;
  final List<RequestModel> backupServices;
  final List<RequestModel> cachedService;

  CalendarLoaded({
    required this.timeLines,
    required this.selectedDate,
    required this.isWeekMode,
    required this.backupServices,
    required this.cachedService,
  });
  @override
  List get props =>
      [timeLines, selectedDate, isWeekMode, backupServices, cachedService];
}
