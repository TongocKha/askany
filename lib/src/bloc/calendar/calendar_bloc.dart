import 'package:askany/src/data/remote_data_source/calendar_repository.dart';
import 'package:askany/src/helpers/date_time_helper.dart';
import 'package:askany/src/models/request_model.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
part 'calendar_event.dart';
part 'calendar_state.dart';

class CalendarBloc extends Bloc<CalendarEvent, CalendarState> {
  CalendarBloc() : super(CalendarInitial());

  Map<String, List<RequestModel>> schedules = {};
  List<RequestModel> backupServices = [];
  List<RequestModel> cachedServices = [];

  DateTime selectedDate = DateTime.now().subtract(
    Duration(hours: TimeOfDay.now().hour, minutes: TimeOfDay.now().minute),
  );
  bool isWeekMode = false;
  bool isOverTimelines = false;

  @override
  Stream<CalendarState> mapEventToState(CalendarEvent event) async* {
    if (event is RefreshCalendarEvent) {
      await _refreshCalendar(event);
      addAllServicesToBackupList();
      yield _getCalendarLoaded();
    }
    if (event is CleanCalendarEvent) {
      cleanCalendar();
      yield CalendarInitial();
    }

    if (event is GetCalendarEvent) {
      if (schedules.isEmpty ||
          schedules[DateFormat('MM/yyyy').format(selectedDate)] == null) {
        yield CalendarInitial();
      } else {
        yield _getCalendarLoading();
      }
      await _fetchTimelines(selectedDate);
      addAllServicesToBackupList();

      yield _getCalendarLoaded();
    }

    if (event is PickDateEvent) {
      _pickDate(event.date);
      await _fetchTimelines(selectedDate);
      yield _getCalendarLoaded();
    }

    if (event is ToggleModeEvent) {
      _toggleMode();
      await _fetchTimelines(selectedDate);
      yield _getCalendarLoaded();
    }

    if (event is ChooseWeekModeEvent) {
      if (!isWeekMode) {
        _chooseWeekMode();
        await _fetchTimelines(selectedDate);
        yield _getCalendarLoaded();
      }
    }

    if (event is ChooseMonthModeEvent) {
      if (isWeekMode) {
        _chooseMonthMode();
        await _fetchTimelines(selectedDate);
        yield _getCalendarLoaded();
      }
    }

    if (event is NextMonthEvent) {
      _updateMonth();
      await _fetchTimelines(selectedDate);
      yield _getCalendarLoaded();
    }

    if (event is PreviousMonthEvent) {
      _updateMonth(isNext: false);
      await _fetchTimelines(selectedDate);
      yield _getCalendarLoaded();
    }

    if (event is RemoveServiceEvent) {
      _removeService(event.requestModel);
      yield _getCalendarLoaded();
    }
  }

  // Private methods
  CalendarLoaded _getCalendarLoaded() {
    List<RequestModel> requests = _scheduleByDate;
    return CalendarLoaded(
        timeLines: requests,
        selectedDate: selectedDate,
        isWeekMode: isWeekMode,
        backupServices: backupServices,
        cachedService: cachedServices);
  }

  CalendarLoading _getCalendarLoading() {
    List<RequestModel> requests = _scheduleByDate;
    return CalendarLoading(
        scheduleRequests: requests,
        selectedDate: selectedDate,
        isWeekMode: isWeekMode,
        backupServices: backupServices,
        cachedService: cachedServices);
  }

  Future<void> _refreshCalendar(RefreshCalendarEvent event) async {
    schedules.remove(DateFormat('MM/yyyy').format(selectedDate));
    await _fetchTimelines(selectedDate);
    event.handleFinished();
  }

  Future<void> _removeService(RequestModel requestModel) async {
    cachedServices = [];
    cachedServices.add(requestModel);

    if (schedules.isNotEmpty ||
        schedules[DateFormat('MM/yyyy').format(selectedDate)] != null) {
      schedules[DateFormat('MM/yyyy').format(selectedDate)] =
          schedules[DateFormat('MM/yyyy').format(selectedDate)]!
              .where((request) => request.id != requestModel.id)
              .toList();

      await _fetchTimelines(selectedDate);
    }
  }

  List<RequestModel> get _scheduleByDate {
    List<RequestModel> requests =
        schedules[DateFormat('MM/yyyy').format(selectedDate)] ?? [];
    if (isWeekMode) {
      return requests
          .where((request) => isEqualTwoDate(selectedDate, request.startTime!))
          .toList();
    }

    return requests
        .where((request) => selectedDate.isBefore(request.startTime!))
        .toList();
  }

  _toggleMode() {
    isWeekMode = !isWeekMode;
  }

  _chooseWeekMode() {
    isWeekMode = true;
  }

  _chooseMonthMode() {
    isWeekMode = false;
  }

  _pickDate(DateTime date) {
    selectedDate = date;
  }

  _updateMonth({bool isNext = true}) {
    if (isWeekMode) {
      if (isNext) {
        selectedDate = selectedDate.add(
          Duration(
            days: 7,
          ),
        );
      } else {
        selectedDate = selectedDate.subtract(
          Duration(
            days: 7,
          ),
        );
      }
    } else {
      int _currentMonth = selectedDate.month;
      if (isNext) {
        selectedDate = selectedDate.add(
          Duration(
            days: dayCountForMonth[_currentMonth - 1],
          ),
        );
      } else {
        _currentMonth -= 1;
        if (_currentMonth > 12) {
          _currentMonth = 1;
        } else if (_currentMonth < 1) {
          _currentMonth = 12;
        }
        selectedDate = selectedDate.subtract(
          Duration(
            days: dayCountForMonth[_currentMonth - 1],
          ),
        );
      }
    }
  }

  Future<void> _fetchTimelines(DateTime date) async {
    if (schedules.isEmpty ||
        schedules[DateFormat('MM/yyyy').format(date)] == null) {
      List<RequestModel> scheduleRequests =
          await CalendarRepository().getListScheduleRequest(
        date: selectedDate,
        skip: 0,
      );

      scheduleRequests.sort((a, b) => a.startTime!.compareTo(b.startTime!));

      schedules[DateFormat('MM/yyyy').format(date)] = scheduleRequests;
    }
  }

  int quantityPerDate(DateTime date) {
    List<RequestModel> requests =
        schedules[DateFormat('MM/yyyy').format(date)] ?? [];
    int quantity = requests
        .where((request) => isEqualTwoDate(date, request.startTime!))
        .toList()
        .length;
    return quantity >= 3 ? 3 : quantity;
  }

  void addAllServicesToBackupList() {
    cleanBackupList();
    schedules.forEach(
      (schedulesDate, services) {
        backupServices.addAll(services);
      },
    );
  }

  void addNewServiceToBackupList({required RequestModel service}) {}

  void deleteService({required RequestModel service}) {}

  void cleanBackupList() {
    backupServices.clear();
    cachedServices.clear();
  }

  void cleanCalendar() {
    schedules = {};
    backupServices = [];
    cachedServices = [];

    selectedDate = DateTime.now().subtract(
      Duration(hours: TimeOfDay.now().hour, minutes: TimeOfDay.now().minute),
    );
    isWeekMode = false;
    isOverTimelines = false;
  }
}
