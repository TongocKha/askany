part of 'calendar_bloc.dart';

@immutable
abstract class CalendarEvent {}

class RefreshCalendarEvent extends CalendarEvent {
  final Function handleFinished;
  RefreshCalendarEvent({required this.handleFinished});
}

class GetCalendarEvent extends CalendarEvent {}

class PickDateEvent extends CalendarEvent {
  final DateTime date;

  PickDateEvent({required this.date});
}

class NextMonthEvent extends CalendarEvent {}

class PreviousMonthEvent extends CalendarEvent {}

class ToggleModeEvent extends CalendarEvent {}

class ChooseWeekModeEvent extends CalendarEvent {}

class ChooseMonthModeEvent extends CalendarEvent {}

class RemoveServiceEvent extends CalendarEvent {
  final RequestModel requestModel;
  RemoveServiceEvent({required this.requestModel});
}

class CleanCalendarEvent extends CalendarEvent {}
