part of 'video_player_bloc.dart';

@immutable
abstract class VideoPlayerState {}

class VideoPlayerInitial extends VideoPlayerState {}

class VideoPlaying extends VideoPlayerState {
  final Map<String, YoutubePlayerController?> videos;
  VideoPlaying({
    required this.videos,
  });
}
