import 'package:askany/src/helpers/device_orientation_helper.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

part 'video_player_event.dart';
part 'video_player_state.dart';

class VideoPlayerBloc extends Bloc<VideoPlayerEvent, VideoPlayerState> {
  VideoPlayerBloc() : super(VideoPlayerInitial());

  String urlToVideo = '';
  Map<String, YoutubePlayerController?> videos = {};

  @override
  Stream<VideoPlayerState> mapEventToState(VideoPlayerEvent event) async* {
    if (event is PlayVideoEvent) {
      await _playVideo(event);
      yield _videoPlaying;
    }

    if (event is DisposeVideoEvent) {
      if (videos[urlToVideo] != null) {
        urlToVideo = '';
        videos[urlToVideo]?.pause();
        videos[urlToVideo]?.close();
        yield _videoPlaying;
      }
    }
  }

  // MARK: Private methods
  VideoPlaying get _videoPlaying => VideoPlaying(
        videos: videos,
      );

  Future<void> _playVideo(PlayVideoEvent event) async {
    if (event.urlToVideo == null) {
      return;
    }

    urlToVideo = event.urlToVideo!;
    String? videoId = convertUrlToId(urlToVideo);
    if (videoId != null) {
      videos[urlToVideo] = YoutubePlayerController(
        initialVideoId: videoId,
        params: YoutubePlayerParams(
          showFullscreenButton: true,
          mute: false,
          autoPlay: true,
          loop: false,
          enableCaption: false,
          showControls: true,
          desktopMode: false,
          privacyEnhanced: true,
          useHybridComposition: true,
        ),
      );

      videos[urlToVideo]?.onEnterFullscreen = () {
        DeviceOrientationHelper().setLandscape();
      };
      videos[urlToVideo]?.onExitFullscreen = () {
        DeviceOrientationHelper().setPortrait();
      };
    }
  }

  static String? convertUrlToId(String url, {bool trimWhitespaces = true}) {
    if (!url.contains("http") && (url.length == 11)) return url;
    if (trimWhitespaces) url = url.trim();

    for (var exp in [
      RegExp(r"^https:\/\/(?:www\.|m\.)?youtube\.com\/watch\?v=([_\-a-zA-Z0-9]{11}).*$"),
      RegExp(
          r"^https:\/\/(?:www\.|m\.)?youtube(?:-nocookie)?\.com\/embed\/([_\-a-zA-Z0-9]{11}).*$"),
      RegExp(r"^https:\/\/youtu\.be\/([_\-a-zA-Z0-9]{11}).*$")
    ]) {
      Match? match = exp.firstMatch(url);
      if (match != null && match.groupCount >= 1) return match.group(1);
    }

    return null;
  }
}
