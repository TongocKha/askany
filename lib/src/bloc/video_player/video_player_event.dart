part of 'video_player_bloc.dart';

@immutable
abstract class VideoPlayerEvent {}

class PlayVideoEvent extends VideoPlayerEvent {
  final String? urlToVideo;
  PlayVideoEvent({required this.urlToVideo});
}

class StopVideoEvent extends VideoPlayerEvent {}

class DisposeVideoEvent extends VideoPlayerEvent {}
