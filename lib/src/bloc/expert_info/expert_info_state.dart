part of 'expert_info_bloc.dart';

@immutable
abstract class ExpertInfoState {
  /// 0: List<ExpertModel>
  List get props => [];
}

class ExpertInfoInitial extends ExpertInfoState {}

class GetInfoExpert extends ExpertInfoState {
  final Map<String, ExpertModel> experts;
  GetInfoExpert({required this.experts});

  @override
  List get props => [experts];
}
