import 'package:askany/src/data/remote_data_source/expert_repository.dart';
import 'package:askany/src/models/expert_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:askany/src/ui/search/widgets/bottom_sheet_info_expert.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
part 'expert_info_event.dart';
part 'expert_info_state.dart';

class ExpertInfoBloc extends Bloc<ExpertInfoEvent, ExpertInfoState> {
  ExpertInfoBloc() : super(ExpertInfoInitial());

  Map<String, ExpertModel> experts = {};

  @override
  Stream<ExpertInfoState> mapEventToState(ExpertInfoEvent event) async* {
    if (event is GetInfoExpertEvent) {
      if (experts[event.expertId] == null) {
        await _getInfo(event.expertId);
      }
      yield _getInfoExpert;
    }

    if (event is CleanExpertInfoEvent) {
      yield ExpertInfoInitial();
    }

    if (event is ShowBottomSheetInfoEvent) {
      if (experts[event.expertId] == null) {
        showDialogLoading();
        await _getInfo(event.expertId);
        AppNavigator.pop();
      }
      showModalBottomSheet(
        context: event.context,
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        builder: (context) {
          return BottomSheetInfoExpert(
            expert: experts[event.expertId]!,
          );
        },
      );
    }
  }

  //private
  GetInfoExpert get _getInfoExpert => GetInfoExpert(experts: experts);

  Future<void> _getInfo(String expertId) async {
    ExpertModel? expertInfo =
        await ExpertRepository().getInfoExpert(idExpert: expertId);
    if (expertInfo != null) {
      experts[expertId] = expertInfo;
    }
  }
}
