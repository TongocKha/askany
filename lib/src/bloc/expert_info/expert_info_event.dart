part of 'expert_info_bloc.dart';

@immutable
abstract class ExpertInfoEvent {}

class GetInfoExpertEvent extends ExpertInfoEvent {
  final String expertId;
  GetInfoExpertEvent({required this.expertId});
}

class ShowBottomSheetInfoEvent extends ExpertInfoEvent {
  final BuildContext context;
  final String expertId;
  ShowBottomSheetInfoEvent({required this.context, required this.expertId});
}

class CleanExpertInfoEvent extends ExpertInfoEvent {}
