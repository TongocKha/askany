import 'dart:async';
import 'dart:io';
import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/badge/badge_bloc.dart';
import 'package:askany/src/bloc/chat/chat_bloc.dart';
import 'package:askany/src/bloc/message_images/message_images_bloc.dart';
import 'package:askany/src/bloc/photo_manager/photo_manager_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/data/local_data_source/message_local_data.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/data/remote_data_source/chat_repository.dart';
import 'package:askany/src/data/remote_data_source/message_repository.dart';
import 'package:askany/src/helpers/clipboard.dart';
import 'package:askany/src/helpers/device_helper.dart';
import 'package:askany/src/helpers/photo_helper.dart';
import 'package:askany/src/models/account_model.dart';
import 'package:askany/src/models/conversation_model.dart';
import 'package:askany/src/models/message_model.dart';
import 'package:askany/src/models/message_offer_model.dart';
import 'package:askany/src/models/slide_mode.dart';
import 'package:askany/src/services/firebase_messaging/local_notification.dart';
import 'package:askany/src/services/socket/socket.dart';
import 'package:askany/src/services/socket/socket_emit.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_with_text_and_pop_button.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:askany/src/helpers/extentions/string_extention.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

part 'message_event.dart';
part 'message_state.dart';

class MessageBloc extends Bloc<MessageEvent, MessageState> {
  MessageBloc() : super(MessageInitial());

  Map<String, Map<String, dynamic>> messagesMap = {};
  String conversationId = '';
  MessageModel? chosenMessage;
  MessageModel? parentMessage;
  List<MessageModel> newMessages = [];
  List<MessageModel>? selectedMessages;
  GlobalKey? keyChosen;
  List<MessageModel> searchMessages = [];
  bool isOverSearch = false;
  Map<String, int> totalConversations = {};

  @override
  Stream<MessageState> mapEventToState(MessageEvent event) async* {
    if (event is OnMessageEvent) {
      chosenMessage = null;
      parentMessage = null;
      yield MessageInitial();
      conversationId = event.conversationId;
      SocketEmit().joinRoom(conversationId);
      if (!event.isSeen) {
        await _markSeenAllMessage();
      }
      if (_listMessageByConversation(conversationId: conversationId).isEmpty) {
        await _getMessageByConversation();
        AppBloc.messageImageBloc
            .add(OnMessagesImageEvent(conversationId: conversationId));
        _getErrorMessage();
      }
      yield _getDoneMessage;
    }

    if (event is GetMessageEvent) {
      if (!_getIsOverMessage(conversationId: conversationId)) {
        yield _gettingMessage;
        await _getMessageByConversation();
        AppBloc.messageImageBloc
            .add(GetMessgesImagesEvent(conversationId: conversationId));
        yield _getDoneMessage;

        if (event.scrollEvent != null) {
          Future.delayed(Duration(milliseconds: DELAY_250_MS), () {
            add(
              ScrollToMessageEvent(
                parentMessageId: event.scrollEvent!.parentMessageId,
                controller: event.scrollEvent!.controller,
                animationController: event.scrollEvent!.animationController,
              ),
            );
          });
        }
      }
    }

    if (event is SendMessageEvent) {
      MessageModel _msgModel = _sendMessageLocal(event);
      yield _getDoneMessage;
      await _sendMessageRemote(
        data: event.message,
        messageId: _msgModel.id,
        images: event.files ?? _msgModel.localImages,
        parentId: _msgModel.parent?.id,
      );
      yield _getDoneMessage;
    }

    if (event is InsertMessageEvent) {
      _insertMessage(event);

      if (!event.message.isMe) {
        if (conversationId != event.message.conversation) {
          int indexOfConversation = AppBloc.chatBloc.conversations.indexWhere(
            (conversation) => conversation.id == event.message.conversation,
          );
          String title = '';
          AccountModel? userSender;

          ConversationModel? conversation;

          if (indexOfConversation != -1) {
            title = AppBloc.chatBloc.conversations[indexOfConversation].conversationTitle;
            userSender = AppBloc.chatBloc.conversations[indexOfConversation].allMembers.firstWhere(
              (element) => element.id == event.message.userSender,
            );
            conversation = AppBloc.chatBloc.conversations[indexOfConversation];
          } else {
            conversation = await _getNewConversationInfo(
              conversationId: event.message.conversation,
            );

            if (conversation != null) {
              title = conversation.conversationTitle;
              userSender = conversation.allMembers.firstWhere(
                (element) => element.id == event.message.userSender,
              );

              AppBloc.chatBloc.add(InsertConversationEvent(conversation: conversation));
            }
          }

          if (conversation != null) {
            LocalNotification().showNotificationWithSound(
              hashCode: 9798,
              title: title,
              description: event.message.data.isEmpty
                  ? '$title ' + Strings.justSendPictures.i18n
                  : event.message.data,
              payload: {...conversation.toMap(), 'route': 'chat'},
              urlToImage: userSender?.avatar?.urlToImage,
            );
          }
        } else {
          newMessages.insert(0, event.message);
          DeviceHelper().soundAndRing();
          Future.delayed(Duration(seconds: DELAY_TWO_SECONDS), () {
            add(RemoveNewMessageEvent(messageId: event.message.id));
          });
        }
      }

      yield _getDoneMessage;
    }

    if (event is InsertAutomaticMessageEvent) {
      _insertAutoMessage(event);
      yield _getDoneMessage;
    }

    if (event is RemoveNewMessageEvent) {
      int indexOfNewMessage = newMessages.indexWhere((message) => message.id == event.messageId);
      if (indexOfNewMessage != -1) {
        newMessages.removeAt(indexOfNewMessage);
      }
      yield _getDoneMessage;
    }

    if (event is RetrySendMessageEvent) {
      _retryMessageLocal(event.message.id);
      yield _getDoneMessage;
      await _sendMessageRemote(
        data: event.message.data,
        messageId: event.message.id,
        isRetry: true,
        images: event.message.localImages,
        parentId: null,
      );
      yield _getDoneMessage;
    }

    if (event is DeleteMessageEvent) {
      if (_isCheckValidateTimeMessage(event.message.createdAt)) {
        if (selectedMessages != null) {
          selectedMessages = null;
        }

        await _deleteMessage(event);
        yield _getDoneMessage;
      }
    }

    if (event is PickEditMessageEvent) {
      chosenMessage = event.message;
      yield _getDoneMessage;
    }

    if (event is CancelEditMessageEvent) {
      chosenMessage = null;
      keyChosen = null;
      yield _getDoneMessage;
    }

    if (event is SendEditMessageEvent) {
      await _handleEditMessage(event);
      chosenMessage = null;
      keyChosen = null;
      yield _getDoneMessage;
    }

    if (event is SocketUpdateMessageEvent) {
      _updateMessage(messageId: event.messageId, data: event.data);
      yield _getDoneMessage;
    }

    if (event is SocketDeleteMessageEvent) {
      _deleteMessageLocal(messageId: event.messageId);
      yield _getDoneMessage;
    }

    if (event is QuoteMessageEvent) {
      if (AppBloc.photoManagerBloc.imagesChoosen.isNotEmpty) {
        AppBloc.photoManagerBloc.add(CleanPhotoPickerEvent());
      }
      parentMessage = event.message;
      yield _getDoneMessage;
    }

    if (event is SelectMessageEvent) {
      if (event.message == null) {
        selectedMessages = null;
      } else {
        if (selectedMessages == null) {
          selectedMessages = [event.message!];
        } else {
          int indexOfSelected = selectedMessages!.indexOf(event.message!);
          if (indexOfSelected != -1) {
            selectedMessages!.removeAt(indexOfSelected);
          } else {
            selectedMessages!.add(event.message!);
          }
        }
      }

      yield _getDoneMessage;
    }

    if (event is CopyMessageEvent) {
      List<AccountModel> allUsers = AppBloc.chatBloc.conversations
          .where((element) => element.id == conversationId)
          .first
          .allMembers;
      String copyText = '';

      selectedMessages!.sort((a, b) => a.createdAt.compareTo(b.createdAt));
      selectedMessages!.asMap().forEach((index, msg) {
        String oneLine =
            allUsers.where((user) => user.id == msg.userSender).first.fullname!.formatName() +
                ', ${DateFormat('HH:mm a').format(msg.createdAt)}\n${msg.data}'
                    '${index == selectedMessages!.length - 1 ? '' : '\n\n'}';
        copyText += oneLine;
      });

      CustomClipboard.copy(copyText);

      selectedMessages = null;
      yield _getDoneMessage;
    }

    if (event is DisposeMessageEvent) {
      if (!AppBloc.videoCallBloc.isCalling) {
        // SocketEmit().leaveRoomChat(conversationId);
        conversationId = '';
        selectedMessages = null;
      }
    }

    if (event is ScrollToMessageEvent) {
      _handleScrollToMessage(event);
    }

    if (event is CleanMessageEvent) {
      _handleClean();
      yield MessageInitial();
    }

    if (event is SearchMessageEvent) {
      if (searchMessages.isNotEmpty) {
        searchMessages.clear();
        yield _getDoneMessage;
      }
      await _getSearchMessage(event);
      yield _getDoneMessage;
    }
    if (event is ScrollMessageSearchEvent) {
      // yield _gettingMessage;
      // await _scrollMessageSearch(event);
      // yield _getDoneMessage;
      // await Future.delayed(Duration(milliseconds: DELAY_TWO_SECONDS));
      // event.itemScrollController.jumpTo(
      //   index: totalConversations[conversationId]! - event.index + 1,
      // );
      List<MessageModel> listMessage = _listMessageByConversation(conversationId: conversationId);
      int indexMessageId = listMessage.indexWhere(
        (msg) => msg.id == event.messageId,
      );
      if (indexMessageId != -1) {
        await Future.delayed(Duration(milliseconds: 200));
        event.itemScrollController.jumpTo(
          index: totalConversations[conversationId]! - event.index + 1,
        );
      } else {
        yield _gettingMessage;
        await _scrollMessageSearch(event);
        yield _getDoneMessage;
        add(
          ScrollMessageSearchEvent(
            index: event.index,
            messageId: event.messageId,
            itemScrollController: event.itemScrollController,
          ),
        );
      }
    }
  }

  // Private methods

  GettingMessage get _gettingMessage => GettingMessage(
        messages: _listMessageByConversation(
          conversationId: conversationId,
        ),
        chosenMessage: chosenMessage,
        newMessages: newMessages,
        selectedMessages: selectedMessages,
        parentMessage: parentMessage,
        searchMessages: searchMessages,
      );
  GetDoneMessage get _getDoneMessage => GetDoneMessage(
        messages: _listMessageByConversation(
          conversationId: conversationId,
        ),
        chosenMessage: chosenMessage,
        newMessages: newMessages,
        selectedMessages: selectedMessages,
        parentMessage: parentMessage,
        searchMessages: searchMessages,
      );

  List<MessageModel> _listMessageByConversation({required String conversationId}) =>
      messagesMap[conversationId]?[MESSAGE_FIELD] ?? [];

  MessageModel _newMessage(String data) {
    String messageId = DateTime.now().microsecondsSinceEpoch.toString();
    MessageModel msgModel = MessageModel(
      id: messageId,
      conversation: conversationId,
      userSender: AppBloc.userBloc.getAccount.id!,
      data: data,
      isSeen: 1,
      status: 1,
      createdAt: DateTime.now(),
      modifiedAt: DateTime.now(),
      isSending: true,
    );

    if (AppBloc.photoManagerBloc.imagesChoosen.isNotEmpty) {
      msgModel.localImages = [];
      msgModel.localImages?.addAll(
        AppBloc.photoManagerBloc.imagesChoosen.map(
          (path) => File(path),
        ),
      );
    }

    if (parentMessage != null) {
      msgModel.parent = parentMessage;
    }

    return msgModel;
  }

  MessageModel _newAutoMessage(InsertAutomaticMessageEvent event) {
    return MessageModel(
      id: DateTime.now().microsecondsSinceEpoch.toString(),
      conversation: event.conversationId,
      userSender: AppBloc.userBloc.getAccount.id!,
      data: event.message.status == OFFER_CANCELED
          ? '${Strings.userCancelServiceRequestNoti.i18n} ${event.message.requestTitle}'
          : '${Strings.youAreChosenForRequest.i18n}: ${event.message.requestTitle}',
      isSeen: IS_SEEN,
      status: 1,
      createdAt: DateTime.now(),
      modifiedAt: DateTime.now(),
      isSending: false,
      request: event.message,
      typeSystem: event.message.status == OFFER_CANCELED ? 2 : 1,
    );
  }

  bool _getIsOverMessage({required String conversationId}) =>
      messagesMap[conversationId]?[IS_OVER_FIELD] ?? false;

  void _insertAutoMessage(InsertAutomaticMessageEvent event) {
    List<MessageModel> _messages = _listMessageByConversation(
      conversationId: event.conversationId,
    );

    MessageModel _newMessage = _newAutoMessage(event);

    if (_messages.isNotEmpty) {
      _messages.insert(0, _newMessage);
    }

    AppBloc.chatBloc.add(
      UpdateLatestMessageEvent(
        conversationId: event.conversationId,
        message: _newMessage,
      ),
    );

    SocketEmit().sendMessage(message: _newMessage);
  }

  void _insertMessage(InsertMessageEvent event) async {
    List<MessageModel> _messages = _listMessageByConversation(
      conversationId: event.message.conversation,
    );

    if (conversationId == event.message.conversation) {
      _markSeenAllMessage();
    } else {
      if (_messages.isEmpty) {
        AppBloc.badgesBloc.add(GetBadgesChatEvent());
      } else {
        int indexOfConversation = AppBloc.chatBloc.conversations.indexWhere(
          (conversation) => conversation.id == event.message.conversation,
        );
        if (indexOfConversation != -1 &&
            AppBloc.chatBloc.conversations[indexOfConversation].latestMessage.isSeen) {
          AppBloc.badgesBloc.add(
            UpdateBadgesChatEvent(extendValue: 1),
          );
        } else {
          AppBloc.badgesBloc.add(GetBadgesChatEvent());
        }
      }
    }

    if (_messages.isNotEmpty) {
      int indexOfMessage = _messages.indexWhere((msg) => msg.id == event.message.id);

      if (indexOfMessage == -1) {
        _messages.insert(0, event.message);
        _setListMessageByConversation(
          conversationId: event.message.conversation,
          messages: _messages,
        );
      }
    }

    MessageModel messageModelConvert = event.message;
    if (messageModelConvert.conversation == conversationId) {
      messageModelConvert.status = IS_SEEN;
    }

    AppBloc.chatBloc.add(
      UpdateLatestMessageEvent(
        conversationId: event.message.conversation,
        message: messageModelConvert,
      ),
    );
  }

  void _retryMessageLocal(String messageId) {
    List<MessageModel> _messages = _listMessageByConversation(conversationId: conversationId);
    int indexOfMessage = _messages.indexWhere((e) => e.id == messageId);
    if (indexOfMessage != -1) {
      _messages[indexOfMessage].isSending = true;
      _messages[indexOfMessage].isError = false;
    }
  }

  MessageModel _sendMessageLocal(SendMessageEvent event) {
    MessageModel _newMessageLocal = _newMessage(event.message);

    if (AppBloc.photoManagerBloc.imagesChoosen.isNotEmpty) {
      AppBloc.photoManagerBloc.add(
        CleanPhotoPickerEvent(),
      );
    }

    parentMessage = null;

    List<MessageModel> _messages = _listMessageByConversation(conversationId: conversationId);
    _messages.insert(0, _newMessageLocal);
    _setListMessageByConversation(conversationId: conversationId, messages: _messages);
    AppBloc.chatBloc.add(
      UpdateLatestMessageEvent(
        conversationId: conversationId,
        message: _newMessageLocal,
      ),
    );

    return _newMessageLocal;
  }

  Future<List<File>> _reduceImageSize({required List<File>? images}) async {
    final List<File> _images = [];
    if (images != null && images.isNotEmpty) {
      for (int i = 0; i <= images.length; i++) {
        final File? _reducedSizeImage = await PhotoHelper().reduceSize(images[i].path, quality: 85);

        if (_reducedSizeImage != null) {
          _images.add(_reducedSizeImage);
        }
      }
    }

    return _images;
  }

  Future<void> _sendMessageRemote({
    required String data,
    required String messageId,
    required String? parentId,
    required List<File>? images,
    bool isRetry = false,
  }) async {
    final List<File>? _images = await _reduceImageSize(images: images);
    MessageModel? message = data == AUTHOR_PROJECT || !isSocketConnected()
        ? null
        : await MessageRepository().createMessage(
            conversationId: conversationId,
            data: data,
            images: _images,
            parentId: parentId == null ? null : parentId,
          );

    if (message != null) {
      SocketEmit().sendMessage(
        message: message,
      );

      if (isRetry) {
        List<MessageModel> _errorMessages = MessageLocal().errorMessages;
        int indexOfError = _errorMessages.indexWhere((e) => e.id == messageId);
        if (indexOfError != -1) {
          _errorMessages.removeAt(indexOfError);
          MessageLocal().saveErrorMessages(_errorMessages);
        }

        AppBloc.chatBloc.add(
          UpdateLatestMessageEvent(
            conversationId: conversationId,
            message: message,
          ),
        );
      }

      List<MessageModel> _messages = _listMessageByConversation(conversationId: conversationId);
      int indexOfPending = _messages.indexWhere((e) => e.id == messageId);
      if (indexOfPending != -1) {
        _messages[indexOfPending] = message;
      }
      _messages.sort((a, b) => b.createdAt.compareTo(a.createdAt));
      _setListMessageByConversation(conversationId: conversationId, messages: _messages);
    } else {
      List<MessageModel> _messages = _listMessageByConversation(conversationId: conversationId);
      int indexOfPending = _messages.indexWhere((e) => e.id == messageId);
      if (indexOfPending != -1) {
        _messages[indexOfPending].isSending = false;
        _messages[indexOfPending].isError = true;

        // Store to Local Storage
        if (!isRetry) {
          List<MessageModel> _errorMessages = MessageLocal().errorMessages;
          _errorMessages.add(_messages[indexOfPending]);
          MessageLocal().saveErrorMessages(_errorMessages);
        }
      }
    }
  }

  void _setListMessageByConversation({
    required String conversationId,
    required List<MessageModel> messages,
    bool isOver = false,
  }) {
    messagesMap[conversationId] = {
      MESSAGE_FIELD: messages,
      IS_OVER_FIELD: isOver,
    };
  }

  Future<void> _getSearchMessage(SearchMessageEvent event) async {
    Map<List<MessageModel>, int> _messages = await MessageRepository().getSearchMessage(
      conversationId: conversationId,
      skip: searchMessages.length,
      search: event.searchKey,
    );

    if (_messages.keys.length < LIMIT_API_15) {
      isOverSearch = true;
    }

    searchMessages.addAll(_messages.keys.first);
  }

  Future<void> _scrollMessageSearch(ScrollMessageSearchEvent event) async {
    List<MessageModel> _messagesStore = _listMessageByConversation(conversationId: conversationId);
    //bool _isOver = _getIsOverMessage(conversationId: conversationId);

    if (totalConversations[conversationId] != null &&
        ((totalConversations[conversationId]! - event.index) >= _messagesStore.length)) {
      Map<List<MessageModel>, int> _messages = await MessageRepository().getSearchMessage(
        conversationId: conversationId,
        skip: _messagesStore.length,
        limit: totalConversations[conversationId]! - event.index - _messagesStore.length + 1,
      );

      _messagesStore.addAll(_messages.keys.first);
      _setListMessageByConversation(
        conversationId: conversationId,
        messages: _messagesStore,
        // isOver: _isOver,
      );
    }
  }

  Future<void> _getMessageByConversation() async {
    List<MessageModel> _messagesStore = _listMessageByConversation(conversationId: conversationId);
    bool _isOver = _getIsOverMessage(conversationId: conversationId);

    Map<List<MessageModel>, int> _messages = await MessageRepository().getMessageByConversation(
      conversationId: conversationId,
      skip: _messagesStore.length,
    );

    if (_messages.keys.first.length < LIMIT_API_15) {
      _isOver = true;
    }

    _messagesStore.addAll(_messages.keys.first);

    _setListMessageByConversation(
      conversationId: conversationId,
      messages: _messagesStore,
      isOver: _isOver,
    );
    if (totalConversations[conversationId] == null) {
      totalConversations[conversationId] = _messages.values.first;
    }
  }

  void _getErrorMessage() async {
    List<MessageModel> _errorMessages = MessageLocal()
        .errorMessages
        .where((e) => e.conversation == conversationId && e.userSender == UserLocal().getUser().id)
        .toList();
    if (_errorMessages.isNotEmpty) {
      List<MessageModel> _messages = _listMessageByConversation(conversationId: conversationId);
      _messages.addAll(_errorMessages);
      _messages.sort((a, b) => b.createdAt.compareTo(a.createdAt));
    }
  }

  Future<bool> _deleteMessage(DeleteMessageEvent event) async {
    bool isDeleteSucceed = true;
    if (!event.message.isSending && !event.message.isError) {
      isDeleteSucceed = await MessageRepository().deleteMessage(messageId: event.message.id);

      if (isDeleteSucceed) {
        SocketEmit().deleteMessage(message: event.message);
        _deleteMessageLocal(messageId: event.message.id);
      }
    } else if (event.message.isError) {
      List<MessageModel> _errorMessages = MessageLocal().errorMessages;
      int indexOfError = _errorMessages.indexWhere((e) => e.id == event.message.id);
      if (indexOfError != -1) {
        _errorMessages.removeAt(indexOfError);
        MessageLocal().saveErrorMessages(_errorMessages);
      }
      _deleteMessageLocal(messageId: event.message.id, hasDeleteLocal: true);
    }

    return isDeleteSucceed;
  }

  void _deleteMessageLocal({required String messageId, bool hasDeleteLocal = false}) {
    List<MessageModel> _messages = _listMessageByConversation(conversationId: conversationId);
    int indexOfMessages = _messages.indexWhere((e) => e.id == messageId);
    if (indexOfMessages != -1) {
      if (!hasDeleteLocal) {
        _messages[indexOfMessages].status = -1;
        _messages[indexOfMessages].data = Strings.revokedMessageNoti.i18n;
      } else {
        _messages.removeAt(indexOfMessages);
      }

      _setListMessageByConversation(conversationId: conversationId, messages: _messages);

      if (indexOfMessages == 0) {
        AppBloc.chatBloc.add(
          UpdateLatestMessageEvent(
            conversationId: conversationId,
            message: _messages.length > 0 ? _messages[0] : _newMessage(''),
          ),
        );
      }
    }
  }

  Future<void> _handleEditMessage(SendEditMessageEvent event) async {
    if (chosenMessage == null) {
      return;
    }

    bool isEditSucceed = await MessageRepository().editMessage(
      messageId: chosenMessage!.id,
      data: event.message,
    );

    if (isEditSucceed) {
      _updateMessage(messageId: chosenMessage!.id, data: event.message, isMyUpdate: true);

      if (keyChosen != null) {
        List<MessageModel> listMessage = _listMessageByConversation(conversationId: conversationId);
        int indexOfMessage = listMessage.indexWhere(
          (msg) => msg.id == chosenMessage!.id,
        );

        if (indexOfMessage != -1) {
          // event.controller.scrollTo(
          //   index: indexOfMessage + 1,
          //   duration: Duration(
          //     milliseconds: DURATION_DEFAULT_ANIMATION,
          //   ),
          // );
        }
      }
    }
  }

  void _updateMessage({required String messageId, required String data, bool isMyUpdate = false}) {
    List<MessageModel> _messages = _listMessageByConversation(conversationId: conversationId);
    int indexOfMessages = _messages.indexWhere((e) => e.id == messageId);
    if (indexOfMessages != -1) {
      _messages[indexOfMessages].data = data;
      _setListMessageByConversation(conversationId: conversationId, messages: _messages);

      if (isMyUpdate) {
        SocketEmit().editMessage(
          message: _messages[indexOfMessages],
        );
      }

      if (indexOfMessages == 0) {
        AppBloc.chatBloc.add(
          UpdateLatestMessageEvent(
            conversationId: conversationId,
            message: _messages[0],
          ),
        );
      }
    }
  }

  Future<void> _markSeenAllMessage() async {
    AppBloc.chatBloc.add(MarkReadEvent(conversationId: conversationId));
    await MessageRepository().markSeenAllMessage(conversationId: conversationId);
  }

  Future<ConversationModel?> _getNewConversationInfo({required String conversationId}) async {
    ConversationModel? conversation = await ChatRepository().getDetailsConversation(
      conversationId: conversationId,
    );

    if (conversation != null) {
      AppBloc.chatBloc.add(
        InsertConversationEvent(
          conversation: conversation,
        ),
      );
    }

    return conversation;
  }

  void _handleScrollToMessage(ScrollToMessageEvent event) async {
    List<MessageModel> listMessage = _listMessageByConversation(conversationId: conversationId);
    int indexOfParent = listMessage.indexWhere(
      (msg) => msg.id == event.parentMessageId,
    );

    if (indexOfParent != -1) {
      for (int i = 0; i < listMessage.length; i++) {
        if (i == indexOfParent) {
          listMessage[i].animationController = event.animationController;
        } else {
          listMessage[i].animationController = null;
        }
      }

      event.controller.scrollTo(
        index: indexOfParent + 1,
        duration: Duration(milliseconds: DURATION_DEFAULT_ANIMATION),
      );
      event.animationController.repeat();
      event.animationController.addListener(() {
        debugPrint(event.animationController.status.toString());
      });
      Future.delayed(
        Duration(milliseconds: DURATION_DEFAULT_ANIMATION * 10),
        () {
          event.animationController.stop();
          event.animationController.reset();
        },
      );
    } else {
      event.controller.scrollTo(
        index: listMessage.length,
        duration: Duration(
          seconds: DELAY_A_MINUTE,
        ),
      );
      add(GetMessageEvent(
        conversationId: conversationId,
        scrollEvent: ScrollToMessageEvent(
          animationController: event.animationController,
          parentMessageId: event.parentMessageId,
          controller: event.controller,
        ),
      ));
    }
  }

  void _handleClean() {
    messagesMap = {};
    conversationId = '';
    AppBloc.messageImageBloc.add(CleanMessagesImagesEvent());
  }

  bool _isCheckValidateTimeMessage(DateTime createAt) {
    bool result = DateTime.now().difference(createAt).inMinutes <= 2;
    if (!result) {
      dialogAnimationWrapper(
        slideFrom: SlideMode.bot,
        child: DialogWithTextAndPopButton(
          bodyAfter: Strings.cannotEditMessageNoti.i18n,
          bodyAlign: TextAlign.center,
        ),
      );
    }
    return result;
  }
}
