part of 'message_bloc.dart';

@immutable
abstract class MessageState {
  /// 0: List<Message> || 1: chosen message ||
  /// 2: List<Message> new messages || 3: List<MessageModel>? selected messages
  /// 4: List<Message> search messages
  List<dynamic> get props => [];
}

class MessageInitial extends MessageState {
  @override
  List get props => [[], null, [], [], false, null, []];
}

class GettingMessage extends MessageState {
  final List<MessageModel> messages;
  final MessageModel? chosenMessage;
  final List<MessageModel> newMessages;
  final List<MessageModel>? selectedMessages;
  final bool isTyping;
  final MessageModel? parentMessage;
  final List<MessageModel>? searchMessages;
  GettingMessage({
    required this.messages,
    required this.chosenMessage,
    required this.newMessages,
    required this.selectedMessages,
    this.parentMessage,
    this.isTyping = false,
    this.searchMessages,
  });

  @override
  List get props => [
        messages,
        chosenMessage,
        newMessages,
        selectedMessages,
        isTyping,
        parentMessage,
        searchMessages,
      ];
}

class GetDoneMessage extends MessageState {
  final List<MessageModel> messages;
  final MessageModel? chosenMessage;
  final List<MessageModel> newMessages;
  final List<MessageModel>? selectedMessages;
  final bool isTyping;
  final MessageModel? parentMessage;
  final List<MessageModel>? searchMessages;
  GetDoneMessage({
    required this.messages,
    required this.chosenMessage,
    required this.newMessages,
    required this.selectedMessages,
    this.isTyping = false,
    this.parentMessage,
    this.searchMessages,
  });

  @override
  List get props => [
        messages,
        chosenMessage,
        newMessages,
        selectedMessages,
        isTyping,
        parentMessage,
        searchMessages,
      ];
}
