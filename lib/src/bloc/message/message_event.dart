part of 'message_bloc.dart';

@immutable
abstract class MessageEvent {}

class OnMessageEvent extends MessageEvent {
  final String conversationId;
  final bool isSeen;
  OnMessageEvent({required this.conversationId, required this.isSeen});
}

class RemoveNewMessageEvent extends MessageEvent {
  final String messageId;
  RemoveNewMessageEvent({required this.messageId});
}

class GetMessageEvent extends MessageEvent {
  final String conversationId;
  final ScrollToMessageEvent? scrollEvent;
  GetMessageEvent({required this.conversationId, this.scrollEvent});
}

class DeleteMessageEvent extends MessageEvent {
  final MessageModel message;
  DeleteMessageEvent({required this.message});
}

class MarkSeenMessageEvent extends MessageEvent {}

class SendMessageEvent extends MessageEvent {
  final String message;
  final List<File>? files;
  SendMessageEvent({
    required this.message,
    required this.files,
  });
}

class QuoteMessageEvent extends MessageEvent {
  final MessageModel? message;
  QuoteMessageEvent({required this.message});
}

class InsertMessageEvent extends MessageEvent {
  final MessageModel message;
  InsertMessageEvent({required this.message});
}

class InsertAutomaticMessageEvent extends MessageEvent {
  final MessageOfferModel message;
  final String conversationId;
  InsertAutomaticMessageEvent(
      {required this.message, required this.conversationId});
}

class RetrySendMessageEvent extends MessageEvent {
  final List<File?>? files;
  final MessageModel message;
  RetrySendMessageEvent({
    required this.files,
    required this.message,
  });
}

class PickEditMessageEvent extends MessageEvent {
  final MessageModel message;
  PickEditMessageEvent({required this.message});
}

class SendEditMessageEvent extends MessageEvent {
  final String message;
  SendEditMessageEvent({required this.message});
}

class SocketUpdateMessageEvent extends MessageEvent {
  final String messageId;
  final String data;
  SocketUpdateMessageEvent({required this.messageId, required this.data});
}

class SocketDeleteMessageEvent extends MessageEvent {
  final String messageId;
  SocketDeleteMessageEvent({required this.messageId});
}

class CancelEditMessageEvent extends MessageEvent {}

class SelectMessageEvent extends MessageEvent {
  final MessageModel? message;
  SelectMessageEvent({this.message});
}

class CopyMessageEvent extends MessageEvent {}

class ScrollToMessageEvent extends MessageEvent {
  final String parentMessageId;
  final ItemScrollController controller;
  final AnimationController animationController;

  ScrollToMessageEvent(
      {required this.parentMessageId,
      required this.controller,
      required this.animationController});
}

class DisposeMessageEvent extends MessageEvent {}

class CleanMessageEvent extends MessageEvent {}

class SearchMessageEvent extends MessageEvent {
  final String searchKey;
  SearchMessageEvent({required this.searchKey});
}

class ScrollMessageSearchEvent extends MessageEvent {
  final int index;
  final String messageId;
  final ItemScrollController itemScrollController;
  ScrollMessageSearchEvent({
    required this.index,
    required this.itemScrollController,
    required this.messageId,
  });
}
