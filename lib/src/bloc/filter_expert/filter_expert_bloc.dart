import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/data/remote_data_source/expert_repository.dart';
import 'package:askany/src/models/expert_model.dart';
import 'package:askany/src/models/search_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'filter_expert_event.dart';
part 'filter_expert_state.dart';

class FilterExpertBloc extends Bloc<FilterExpertEvent, FilterExpertState> {
  FilterExpertBloc() : super(FilterExpertInitial());

  List<ExpertModel> experts = [];
  bool isOverFilter = false;

  SearchModel? searchExpert;

  @override
  Stream<FilterExpertState> mapEventToState(FilterExpertEvent event) async* {
    if (event is GetMoreFilterExpertEvent) {
      if (!isOverFilter) {
        yield _gettingFilterExpert;
        await _filterExpert(searchExpert!);
        yield _getDoneFilterExpert;
      }
    }
    if (event is GetFilterExpertEvent) {
      experts.clear();
      isOverFilter = false;
      if (experts.isEmpty) {
        yield FilterExpertInitial();
      } else {
        yield _gettingFilterExpert;
      }

      if (!isOverFilter) {
        await _filterExpert(event.searchModel);
      }

      yield _getDoneFilterExpert;
    }
    if (event is CleanFilterExpertEvent) {
      experts.clear();
      isOverFilter = false;
      searchExpert = null;
      _getDoneFilterExpert;
    }

    if (event is OnFilterExpertEvent) {
      AppNavigator.push(Routes.FILTER_EXPERT);
      experts.clear();
      isOverFilter = false;
      searchExpert = SearchModel(
        category: event.slug,
        specialty: searchExpert?.specialty ?? null,
        position: searchExpert?.position ?? null,
        province: searchExpert?.province ?? null,
        experienceYears: searchExpert?.experienceYears ?? null,
        companyName: searchExpert?.companyName ?? null,
        contactType: searchExpert?.contactType ?? null,
        cost: searchExpert?.cost ?? null,
        ratings: searchExpert?.ratings ?? null,
        sortType: searchExpert?.sortType ?? null,
      );
      if (experts.isEmpty) {
        yield FilterExpertInitial();
      } else {
        yield _gettingFilterExpert;
      }

      if (!isOverFilter) {
        await _filterExpert(searchExpert!);
      }

      yield _getDoneFilterExpert;
    }
  }

  //private

  GettingFilterExpert get _gettingFilterExpert =>
      GettingFilterExpert(experts: experts);
  GetDoneFilterExpert get _getDoneFilterExpert =>
      GetDoneFilterExpert(experts: experts);

  Future<void> _filterExpert(SearchModel searchModel) async {
    searchExpert = SearchModel(
      category: searchExpert?.category ?? null,
      specialty: searchModel.specialty ?? null,
      position: searchModel.position ?? null,
      province: searchModel.province ?? null,
      experienceYears: searchModel.experienceYears ?? null,
      companyName: searchModel.companyName ?? null,
      contactType: searchModel.contactType ?? null,
      cost: searchModel.cost ?? null,
      ratings: searchModel.ratings ?? null,
      sortType: searchModel.sortType ?? null,
    );

    List<ExpertModel> _experts = await ExpertRepository().filterExpert(
      skip: experts.length,
      searchModel: searchExpert!,
    );

    if (_experts.length < LIMIT_API_5) {
      isOverFilter = true;
    }
    experts.addAll(_experts);
  }
}
