part of 'filter_expert_bloc.dart';

@immutable
abstract class FilterExpertState {
  List<dynamic> get props => [];
}

class FilterExpertInitial extends FilterExpertState {}

class GettingFilterExpert extends FilterExpertState {
  final List<ExpertModel> experts;
  GettingFilterExpert({required this.experts});

  List get props => [experts];
}

class GetDoneFilterExpert extends FilterExpertState {
  final List<ExpertModel> experts;
  GetDoneFilterExpert({required this.experts});
  List get props => [experts];
}
