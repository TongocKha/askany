part of 'filter_expert_bloc.dart';

@immutable
abstract class FilterExpertEvent {}

class GetMoreFilterExpertEvent extends FilterExpertEvent {}

class GetFilterExpertEvent extends FilterExpertEvent {
  final SearchModel searchModel;
  GetFilterExpertEvent({required this.searchModel});
}

class OnFilterExpertEvent extends FilterExpertEvent {
  final String slug;
  OnFilterExpertEvent({required this.slug});
}

class CleanFilterExpertEvent extends FilterExpertEvent {}
