import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/data/remote_data_source/expert_repository.dart';
import 'package:askany/src/models/expert_model.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'recommend_expert_event.dart';
part 'recommend_expert_state.dart';

class RecommendExpertBloc
    extends Bloc<RecommendExpertEvent, RecommendExpertState> {
  RecommendExpertBloc() : super(RecommendExpertInitial());

  List<ExpertModel> recommendExperts = [];
  bool isOverRecommend = false;

  @override
  Stream<RecommendExpertState> mapEventToState(
      RecommendExpertEvent event) async* {
    if (event is GetListRecommendEvent) {
      if (recommendExperts.isNotEmpty) {
        yield RecommendExpertInitial();
      } else {
        await _getListRecommend(event.specialty);
      }
      yield _getDoneExpertRecommend;
    }

    if (event is GetListRecommendFinishEvent) {
      _cleanBloc();
      if (recommendExperts.isNotEmpty) {
        yield _gettingExpertRecommend;
      }

      if (!isOverRecommend) {
        await _getListRecommend(event.specialty);
      }

      yield _getDoneExpertRecommend;
    }
  }

  //private

  GettingExpertRecommend get _gettingExpertRecommend =>
      GettingExpertRecommend(experts: recommendExperts);
  GetDoneExpertRecommend get _getDoneExpertRecommend =>
      GetDoneExpertRecommend(experts: recommendExperts);

  Future<void> _getListRecommend(String specialty) async {
    List<ExpertModel> _experts = await ExpertRepository().getListRecommend(
      specialty: specialty,
      skip: recommendExperts.length,
    );

    if (_experts.length < LIMIT_API_5) {
      isOverRecommend = true;
    }
    recommendExperts.addAll(_experts);
  }

  _cleanBloc() {
    isOverRecommend = false;
    recommendExperts.clear();
  }
}
