part of 'recommend_expert_bloc.dart';

@immutable
abstract class RecommendExpertState {
  List get props => [];
}

class RecommendExpertInitial extends RecommendExpertState {}

class GettingExpertRecommend extends RecommendExpertState {
  final List<ExpertModel> experts;
  GettingExpertRecommend({required this.experts});

  @override
  List get props => [experts];
}

class GetDoneExpertRecommend extends RecommendExpertState {
  final List<ExpertModel> experts;
  GetDoneExpertRecommend({required this.experts});

  @override
  List get props => [experts];
}
