part of 'recommend_expert_bloc.dart';

@immutable
abstract class RecommendExpertEvent {}

class GetListRecommendEvent extends RecommendExpertEvent {
  final String specialty;
  GetListRecommendEvent({required this.specialty});
}

class GetListRecommendFinishEvent extends RecommendExpertEvent {
  final String specialty;
  GetListRecommendFinishEvent({required this.specialty});
}
