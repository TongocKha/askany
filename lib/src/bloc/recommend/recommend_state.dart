part of 'recommend_bloc.dart';

@immutable
abstract class RecommendState {
  List get props => [[]];
}

class RecommendInitial extends RecommendState {}

class GettingExpertRecommendHome extends RecommendState {
  final List<ExpertModel> experts;
  GettingExpertRecommendHome({required this.experts});

  @override
  List get props => [experts];
}

class GetDoneExpertRecommendHome extends RecommendState {
  final List<ExpertModel> experts;
  GetDoneExpertRecommendHome({required this.experts});

  @override
  List get props => [experts];
}
