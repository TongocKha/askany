import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/data/remote_data_source/expert_repository.dart';
import 'package:askany/src/models/expert_model.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'recommend_event.dart';
part 'recommend_state.dart';

class RecommendBloc extends Bloc<RecommendEvent, RecommendState> {
  RecommendBloc() : super(RecommendInitial());

  List<ExpertModel> recommendExperts = [];
  bool isOverRecommend = false;

  @override
  Stream<RecommendState> mapEventToState(RecommendEvent event) async* {
    if (event is GetListRecommendHomeEvent) {
      if (recommendExperts.isNotEmpty) {
        yield RecommendInitial();
      } else {
        await _getListRecommend(event.specialty);
      }
      yield _getDoneExpertRecommend;
    }

    if (event is GetListRecommendFinishHomeEvent) {
      if (recommendExperts.isNotEmpty) {
        yield _gettingExpertRecommend;
      }

      if (!isOverRecommend) {
        await _getListRecommend(event.specialty);
      }

      yield _getDoneExpertRecommend;
    }

    if (event is CleanRecommendEvent) {
      _cleanBloc();
      yield _getDoneExpertRecommend;
    }
  }

  GettingExpertRecommendHome get _gettingExpertRecommend =>
      GettingExpertRecommendHome(experts: recommendExperts);
  GetDoneExpertRecommendHome get _getDoneExpertRecommend =>
      GetDoneExpertRecommendHome(experts: recommendExperts);

  Future<void> _getListRecommend(String specialty) async {
    List<ExpertModel> _experts = await ExpertRepository().getListRecommend(
      specialty: specialty,
      skip: recommendExperts.length,
    );

    if (_experts.length < LIMIT_API_5) {
      isOverRecommend = true;
    }
    recommendExperts.addAll(_experts);
  }

  _cleanBloc() {
    isOverRecommend = false;
    recommendExperts.clear();
  }
}
