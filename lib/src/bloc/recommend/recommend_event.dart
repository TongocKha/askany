part of 'recommend_bloc.dart';

@immutable
abstract class RecommendEvent {}

class GetListRecommendHomeEvent extends RecommendEvent {
  final String specialty;
  GetListRecommendHomeEvent({required this.specialty});
}

class GetListRecommendFinishHomeEvent extends RecommendEvent {
  final String specialty;
  GetListRecommendFinishHomeEvent({required this.specialty});
}

class CleanRecommendEvent extends RecommendEvent {}
