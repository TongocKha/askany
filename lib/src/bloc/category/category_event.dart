part of 'category_bloc.dart';

@immutable
abstract class CategoryEvent {}

class OnCategoriesEvent extends CategoryEvent {}

class GetCategoriesInfoEvent extends CategoryEvent {}

class CleanCategoriesInfoEvent extends CategoryEvent {}

class GetCategoriesParentEvent extends CategoryEvent {
  final CategoryInfoModel slug;
  GetCategoriesParentEvent({required this.slug});
}

class CleanCategoriesParentEvent extends CategoryEvent {}

class RefreshCategoriesEvent extends CategoryEvent {}

class GetCategoryInfoEvent extends CategoryEvent {
  final String categoryId;
  GetCategoryInfoEvent({required this.categoryId});
}
