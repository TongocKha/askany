import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/home/home_bloc.dart';
import 'package:askany/src/bloc/request/request_bloc.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/data/remote_data_source/category_repository.dart';
import 'package:askany/src/models/category_model.dart';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'category_event.dart';
part 'category_state.dart';

class CategoryBloc extends Bloc<CategoryEvent, CategoryState> {
  CategoryBloc() : super(CategoryInitial());
  List<CategoryInfoModel> categoriesList = [];
  bool isOverInfo = false;

  @override
  Stream<CategoryState> mapEventToState(CategoryEvent event) async* {
    if (event is OnCategoriesEvent) {
      if (!isOverInfo) {
        if (categoriesList.isEmpty) {
          yield CategoryInitial();

          await getCategoryList();
        }
      }
      yield _getDoneCategoryInfo;
    }

    if (event is GetCategoriesInfoEvent) {
      if (!isOverInfo) {
        if (categoriesList.isEmpty) {
          yield CategoryInitial();
        } else {
          yield _gettingCategoryInfo;
        }
        await getCategoryList();

        yield _getDoneCategoryInfo;
      }
    }
    if (event is RefreshCategoriesEvent) {
      yield CategoryInitial();
      _cleanBloc();
      await getCategoryList();
      yield _getDoneCategoryInfo;
    }

    if (event is CleanCategoriesInfoEvent) {
      _cleanBloc();
      yield CategoryInitial();
    }

    if (event is GetCategoryInfoEvent) {
      int indexCategoryInfo = -1;
      int indexOf = -1;

      categoriesList.asMap().forEach((index, categoryInfo) {
        indexOf = categoryInfo.listSubCates
            .indexWhere((category) => category.id == event.categoryId);

        if (indexOf != -1) {
          indexCategoryInfo = index;
        }
      });

      if (indexCategoryInfo != -1 && indexOf != -1) {
        AppBloc.requestBloc.add(
          FilterRequestEvent(
            searchKey: '',
            cost: '$MIN_MONEY - $MAX_MONEY',
            category: categoriesList[indexCategoryInfo].listSubCates[indexOf],
          ),
        );
        AppBloc.homeBloc.add(OnChangeIndexEvent(index: 1));
      }
    }
    yield _getDoneCategoryInfo;
  }

  // MARK: Private methods
  GettingCategoryInfo get _gettingCategoryInfo =>
      GettingCategoryInfo(categories: categoriesList);
  GetDoneCategoryInfo get _getDoneCategoryInfo =>
      GetDoneCategoryInfo(categories: categoriesList);

  Future<void> getCategoryList() async {
    List<CategoryInfoModel> _categories =
        await CategoryRepository().getCategories();
    if (_categories.length < LIMIT_API_5) {
      isOverInfo = true;
    }
    categoriesList.addAll(_categories);
  }

  _cleanBloc() {
    categoriesList.clear();
    isOverInfo = false;
  }
}
