part of 'category_bloc.dart';

@immutable
abstract class CategoryState {
  List get props => [];
}

class CategoryInitial extends CategoryState {
  List get props => [[]];
}

class GettingCategoryInfo extends CategoryState {
  final List<CategoryInfoModel> categories;
  GettingCategoryInfo({required this.categories});
  List get props => [categories];
}

class GetDoneCategoryInfo extends CategoryState {
  final List<CategoryInfoModel> categories;
  GetDoneCategoryInfo({required this.categories});
  List get props => [categories];
}
