import 'package:askany/src/models/social_model.dart';

abstract class AuthEvent {}

class OnAuthCheck extends AuthEvent {}

class OnAuthProcess extends AuthEvent {
  final String username;
  final String password;
  OnAuthProcess({required this.username, required this.password});
}

class RegisterEvent extends AuthEvent {
  final String email;
  final String password;
  final int province;
  final String fullname;
  final String phone;
  RegisterEvent({
    required this.email,
    required this.password,
    required this.province,
    required this.fullname,
    required this.phone,
  });
}

class LoginEvent extends AuthEvent {
  final bool isRemember;
  final bool isExpert;
  final String email;
  final String password;
  LoginEvent({
    required this.email,
    required this.password,
    required this.isRemember,
    required this.isExpert,
  });
}

class LoginWithSocialEvent extends AuthEvent {
  final SocialModel social;
  final bool isExpert;
  LoginWithSocialEvent({required this.social, required this.isExpert});
}

class ChangePasswordEvent extends AuthEvent {
  final String password;
  final String newPassword;
  ChangePasswordEvent({required this.password, required this.newPassword});
}

class UpdateEmailEvent extends AuthEvent {
  final String email;
  UpdateEmailEvent({required this.email});
}

class UpdatePhoneEvent extends AuthEvent {
  final String phone;
  UpdatePhoneEvent({required this.phone});
}

class VerifyOtpEvent extends AuthEvent {
  final String phone;
  final String code;
  VerifyOtpEvent({required this.phone, required this.code});
}

class VerifyPhoneEvent extends AuthEvent {
  VerifyPhoneEvent();
}

class VerifyCompanyEvent extends AuthEvent {
  final String linkedinURL;
  final String images;
  VerifyCompanyEvent({required this.linkedinURL, required this.images});
}

class RefreshTokenEvent extends AuthEvent {}

class ForgotPasswordEvent extends AuthEvent {
  final String email;
  ForgotPasswordEvent({required this.email});
}

class LogOutEvent extends AuthEvent {}

// class CheckChangePassWordSuccess extends AuthEvent{
//   late final bool isChangePassWordSuccess;
// }
