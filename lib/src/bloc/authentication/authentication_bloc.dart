import 'dart:async';
import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/authentication/bloc.dart';
import 'package:askany/src/bloc/user/user_bloc.dart';
import 'package:askany/src/configs/application.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/data/remote_data_source/auth_repository.dart';
import 'package:askany/src/data/remote_data_source/user_repository.dart';
import 'package:askany/src/models/account_model.dart';
import 'package:askany/src/models/enums/authentication_fail.dart';
import 'package:askany/src/models/expert_model.dart';
import 'package:askany/src/models/slide_mode.dart';
import 'package:askany/src/routes/app_navigator_observer.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/services/socket/socket.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_confirm_cancel.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_notify_auth.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_with_image_and_text.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_with_text_and_pop_button.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:bloc/bloc.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  AuthBloc() : super(InitialAuthenticationState());

  final application = Application();

  @override
  Stream<AuthState> mapEventToState(event) async* {
    if (event is OnAuthCheck) {
      bool isLogin = _onAuthCheck();
      if (isLogin) {
        yield _getAuthenticationSuccess();
      } else {
        yield AuthenticationFail();
      }
    }

    if (event is LoginEvent) {
      try {
        bool isSuccess = await _handleLogin(event);
        if (isSuccess) {
          if (event.isRemember) {
            UserLocal().saveAccountRemember(event.email, event.password, event.isExpert);
          } else {
            UserLocal().deleteAccountRemember(event.email);
          }

          AppNavigator.popUntil(Routes.ROOT);
          yield _getAuthenticationSuccess();
        } else {
          yield AuthenticationFail();
        }
      } catch (exception) {
        if (exception is AuthenticationException) {
          AuthenticationException authException = exception;
          String? description = authException.description;

          AppNavigator.pop();

          if (authException.index == 2) {
            UserLocal().deleteAccountRemember(event.email);
          }

          if (description != null) {
            if (authException == AuthenticationException.NOT_REGISTRATION_EXPERT) {
              UserLocal().clearAccessToken();
              dialogAnimationWrapper(
                borderRadius: 10.sp,
                slideFrom: SlideMode.bot,
                child: DialogConfirmCancel(
                  bodyBefore: description,
                  confirmText: Strings.registerNow.i18n,
                  onConfirmed: () async {
                    showDialogLoading();
                    ExpertModel? _expert = await _handleStartRegisterExpert();
                    AppNavigator.pop();
                    AppNavigator.push(Routes.REGISTER_EXPERT, arguments: {
                      'expertModel': _expert,
                    });
                    // UrlLauncherHelper.launchUrl(URL_REGISTER_USER);
                  },
                ),
              );
            } else if (authException == AuthenticationException.UNCONFIRMED_ACCOUNT) {
              showGeneralDialog(
                context: AppNavigator.context!,
                barrierDismissible: false,
                barrierColor: Colors.white,
                pageBuilder: (context, animation, secondaryAnimation) {
                  return DialogNotifyAuth(
                    email: event.email,
                    buttonTitle: Strings.underStood.i18n,
                    content: Strings.verifyEmailNoti.i18n,
                    title2: Strings.verifyEmailNoti2.i18n,
                    title1: Strings.pleaseAccess.i18n,
                    image: null,
                  );
                },
              );
            } else {
              dialogAnimationWrapper(
                borderRadius: 10.sp,
                slideFrom: SlideMode.bot,
                child: DialogWithTextAndPopButton(
                  title: Strings.loginFailed.i18n,
                  bodyBefore: description,
                  bodyAlign: TextAlign.center,
                ),
              );
            }
          }
        }
      }
    }

    if (event is LoginWithSocialEvent) {
      try {
        bool isSuccess = await _handleLoginWithSocial(event);
        if (isSuccess) {
          AppNavigator.popUntil(Routes.ROOT);
          yield _getAuthenticationSuccess();
        } else {
          yield AuthenticationFail();
        }
      } catch (exception) {
        if (exception is AuthenticationException) {
          AuthenticationException authException = exception;
          String? description = authException.description;

          AppNavigator.pop();

          if (description != null) {
            if (authException == AuthenticationException.NOT_REGISTRATION_EXPERT) {
              UserLocal().clearAccessToken();
              dialogAnimationWrapper(
                borderRadius: 10.sp,
                slideFrom: SlideMode.bot,
                child: DialogConfirmCancel(
                  bodyBefore: description,
                  confirmText: 'Đăng kí ngay',
                  onConfirmed: () async {
                    showDialogLoading();
                    ExpertModel? _expert = await _handleStartRegisterExpert();
                    AppNavigator.pop();
                    AppNavigator.push(Routes.REGISTER_EXPERT, arguments: {
                      'expertModel': _expert,
                    });
                    // UrlLauncherHelper.launchUrl(URL_REGISTER_USER);
                  },
                ),
              );
            }
          }
        }
      }
    }

    if (event is RegisterEvent) {
      try {
        bool isSuccess = await _handleRegister(event);
        AppNavigator.pop();
        if (isSuccess) {
          AppNavigator.popUntil(Routes.ROOT);
          await showGeneralDialog(
            context: AppNavigator.context!,
            barrierDismissible: false,
            barrierColor: Colors.white,
            pageBuilder: (context, animation, secondaryAnimation) {
              return DialogNotifyAuth(
                email: event.email,
                buttonTitle: 'Hoàn thành',
                content: 'Chúc mừng bạn đã đăng ký thành công',
                title2:
                    ' và làm theo hướng dẫn để xác thực email. (Lưu ý kiểm tra mục Spam nếu không nhận được email xác thực)',
                title1: 'Vui lòng truy cập ',
                image: Image.asset(
                  imageRegisterSuccess,
                  width: 80.sp,
                  height: 80.sp,
                ),
              );
            },
          );
        }
      } catch (exception) {
        if (exception is AuthenticationException) {
          AuthenticationException authException = exception;
          String? description = authException.description;

          AppNavigator.pop();

          if (description != null) {
            if (authException == AuthenticationException.EMAIL_EXISTS) {
              dialogAnimationWrapper(
                borderRadius: 10.sp,
                slideFrom: SlideMode.bot,
                child: DialogWithTextAndPopButton(
                  title: 'Đăng ký thất bại',
                  bodyAfter:
                      'Email này đã được sử dụng để đăng ký tài khoản, hãy thử sử dụng email khác!',
                ),
              );
            } else if (authException == AuthenticationException.PHONE_EXISTS) {
              dialogAnimationWrapper(
                borderRadius: 10.sp,
                slideFrom: SlideMode.bot,
                child: DialogWithTextAndPopButton(
                  title: 'Đăng ký thất bại',
                  bodyAfter:
                      'Số điện thoại này đã được sử dụng để đăng ký tài khoản, hãy thử sử dụng số điện thoại khác!',
                ),
              );
            } else {
              dialogAnimationWrapper(
                borderRadius: 10.sp,
                slideFrom: SlideMode.bot,
                child: DialogWithTextAndPopButton(
                  title: Strings.errorOccurredNoti.i18n,
                  bodyAfter: Strings.errorOccurredTryAgain.i18n,
                ),
              );
            }
          }
        }
      }
    }

    if (event is ChangePasswordEvent) {
      bool isSuccess = await _handleChangePassword(event);
      AppNavigator.pop();
      if (isSuccess) {
        AppNavigator.pop();
      } else {
        dialogAnimationWrapper(
          slideFrom: SlideMode.bot,
          child: DialogWithTextAndPopButton(
            bodyAfter: 'Mật khẩu cũ không chính xác,\n vui lòng nhập lại!',
            bodyAlign: TextAlign.center,
          ),
        );
      }
    }

    if (event is UpdateEmailEvent) {
      bool isSuccess = await updateEmail(event);
      AppNavigator.pop();
      if (isSuccess) {
        AppNavigator.popUntil(Routes.ROOT);
        dialogAnimationWrapper(
          slideFrom: SlideMode.bot,
          paddingTop: 18.sp,
          child: DialogWithTextAndPopButton(
            title: 'Xác thực email',
            bodyBefore: 'Vui lòng truy cập',
            highlightText: event.email.toString(),
            bodyAfter:
                'và làm theo hướng dẫn để xác thực email. (Lưu ý kiểm tra mục Spam nếu không nhận được email xác thực)',
          ),
          borderRadius: 12.sp,
        );
      } else {
        dialogAnimationWrapper(
          borderRadius: 10.sp,
          slideFrom: SlideMode.bot,
          child: DialogWithTextAndPopButton(
            title: 'Email của bạn đã được sử dụng.',
            bodyBefore: 'Vui lòng chọn email khác!',
            bodyAlign: TextAlign.center,
          ),
        );
      }
    }

    if (event is UpdatePhoneEvent) {
      bool isSuccess = await updatePhone(event);
      if (isSuccess) {
      } else {
        await dialogAnimationWrapper(
          borderRadius: 10.sp,
          slideFrom: SlideMode.bot,
          child: DialogWithTextAndPopButton(
            title: 'Số điện thoại của bạn đã được sử dụng.',
            bodyBefore: 'Vui lòng chọn số điện thoại khác!',
            bodyAlign: TextAlign.center,
          ),
        );
        AppNavigator.pop();
      }
    }

    if (event is VerifyOtpEvent) {
      bool isSuccess = await verifyOTP(event);
      if (isSuccess) {
        await dialogAnimationWrapper(
          slideFrom: SlideMode.bot,
          paddingTop: 18.sp,
          child: DialogWithImageAndText(
            image: Image.asset(
              iconCheckSuccess,
              width: 35.sp,
            ),
            text: 'Bạn đã xác thực số điện thoại thành công!',
          ),
          borderRadius: 12.sp,
        );
        if (AppNavigatorObserver.routeNames.contains(Routes.EDIT_PROFILE)) {
          AppNavigator.popUntil(Routes.EDIT_PROFILE);
        } else {
          AppNavigator.pop();
          AppNavigator.pop();
        }
      } else {
        await dialogAnimationWrapper(
          slideFrom: SlideMode.bot,
          paddingTop: 18.sp,
          child: DialogWithImageAndText(
            image: Image.asset(
              iconCheckFaild,
              width: 35.sp,
            ),
            text: 'Bạn đã xác thực số điện thoại thất bại!',
          ),
          borderRadius: 12.sp,
        );
      }
    }

    if (event is VerifyPhoneEvent) {
      bool isSuccess = await verifyPhone(event);
      AppNavigator.pop();
      if (isSuccess) {
        AppNavigator.pop();
      } else {
        // Show dialog update phone fail
      }
    }

    if (event is VerifyCompanyEvent) {
      bool isSuccess = await verifyCompany(event);
      AppNavigator.pop();
      if (isSuccess) {
        AppNavigator.pop();
      } else {
        // Show dialog update phone fail
      }
    }

    if (event is RefreshTokenEvent) {
      bool isSuccess = await _handleRefreshToken(event);
      if (isSuccess) {
        yield _getAuthenticationSuccess();
      } else {
        await _handleLogOut();
        yield AuthenticationFail();
      }
    }

    if (event is ForgotPasswordEvent) {
      bool isSuccess = await _handleForgotPassword(event);
      AppNavigator.pop();
      if (isSuccess) {
        await dialogAnimationWrapper(
          slideFrom: SlideMode.bot,
          child: DialogWithTextAndPopButton(
            bodyAlign: TextAlign.center,
            bodyBefore:
                'Vui lòng kiểm tra hộp thư và làm theo hướng dẫn để thiết lập lại mật khẩu.',
            bodyFontSize: 13.sp,
            bodyColor: colorBlack1,
            padding: EdgeInsets.only(
              left: 24.sp,
              right: 24.sp,
              top: 22.sp,
              bottom: 18.sp,
            ),
          ),
          borderRadius: 10.sp,
        );
        AppNavigator.popUntil(Routes.AUTHENTICATION);
      } else {
        // Show dialog change password fail
      }
    }

    if (event is LogOutEvent) {
      await _handleLogOut();
      AppNavigator.popUntil(Routes.ROOT);
      yield AuthenticationFail();
    }
  }

  // Private methods
  AuthenticationSuccess _getAuthenticationSuccess() {
    if (state is! AuthenticationSuccess) {
      AppBloc.cleanBloc();
      AppBloc.initialHomeBloc();
    }
    return AuthenticationSuccess();
  }

  bool _onAuthCheck() {
    return UserLocal().getAccessToken() != '';
  }

  Future<bool> _handleLogin(LoginEvent event) async {
    AccountModel? user = await AuthRepository().login(
      email: event.email,
      password: event.password,
      isExpert: event.isExpert,
    );
    AppNavigator.pop();
    if (user != null) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> _handleLoginWithSocial(LoginWithSocialEvent event) async {
    AccountModel? user = await AuthRepository().registerSocial(
      social: event.social,
      isExpert: event.isExpert,
    );
    AppNavigator.pop();
    if (user != null) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> _handleRegister(RegisterEvent event) async {
    AccountModel? user = await AuthRepository().register(
      email: event.email,
      password: event.password,
      fullname: event.fullname,
      province: event.province,
      phone: event.phone,
    );

    return user != null;
  }

  Future<bool> _handleChangePassword(ChangePasswordEvent event) async {
    String email = '${AppBloc.userBloc.getAccount.email}';
    var isExpert = AppBloc.userBloc.getAccount.isExpert;
    bool isSucceed = await AuthRepository().changePassword(
      password: event.password,
      newPassword: event.newPassword,
    );
    if (isSucceed) {
      UserLocal().deleteAccountRemember(email);
      UserLocal().saveAccountRemember(email, event.newPassword, isExpert!);
    }
    return isSucceed;
  }

  Future<bool> updateEmail(UpdateEmailEvent event) async {
    bool isSucceed = await AuthRepository().updateEmail(
      email: event.email,
    );
    return isSucceed;
  }

  Future<bool> updatePhone(UpdatePhoneEvent event) async {
    bool isSucceed = event.phone == AppBloc.userBloc.getAccount.phone
        ? await AuthRepository().verifyPhone()
        : await AuthRepository().updatePhone(
            phone: event.phone,
          );

    return isSucceed;
  }

  Future<bool> verifyOTP(VerifyOtpEvent event) async {
    bool isSucceed = await AuthRepository().verifyOTP(phone: event.phone, code: event.code);

    if (isSucceed) {
      AppBloc.userBloc.add(UpdatePhoneUserEvent(phone: event.phone));
    }

    return isSucceed;
  }

  Future<bool> verifyPhone(VerifyPhoneEvent event) async {
    bool isSucceed = await AuthRepository().verifyPhone();
    return isSucceed;
  }

  Future<bool> verifyCompany(VerifyCompanyEvent event) async {
    bool isSucceed =
        await AuthRepository().verifyCompany(linkedinURL: event.linkedinURL, images: event.images);
    return isSucceed;
  }

  Future<bool> _handleRefreshToken(RefreshTokenEvent event) async {
    bool isSucceed = await AuthRepository().refreshToken();
    return isSucceed;
  }

  Future<bool> _handleForgotPassword(ForgotPasswordEvent event) async {
    bool isSucceed = await AuthRepository().forgotPassword(
      email: event.email,
    );
    return isSucceed;
  }

  Future<ExpertModel?> _handleStartRegisterExpert() async {
    ExpertModel? _expert;

    await UserRepository().userRegisterExpert();

    // Process switch token to expert
    String? tokenExpert = await AuthRepository().switchToken();
    UserLocal().clearAccessToken();
    if (tokenExpert != null) {
      UserLocal().saveBackupToken(tokenExpert);
      List data = await UserRepository().getUserByToken();

      if (data[1] != null) {
        _expert = data[1];
      }
    }
    AppNavigator.pop();

    return _expert;
  }

  Future<void> _handleLogOut() async {
    AppBloc.cleanBloc();
    UserLocal().clearAccessToken();
    UserLocal().saveIsExpert(false);
    disconnectBeforeConnect();
    await FirebaseMessaging.instance.deleteToken();
  }
}
