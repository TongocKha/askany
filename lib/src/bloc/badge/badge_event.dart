part of 'badge_bloc.dart';

@immutable
abstract class BadgesEvent {}

class GetBadgesChatEvent extends BadgesEvent {}

class UpdateBadgesChatEvent extends BadgesEvent {
  final int extendValue;
  UpdateBadgesChatEvent({required this.extendValue});
}

class GetBadgesNotificationEvent extends BadgesEvent {
  final int badges;
  GetBadgesNotificationEvent({required this.badges});
}

class UpdateBadgesNotificationEvent extends BadgesEvent {
  final int extendValue;
  UpdateBadgesNotificationEvent({required this.extendValue});
}

class CleanBadgesEvent extends BadgesEvent {}
