import 'package:askany/src/data/remote_data_source/chat_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_app_badger/flutter_app_badger.dart';
import 'package:meta/meta.dart';

part 'badge_event.dart';
part 'badge_state.dart';

class BadgesBloc extends Bloc<BadgesEvent, BadgesState> {
  BadgesBloc() : super(BadgeInitial());

  int badgesChat = 0;
  int badgesNotification = 0;

  @override
  Stream<BadgesState> mapEventToState(BadgesEvent event) async* {
    if (event is GetBadgesChatEvent) {
      await _getBadgesChat();
      yield _badgesDone;
    }

    if (event is UpdateBadgesChatEvent) {
      badgesChat += event.extendValue;
      yield _badgesDone;
    }

    if (event is GetBadgesNotificationEvent) {
      badgesNotification = event.badges;
      yield _badgesDone;
    }

    if (event is UpdateBadgesNotificationEvent) {
      badgesNotification += event.extendValue;
      yield _badgesDone;
    }

    if (event is CleanBadgesEvent) {
      _clearBadges();
      yield BadgeInitial();
    }

    _updateBadgesSystem();
  }

  // MARK: Private methods
  GetBadgesDone get _badgesDone => GetBadgesDone(
        badgesChat: badgesChat,
        badgesNotification: badgesNotification,
      );

  Future<void> _getBadgesChat() async {
    int badges = await ChatRepository().getBadgesConversation();

    badgesChat = badges;
  }

  _updateBadgesSystem() {
    int totalBadges = badgesChat + badgesNotification;
    if (totalBadges == 0) {
      FlutterAppBadger.removeBadge();
    } else {
      FlutterAppBadger.updateBadgeCount(totalBadges);
    }
  }

  _clearBadges() {
    badgesChat = 0;
    badgesNotification = 0;
  }
}
