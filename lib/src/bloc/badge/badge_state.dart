part of 'badge_bloc.dart';

@immutable
abstract class BadgesState {}

class BadgeInitial extends BadgesState {}

class GetBadgesDone extends BadgesState {
  final int badgesChat;
  final int badgesNotification;
  GetBadgesDone({required this.badgesChat, required this.badgesNotification});
}
