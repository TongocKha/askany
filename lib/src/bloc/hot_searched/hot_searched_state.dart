part of 'hot_searched_bloc.dart';

@immutable
abstract class HotSearchedState {
  List get props => [[]];
}

class HotSearchedInitial extends HotSearchedState {
  List get props => [[]];
}

class GetDoneHotSearch extends HotSearchedState {
  final List<String> hotSearch;
  GetDoneHotSearch({required this.hotSearch});

  @override
  List get props => [hotSearch];
}
