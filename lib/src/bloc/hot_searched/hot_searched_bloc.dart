import 'package:askany/src/data/remote_data_source/search_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'hot_searched_event.dart';
part 'hot_searched_state.dart';

class HotSearchedBloc extends Bloc<HotSearchedEvent, HotSearchedState> {
  HotSearchedBloc() : super(HotSearchedInitial());
  List<String> hotSearchs = [];
  @override
  Stream<HotSearchedState> mapEventToState(HotSearchedEvent event) async* {
    if (event is GetHotSearchEvent) {
      if (hotSearchs.isEmpty) {
        await _getHotSearch();
        yield _getDoneHotSearch;
      }
    }

    if (event is RefreshHotSearchEvent) {
      yield HotSearchedInitial();
      await _getHotSearch();
      yield _getDoneHotSearch;
    }
  }

  // Private

  GetDoneHotSearch get _getDoneHotSearch =>
      GetDoneHotSearch(hotSearch: hotSearchs);

  Future<void> _getHotSearch() async {
    List<String> _hotSearch = await SearchRepository().getHotSearch();

    if (_hotSearch.isNotEmpty) {
      hotSearchs.addAll(_hotSearch);
    }
  }
}
