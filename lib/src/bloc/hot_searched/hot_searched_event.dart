part of 'hot_searched_bloc.dart';

@immutable
abstract class HotSearchedEvent {}

class GetHotSearchEvent extends HotSearchedEvent {}

class RefreshHotSearchEvent extends HotSearchedEvent {}
