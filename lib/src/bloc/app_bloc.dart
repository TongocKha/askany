import 'package:askany/src/bloc/app_state/app_state_bloc.dart';
import 'package:askany/src/bloc/application/application_bloc.dart';
import 'package:askany/src/bloc/authentication/bloc.dart';
import 'package:askany/src/bloc/badge/badge_bloc.dart';
import 'package:askany/src/bloc/calendar/calendar_bloc.dart';
import 'package:askany/src/bloc/category/category_bloc.dart';
import 'package:askany/src/bloc/category_home/category_home_bloc.dart';
import 'package:askany/src/bloc/category_info/category_info_bloc.dart';
import 'package:askany/src/bloc/chat/chat_bloc.dart';
import 'package:askany/src/bloc/content/content_bloc.dart';
import 'package:askany/src/bloc/discover/discover_bloc.dart';
import 'package:askany/src/bloc/experiences/experiences_bloc.dart';
import 'package:askany/src/bloc/expert/expert_bloc.dart';
import 'package:askany/src/bloc/expert_info/expert_info_bloc.dart';
import 'package:askany/src/bloc/filter_expert/filter_expert_bloc.dart';
import 'package:askany/src/bloc/home/home_bloc.dart';
import 'package:askany/src/bloc/hot_expert_home/hot_expert_home_bloc.dart';
import 'package:askany/src/bloc/hot_searched/hot_searched_bloc.dart';
import 'package:askany/src/bloc/message/message_bloc.dart';
import 'package:askany/src/bloc/message_images/message_images_bloc.dart';
import 'package:askany/src/bloc/notification/notification_bloc.dart';
import 'package:askany/src/bloc/payment_card/payment_card_bloc.dart';
import 'package:askany/src/bloc/photo_manager/photo_manager_bloc.dart';
import 'package:askany/src/bloc/rating/rating_bloc.dart';
import 'package:askany/src/bloc/recommend/recommend_bloc.dart';
import 'package:askany/src/bloc/recommend_expert/recommend_expert_bloc.dart';
import 'package:askany/src/bloc/recommend_skill/recommend_skill_bloc.dart';
import 'package:askany/src/bloc/register_expert/register_expert_bloc.dart';
import 'package:askany/src/bloc/request/request_bloc.dart';
import 'package:askany/src/bloc/request_info/request_info_bloc.dart';
import 'package:askany/src/bloc/service_managament/service_management_bloc.dart';
import 'package:askany/src/bloc/skill/skill_bloc.dart';
import 'package:askany/src/bloc/skill_info/skill_info_bloc.dart';
import 'package:askany/src/bloc/timer/timer_bloc.dart';
import 'package:askany/src/bloc/transaction/transaction_bloc.dart';
import 'package:askany/src/bloc/user/user_bloc.dart';
import 'package:askany/src/bloc/video_call/video_call_bloc.dart';
import 'package:askany/src/bloc/video_player/video_player_bloc.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/services/socket/socket.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AppBloc {
  static final homeBloc = HomeBloc();
  static final appStateBloc = AppStateBloc();
  static final applicationBloc = ApplicationBloc();
  static final authBloc = AuthBloc();
  static final videoCallBloc = VideoCallBloc();
  static final chatBloc = ChatBloc();
  static final messageBloc = MessageBloc();
  static final userBloc = UserBloc();
  static final requestBloc = RequestBloc();
  static final skillBloc = SkillBloc();
  static final timerBloc = TimerBloc();
  static final badgesBloc = BadgesBloc();
  static final notificationBloc = NotificationBloc();
  static final transactionBloc = TransactionBloc();
  static final expertBloc = ExpertBloc();
  static final videoPlayerBloc = VideoPlayerBloc();
  static final serviceManagamentBloc = ServiceManagamentBloc();
  static final categoryBloc = CategoryBloc();
  static final ratingBloc = RatingBloc();
  static final contentBloc = ContentBloc();
  static final recommendHomeBloc = RecommendBloc();
  static final expertInfoBloc = ExpertInfoBloc();
  static final experienceBloc = ExperiencesBloc();
  static final registerExpertBloc = RegisterExpertBloc();
  static final categoryHomeBloc = CategoryHomeBloc();
  static final discoverBloc = DiscoverBloc();
  static final filterExpertBloc = FilterExpertBloc();
  static final recommendExpertBloc = RecommendExpertBloc();
  static final recommendSkillBloc = RecommendSkillBloc();
  static final hotExpertHomeBloc = HotExpertHomeBloc();
  static final hotSearchBloc = HotSearchedBloc();
  // static final singleServiceManagamentBloc = SingleServiceManagamentBloc();
  static final paymentCard = PaymentCardBloc();
  static final calendarBloc = CalendarBloc();
  static final photoManagerBloc = PhotoManagerBloc();
  static final skillInfoBloc = SkillInfoBloc();
  static final requestInfoBloc = RequestInfoBloc();
  static final categoryInfoBloc = CategoryInfoBloc();
  static final messageImageBloc = MessageImagesBloc();
  static final List<BlocProvider> providers = [
    BlocProvider<HomeBloc>(
      create: (context) => homeBloc,
    ),
    BlocProvider<AppStateBloc>(
      create: (context) => appStateBloc,
    ),
    BlocProvider<ApplicationBloc>(
      create: (context) => applicationBloc,
    ),
    BlocProvider<AuthBloc>(
      create: (context) => authBloc,
    ),
    BlocProvider<VideoCallBloc>(
      create: (context) => videoCallBloc,
    ),
    BlocProvider<UserBloc>(
      create: (context) => userBloc,
    ),
    BlocProvider<RequestBloc>(
      create: (context) => requestBloc,
    ),
    BlocProvider<SkillBloc>(
      create: (context) => skillBloc,
    ),
    BlocProvider<TimerBloc>(
      create: (context) => timerBloc,
    ),
    BlocProvider<ChatBloc>(
      create: (context) => chatBloc,
    ),
    BlocProvider<MessageBloc>(
      create: (context) => messageBloc,
    ),
    BlocProvider<BadgesBloc>(
      create: (context) => badgesBloc,
    ),
    BlocProvider<NotificationBloc>(
      create: (context) => notificationBloc,
    ),
    BlocProvider<TransactionBloc>(
      create: (context) => transactionBloc,
    ),
    BlocProvider<ExpertBloc>(
      create: (context) => expertBloc..add(GetExpertEvent()),
    ),
    BlocProvider<VideoPlayerBloc>(
      create: (context) => videoPlayerBloc,
    ),
    BlocProvider<ServiceManagamentBloc>(
      create: (context) => serviceManagamentBloc,
    ),
    BlocProvider<CategoryBloc>(
      create: (context) => categoryBloc,
    ),
    BlocProvider<RatingBloc>(
      create: (context) => ratingBloc,
    ),
    BlocProvider<ContentBloc>(
      create: (context) => contentBloc,
    ),
    BlocProvider<RecommendBloc>(
      create: (context) => recommendHomeBloc,
    ),
    BlocProvider<ExpertInfoBloc>(
      create: (context) => expertInfoBloc,
    ),
    BlocProvider<ExperiencesBloc>(
      create: (context) => experienceBloc,
    ),
    BlocProvider<RegisterExpertBloc>(
      create: (context) => registerExpertBloc,
    ),
    BlocProvider<CategoryHomeBloc>(
      create: (context) => categoryHomeBloc,
    ),
    BlocProvider<DiscoverBloc>(
      create: (context) => discoverBloc,
    ),
    BlocProvider<FilterExpertBloc>(
      create: (context) => filterExpertBloc,
    ),
    BlocProvider<RecommendExpertBloc>(
      create: (context) => recommendExpertBloc,
    ),
    BlocProvider<RecommendSkillBloc>(
      create: (context) => recommendSkillBloc,
    ),
    BlocProvider<HotExpertHomeBloc>(
      create: (context) => hotExpertHomeBloc,
    ),
    BlocProvider<HotSearchedBloc>(
      create: (context) => hotSearchBloc,
    ),
    // BlocProvider<SingleServiceManagamentBloc>(
    //   create: (context) => singleServiceManagamentBloc,
    // ),
    BlocProvider<PaymentCardBloc>(
      create: (context) => paymentCard,
    ),
    BlocProvider<CalendarBloc>(
      create: (context) => calendarBloc,
    ),
    BlocProvider<PhotoManagerBloc>(
      create: (context) => photoManagerBloc,
    ),
    BlocProvider<SkillInfoBloc>(
      create: (context) => skillInfoBloc,
    ),
    BlocProvider<RequestInfoBloc>(
      create: (context) => requestInfoBloc,
    ),
    BlocProvider<CategoryInfoBloc>(
      create: (context) => categoryInfoBloc,
    ),
    BlocProvider<MessageImagesBloc>(
      create: (context) => messageImageBloc,
    ),
  ];

  static void dispose() {
    appStateBloc.close();
    applicationBloc.close();
    authBloc.close();
    videoCallBloc.close();
    userBloc.close();
    requestBloc.close();
    skillBloc.close();
    timerBloc.close();
    chatBloc.close();
    messageBloc.close();
    badgesBloc.close();
    notificationBloc.close();
    transactionBloc.close();
    expertBloc.close();
    videoPlayerBloc.close();
    serviceManagamentBloc.close();
    categoryBloc.close();
    ratingBloc.close();
    homeBloc.close();
    contentBloc.close();
    recommendHomeBloc.close();
    expertInfoBloc.close();
    registerExpertBloc.close();
    categoryHomeBloc.close();
    discoverBloc.close();
    filterExpertBloc.close();
    recommendExpertBloc.close();
    recommendSkillBloc.close();
    hotExpertHomeBloc.close();
    hotSearchBloc.close();
    paymentCard.close();
    calendarBloc.close();
    photoManagerBloc.close();
    skillInfoBloc.close();
    requestInfoBloc.close();
    categoryInfoBloc.close();
    messageImageBloc.close();
  }

  static void initialHomeBloc() {
    initialHomeBlocWithoutAuth();
    if (UserLocal().getAccessToken() != '') {
      connectAndListen();
      initialHomeBlocWithAuth();
    }
  }

  static void initialHomeBlocWithoutAuth() {
    hotSearchBloc.add(GetHotSearchEvent());
    categoryBloc.add(OnCategoriesEvent());
    categoryHomeBloc.add(OnCategoryHomeEvent());
    discoverBloc.add(OnDiscoverEvent());
    hotExpertHomeBloc.add(GetHotExpertEvent());
  }

  static void initialHomeBlocWithAuth() {
    authBloc.add(RefreshTokenEvent());
    userBloc.add(GetUserInfo());
    badgesBloc.add(GetBadgesChatEvent());
    chatBloc.add(OnChatEvent());
    notificationBloc.add(GetNotificationEvent());
    serviceManagamentBloc.add(GetCancelServiceReasonsEvent());
    hotSearchBloc.add(GetHotSearchEvent());
    paymentCard.add(OnPaymentCardEvent());
    calendarBloc..add(GetCalendarEvent());
  }

  static void cleanBloc() {
    userBloc.add(CleanUserEvent());
    videoCallBloc.add(EndVideoCallEvent());
    requestBloc.add(CleanRequestEvent());
    skillBloc.add(CleanSkillEvent());
    chatBloc.add(CleanChatEvent());
    messageBloc.add(CleanMessageEvent());
    messageImageBloc.add(CleanMessagesImagesEvent());
    badgesBloc.add(CleanBadgesEvent());
    notificationBloc.add(CleanNotificationEvent());
    transactionBloc.add(CleanTransactionEvent());
    serviceManagamentBloc.add(CleanServiceManagamentEvent());
    homeBloc.add(OnChangeIndexEvent());
    contentBloc.add(CleanContentEvent());
    ratingBloc.add(CleanRatingExpertEvent());
    experienceBloc.add(CleanExperienceEvent());
    filterExpertBloc.add(CleanFilterExpertEvent());
    paymentCard.add(CleanPaymentCardEvent());
    requestInfoBloc.add(ClearRequestInfoEvent());
  }

  ///Singleton factory
  static final AppBloc _instance = AppBloc._internal();

  factory AppBloc() {
    return _instance;
  }

  AppBloc._internal();
}
