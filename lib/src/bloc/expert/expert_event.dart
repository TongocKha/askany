part of 'expert_bloc.dart';

@immutable
abstract class ExpertEvent {}

class GetExpertEvent extends ExpertEvent {}

class OnExpertCategoryEvent extends ExpertEvent {
  final String slug;
  OnExpertCategoryEvent({required this.slug});
}

class GetExpertCategoryEvent extends ExpertEvent {
  final String slug;
  GetExpertCategoryEvent({required this.slug});
}

class RefreshExpertCategoryEvent extends ExpertEvent {}
