part of 'expert_bloc.dart';

@immutable
abstract class ExpertState {
  /// 0: List<ExpertModel>
  List get props => [];
}

class ExpertInitial extends ExpertState {
  @override
  List get props => [[]];
}

class GettingExpert extends ExpertState {
  final List<ExpertModel> experts;
  GettingExpert({required this.experts});

  @override
  List get props => [experts];
}

class GetDoneExpert extends ExpertState {
  final List<ExpertModel> experts;
  GetDoneExpert({required this.experts});

  @override
  List get props => [experts];
}
