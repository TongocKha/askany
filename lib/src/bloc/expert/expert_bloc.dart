import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/data/remote_data_source/user_repository.dart';
import 'package:askany/src/models/expert_model.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'expert_event.dart';
part 'expert_state.dart';

class ExpertBloc extends Bloc<ExpertEvent, ExpertState> {
  ExpertBloc() : super(ExpertInitial());

  List<ExpertModel> experts = [];
  bool isOver = false;

  @override
  Stream<ExpertState> mapEventToState(ExpertEvent event) async* {
    if (event is GetExpertEvent) {
      if (!isOver) {
        if (experts.isEmpty) {
          yield ExpertInitial();
        } else {
          yield _gettingExpert;
        }

        await _getExperts();

        yield _getDoneExpert;
      }
    }
    if (event is RefreshExpertCategoryEvent) {
      yield ExpertInitial();
      _cleanBloc();
      await _getExperts();
      yield _getDoneExpert;
    }
  }

  // MARK: Private methods
  GettingExpert get _gettingExpert => GettingExpert(experts: experts);
  GetDoneExpert get _getDoneExpert => GetDoneExpert(experts: experts);

  Future<void> _getExperts() async {
    List<ExpertModel> _experts = await UserRepository().getExperts(skip: experts.length);

    if (_experts.length < LIMIT_API_5) {
      isOver = true;
    }

    experts.addAll(_experts);
  }

  _cleanBloc() {
    experts.clear();
    isOver = false;
  }
}
