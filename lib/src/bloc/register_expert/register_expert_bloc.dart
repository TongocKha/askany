import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/data/remote_data_source/user_repository.dart';
import 'package:askany/src/models/expert_model.dart';
import 'package:askany/src/models/slide_mode.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_with_text_and_pop_button.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'register_expert_event.dart';
part 'register_expert_state.dart';

class RegisterExpertBloc
    extends Bloc<RegisterExpertEvent, RegisterExpertState> {
  RegisterExpertBloc() : super(RegisterExpertInitial());

  @override
  Stream<RegisterExpertState> mapEventToState(
      RegisterExpertEvent event) async* {
    if (event is UpdateRegisterStep1Event) {
      await _updateInfoExpert(event);
    }

    if (event is UpdateRegisterStep3Event) {
      await _updateServicePrice(event);
    }
  }

  // MARK: Private methods
  Future<void> _updateInfoExpert(UpdateRegisterStep1Event event) async {
    bool _isUpdateSucceed = await UserRepository().updateInfoExpert(
      languages: event.expert.languages!.map((e) => e.id).toList(),
      specialties: event.expert.specialties.map((e) => e.id).toList(),
      experienceYears: event.expert.experienceYears.toString(),
      positionId: event.expert.highestPosition,
      companyName: event.expert.companyName,
      introduce: event.expert.introduce,
    );

    AppNavigator.pop();

    if (_isUpdateSucceed) {
      event.handleFinished();
    } else {
      dialogAnimationWrapper(
        slideFrom: SlideMode.bot,
        child: DialogWithTextAndPopButton(
          title: Strings.errorOccurredNoti.i18n,
          bodyAfter: Strings.errorOccurredTryAgain.i18n,
        ),
      );
    }
  }

  Future<void> _updateServicePrice(UpdateRegisterStep3Event event) async {
    bool _isUpdateSucceed = await UserRepository().updateSericePrice(
      callPrice: event.expert.callPrice!,
      meetPrice: event.expert.meetPrice!,
      timelines: event.expert.timelinePriceModel!,
    );

    AppNavigator.pop();

    if (_isUpdateSucceed) {
      event.handleFinished();
    } else {
      dialogAnimationWrapper(
        slideFrom: SlideMode.bot,
        child: DialogWithTextAndPopButton(
          title: Strings.errorOccurredNoti.i18n,
          bodyAfter: Strings.errorOccurredTryAgain.i18n,
        ),
      );
    }
  }
}
