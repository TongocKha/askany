part of 'register_expert_bloc.dart';

@immutable
abstract class RegisterExpertEvent {}

class UpdateRegisterStep1Event extends RegisterExpertEvent {
  final ExpertModel expert;
  final Function handleFinished;
  UpdateRegisterStep1Event({
    required this.expert,
    required this.handleFinished,
  });
}

class UpdateRegisterStep3Event extends RegisterExpertEvent {
  final ExpertModel expert;
  final Function handleFinished;
  UpdateRegisterStep3Event({
    required this.expert,
    required this.handleFinished,
  });
}
