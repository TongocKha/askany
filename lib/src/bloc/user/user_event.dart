part of 'user_bloc.dart';

@immutable
abstract class UserEvent {}

class UpdateInfoUserEvent extends UserEvent {
  final String province;
  final String fullname;
  final File? avatar;
  UpdateInfoUserEvent({
    required this.province,
    required this.fullname,
    this.avatar,
  });
}

class UpdateInfoExpertEvent extends UserEvent {
  final List<LanguageModel> languagues;
  final List<SpecialtyModel> specialties;
  final String experienceYears;
  final String positionId;
  final String companyName;
  final String introduce;
  UpdateInfoExpertEvent({
    required this.languagues,
    required this.specialties,
    required this.experienceYears,
    required this.positionId,
    required this.companyName,
    required this.introduce,
  });
}

class UpdateServicePriceEvent extends UserEvent {
  final BudgetModel callPrice;
  final BudgetModel meetPrice;
  final List<TimelinePriceModel> timelines;
  UpdateServicePriceEvent({
    required this.callPrice,
    required this.meetPrice,
    required this.timelines,
  });
}

class GetUserInfo extends UserEvent {}

class UpdateUserWalletEvent extends UserEvent {
  final double updatedWallet;

  UpdateUserWalletEvent({required this.updatedWallet});
}

class UpdatePhoneUserEvent extends UserEvent {
  final String phone;
  UpdatePhoneUserEvent({required this.phone});
}

class UpdateEmailUserEvent extends UserEvent {
  final String email;
  UpdateEmailUserEvent({required this.email});
}

class GetLanguagesEvent extends UserEvent {}

class BlockExpertEvent extends UserEvent {
  final String expertId;
  BlockExpertEvent({required this.expertId});
}

class CleanUserEvent extends UserEvent {}
