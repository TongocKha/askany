import 'dart:io';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/data/remote_data_source/language_repository.dart';
import 'package:askany/src/data/remote_data_source/user_repository.dart';
import 'package:askany/src/models/account_model.dart';
import 'package:askany/src/models/budget_model.dart';
import 'package:askany/src/models/expert_model.dart';
import 'package:askany/src/models/language_model.dart';
import 'package:askany/src/models/slide_mode.dart';
import 'package:askany/src/models/specialty_model.dart';
import 'package:askany/src/models/time_line_price_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_with_text_and_pop_button.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
part 'user_event.dart';
part 'user_state.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  UserBloc() : super(UserInitial());

  AccountModel? accountModel;
  ExpertModel? expertModel;
  List<LanguageModel> languages = [];
  double sendProgress = 0.0;

  @override
  Stream<UserState> mapEventToState(UserEvent event) async* {
    if (event is GetUserInfo) {
      yield UserGetDone(accountModel: getAccount);
      bool isSucceed = await _getInfoUser();
      if (isSucceed) {
        yield UserGetDone(accountModel: accountModel!);
      }
    }

    if (event is UpdateInfoUserEvent) {
      bool isUpdateSucceed = await _updateInfoUser(event);
      yield UserGetDone(accountModel: accountModel!);
      AppNavigator.pop();
      if (isUpdateSucceed) {
        AppNavigator.pop();
      } else {
        // Show dialog update fail
      }
    }

    if (event is UpdateUserWalletEvent) {
      if (accountModel != null) {
        accountModel = accountModel!.copyWith(wallet: event.updatedWallet);
        yield UserGetDone(accountModel: accountModel!);
      }
    }

    if (event is UpdatePhoneUserEvent) {
      accountModel!.phone = event.phone;
      accountModel!.isVerifyPhone = true;
      yield UserGetDone(accountModel: accountModel!);
    }

    if (event is UpdateEmailUserEvent) {
      accountModel!.email = event.email;
      yield UserGetDone(accountModel: accountModel!);
    }

    if (event is GetLanguagesEvent) {
      await _getLanguages();
    }

    if (event is UpdateInfoExpertEvent) {
      await _updateInfoExpert(event);
    }

    if (event is UpdateServicePriceEvent) {
      await _updateServicePrice(event);
    }

    if (event is BlockExpertEvent) {
      bool? isSuccess = await UserRepository().blocExpert(
        expertId: event.expertId,
      );

      AppNavigator.pop();

      if (isSuccess!) {
        AppNavigator.pop();
      } else {
        dialogAnimationWrapper(
          slideFrom: SlideMode.bot,
          child: DialogWithTextAndPopButton(
            title: Strings.errorOccurredNoti.i18n,
            bodyAfter: Strings.errorOccurredTryAgain.i18n,
          ),
        );
      }
    }

    if (event is CleanUserEvent) {
      accountModel = null;
      yield UserGetDone(accountModel: getAccount);
    }
  }

  // Private methods
  Future<bool> _getInfoUser() async {
    List accountResponse = await UserRepository().getUserByToken();
    if (accountResponse[0] != null) {
      accountModel = accountResponse[0];
      UserLocal().saveAccount(accountModel!);
    }

    if (accountResponse[1] != null) {
      expertModel = accountResponse[1];
    }

    return accountResponse[0] != null;
  }

  Future<bool> _updateInfoUser(UpdateInfoUserEvent event) async {
    AccountModel? accountResponse = await UserRepository().updateInfoUserById(
      fullname: event.fullname,
      province: event.province,
      image: event.avatar,
    );
    if (accountResponse != null) {
      accountModel!.avatar = accountResponse.avatar;
      accountModel!.fullname = accountResponse.fullname;
      accountModel!.province = accountResponse.province;
      UserLocal().saveAccount(accountModel!);
    }

    return accountResponse != null;
  }

  Future<void> _getLanguages() async {
    List<LanguageModel> _languages = await LanguageRepository().getLanguages();

    languages.addAll(_languages);
  }

  Future<void> _updateInfoExpert(UpdateInfoExpertEvent event) async {
    bool _isUpdateSucceed = await UserRepository().updateInfoExpert(
      languages: event.languagues.map((e) => e.id).toList(),
      specialties: event.specialties.map((e) => e.id).toList(),
      experienceYears: event.experienceYears,
      positionId: event.positionId,
      companyName: event.companyName,
      introduce: event.introduce,
    );

    AppNavigator.pop();

    if (_isUpdateSucceed) {
      expertModel!.languages = event.languagues;
      expertModel!.specialties = event.specialties;
      expertModel!.experienceYears = int.parse(event.experienceYears);
      expertModel!.highestPosition = event.positionId;
      expertModel!.companyName = event.companyName;
      expertModel!.introduce = event.introduce;
      AppNavigator.pop();
    } else {
      dialogAnimationWrapper(
        slideFrom: SlideMode.bot,
        child: DialogWithTextAndPopButton(
          title: Strings.errorOccurredNoti.i18n,
          bodyAfter: Strings.errorOccurredTryAgain.i18n,
        ),
      );
    }
  }

  Future<void> _updateServicePrice(UpdateServicePriceEvent event) async {
    bool _isUpdateSucceed = await UserRepository().updateSericePrice(
      callPrice: event.callPrice,
      meetPrice: event.meetPrice,
      timelines: event.timelines,
    );

    AppNavigator.pop();

    if (_isUpdateSucceed) {
      AppNavigator.pop();
      expertModel!.callPrice = event.callPrice;
      expertModel!.meetPrice = event.meetPrice;
      expertModel!.timelinePriceModel = event.timelines
          .where((timeline) =>
              timeline.weeklySchedules.isNotEmpty &&
              timeline.weeklySchedules.first.timesCall.isNotEmpty)
          .toList();
    } else {
      dialogAnimationWrapper(
        slideFrom: SlideMode.bot,
        child: DialogWithTextAndPopButton(
          title: Strings.errorOccurredNoti.i18n,
          bodyAfter: Strings.errorOccurredTryAgain.i18n,
        ),
      );
    }
  }

  // Public methods
  AccountModel get getAccount => accountModel ?? UserLocal().getUser();
}
