part of 'user_bloc.dart';

@immutable
abstract class UserState {}

class UserInitial extends UserState {}

class UserGetting extends UserState {}

class UserGetDone extends UserState {
  late final AccountModel accountModel;
  UserGetDone({
    required this.accountModel,
  });
}
