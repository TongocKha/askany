part of 'progress_bloc.dart';

@immutable
abstract class ProgressState {}

class ProgressInitial extends ProgressState {}

class ProgressLoaded extends ProgressState {
  final double progress;
  ProgressLoaded({required this.progress});
}
