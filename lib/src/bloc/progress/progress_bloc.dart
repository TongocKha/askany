import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'progress_event.dart';
part 'progress_state.dart';

class ProgressBloc extends Bloc<ProgressEvent, ProgressState> {
  ProgressBloc() : super(ProgressInitial());

  double progressSend = 0.0;
  double progressReceive = 0.0;

  @override
  Stream<ProgressState> mapEventToState(ProgressEvent event) async* {
    if (event is UpdateProgressSendEvent) {
      _updateProgressSend(event);
      yield getProgress();
    }

    if (event is UpdateProgressReceiveEvent) {
      _updateProgressReceive(event);
      yield getProgress();
    }

    if (event is ResetProgressEvent) {
      progressSend = 0.0;
      progressReceive = 0.0;
      yield getProgress();
    }
  }

  ProgressLoaded getProgress() {
    return ProgressLoaded(progress: ((progressSend + progressReceive) / 2) * 100);
  }

  // Private methods
  void _updateProgressSend(UpdateProgressSendEvent event) {
    progressSend = event.progress;
  }

  void _updateProgressReceive(UpdateProgressReceiveEvent event) {
    progressReceive = event.progress;
  }
}
