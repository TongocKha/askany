part of 'progress_bloc.dart';

@immutable
abstract class ProgressEvent {}

class UpdateProgressSendEvent extends ProgressEvent {
  final double progress;
  UpdateProgressSendEvent({required this.progress});
}

class UpdateProgressReceiveEvent extends ProgressEvent {
  final double progress;
  UpdateProgressReceiveEvent({required this.progress});
}

class ResetProgressEvent extends ProgressEvent {}
