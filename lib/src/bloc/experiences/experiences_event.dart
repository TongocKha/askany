part of 'experiences_bloc.dart';

@immutable
abstract class ExperiencesEvent {}

class RefreshExperienceEvent extends ExperiencesEvent {
  final Function handleFinished;
  RefreshExperienceEvent({required this.handleFinished});
}

class OnExperiencesEvent extends ExperiencesEvent {}

class GetExperiencesEvent extends ExperiencesEvent {}

class AddExperienceEvent extends ExperiencesEvent {
  final ExperienceModel experience;
  final Function? handleFinished;
  AddExperienceEvent({required this.experience, this.handleFinished});
}

class UpdateExperienceEvent extends ExperiencesEvent {
  final ExperienceModel experience;
  UpdateExperienceEvent({required this.experience});
}

class DeleteExperienceEvent extends ExperiencesEvent {
  final String experienceId;
  DeleteExperienceEvent({required this.experienceId});
}

class CleanExperienceEvent extends ExperiencesEvent {}
