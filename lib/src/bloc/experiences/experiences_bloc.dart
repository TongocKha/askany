import 'dart:ui';

import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/data/remote_data_source/experience_repository.dart';
import 'package:askany/src/models/experience_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_with_text_and_pop_button.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:askany/src/ui/style/calendar_style.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'experiences_event.dart';
part 'experiences_state.dart';

class ExperiencesBloc extends Bloc<ExperiencesEvent, ExperiencesState> {
  ExperiencesBloc() : super(ExperiencesInitial());

  List<ExperienceModel> experiences = [];
  bool isOver = false;

  @override
  Stream<ExperiencesState> mapEventToState(ExperiencesEvent event) async* {
    if (event is RefreshExperienceEvent) {
      _cleanBloc();
      await _getExperiences();
      event.handleFinished();
      yield _getDoneExperiences;
    }

    if (event is OnExperiencesEvent) {
      if (experiences.isEmpty) {
        yield ExperiencesInitial();
        await _getExperiences();
      }
      yield _getDoneExperiences;
    }

    if (event is GetExperiencesEvent) {
      if (!isOver) {
        yield _gettingExperiences;
        await _getExperiences();
        yield _getDoneExperiences;
      }
    }

    if (event is AddExperienceEvent) {
      bool isCreateSucceed = await _addExperience(event);
      if (isCreateSucceed) {
        if (event.handleFinished != null) {
          event.handleFinished!();
        } else {
          AppNavigator.pop();
        }
      } else {
        // Show popup failure!
      }
      yield _getDoneExperiences;
    }

    if (event is UpdateExperienceEvent) {
      bool isUpdateSucceed = await _updateExperience(event);
      if (isUpdateSucceed) {
        AppNavigator.pop();
      } else {
        // Show popup failure!
      }
      yield _getDoneExperiences;
    }

    if (event is DeleteExperienceEvent) {
      if (experiences.length > 1) {
        bool isDeleteSucceed = await _deleteExperience(event);
        if (isDeleteSucceed) {
          AppNavigator.pop();
        } else {
          // Show popup failure!
        }
        yield _getDoneExperiences;
      } else {
        AppNavigator.pop();
        AppNavigator.pop();
        dialogAnimationWrapper(
          child: DialogWithTextAndPopButton(
            bodyColor: colorBlack1,
            bodyAlign: TextAlign.center,
            bodyAfter: Strings.cannotDeleteExperienceNoti.i18n,
          ),
        );
      }
    }

    if (event is CleanExperienceEvent) {
      _cleanBloc();
      yield ExperiencesInitial();
    }
  }

  // MARK: Private methods
  GettingExperiences get _gettingExperiences =>
      GettingExperiences(experiences: experiences);
  GetDoneExperiences get _getDoneExperiences =>
      GetDoneExperiences(experiences: experiences);

  Future<void> _getExperiences() async {
    List<ExperienceModel> _experiences =
        await ExperienceRepository().getExperiences(
      skip: experiences.length,
    );

    if (_experiences.length < LIMIT_API_10) {
      isOver = true;
    }

    experiences.addAll(_experiences);
  }

  Future<bool> _addExperience(AddExperienceEvent event) async {
    ExperienceModel? _experience = await ExperienceRepository().addExperience(
      experience: event.experience,
    );

    AppNavigator.pop();

    if (_experience != null) {
      experiences.add(_experience);
    }

    return _experience != null;
  }

  Future<bool> _updateExperience(UpdateExperienceEvent event) async {
    bool _isUpdateSucceed = await ExperienceRepository().updateExperience(
      experience: event.experience,
    );

    AppNavigator.pop();

    if (_isUpdateSucceed) {
      int indexOfExperience = experiences.indexWhere(
        (experience) => experience.id == event.experience.id,
      );

      if (indexOfExperience != -1) {
        experiences[indexOfExperience] = event.experience;
      }
    }

    return _isUpdateSucceed;
  }

  Future<bool> _deleteExperience(DeleteExperienceEvent event) async {
    bool _isDeleteSucceed = await ExperienceRepository().deleteExperience(
      experienceId: event.experienceId,
    );

    AppNavigator.pop();

    if (_isDeleteSucceed) {
      int indexOfExperience = experiences.indexWhere(
        (experience) => experience.id == event.experienceId,
      );

      if (indexOfExperience != -1) {
        experiences.removeAt(indexOfExperience);
      }
    }

    return _isDeleteSucceed;
  }

  _cleanBloc() {
    experiences.clear();
    isOver = false;
  }
}
