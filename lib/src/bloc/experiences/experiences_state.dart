part of 'experiences_bloc.dart';

@immutable
abstract class ExperiencesState {
  /// 0: List<ExperienceModel> experiences
  List get props => [];
}

class ExperiencesInitial extends ExperiencesState {
  @override
  List get props => [[]];
}

class GettingExperiences extends ExperiencesState {
  final List<ExperienceModel> experiences;
  GettingExperiences({required this.experiences});

  @override
  List get props => [experiences];
}

class GetDoneExperiences extends ExperiencesState {
  final List<ExperienceModel> experiences;
  GetDoneExperiences({required this.experiences});

  @override
  List get props => [experiences];
}
