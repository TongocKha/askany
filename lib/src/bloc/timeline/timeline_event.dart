part of 'timeline_bloc.dart';

@immutable
abstract class TimelineEvent {}

class GetDateAvailable extends TimelineEvent {
  final String expertId;
  final int contactType;
  final DateTime selectedDate;
  GetDateAvailable({required this.selectedDate, required this.contactType, required this.expertId});
}

class GetTimeAvailable extends TimelineEvent {
  final String day;
  GetTimeAvailable({required this.day});
}
