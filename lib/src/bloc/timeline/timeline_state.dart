part of 'timeline_bloc.dart';

@immutable
abstract class TimelineState {
  /// 0: ExpertTimelineModel timeline
  List<dynamic> get props => [];
}

class TimelineInitial extends TimelineState {
  @override
  List get props => [null];
}

class TimelineLoading extends TimelineState {
  final ExpertTimelineModel timeline;
  TimelineLoading({required this.timeline});

  @override
  List get props => [timeline];
}

class TimelineLoaded extends TimelineState {
  final DateTime selectedDate;
  final ExpertTimelineModel timeline;
  TimelineLoaded({required this.timeline, required this.selectedDate});

  @override
  List get props => [timeline];
}
