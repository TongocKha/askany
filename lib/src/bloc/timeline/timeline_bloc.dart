import 'package:askany/src/data/remote_data_source/request_repository.dart';
import 'package:askany/src/models/expert_timeline_model.dart';
import 'package:bloc/bloc.dart';
import 'package:intl/intl.dart';
import 'package:meta/meta.dart';

part 'timeline_event.dart';
part 'timeline_state.dart';

class TimelineBloc extends Bloc<TimelineEvent, TimelineState> {
  TimelineBloc() : super(TimelineInitial());

  List<int> dateAvailableInMonth = [];
  String expertId = '';
  DateTime selectedDate = DateTime.now();
  int contactType = 0;
  Map<String, ExpertTimelineModel> timelines = {};

  @override
  Stream<TimelineState> mapEventToState(TimelineEvent event) async* {
    if (event is GetDateAvailable) {
      yield _timelineLoading;
      await _handleGetDate(event);
      yield _timelineLoaded;
    }

    if (event is GetTimeAvailable) {
      yield _timelineLoading;
      await _handleGetTime(event.day);
      yield _timelineLoaded;
    }
  }

  // Private methods
  ExpertTimelineModel get _timelineDefault => ExpertTimelineModel(
        busyTimes: [],
        scheduleTime: [],
        dateAvailableInMonth: dateAvailableInMonth,
      );

  ExpertTimelineModel? get _getTimelineFromMap {
    return timelines[DateFormat('yyyy-MM-dd').format(selectedDate)];
  }

  setTimelineByDay({required ExpertTimelineModel timeline}) {
    timelines[DateFormat('yyyy-MM-dd').format(selectedDate)] = timeline;
  }

  TimelineLoading get _timelineLoading {
    ExpertTimelineModel expertTimelineModel = _getTimelineFromMap ?? _timelineDefault;
    expertTimelineModel.dateAvailableInMonth = dateAvailableInMonth;
    return TimelineLoading(
      timeline: expertTimelineModel,
    );
  }

  TimelineLoaded get _timelineLoaded {
    ExpertTimelineModel expertTimelineModel = _getTimelineFromMap ?? _timelineDefault;
    expertTimelineModel.dateAvailableInMonth = dateAvailableInMonth;
    return TimelineLoaded(
      timeline: expertTimelineModel,
      selectedDate: selectedDate,
    );
  }

  Future<void> _handleGetDate(GetDateAvailable event) async {
    selectedDate = event.selectedDate;
    contactType = event.contactType;
    expertId = event.expertId;
    ExpertTimelineModel? timeline = await RequestRepository().getExpertTimeline(
      expertId: expertId,
      selectedDate: selectedDate,
      contactType: contactType,
    );

    if (timeline != null) {
      dateAvailableInMonth = timeline.dateAvailableInMonth;

      dateAvailableInMonth.sort((a, b) => a.compareTo(b));

      bool enableThisDate =
          dateAvailableInMonth.indexWhere((date) => date == event.selectedDate.day) != -1;

      if (!enableThisDate) {
        int nextDayAvailable = dateAvailableInMonth.firstWhere(
          (date) => date > event.selectedDate.day,
          orElse: () => selectedDate.day,
        );
        selectedDate = DateTime(selectedDate.year, selectedDate.month, nextDayAvailable);
      }
      await _handleGetTime(event.selectedDate.day.toString());
    }
  }

  Future<void> _handleGetTime(String day) async {
    selectedDate = DateTime(selectedDate.year, selectedDate.month, int.parse(day));
    ExpertTimelineModel? timelineFromMap = _getTimelineFromMap;
    if (timelineFromMap == null) {
      ExpertTimelineModel? timeline = await RequestRepository().getExpertTimeline(
        expertId: expertId,
        selectedDate: selectedDate,
        contactType: contactType,
        day: day,
      );

      if (timeline != null) {
        setTimelineByDay(timeline: timeline);
      }
    }
  }
}
