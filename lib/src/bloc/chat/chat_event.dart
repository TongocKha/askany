part of 'chat_bloc.dart';

@immutable
abstract class ChatEvent {}

class RefreshConversationEvent extends ChatEvent {
  final Function handleFinished;
  RefreshConversationEvent({required this.handleFinished});
}

class OnChatEvent extends ChatEvent {}

class GetConversationEvent extends ChatEvent {}

class InsertConversationEvent extends ChatEvent {
  final ConversationModel conversation;
  InsertConversationEvent({required this.conversation});
}

class SearchConversationEvent extends ChatEvent {
  final String searchKey;
  SearchConversationEvent({required this.searchKey});
}

class DeleteConversationEvent extends ChatEvent {
  final String conversationId;
  DeleteConversationEvent({required this.conversationId});
}

class MarkReadEvent extends ChatEvent {
  final String conversationId;
  final bool isSeen;
  MarkReadEvent({required this.conversationId, this.isSeen = true});
}

class AddConversationEvent extends ChatEvent {}

class UpdateLatestMessageEvent extends ChatEvent {
  final String conversationId;
  final MessageModel message;
  UpdateLatestMessageEvent(
      {required this.conversationId, required this.message});
}

class UpdateDraftMessageEvent extends ChatEvent {
  final String conversationId;
  final String data;
  UpdateDraftMessageEvent({required this.data, required this.conversationId});
}

class ChangeConversationNameEvent extends ChatEvent {
  final String conversationId;
  final String conversationName;
  ChangeConversationNameEvent(
      {required this.conversationId, required this.conversationName});
}

class CheckCanChatEvent extends ChatEvent {
  final String expertId;
  CheckCanChatEvent({required this.expertId});
}

class IgnoreConversationEvent extends ChatEvent {
  final String conversationId;
  IgnoreConversationEvent({required this.conversationId});
}

class AddTypingConversationEvent extends ChatEvent {
  final String conversationId;
  AddTypingConversationEvent({required this.conversationId});
}

class RemoveTypingConversationEvent extends ChatEvent {
  final String conversationId;
  RemoveTypingConversationEvent({required this.conversationId});
}

class CleanChatEvent extends ChatEvent {}
