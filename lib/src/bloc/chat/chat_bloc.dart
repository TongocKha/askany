import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/badge/badge_bloc.dart';
import 'package:askany/src/bloc/message/message_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/data/local_data_source/message_local_data.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/data/remote_data_source/chat_repository.dart';
import 'package:askany/src/data/remote_data_source/message_repository.dart';
import 'package:askany/src/models/account_model.dart';
import 'package:askany/src/models/conversation_model.dart';
import 'package:askany/src/models/message_model.dart';
import 'package:askany/src/models/slide_mode.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_with_text_and_pop_button.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/extentions/string_extention.dart';

part 'chat_event.dart';
part 'chat_state.dart';

class ChatBloc extends Bloc<ChatEvent, ChatState> {
  ChatBloc() : super(ConversationInitial());

  List<ConversationModel> conversations = [];
  List<ConversationModel> backupConversations = [];
  bool isOverConversations = false;
  bool isOverSearchConversations = false;
  String searchKey = '';
  List<String> isTypings = [];

  Map<String, String> draftMessages = {};

  @override
  Stream<ChatState> mapEventToState(ChatEvent event) async* {
    if (event is RefreshConversationEvent) {
      await _refreshConversation(event);
      _mapErrorMessageToConversation();
      AppBloc.messageBloc.add(CleanMessageEvent());
      yield _getDoneConversation;
    }

    if (event is OnChatEvent) {
      searchKey = '';

      if (draftMessages.isEmpty && MessageLocal().draftMessages.isNotEmpty) {
        draftMessages = MessageLocal().draftMessages;
      }

      if (conversations.isEmpty) {
        yield ConversationInitial();
        await _getConversations();
        _mapErrorMessageToConversation();
      }

      yield _getDoneConversation;
    }

    if (event is GetConversationEvent) {
      if (!isOverConversations) {
        yield _gettingConversation;
        await _getConversations();
        yield _getDoneConversation;
      }
    }

    if (event is InsertConversationEvent) {
      _insertConversation(event);
      yield _getDoneConversation;
    }

    if (event is AddConversationEvent) {}

    if (event is SearchConversationEvent) {
      if (searchKey.isNotEmpty && event.searchKey.isEmpty) {
        backupConversations.clear();
        isOverSearchConversations = false;
      }

      searchKey = event.searchKey.toLowerCase().formatVietnamese();

      if (searchKey.isNotEmpty) {
        if (backupConversations.isEmpty) {
          backupConversations.addAll(getFilterListConversation);
          yield ConversationInitial();
        } else {
          yield _gettingConversation;
        }

        await _getConversations();
      }
      yield _getDoneConversation;
    }

    if (event is DeleteConversationEvent) {
      await _deleteConversation(event);
      yield _getDoneConversation;
    }

    if (event is MarkReadEvent) {
      await _markSeenMessage(event);
      yield _getDoneConversation;
    }

    if (event is UpdateLatestMessageEvent) {
      await _handleUpdateLatestMessage(event);
      yield _getDoneConversation;
    }

    if (event is UpdateDraftMessageEvent) {
      _updateDraftMessage(event);
      yield _getDoneConversation;
    }

    if (event is ChangeConversationNameEvent) {
      await _changeConversationName(event);
      yield _getDoneConversation;
    }

    if (event is CheckCanChatEvent) {
      await _hasChatExpert(event);
    }

    if (event is CleanChatEvent) {
      _handleClean();
      yield ConversationInitial();
    }

    if (event is IgnoreConversationEvent) {
      await _ignoreConversation(event.conversationId);
      yield _getDoneConversation;
    }

    if (event is AddTypingConversationEvent) {
      _handleAddConversationTyping(event);
      yield _getDoneConversation;
    }

    if (event is RemoveTypingConversationEvent) {
      _handleRemoveConversationTyping(event);
      yield _getDoneConversation;
    }
  }

  // States
  GettingConversation get _gettingConversation => GettingConversation(
        conversations:
            searchKey.isEmpty ? getFilterListConversation : backupConversations,
        isTypings: isTypings,
      );
  GetDoneConversation get _getDoneConversation => GetDoneConversation(
        conversations:
            searchKey.isEmpty ? getFilterListConversation : backupConversations,
        isTypings: isTypings,
      );

  List<ConversationModel> get getFilterListConversation {
    _filterDuplicateConversation();
    conversations.sort((a, b) =>
        b.latestMessage.createdAt.compareTo(a.latestMessage.createdAt));

    List<ConversationModel> backConversations =
        conversations.map((e) => e.copyWith()).toList();

    backConversations.asMap().forEach((index, e) {
      if (draftMessages[e.id] != null && draftMessages[e.id]!.isNotEmpty) {
        e.latestMessage = LatestMessage(
          id: '',
          expertSender: AppBloc.userBloc.getAccount.id!,
          data: '[draft] ${draftMessages[e.id]}',
          createdAt: conversations[index].latestMessage.createdAt,
        );
      } else {
        e.latestMessage = conversations[index].latestMessage;
      }
    });

    if (searchKey.isEmpty) {
      return backConversations;
    }

    List searchKeys = searchKey.split(' ');
    List<ConversationModel> result = backConversations.where(
      (e) {
        bool allMatches = searchKeys.every(
          (key) => e.conversationTitle
              .toLowerCase()
              .formatVietnamese()
              .contains(key),
        );

        return allMatches;
      },
    ).toList();
    return result;
  }

  Future<void> _refreshConversation(RefreshConversationEvent event) async {
    conversations.clear();
    isOverConversations = false;
    await _getConversations();
    event.handleFinished();
  }

  // Private methods
  Future<void> _getConversations() async {
    if (searchKey.length > SINGLE_CHARACTER) {
      if (isOverSearchConversations) {
        return;
      }
    } else {
      if (isOverConversations) {
        return;
      }
    }

    List<ConversationModel> _conversations =
        await ChatRepository().getConversations(
      skip: searchKey.length > SINGLE_CHARACTER
          ? (backupConversations.length - getFilterListConversation.length)
          : conversations.length,
      search: searchKey.length > SINGLE_CHARACTER ? searchKey : null,
    );

    if (searchKey.length > SINGLE_CHARACTER) {
      if (_conversations.length < LIMIT_API_10) {
        isOverSearchConversations = true;
      }
      backupConversations.addAll(_conversations);
    } else {
      if (_conversations.length < LIMIT_API_10) {
        isOverConversations = true;
      }
      conversations.addAll(_conversations);
    }
  }

  Future<void> _handleUpdateLatestMessage(
      UpdateLatestMessageEvent event) async {
    int index = conversations
        .indexWhere((conversation) => conversation.id == event.conversationId);
    if (index != -1) {
      if (event.message.data.isEmpty) {
        conversations.removeAt(index);
      } else {
        conversations[index].latestMessage =
            LatestMessage.fromMessageModel(event.message);
        conversations.insert(0, conversations[index]);
        conversations.removeAt(index + 1);
      }
    } else {
      // Get info conversation & add to list conversations
      ConversationModel? conversation =
          await ChatRepository().getDetailsConversation(
        conversationId: event.conversationId,
      );

      if (conversation != null) {
        conversations.insert(0, conversation);
      }
    }
  }

  Future<void> _markSeenMessage(MarkReadEvent event) async {
    int index = conversations
        .indexWhere((conversation) => conversation.id == event.conversationId);
    if (index != -1) {
      int isSeen = event.isSeen ? IS_SEEN : NOT_SEEN;
      final bool updateSucceed =
          await MessageRepository().updateStatusSeenMessage(
        messageId: conversations[index].latestMessage.id,
        isSeen: isSeen,
      );
      if (updateSucceed) {
        conversations[index].latestMessage.isSeen = event.isSeen;
        AppBloc.badgesBloc
            .add(UpdateBadgesChatEvent(extendValue: event.isSeen ? -1 : 1));
      }
    }
  }

  Future<bool> _deleteConversation(DeleteConversationEvent event) async {
    bool isDeleteSucceed = await ChatRepository().deleteConversation(
      conversationId: event.conversationId,
    );

    if (isDeleteSucceed) {
      int index = conversations.indexWhere(
        (conversation) => conversation.id == event.conversationId,
      );
      if (index != -1) {
        if (!conversations[index].latestMessage.isSeen) {
          AppBloc.badgesBloc.add(UpdateBadgesChatEvent(extendValue: -1));
        }
        conversations.removeAt(index);
      }
    } else {
      dialogAnimationWrapper(
        slideFrom: SlideMode.bot,
        child: DialogWithTextAndPopButton(
          bodyAfter: Strings.cannotDeleteChatNoti.i18n,
          bodyAlign: TextAlign.center,
        ),
      );
    }

    return isDeleteSucceed;
  }

  Future<void> _changeConversationName(
      ChangeConversationNameEvent event) async {
    bool isChangeSucceed = await ChatRepository().updateConversationName(
      conversationId: event.conversationId,
      conversationName: event.conversationName,
    );

    if (isChangeSucceed) {
      int indexOfConversations = conversations.indexWhere(
          (conversation) => conversation.id == event.conversationId);

      if (indexOfConversations != -1) {
        conversations[indexOfConversations].conversationName =
            event.conversationName;
      }
    }
  }

  Future<void> _ignoreConversation(String conversationId) async {
    int indexOfConverations = conversations
        .indexWhere((conversation) => conversation.id == conversationId);

    if (indexOfConverations != -1) {
      if (conversations[indexOfConverations].ignoreNotis.isNotEmpty) {
        bool isUnIgnoreConversation = await ChatRepository()
            .unIgnoreConversation(conversationId: conversationId);
        if (isUnIgnoreConversation) {
          conversations[indexOfConverations]
              .ignoreNotis
              .remove(conversations[indexOfConverations].users.first.id ?? '');
        }
      } else {
        bool isIgnoreConversation = await ChatRepository()
            .ignoreConversation(conversationId: conversationId);
        if (isIgnoreConversation) {
          conversations[indexOfConverations]
              .ignoreNotis
              .add(conversations[indexOfConverations].users.first.id ?? '');
        }
      }
    }
  }

  _updateDraftMessage(UpdateDraftMessageEvent event) {
    draftMessages[event.conversationId] = event.data;
    draftMessages.removeWhere((key, value) => value.isEmpty);
    MessageLocal().saveDraftMessages(draftMessages);
  }

  _mapErrorMessageToConversation() {
    List<MessageModel> _errorMessages = MessageLocal()
        .errorMessages
        .where((e) => e.userSender == UserLocal().getUser().id!)
        .toList();

    _errorMessages.forEach((msg) {
      int indexOfConversations = conversations
          .indexWhere((conversation) => conversation.id == msg.conversation);
      if (indexOfConversations != -1 &&
          msg.createdAt.isAfter(
              conversations[indexOfConversations].latestMessage.createdAt)) {
        conversations[indexOfConversations].latestMessage =
            LatestMessage.fromMessageModel(msg);
      }
    });
  }

  _insertConversation(InsertConversationEvent event) {
    int indexOfConversation = conversations.indexWhere(
      (conversation) => conversation.id == event.conversation.id,
    );
    if (indexOfConversation == -1) {
      conversations.insert(0, event.conversation);
    }
  }

  _filterDuplicateConversation() {
    conversations = conversations.toSet().toList();
  }

  Future<void> _hasChatExpert(CheckCanChatEvent event) async {
    ConversationModel? _conversation =
        await ChatRepository().getConversationByExpert(
      expertId: event.expertId,
    );

    AppNavigator.pop();

    if (_conversation != null) {
      AppNavigator.push(
        Routes.CONVERSATION,
        arguments: {
          'conversationModel': _conversation,
        },
      );
    } else {
      dialogAnimationWrapper(
        slideFrom: SlideMode.bot,
        child: DialogWithTextAndPopButton(
          title: Strings.cannotSendMessgaeNoti.i18n,
          bodyAfter: Strings.cannotSendMessageReason.i18n,
        ),
      );
    }
  }

  void _handleAddConversationTyping(AddTypingConversationEvent event) {
    int indexOfConversation = isTypings
        .indexWhere((conversationId) => event.conversationId == conversationId);

    if (indexOfConversation == -1) {
      isTypings.add(event.conversationId);
    }
  }

  void _handleRemoveConversationTyping(RemoveTypingConversationEvent event) {
    int indexOfConversation = isTypings
        .indexWhere((conversationId) => event.conversationId == conversationId);

    if (indexOfConversation != -1) {
      isTypings.removeAt(indexOfConversation);
    }
  }

  void _handleClean() {
    conversations.clear();
    draftMessages.clear();
    isOverConversations = false;
    MessageLocal().removeDraftMessages();
  }

  List<AccountModel> get listUsers {
    List<AccountModel> result = [];
    conversations.forEach((e) => result.addAll(e.allMembers));
    return result;
  }
}
