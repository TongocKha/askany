part of 'chat_bloc.dart';

@immutable
abstract class ChatState {
  /// 0: conversations
  List<dynamic> get props => [];
}

class ConversationInitial extends ChatState {
  @override
  List get props => [[], []];
}

class GettingConversation extends ChatState {
  final List<ConversationModel> conversations;
  final List<String> isTypings;

  GettingConversation({required this.conversations, required this.isTypings});

  @override
  List get props => [conversations, isTypings];
}

class GetDoneConversation extends ChatState {
  final List<ConversationModel> conversations;
  final List<String> isTypings;
  GetDoneConversation({required this.conversations, required this.isTypings});

  @override
  List get props => [conversations, isTypings];
}
