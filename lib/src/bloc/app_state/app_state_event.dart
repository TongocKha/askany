abstract class AppStateEvent {}

class OnResume extends AppStateEvent {}

class OnBackground extends AppStateEvent {}

class OnStartApp extends AppStateEvent {}
