import 'dart:async';
import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/home/home_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/helpers/deep_link_helper.dart';
import 'package:askany/src/models/slide_mode.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/services/method_channel/initial_app_channel.dart';
import 'package:askany/src/services/socket/socket.dart';
import 'package:askany/src/services/socket/socket_emit.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_confirm_cancel.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:bloc/bloc.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/services.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'bloc.dart';

class AppStateBloc extends Bloc<AppStateEvent, AppState> {
  AppStateBloc() : super(AppStateInitial());
  final Connectivity _connectivity = Connectivity();
  // ignore: cancel_subscriptions
  late StreamSubscription<ConnectivityResult> connectivitySubscription;
  bool isNetworkConnected = true;
  bool isActive = true;

  @override
  Stream<AppState> mapEventToState(AppStateEvent event) async* {
    if (event is OnStartApp) {
      _initConnectivity();
      connectivitySubscription =
          _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
      Future.delayed(Duration(seconds: DELAY_TWO_SECONDS * 5), () async {
        await DeepLinkHelper.initUniUrls();
      });
    }

    if (event is OnResume) {
      _handleResume();
    }

    if (event is OnBackground) {
      _handleBackground();
    }
  }

  // Private Methods
  void _handleResume() {
    if (socket != null && !isActive) {
      isActive = true;
      SocketEmit().sendSetActive();
    }
  }

  void _handleBackground() {
    if (socket != null && isActive) {
      isActive = false;
      InitialAppChannel.resetFlag();
      SocketEmit().sendSetInActive();
    }
  }

  Future<void> _initConnectivity() async {
    ConnectivityResult result = ConnectivityResult.none;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        _handleNetworkConnected();
        break;
      case ConnectivityResult.mobile:
        _handleNetworkConnected();
        break;
      default:
        _handleConnectNetworkFail();
        break;
    }
  }

  void _handleNetworkConnected() {
    if (!isNetworkConnected) {
      // Pop dialog alert no internet connected
      AppNavigator.popUntil(Routes.ROOT);
      AppBloc.homeBloc.add(OnChangeIndexEvent());
      AppBloc.initialHomeBloc();
    }
    isNetworkConnected = true;
  }

  void _handleConnectNetworkFail() async {
    if (isNetworkConnected) {
      isNetworkConnected = false;
      // Show dialog alert no internet and reconnect
      await dialogAnimationWrapper(
        dismissible: false,
        borderRadius: 10.sp,
        slideFrom: SlideMode.bot,
        child: DialogConfirmCancel(
          bodyBefore: Strings.connectionNoti.i18n,
          confirmText: Strings.retry.i18n,
          onConfirmed: () {
            AppNavigator.popUntil(Routes.ROOT);
            AppBloc.homeBloc.add(OnChangeIndexEvent());
            isNetworkConnected = true;
          },
        ),
      );
    }
  }
}
