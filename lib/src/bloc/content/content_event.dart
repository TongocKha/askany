part of 'content_bloc.dart';

@immutable
abstract class ContentEvent {}

class OnExpertCategoryEvent extends ContentEvent {
  final String slug;
  OnExpertCategoryEvent({required this.slug});
}

class GetExpertCategoryEvent extends ContentEvent {
  final String slug;
  GetExpertCategoryEvent({required this.slug});
}

class CleanContentEvent extends ContentEvent {}
