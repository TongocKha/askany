part of 'content_bloc.dart';

@immutable
abstract class ContentState {
  List<dynamic> get props => [];
}

class ContentInitial extends ContentState {
  List get props => [[], []];
}

class GettingContents extends ContentState {
  final List<ContentModel> contents;

  GettingContents({required this.contents});

  List get props => [contents, []];
}

class GetDoneContents extends ContentState {
  final List<ContentModel> contents;

  GetDoneContents({required this.contents});

  List get props => [contents, []];
}

class GettingExpertCategory extends ContentState {
  final List<ExpertModel> experts;
  final List<ContentModel> contents;
  GettingExpertCategory({required this.contents, required this.experts});

  @override
  List get props => [contents, experts];
}

class GetDoneExpertCategory extends ContentState {
  final List<ExpertModel> experts;
  final List<ContentModel> contents;
  GetDoneExpertCategory({required this.contents, required this.experts});

  @override
  List get props => [contents, experts];
}

class ExpertInitial extends ContentState {
  @override
  List get props => [[], []];
}
