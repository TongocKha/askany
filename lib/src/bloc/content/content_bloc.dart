import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/data/remote_data_source/category_repository.dart';
import 'package:askany/src/models/content_model.dart';
import 'package:askany/src/models/expert_model.dart';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'content_event.dart';
part 'content_state.dart';

class ContentBloc extends Bloc<ContentEvent, ContentState> {
  ContentBloc() : super(ContentInitial());
  List<ContentModel> contentList = [];
  bool isOVerContent = false;

  List<ExpertModel> expertCategory = [];
  bool isOverExpertCategory = false;

  @override
  Stream<ContentState> mapEventToState(ContentEvent event) async* {
    if (event is OnExpertCategoryEvent) {
      if (!isOverExpertCategory) {
        if (expertCategory.isEmpty) {
          yield ExpertInitial();

          await getExpertCategory(event.slug);
        }
      }

      if (!isOVerContent) {
        if (contentList.isEmpty) {
          yield ContentInitial();
        } else {
          yield _gettingContent;
        }
        await getContenList(event.slug);
      }
      yield _getDoneExpertCategory;
    }

    if (event is GetExpertCategoryEvent) {
      yield ExpertInitial();
      if (!isOverExpertCategory) {
        if (expertCategory.isEmpty) {
          yield ExpertInitial();
        } else {
          yield _gettingExpertCategory;
        }

        await getExpertCategory(event.slug);
      }
      if (!isOVerContent) {
        if (contentList.isEmpty) {
          yield ContentInitial();
        } else {
          yield _gettingContent;
        }
        await getContenList(event.slug);
      }

      yield _getDoneExpertCategory;
    }
    if (event is CleanContentEvent) {
      _cleanBloc();
      yield _getDoneContent;
    }
  }

  // MARK: Private methods

  GettingContents get _gettingContent => GettingContents(contents: contentList);
  GetDoneContents get _getDoneContent => GetDoneContents(contents: contentList);

  GettingExpertCategory get _gettingExpertCategory =>
      GettingExpertCategory(contents: contentList, experts: expertCategory);

  GetDoneExpertCategory get _getDoneExpertCategory =>
      GetDoneExpertCategory(contents: contentList, experts: expertCategory);

  Future<void> getExpertCategory(String slug) async {
    List<ExpertModel> _experts = await CategoryRepository()
        .getExpertCategory(slug: slug, skip: expertCategory.length);

    if (_experts.length < LIMIT_API_5) {
      isOverExpertCategory = true;
    }
    expertCategory.addAll(_experts);
  }

  Future<void> getContenList(String slug) async {
    List<ContentModel> _contents = await CategoryRepository()
        .getContentCategory(slug: slug, skip: contentList.length);

    if (_contents.length < LIMIT_API_5) {
      isOVerContent = true;
    }
    contentList.addAll(_contents);
  }

  _cleanBloc() {
    contentList.clear();
    isOVerContent = false;
    expertCategory.clear();
    isOverExpertCategory = false;
  }
}
