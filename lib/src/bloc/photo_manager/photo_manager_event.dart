part of 'photo_manager_bloc.dart';

@immutable
abstract class PhotoManagerEvent {}

class OnPhotoManagerEvent extends PhotoManagerEvent {}

class GetPhotoManagerEvent extends PhotoManagerEvent {}

class TogglePhotoEvent extends PhotoManagerEvent {
  final String path;
  TogglePhotoEvent({required this.path});
}

class CleanPhotoPickerEvent extends PhotoManagerEvent {}

class DisposePhotoEvent extends PhotoManagerEvent {}
