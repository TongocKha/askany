part of 'photo_manager_bloc.dart';

@immutable
abstract class PhotoManagerState {
  List get props => [];
}

class PhotoManagerInitial extends PhotoManagerState {
  @override
  List get props => [[], []];
}

class GettingPhotoManager extends PhotoManagerState {
  final List<File> images;
  final List<String> imagesChoosen;
  final bool isCameraGranted;
  GettingPhotoManager({required this.images, required this.imagesChoosen, required this.isCameraGranted});

  @override
  List get props => [images, imagesChoosen, isCameraGranted];
}

class GetDonePhotoManager extends PhotoManagerState {
  final List<File> images;
  final List<String> imagesChoosen;
  final bool isCameraGranted;
  GetDonePhotoManager({required this.images, required this.imagesChoosen, required this.isCameraGranted});

  @override
  List get props => [images, imagesChoosen, isCameraGranted];
}
