import 'dart:io';
import 'package:askany/src/helpers/permission_helper.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/ui/conversation/widgets/bottom_sheet_image_picker.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:photo_manager/photo_manager.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
part 'photo_manager_event.dart';
part 'photo_manager_state.dart';

class PhotoManagerBloc extends Bloc<PhotoManagerEvent, PhotoManagerState> {
  PhotoManagerBloc() : super(PhotoManagerInitial());

  List<File> images = [];
  List<String> imagesChoosen = [];
  late List<AssetPathEntity> _albums;
  bool isOver = false;
  bool isCameraGranted = false;
  @override
  Stream<PhotoManagerState> mapEventToState(PhotoManagerEvent event) async* {
    if (event is OnPhotoManagerEvent) {
      // Request permission
      bool isGranted = await PermissionHelper().checkPermissionAndRequest(Permission.photos);
      isCameraGranted = await PermissionHelper().checkPermissionAndRequest(Permission.camera);

      if (isGranted) {
        showModalBottomSheet(
          isScrollControlled: true,
          backgroundColor: Colors.transparent,
          context: AppNavigator.context!,
          builder: (BuildContext context) => ImagePickerBottomSheet(),
        );

        _albums = await PhotoManager.getAssetPathList(type: RequestType.image);
        if (images.isEmpty) {
          await _getMediaList();
        }
        yield _getDonePhotoManager;
      }
    }

    if (event is GetPhotoManagerEvent) {
      if (!isOver) {
        yield _gettingPhotoManager;
        await _getMediaList();
        yield _getDonePhotoManager;
      }
    }

    if (event is TogglePhotoEvent) {
      _togglePhoto(event);
      yield _getDonePhotoManager;
    }

    if (event is DisposePhotoEvent) {
      _releaseMemory();
      yield _getDonePhotoManager;
    }

    if (event is CleanPhotoPickerEvent) {
      _cleanPicker();
      yield _getDonePhotoManager;
    }
  }

  // MARK: Private methods
  GettingPhotoManager get _gettingPhotoManager => GettingPhotoManager(
        images: images,
        imagesChoosen: imagesChoosen,
        isCameraGranted: isCameraGranted,
      );
  GetDonePhotoManager get _getDonePhotoManager => GetDonePhotoManager(
      images: images, imagesChoosen: imagesChoosen, isCameraGranted: isCameraGranted);

  int get limitFormatter => 0.itemCountGridViewMoney * 3;

  Future<void> _getMediaList() async {
    List<AssetEntity> imageList = await _albums[0].getAssetListRange(
      start: images.length,
      end: images.length + limitFormatter,
    );

    if (imageList.length < limitFormatter) {
      isOver = true;
    }

    for (AssetEntity asset in imageList) {
      AssetEntity entity = asset;
      File? file = await entity.file;
      if (file != null) {
        images.add(file);
      }
    }
  }

  _togglePhoto(TogglePhotoEvent event) {
    int indexOfPicked = imagesChoosen.indexWhere((path) => path == event.path);

    if (indexOfPicked == -1) {
      if (imagesChoosen.length < 6) {
        imagesChoosen.add(event.path);
      }
    } else {
      imagesChoosen.removeAt(indexOfPicked);
    }
  }

  _releaseMemory() {
    isOver = false;
    if (images.length >= 9) {
      images = images.sublist(0, 8);
    } else {
      images = images.sublist(0, images.length);
    }
    PhotoManager.releaseCache();
  }

  _cleanPicker() {
    imagesChoosen.clear();
  }
}
