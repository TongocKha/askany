class Routes {
  static const ROOT = '/';
  static const AUTHENTICATION = '/authentication';
  static const FORGOT_PASSWORD = '/forgotPassword';
  static const REGISTER_SOCIAL = '/registerSocial';
  static const REGISTER_EXPERT = '/registerExpert';
  static const CHOOSE_ACCOUNT = '/chooseAccount';

  // Calendar
  static const CALENDAR = '/calendar';
  static const DETAILS_SERVICE = '/detailsService';

  // Chat
  static const CHAT = '/chat';
  static const CONVERSATION = '/conversation';
  static const EDIT_PHOTO_MESSAGE = '/editPhotoMessage';

  // Request
  static const CREATE_REQUEST = '/createRequest';
  static const DETAILS_REQUEST = '/detailsRequest';
  static const ORDER_SERVICE = '/orderService';
  static const FILTER_REQUEST = '/filterRequest';
  static const CREATE_REQUEST_FILTER = '/createRequestFilter';

  // Booking
  static const EXPERT_INFO = '/expertInfo';
  static const CONSULT_SERVICE_INFO = '/consultServiceInfo';
  static const WEB_VIEW_PAYMENT = '/webViewPayment';
  static const PAYMENT_RESULT = '/paymentResult';
  static const ORDER_EXPERT = '/orderExpert';

  // Video Call
  static const VIDEO_CALL = '/videoCall';

  // Account
  static const EDIT_PROFILE = '/editProfile';
  static const CHANGE_PASSWORD = '/changePassword';
  static const ACCOUNT_VERIFICATION = '/accountVerification';
  static const LANGUAGE = '/language';
  static const ACCOUNT_VERIFICATION_EMAIL = '/accountVerificationEmail';
  static const ACCOUNT_VERIFICATION_PHONE = '/accountVerificationPhone';
  static const ACCOUNT_VERIFICATION_EMAIL_SUCCESS =
      '/accountVerificationEmailSuccess';
  static const ACCOUNT_SKILL = '/accountSkill';
  static const ADD_SKILL = '/addSkill';
  static const ACCOUNT_VERTIFY_COMPANY_POSITTION =
      '/accountVertifyCompanyPosittion';
  static const RECHARGE_VNPAY = '/rechargeVNPAY';
  static const SUCCESSFUL_RECHARGE_VNPAY = '/successfulRechargeVNPAY';
  static const WALLET = '/wallet';
  static const SAVE_CREDIT_CARD = '/saveCreditCard';
  static const ADD_INFO_CARD_PAYMENT = '/addInfoCardPayment';
  static const ADD_DETAIL_CARD_PAYMENT = '/addDetailCardPayment';
  static const TRANSACTION_HISTORY = '/transactionHistory';
  static const TRANSACTION_DETAILS = '/transactionDetails';
  static const WITHDRAWMONEY = '/withdrawMoney';
  static const WITHDRAWMONEYTOTHECARD = '/withdrawMoneyToTheCard';
  static const WITHDRAWMONEYTOTHEACCOUNT = '/withdrawMoneyToTheAccount';
  static const WITHDRAWUNDER10 = '/withdrawUnder10';
  static const WITHDRAWOVER10 = 'withdrawOver10';

  //My service
  static const MY_SERVICE = '/myService';
  static const SERVICE_COMPLAINT = 'serviceComplaint';

  static const CHANGE_EMAIL = '/changeEmail';
  static const CHANGE_PHONE = '/changePhone';
  static const VERIFICATION_OTP = '/verificationOTP';
  static const PROFILE_EXPERT = '/profileExpert';
  static const INFO_EXPERT = '/infoExpert';
  static const EXPERT_EDIT_PRICE = '/expertEditPrice';
  static const CONTINUES_PAYMENT = '/continuesPayment';

  // Global
  static const DIALOG = '/dialog';
  static const EDIT_PHOTO = '/editPhoto';

  //Discover
  static const DISCOVER = '/discover';
  static const DETAIL_DISCOVER = '/detaildiscover';

  //Search
  static const SEARCH = '/search';
  static const DETAIL_SKILL_SEARCH = '/detailSkillSearch';

  //Category
  static const SUB_CATEGORY = '/subCategory';
  static const DETAILS_CATEGORY = '/detailsCategory';
  static const DETAILS_SPECIALIST = '/detailsSpecialist';
  static const SKILLS = '/skills';

  //Filter
  static const FILTER = '/filter';
  static const FILTER_EXPERT = '/filterExpert';

  //Service

  static const SERVICE_CANCEL = '/serviceCancel';
  static const SERVICE_FEEDBACK = '/serviceFeedback';
  static const REPLY_FEEDBACK = '/answerSserviceFeedback';

  // Review
  static const REVIEWSSKILL = '/reviews/skill';
  static const REVIEWSEXPERT = '/reviews/expert';

  // Experience
  static const EXPERIENCE = '/experience';
  static const ADD_EXPERIENCE = '/addExperience';

  // Photo
  static const PHOTO_VIEWER = '/photoViewer';
}
