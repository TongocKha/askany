import 'package:askany/src/app.dart';
import 'package:askany/src/routes/app_navigator_observer.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/routes/scaffold_wrapper.dart';
import 'package:askany/src/routes/transition_route.dart';
import 'package:askany/src/ui/account/screens/add_skill_screen.dart';
import 'package:askany/src/ui/account/screens/account_skill_screen.dart';
import 'package:askany/src/ui/account/screens/account_verification_phone_screen.dart';
import 'package:askany/src/ui/account/screens/account_verification_screen.dart';
import 'package:askany/src/ui/account/screens/account_verification_email_screen.dart';
import 'package:askany/src/ui/account/screens/account_verification_email_success_screen.dart';
import 'package:askany/src/ui/account/screens/account_vertification_company_screen.dart';
import 'package:askany/src/ui/account/screens/change_email_screen.dart';
import 'package:askany/src/ui/account/screens/change_password_screen.dart';
import 'package:askany/src/ui/account/screens/change_phone_screen.dart';
import 'package:askany/src/ui/account/screens/edit_profile_screen.dart';
import 'package:askany/src/ui/account/screens/experience_screen.dart';
import 'package:askany/src/ui/account/screens/expert_modify_price_screen.dart';
import 'package:askany/src/ui/account/screens/info_expert_screen.dart';
import 'package:askany/src/ui/account/screens/language_screen.dart';
import 'package:askany/src/ui/account/screens/input_otp_screen.dart';
import 'package:askany/src/ui/account/screens/profile_expert_screen.dart';
import 'package:askany/src/ui/authentication/authentication_screen.dart';
import 'package:askany/src/ui/authentication/screens/choose_account_screen.dart';
import 'package:askany/src/ui/authentication/screens/forgot_password_screen.dart';
import 'package:askany/src/ui/authentication/screens/register_expert/register_expert_screen.dart';
import 'package:askany/src/ui/authentication/screens/register_expert/screens/add_experience_screen.dart';
import 'package:askany/src/ui/authentication/screens/register_social.dart';
import 'package:askany/src/ui/booking/screens/consult_service_info_screen.dart';
import 'package:askany/src/ui/booking/screens/expert_info_screen.dart';
import 'package:askany/src/ui/booking/screens/order_expert_screen.dart';
import 'package:askany/src/ui/calendar/calendar_screen.dart';
import 'package:askany/src/ui/category/screen/filter_expert_screen.dart';
import 'package:askany/src/ui/conversation/widgets/edit_photo_message.dart';
import 'package:askany/src/ui/common/screens/photo_viewer_screen.dart';
import 'package:askany/src/ui/my_service/screens/cancel_service_screen.dart';
import 'package:askany/src/ui/my_service/screens/continues_payment_screen.dart';
import 'package:askany/src/ui/my_service/screens/service_details_screen.dart';
import 'package:askany/src/ui/category/screen/details_category_screen.dart';
import 'package:askany/src/ui/category/screen/details_expert_screen.dart';
import 'package:askany/src/ui/category/screen/skills_screen.dart';
import 'package:askany/src/ui/category/screen/sub_category_screen.dart';
import 'package:askany/src/ui/chat/chat_screen.dart';
import 'package:askany/src/ui/common/screens/edit_photo_screen.dart';
import 'package:askany/src/ui/conversation/conversation_screen.dart';
import 'package:askany/src/ui/filter/filter_screen.dart';
import 'package:askany/src/ui/home/screens/details_discover_screen.dart';
import 'package:askany/src/ui/home/screens/discover_screen.dart';
import 'package:askany/src/ui/my_service/my_service_screen.dart';
import 'package:askany/src/ui/my_service/screens/service_complaint_screen.dart';
import 'package:askany/src/ui/my_service/screens/reply_feedback_screen.dart';
import 'package:askany/src/ui/my_service/screens/user_service_feedback_screen.dart';
import 'package:askany/src/ui/request/screens/create_request_filter_screen.dart';
import 'package:askany/src/ui/request/screens/create_request_screen.dart';
import 'package:askany/src/ui/request/screens/details_request_screen.dart';
import 'package:askany/src/ui/request/screens/filter_request_screen.dart';
import 'package:askany/src/ui/request/screens/order_service_screen.dart';
import 'package:askany/src/ui/request/screens/payment_result_screen.dart';
import 'package:askany/src/ui/request/screens/web_view_payment_screen.dart';
import 'package:askany/src/ui/review_service/screens/review_expert_screen.dart';
import 'package:askany/src/ui/search/screens/details_skill_search_screen.dart';
import 'package:askany/src/ui/review_service/screens/review_skill_screen.dart';
import 'package:askany/src/ui/search/search_screen.dart';
import 'package:askany/src/ui/video_call/video_call_screen.dart';
import 'package:askany/src/ui/wallet/paying_screen/recharge_screen.dart';
import 'package:askany/src/ui/wallet/paying_screen/successful_recharge_screen.dart';
import 'package:askany/src/ui/wallet/screen/add_detail_card_payment.dart';
import 'package:askany/src/ui/wallet/screen/add_info_card_payment.dart';
import 'package:askany/src/ui/wallet/screen/save_credit_card_screen.dart';
import 'package:askany/src/ui/wallet/screen/transaction_details_screen.dart';
import 'package:askany/src/ui/wallet/screen/transaction_history_screen.dart';
import 'package:askany/src/ui/wallet/screen/withdraw_over_10_screen.dart';
import 'package:askany/src/ui/wallet/screen/withdraw_under_10_screen.dart';
import 'package:askany/src/ui/wallet/wallet_screen.dart';
import 'package:askany/src/ui/wallet/screen/withdraw_money_screen.dart';
import 'package:askany/src/ui/wallet/screen/withdraw_money_to_the_account_screen.dart';
import 'package:askany/src/ui/wallet/screen/withdraw_money_to_the_card_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AppNavigator extends RouteObserver<PageRoute<dynamic>> {
  static GlobalKey<NavigatorState> navigatorKey = GlobalKey();
  Route<dynamic> getRoute(RouteSettings settings) {
    Map<String, dynamic>? arguments = _getArguments(settings);
    switch (settings.name) {
      case Routes.ROOT:
        return _buildRoute(
          settings,
          App(),
        );

      // Authentication
      case Routes.CHOOSE_ACCOUNT:
        return _buildRoute(
          settings,
          ChooseAccountScreen(),
        );
      case Routes.AUTHENTICATION:
        return _buildRoute(
          settings,
          AuthenticateScreen(
            isExpert: arguments!['isExpert'],
          ),
        );
      case Routes.FORGOT_PASSWORD:
        return _buildRoute(
          settings,
          ForgotPasswordScreen(),
        );
      case Routes.REGISTER_SOCIAL:
        return _buildRoute(
          settings,
          RegisterSocialScreen(),
        );
      case Routes.REGISTER_EXPERT:
        return _buildRoute(
          settings,
          RegisterExpertScreen(
            expertModel: arguments?['expertModel'],
          ),
        );

      // Calendar
      case Routes.CALENDAR:
        return _buildRoute(
          settings,
          CalendarScreen(),
        );
      case Routes.DETAILS_SERVICE:
        return _buildRoute(
          settings,
          ServiceDetailsScreen(
            service: arguments?['requestModel'],
          ),
        );

      // Chat
      case Routes.CHAT:
        return _buildRoute(
          settings,
          ChatScreen(),
        );
      case Routes.CONVERSATION:
        return _buildRoute(
          settings,
          ImageDiviceProvider(
            conversationModel: arguments!['conversationModel'],
          ),
        );
      case Routes.EDIT_PHOTO_MESSAGE:
        return _buildRoute(
          settings,
          EditPhotoMessageScreen(
            file: arguments!['file'],
            handleFinish: arguments['handleFinish'],
          ),
        );
      // Request
      case Routes.CREATE_REQUEST:
        return _buildRoute(
          settings,
          CreateRequestScreen(
            request: arguments?['requestModel'],
            isNavigationFromSearch:
                arguments?['isNavigationFromSearch'] ?? false,
          ),
        );
      case Routes.DETAILS_REQUEST:
        return _buildRoute(
          settings,
          DetailsRequestScreen(
            request: arguments!['requestModel'],
            isCommonRequest: arguments['isCommonRequest'] ?? false,
          ),
        );
      case Routes.ORDER_SERVICE:
        return _buildRoute(
          settings,
          OrderServiceScreen(
            request: arguments?['request'],
            offer: arguments?['offer'],
          ),
        );

      case Routes.FILTER_REQUEST:
        return _buildRoute(
          settings,
          FilterRequestScreen(),
        );
      case Routes.CREATE_REQUEST_FILTER:
        return _buildRoute(
          settings,
          CreateRequestFilterScreen(),
        );

      case Routes.SERVICE_CANCEL:
        return _buildRoute(
          settings,
          CancelServiceScreen(
            service: arguments?['service'],
            tabNumber: arguments?['tabNumber'],
          ),
        );

      // Booking
      case Routes.EXPERT_INFO:
        return _buildRoute(
          settings,
          ExpertInfoScreen(
            expertId: arguments?['expertId'],
          ),
        );
      case Routes.CONSULT_SERVICE_INFO:
        return _buildRoute(
          settings,
          ConSultServiceInfoScreen(),
        );
      case Routes.WEB_VIEW_PAYMENT:
        return _buildRoute(
          settings,
          WebViewPaymentScreen(
            url: arguments?['url'],
            onPaymentDone: arguments?['onPaymentDone'],
          ),
        );
      case Routes.PAYMENT_RESULT:
        return _buildRoute(
          settings,
          PaymentResultScreen(
            isSuccess: arguments?['isSuccess'],
            isCheckChatScreen: arguments?['isCheckChatScreen'] ?? false,
          ),
        );

      case Routes.ORDER_EXPERT:
        return _buildRoute(
          settings,
          OrderExpertScreen(
            skillModel: arguments?['skillModel'],
            expertModel: arguments?['expertModel'],
            contactForm: arguments?['contactForm'],
          ),
        );

      // Video Call
      case Routes.VIDEO_CALL:
        return _buildRoute(
          settings,
          VideoCallScreen(
            conversation: arguments!['conversationId'],
          ),
        );

      // Account
      case Routes.EDIT_PROFILE:
        return _buildRoute(
          settings,
          EditProfileScreen(),
        );
      case Routes.CHANGE_PASSWORD:
        return _buildRoute(
          settings,
          ChangePasswordScreen(),
        );
      case Routes.ACCOUNT_VERIFICATION:
        return _buildRoute(
          settings,
          AccountVerificationScreen(),
        );
      case Routes.ACCOUNT_SKILL:
        return _buildRoute(
          settings,
          AccountSkillScreen(),
        );
      case Routes.ADD_SKILL:
        return _buildRoute(
          settings,
          AddSkillScreen(
            skill: arguments?['skillModel'],
          ),
        );
      case Routes.ACCOUNT_VERTIFY_COMPANY_POSITTION:
        return _buildRoute(
          settings,
          AccountVertifyCompanyScreen(),
        );
      case Routes.PROFILE_EXPERT:
        return _buildRoute(
          settings,
          ProfileExpertScreen(),
        );
      case Routes.INFO_EXPERT:
        return _buildRoute(
          settings,
          InfoExpertScreen(),
        );

      case Routes.LANGUAGE:
        return _buildRoute(
          settings,
          LanguageScreen(),
        );
      case Routes.ACCOUNT_VERIFICATION_EMAIL:
        return _buildRoute(
          settings,
          AccountVerificationEmailScreen(),
        );
      case Routes.ACCOUNT_VERIFICATION_EMAIL_SUCCESS:
        return _buildRoute(
          settings,
          AccountVerificationEmailSuccessScreen(),
        );
      case Routes.CHANGE_EMAIL:
        return _buildRoute(
          settings,
          ChangeEmailScreen(),
        );
      case Routes.CHANGE_PHONE:
        return _buildRoute(
          settings,
          ChangePhoneScreen(),
        );
      case Routes.VERIFICATION_OTP:
        return _buildRoute(
          settings,
          InputOTPScreen(phone: arguments!['phone']),
        );
      case Routes.ACCOUNT_VERIFICATION_PHONE:
        return _buildRoute(
          settings,
          AccountVerificationPhoneScreen(),
        );

      //Wallet
      case Routes.RECHARGE_VNPAY:
        return _buildRoute(
          settings,
          RechargeScreen(),
        );
      case Routes.SUCCESSFUL_RECHARGE_VNPAY:
        return _buildRoute(
            settings,
            SuccessfulRechargeVNPAY(
              money: arguments!['money'],
            ));
      case Routes.WALLET:
        return _buildRoute(
          settings,
          WalletScreen(),
        );
      case Routes.SAVE_CREDIT_CARD:
        return _buildRoute(
          settings,
          SaveCreditCard(),
        );
      case Routes.ADD_INFO_CARD_PAYMENT:
        return _buildRoute(
          settings,
          AddInfoCardPayment(),
        );
      case Routes.ADD_DETAIL_CARD_PAYMENT:
        return _buildRoute(
          settings,
          AddDetailCardPayment(
            nameCreditCard: arguments!['nameCreditCard'],
            imageCreditCard: arguments['imageCreditCard'],
            paymentCardModel: arguments['paymentCardModel'],
          ),
        );
      case Routes.TRANSACTION_HISTORY:
        return _buildRoute(
          settings,
          TransactionHistory(),
        );
      case Routes.TRANSACTION_DETAILS:
        return _buildRoute(
            settings,
            TransactionDetails(
              transaction: arguments!['transaction'],
            ));
      case Routes.WITHDRAWMONEY:
        return _buildRoute(
          settings,
          WithdrawMoney(),
        );
      case Routes.WITHDRAWMONEYTOTHECARD:
        return _buildRoute(
          settings,
          WithdrawMoneyToTheCard(),
        );
      case Routes.WITHDRAWMONEYTOTHEACCOUNT:
        return _buildRoute(
          settings,
          WithDrawMoneyToTheAccount(),
        );
      case Routes.WITHDRAWUNDER10:
        return _buildRoute(
          settings,
          WithdrawUnder10(
            id: arguments?['id'],
            amount: arguments?['amount'],
            type: arguments?['type'],
          ),
        );
      case Routes.WITHDRAWOVER10:
        return _buildRoute(
          settings,
          WithdrawOver10(
            id: arguments?['id'],
            amount: arguments!['amount'],
            type: arguments['type'],
          ),
        );

      //My service
      case Routes.MY_SERVICE:
        return _buildRoute(
          settings,
          MyServiceScreen(
            initialIndex: arguments!['index'],
          ),
        );

      case Routes.CONTINUES_PAYMENT:
        return _buildRoute(
          settings,
          ContinuesPaymentScreen(
            request: arguments?['request'],
          ),
        );
      case Routes.SERVICE_COMPLAINT:
        return _buildRoute(
          settings,
          ServiceComplaintScreen(
            service: arguments!['service'],
          ),
        );
      case Routes.EXPERT_EDIT_PRICE:
        return _buildRoute(
          settings,
          ExpertEditPriceScreen(),
        );

      // Global
      case Routes.EDIT_PHOTO:
        return _buildRoute(
          settings,
          EditPhotoScreen(
            image: arguments!['image'],
            handleFinish: arguments['handleFinish'],
          ),
        );
      //Discover

      case Routes.DISCOVER:
        return _buildRoute(
          settings,
          DiscoverScreen(),
        );

      case Routes.DETAIL_DISCOVER:
        return _buildRoute(
          settings,
          DetailsDiscoverScreen(
            discoverModel: arguments!['discoverModel'],
          ),
        );
      case Routes.SEARCH:
        return _buildRoute(
          settings,
          SearchScreen(),
        );

      // Category

      case Routes.SUB_CATEGORY:
        return _buildRoute(
          settings,
          SubCategoryScreen(
            categoryInfo: arguments!['categoryInfo'],
          ),
        );
      case Routes.DETAILS_CATEGORY:
        return _buildRoute(
          settings,
          DetailsCategoryScreen(
            categoryModel: arguments?['categoryModel'],
          ),
        );
      case Routes.DETAILS_SPECIALIST:
        return _buildRoute(
          settings,
          DetailsExpertScreen(
            expertId: arguments?['expertId'],
            specialtyId: arguments?['specialtyId'],
          ),
        );

      case Routes.FILTER:
        return _buildRoute(
          settings,
          FilterScreen(),
        );
      case Routes.FILTER_EXPERT:
        return _buildRoute(
          settings,
          FilterExpertScreen(),
        );
      case Routes.DETAIL_SKILL_SEARCH:
        return _buildRoute(
          settings,
          DetailsSkillSearchScreen(
            skillModel: arguments?['skillModel'],
            expertModel: arguments?['expertModel'],
          ),
        );
      case Routes.REVIEWSSKILL:
        return _buildRoute(
          settings,
          ReviewSkillScreen(
            skillId: arguments?['skillId'],
          ),
        );

      case Routes.REVIEWSEXPERT:
        return _buildRoute(
          settings,
          ReviewExpertScreen(
            expertModel: arguments?['expertModel'],
            ratingSingleCounts: arguments?['ratingSingleCounts'],
          ),
        );

      case Routes.SKILLS:
        return _buildRoute(
          settings,
          SkillsScreen(
            expertModel: arguments?['expertModel'],
          ),
        );

      // ServiceManagament

      case Routes.SERVICE_FEEDBACK:
        return _buildRoute(
          settings,
          ServiceFeedbackScreen(
            service: arguments!['service'],
          ),
        );

      case Routes.REPLY_FEEDBACK:
        return _buildRoute(
          settings,
          ReplyFeedbackScreen(
            service: arguments!['service'],
          ),
        );

      // Experience
      case Routes.EXPERIENCE:
        return _buildRoute(
          settings,
          ExperienceScreen(),
        );
      case Routes.ADD_EXPERIENCE:
        return _buildRoute(
          settings,
          AddExperienceScreen(
            experience: arguments?['experienceModel'],
          ),
        );

      // Photo
      case Routes.PHOTO_VIEWER:
        return _buildRoute(
          settings,
          PhotoViewScreen(
            images: arguments!['images'],
            indexListImage: arguments['indexListImage'],
            initPosition: arguments['initialIndex'],
            urlToImages: arguments['urlToImages'],
          ),
        );

      default:
        return _buildRoute(settings, AccountVerificationPhoneScreen());
    }
  }

  _buildRoute(
    RouteSettings routeSettings,
    Widget builder,
  ) {
    return AppMaterialPageRoute(
      builder: (context) => ScaffoldWrapper(child: builder),
      settings: routeSettings,
    );
  }

  _getArguments(RouteSettings settings) {
    return settings.arguments;
  }

  static Future push<T>(
    String route, {
    Object? arguments,
  }) {
    return state.pushNamed(route, arguments: arguments);
  }

  static Future pushNamedAndRemoveUntil<T>(
    String route, {
    Object? arguments,
  }) {
    return state.pushNamedAndRemoveUntil(
      route,
      (route) => false,
      arguments: arguments,
    );
  }

  static Future replaceWith<T>(
    String route, {
    Map<String, dynamic>? arguments,
  }) {
    return state.pushReplacementNamed(route, arguments: arguments);
  }

  static void popUntil<T>(String route) {
    state.popUntil(ModalRoute.withName(route));
  }

  static void pop() {
    if (canPop) {
      state.pop();
    }
  }

  static bool get canPop => state.canPop();

  static String? currentRoute() => AppNavigatorObserver.currentRouteName;

  static BuildContext? get context => navigatorKey.currentContext;

  static NavigatorState get state => navigatorKey.currentState!;
}
