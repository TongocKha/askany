import 'dart:convert';

class TimeLineHardData {
  final String title;
  final List<String> urlToImages;
  final DateTime startTime;
  final DateTime endTime;
  final bool isCancel;
  TimeLineHardData({
    required this.title,
    required this.urlToImages,
    required this.startTime,
    required this.endTime,
    this.isCancel = false,
  });

  TimeLineHardData copyWith({
    String? title,
    String? description,
    List<String>? urlToImages,
    DateTime? date,
  }) {
    return TimeLineHardData(
      title: title ?? this.title,
      urlToImages: urlToImages ?? this.urlToImages,
      startTime: date ?? this.startTime,
      endTime: date ?? this.endTime,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'title': title,
      'urlToImages': urlToImages,
      'date': startTime.millisecondsSinceEpoch,
    };
  }

  factory TimeLineHardData.fromMap(Map<String, dynamic> map) {
    return TimeLineHardData(
      title: map['title'] ?? '',
      urlToImages: List<String>.from(map['urlToImages']),
      startTime: DateTime.fromMillisecondsSinceEpoch(map['startTime']),
      endTime: DateTime.fromMillisecondsSinceEpoch(map['endTime']),
    );
  }

  String toJson() => json.encode(toMap());

  factory TimeLineHardData.fromJson(String source) => TimeLineHardData.fromMap(json.decode(source));

  @override
  String toString() {
    return 'TimeLineHardData(title: $title, urlToImages: $urlToImages, date: $startTime)';
  }
}

List<TimeLineHardData> timelinesFaker = [
  TimeLineHardData(
    title: 'Tư vấn hồ sơ du học Mỹ bao đậu năm 2021',
    urlToImages: [
      'https://avatars.githubusercontent.com/u/60530946?v=4',
      'https://avatars.githubusercontent.com/u/67500361?v=4',
    ],
    startTime: DateTime.now().subtract(Duration(hours: 2)),
    endTime: DateTime.now().subtract(Duration(hours: 1)),
  ),
  TimeLineHardData(
    title: 'Tư vấn những ngành đang hot khi du học Mỹ',
    urlToImages: [
      'https://avatars.githubusercontent.com/u/60530946?v=4',
      'https://avatars.githubusercontent.com/u/67500361?v=4',
    ],
    startTime: DateTime.now().add(Duration(hours: 2)),
    endTime: DateTime.now().add(Duration(hours: 3)),
  ),
  TimeLineHardData(
    title: 'Tư vấn chi phí nhà ở và cách tìm nhà ở khi du học Mỹ',
    urlToImages: [
      'https://avatars.githubusercontent.com/u/60530946?v=4',
      'https://avatars.githubusercontent.com/u/67500361?v=4',
    ],
    startTime: DateTime.now().add(Duration(hours: 4)),
    endTime: DateTime.now().add(Duration(hours: 6)),
  ),
  TimeLineHardData(
    title: 'Tư vấn phim sẽ gầy hay nhất 2021',
    urlToImages: [
      'https://avatars.githubusercontent.com/u/60530946?v=4',
      'https://avatars.githubusercontent.com/u/67500361?v=4',
    ],
    startTime: DateTime.now().add(Duration(days: 2)),
    endTime: DateTime.now().add(Duration(days: 2, hours: 1)),
  ),
];
