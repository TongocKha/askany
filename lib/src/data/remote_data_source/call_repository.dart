import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/constants/endpoints.dart';
import 'package:askany/src/constants/http_status_code.dart';
import 'package:askany/src/data/remote_data_source/base_repository.dart';
import 'package:dio/dio.dart';

class CallRepository {
  Future<String?> createCall({
    required String conversationId,
    required String sdp,
    required String receiverId,
  }) async {
    Map<String, dynamic> body = {
      'sdp': sdp,
      'room': conversationId,
      'receiverId': receiverId,
      'createdBy': AppBloc.userBloc.getAccount.id,
    };

    Response response = await BaseRepository().postRoute(Endpoints.CALL, body);

    if (response.statusCode == StatusCode.CREATED) {
      return response.data['data']['_id'];
    }

    return null;
  }

  Future<bool> endCall({
    required String callId,
  }) async {
    Map<String, dynamic> body = {
      'status': END_CALL,
    };

    Response response = await BaseRepository().putRoute(Endpoints.CALL + '/$callId', body);

    if (response.statusCode == StatusCode.OK) {
      return true;
    }

    return false;
  }
}
