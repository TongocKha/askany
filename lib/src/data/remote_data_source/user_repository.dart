import 'dart:io';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/constants/endpoints.dart';
import 'package:askany/src/constants/http_status_code.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/helpers/path_helper.dart';
import 'package:askany/src/models/account_model.dart';
import 'package:askany/src/models/budget_model.dart';
import 'package:askany/src/models/expert_model.dart';
import 'package:askany/src/models/time_line_price_model.dart';
import 'package:dio/dio.dart';
import 'package:http_parser/http_parser.dart';

import 'base_repository.dart';

class UserRepository {
  Future<List> getUserByToken() async {
    Response response = await BaseRepository().getRoute(
      UserLocal().getIsExpert() ? Endpoints.GET_EXPERT_BY_TOKEN : Endpoints.GET_USER_BY_TOKEN,
    );
    if (response.statusCode == StatusCode.OK) {
      var dataJson = response.data['data']['infoUser'];
      ExpertModel? expertModel =
          UserLocal().getIsExpert() ? ExpertModel.fromMapProfile(dataJson) : null;
      return [AccountModel.fromMap(dataJson), expertModel];
    } else {
      return [null, null];
    }
  }

  Future<AccountModel?> updateInfoUserById({
    required String fullname,
    required String province,
    File? image,
  }) async {
    Map<String, dynamic> body = {
      'fullname': fullname,
      'province': province,
    };
    if (image != null) {
      body['avatar'] = await MultipartFile.fromFile(
        image.path,
        filename: image.path.split('/').last,
        contentType: MediaType('image', image.path.split('.').last.toString()),
      );
    }
    FormData formData = FormData.fromMap(body);

    Response response = await BaseRepository().putFormData(
      UserLocal().getIsExpert() ? Endpoints.EXPERTS : Endpoints.UPDATE_INFO_USER,
      formData,
    );

    if (image != null) {
      await PathHelper.deleteCacheImageDir(image.path);
    }

    if (response.statusCode == StatusCode.OK) {
      var dataJson = response.data['data'];
      return AccountModel.fromMap(dataJson);
    } else {
      return null;
    }
  }

  Future<bool> updateInfoExpert({
    required List<String> languages,
    required List<String> specialties,
    required String experienceYears,
    required String positionId,
    required String companyName,
    required String introduce,
  }) async {
    Map<String, dynamic> body = {
      'languages': languages.join(','),
      'specialties': specialties.join(','),
      'experienceYears': experienceYears,
      'companyName': companyName,
      'introduce': introduce,
      'highestPosition': positionId,
    };

    Response response = await BaseRepository().putRoute(
      Endpoints.EXPERTS,
      body,
    );

    if (response.statusCode == StatusCode.OK) {
      return true;
    }

    return false;
  }

  Future<bool> updateSericePrice({
    required BudgetModel callPrice,
    required BudgetModel meetPrice,
    required List<TimelinePriceModel> timelines,
  }) async {
    var body = {
      'callPrice': callPrice.toJsonUpdate(),
      'meetPrice': meetPrice.toJsonUpdate(),
    };

    FormData form = FormData.fromMap(body);

    timelines.forEach((timeline) {
      if (timeline.weeklySchedules.isNotEmpty &&
          timeline.weeklySchedules.first.timesCall.isNotEmpty) {
        form.fields.add(MapEntry('timeLine', timeline.toJsonUpdate()));
      }
    });

    Response response = await BaseRepository().putFormData(
      Endpoints.SERVICE_PRICE,
      form,
    );

    if (response.statusCode == StatusCode.OK) {
      return true;
    }

    return false;
  }

  Future<List<ExpertModel>> getExperts({
    required int skip,
    int limit = LIMIT_API_5,
  }) async {
    Response response = await BaseRepository().getRoute(
      Endpoints.RECOMMEND_EXPERTS,
      query: 'skip=$skip&limit=$limit',
    );
    if (response.statusCode == StatusCode.OK) {
      List rawData = response.data['data']?['listData'] ?? [];
      return rawData.map((expert) => ExpertModel.fromMap(expert)).toList();
    }
    return [];
  }

  Future<ExpertModel?> getExpertInfoByToken() async {
    Response response = await BaseRepository().getRoute(
      Endpoints.INFO_BY_TOKEN,
    );
    if (response.statusCode == StatusCode.OK) {
      final rawData = response.data['data']['expert'];

      if (rawData == null) {
        return null;
      }

      return ExpertModel.fromMapProfile(rawData);
    }
    return null;
  }

  Future<bool> userRegisterExpert() async {
    Response response = await BaseRepository().postRoute(
      Endpoints.USER_REGISTER_EXPERT,
      {},
    );

    if (response.statusCode == StatusCode.CREATED) {
      return true;
    }
    return false;
  }

  Future<bool?> blocExpert({
    required String expertId,
  }) async {
    var body = {
      'expert': expertId,
    };

    Response response = await BaseRepository().putRoute(
      Endpoints.BLOCK_EXPERT,
      body,
    );

    if (response.statusCode == StatusCode.OK) {
      return true;
    } else {
      String message = response.data['message'];
      if (message == BLOC_EXPERT_EXISTS) {
        return true;
      }
    }
    return false;
  }
}
