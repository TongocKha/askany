import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/constants/endpoints.dart';
import 'package:askany/src/constants/http_status_code.dart';
import 'package:askany/src/models/position_model.dart';
import 'package:askany/src/models/search_model.dart';
import 'package:askany/src/models/skill_model.dart';
import 'package:dio/dio.dart';

import 'base_repository.dart';

class SkillRepository {
  Future<List<SkillModel>> getListSkill({
    required int skip,
    required String expertId,
    int status = 1,
    int limit = 10,
  }) async {
    Response response = await BaseRepository().getRoute(
      Endpoints.GET_SKILL + '/' + expertId,
      query: 'status=$status&skip=$skip&limit=$limit',
    );

    if (response.statusCode == StatusCode.OK) {
      final skills = response.data['data'] as List;
      return skills.map((skill) => SkillModel.fromMap(skill)).toList();
    }

    return [];
  }

  Future<SkillModel?> createSkill({required SkillModel skill}) async {
    FormData form = FormData.fromMap(skill.toMapForCreate());

    Response response = await BaseRepository().sendFormData(
      Endpoints.GET_SKILL,
      form,
    );

    if (response.statusCode == StatusCode.CREATED) {
      return SkillModel.fromCreateRequest(response.data['data']);
    }
    return null;
  }

  Future<SkillModel?> updateSkill({required SkillModel skill}) async {
    FormData form = FormData.fromMap(skill.toMapForCreate());
    Response response = await BaseRepository().putFormData(
      Endpoints.GET_SKILL + '/${skill.id}',
      form,
    );
    if (response.statusCode == StatusCode.OK) {
      return SkillModel.fromCreateRequest(response.data['data']);
    }
    return null;
  }

  Future<bool> deleteSkill({required String id}) async {
    Response response = await BaseRepository().deleteRoute(Endpoints.GET_SKILL + '/$id');
    if (response.statusCode == StatusCode.OK) {
      return true;
    }
    return false;
  }

  Future<List<PositionModel>> getPositions() async {
    Response response = await BaseRepository().getRoute(
      Endpoints.POSITIONS,
      query: 'limit=$UNLIMITED_API',
    );
    if (response.statusCode == StatusCode.OK) {
      List dataJson = response.data['data'];
      return dataJson.map((position) => PositionModel.fromMap(position)).toList();
    }
    return [];
  }

  Future<List> searchSkill({
    required SearchModel searchModel,
    required int skip,
    int limit = LIMIT_API_5,
  }) async {
    Response response = await BaseRepository().getRoute(
      Endpoints.SEARCH_SKILL,
      query: searchModel.query + 'skip=$skip&limit=$limit',
    );

    if (response.statusCode == StatusCode.OK) {
      List skills = response.data['data']['listData'];
      int totalSkill = response.data['data']['total'];
      return [skills.map((position) => SkillModel.fromMapSearch(position)).toList(), totalSkill];
    }
    return [[], 0];
  }

  Future<List<SkillModel>?> getListSkillRecommend({
    required String key,
    required int skip,
    int limit = LIMIT_API_15,
  }) async {
    Response response = await BaseRepository().getRoute(
      Endpoints.RECOMMEND_SKILLS,
      query: 'key=$key&limit=$limit&skip=$skip',
    );

    if (response.statusCode == StatusCode.OK) {
      List listSkill = response.data['data']?['listData'] ?? [];
      return listSkill.map((skill) => SkillModel.fromMapSearch(skill)).toList();
    }
    return [];
  }

  Future<SkillModel?> getInfoSkill({
    required String skillId,
  }) async {
    Response response = await BaseRepository().getRoute(
      Endpoints.INFO_SKILLS + '/' + skillId,
    );

    if (response.statusCode == StatusCode.OK) {
      final skillModel = response.data['data'];
      return SkillModel.fromMapInfoSkill(skillModel);
    }

    return null;
  }
}
