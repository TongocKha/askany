import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/constants/endpoints.dart';
import 'package:askany/src/constants/http_status_code.dart';
import 'package:askany/src/data/remote_data_source/base_repository.dart';
import 'package:askany/src/models/specialty_model.dart';
import 'package:dio/dio.dart';

class SpecialtyRepository {
  Future<List<SpecialtyModel>> getSpecialties({
    required int skip,
    int limit = UNLIMITED_QUANTITY,
  }) async {
    Response response = await BaseRepository().getRoute(Endpoints.SPECIALTY, query: 'limit=$limit');

    if (response.statusCode == StatusCode.OK) {
      final spectialties = response.data['data']['listSpecialties'] as List;
      return spectialties
          .where((spectialty) => spectialty['vi'] != null)
          .map((spectialty) => SpecialtyModel.fromMap(spectialty))
          .toList();
    }

    return [];
  }

  Future<List<SpecialtyModel>> getMySpecialties({required int skip, int limit = 30}) async {
    Response response = await BaseRepository().getRoute(Endpoints.MY_SPECIALTY);
    if (response.statusCode == StatusCode.OK) {
      final spectialties = response.data['data']['specialties'] as List;
      return spectialties.map((spectialty) => SpecialtyModel.fromMap(spectialty)).toList();
    }

    return [];
  }
}
