import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/badge/badge_bloc.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/constants/endpoints.dart';
import 'package:askany/src/constants/http_status_code.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/data/remote_data_source/base_repository.dart';
import 'package:askany/src/models/notification_model.dart';
import 'package:askany/src/models/request_model.dart';
import 'package:dio/dio.dart';

class NotificationRepository {
  Future<List<NotificationModel>> getNotifications({required int skip}) async {
    Response response = await BaseRepository().getRoute(
      Endpoints.NOTIFICATIONS_LIST + '/${UserLocal().getIsExpert() ? 'expert' : 'user'}',
      query: 'skip=$skip&limit=$LIMIT_API_10',
    );

    if (response.statusCode == StatusCode.OK) {
      AppBloc.badgesBloc.add(
        GetBadgesNotificationEvent(
          badges: response.data['data']['amountMessageUnSeen'],
        ),
      );
      List rawData = response.data['data']['listData'];
      return rawData.map((item) => NotificationModel.fromMap(item)).toList();
    }

    return [];
  }

  Future<bool> markAsRead({required String notificationId, int status = 1}) async {
    Response response = await BaseRepository().putRoute(
      Endpoints.NOTIFICATIONS +
          // '/${UserLocal().getIsExpert() ? 'expert' : 'user'}' +
          '/$notificationId',
      {'status': status},
    );

    if (response.statusCode == StatusCode.OK) {
      return true;
    }

    return false;
  }

  Future<bool> markAsReadAll() async {
    Response response = await BaseRepository().patchRoute(
      Endpoints.NOTIFICATIONS_SEEN_ALL + '/${UserLocal().getIsExpert() ? ROLE_EXPERT : ROLE_USER}',
    );

    if (response.statusCode == StatusCode.OK) {
      return true;
    }

    return false;
  }

  Future<bool> deleteNotification({required String notificationId}) async {
    Response response = await BaseRepository().deleteRoute(
      Endpoints.NOTIFICATIONS_LIST + '/$notificationId',
    );

    if (response.statusCode == StatusCode.OK) {
      return true;
    }

    return false;
  }

  Future<NotificationModel?> createNotification({
    required int typeMessage,
    required String userId,
    String? requestId,
    String? offerId,
  }) async {
    var body = {
      'typeMessage': typeMessage.toString(),
      // 'type': type.toString(),
    };

    body[UserLocal().getIsExpert() ? ROLE_USER : ROLE_EXPERT] = userId;

    if (requestId != null) {
      body['request'] = requestId;
    }

    if (offerId != null) {
      body['offer'] = offerId;
    }

    Response response = await BaseRepository().postRoute(
      Endpoints.NOTIFICATIONS + '/${UserLocal().getIsExpert() ? ROLE_EXPERT : ROLE_USER}',
      body,
    );

    if (response.statusCode == StatusCode.CREATED) {
      return NotificationModel.fromMap(response.data['data']);
    }

    return null;
  }

  Future<RequestModel?> getOfferFromId({required String offerId}) async {
    Response response = await BaseRepository().getRoute(Endpoints.GET_OFFER_BY_ID + '/$offerId');
    if (response.statusCode == StatusCode.OK) {
      return RequestModel.fromOfferRequest(response.data['data']);
    }
    return null;
  }
}
