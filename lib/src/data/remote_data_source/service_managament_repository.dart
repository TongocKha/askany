import 'package:askany/src/configs/lang/language_service.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/constants/endpoints.dart';
import 'package:askany/src/constants/http_status_code.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/models/cancel_service_reason_model.dart';
import 'package:askany/src/models/report_model.dart';
import 'package:askany/src/models/request_model.dart';
import 'package:askany/src/models/service_feedback_model.dart';
import 'package:dio/dio.dart';

import 'base_repository.dart';

class ServiceManagamentRepository {
  Future<List<ReasonModel?>> getCancelReasons() async {
    Response response = await BaseRepository().getCancelReasons();
    if (response.statusCode == StatusCode.OK) {
      final reasons = response.data['data'] as List;

      return reasons.map(
        (element) {
          if (!LanguageService.getIsLanguage('vi')) {
            if (element['en'] != null) {
              return ReasonModel.fromMap(element);
            }
          } else {
            return ReasonModel.fromMap(element);
          }
        },
      ).toList();
    }
    return [];
  }

  Future<List<RequestModel>> getListRequestStatus({
    required int status,
    required int skip,
    int limit = LIMIT_API_5,
  }) async {
    Response response = await BaseRepository().getRoute(
      (UserLocal().getIsExpert()
              ? Endpoints.EXPERT_SERVICE_MANAGAMENT
              : Endpoints.USER_SERVICE_MANAGAMENT) +
          '/$status?',
      query: 'skip=$skip&limit=$limit',
    );

    if (response.statusCode == StatusCode.OK) {
      final requests = response.data['data']['listData'] as List;

      return requests
          .where((request) => request['offeredRequest'] != null)
          .map((request) => RequestModel.fromOfferRequest(request))
          .toList();
    }

    return [];
  }

  Future<String?> cancelOffer({
    required String offerId,
    required String reason,
    required String content,
  }) async {
    var body = {
      'reason': reason,
    };

    if (content.trim().isNotEmpty) {
      body['content'] = content.trim();
    }

    Response response = await BaseRepository().patchRoute(
      (UserLocal().getIsExpert()
              ? Endpoints.EXPERT_CANCEL_OFFER
              : Endpoints.USER_CANCEL_OFFER) +
          '/$offerId',
      body: body,
    );
    if (response.statusCode == StatusCode.OK) {
      return response.data['data']['conversationID'];
    }

    return null;
  }

  Future<bool> expertConfirmOffer({required String offerId}) async {
    Response response = await BaseRepository()
        .patchRoute(Endpoints.EXPERT_CONFIRM_OFFER + '/$offerId');

    if (response.statusCode == StatusCode.OK) {
      return true;
    }
    return false;
  }

  Future<bool> userCompleteOffer({required String offerId}) async {
    Response response = await BaseRepository()
        .patchRoute(Endpoints.USER_COMPLETE_OFFER + '/$offerId');

    if (response.statusCode == StatusCode.OK) {
      return true;
    }
    return false;
  }

  Future<bool> userRatingAndFeedback(
      {required String offerId,
      required RatingModel serviceFeedbackModel}) async {
    Response response = await BaseRepository().patchRoute(
      Endpoints.USER_RATING_OFFER + '/$offerId',
      body: {
        'stars': serviceFeedbackModel.stars,
        'content': serviceFeedbackModel.content
      },
    );

    if (response.statusCode == StatusCode.OK) {
      return true;
    }
    return false;
  }

  Future<ReportModel?> reportOffer(
      {required String offerId, required String content}) async {
    var body = {'content': content};
    Response response = await BaseRepository()
        .patchRoute(Endpoints.REPORT_OFFER + '/$offerId', body: body);
    if (response.statusCode == StatusCode.OK) {
      final _reportModel = ReportModel.fromMap(response.data['data']);
      return _reportModel;
    }
    return null;
  }

  Future<bool> expertReplyFeedback({
    required String ratingId,
    required String content,
  }) async {
    String param = Endpoints.EXPERT_REPLY_FEEDBACK + '/$ratingId' + '/reply';
    var body = {'content': content};
    Response response = await BaseRepository().postRoute(param, body);

    if ([StatusCode.OK, StatusCode.CREATED].contains(response.statusCode)) {
      return true;
    }
    return false;
  }

  Future<RequestModel?> getOfferFromId({required String offerId}) async {
    Response response = await BaseRepository()
        .getRoute(Endpoints.GET_OFFER_BY_ID + '/$offerId');
    if (response.statusCode == StatusCode.OK) {
      return RequestModel.fromOfferRequest(response.data['data']);
    }
    return null;
  }
}
