import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/constants/endpoints.dart';
import 'package:askany/src/constants/http_status_code.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/data/remote_data_source/base_repository.dart';
import 'package:askany/src/models/account_model.dart';
import 'package:askany/src/models/enums/authentication_fail.dart';
import 'package:askany/src/models/social_model.dart';
import 'package:dio/dio.dart';

class AuthRepository {
  Future<AccountModel?> login({
    required String email,
    required String password,
    required bool isExpert,
  }) async {
    Response response = await BaseRepository().postRoute(
      Endpoints.LOGIN,
      {
        'email': email,
        'password': password,
      },
    );

    if (response.statusCode == StatusCode.OK) {
      var dataJson = response.data['data'];
      String token = dataJson['token'];
      bool isExpertRaw = dataJson['isExpert'];
      dynamic infoUser = dataJson['infoExpert'] ?? dataJson['infoUser'];
      List skills = dataJson['infoExpert']?['skills'] ?? [];

      AccountModel accountModel = AccountModel.fromMap(infoUser);

      UserLocal().saveAccessToken(token);
      UserLocal().saveIsExpert(isExpert);
      if (isExpert) {
        if (isExpertRaw && skills.isNotEmpty) {
          // Switch token user to expert
          String? tokenExpert = await switchToken();
          if (tokenExpert == null) {
            return null;
          }
          UserLocal().saveAccount(accountModel);

          return accountModel;
        } else {
          UserLocal().saveBackupToken(token);
          UserLocal().clearAccessToken();
          throw AuthenticationException.NOT_REGISTRATION_EXPERT;
        }
      } else {
        String? tokenExpert = await switchToken(role: ROLE_USER);
        if (tokenExpert == null) {
          return null;
        }
        UserLocal().saveAccount(accountModel);

        return accountModel;
      }
    } else {
      String message = response.data['message'];
      if (message == WRONG_PASSWORD) {
        throw AuthenticationException.WRONG_PASSWORD;
      } else if (message == UNCONFIRMED_ACCOUNT) {
        throw AuthenticationException.UNCONFIRMED_ACCOUNT;
      } else {
        throw AuthenticationException.EMAIL_NOT_EXISTS;
      }
    }
  }

  Future<AccountModel?> register({
    required String email,
    required String password,
    required String fullname,
    required int province,
    required String phone,
  }) async {
    var body = {
      'email': email,
      'password': password,
      'fullname': fullname,
      'province': province,
      'phone': phone
    };
    Response response = await BaseRepository().postRoute(
      Endpoints.REGISTER,
      body,
    );

    if (response.statusCode == StatusCode.OK) {
      var dataJson = response.data['data'];
      AccountModel accountModel = AccountModel.fromMap(dataJson);
      return accountModel;
    } else {
      String message = response.data['message'];
      if (message == UNCONFIRMED_ACCOUNT) {
        throw AuthenticationException.UNCONFIRMED_ACCOUNT;
      } else if (message == PHONE_EXISTS) {
        throw AuthenticationException.PHONE_EXISTS;
      } else if (message == EMAIL_EXISTS) {
        throw AuthenticationException.EMAIL_EXISTS;
      } else {
        throw AuthenticationException.SOCIAL_EXISTS;
      }
    }
  }

  Future<AccountModel?> registerSocial({
    required SocialModel social,
    required bool isExpert,
  }) async {
    Response response = await BaseRepository().postRoute(
      Endpoints.REGISTER,
      social.toMap(),
    );

    if (response.statusCode == StatusCode.OK) {
      var dataJson = response.data['data'];
      String token = dataJson['token'];
      bool isExpertRaw = dataJson['isExpert'];
      dynamic infoUser = dataJson['infoExpert'] ?? dataJson['infoUser'];
      List skills = dataJson['infoExpert']?['skills'] ?? [];

      AccountModel accountModel = AccountModel.fromMap(infoUser);

      UserLocal().saveAccessToken(token);
      UserLocal().saveIsExpert(isExpert);
      if (isExpert) {
        if (isExpertRaw && skills.isNotEmpty) {
          // Switch token user to expert
          String? tokenExpert = await switchToken();
          if (tokenExpert == null) {
            return null;
          }
          UserLocal().saveAccount(accountModel);

          return accountModel;
        } else {
          UserLocal().saveBackupToken(token);
          UserLocal().clearAccessToken();
          throw AuthenticationException.NOT_REGISTRATION_EXPERT;
        }
      } else {
        String? tokenExpert = await switchToken(role: ROLE_USER);
        if (tokenExpert == null) {
          return null;
        }
        UserLocal().saveAccount(accountModel);

        return accountModel;
      }
    } else {
      String message = response.data['message'];
      if (message == WRONG_PASSWORD) {
        throw AuthenticationException.WRONG_PASSWORD;
      } else if (message == UNCONFIRMED_ACCOUNT) {
        throw AuthenticationException.UNCONFIRMED_ACCOUNT;
      } else {
        throw AuthenticationException.EMAIL_NOT_EXISTS;
      }
    }
  }

  Future<bool> changePassword({
    required String password,
    required String newPassword,
  }) async {
    var body = {
      'password': password,
      'newPassword': newPassword,
    };

    Response response = await BaseRepository().patchRoute(
      Endpoints.CHANGE_PASSWORD,
      body: body,
    );

    return response.statusCode == StatusCode.OK;
  }

  Future<bool> forgotPassword({
    required String email,
  }) async {
    var body = {
      'email': email,
    };

    Response response = await BaseRepository().patchRoute(Endpoints.FORGOT_PASSWORD, body: body);

    return response.statusCode == StatusCode.OK;
  }

  Future<bool> refreshToken() async {
    Response response = await BaseRepository().getRoute(Endpoints.REFRESH_TOKEN);

    if (response.statusCode == StatusCode.OK) {
      UserLocal().saveAccessToken(response.data['data']);
    }

    return response.statusCode == StatusCode.OK;
  }

  Future<String?> switchToken({String role = ROLE_EXPERT}) async {
    Response response = await BaseRepository().getRoute(Endpoints.SWITCH_TOKEN + '/$role');

    if (response.statusCode == StatusCode.OK) {
      String token = response.data['data'];
      UserLocal().saveAccessToken(token);
      return token;
    }
    return null;
  }

  // Xác thực email
  Future<bool> updateEmail({required String email}) async {
    Map<String, dynamic> body = {'email': email};
    Response response = await BaseRepository().patchRoute(Endpoints.UPDATE_EMAIL, body: body);
    return response.statusCode == StatusCode.OK;
  }

  //xác thực sdt
  Future<bool> updatePhone({required String phone}) async {
    Map<String, dynamic> body = {'phone': phone};
    Response response = await BaseRepository().patchRoute(Endpoints.UPDATE_PHONE, body: body);
    return response.statusCode == StatusCode.OK;
  }

  Future<bool> verifyOTP({required String phone, required String code}) async {
    Map<String, dynamic> body = {'phone': phone, 'code': code};
    Response response = await BaseRepository().patchRoute(Endpoints.VERIFY_OTP, body: body);
    return response.statusCode == StatusCode.OK;
  }

  // xác thực phone
  Future<bool> verifyPhone() async {
    Response response = await BaseRepository().getRoute(Endpoints.VERIFY_PHONE);
    return response.statusCode == StatusCode.OK;
  }

  // xác thực công ty chuyên gia
  Future<bool> verifyCompany({
    required String linkedinURL,
    required String images,
  }) async {
    var body = {
      'linkedinURL': linkedinURL,
      'images': images,
    };
    Response response = await BaseRepository().postRoute(Endpoints.VERIFY_COMPANY, body);
    return response.statusCode == StatusCode.OK;
  }
}
