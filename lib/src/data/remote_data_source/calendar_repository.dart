import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/constants/endpoints.dart';
import 'package:askany/src/constants/http_status_code.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/data/remote_data_source/base_repository.dart';
import 'package:askany/src/models/request_model.dart';
import 'package:dio/dio.dart';
import 'package:intl/intl.dart';

class CalendarRepository {
  Future<List<RequestModel>> getListScheduleRequest({
    required DateTime date,
    required int skip,
    int limit = UNLIMITED_QUANTITY,
  }) async {
    Response response = await BaseRepository().getRoute(
      UserLocal().getIsExpert()
          ? Endpoints.EXPERT_SCHEDULE_REQUEST
          : Endpoints.USER_SCHEDULE_REQUEST,
      query: 'limit=$limit&skip=$skip&date=${DateFormat('yyyy-MM').format(date)}',
    );

    if (response.statusCode == StatusCode.OK) {
      final requests = response.data['data']['listData'] as List;
      return requests.map((request) => RequestModel.fromOfferRequest(request)).toList();
    }
    return [];
  }
}
