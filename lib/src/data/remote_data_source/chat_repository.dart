import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/constants/endpoints.dart';
import 'package:askany/src/constants/http_status_code.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/data/remote_data_source/base_repository.dart';
import 'package:askany/src/models/conversation_model.dart';
import 'package:dio/dio.dart';

class ChatRepository {
  Future<List<ConversationModel>> getConversations({
    required int skip,
    int limit = LIMIT_API_10,
    String? search,
  }) async {
    Response response = await BaseRepository().getRoute(
      UserLocal().getIsExpert()
          ? Endpoints.EXPERT_CONVERSATION
          : Endpoints.USER_CONVERSATION,
      query:
          'skip=$skip&limit=$limit' + (search == null ? '' : '&search=$search'),
    );
    if (response.statusCode == StatusCode.OK) {
      final conversations = response.data['data']['listData'] as List;
      return conversations
          .map((conversation) => ConversationModel.fromMap(conversation))
          .toList();
    }

    return [];
  }

  Future<ConversationModel?> getDetailsConversation({
    required String conversationId,
  }) async {
    Response response = await BaseRepository().getRoute(
      Endpoints.CONVERSATION + '/$conversationId',
    );
    if (response.statusCode == StatusCode.OK) {
      return ConversationModel.fromMap(response.data['data']);
    }

    return null;
  }

  Future<int> getBadgesConversation() async {
    Response response = await BaseRepository().getRoute(
      Endpoints.BADGES_CONVERSATION +
          '/${(UserLocal().getIsExpert() ? 'expert' : 'user')}',
    );
    if (response.statusCode == StatusCode.OK) {
      return response.data?['data']?['countUnseenMessages'] ?? 0;
    }

    return 0;
  }

  Future<bool> updateConversationName({
    required String conversationId,
    required String conversationName,
  }) async {
    var body = {
      'field': 'conversationName',
      'value': conversationName,
    };

    Response response = await BaseRepository().patchRoute(
      Endpoints.CONVERSATION + '/$conversationId',
      body: body,
    );

    return response.statusCode == StatusCode.OK;
  }

  Future<bool> deleteConversation({
    required String conversationId,
  }) async {
    Response response = await BaseRepository()
        .deleteRoute(Endpoints.CONVERSATION + '/$conversationId');

    return response.statusCode == StatusCode.OK;
  }

  Future<ConversationModel?> getConversationByExpert({
    required String expertId,
  }) async {
    Response response = await BaseRepository().getRoute(
      Endpoints.EXISTS_CONVERSATION + '/$expertId',
    );

    if (response.statusCode == StatusCode.OK) {
      final rawData = response.data['data'];
      return ConversationModel.fromMap(rawData);
    }

    return null;
  }

  Future<bool> ignoreConversation({required String conversationId}) async {
    Response response = await BaseRepository().patchRoute(
      Endpoints.IGNORE_CONVERSATION + '/$conversationId',
    );

    if (response.statusCode == StatusCode.OK) {
      return true;
    }
    return false;
  }

  Future<bool> unIgnoreConversation({required String conversationId}) async {
    Response response = await BaseRepository().patchRoute(
      Endpoints.UNIGNORE_CONVERSATION + '/$conversationId',
    );
    if (response.statusCode == StatusCode.OK) {
      return true;
    }
    return false;
  }
}
