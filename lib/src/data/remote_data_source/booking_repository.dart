import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/constants/endpoints.dart';
import 'package:askany/src/constants/http_status_code.dart';
import 'package:askany/src/models/offer_skill_model.dart';
import 'package:askany/src/models/request_model.dart';
import 'package:dio/dio.dart';

import 'base_repository.dart';

class BookingRepository {
  Future<List?> orderSkill({
    required String skillId,
    required String contactForm,
    required int participantsCount,
    String? locationName,
    String? locationAddress,
    required String authorName,
    required String authorPhone,
    required String note,
  }) async {
    var bodyContactMeet = {
      'contactForm': contactForm,
      'participantsCount': participantsCount,
      'locationName': locationName,
      'locationAddress': locationAddress,
      'authorName': authorName,
      'authorPhone': authorPhone,
      'note': note,
    };

    var bodyContactCall = {
      'contactForm': contactForm,
      'participantsCount': participantsCount,
      'authorName': authorName,
      'authorPhone': authorPhone,
      'note': note,
    };

    Response response = await BaseRepository().postRoute(
      Endpoints.BOOKING_EXPERT + '/' + skillId,
      contactForm == CONTACT_CALL ? bodyContactCall : bodyContactMeet,
    );

    if (response.statusCode == StatusCode.CREATED) {
      final offer = response.data['data']?['offer'] ?? [];
      final request = response.data['data']['request']['data'];
      return [
        OfferSkillModel.fromMap(offer),
        RequestModel.fromCreateRequestBySkill(request),
      ];
    }

    return [null, null];
  }
}
