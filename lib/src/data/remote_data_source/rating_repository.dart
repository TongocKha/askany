import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/constants/endpoints.dart';
import 'package:askany/src/constants/http_status_code.dart';
import 'package:askany/src/data/remote_data_source/base_repository.dart';
import 'package:askany/src/models/rating_model.dart';
import 'package:dio/dio.dart';

class RatingRepository {
  Future<List<RatingModel>> getListRatingSkill({
    required String skillId,
    required int skip,
    required int stars,
    int limit = LIMIT_API_5,
  }) async {
    Response response = await BaseRepository().getRoute(
      Endpoints.RATING_SKILL + '/' + skillId,
      query: 'skip=$skip&limit=$limit' + (stars > 0 ? '&star=$stars' : ''),
    );

    if (response.statusCode == StatusCode.OK) {
      List listRatingSkill = response.data['data']?['listData'] ?? [];
      return listRatingSkill.map((ratingModel) => RatingModel.fromMap(ratingModel)).toList();
    }

    return [];
  }

  Future<List<RatingModel>> getListRatingExpert({
    required String expertId,
    required int skip,
    required int stars,
    int limit = LIMIT_API_10,
  }) async {
    Response response = await BaseRepository().getRoute(
      Endpoints.RATING_EXPERT + '/' + expertId,
      query: 'skip=$skip&limit=$limit' + (stars > 0 ? '&star=$stars' : ''),
    );

    if (response.statusCode == StatusCode.OK) {
      List listRatingExpert = response.data['data']?['listData'] ?? [];

      return listRatingExpert.map((ratingModel) => RatingModel.fromMap(ratingModel)).toList();
    }

    return [];
  }
}
