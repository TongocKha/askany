import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/constants/endpoints.dart';
import 'package:askany/src/constants/http_status_code.dart';
import 'package:askany/src/data/remote_data_source/base_repository.dart';
import 'package:askany/src/helpers/date_time_helper.dart';
import 'package:askany/src/models/budget_model.dart';
import 'package:askany/src/models/expert_timeline_model.dart';
import 'package:askany/src/models/offer_model.dart';
import 'package:askany/src/models/offer_skill_model.dart';
import 'package:askany/src/models/request_model.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class RequestRepository {
  Future<List<RequestModel>> getListRequest({
    required String status,
    required int skip,
    int limit = LIMIT_API_5,
  }) async {
    Response response = await BaseRepository().getRoute(
      Endpoints.MY_REQUEST + '/$status',
      query: 'skip=$skip&limit=$limit',
    );

    if (response.statusCode == StatusCode.OK) {
      final requests = response.data['data']['listData'] as List;
      return requests.map((request) => RequestModel.fromMap(request)).toList();
    }

    return [];
  }

  Future<List<RequestModel>> getListRequestCommon({
    required String status,
    required int skip,
    required String search,
    required String cost,
    String? category,
    int limit = LIMIT_API_5,
  }) async {
    Response response = await BaseRepository().getRoute(
      Endpoints.GLOBAL_REQUESTS + '/$status',
      query: 'skip=$skip&limit=$limit' +
          (search.isEmpty ? '' : '&search=$search') +
          (cost.isEmpty ? '' : '&price=$cost') +
          (category == null ? '' : '&category=$category'),
    );

    if (response.statusCode == StatusCode.OK) {
      final requests = response.data['data']['listData'] as List;

      return requests.map((request) => RequestModel.fromMap(request)).toList();
    }

    return [];
  }

  Future<List<RequestModel>> getListRequestWithParamStatus(
      {required String categoryId,
      required int skip,
      int limit = LIMIT_API_5}) async {
    Response response = await BaseRepository().getRoute(
        Endpoints.GLOBAL_REQUEST_SEARCH_BY_CATEGORY +
            '?category=' +
            '$categoryId');

    if (response.statusCode == StatusCode.OK) {
      final requests = response.data['data']['listData'] as List;
      if (requests.length > 0) {
        return requests
            .map((request) => RequestModel.fromMap(request))
            .toList();
      } else {
        return [];
      }
    }
    return [];
  }

  Future<List<RequestModel>> getListRequestByExpert({
    required String status,
    required int skip,
    int limit = LIMIT_API_5,
  }) async {
    Response response = await BaseRepository().getRoute(
      Endpoints.MY_OFFERED_REQUEST + '/$status',
      query: 'skip=$skip&limit=$limit',
    );

    if (response.statusCode == StatusCode.OK) {
      final requests = response.data['data']['listData'] as List;
      return requests
          .map((request) => RequestModel.fromOfferRequest(request))
          .toList();
    }
    return [];
  }

  Future<RequestModel?> createRequest({required RequestModel request}) async {
    FormData form = FormData.fromMap(request.toMapForCreate());
    Response response = await BaseRepository().sendFormData(
      Endpoints.REQUESTS,
      form,
    );

    if (response.statusCode == StatusCode.CREATED) {
      return RequestModel.fromCreateRequest(response.data['data']);
    }
    return null;
  }

  Future<String?> expertCreateOffer({
    required RequestModel request,
    required BudgetModel price,
    required String content,
  }) async {
    Map<String, dynamic> body = {
      'offeredRequest': request.id,
      'price': price.toJsonSkill(),
      'content': content,
    };

    Response response = await BaseRepository().postRoute(
      Endpoints.DETAILS_OFFER,
      body,
    );

    if (response.statusCode == StatusCode.CREATED) {
      return response.data['data']['_id'];
    }
    return null;
  }

  Future<RequestModel?> updateRequest({required RequestModel request}) async {
    FormData form = FormData.fromMap(request.toMapForCreate());
    Response response = await BaseRepository().putFormData(
      Endpoints.REQUESTS + '/${request.id}',
      form,
    );
    if (response.statusCode == StatusCode.OK) {
      return RequestModel.fromCreateRequest(response.data['data']);
    }
    return null;
  }

  Future<bool> deleteRequest(String requestId) async {
    Response response =
        await BaseRepository().deleteRoute(Endpoints.REQUESTS + '/$requestId');
    if (response.statusCode == StatusCode.OK) {
      return true;
    }
    return false;
  }

  Future<bool> expertCancelOffer(String offerId) async {
    Response response = await BaseRepository()
        .patchRoute(Endpoints.EXPERT_CANCEL_OFFER + '/$offerId');

    if (response.statusCode == StatusCode.OK) {
      return true;
    }

    return false;
  }

  Future<String?> userChooseOffer({
    required String requestId,
    required String offerId,
  }) async {
    Map<String, dynamic> body = {'offerID': offerId};
    Response response = await BaseRepository().patchRoute(
      Endpoints.CHOOSE_OFFER + '/$requestId',
      body: body,
    );

    if (response.statusCode == StatusCode.OK) {
      return response.data['data']['conversationID'];
    }

    return null;
  }

  Future<bool> updateUserInfo({
    required String offerId,
    required String authorName,
    required String authorPhone,
    required String note,
    required String locationName,
    required String locationAddress,
  }) async {
    Map<String, dynamic> body = {
      'authorName': authorName,
      'authorPhone': authorPhone,
      'note': note,
      'locationName': locationName,
      'locationAddress': locationAddress,
    };
    Response response = await BaseRepository().patchRoute(
      Endpoints.UPDATE_TIMELINE_AND_USER_INFO + '/$offerId',
      body: body,
    );

    return response.statusCode == StatusCode.OK;
  }

  Future<bool> updateTimeline({
    required String offerId,
    required DateTime startTime,
    required DateTime endTime,
  }) async {
    Map<String, dynamic> body = {
      'startTime': formatTimeToUTC(startTime),
      'endTime': formatTimeToUTC(endTime),
    };
    Response response = await BaseRepository().patchRoute(
      Endpoints.UPDATE_TIMELINE_AND_USER_INFO + '/$offerId',
      body: body,
    );

    return response.statusCode == StatusCode.OK;
  }

  Future<bool> payByPersonalWallet({
    required amount,
    required offerId,
    required expertId,
    required requestId,
  }) async {
    Map<String, dynamic> body = {
      'amount': amount,
      'offer': offerId,
      'expert': expertId,
      'request': requestId,
    };

    Response response = await BaseRepository().postRoute(
      Endpoints.PAY_BY_PERSONAL_WALLET,
      body,
    );
    debugPrint(response.statusCode.toString());
    debugPrint(response.statusMessage.toString());

    debugPrint(response.data.toString());

    return response.statusCode == StatusCode.OK;
  }

  Future<String?> payByVNPay({
    required double amount,
    required String offerId,
    required String requestId,
  }) async {
    Map<String, dynamic> body = {
      'amount': amount,
      'offer': offerId,
      'request': requestId,
      'type': 0,
    };

    Response response =
        await BaseRepository().postRoute(Endpoints.PAY_BY_VNPAY, body);

    return response.statusCode == StatusCode.OK ? response.data['data'] : null;
  }

  Future<bool> userEndRequest({required String requestId}) async {
    Response response = await BaseRepository()
        .patchRoute(Endpoints.USER_END_REQUEST + "/$requestId");

    return response.statusCode == StatusCode.OK;
  }

  Future<OfferModel?> getDetailsOffer({required String offerId}) async {
    Response response =
        await BaseRepository().getRoute(Endpoints.DETAILS_OFFER + "/$offerId");

    if (response.statusCode == StatusCode.OK) {
      return OfferModel.fromMap(response.data['data']);
    }

    return null;
  }

  Future<ExpertTimelineModel?> getExpertTimeline({
    required String expertId,
    required DateTime selectedDate,
    required int contactType,
    String? day,
  }) async {
    Response response = await BaseRepository().getRoute(
      Endpoints.EXPERT_TIMELINE + "/$expertId",
      query:
          'contactType=$contactType&date=${DateFormat('yyyy-MM-dd').format(selectedDate)}' +
              (day != null ? '&day=$day' : ''),
    );

    if (response.statusCode == StatusCode.OK) {
      var timelineRaw = response.data['data'];
      return ExpertTimelineModel.fromMap(timelineRaw);
    }

    return null;
  }

  Future<List> bookingExpert({
    required String authorName,
    required String authorPhone,
    required String note,
    required String expertId,
    required String contactForm,
    required int participantsCount,
    String? locationName,
    String? locationAddress,
  }) async {
    var bodyCall = {
      'authorName': authorName,
      'authorPhone': authorPhone,
      'note': note,
      'contactForm': contactForm,
      'participantsCount': participantsCount,
    };
    var bodyMeet = {
      'authorName': authorName,
      'authorPhone': authorPhone,
      'note': note,
      'contactForm': contactForm,
      'participantsCount': participantsCount,
      'locationName': locationName,
      'locationAddress': locationAddress
    };
    Response response = await BaseRepository().postRoute(
        Endpoints.BOOKING_TARGET_EXPERT + '/$expertId',
        contactForm == CONTACT_CALL ? bodyCall : bodyMeet);

    if (response.statusCode == StatusCode.CREATED) {
      return [
        RequestModel.fromCreateRequestByTargetExpert(
            response.data['data']['request']['data']),
        OfferSkillModel.fromMap(response.data['data']['offer'])
      ];
    }
    return [null, null];
  }

  Future<RequestModel?> getInfoRequest({
    required String requestId,
  }) async {
    Response response = await BaseRepository().getRoute(
      Endpoints.REQUESTS + '/' + requestId,
    );

    if (response.statusCode == StatusCode.OK) {
      final requestModel = response.data['data'];
      return RequestModel.fromMap(requestModel);
    }

    return null;
  }
}
