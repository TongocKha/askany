import 'package:askany/src/constants/endpoints.dart';
import 'package:askany/src/constants/http_status_code.dart';
import 'package:askany/src/data/remote_data_source/base_repository.dart';
import 'package:askany/src/models/discover_model.dart';
import 'package:dio/dio.dart';

class DiscoverRepository {
  Future<DiscoverOriginModel?> getDiscoverItems() async {
    Response response =
        await BaseRepository().getRoute(Endpoints.DISCOVER_ADVISES);
    if (response.statusCode == StatusCode.OK) {
      final DiscoverOriginModel originItem =
          DiscoverOriginModel.fromMap(response.data['data']);

      return originItem;
    }

    return null;
  }
}
