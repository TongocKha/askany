import 'dart:io';

import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/constants/endpoints.dart';
import 'package:askany/src/constants/http_status_code.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/data/remote_data_source/base_repository.dart';
import 'package:askany/src/models/message_images_model.dart';
import 'package:askany/src/models/message_model.dart';
import 'package:dio/dio.dart';
import 'package:http_parser/http_parser.dart';

class MessageRepository {
  Future<bool> markSeenAllMessage({
    required String conversationId,
  }) async {
    Response response = await BaseRepository().getRoute(
      Endpoints.MARK_SEEN_ALL_MESSAGE + '/$conversationId',
    );

    return response.statusCode == StatusCode.OK;
  }

  Future<Map<List<MessageModel>, int>> getMessageByConversation({
    required String conversationId,
    required int skip,
    String? search,
    int limit = LIMIT_API_15,
  }) async {
    Response response = await BaseRepository().getRoute(
      Endpoints.GET_MESSAGE_BY_CONVERSATION + '/$conversationId',
      query: 'limit=$limit&skip=$skip' + (search != null ? '&search=$search' : ''),
    );

    if (response.statusCode == StatusCode.OK) {
      final List messageRaw = response.data['data']['listData'];

      final int total = response.data['data']['total'];
      return {messageRaw.map((message) => MessageModel.fromMap(message)).toList(): total};
    }

    return {};
  }

  Future<Map<List<MessageModel>, int>> getSearchMessage({
    required String conversationId,
    required int skip,
    String? search,
    int limit = LIMIT_API_15,
  }) async {
    Response response = await BaseRepository().getRoute(
      Endpoints.GET_MESSAGE_BY_CONVERSATION + '/$conversationId',
      query: 'limit=$limit&skip=$skip' + (search != null ? '&search=$search' : ''),
    );

    if (response.statusCode == StatusCode.OK) {
      final List messageRaw = response.data['data']['listData'];
      final int total = response.data['data']['total'];
      return {messageRaw.map((message) => MessageModel.fromMap(message)).toList(): total};
    }

    return {};
  }

  Future<List<MessagesImageModel>> getMessagesImages({
    required String id,
    required int skip,
    int limit = LIMIT_API_15,
  }) async {
    Response response = await BaseRepository().getRoute(
      Endpoints.GET_MESSAGE_BY_CONVERSATION + '/$id/images',
      query: 'limit=$limit&skip=$skip',
    );
    if (response.statusCode == StatusCode.OK) {
      final List listImages = response.data['data']['listData'];
      return listImages
          .map((image) => MessagesImageModel.fromMap(image))
          .toList();
    }

    return [];
  }

  Future<MessageModel?> createMessage({
    required String conversationId,
    required String data,
    String? parentId,
    List<File>? images,
  }) async {
    final Map<String, dynamic> body = {
      'conversation': conversationId,
      'data': data,
      '${UserLocal().getIsExpert() ? 'expertSender' : 'userSender'}': UserLocal().getUser().id!,
    };

    if (parentId != null) {
      body['parent'] = parentId;
    }

    FormData _formData = FormData.fromMap(body);

    images?.forEach((image) {
      _formData.files.add(MapEntry(
          'images',
          MultipartFile.fromBytes(
            image.readAsBytesSync(),
            filename: image.path.split('/').last,
            contentType: MediaType('images', image.path.split('.').last.toString()),
          )));
    });

    Response response = await BaseRepository().sendFormData(Endpoints.MESSAGES, _formData);
    if (response.statusCode == StatusCode.CREATED) {
      final messageRaw = response.data['data'];
      return MessageModel.fromMap(messageRaw);
    }

    return null;
  }

  Future<bool> editMessage({
    required String messageId,
    required String data,
  }) async {
    final Map<String, dynamic> body = {
      'field': 'data',
      'value': data,
    };
    Response response = await BaseRepository().patchRoute(
      Endpoints.MESSAGES + '/$messageId',
      body: body,
    );

    return response.statusCode == StatusCode.OK;
  }

  Future<bool> deleteMessage({
    required String messageId,
  }) async {
    Response response = await BaseRepository().deleteRoute(Endpoints.MESSAGES + '/$messageId');

    return response.statusCode == StatusCode.OK;
  }

  Future<bool> updateStatusSeenMessage({
    required String messageId,
    int isSeen = IS_SEEN,
  }) async {
    var body = {
      'field': 'isSeen',
      'value': isSeen.toString(),
    };

    Response response = await BaseRepository().patchRoute(
      Endpoints.MESSAGES + '/$messageId',
      body: body,
    );

    return response.statusCode == StatusCode.OK;
  }
}
