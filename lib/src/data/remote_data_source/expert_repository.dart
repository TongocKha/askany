import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/constants/endpoints.dart';
import 'package:askany/src/constants/http_status_code.dart';
import 'package:askany/src/data/remote_data_source/base_repository.dart';
import 'package:askany/src/models/expert_model.dart';
import 'package:askany/src/models/search_model.dart';
import 'package:dio/dio.dart';

class ExpertRepository {
  Future<ExpertModel?> getInfoExpert({
    required String idExpert,
  }) async {
    Response response = await BaseRepository().getRoute(
      Endpoints.EXPERTS + '/' + idExpert,
    );

    if (response.statusCode == StatusCode.OK) {
      final expert = response.data['data'];
      return ExpertModel.fromMapInfoExpert(expert);
    }

    return null;
  }

  Future<List<ExpertModel>> getListRecommend({
    required String specialty,
    required int skip,
    int limit = LIMIT_API_5,
  }) async {
    Response response = await BaseRepository().getRoute(
      Endpoints.RECOMMEND_EXPERTS,
      query: (specialty.isNotEmpty ? 'specialty=$specialty&' : '') +
          'limit=$limit&skip=$skip',
    );

    if (response.statusCode == StatusCode.OK) {
      List listExpert = response.data['data']?['listData'] ?? [];
      return listExpert.map((expert) => ExpertModel.fromMap(expert)).toList();
    }
    return [];
  }

  Future<List<ExpertModel>> filterExpert({
    required SearchModel searchModel,
    required int skip,
    int limit = LIMIT_API_5,
  }) async {
    Response response = await BaseRepository().getRoute(
      Endpoints.SEARCH_EXPERT,
      query: searchModel.query + 'skip=$skip&limit=$limit',
    );

    if (response.statusCode == StatusCode.OK) {
      List listExpert = response.data['data']?['listData'] ?? [];
      return listExpert
          .map((expert) => ExpertModel.fromMapExpertSearch(expert))
          .toList();
    }
    return [];
  }

  Future<List<ExpertModel>> getHotExpert() async {
    Response response = await BaseRepository().getRoute(
      Endpoints.HOT_EXPERTS,
    );

    if (response.statusCode == StatusCode.OK) {
      List listExpert = response.data['data']?['titles'] ?? [];
      return listExpert
          .map((expert) => ExpertModel.fromMapExpertSearch(expert))
          .toList();
    }
    return [];
  }
}
