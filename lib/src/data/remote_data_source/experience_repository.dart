import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/constants/endpoints.dart';
import 'package:askany/src/constants/http_status_code.dart';
import 'package:askany/src/data/remote_data_source/base_repository.dart';
import 'package:askany/src/models/experience_model.dart';
import 'package:dio/dio.dart';

class ExperienceRepository {
  Future<List<ExperienceModel>> getExperiences({
    required int skip,
    int limit = UNLIMITED_API,
  }) async {
    Response response = await BaseRepository().getRoute(
      Endpoints.GET_EXPERIENCE,
      query: 'skip=$skip&limit=$limit',
    );

    if (response.statusCode == StatusCode.OK) {
      final List experiencesRaw = response.data['data']['listData'];
      return experiencesRaw.map((experience) => ExperienceModel.fromMap(experience)).toList();
    }

    return [];
  }

  Future<ExperienceModel?> addExperience({
    required ExperienceModel experience,
  }) async {
    Response response = await BaseRepository().postRoute(
      Endpoints.EXPERIENCE,
      experience.toMapCreate(),
    );

    if ([StatusCode.CREATED, StatusCode.OK].contains(response.statusCode)) {
      final rawData = response.data['data'];
      return ExperienceModel.fromMap(rawData);
    }

    return null;
  }

  Future<bool> updateExperience({
    required ExperienceModel experience,
  }) async {
    Response response = await BaseRepository().putRoute(
      Endpoints.EXPERIENCE + '/${experience.id}',
      experience.toMapCreate(),
    );

    if (response.statusCode == StatusCode.OK) {
      return true;
    }

    return false;
  }

  Future<bool> deleteExperience({
    required String experienceId,
  }) async {
    Response response = await BaseRepository().deleteRoute(
      Endpoints.EXPERIENCE + '/$experienceId',
    );

    if (response.statusCode == StatusCode.OK) {
      return true;
    }

    return false;
  }
}
