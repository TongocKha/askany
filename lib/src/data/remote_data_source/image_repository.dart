import 'dart:typed_data';

import 'package:flutter/services.dart';

class ImageRepository {
  Future<Uint8List?> getByteArrayFromUrl(String url) async {
    return (await NetworkAssetBundle(Uri.parse(url)).load(url)).buffer.asUint8List();
  }
}
