import 'package:askany/src/constants/endpoints.dart';
import 'package:askany/src/constants/http_status_code.dart';
import 'package:askany/src/data/remote_data_source/base_repository.dart';
import 'package:dio/dio.dart';

class SearchRepository {
  Future<List<String>> getHotSearch() async {
    Response response = await BaseRepository().getRoute(
      Endpoints.HOT_SEARCH,
    );

    if (response.statusCode == StatusCode.OK) {
      List listHotSearch = response.data['data']?['titles'] ?? [];
      return listHotSearch.map((e) => e.toString()).toList();
    }
    return [];
  }
}
