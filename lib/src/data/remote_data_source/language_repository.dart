import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/constants/endpoints.dart';
import 'package:askany/src/constants/http_status_code.dart';
import 'package:askany/src/data/remote_data_source/base_repository.dart';
import 'package:askany/src/models/language_model.dart';
import 'package:dio/dio.dart';

class LanguageRepository {
  Future<List<LanguageModel>> getLanguages() async {
    Response response = await BaseRepository().getRoute(
      Endpoints.LANGUAGES,
      query: 'skip=0&limit=$UNLIMITED_API',
    );

    if (response.statusCode == StatusCode.OK) {
      final List languagesRaw = response.data['data']['listLangs'];
      return languagesRaw.map((language) => LanguageModel.fromMap(language)).toList();
    }

    return [];
  }
}
