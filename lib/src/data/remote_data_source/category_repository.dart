import 'package:askany/src/configs/lang/language_service.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/constants/endpoints.dart';
import 'package:askany/src/constants/http_status_code.dart';
import 'package:askany/src/data/remote_data_source/base_repository.dart';
import 'package:askany/src/models/category_model.dart';
import 'package:askany/src/models/content_model.dart';
import 'package:askany/src/models/expert_model.dart';
import 'package:dio/dio.dart';

class CategoryRepository {
  Future<List<CategoryInfoModel>> getCategories() async {
    final String language = LanguageService.getIsLanguage('vi') ? 'vi' : 'en';
    Response response = await BaseRepository().getRoute(Endpoints.CATEGORY,
        query: 'limit=$UNLIMITED_API&lang=$language');

    if (response.statusCode == StatusCode.OK) {
      final List categories = response.data['data']['listCatesWithSubCates'];
      return categories
          .map((category) => CategoryInfoModel.fromMap(category))
          .toList();
    }

    return [];
  }

  Future<List<ContentModel>> getContentCategory({
    required String slug,
    required int skip,
    int limit = LIMIT_API_5,
  }) async {
    Response response = await BaseRepository().getRoute(
      Endpoints.CATEGORY_SPECIALTY + '/$slug',
      query: 'limit=$limit&skip=$skip',
    );
    if (response.statusCode == StatusCode.OK) {
      List listSpecialties = response.data['data']['listSpecialty'];
      return listSpecialties
          .map((content) => ContentModel.fromMap(content))
          .toList();
    }
    return [];
  }

  Future<List<ExpertModel>> getExpertCategory({
    String? slug,
    String? specialtyId,
    required int skip,
    int limit = LIMIT_API_5,
  }) async {
    Response response = await BaseRepository().getRoute(
      Endpoints.SEARCH_EXPERT,
      query: 'category=$slug&' + 'limit=$limit&skip=$skip',
    );

    if (response.statusCode == StatusCode.OK) {
      List listExpert = response.data['data']?['listData'] ?? [];

      return listExpert
          .map((expert) => ExpertModel.fromMapExpertSearch(expert))
          .toList();
    }
    return [];
  }

  Future<List<CategoryModel>> getCategoryHome() async {
    Response response = await BaseRepository()
        .getRoute(Endpoints.CATEGORY_HOME, query: 'limit=$UNLIMITED_API');
    if (response.statusCode == StatusCode.OK) {
      List listContent = response.data['data']?['titles'] ?? [];
      return listContent
          .map((category) => CategoryModel.fromMapHome(category))
          .toList();
    }
    return [];
  }

  Future<CategoryInfoModel?> getCategoriesSlug({
    required String slug,
  }) async {
    Response response = await BaseRepository().getRoute(
      Endpoints.CATEGORY_PARENT + '/' + slug,
    );

    if (response.statusCode == StatusCode.OK) {
      final categories = response.data['data'];
      return CategoryInfoModel.fromInfoCategory(categories);
    }

    return null;
  }
}
