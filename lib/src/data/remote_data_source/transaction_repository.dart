import 'dart:io';

import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/constants/endpoints.dart';
import 'package:askany/src/constants/http_status_code.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/models/payment_card_model.dart';
import 'package:askany/src/models/transaction_model.dart';
import 'package:dio/dio.dart';
import 'package:http_parser/http_parser.dart';

import 'base_repository.dart';

class TransactionRepository {
  Future<String?> getLinkRecharge(double amount) async {
    Map<String, dynamic> body = {
      'amount': amount,
      'type': UserLocal().getIsExpert() ? 5 : 4,
    };
    Response response = await BaseRepository().postRoute(
        (UserLocal().getIsExpert()
            ? Endpoints.GET_EXPERT_LINK_RECHARGE
            : Endpoints.GET_USER_LINK_RECHARGE),
        body);
    if (response.statusCode == StatusCode.OK) {
      return response.data['data'];
    } else {
      return null;
    }
  }

  Future<List<TransactionModel>> getTransactions({
    required int skip,
    int limit = LIMIT_API_10,
  }) async {
    Response response = await BaseRepository().getRoute(
      (UserLocal().getIsExpert()
          ? Endpoints.EXPERT_TRANSACITONS
          : Endpoints.USER_TRANSACITONS),
      query: 'skip=$skip&limit=$limit',
    );
    if (response.statusCode == StatusCode.OK) {
      List rawData = response.data['data']['listData'];

      return rawData
          .map((transaction) => TransactionModel.fromMap(transaction))
          .toList();
    }
    return [];
  }

  Future<bool> withdrawRequest({
    required double amount,
    required List<File> images,
    required String bank,
  }) async {
    Map<String, dynamic> body = {
      "amount": amount.toString(),
      'bank': bank,
      'images': [],
    };
    FormData _formData = FormData.fromMap(body);
    images.forEach((images) async {
      _formData.files.add(MapEntry(
        'images',
        MultipartFile.fromBytes(
          images.readAsBytesSync(),
          filename: images.path.split('/').last,
          contentType:
              MediaType('images', images.path.split('.').last.toString()),
        ),
      ));
    });

    Response response = await BaseRepository().sendFormData(
      UserLocal().getIsExpert()
          ? Endpoints.WITHDRAW_EXPERT
          : Endpoints.WITHDRAW_USER,
      _formData,
    );
    if ([StatusCode.OK, StatusCode.CREATED].contains(response.statusCode)) {
      return true;
    }

    return false;
  }

  Future<List<PaymentCardModel>> getPaymentCard() async {
    Response response = await BaseRepository().getRoute(Endpoints.BANKS);
    if (response.statusCode == StatusCode.OK) {
      List rawData = response.data['data']['listData'];

      return rawData
          .map((paymentCard) => PaymentCardModel.fromMap(paymentCard))
          .toList();
    }
    return [];
  }

  Future<String?> addPaymentCard({
    required String bankName,
    required int cardType,
    required String accountName,
    required String cardNumber,
    required String bankCode,
    String? country,
    String? postOfficeCode,
    String? billAddress,
    String? city,
  }) async {
    Map<String, String> body = {
      'bankName': bankName,
      'cardType': cardType.toString(),
      'accountName': accountName,
      'cardNumber': cardNumber.replaceAll('\t\t', ''),
      'bankCode': bankCode,
    };

    if (country != null && country.isNotEmpty) {
      body['country'] = country;
    }

    if (postOfficeCode != null && postOfficeCode.toString().isNotEmpty) {
      body['postOfficeCode'] = postOfficeCode.toString();
    }

    if (billAddress != null && billAddress.isNotEmpty) {
      body['billAddress'] = billAddress.toString();
    }
    if (city != null && city.isNotEmpty) {
      body['city'] = city;
    }
    Response response = await BaseRepository().postRoute(
      Endpoints.BANKS,
      body,
    );

    if ([StatusCode.OK, StatusCode.CREATED].contains(response.statusCode)) {
      return response.data['data']['_id'];
    }
    return null;
  }

  Future<String?> updatePaymentCard({
    required String id,
    required String bankName,
    int? cardType,
    required String accountName,
    required String cardNumber,
    String? country,
    required String bankCode,
    String? postOfficeCode,
    String? billAddress,
    String? city,
  }) async {
    Map<String, String> body = {
      'bankName': bankName,
      'accountName': accountName,
      'cardNumber': cardNumber.replaceAll('\t\t', ''),
      'bankCode': bankCode,
    };

    if (cardType != null && cardType.toString().isNotEmpty) {
      body['cardType'] = cardType.toString();
    }

    if (country != null && country.isNotEmpty) {
      body['country'] = country.toString();
    }
    if (postOfficeCode != null && postOfficeCode.toString().isNotEmpty) {
      body['postOfficeCode'] = postOfficeCode.toString();
    }
    if (billAddress != null && billAddress.isNotEmpty) {
      body['billAddress'] = billAddress.toString();
    }
    if (city != null && city.isNotEmpty) {
      body['city'] = city;
    }

    Response response = await BaseRepository().putRoute(
      Endpoints.BANKS + '/$id',
      body,
    );

    if ([StatusCode.OK, StatusCode.CREATED].contains(response.statusCode)) {
      return response.data['data']['_id'];
    }
    return null;
  }

  Future<bool> deletePaymentCard({required String id}) async {
    Response response =
        await BaseRepository().deleteRoute(Endpoints.BANKS + '/$id');
    return response.statusCode == StatusCode.OK;
  }
}
