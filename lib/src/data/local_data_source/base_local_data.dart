import 'package:askany/src/constants/storage_key.dart';
import 'package:askany/src/helpers/path_helper.dart';
import 'package:hive/hive.dart';

class BaseLocalData {
  static Future<void> initialBox() async {
    var path = await PathHelper.appDir;
    Hive..init(path.path);
    await Hive.openBox(StorageKey.BOX_USER);
    await Hive.openBox(StorageKey.BOX_REQUEST);
    await Hive.openLazyBox(StorageKey.BOX_MESSAGE);
    await Hive.openBox(StorageKey.BOX_SEARCH);
  }
}
