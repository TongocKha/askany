import 'package:askany/src/constants/storage_key.dart';
import 'package:askany/src/models/specialty_model.dart';
import 'package:hive/hive.dart';

class RequestLocal {
  var box = Hive.box(StorageKey.BOX_REQUEST);

  void saveSpecialties(List<SpecialtyModel> specialties) {
    box.put(StorageKey.SPECIALTIES, specialties.map((item) => item.toJson()).toList());
  }

  List<SpecialtyModel> get specialties {
    List<dynamic>? rawData = box.get(StorageKey.SPECIALTIES);

    if (rawData != null) {
      return rawData.map((item) => SpecialtyModel.fromJson(item)).toList();
    }

    return [];
  }
}
