import 'dart:convert';

import 'package:askany/src/constants/storage_key.dart';
import 'package:askany/src/models/message_model.dart';
import 'package:hive/hive.dart';

class MessageLocal {
  var box = Hive.box(StorageKey.BOX_REQUEST);

  void removeDraftMessages() {
    box.delete(StorageKey.DRAFT_MESSAGES);
  }

  void saveDraftMessages(Map<String, String> messages) {
    box.put(StorageKey.DRAFT_MESSAGES, json.encode(messages).toString());
  }

  void saveErrorMessages(List<MessageModel> messages) {
    box.put(StorageKey.ERROR_MESSAGES, messages.map((item) => item.toJson()).toList());
  }

  List<MessageModel> get errorMessages {
    List<dynamic>? rawData = box.get(StorageKey.ERROR_MESSAGES);

    if (rawData != null) {
      return rawData.map((item) => MessageModel.fromJson(item)).toList();
    }

    return [];
  }

  Map<String, String> get draftMessages {
    dynamic rawData = box.get(StorageKey.DRAFT_MESSAGES) as String?;
    if (rawData == null) {
      return {};
    }
    return jsonDecode(rawData).cast<String, String>();
  }
}
