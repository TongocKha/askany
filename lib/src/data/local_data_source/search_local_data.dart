import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/constants/storage_key.dart';
import 'package:askany/src/models/search_model.dart';
import 'package:hive/hive.dart';

class SearchLocal {
  var box = Hive.box(StorageKey.BOX_SEARCH);

  // Nearby Search
  void saveSearch(String searchKey) {
    List<LocalSearchModel> result = listSearch;
    int indexOfSearch = result.indexWhere(
      (search) =>
          search.searchKey == searchKey && search.createdBy == AppBloc.userBloc.getAccount.id,
    );

    LocalSearchModel searchModel = LocalSearchModel(
      searchKey: searchKey,
      createdAt: DateTime.now(),
      createdBy: AppBloc.userBloc.getAccount.id!,
    );

    if (indexOfSearch == -1) {
      result.add(searchModel);
    } else {
      result[indexOfSearch] = searchModel;
    }

    box.put(StorageKey.SEARCH_KEY, result.map((item) => item.toJson()).toList());
  }

  void removeSearch() {
    box.delete(StorageKey.SEARCH_KEY);
  }

  void removeSearchKey(String searchKey) {
    List<LocalSearchModel> result = listSearch;
    int indexOfSearch = result.indexWhere(
      (search) =>
          search.searchKey == searchKey && search.createdBy == AppBloc.userBloc.getAccount.id,
    );

    if (indexOfSearch != -1) {
      result.removeAt(indexOfSearch);
    }

    box.put(StorageKey.SEARCH_KEY, result.map((item) => item.toJson()).toList());
  }

  List<LocalSearchModel> get listSearch {
    List<dynamic> rawData = box.get(StorageKey.SEARCH_KEY) ?? [];
    List<LocalSearchModel> result = rawData
        .map((e) => LocalSearchModel.fromJson(e))
        .toList()
        .where(
          (search) => search.createdBy == AppBloc.userBloc.getAccount.id,
        )
        .toList();
    result.sort((a, b) => b.createdAt.compareTo(a.createdAt));
    return result;
  }
}
