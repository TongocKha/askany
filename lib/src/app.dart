import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/authentication/bloc.dart';
import 'package:askany/src/bloc/bloc.dart';
import 'package:askany/src/configs/lang/language_service.dart';
import 'package:askany/src/configs/themes/theme_service.dart';
import 'package:askany/src/configs/themes/themes.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:askany/src/home.dart';
import 'package:askany/src/routes/app_navigator_observer.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/routes/scaffold_wrapper.dart';
import 'package:askany/src/ui/splash/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'helpers/device_orientation_helper.dart';

class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AppState();
}

class _AppState extends State<App> with WidgetsBindingObserver {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addObserver(this);
    DeviceOrientationHelper().setPortrait();
    AppBloc.applicationBloc.add(OnSetupApplication(context));
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (AppBloc.timerBloc.duration == 0) {
      switch (state) {
        case AppLifecycleState.resumed:
          AppBloc.appStateBloc.add(OnResume());
          break;
        case AppLifecycleState.paused:
          AppBloc.appStateBloc.add(OnBackground());

          break;
        default:
          break;
      }
    }
  }

  @override
  void dispose() {
    AppBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: AppBloc.providers,
      child: BlocBuilder<AuthBloc, AuthState>(
        buildWhen: (previous, current) => previous != current,
        builder: (context, auth) {
          return Sizer(
            builder: (context, orientation, deviceType) {
              return MaterialApp(
                navigatorKey: AppNavigator.navigatorKey,
                debugShowCheckedModeBanner: false,
                title: 'Askany',
                locale: LanguageService.locale,
                supportedLocales: LanguageService.supportLanguage,
                localizationsDelegates: [
                  GlobalMaterialLocalizations.delegate,
                  GlobalWidgetsLocalizations.delegate,
                  GlobalCupertinoLocalizations.delegate,
                ],
                theme: AppTheme.light().data,
                darkTheme: AppTheme.dark().data,
                themeMode: ThemeService.currentTheme,
                initialRoute: Routes.ROOT,
                onGenerateRoute: (settings) {
                  return AppNavigator().getRoute(settings);
                },
                navigatorObservers: [
                  AppNavigatorObserver(),
                ],
                builder: (context, child) {
                  return MediaQuery(
                    child: child!,
                    data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
                  );
                },
                home: BlocBuilder<ApplicationBloc, ApplicationState>(
                  buildWhen: (previous, current) => previous != current,
                  builder: (context, application) {
                    if (application is ApplicationCompleted) {
                      return ScaffoldWrapper(
                        child: Home(),
                      );
                    }
                    return ScaffoldWrapper(child: SplashScreen());
                  },
                ),
              );
            },
          );
        },
      ),
    );
  }
}
