import 'package:in_app_review/in_app_review.dart';

class InAppReviewAskany {
  static final InAppReview inAppReview = InAppReview.instance;

  static void reviewApp() async {
    final _isAvailable = await inAppReview.isAvailable();

    if (_isAvailable) {
      inAppReview.requestReview();
    }
  }
}
