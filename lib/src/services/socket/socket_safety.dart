import 'package:askany/src/helpers/utils/logger.dart';
import 'package:askany/src/services/queue/queue_service.dart';
import 'package:askany/src/services/socket/socket.dart';

class SocketSafety {
  static List<Map<String, dynamic>> events = [];

  static void emit(Function fn) {
    if (socket == null || !socket!.connected) {
      QueueService.addQueue(() => fn());
    } else {
      fn();
    }
  }

  static bool isNotDuplicated({required String event, dynamic data}) {
    UtilLogger.log(event, data);
    Map<String, dynamic> eventData = {
      event: data,
    };

    if (events.isEmpty) {
      return true;
    }

    if (events.last != eventData) {
      events.add(eventData);
      return true;
    }

    return false;
  }
}
