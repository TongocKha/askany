import 'dart:io';
import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/authentication/bloc.dart';
import 'package:askany/src/bloc/chat/chat_bloc.dart';
import 'package:askany/src/bloc/message/message_bloc.dart';
import 'package:askany/src/bloc/notification/notification_bloc.dart';
import 'package:askany/src/bloc/video_call/video_call_bloc.dart';
import 'package:askany/src/configs/application.dart';
import 'package:askany/src/constants/socket_event.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/helpers/device_helper.dart';
import 'package:askany/src/models/candidate_model.dart';
import 'package:askany/src/models/incoming_call_model.dart';
import 'package:askany/src/models/message_model.dart';
import 'package:askany/src/models/notification_model.dart';
import 'package:askany/src/services/method_channel/initial_app_channel.dart';
import 'package:askany/src/services/queue/queue_service.dart';
import 'package:askany/src/services/socket/socket_emit.dart';
import 'package:askany/src/services/socket/socket_safety.dart';
import 'package:flutter/material.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;

IO.Socket? socket;

void connectAndListen() async {
  disconnectBeforeConnect();
  String socketUrl = Application.socketUrl;
  socket = IO.io(
    socketUrl,
    IO.OptionBuilder().enableForceNew().setTransports(['websocket']).setExtraHeaders({
      'authorization':
          'Bearer ${Application.canChat ? UserLocal().getAccessToken() : UserLocal().getUser().id}',
      'secret': 'appfake@waodate.com',
    }).build(),
  );
  socket!.connect();
  socket!.onConnect((_) async {
    debugPrint('connected');

    // Listen to events
    socket!.on(SocketEvents.CALLING_SSC, (data) {
      if (SocketSafety.isNotDuplicated(event: SocketEvents.CALLING_SSC, data: data)) {
        if (!AppBloc.videoCallBloc.isCalling && data is! String && data['error'] == null) {
          IncomingCallModel incomingCallModel = IncomingCallModel.fromMap(data);
          if (Platform.isIOS) {
            InitialAppChannel.setIncomingCallModel(incomingCallModel);
          }
          AppBloc.videoCallBloc.add(
            AnswerVideoCallEvent(incomingCallModel: incomingCallModel),
          );
        } else {
          // if (data['error'] != null && data['error']) {
          //   dialogAnimationWrapper(
          //     slideFrom: SlideMode.bot,
          //     child: DialogWithTextAndPopButton(
          //       bodyBefore: 'Máy bận! Hãy thử lại sau.',
          //     ),
          //   );
          // }
        }
      }
    });

    socket!.on(SocketEvents.IGNORE_CALL_SSC, (data) {
      if (SocketSafety.isNotDuplicated(event: SocketEvents.IGNORE_CALL_SSC, data: data)) {
        if (!AppBloc.videoCallBloc.isCalling) {
          AppBloc.videoCallBloc.add(DisposeVideoCallEvent());
        }
      }
    });

    socket!.on(SocketEvents.END_CALL_SSC, (data) {
      if (SocketSafety.isNotDuplicated(event: SocketEvents.END_CALL_SSC, data: data)) {
        AppBloc.videoCallBloc.add(EndVideoCallEvent());
      }
    });

    socket!.on(SocketEvents.LEAVE_ROOM_SSC, (data) {
      if (SocketSafety.isNotDuplicated(event: SocketEvents.LEAVE_ROOM_SSC, data: data)) {
        // AppBloc.videoCallBloc.add(EndVideoCallEvent());
      }
    });

    socket!.on(SocketEvents.ANSWER_SSC, (data) {
      if (SocketSafety.isNotDuplicated(event: SocketEvents.ANSWER_SSC, data: data)) {
        AppBloc.videoCallBloc.add(
          SetRemoteDescriptionEvent(sdp: data),
        );
      }
    });

    socket!.on(SocketEvents.SEND_CANDIDATE_SSC, (data) {
      if (SocketSafety.isNotDuplicated(event: SocketEvents.SEND_CANDIDATE_SSC, data: data)) {
        AppBloc.videoCallBloc.add(
          AddCandidateEvent(candidate: CandidateModel.fromMap(data)),
        );
      }
    });

    socket!.on(SocketEvents.SHARE_SCREEN_SSC, (data) {
      if (SocketSafety.isNotDuplicated(event: SocketEvents.SHARE_SCREEN_SSC, data: data)) {
        AppBloc.videoCallBloc.add(
          AnswerShareVideoCallEvent(sdp: data),
        );
      }
    });

    socket!.on(SocketEvents.STOP_SHARE_SCREEN_SSC, (data) {
      if (SocketSafety.isNotDuplicated(event: SocketEvents.STOP_SHARE_SCREEN_SSC, data: data)) {
        AppBloc.videoCallBloc.add(StopShareScreenEvent());
      }
    });

    socket!.on(SocketEvents.ANSWER_SHARE_SCREEN_SSC, (data) {
      if (SocketSafety.isNotDuplicated(event: SocketEvents.ANSWER_SHARE_SCREEN_SSC, data: data)) {
        AppBloc.videoCallBloc.add(
          SetShareRemoteDescriptionEvent(sdp: data),
        );
      }
    });

    socket!.on(SocketEvents.SEND_CANDIDATE_SHARE_SCREEN_SSC, (data) {
      if (SocketSafety.isNotDuplicated(
          event: SocketEvents.SEND_CANDIDATE_SHARE_SCREEN_SSC, data: data)) {
        AppBloc.videoCallBloc.add(
          AddShareCandidateEvent(candidate: CandidateModel.fromMap(data)),
        );
      }
    });

    socket!.on(SocketEvents.RAISE_HAND_SSC, (data) {
      if (SocketSafety.isNotDuplicated(event: SocketEvents.RAISE_HAND_SSC, data: data)) {
        AppBloc.videoCallBloc.add(
          RemoteRaiseHandEvent(isRaising: data),
        );
      }
    });

    socket!.on(SocketEvents.TOGGLE_CAMERA_SSC, (data) {
      if (SocketSafety.isNotDuplicated(event: SocketEvents.TOGGLE_CAMERA_SSC, data: data)) {
        AppBloc.videoCallBloc.add(
          SetRemoteCameraEvent(isOn: data),
        );
      }
    });

    socket!.on(SocketEvents.TOGGLE_MIC_SSC, (data) {
      if (SocketSafety.isNotDuplicated(event: SocketEvents.TOGGLE_MIC_SSC, data: data)) {
        AppBloc.videoCallBloc.add(
          SetRemoteMicEvent(isOn: data),
        );
      }
    });

    socket!.on(SocketEvents.SEND_MESSAGE_SSC, (data) {
      if (SocketSafety.isNotDuplicated(event: SocketEvents.SEND_MESSAGE_SSC, data: data)) {
        AppBloc.messageBloc.add(InsertMessageEvent(message: MessageModel.fromMap(data['data'])));
      }
    });

    socket!.on(SocketEvents.MESSAGE_UPDATED_SSC, (data) {
      if (SocketSafety.isNotDuplicated(event: SocketEvents.MESSAGE_UPDATED_SSC, data: data)) {
        var message = data['data'];
        AppBloc.messageBloc.add(
          SocketUpdateMessageEvent(messageId: message['_id'], data: message['data']),
        );
      }
    });

    socket!.on(SocketEvents.MESSAGE_REMOVED_SSC, (data) {
      if (SocketSafety.isNotDuplicated(event: SocketEvents.MESSAGE_REMOVED_SSC, data: data)) {
        var message = data['data'];
        AppBloc.messageBloc.add(
          SocketDeleteMessageEvent(messageId: message['_id']),
        );
      }
    });

    socket!.on(SocketEvents.LOGOUT_SSC, (data) {
      if (SocketSafety.isNotDuplicated(event: SocketEvents.LOGOUT_SSC, data: data)) {
        AppBloc.authBloc.add(LogOutEvent());
      }
    });

    socket!.on(SocketEvents.SEND_NOTI_SSC, (data) {
      if (SocketSafety.isNotDuplicated(event: SocketEvents.SEND_NOTI_SSC, data: data)) {
        AppBloc.notificationBloc.add(
          InsertNotificationEvent(notification: NotificationModel.fromMap(data)),
        );
      }
    });

    socket!.on(SocketEvents.TYPING_SSC, (data) {
      if (SocketSafety.isNotDuplicated(event: SocketEvents.TYPING_SSC, data: data)) {
        AppBloc.chatBloc.add(AddTypingConversationEvent(conversationId: data['sendingData']));
      }
    });

    socket!.on(SocketEvents.STOP_TYPING_SSC, (data) {
      if (SocketSafety.isNotDuplicated(event: SocketEvents.STOP_TYPING_SSC, data: data)) {
        AppBloc.chatBloc.add(RemoveTypingConversationEvent(conversationId: data['sendingData']));
      }
    });

    socket!.onDisconnect((_) => debugPrint('disconnect'));

    QueueService.runQueue();
    DeviceHelper().getDeviceInfo().then((value) {
      SocketEmit().sendDeviceInfo();
    });
  });
}

void disconnectBeforeConnect() {
  if (socket != null) {
    if (socket!.connected) {
      socket!.disconnect();
      socket = null;
    }
  }
}

bool isSocketConnected() {
  return socket != null && socket!.connected;
}
