import 'package:askany/src/constants/socket_event.dart';
import 'package:askany/src/helpers/device_helper.dart';
import 'package:askany/src/models/candidate_model.dart';
import 'package:askany/src/models/message_model.dart';
import 'package:askany/src/models/notification_model.dart';
import 'package:askany/src/services/socket/socket.dart';
import 'package:askany/src/services/socket/socket_safety.dart';

class SocketEmit {
  sendCalling({required String sdp, required String receiverId}) {
    SocketSafety.emit(() {
      socket!.emit(SocketEvents.CALLING_CSS, {
        'sdp': sdp,
        'receiverId': receiverId,
      });
    });
  }

  sendAnswer({required String sdp, required String roomId}) {
    SocketSafety.emit(() {
      socket!.emit(SocketEvents.ANSWER_CSS, {
        'room': roomId,
        'sdp': sdp,
      });
    });
  }

  sendCandidate({required CandidateModel candidate, required String roomId}) {
    SocketSafety.emit(() {
      socket!.emit(SocketEvents.SEND_CANDIDATE_CSS, {
        'candidate': candidate.toMap(),
        'room': roomId,
      });
    });
  }

  sendShareOffer({required String? sdp, required String roomId}) {
    SocketSafety.emit(() {
      socket!.emit(SocketEvents.SHARE_SCREEN_CSS, {
        'sdp': sdp,
        'room': roomId,
      });
    });
  }

  sendStopShareScreen({required String roomId}) {
    SocketSafety.emit(() {
      socket!.emit(SocketEvents.STOP_SHARE_SCREEN_CSS, {
        'room': roomId,
      });
    });
  }

  sendShareAnswer({required String sdp, required String roomId}) {
    SocketSafety.emit(() {
      socket!.emit(SocketEvents.ANSWER_SHARE_SCREEN_CSS, {
        'sdp': sdp,
        'room': roomId,
      });
    });
  }

  sendShareCandidate({required CandidateModel candidate, required String roomId}) {
    SocketSafety.emit(() {
      socket!.emit(SocketEvents.SEND_CANDIDATE_SHARE_SCREEN_CSS, {
        'candidate': candidate.toMap(),
        'room': roomId,
      });
    });
  }

  sendRaiseHand({required String roomId, isRaising = true}) {
    SocketSafety.emit(() {
      socket!.emit(SocketEvents.RAISE_HAND_CSS, {
        'room': roomId,
        'isRaising': isRaising,
      });
    });
  }

  sendOnMic({required String roomId, isOnMic = true}) {
    SocketSafety.emit(() {
      socket!.emit(SocketEvents.TOGGLE_MIC_CSS, {
        'room': roomId,
        'isOnMic': isOnMic,
      });
    });
  }

  sendOnCamera({required String roomId, isOnCamera = true}) {
    SocketSafety.emit(() {
      socket!.emit(SocketEvents.TOGGLE_CAMERA_CSS, {
        'room': roomId,
        'isOnCamera': isOnCamera,
      });
    });
  }

  sendEndCall({required String roomId}) {
    SocketSafety.emit(() {
      socket!.emit(SocketEvents.END_CALL_CSS, {
        'room': roomId,
      });
    });
  }

  joinRoom(conversationId) {
    SocketSafety.emit(() {
      socket!.emit(SocketEvents.JOIN_ROOM_CSS, {
        'conversationID': conversationId,
      });
    });
  }

  leaveRoomChat(conversationId) {
    SocketSafety.emit(() {
      socket!.emit(SocketEvents.LEAVE_ROOM_CSS, {
        'conversationID': conversationId,
      });
    });
  }

  sendMessage({required MessageModel message}) {
    SocketSafety.emit(() {
      socket!.emit(SocketEvents.SEND_MESSAGE_CSS, message.toMapSendSocket());
    });
  }

  editMessage({required MessageModel message}) {
    SocketSafety.emit(() {
      socket!.emit(SocketEvents.MESSAGE_UPDATED_CSS, message.toMapSendSocketUpdate());
    });
  }

  deleteMessage({required MessageModel message}) {
    SocketSafety.emit(() {
      socket!.emit(SocketEvents.MESSAGE_REMOVED_CSS, message.toMapSendSocketDelete());
    });
  }

  leaveRoom(roomId) {
    SocketSafety.emit(() {
      socket!.emit(SocketEvents.LEAVE_ROOM_CSS, {
        'room': roomId,
      });
    });
  }

  sendSetActive() {
    SocketSafety.emit(() {
      socket!.emit(
        SocketEvents.SET_ACTIVE_CSS,
        {
          'deviceUuid': DeviceHelper.deviceModel?.deviceUuid,
        },
      );
    });
  }

  sendSetInActive() {
    SocketSafety.emit(() {
      socket!.emit(
        SocketEvents.SET_INACTIVE_CSS,
        {
          'deviceUuid': DeviceHelper.deviceModel?.deviceUuid,
        },
      );
    });
  }

  sendDeviceInfo() async {
    SocketSafety.emit(() async {
      socket!.emit(
        SocketEvents.SEND_DEVICE_CSS,
        DeviceHelper.deviceModel?.toMap(),
      );
    });
  }

  sendLogout() async {
    SocketSafety.emit(() async {
      socket!.emit(
        SocketEvents.LOGOUT_CSS,
        {
          'deviceUuid': DeviceHelper.deviceModel?.deviceUuid,
        },
      );
    });
  }

  sendTyping({required String receiverId, required String conversationId}) {
    SocketSafety.emit(() {
      socket!.emit(SocketEvents.TYPING_CSS, {
        "receiverID": receiverId,
        "conversationID": conversationId,
      });
    });
  }

  sendStopTyping({required String receiverId, required String conversationId}) {
    SocketSafety.emit(() {
      socket!.emit(SocketEvents.STOP_TYPING_CSS, {
        "receiverID": receiverId,
        "conversationID": conversationId,
      });
    });
  }

  sendNotification(NotificationModel notification) {
    SocketSafety.emit(() {
      socket!.emit(
        SocketEvents.SEND_NOTI_CSS,
        notification.toMap(),
      );
    });
  }
}
