import 'package:askany/src/constants/constants.dart';
import 'package:flutter/services.dart';

class ForegroundService {
  static const MethodChannel _shareScreenChannel = const MethodChannel(SHARE_SCREEN_CHANNEL);

  static Future<void> startForegroundService() async {
    await _shareScreenChannel.invokeMethod("START");
  }

  static Future<void> stopForegroundService() async {
    await _shareScreenChannel.invokeMethod("STOP");
  }
}
