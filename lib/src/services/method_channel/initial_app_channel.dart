import 'dart:io';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/models/incoming_call_model.dart';
import 'package:flutter/services.dart';

class InitialAppChannel {
  static const MethodChannel _initialChannel = const MethodChannel(INITIAL_APP_CHANNEL);
  static IncomingCallModel? incomingCallModel;
  static bool isNavigationFromIncoming = false;
  static bool flagCheck = false;

  static Future<bool> checkIntentFromIncomingCall() async {
    if (Platform.isAndroid) {
      if (flagCheck) {
        return false;
      }

      flagCheck = true;
      bool response = await _initialChannel.invokeMethod("arguments") ?? false;
      return response;
    } else {
      flagCheck = true;
      return isNavigationFromIncoming;
    }
  }

  static void resetFlag() {
    if (Platform.isIOS) {
      isNavigationFromIncoming = false;
    }
  }

  static Future<void> setNavigationFromIncoming(bool value) async {
    isNavigationFromIncoming = value;
  }

  static Future<void> setIncomingCallModel(IncomingCallModel model) async {
    incomingCallModel = model;
  }
}
