// import 'package:askany/src/routes/app_pages.dart';
// import 'package:momo_vn/momo_vn.dart';

// class MomoAppPayment {
//   MomoVn? _momoPay;

//   handlePaymentMomo(int amount) {
//     MomoPaymentInfo options = MomoPaymentInfo(
//       merchantName: "Askany",
//       appScheme: "momoiwtv20220329",
//       merchantCode: 'MOMOIWTV20220329',
//       partnerCode: 'MOMOIWTV20220329',
//       amount: amount,
//       orderId: DateTime.now().microsecondsSinceEpoch.toString(),
//       orderLabel: 'THANH TOÁN NẠP TIỀN ASKANY',
//       merchantNameLabel: "THANH TOÁN NẠP TIỀN ASKANY",
//       fee: 0,
//       description: 'Nạp tiền vào ví Askany',
//       partner: 'merchant',
//       isTestMode: true,
//       extra: "{\"key1\":\"value1\",\"key2\":\"value2\"}",
//     );
//     try {
//       _momoPay = MomoVn();
//       _momoPay?.on(MomoVn.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
//       _momoPay?.on(MomoVn.EVENT_PAYMENT_ERROR, _handlePaymentError);
//       _momoPay?.open(options);
//     } catch (e) {
//     print(e.toString());
//     }
//   }

//   void _handlePaymentSuccess(PaymentResponse response) {
//     AppNavigator.pop();
//     print('THANH TOAN MOMO THANH CONG');
//     _momoPay?.clear();
//   }

//   void _handlePaymentError(PaymentResponse response) {
//     AppNavigator.pop();
//     print('THANH TOAN MOMO THAT BAI');
//     _momoPay?.clear();
//   }
// }
