// Platform messages are asynchronous, so we initialize in an async method.
import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/video_call/video_call_bloc.dart';
import 'package:askany/src/helpers/utils/logger.dart';
import 'package:askany/src/services/method_channel/initial_app_channel.dart';
import 'package:askany/src/services/vibrate/vibrate_incoming_call.dart';
import 'package:flutter/services.dart';
import 'package:flutter_callkit_incoming/flutter_callkit_incoming.dart';

var textEvents = "";
Future<void> listenerEvent() async {
  // Platform messages may fail, so we use a try/catch PlatformException.
  // We also handle the message potentially returning null.
  try {
    FlutterCallkitIncoming.onEvent.listen((event) async {
      UtilLogger.log('FlutterCallkitIncoming.onEvent.listen', event);
      // if (!mounted) return;
      switch (event!.name) {
        case CallEvent.ACTION_CALL_INCOMING:
          UtilLogger.log('CallEvent.ACTION_CALL_INCOMING');
          break;
        case CallEvent.ACTION_CALL_START:
          UtilLogger.log('CallEvent.ACTION_CALL_START');
          // InitialAppChannel.setNavigationFromIncoming(true);
          // AppBloc.videoCallBloc.add(
          //   StartVideoCallEvent(incomingCallModel: InitialAppChannel.incomingCallModel!),
          // );
          break;
        case CallEvent.ACTION_CALL_ACCEPT:
          UtilLogger.log('CallEvent.ACTION_CALL_ACCEPT');
          InitialAppChannel.setNavigationFromIncoming(true);
          AppBloc.videoCallBloc.add(
            StartVideoCallEvent(incomingCallModel: InitialAppChannel.incomingCallModel!),
          );
          AppBloc.videoCallBloc.add(ToggleVideoEvent());
          stopRingAndVibrateIncomingCall();
          break;
        case CallEvent.ACTION_CALL_DECLINE:
          UtilLogger.log('CallEvent.ACTION_CALL_DECLINE');
          stopRingAndVibrateIncomingCall();
          break;
        case CallEvent.ACTION_CALL_ENDED:
          UtilLogger.log('CallEvent.ACTION_CALL_ENDED');
          AppBloc.videoCallBloc.add(EndVideoCallEvent());
          break;
        case CallEvent.ACTION_CALL_TIMEOUT:
          UtilLogger.log('CallEvent.ACTION_CALL_TIMEOUT');
          break;
        case CallEvent.ACTION_CALL_CALLBACK:
          UtilLogger.log('CallEvent.ACTION_CALL_CALLBACK');
          break;
        case CallEvent.ACTION_CALL_TOGGLE_HOLD:
          UtilLogger.log('CallEvent.ACTION_CALL_TOGGLE_HOLD');
          break;
        case CallEvent.ACTION_CALL_TOGGLE_MUTE:
          UtilLogger.log('CallEvent.ACTION_CALL_TOGGLE_MUTE');
          AppBloc.videoCallBloc.add(ToggleMicEvent());
          break;
        case CallEvent.ACTION_CALL_TOGGLE_DMTF:
          UtilLogger.log('CallEvent.ACTION_CALL_TOGGLE_DMTF');
          break;
        case CallEvent.ACTION_CALL_TOGGLE_GROUP:
          UtilLogger.log('CallEvent.ACTION_CALL_TOGGLE_GROUP');
          break;
        case CallEvent.ACTION_CALL_TOGGLE_AUDIO_SESSION:
          UtilLogger.log('CallEvent.ACTION_CALL_TOGGLE_AUDIO_SESSION');
          break;
      }
      // setState(() {
      //   textEvents += "${event.toString()}\n";
      // });
    });
  } on Exception {}
}

Future<void> endCurrentCallIncomming() async {
  String? uuidCurent = await getUuidCallPartner();

  print("endCurrentCallIncomming" + (uuidCurent ?? ''));
  if (uuidCurent != null) {
    var params = <String, dynamic>{'id': uuidCurent};
    await FlutterCallkitIncoming.endCall(params);
  }
}

Future<String?> getVoipServiceTokenDevice() async {
  const platform = MethodChannel('com.askany.ndn/getVoipDeviceTokent');
  try {
    final String result = await platform.invokeMethod('getDeviceTokentVoip');
    return result;
  } on PlatformException catch (e) {
    UtilLogger.log('getVoipServiceTokenDevice.PlatformException', e.message);
    return "";
  }
}

Future<String?> getUuidCallPartner() async {
  const platform = MethodChannel('com.askany.ndn/uuidCall');
  try {
    return await platform.invokeMethod('getUuidCall');
  } on PlatformException catch (e) {
    return e.message;
  }
}

Future<bool?> isLockScreen() async {
  const _channel = MethodChannel('com.askany.ndn/checkLock');
  try {
    return await _channel.invokeMethod('isLockScreen') as bool?;
  } catch (e) {
    UtilLogger.log('isLockScreen.PlatformException', e);
  }
}

Future<void> enableSound() async {
  const _channel = MethodChannel('com.askany.ndn/enableSoundSpeaker');
  try {
    await _channel.invokeMethod('enableSound');
  } catch (e) {
    UtilLogger.log('enableSound', e);
  }
}
