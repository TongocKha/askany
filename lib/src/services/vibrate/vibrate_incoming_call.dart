import 'dart:async';
import 'dart:io';
import 'package:flutter_ringtone_player/flutter_ringtone_player.dart';
import 'package:vibration/vibration.dart';

bool isCheckStop = false;
int counter = 0;
Timer? timer;
void startVibrationIncommingcallIOS() {
  timer = Timer.periodic(Duration(seconds: 1), (timer) {
    if (counter < 20) {
      if (isCheckStop) {
        isCheckStop = false;
        counter = 0;
        timer.cancel();
      } else {
        if (counter % 2 == 0) {
          Vibration.vibrate(duration: 500);
        } else {
          Vibration.vibrate(duration: 1000);
        }
      }
    } else {
      timer.cancel();
      counter = 0;
    }
  });
}

void stopVibrationIncomingCallIOS() {
  isCheckStop = true;
}

void startRingAndVibrateIncomingCall() {
  if (Platform.isAndroid) {
    FlutterRingtonePlayer.play(
      android: AndroidSounds.ringtone,
      ios: IosSounds.glass,
      looping: true,
      volume: 20.0,
      asAlarm: false,
    );
    Vibration.vibrate(
      pattern: List.generate(20, (index) => index % 2 == 0 ? 500 : 1000),
    );
  } else {
    FlutterRingtonePlayer.play(
      android: AndroidSounds.ringtone,
      ios: IosSounds.glass,
      looping: true,
      volume: 20.0,
      asAlarm: false,
    );
    startVibrationIncommingcallIOS();
  }
}

void stopRingAndVibrateIncomingCall() {
  if (Platform.isAndroid) {
    FlutterRingtonePlayer.stop();
    Vibration.cancel();
  } else {
    FlutterRingtonePlayer.stop();
    stopVibrationIncomingCallIOS();
  }
}
