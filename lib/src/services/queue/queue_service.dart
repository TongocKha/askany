import 'dart:collection';

class QueueService {
  Queue<Function> _queue = Queue<Function>();
  static String msg = "QUEUE_SERVICE";

  // Global Queue
  static void addQueue(Function fn) async {
    QueueService()._queue.add(fn);
  }

  static void runQueue() {
    QueueService()._queue.forEach((fn) {
      fn();
      QueueService()._queue.removeFirst();
    });
  }
}
