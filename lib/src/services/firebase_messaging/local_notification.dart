import 'dart:convert';
import 'dart:typed_data';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/data/remote_data_source/image_repository.dart';
import 'package:askany/src/services/firebase_messaging/handle_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class LocalNotification {
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      new FlutterLocalNotificationsPlugin();

  onSelectNotification({required String? notify}) {
    if (notify != null) {
      Map data = jsonDecode(notify);
      navigatorToScreen(data['route'], data);
    }
  }

  Future showNotificationWithSound({
    required int hashCode,
    required String title,
    required String description,
    dynamic payload,
    String? urlToImage,
  }) async {
    ByteArrayAndroidBitmap? bigPicture;
    BigPictureStyleInformation? bigPictureStyleInformation;

    if (urlToImage != null) {
      Uint8List? imageBuffer = await ImageRepository().getByteArrayFromUrl(
        urlToImage,
      );
      if (imageBuffer != null) {
        bigPicture = ByteArrayAndroidBitmap(
          imageBuffer,
        );
        bigPictureStyleInformation = BigPictureStyleInformation(
          bigPicture,
          contentTitle: title,
          htmlFormatContentTitle: true,
          summaryText: description,
          htmlFormatSummaryText: true,
        );
      }
    }
    flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
    var android = new AndroidInitializationSettings(APPLICATION_ANDROID_ICON);
    var iOS = new IOSInitializationSettings();
    var initSetttings = new InitializationSettings(android: android, iOS: iOS);

    // On select notification
    flutterLocalNotificationsPlugin.initialize(
      initSetttings,
      onSelectNotification: (String? notify) {
        return onSelectNotification(notify: notify);
      },
    );

    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
      NOTIFICATION_CHANNEL_ID,
      title,
      importance: Importance.max,
      priority: Priority.max,
      largeIcon: bigPicture,
      styleInformation: bigPictureStyleInformation,
      enableVibration: false,
      enableLights: true,
      autoCancel: true,
      playSound: false,
    );

    // You can custom sound and badge here. Need a file aiff... Tks bro
    var iOSPlatformChannelSpecifics = new IOSNotificationDetails(
      sound: 'alarm.aiff',
    );
    var platformChannelSpecifics = new NotificationDetails(
      android: androidPlatformChannelSpecifics,
      iOS: iOSPlatformChannelSpecifics,
    );

    await flutterLocalNotificationsPlugin.show(
      hashCode,
      title,
      description,
      platformChannelSpecifics,
      payload: jsonEncode(payload),
    );

    Future.delayed(Duration(seconds: DELAY_TWO_SECONDS * 5), () async {
      await cancelNotification(hashCode);
    });
  }

  Future<void> cancelNotification(int id) async {
    await flutterLocalNotificationsPlugin.cancel(id);
  }
}
