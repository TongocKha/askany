import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/home/home_bloc.dart';
import 'package:askany/src/models/conversation_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/services/firebase_messaging/local_notification.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

// DEFINE CONSTANT
const String PROFILE = 'profile';
const String HOME = 'home';
const String CHAT = 'chat';
const String NOTIFICATION = 'notification';
const String CONVERSATION = 'conversation';

navigatorToScreen(String screenKey, dynamic payload) {
  switch (screenKey) {
    case PROFILE:
      break;
    case HOME:
      AppBloc.homeBloc.add(OnChangeIndexEvent());
      AppNavigator.popUntil(Routes.ROOT);
      break;
    case CHAT:
      if (AppNavigator.currentRoute() != Routes.ROOT) {
        AppNavigator.replaceWith(Routes.CHAT);
      } else {
        AppNavigator.push(Routes.CHAT);
      }
      Map<String, dynamic> conversationRaw = payload as Map<String, dynamic>;
      conversationRaw.remove('route');
      AppNavigator.push(Routes.CONVERSATION, arguments: {
        'conversationModel': ConversationModel.fromMap(conversationRaw),
      });
      break;
    case NOTIFICATION:
      AppBloc.homeBloc.add(OnChangeIndexEvent(index: 3));
      AppNavigator.popUntil(Routes.ROOT);
      break;
    case CONVERSATION:
      if (AppNavigator.currentRoute() != Routes.ROOT) {
        AppNavigator.replaceWith(Routes.CHAT);
      } else {
        AppNavigator.push(Routes.CHAT);
      }
      break;
    default:
      break;
  }
}

Future<String?> getFirebaseMessagingToken() async {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;
  String? token = await _firebaseMessaging.getToken();
  return token;
}

Future<String?> getDeviceTokenAPNS() async {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;
  String? token = await _firebaseMessaging.getAPNSToken();
  return token;
}

Future<void> requestPermission() async {
  FirebaseMessaging messaging = FirebaseMessaging.instance;
  NotificationSettings settings = await messaging.requestPermission(
    alert: true,
    announcement: false,
    badge: true,
    carPlay: false,
    criticalAlert: false,
    provisional: false,
    sound: true,
  );
  if (settings.authorizationStatus == AuthorizationStatus.authorized) {
    debugPrint('User granted permission');
  } else if (settings.authorizationStatus == AuthorizationStatus.provisional) {
    debugPrint('User granted provisional permission');
  } else {
    debugPrint('User declined or has not accepted permission');
  }
  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
    alert: false,
    badge: false,
    sound: false,
  );
}

handleReceiveNotification() async {
  FirebaseMessaging.instance.getInitialMessage().then((RemoteMessage? message) {
    handleTouchOnNotification(message);
  });

  FirebaseMessaging.onMessage.listen((RemoteMessage message) async {
    if (message.data['route'] != CHAT) {
      LocalNotification().showNotificationWithSound(
        hashCode: message.hashCode,
        title: message.notification!.title!,
        description: message.notification!.body!,
        payload: message.data,
        urlToImage: message.notification?.android?.imageUrl,
      );
    }
  });

  FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
    debugPrint('A new onMessageOpenedApp event was published!' + ' ${message.data.toString()}');

    handleTouchOnNotification(message);
  }).onError((error) => print('Error: $error [\'lambiengcode\']'));
}

handleTouchOnNotification(RemoteMessage? message) {
  navigatorToScreen(message?.data['route'] ?? '', message?.data);
}
