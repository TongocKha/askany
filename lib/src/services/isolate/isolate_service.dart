import 'dart:async';
import 'package:flutter_isolate/flutter_isolate.dart';

class IsolateService {
  static Future<void> createIsolate({String msg = 'THREAD', required Function callBack}) async {
    await FlutterIsolate.spawn((arg) => callBack(), msg);
  }

  static void killIsolate(FlutterIsolate isolate) {
    isolate.kill();
  }
}
