import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/request/request_bloc.dart';
import 'package:askany/src/bloc/skill/skill_bloc.dart';
import 'package:askany/src/bloc/user/user_bloc.dart';
import 'package:askany/src/configs/lang/language_service.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/data/local_data_source/base_local_data.dart';
import 'package:askany/src/models/slide_mode.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/services/firebase_messaging/handle_messaging.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_notify_error.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

import 'lang/localization.dart';

class Application {
  /// [Production - Dev]
  static String baseUrl = 'https://service-askany.damdev.tk/';
  static String socketUrl = 'https://socket-askany.damdev.tk/';
  static const String websiteUrl = 'https://askanydev.tk/';
  static const String backupUrl = 'http://139.180.159.84/';
  static const String backupSocketUrl = 'http://139.180.184.80/';
  static const String mode = MODE_PRODUCTION;
  static const bool canChat = true;
  static bool isShowingError = false;

  Future<void> initialAppLication(BuildContext context) async {
    try {
      await BaseLocalData.initialBox();
      await Firebase.initializeApp();

      // Listen FCM Notifications
      handleReceiveNotification();

      // Initial language
      LanguageService().initialLanguage(context);

      AppBloc.userBloc.add(GetLanguagesEvent());
      AppBloc.requestBloc.add(GetSpecialtyEvent());
      AppBloc.skillBloc.add(GetPositionsEvent());
    } catch (error) {
      debugPrint(error.toString());
    }
  }

  void switchDomainToBackup() {
    baseUrl = backupUrl;
    socketUrl = backupSocketUrl;
    AppNavigator.pop();
    if (!isShowingError) {
      isShowingError = true;
      dialogAnimationWrapper(
        dismissible: false,
        borderRadius: 10.sp,
        paddingBottom: 80.h,
        slideFrom: SlideMode.top,
        duration: DELAY_HALF_SECOND,
        timeForDismiss: DELAY_HALF_SECOND * 4,
        child: DialogNotifyError(
          title: Strings.connectionFailed.i18n,
          body: Strings.connectAgain.i18n,
        ),
      );
    }
  }

  ///Singleton factory
  static final Application _instance = Application._internal();

  factory Application() {
    return _instance;
  }

  Application._internal();
}
