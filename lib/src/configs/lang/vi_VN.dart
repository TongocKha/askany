import 'package:askany/src/configs/lang/localization.dart';

Map<String, String> vietnamese = {
  // Language
  Strings.vietnamese: 'Tiếng Việt',
  Strings.english: 'Tiếng Anh',

  // Home
  Strings.home: 'Trang chủ',
  Strings.category: 'Danh mục',
  Strings.calendar: 'Lịch',
  Strings.request: 'Yêu cầu',
  Strings.message: 'Tin nhắn',
  Strings.notification: 'Thông báo',
  Strings.account: 'Tài khoản',

  // Request
  Strings.serviceRequest: 'Yêu cầu dịch vụ của tôi',
  Strings.bidServiceRequest: 'Yêu cầu dịch vụ đã chào giá',
  Strings.happening: 'Đang diễn ra',
  Strings.finished: 'Đã kết thúc',

  // Account Screen

  Strings.verifiedAccount: 'Đã xác thực',
  Strings.unVerifiedAccount: 'Chưa xác thực',
  Strings.myServiceManagementTitle: 'Quản lý dịch vụ của tôi',
  Strings.expertProfile: 'Hồ sơ chuyên gia',
  Strings.personalInfor: 'Thông tin cá nhân',
  Strings.changePassword: 'Đổi mật khẩu',
  Strings.verifyAccountAppbartext: 'Xác thực tài khoản',
  Strings.languages: 'Ngôn ngữ',

  // Videocall

  Strings.sharingScreenNoti: 'Bạn đang chia sẻ màn hình',

  Strings.mySkills: 'Kỹ năng',
  Strings.bodyOfAddSkillDialog:
      'Hãy tạo thêm kỹ năng để được ưu tiên\nhiển thị trên mục Chuyên gia của\nchúng tôi!',

  Strings.addMoreSkillsWithPlus: "+ Thêm kỹ năng",
  Strings.yearsOfExperience: " năm kinh nghiệm",
  Strings.yearOfExperience: " năm kinh nghiệm",
  Strings.createFirstSkillSugesstion: 'Bạn chưa có kỹ năng nào, hãy tạo ngay!',
  Strings.createNewSkill: 'Tạo kỹ năng',
  Strings.verifyEmailAppbarText: 'Xác thực email',
  Strings.verifyEmailSuggestion:
      'Bạn vẫn chưa xác thực email,\nhãy xác thực ngay để nhận những phần quà\nhấp dẫn từ Askany nhé.',

  // Account verification
  Strings.yourEmailHasBeenVerified: 'Email của bạn đã được xác thực.',
  Strings.pleaseOpenInboxOf: 'Vui lòng truy cập',
  Strings.openInboxSuggestion:
      'và làm theo hướng dẫn để xác thực email. (Lưu ý kiểm tra mục Spam nếu không nhận được email xác thực)',
  Strings.verifyNow: 'Xác thực ngay',
  Strings.changeYourEmail: 'Thay đổi email',

  // AccountVerificationEmailSuccessScreen

  Strings.congratulationYouHasSuccesfullyVerifiedYourEmailPart1:
      'Chúc mừng bạn',
  Strings.congratulationYouHasSuccesfullyVerifiedYourEmailPart2:
      'đã xác thực email thành công',
  Strings.nowYouCanExperiencingOurAmazingFeaturePart1:
      'Hiện tại bạn có thể bắt đầu trải nghiệm những',
  Strings.nowYouCanExperiencingOurAmazingFeaturePart2:
      'tính năng tuyệt vời của chúng tôi',
  Strings.backToHomePage: 'Quay về trang chủ',

  // AccountVerificationPhoneScreen
  Strings.verifyYourPhoneAppbarText: 'Xác thực số điện thoại',
  Strings.verifyPhoneSuggestion:
      'Bạn vẫn chưa xác thực số điện thoại,\nhãy xác thực ngay để nhận những phần quà\nhấp dẫn từ Askany nhé.',
  Strings.yourPhoneNumberHasBeenVerified:
      'Số điện thoại của bạn đã được xác thực.',
  Strings.changeYourPhoneNumber: 'Thay đổi số điện thoại',

  // AccountVerificationScreen

  Strings.verifyYourCompanyAndPositionAppbartext: 'Xác thực công ty và chức vụ',

  // AccountVertifyCompanyScreen
  Strings.processing: 'Đang xử lý',
  Strings.confirmed: 'Đã xác nhận',
  Strings.rejected: 'Từ chối',
  Strings.yourCompanyAndPositionHaveBeenVerified:
      "Công ty và chức vụ của bạn đã được xác thực.",
  Strings.showEmploymentContractSuggestion:
      "Bạn có thể để lại link tài khoản Linkedin hoặc tải lên ảnh hợp đồng lao động để chúng tôi có thể xác thực Công ty và chức vụ của bạn.",
  Strings.linkedinAccount: "Tài khoản Linkedin",
  Strings.linkedinLinkHintext: "Link Linkedin",
  Strings.atLeast3Characters: "",
  Strings.eitherUploadPhoto: "Hoặc tải lên ảnh xác thực",
  Strings.notice: "Thông báo!",
  Strings.privideEnoughInformationNotice:
      "Bạn vui lòng thêm đầy đủ thông tin để chúng tôi có thể xác thực.",
  Strings.verificationInformationSentSuccessfully: "Đã gửi thông tin xác thực!",
  Strings.checkWithin7Days:
      "Chúng tôi sẽ kiểm tra và xử lý xác thực cho bạn trong vòng 7 ngày làm việc.",

  // AddSkillScreen
  Strings.vnd: "VND",
  Strings.usd: "USD",
  Strings.addMoreAppbarText: "Thêm",
  Strings.editAppbarText: "Chỉnh sửa",
  Strings.skillLowerKeyAppbarText: " kỹ năng",
  Strings.selecrASectorSuggestionTitle: "Chọn lĩnh vực bạn có kỹ năng",
  Strings.selectASectorHintext: 'Chọn lĩnh vực',
  Strings.haveNotSelectedAnySectorErrortext: 'Bạn chưa chọn lĩnh vực',
  Strings.nameOfYourSkillTitle: "Tên kỹ năng bạn có",
  Strings.skillNameHintext: "Tên kỹ năng",
  Strings.skillNameCannotBeLeftBlankErrortext: 'Bạn chưa nhập tên kỹ năng',
  Strings.yearsCountOfExprieceTitle: "Số năm kinh nghiệm",
  Strings.enterYearsCountOfExperienceHintext: 'Kinh nghiệm trong lĩnh vực',
  Strings.yearsCountOfExperienceCannotBeBlankErrortext:
      'Bạn chưa nhập số năm kinh nghiệm',
  Strings.whatYourHighestPositionTitle: "Chức vụ cao nhất bạn đạt được?",
  Strings.selectYourHighestPositionHintext: 'Chọn chức vụ cao nhất',
  Strings.positionCannotBeLeftBlankErrortext: 'Bạn chưa chọn chức vụ',
  Strings.whatYourCurrentCompanyTitle: "Công ty hiện tại của bạn?",
  Strings.companyNameHintext: 'Tên công ty',
  Strings.companyNameCannotBeLeftBlankErrortext: 'Bạn chưa nhập tên công ty',
  Strings.skillDescriptionTitle: "Mô tả kỹ năng",
  Strings.describeYourSkillHintext: 'Hãy mô tả về kỹ năng của bạn',
  Strings.skillDescribeCannotBeLeftBlankErrorText:
      'Bạn chưa nhập mô tả về kỹ năng của bạn',
  Strings.costOfASingleCallingServiceTitle:
      'Giá một gói gọi điện (Mỗi 15 phút)',
  Strings.fifteenMinutes: '15 phút',
  Strings.costCannotBeLeftblankErrortext: 'Xin nhập giá',
  Strings.costOfASingleMeetingServiceTitle:
      'Giá một gói gặp mặt trực tiếp (Mỗi 60 phút)',
  Strings.sixtyMinutes: '60 phút',
  Strings.participantsCountSuggetion:
      "Lưu ý: Nếu có 2 người tham gia thi mức giá không đổi, nếu có nhiều hơn 2 người tham gia thì phí dịch vụ sẽ cộng thêm 50%.",
  Strings.call: 'Gọi điện',
  Strings.meet: 'Tư vấn trực tiếp',
  Strings.serviceKeywordTitle: 'Từ khóa về gói dịch vụ (tối đa 3 từ)',
  Strings.serviceKeywordHintext: 'Từ khoá',
  Strings.serviceKeyWordBlankErrortext: 'Bạn chưa nhập từ khóa nào',
  Strings.addMoreSkillWithoutPlus: 'Thêm kĩ năng',
  Strings.save: 'Lưu',
  Strings.clear: 'Xóa',
  Strings.areYouSureToSlearThisSkill: 'Bạn chắc chắn muốn xóa kỹ năng này?',
  Strings.cancel: 'Hủy',
  Strings.deleteThisSkill: 'Xóa kỹ năng này',

  // ChangeEmailScreen

  Strings.changeEmailAppbarText: 'Thay đổi email',
  Strings.enterYourEmailSugestion: 'Bạn vui lòng nhập email mới để thay đổi.',
  Strings.emailHintext: 'Email',
  Strings.emailMustBeAtLeast3CharactersErrortext:
      'Email phải có ít nhất 3 ký tự',
  Strings.next: 'Tiếp theo',

  // ChangePasswordScreen
  Strings.changePasswordAppbarText: 'Đổi mật khẩu',
  Strings.currentPasswordTitle: 'Mật khẩu cũ',
  Strings.yourPassWordMustBeAtLeast6CharactersErrortext:
      'Mật khẩu phải có ít nhất 6 ký tự',
  Strings.newPasswordTitle: 'Mật khẩu mới',
  Strings.confirmPasswordTitle: 'Xác nhận mật khẩu mới',
  Strings.passwordDoesNotMatchErrortext: 'Mật khẩu mới không khớp',

  // ChangePhoneScreen

  Strings.changePhoneNumberAppbarText: 'Thay đổi số điện thoại',
  Strings.enterPhoneNumberSuggestion:
      'Bạn vui lòng nhập số điện thoại mới để thay đổi.',
  Strings.phoneNumberHintext: 'Số điện thoại',
  Strings.phoneNumberMustBeAtLeast9CharactersErrortext:
      'Số diện thoại phải có ít nhất 9 ký tự',

  // EditProfileScreen

  Strings.editProfileAppbartext: 'Thông tin cá nhân',
  Strings.update: "Cập nhật",
  Strings.nameTitle: 'Tên',
  Strings.nameMustBeAtLeast3CharactersErrortext: 'Tên phải có ít nhất 3 ký tự',
  Strings.addressTitle: 'Nơi ở',
  Strings.address: 'Nơi ở',
  Strings.phoneNumberTitle: 'Số điện thoại',
  Strings.verifyYourPhoneNumber: 'Xác thực số điện thoại',
  Strings.logout: 'Đăng xuất',

  // ExperienceScreen

  Strings.exprienceAppbarText: 'Kinh nghiệm',
  Strings.createExperienceSuggestion:
      'Bạn chưa có kinh nghiệm nào, hãy tạo ngay!',
  Strings.createNewExprience: 'Tạo kinh nghiệm',
  Strings.createNewExprienceWithPlus: "+ Thêm kinh nghiệm làm việc",

  // ExpertEditPriceScreen
  Strings.priceOfASingleServiceAppbartext: 'Giá một gói tư vấn',
  Strings.enterPriceErrortext: 'Xin nhập giá',
  Strings.availableTimeTitle: "Thời gian có thể tư vấn",
  Strings.availableTimeSelectionNoti:
      "Lưu ý: Bạn có thể chọn liên tiếp nhiều ngày và thêm cùng một khoảng thời gian cố định như nhau.",
  Strings.availableTimeNotValid: 'Không thể thêm ngày',
  Strings.availableTimeSelectionNoti:
      'Bạn đã đặt giờ tất cả các ngày, hãy chỉnh sửa cho phù hợp với thời gian của bạn!',
  Strings.addMoreAvailableDayWithPlus: '+ Chọn thêm ngày khác',
  Strings.addMoreAvailableTimeWithPlus: '+ Thêm thời gian',
  Strings.cannotAddThisAvailableTime: 'Không thể thêm khung giờ',
  Strings.pleaseSelecteAvailableDayFirst:
      'Bạn chưa chọn ngày rảnh, hãy chọn ngày trước khi thêm thời gian rảnh.',
  Strings.priceMustOverHaflBuckErorrtext:
      'Giá 1 gói phải lớn hơn hoặc bằng 0.5 \$',
  Strings.priceMustbeOver12k: 'Giá 1 gói phải lớn hơn hoặc bằng 12.000 VNĐ',
  Strings.noTimeSelectedYet: 'Chưa chọn thời gian',
  Strings.noTimeSelectedYetNoti:
      'Hãy chọn đầy đủ thời gian để hoàn tất chỉnh sửa!',

  // InfoExpertScreen

  Strings.expertInfomationAppbartext: 'Thông tin chuyên gia',
  Strings.howManyLanguages: 'Bạn có thể nói được ngôn ngữ nào?',
  Strings.blankLanguageErrortext: 'Bạn chưa chọn ngôn ngữ',
  Strings.whatSectorYouHaveExprence:
      'Bạn có kinh nghiệm thực tế ở lĩnh vực nào?',
  Strings.sectorOfExpertise: 'Lĩnh vực chuyên môn',
  Strings.sector: 'Lĩnh vực',
  Strings.sectorCannotBeLeftBlankErrortext: 'Bạn chưa chọn lĩnh vực',
  Strings.howManyYearsOfExprience: 'Số năm kinh nghiệm của bạn?',
  Strings.yearsCountOfExperienceHinttext: 'Số năm kinh nghiệm',
  Strings.yearsCountOfExperienceCannotBeBlankErrortext:
      'Bạn chưa nhập số năm kinh nghiệm !',

  Strings.yourCompanyYouWorkForTitle: 'Công ty làm việc của bạn?',
  Strings.descriptionAboutYourExperienceAndSkills:
      'Giới thiệu về kinh nghiệm và kỹ năng của bạn',
  Strings.enterDescriptionAboutYouHintext: 'Nhập nội dung',
  Strings.descriptionAboutYouCannotBeLeftBlankErrortext:
      'Hãy viết vài dòng giới thiệu về bạn',

  // InputOTPScreen

  Strings.enterOTPsuggestion:
      'Vui lòng kiểm tra tin nhắn và nhập mã vào ô dưới đây.',
  Strings.verify: 'Xác thực',

  // ProfileExpertScreen

  Strings.viewInPage: 'Xem trang',
  Strings.review: 'đánh giá',
  Strings.reviews: 'đánh giá',
  Strings.reviewsAppbartext: 'Đánh giá',

  // ServiceManagamentScreen

  Strings.youDontHavenAnyRequest: 'Bạn chưa có yêu cầu dịch vụ nào!',
  Strings.confirm: 'Xác nhận',

  // Components

  // BottomSheetPosition
  Strings.yourPosition: 'Chức vụ của bạn',

  //ContainerManageAccount

  Strings.consultingSchedule: 'Lịch tư vấn',
  Strings.myWallet: 'Ví của tôi',

  // ManageServiceArea

  Strings.confirming: 'Chờ xác nhận',
  Strings.advise: 'Tư vấn',
  Strings.completed: 'Hoàn thành',

  // ServiceManagamentCard
  Strings.areYouSureYouWouldLikeToConsultService:
      'Bạn xác nhận tư vấn gói dịch vụ ',
  Strings.agree: 'Xác nhận',
  Strings.expert: 'Chuyên gia',
  Strings.experts: 'Chuyên gia',
  Strings.callingPackage: 'Gói gọi điện',
  Strings.meetingPackage: 'Gói gặp mặt',
  Strings.place: 'Địa điểm',
  Strings.waitForPayment: 'Chờ thanh toán',
  Strings.reportProcessing: 'Đang xử lý',
  Strings.rpClosed: 'Đã đóng',

  // PeriodContainer

  Strings.monday: 'Thứ 2',
  Strings.sunday: 'Chủ nhật',
  Strings.month[0]: 'Tháng 1',
  Strings.month[1]: 'Tháng 2',
  Strings.month[2]: 'Tháng 3',
  Strings.month[3]: 'Tháng 4',
  Strings.month[4]: 'Tháng 5',
  Strings.month[5]: 'Tháng 6',
  Strings.month[6]: 'Tháng 7',
  Strings.month[7]: 'Tháng 8',
  Strings.month[8]: 'Tháng 9',
  Strings.month[9]: 'Tháng 10',
  Strings.month[10]: 'Tháng 11',
  Strings.month[11]: 'Tháng 12',

  Strings.repeatEveryWeek: 'Lặp lại mỗi tuần',
  Strings.availableForCalling: 'Có thể gọi điện',
  Strings.availableForMeeting: 'Có thể gặp mặt',
  Strings.addMoreAvailableDayWithPlus: '+ Thêm thời gian',

  // SkillCard

  Strings.minute: 'phút',
  Strings.minutes: 'phút',

  // AddExperienceScreen
  Strings.add: 'Thêm',
  Strings.experienceInWork: 'kinh nghiệm làm việc',

  // FormAddExperience
  Strings.yourExperience: 'Kinh nghiệm làm việc của bạn',
  Strings.company: 'Công ty',
  Strings.specialization: 'Chuyên môn',

  Strings.underStood: 'Đã hiểu',
  Strings.send: 'Gửi',
  Strings.specializationCannotBeLeftBlankErrortext: 'Bạn chưa chọn chuyên môn',
  Strings.positionInWork: 'Chức vụ',
  Strings.fromTime: 'Thời gian bắt đầu',
  Strings.monthCannotBeLeftBlankErrortext: 'Bạn chưa chọn tháng',
  Strings.year: 'Năm',
  Strings.yearCannotBeLeftBlankErrortext: 'Bạn chưa chọn năm',
  Strings.toTime: 'Thời gian kết thúc',
  Strings.detailsOfWork: 'Chi tiết công việc',
  Strings.descriptionAboutYouCannotBeLeftBlankErrortext:
      'Bạn chưa nhập nội dung',
  Strings.areYouSureToClearThisExperience:
      'Bạn chắc chắn muốn xóa kinh nghiệm này?',
  Strings.deleteThisExperience: 'Xóa kinh nghiệm này',

  // FormAddSkill
  Strings.earnMoneySuggestion:
      'Bạn có kỹ năng, chúng tôi sẽ giúp bạn kiếm tiền.',

  // RegisterExpertStep1
  Strings.createYourSelfAIncredibleProfileSuggestion:
      'Tạo cho mình một profile chất lượng',

  // RegisterExpertStep3
  Strings.costOfASingleService: 'Giá một gói tư vấn của bạn',

  // RegisterExpertScreen

  Strings.nextStep: 'Tiếp tục',
  // ChooseAccountScreen

  Strings.welcomeToAskAny: "Chào mừng đến với Askany!",
  Strings.tellUsWhatAreYouLookingFor:
      'Hãy cho chúng tôi biết bạn đang tìm kiếm điều gì?',
  Strings.iNeedSomeAdvisse: 'Tôi cần tư vấn',
  Strings.imAnExpert: 'Tôi là chuyên gia',

  // ForgotPasswordScreen
  Strings.forgotPasswordAppbartext: 'Quên mật khẩu',
  Strings.enterEmailToGetLinkSuggestion:
      'Vui lòng nhập Email bạn đã đăng ký với chúng tôi, chúng tôi sẽ gửi link thiết lập lại mật khẩu cho bạn.',
  Strings.emailCannotBeLeftBlankErrortext: 'Không được để trống Email!',

  // LoginScreen
  Strings.invalidEmailErrortext: 'Email không hợp lệ',
  Strings.passwordMustBeAtLeast6CharactersErrortext: 'Mật khẩu ít nhất 6 kí tự',
  Strings.passwordHintext: 'Mật khẩu',
  Strings.login: 'Đăng nhập',
  Strings.keepMeLoggedIn: 'Nhớ tài khoản',
  Strings.newUser: 'Bạn chưa có tài khoản?',
  Strings.signUp: 'Đăng ký',

  // RegisterScreen

  Strings.invalidEmailErrortext: 'Số điện thoại không hợp lệ!',
  Strings.fullnameMustBeAtLeast3CharactersErrortext: 'Họ Tên ít nhất 3 kí tự!',
  Strings.addressCannotBeLeftBlankErrortext: 'Hãy chọn nơi ở của bạn',
  Strings.alreadyHaveAnAccount: 'Bạn đã có tài khoản?',

  // RegisterSocialScreen
  Strings.nameCannotBeLeftBlankErrortext: 'Không được để trống tên!',
  Strings.passwordMustBeAtLeast6CharactersErrortext:
      'Không được để trống mật khẩu!',
  Strings.phoneNumberCannotBeLeftBlankErrortext:
      'Không được để trống số điện thoại!',
  Strings.pressSignUpMean: 'Đăng ký là bạn đồng ý với ',
  Strings.rules: 'Điều khoản',
  Strings.ofUs: ' của chúng tôi',

  // ExperienceCard

  // LanguageBottomSheet
  Strings.clearAll: 'Xoá hết',
  Strings.ok: 'Đồng ý',

  // RegisterExpertHeader

  Strings.createProfile: 'Tạo profile',
  Strings.skillsOfYou: 'Kỹ năng bạn có',
  Strings.step: 'Bước',

  // SpecialtyBottomSheet

  Strings.sectorOfRequestAppbartext: 'Lĩnh vực yêu cầu',
  //AuthFooter
  Strings.or: 'Hoặc',

  // ConSultServiceInfoScreen

  // Strings.overviewOfThisServiceAppbartext :'Tổng quan về gói tư vấn này',
  // Strings.totalNumberOfParticipants :  'Số người tham gia: '

  // CalendarScreen

  Strings.seeByMonthOrWeekSuggestion: "Chọn để xem lịch theo tháng hoặc tuần",
  Strings.swipeLeftorRightToSeeOtherMonthSuggestion:
      "Lướt lịch sang trái hoặc phải để xem những tháng khác",

  //CalendarHeader

  Strings.monthlyCalendar: 'Lịch tháng',
  Strings.weeklyCalendar: 'Lịch tuần',
  Strings.welcomeBack: 'Chào mừng trở lại, ',

  // TimelineMonthEmpty
  Strings.dontHaveAnyConsultationBooked: 'Bạn chưa có lịch tư vấn nào!',
  Strings.findExpertSuggestion:
      'Tìm một chuyên gia\ngiải quyết\nvấn đề cho bạn!',

  // CategoryScreen

  Strings.categoryAppbartext: 'Danh mục',
  Strings.findAnExpertInEverySectorsWhoCanResolveYourProblems:
      'Tìm kiếm ngay những chuyên gia ở tất cả các lĩnh vực có thể giải quyết vấn đề của bạn!',
  // DetailsCategoryScreen
  Strings.unfortunatelyNoExpertInThisSectorSuggestion:
      'Rất tiếc lĩnh vực này chưa có chuyên \ngia nào.',
  Strings.noExpertInThisSectorSuggestion:
      'Chưa có chuyên gia nào trong lĩnh vực này!',

  // DetailsExpertScreen
  Strings.sendMessages: 'Gửi tin nhắn',
  Strings.contactNow: 'Liên hệ ngay',
  Strings.infomationTabbartext: 'Thông tin',
  Strings.reviewTabbartext: 'Đánh giá',
  Strings.minimum: 'Tối thiểu',
  Strings.block: 'Chặn',
  Strings.afterblockedSuggestion:
      'Sau khi chặn, bạn sẽ không thể tìm thấy người dùng này trên Askany nữa.',
  Strings.blockUser: 'Chặn người dùng',
  Strings.currentPosition: 'Chức vụ hiện tại',
  Strings.currentCompany: 'Công ty hiện tại',
  Strings.otherExperts: 'Chuyên gia khác',
  Strings.noneRecommendationsSuggestion: 'Không có đề xuất nào phù hợp!',

  // FilterExpertScreen

  Strings.filter: 'Lọc',
  Strings.soft: 'Sắp xếp',
  Strings.latest: 'Mới nhất',
  Strings.mostPopular: 'Phổ biến nhất',
  Strings.rating: 'Đánh giá',
  Strings.price: 'Giá tiền',
  Strings.contactForm: 'Hình thức liên hệ',
  Strings.accoutType: 'Loại tài khoản',
  Strings.all: 'Tất cả',
  Strings.verifiedAccoutFilter: 'Tài khoản đã xác nhận',
  Strings.noReviews: 'Chưa có đánh giá',

  // ChatScreen
  Strings.yourInboxIsEmptyNoti: 'Bạn chưa có tin nhắn nào',
  Strings.delete: 'Xóa',

  // BottomChatOptions
  Strings.editName: 'Chỉnh sửa tên',
  Strings.markAsRead: 'Đánh dấu đã đọc',
  Strings.markAsUnread: 'Đánh dấu chưa đọc',
  Strings.muteNotification: 'Tắt thông báo',
  Strings.unmuteNotification: Strings.unmuteNotification,
  Strings.deleteChat: 'Xoá trò chuyện',
  Strings.editNameOfChat: 'Chỉnh sửa tên cuộc trò chuyện',
  Strings.areYouSureToDeleteThisChat:
      'Bạn có chắc chắn muốn xoá\ncuộc trò chuyện này?',

  // CallingBar

  Strings.tapToReturnTheCall: "Nhấn để quay lại cuộc gọi",

  // CustomImagePicker

  Strings.uploadANewPhoto: 'Tải ảnh mới',

  // DialogInput

  Strings.enterChatName: 'Nhập tên cho cuộc trò chuyện...',
  Strings.chatNameMustBeAtLeast3CharactersErrortext:
      "Không được chỉnh sửa dưới 3 kí tự",

  // TitleAndSeeMore
  Strings.viewAll: 'Xem tất cả',

  // ConversationScreen
  Strings.copy: 'Sao chép',
  Strings.areYourSureToDeleteThisMessage:
      'Bạn có chắc chắn xoá tin nhắn này không?',

  // AppointmentChatCard
  Strings.servicePackage: 'Gói dịch vụ',
  Strings.hasBeenCanceled: 'đã bị hủy',
  Strings.hasOrderedTheServicePackageNamed: 'đã đặt gói dịch vụ',
  Strings.thisServiceHasBennCanceledBecauseOfExpertIsBusyAtThatTime:
      'Gói dịch vụ bị hủy do Chuyên gia đã bận vào thời gian này.',
  Strings.time: 'Thời gian',
  Strings.viewAllDetails: 'Xem chi tiết',

  // ConversationHeaderSelectedMessage
  Strings.selectedMessage: 'thư đã chọn',
  Strings.selectedMessages: 'thư đã chọn',
  Strings.thisChatOnAskAny: 'Cuộc trò chuyện trên Askany',

  // InputMessage
  Strings.yourMessageHintext: 'Soạn tin nhắn...',

  // MessageCallCard

  Strings.thisCallIsEnd: 'Cuộc gọi này đã kết thúc',
  Strings.youMissedThisCall: 'Bạn đã bỏ lỡ cuộc gọi',
  Strings.atTime: 'lúc',

  // MessageCallCard

  Strings.sending: 'Đang gửi tin...',
  Strings.failedRetry: 'Thất bại, hãy thử lại!',
  // HomeScreen

  Strings.discover: 'Khám phá',
  Strings.expertCommunity: 'Cộng đồng chuyên gia tư vấn',
  Strings.monthExpert: 'Chuyên gia hot tháng',
  Strings.seeMore: 'Xem thêm',
  Strings.popularServices: 'Dịch vụ tư vấn phổ biến',
  Strings.plentifulAndProfessionalExpertCommunity:
      'Đội ngũ chuyên gia \nphong phú và chuyên nghiệp',
  Strings.solutionForAllSector: 'Giải pháp cho mọi lĩnh vực',
  Strings.findSomething: 'Tìm kiếm',

  // CancelDescription

  Strings.customer: 'Khách hàng',
  Strings.thisServiceHasBeenCanceled: 'Gói dịch vụ này đã bị hủy',
  Strings.cancelBy: 'Hủy bởi',
  Strings.cancelReason: 'Lý do huỷ',
  Strings.refundCompletedNoti:
      'Chúng tôi đã hoàn tất thao tác hoàn tiền cho bạn vào ví Ask Any.',
  Strings.refundIn24HoursNoti:
      'Gói dịch vụ này đang được xử lý hoàn tiền. Chúng tôi sẽ hoàn tiền cho bạn chậm nhất trong vòng 24h làm việc.',
  Strings.refundDetails: 'Chi tiết hoàn trả',
  Strings.refundAt: 'Hoàn tiền tới',
  Strings.askAnyWallet: 'Ví Ask Any',
  Strings.refundAmount: 'Số tiền hoàn lại',
  Strings.repaidTime: 'Thời gian hoàn',

  // CancelServiceScreen

  Strings.selectAReason: 'Chọn lý do',
  Strings.cancelThisServiceAppbartext: 'Huỷ gói dịch vụ',
  Strings.pleaseTellUsTheReasonWhyYouCancelThisServicePart1:
      'Vui lòng cho chúng tôi biết lý do vì sao bạn muốn hủy gói dịch vụ tư vấn',
  Strings.pleaseTellUsTheReasonWhyYouCancelThisServicePart2: 'không?',
  Strings.reason: 'Lý do',
  Strings.reasonCannotBeLeftBlankErrortext: 'Hãy chọn lí do huỷ dịch vụ',
  Strings.details: 'Chi tiết',
  Strings.contentHintext: 'Nội dung',

  // ContinuesPaymentScreen
  Strings.orderingService: 'Đặt gói dịch vụ',
  Strings.enteringInformation: 'Nhập thông tin',
  Strings.selectingTime: 'Chọn thời gian',
  Strings.payment: 'Thanh toán',
  Strings.totalPrice: 'Tổng cộng',
  Strings.youHaveNotSelectedAnyTimeErrortext:
      'Bạn chưa chọn thời gian hẹn, vui lòng chọn thời gian hẹn để đến bước tiếp theo',

  // PaymentInfoServiceCard
  Strings.paymentInformation: 'Thông tin thanh toán',
  Strings.serviceType: 'Loại gói',
  Strings.participants: 'Số người tham gia',
  Strings.people: 'người',
  Strings.costFor: 'Giá',
  Strings.adviseDuration: 'Thời lượng tư vấn',
  Strings.extraFee: 'Phí dịch vụ cộng thêm',

  // ReplyFeedbackScreen
  Strings.replyFeedbackAppbartext: 'Trả lời đánh giá',
  Strings.replyFeedbackOf: 'Trả lời đánh giá của khách hàng',

  // ReportCompletedView

  Strings.complain: 'Khiếu nại',
  Strings.complaint: 'Khiếu nại',
  Strings.complainReason: 'Lý do khiếu nại',
  Strings.thisComplainHasBeenClosed: 'Khiếu nại này đã đóng',

  Strings.complaintIsBeingVerify:
      'Khiếu nại này đang được chúng tôi xác minh và xử lý. Thời gian xử lý khiếu nại tối đa là 7 ngày làm việc, vui lòng kiểm tra trong chi tiết dịch vụ để cập nhật thêm.',

  // ServiceComplaintScreen

  Strings.serviceComplaining: 'Khiếu nại gói dịch vụ',
  Strings.pleaseTellUsTheReasonWhyYouComplainPart1:
      'Vui lòng cho chúng tôi biết lý do vì sao bạn muốn khiếu nại gói dịch vụ tư vấn',
  Strings.pleaseTellUsTheReasonWhyYouComplainPart2: 'không?',

  // ServiceDescriptionCard

  Strings.service: 'Dịch vụ',
  Strings.contacter: 'Người liên hệ',

  // ServiceDetailsScreen

  Strings.buyingService: 'Mua gói',
  Strings.confirmingService: 'Chờ xác nhận',
  Strings.advising: 'Tư vấn',
  Strings.complete: 'Hoàn thành',
  Strings.cancelService: 'Huỷ gói dịch vụ',
  Strings.serviceDetailsAppbartext: "Chi tiết dịch vụ",
  //ServiceFeedbackScreen

  Strings.serviceReview: 'Đánh giá dịch vụ',
  Strings.areYouSatisfiedWith: 'Bạn có hài lòng với dịch vụ tư vấn của gói',
  Strings.areYouSatisfiedWithPart2: '',
  Strings.postReview: 'Gửi đánh giá',

  // ContinuesPaymentStepOne
  Strings.yourContactInformation: 'Thông tin liên hệ của bạn',
  Strings.note: 'Lời nhắn',
  Strings.noteHintext: 'Nhập lời nhắn',
  Strings.intoPrice: 'Thành tiền',
  Strings.choosePaymentMethod: 'Chọn phương thức thanh toán',
  Strings.paymentbyVNPay: 'Thanh toán bằng VNPay',
  Strings.paymentByAskAnyWallet: 'Thanh Toán bằng ví Ask Any',
  Strings.walletBalance: 'Số dư còn lại trong ví',
  Strings.notEnoughBalanceNoti:
      'Tài khoản ví không đủ số dư. Vui lòng nạp thêm tiền để tiến hành thanh toán.',
  Strings.morning: 'Sáng',
  Strings.afternoon: 'Chiều',
  Strings.invalidTimeErrortext:
      'Thời gian bắt đầu không hợp lệ, vui lòng chọn lại thời gian',
  Strings.chooseAdviseDate: 'Chọn ngày tư vấn',
  Strings.chooseAdviceTime: 'Chọn thời gian bắt đầu tư vấn',

  // ServiceDetailsBottomButton
  Strings.youConfirmAdvisingTheServiceP1: 'Bạn xác nhận tư vấn gói dịch vụ',
  Strings.youConfirmAdvisingTheServiceP2: '?',
  Strings.youComfirmCompletion: 'Bạn xác nhận hoàn thành gói dịch vụ này?',

  // ServiceFeedbackView
  Strings.thereNoReviewYet: 'Chưa có đánh giá',
  Strings.advised: 'Đã tư vấn',
  Strings.canceled: 'Hủy',
  Strings.complained: 'Khiếu nại',

  // NotificationScreen

  Strings.markAllAsReadNoti: 'Đánh dấu đã đọc tất cả thông báo?',
  Strings.readAll: 'Đọc tất cả',
  Strings.youDontHaveAnyNoti: 'Bạn chưa có thông báo nào',

  // CreateRequestScreen
  // CreateRequestScreen
  Strings.createNewRequest: 'Tạo yêu cầu dịch vụ mới',
  Strings.editRequestDetails: 'Chỉnh sửa yêu cầu dịch vụ',
  Strings.myRequest: 'Yêu cầu tư vấn của bạn',
  Strings.requestTitleHintext: 'Bạn muốn được tư vấn gì?',
  Strings.requestTitleCannotBeLeftBlankErrortext:
      'Bạn chưa nhập yêu cầu tư vấn',
  Strings.requestContent: 'Nội dung yêu cầu',
  Strings.requestContentHintext: 'Nội dung cần được tư vấn',
  Strings.requestContentCannotBeLeftBlankErrortext:
      'Bạn chưa nhập nội dung cần được tư vấn',
  Strings.budget: 'Ngân sách',
  Strings.availableBudget: 'Ngân sách có thể chi trả',
  Strings.budgetCannotBeLeftBlankErrortext: 'Bạn chưa nhập giá ngân sách',
  Strings.participantHintext: 'Số lượng người',
  Strings.participantsCannotBeLeftBlankErrortext:
      'Bạn chưa nhập số lượng người',
  Strings.bidingClosingDate: 'Ngày kết thúc chào giá',
  Strings.placeCannotBeLeftBlankErrortext: 'Bạn chưa nhập tên địa điểm',
  Strings.placeNameHintext: 'Địa chỉ',
  Strings.placeNameCannotBeLeftBlankErrortext: 'Bạn chưa nhập địa chỉ',
  Strings.postRequest: 'Đăng yêu cầu',
  Strings.priceHintext: 'Giá',

  // DetailsRequestScreen
  Strings.noBidsYet: 'Hiện chưa có lượt chào giá nào!',
  Strings.bidingEnd: 'Chào giá kết thúc',
  Strings.congratulationYouAreTheChosenOne: 'chúc mừng bạn là người được chọn!',
  Strings.unfortunalyYouAreNotTheChosen: 'rất tiếc bạn chưa được chọn!',
  Strings.selectedExpert: 'Chuyên gia được chọn tư vấn',
  Strings.bidExperts: 'Chuyên gia đã chào giá',
  Strings.yourBids: 'Chào giá của bạn',
  Strings.offer: 'Chào giá',
  Strings.cancelOffer: 'Hủy chào giá',
  Strings.areYouSureToCancelP1: 'Bạn xác nhận muốn hủy chào giá gói',
  Strings.areYouSureToCancelP2: '?',
  Strings.customerInformation: 'Thông tin khách hàng',

  // FilterRequestScreen
  Strings.searchRequest: 'Tìm kiếm yêu cầu',
  Strings.searchHintext: 'Tìm kiếm',
  Strings.currency: 'đ',

  // MyRequestScreen

  Strings.beingInprogress: 'Đang diễn ra',
  Strings.closed: 'Đã kết thúc',

  // OrderServiceScreen

  // PaymentResultScreen
  Strings.paymentResult: 'Kết quả giao dịch',
  Strings.paymentSuccess: 'Thanh toán thành công.',
  Strings.paymentFailure: 'Thanh toán thất bại.',
  Strings.paymentSuccessNoti:
      'Bạn đã thanh toán gói dịch vụ thành công. Vui lòng vào Lịch của bạn để xem chi tiết.',
  Strings.paymentFailureNoti:
      'Thanh toán gói dịch vụ thất bại, bạn hãy vui long lòng thử lại sau.',

  // RequestServiceScreen

  Strings.requestSearchingHintext: 'Tìm kiếm danh mục',

  // CreateOffer
  Strings.offerForThisRequest: 'Chào giá cho Yêu cầu tư vấn này ngay!',
  Strings.yourOfferForThisRequest: 'Chào giá của bạn cho Yêu cầu tư vấn này',
  Strings.timeHintext: 'VD: 2',
  Strings.adviseDurationMustBeDivisibleBy15Errortext:
      'Thời lượng buổi tư vấn phải là bội số của 15!',
  Strings.adviseDurationCannotBeLeftBlankErrortext:
      'Bạn chưa nhập thời lượng tư vấn',
  Strings.hoursUperCase: 'Giờ',
  Strings.hourUperCase: 'Giờ',

  Strings.minutesUperCase: 'Phút',
  Strings.minuteUperCase: 'Phút',

  Strings.convinceCustomers:
      'Thuyết phục khách hàng về những gì bạn có thể làm được',
  Strings.postOffer: 'Gửi chào giá',

  // OfferRequestCard

  Strings.offerContent: 'Nội dung chào giá',
  Strings.offerCost: 'Báo giá',
  Strings.showMore: 'Xem thêm',
  Strings.showLess: 'Rút gọn',
  Strings.chooseExpert: 'Chọn chuyên gia tư vấn',
  Strings.yourDecidedToChoose: 'Bạn chắc chắn lựa chọn chuyên gia',
  Strings.forThisRequest: 'thực hiện tư vấn cho yêu cầu này?',
  Strings.selected: 'Được chọn',
  Strings.select: 'Lựa chọn',
  Strings.fullName: 'Họ tên',

  // RequestCard
  Strings.expirationTime: 'Hết hạn',
  Strings.noOffersNoti: 'Chưa có lượt chào giá nào',
  Strings.offers: 'lượt chào giá',
  Strings.bidding: 'Đang chào giá',
  Strings.bidSuccess: 'Chào giá thành công',
  Strings.bid: 'Đã chào giá',
  Strings.noExpertSelected: 'Chưa chọn chuyên gia',
  Strings.selectedAnExpert: 'Đã chọn chuyên giaß',

  // RequestDetailsCard
  Strings.requestHasBidNoti:
      'Yêu cầu này đã có người tham gia chào giá và không thể chỉnh sửa được nữa, bạn có muốn xóa yêu cầu này không?',
  Strings.deepInfor: 'Thông tin chi tiết',
  Strings.thereAre: 'Có',
  Strings.thereIs: 'Có',
  Strings.expirationDate: 'Ngày kết thúc chào giá',
  Strings.endRequest: "Kết thúc yêu cầu",
  Strings.youDecidedToEndRequest:
      "Bạn xác nhận muốn kết thúc yêu cầu này trước thời gian đã chọn?",
  Strings.makeACall: 'Gọi điện với chuyên gia!',
  Strings.makeAAppointment: 'Hẹn gặp chuyên gia!',
  Strings.notUpdated: 'Chưa cập nhật',
  Strings.requestsAppbartext: 'Yêu cầu dịch vụ',
  Strings.bidRequests: 'Yêu cầu đã chào giá',
  Strings.myRequests: 'Yêu cầu của tôi',
  Strings.pressToCreateNewRequest: "Chọn để tạo yêu cầu dịch vụ mới",

  // BottomSheetContactExpert
  Strings.youCannotMakeAAppointmentNoti:
      'Bạn không thể đặt lịch hẹn gặp mặt trực tiếp, chuyên gia này chỉ có thể đặt lịch gọi điện.',
  Strings.youCannotMakeACallNoti:
      'Bạn không thể đặt lịch gọi điện, chuyên gia này chỉ có thể đặt lịch gặp mặt.',
  Strings.reviewsCount: 'lượt đánh giá',
  Strings.reviewCount: 'lượt đánh giá',

  // DetailsSkillSearchScreen
  Strings.otherServices: 'Dịch vụ khác',
  Strings.thereNoRecommendServicesNoti: 'Chưa có dịch vụ đề xuất!',
  // NoResultScreen
  Strings.thereNoResultNoti: 'Không có kết quả nào được tìm thấy.',
  Strings.youWantToFindSomeAdviseForMoreSpecialProblem:
      'Bạn đang cần tư vấn một vấn đề đặc biệt hơn?\n',
  Strings.let: 'Hãy',
  Strings.now: 'ngay.',

  // ResultScreen
  Strings.readyServicesCount: 'dịch vụ đang sẵn sàng',
  Strings.readyServiceCount: 'dịch vụ đang sẵn sàng',

  // BottomSheetInfoExpert
  Strings.anExpertIn: 'Chuyên viên',

  // NearbySearch
  Strings.recentSearching: 'Tìm kiếm gần đây',
  Strings.whatAreYouLookingFor: 'Bạn cần tư vấn gì?',
  Strings.trending: 'Tìm kiếm nhiều nhất',

  // Wallet style

  Strings.failure: 'Thất bại',
  Strings.waitingForProcessing: 'Chờ xử lý',
  Strings.success: 'Thành công',
  Strings.userReported: 'Người dùng tố cáo',
  Strings.userPayOutByVNPay: 'Người dùng thanh\ntoán bằng ví VNPAY',
  Strings.userPayOutByAskany: 'Người dùng thanh\ntoán bằng ví AKANY',
  Strings.adminTransferForExpert: 'Quản lí chuyển tiền\ncho chuyên gia',
  Strings.adminRefundForUser: 'Quản lí hoàn tiền\ncho người dùng',
  Strings.userPayInByVNPay: 'Người dùng nạp tiền\nASKANY bằng VNPAY',
  Strings.expertPayInByVNPay: 'Chuyên gia nạp tiền\nASKANY bằng VNPAY',
  Strings.userWithDrawToBank: 'Người dùng rút tiền\nvề ngân hàng',
  Strings.expertWithDrawToBank: 'Chuyên gia rút tiền\nvề ngân hàng',

  // ChatInCallScreen
  Strings.chatInCall: "Tin nhắn trong cuộc gọi",
  // IncomingCallScreen
  Strings.incomingCall: "Cuộc gọi đến",
  Strings.reject: 'Từ chối',
  Strings.answer: "Trả lời",

  // ParticipantScreen

  Strings.callParticipants: "Người tham gia",

  // TabMenuVertical
  Strings.startSharingNoti:
      'Ask Any sẽ bắt đầu chia sẻ mọi thứ hiển thị trên màn hình của bạn.',
  Strings.startSharing: 'BẮT ĐẦU CHIA SẺ',
  Strings.stopSharing: 'KẾT THÚC CHIA SẺ',
  Strings.shareMyScreen: 'Chia sẻ màn hình',

  Strings.stopSharingMyScreen: 'Dừng chia sẻ màn hình',
  Strings.raiseYourHand: 'Giơ tay',
  Strings.fullScreen: 'Toàn màn hình',
  Strings.record: 'Quay màn hình',

  // RechargeScreen

  Strings.listPrice[0]: "50.000",
  Strings.listPrice[1]: "100.000",
  Strings.listPrice[2]: "200.000",
  Strings.listPrice[3]: "500.000",
  Strings.listPrice[4]: "1.000.000",
  Strings.listPrice[5]: "2.000.000",
  Strings.payInAppbartext: 'Nạp tiền',
  Strings.amountToPayIn: "Số tiền cần nạp",
  Strings.amountCannotBeLeftBlankErrortext: 'Bạn chưa nhập số tiền',
  Strings.minimumAmountIs: 'Số tiền phải > 10.000đ',
  Strings.payInSucessNotiP1: 'Bạn đã nạp thành công',
  Strings.payInSucessNotiP2:
      'vào ví Askany, vui lòng vào lịch sử giao dịch để xem thêm chi tiết.',
  Strings.checkMyWallet: 'Kiểm tra ví tiền',

  // AddDetailCardPayment
  Strings.specialtiesTitle[0]: "Việt Nam",
  Strings.addCardP1: 'Thêm thẻ',
  Strings.addCardP2: '',
  Strings.editPaymentCard: 'Chỉnh sửa thẻ thanh toán',
  Strings.cardP1: 'Thẻ ',
  Strings.cardP2: '',
  Strings.accountName: "Tên chủ thẻ",
  Strings.accountNameHintext: "Tên chủ thẻ",
  Strings.accountNameCannotBeLeftBlankErrortext: 'Bạn chưa nhập tên chủ thẻ',
  Strings.invoiceAddress: "Địa chỉ hóa đơn",
  Strings.invoiceAddressHintext: 'Địa chỉ hóa đơn',
  Strings.invoiceAddressCannotBeLeftBlankErrortext:
      'Bạn chưa nhập địa chỉ hóa đơn',
  Strings.city: "Thành phố",
  Strings.cityHintext: 'Thành phố',
  Strings.cityCannotBeLeftBlankErrortext: 'Bạn chưa nhập tên thành phố',
  Strings.zipCode: "Mã bưu điện",
  Strings.zipCodeHintext: 'Mã bưu điện',
  Strings.zipCodeCannotBeLeftBlankErrortext: 'Bạn chưa nhập mã bưu điện',
  Strings.country: "Quốc gia",
  Strings.countryHintext: '',
  Strings.countryCannotBeLeftBlankErrortext: 'Bạn chưa chọn quốc gia',
  Strings.creditCardInformation: "Thông tin thẻ",
  Strings.creditCardNumber: "Số thẻ",
  Strings.creditCardNumberHintext: 'Nhập số thẻ',
  Strings.creditCardNumberCannotBeLeftBlankErrortext: 'Bạn chưa nhập mã số thẻ',
  Strings.creditCardExpirationDate: "Ngày hết hạn",
  Strings.cvv: 'CVV',
  Strings.expiretionDateCannotBeLeftBlankErrortext:
      'Bạn chưa nhập ngày hết hạn',
  Strings.cvvCannotBeLeftBlankErrortext: 'Bạn chưa nhập CVV',
  Strings.areYouSureToDeleteThisCreditCard:
      'Bạn xác nhận muốn xóa thẻ thanh toán này khỏi danh sách thẻ đã lưu?',
  Strings.deleteThisCreditCard: 'Xóa thẻ này',

  // AddInfoCardPayment

  Strings.addCreditInforAppbartext: 'Thêm thông tin thẻ thanh toán',
  Strings.international: 'Thẻ quốc tế',
  Strings.nationalATM: 'Thẻ ATM nội địa',

  // SaveCreditCard
  Strings.rememberCreditCardAppbartext: 'Lưu thẻ thanh toán',
  Strings.rememberCreditCardInforSuggestion:
      'Lưu thông tin thẻ thanh toán để có thể nạp và rút tiền nhanh chóng.',
  Strings.savedCards: 'Thẻ đã lưu',

  // TransactionDetails
  Strings.transactionDetailsAppbartext: 'Chi tiết giao dịch',
  Strings.transAmount: 'Số tiền',
  Strings.transFrom: 'Người chuyển',
  Strings.transTo: 'Người nhận',
  Strings.paymentMethod: 'Phương thức giao dịch',
  Strings.transFee: 'Phí giao dịch',

  // TransactionHistory
  Strings.transHistoryAppbartext: 'Lịch sử giao dịch',

  // WithdrawATMCard

  Strings.bankName: 'Tên ngân hàng',
  Strings.bankNameHinttext: 'Ngân hàng ACB',
  Strings.supportedType: 'Loại thẻ hỗ trợ',
  Strings.creditCard: 'Thẻ Credit Card',

  //WithdrawMoney
  Strings.payOutAppbartext: 'Rút tiền',
  Strings.payOutProcessingTimeNoti:
      'Lưu ý: Yêu cầu rút tiền sẽ được chúng tôi xử lý trong vòng 30 làm việc.',
  Strings.receiveMoneyMethod: 'Phương thức nhận tiền',
  Strings.atmNumber: 'Thẻ ngân hàng',
  Strings.bankAccount: 'Tài khoản ngân hàng',

  // WithDrawMoneyToTheAccount
  Strings.withdrawToBankAppbartext: 'Rút tiền về tài khoản ngân hàng',
  Strings.withdrawAmount: 'Số tiền cần rút',
  Strings.withdrawAmountCannotBeLeftblankErrortext:
      'Bạn chưa nhập số tiền của bạn',
  Strings.minimumWithdrawAmountIs2bucks: "Số tiền tối thiểu là 50.000đ",
  Strings.receivingAccountInfor: 'Thông tin tài khoản nhận tiền',
  Strings.bankBranchName: 'Tên chi nhánh',
  Strings.accountNumber: 'Số tài khoản',

  //WithdrawMoneyToTheCard
  Strings.withdrawToCard: 'Rút tiền về thẻ ngân hàng',
  Strings.cardType: 'Loại thẻ',
  Strings.cardTypeHintext: 'Chọn loại thẻ',
  Strings.cardTypeCannotBeLeftBlankErrortext: 'Bạn chưa chọn loại thẻ',

  // WithdrawOver10
  Strings.withdrawVerifyNoti:
      'Bạn cần xác minh một trong những giấy tờ sau để hoàn thành yêu cầu rút tiền: Thẻ ngân hàng, Hoá đơn (điện, nước). Bằng lái xe.',
  Strings.uploadPhoto: 'Tải ảnh lên',

  // WithdrawUnder10
  Strings.withdrawVerifyIDNoti:
      'Bạn cần xác minh Chứng minh nhân dân để hoàn thành yêu cầu rút tiền.',
  Strings.frontOfId: 'Mặt trước CMND',
  Strings.backsideOfId: 'Mặt sau CMND',
  Strings.edit: 'Sửa',

  // ConfirmationWithdraw
  Strings.withdrawConfirminationAppbartext: 'Xác nhận rút tiền',
  Strings.verifyByPersonalInfor: 'Xác minh thông tin cá nhân ',
  Strings.total: 'Tổng cộng',
  Strings.receivedMoneyFrom: 'Nhận tiền từ gói dịch vụ',
  Strings.recentTrans: 'Giao dịch gần nhất',
  // blocs
  Strings.connectionNoti:
      'Bạn không có kết nối mạng, hãy kiểm tra lại kết nối!',
  Strings.retry: 'Thử lại',
  Strings.registerNow: 'Đăng kí ngay',
  Strings.verifyEmailNoti:
      'Vui lòng xác thực email\ntrước khi tiến hành đăng nhập!',
  Strings.verifyEmailNoti2:
      ' và làm theo hướng dẫn để xác thực email. (Lưu ý kiểm tra mục Spam nếu không nhận được email xác thực)',
  Strings.pleaseAccess: 'Vui lòng truy cập ',
  Strings.loginFailed: 'Đăng nhập thất bại!',
  Strings.cannotDeleteChatNoti:
      'Không thể xóa cuộc hội thoại,\nvui lòng thử lại sau!',

  Strings.cannotSendMessgaeNoti: 'Không thể nhắn tin',
  Strings.cannotSendMessageReason:
      'Bạn không thể nhắn tin với chuyên gia này vì bạn chưa đặt lịch tư vấn với chuyên gia.',

  Strings.cannotDeleteExperienceNoti:
      'Bạn không thể xóa kinh nghiệm này\nvì hồ sơ của bạn phải có ít nhất\n1 kinh nghiệm.',

  Strings.userCancelServiceRequestNoti: 'Người dùng đã huỷ yêu cầu dịch vụ',
  Strings.youAreChosenForRequest:
      'Bạn đã được chọn làm người tư vấn cho yêu cầu',
  Strings.revokedMessageNoti: 'Tin nhắn đã thu hồi',
  Strings.cannotEditMessageNoti:
      'Tin nhắn gửi quá 2 phút không thể xoá hoặc chỉnh sửa!',

  Strings.errorOccurredNoti: 'Có lỗi xảy ra',
  Strings.errorOccurredTryAgain: 'Hệ thống đang có lỗi, hãy thử lại sau!',

  Strings.successfulBidNoti: 'Chào giá thành công!',
  Strings.successfulBidCongratulations: 'Chúc mừng bạn đã chào giá thành công',
  Strings.failedBid: 'Chào giá thất bại!',
  Strings.failedBidTryAgain: 'Vui lòng thử lại sau',
  Strings.cancelBidFailed: 'Hủy chào giá thất bại!',

  Strings.cancelServiceSuccessNoti: 'Hủy gói dịch vụ thành công.',
  Strings.refundIn24Hours:
      'Chúng tôi sẽ hoàn tiền cho bạn trong vòng 24h làm việc, vui lòng kiểm tra trong chi tiết dịch vụ để cập nhật thêm.',

  Strings.complainSuccessNoti: 'Bạn đã gửi\nthông tin khiếu nại thành công!',
  Strings.verifyComplaintIn7Days:
      'Chúng tôi sẽ xác minh và xử lý khiếu nại cả bạn trong vòng 7 ngày, vui lòng kiểm tra trong chi tiết dịch vụ để cập nhật thêm',

  Strings.unpaidServicesNoti: 'Bạn còn\ngói dịch vụ chưa thanh toán!',
  Strings.unpaidSercicesDetailsP1: 'Gói dịch vụ',
  Strings.unpaidSercicesDetailsP2:
      'vẫn chưa được thanh toán, hãy kiểm tra ngay nhé!',

  Strings.cannotDeteteThisSkillNoti:
      'Bạn không thể xóa kỹ năng này vì hồ sơ\ncủa bạn phải có ít nhất 1 kỹ năng.',

  Strings.withdrawSuccessNoti: 'Yêu cầu rút tiền thành công!',
  Strings.withdrawProcessIn30Days:
      'Chúng tôi sẽ xử lý yêu cầu của bạn trong vòng 30 ngày làm việc.',

  Strings.withdrawFailedNoti: 'Yêu cầu rút tiền thất bại!',

  Strings.notBookedYetNoti:
      'Không thể gọi cho người dùng này vì bạn và người dùng này chưa có lịch hẹn tư vấn.',
  Strings.doItNow: 'Đặt lịch ngay',

  // fix

  Strings.requestsOfme: 'Yêu cầu của tôi',
  Strings.requests: 'Yêu cầu dịch vụ',

  // authentication

  Strings.notRegistrationExpertNoti:
      'Bạn chưa đăng kí thông tin chuyên gia. Vui lòng đăng ký thông tin để bắt đầu trở thành chuyên gia trên Askany.',
  Strings.emailNotExistNoti:
      'Email không tồn tại, hãy kiểm tra lại email đăng nhập!',
  Strings.wrongPasswordNoti: 'Mật khẩu không chính xác, hãy thử lại!',

  Strings.unConfirmedAccount: 'Chưa xác thực email!',

  Strings.phoneExistedNoti:
      'Số điện thoại này đã được đăng kí, vui lòng sử dụng số điện thoại khác!',
  Strings.emailExistedNoti:
      'Email đã được đăng kí, vui lòng sử dụng email khác!',
  Strings.socialExistedNoti:
      'Tài khoản mạng xã hội này đã được đăng kí, vui lòng sử dụng tài khoản khác!',

  // helpers

  Strings.askAnyWantToAccessYourCamera: 'AskAny muốn truy cập Camera',
  Strings.allowAskAnyToAccessYourCamera:
      'Ứng dụng này sẽ cho phép bạn chụp ảnh, quay video, video call và hơn thế.',

  Strings.monthNames[0]: 'Tháng 1',
  Strings.monthNames[1]: 'Tháng 2',
  Strings.monthNames[2]: 'Tháng 3',
  Strings.monthNames[3]: 'Tháng 4',
  Strings.monthNames[4]: 'Tháng 5',
  Strings.monthNames[5]: 'Tháng 6',
  Strings.monthNames[6]: 'Tháng 7',
  Strings.monthNames[7]: 'Tháng 8',
  Strings.monthNames[8]: 'Tháng 9',
  Strings.monthNames[9]: 'Tháng 10',
  Strings.monthNames[10]: 'Tháng 11',
  Strings.monthNames[11]: 'Tháng 12',

  Strings.dayNames[0]: 'T2',
  Strings.dayNames[1]: 'T3',
  Strings.dayNames[2]: 'T4',
  Strings.dayNames[3]: 'T5',
  Strings.dayNames[4]: 'T6',
  Strings.dayNames[5]: 'T7',
  Strings.dayNames[6]: 'CN',

  Strings.invalid: 'Không hợp lệ',

  Strings.MY_MESSAGE_OPTIONS[0]: 'Sao chép',
  Strings.MY_MESSAGE_OPTIONS[1]: 'Sửa tin nhắn',
  Strings.MY_MESSAGE_OPTIONS[2]: 'Trả lời tin nhắn',
  Strings.MY_MESSAGE_OPTIONS[3]: 'Chọn tin nhắn',
  Strings.MY_MESSAGE_OPTIONS[4]: 'Xoá tin nhắn',

  Strings.FRIEND_MESSAGE_OPTIONS[0]: 'Sao chép',
  Strings.FRIEND_MESSAGE_OPTIONS[1]: 'Trả lời tin nhắn',
  Strings.FRIEND_MESSAGE_OPTIONS[2]: 'Chọn tin nhắn',
  Strings.FRIEND_MESSAGE_OPTIONS[3]: 'Báo xấu',

  Strings.CHAT_OPTIONS[0]: 'Chỉnh sửa tên',
  Strings.CHAT_OPTIONS[1]: 'Thông tin chuyên gia',
  Strings.CHAT_OPTIONS[2]: 'Đánh dấu',
  Strings.CHAT_OPTIONS[3]: 'thông báo',
  Strings.CHAT_OPTIONS[4]: 'Xoá trò chuyện',

  //connection dialog

  Strings.connectionFailed: 'Lỗi kết nối',
  Strings.connectAgain: 'Vui lòng thử lại sau!',

  // Download Dialog

  Strings.downloadImageFailed: 'Tải xuống thất bại',
  Strings.downloadImageSuccess: 'Đã lưu',

  // Fix Bugs April

  Strings.rateNow: 'Đánh giá',
  Strings.justSendPictures: 'vừa gửi hình ảnh.',
  Strings.descriptionCancelBookingExpert:
      'Bạn có chắc chắn muốn dừng đặt lịch chuyên gia này?',


      Strings.adviseDurationMustBeLessThan240m: 'Thời lượng tư vấn phải ít hơn 240 phút.',
      Strings.adviseDurationMustBeLessThan4h: 'Thời lương tư vấn phải ít hơn 4 giờ'
};
