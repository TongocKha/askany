import 'package:askany/src/configs/lang/localization.dart';

Map<String, String> english = {
  // Language
  Strings.vietnamese: 'Vietnamese',
  Strings.english: 'English',

  // Home
  Strings.home: 'Home',
  Strings.category: 'Category',
  Strings.calendar: 'Calendar',
  Strings.request: 'Request',
  Strings.message: 'Message',
  Strings.notification: 'Notification',
  Strings.account: 'Account',

  // Request
  Strings.serviceRequest: 'My service request',
  Strings.bidServiceRequest: 'Bid service requests',
  Strings.happening: 'Happening',
  Strings.finished: 'Finished',

  // Videocall
  Strings.sharingScreenNoti: 'You\'re sharing your screen',

  // Account Screen

  Strings.verifiedAccount: 'Verified',
  Strings.unVerifiedAccount: 'Unverified',
  Strings.myServiceManagementTitle: 'Service management',
  Strings.expertProfile: 'Expert\'s profile',
  Strings.personalInfor: 'Personal information',
  Strings.changePassword: 'Change password',
  Strings.verifyAccountAppbartext: 'Verify account',
  Strings.languages: 'Languages',

  Strings.mySkills: 'My Skills',
  Strings.bodyOfAddSkillDialog:
      'Create more skills to get priority showing on our Expert section!',
  Strings.addMoreSkillsWithPlus: '+ Add new skill',
  Strings.yearsOfExperience: ' years of experience',
  Strings.yearOfExperience: ' year of experience',
  Strings.createFirstSkillSugesstion: 'You have no skills, create them now!',
  Strings.createNewSkill: 'Create new skill',
  Strings.verifyEmailAppbarText: 'Verify your Email',
  Strings.verifyEmailSuggestion:
      'You still have not verified your email,\nplease verify now to receive attractive gifts\nfrom Askany.',

  // Account verification

  Strings.yourEmailHasBeenVerified: 'Your email has been verified.',
  Strings.pleaseOpenInboxOf: 'Please open inbox of',
  Strings.openInboxSuggestion:
      'and follow the instructions to authenticate the email. (Note to check the Spam section if you do not receive the verification email)',
  Strings.verifyNow: 'Verify now',
  Strings.changeYourEmail: 'Change your email',

  // AccountVerificationEmailSuccessScreen

  Strings.congratulationYouHasSuccesfullyVerifiedYourEmailPart1:
      'Congratulation',
  Strings.congratulationYouHasSuccesfullyVerifiedYourEmailPart2:
      'You have successfully verified your email',
  Strings.nowYouCanExperiencingOurAmazingFeaturePart1:
      'Now you can start experiencing',
  Strings.nowYouCanExperiencingOurAmazingFeaturePart2: 'our amazing features.',
  Strings.backToHomePage: 'Back to home page',

  // AccountVerificationPhoneScreen
  Strings.verifyYourPhoneAppbarText: 'Verify your phone number',
  Strings.verifyPhoneSuggestion:
      'You still have not verified your phone,\nplease verify now to receive attractive gifts\nfrom Askany.',
  Strings.yourPhoneNumberHasBeenVerified:
      'Your phone number has been verified.',
  Strings.changeYourPhoneNumber: 'Change your phone number',

  // AccountVerificationScreen

  Strings.verifyYourCompanyAndPositionAppbartext:
      'Verify your company and your position',

  // AccountVertifyCompanyScreen
  Strings.processing: 'Processing',
  Strings.confirmed: 'Confirmed',
  Strings.rejected: 'Rejected',
  // reject or refuse
  Strings.yourCompanyAndPositionHaveBeenVerified:
      'Your company and position have been verified.',
  Strings.showEmploymentContractSuggestion:
      'You can leave your Linkedin account link or upload a photo of your employment contract so we can verify your Company and Position.',
  Strings.linkedinAccount: 'Linkedin account',
  Strings.linkedinLinkHintext: 'Your Linkedin link',
  Strings.atLeast3Characters: '',
  // 'Kí tự phải có ít nhất 3 ký tự' ???
  Strings.eitherUploadPhoto: 'Either upload photos to verify',
  Strings.notice: 'Notice!',
  Strings.privideEnoughInformationNotice:
      'Please provide enough information for us to verify.',
  Strings.verificationInformationSentSuccessfully:
      'Verification information sent successfully',
  Strings.checkWithin7Days:
      'We will check and process the authentication for you within 7 days.',

  // AddSkillScreen
  Strings.vnd: 'VND',
  Strings.usd: 'USD',
  Strings.addMoreAppbarText: 'Add more',
  Strings.editAppbarText: 'Edit',
  Strings.skillLowerKeyAppbarText: ' skills',
  Strings.selecrASectorSuggestionTitle:
      'Select a sector in which you have skill.',
  Strings.selectASectorHintext: 'Select a sector',
  Strings.haveNotSelectedAnySectorErrortext: 'You have not selected any sector',
  Strings.nameOfYourSkillTitle: 'Name of your skill',
  Strings.skillNameHintext: 'Skill\'s name',
  Strings.skillNameCannotBeLeftBlankErrortext:
      'Skill\'s name cannot be left blank!',
  Strings.yearsCountOfExprieceTitle: 'Years of experience',
  Strings.enterYearsCountOfExperienceHintext: 'Enter a number',
  Strings.yearsCountOfExperienceCannotBeBlankErrortext:
      'Years count of experience cannot be left blank!',
  Strings.whatYourHighestPositionTitle: 'What is your highest position?',
  Strings.selectYourHighestPositionHintext: 'Select your highest position',
  Strings.positionCannotBeLeftBlankErrortext: 'Please choose a position',
  Strings.whatYourCurrentCompanyTitle: 'What\'s your current company?',
  Strings.companyNameHintext: 'Enter your company\'s name',
  Strings.companyNameCannotBeLeftBlankErrortext:
      'Company\'s name cannot be left blank!',
  Strings.skillDescriptionTitle: 'Your sikll\'s description',
  Strings.describeYourSkillHintext: 'Describe your skill',
  Strings.skillDescribeCannotBeLeftBlankErrorText:
      'Skill\'s describe cannot be left blank!',
  Strings.costOfASingleCallingServiceTitle:
      'The cost of a single calling service (Per 15 minutes)',
  Strings.fifteenMinutes: '15 minutes',
  Strings.costCannotBeLeftblankErrortext: 'Cost cannot be left blank!',
  Strings.costOfASingleMeetingServiceTitle:
      'The cost of a single meeting service (Per 60 minutes)',
  Strings.sixtyMinutes: '60 minutes',
  Strings.participantsCountSuggetion:
      'Note: The pricing will be the same whether there are just two participants, if there are more than two participants, the service cost will be increased by 50%.',
  // Strings. schedulerForAPhoneCall : 'Scheduler for a phone call',
  Strings.call: 'Call',
  Strings.meet: 'Meet',
  Strings.serviceKeywordTitle: 'Keyword about your service (max 3 items)',
  Strings.serviceKeywordHintext: 'Keywords',
  Strings.serviceKeyWordBlankErrortext: 'Please enter at least one keyword!',
  Strings.addMoreSkillWithoutPlus: 'Add more skills',
  Strings.save: 'Save',
  Strings.clear: 'Clear',
  Strings.areYouSureToSlearThisSkill:
      'Make sure that you decided to clear this skill!',
  Strings.cancel: 'Cancel',
  Strings.deleteThisSkill: 'Delete this skill',

  // ChangeEmailScreen

  Strings.changeEmailAppbarText: 'Change email',
  Strings.enterYourEmailSugestion: 'Please enter your email address',
  Strings.emailHintext: 'Email',
  Strings.emailMustBeAtLeast3CharactersErrortext:
      'Email must be at least 3 characters',
  Strings.next: 'Next',

  // ChangePasswordScreen
  Strings.changePasswordAppbarText: 'Change password',
  Strings.currentPasswordTitle: 'Current password',
  Strings.yourPassWordMustBeAtLeast6CharactersErrortext:
      'Your password must be at least 6 characters!',
  Strings.newPasswordTitle: 'New password',
  Strings.confirmPasswordTitle: 'Confirm password',
  Strings.passwordDoesNotMatchErrortext: 'Confirm password does not match!',

  // ChangePhoneScreen

  Strings.changePhoneNumberAppbarText: 'Change phone number',
  Strings.enterPhoneNumberSuggestion: 'Please enter your phone number',
  Strings.phoneNumberHintext: 'Your phone number',
  Strings.phoneNumberMustBeAtLeast9CharactersErrortext:
      'Phone number must be at least 9 characters!',

  // EditProfileScreen

  Strings.editProfileAppbartext: 'Personal Infomation',
  Strings.update: 'Update',
  Strings.nameTitle: 'Name',
  Strings.nameMustBeAtLeast3CharactersErrortext:
      'Name must be at least 3 characters!',
  Strings.addressTitle: 'Address',
  Strings.address: 'Your address',
  Strings.phoneNumberTitle: 'Phone number',
  Strings.verifyYourPhoneNumber: 'Verify your phone number',
  Strings.logout: 'Log out',

  // ExperienceScreen
  Strings.exprienceAppbarText: 'Exprience',
  Strings.createExperienceSuggestion:
      'You have no experience, so create one right now!',
  Strings.createNewExprience: 'Create new exprience',
  Strings.createNewExprienceWithPlus: '+ Create new experience',

  // ExpertEditPriceScreen

  Strings.priceOfASingleServiceAppbartext: 'Price of a single service',
  Strings.enterPriceErrortext: 'Enter price!',
  Strings.availableTimeTitle: 'Available time',
  Strings.availableTimeSelectionNoti:
      "Note: You can add the same fixed time period for numerous days in a row.",
  Strings.availableTimeNotValid: 'Your available time is not valid',
  Strings.availavleTimeIsAllDayNoti:
      'You\'ve timed all of your days, please edit to fit your schedule!',
  Strings.addMoreAvailableDayWithPlus: '+ Add more available day',
  Strings.priceMustOverHaflBuckErorrtext: 'Minimum price is 0.5 \$',
  Strings.priceMustbeOver12k: 'Minimum price is 12.000 VND!',
  Strings.noTimeSelectedYet: 'No available time selected yet',
  Strings.noTimeSelectedYetNoti:
      'Please select valid time to complete this step!',

  // InfoExpertScreen

  Strings.expertInfomationAppbartext: 'Expert\'s information',
  Strings.howManyLanguages: 'How many languages are you fluent in?',
  Strings.blankLanguageErrortext: 'Languages cannot be left blank!',
  Strings.whatSectorYouHaveExprence: 'What sector do you have experience in?',
  Strings.sectorOfExpertise: 'Sector of Expertise',
  Strings.sector: 'Sector',
  Strings.sectorCannotBeLeftBlankErrortext: 'Sector cannot be left blank!',
  Strings.howManyYearsOfExprience: 'How many years of experiece do you have?',
  Strings.yearsCountOfExperienceHinttext: 'Years of experience',
  Strings.yearsCountofExperienceBlankErrortext:
      'Years count of your experience cannot be left blank!',
  Strings.yourCompanyYouWorkForTitle: 'Your company which your\'re working for',
  Strings.descriptionAboutYourExperienceAndSkills:
      'Description about your Experience and Skills',
  Strings.enterDescriptionAboutYouHintext: 'Enter some description about you',
  Strings.descriptionAboutYouCannotBeLeftBlankErrortext:
      'Description cannot be left blank!',

  // InputOTPScreen

  Strings.enterOTPsuggestion:
      'Please check your message and input the code in the following boxes.',
  Strings.verify: 'Verify',

  // ProfileExpertScreen

  Strings.viewInPage: 'View in page',
  Strings.reviews: 'reviews',
  Strings.reviewsAppbartext: 'Reviews',

  Strings.review: 'review',

  // ServiceManaGamentScreen

  Strings.youDontHavenAnyRequest: 'You have no service requests yet!',
  Strings.confirm: 'Confirm',

  // Components

  // BottomSheetPosition

  Strings.yourPosition: 'Your position in work',

  // ContainerManageAccount

  Strings.consultingSchedule: 'Calendar',
  Strings.myWallet: 'My wallet',

  // ManageServiceArea

  Strings.confirming: 'Confirming',
  Strings.advise: 'Advised',
  Strings.completed: 'Completed',

  // ServiceManaGamentCard
  Strings.areYouSureYouWouldLikeToConsultService:
      'Are you sure you would like to consult service',
  Strings.agree: 'Agree',
  Strings.expert: 'Expert',
  Strings.experts: 'Experts',
  Strings.callingPackage: 'Calling package',
  Strings.meetingPackage: 'Meeting package',
  Strings.place: 'Place',
  Strings.waitForPayment: 'Wait for payment',
  Strings.reportProcessing: 'Processing',

  // PeriodContainer

  Strings.monday: 'Monday',
  Strings.sunday: 'Sunday',

  Strings.month[0]: 'Jan',
  Strings.month[1]: 'Feb',
  Strings.month[2]: 'Mar',
  Strings.month[3]: 'Apr',
  Strings.month[4]: 'May',
  Strings.month[5]: 'Jun',
  Strings.month[6]: 'Jul',
  Strings.month[7]: 'Aug',
  Strings.month[8]: 'Sep',
  Strings.month[9]: 'Oct',
  Strings.month[10]: 'Nov',
  Strings.month[11]: 'Dec',

  Strings.repeatEveryWeek: 'Repeat every week',
  Strings.availableForCalling: 'Available for calling',
  Strings.availableForMeeting: 'Available for meeting',
  Strings.addMoreAvailableTimeWithPlus: '+ Add more available time',
  Strings.cannotAddThisAvailableTime: 'Unable to add time slot',
  Strings.pleaseSelecteAvailableDayFirst: 'Please select available day first!',

  // SkillCard

  Strings.minute: 'minute',
  Strings.minutes: 'minutes',

  // AddExperienceScreen

  Strings.add: 'Add',
  Strings.experienceInWork: 'experience',

  // FormAddExperience

  Strings.yourExperience: 'Your experience',
  Strings.company: 'Company',
  Strings.specialization: 'Specialization',
  Strings.specializationCannotBeLeftBlankErrortext:
      'Specialization cannot be left blank!',

  Strings.underStood: 'Clarified',
  Strings.send: 'Send',
  Strings.positionInWork: 'Your position in work',
  Strings.fromTime: 'From',
  Strings.monthCannotBeLeftBlankErrortext: 'Month cannot be left blank!',
  Strings.year: '',
  Strings.yearCannotBeLeftBlankErrortext: 'Year cannot be left blank!',
  Strings.toTime: 'To',
  Strings.detailsOfWork: 'Details of your work',
  Strings.detailsCannotBeLeftblankErrortext: 'Details cannot be left blank!',
  Strings.areYouSureToClearThisExperience:
      'Are you sure to clear this experience?',
  Strings.deleteThisExperience: 'Delete this experience',

  // FormAddSkill
  Strings.earnMoneySuggestion:
      'You have the skills, we\'ll help you to make money.',

  // RegisterExpertStep1

  Strings.createYourSelfAIncredibleProfileSuggestion:
      'Create yourself a incredible profile',

  // RegisterExpertStep3
  Strings.costOfASingleService: 'Cost of a single service',

  // RegisterExpertScreen
  Strings.nextStep: 'Next step',

  // ChooseAccountScreen
  Strings.welcomeToAskAny: 'Welcome to AskAny!',
  Strings.tellUsWhatAreYouLookingFor:
      'Let\'s tell us\nwhat are you looing for?',
  Strings.iNeedSomeAdvisse: 'I need some advise',
  Strings.imAnExpert: 'I\'m an Expert',
  // ForgotPasswordScreen

  Strings.forgotPasswordAppbartext: 'Forgot password',
  Strings.enterEmailToGetLinkSuggestion:
      'Please enter the the email you used to register with us, and we\'ll give you a link to reset your password.',
  Strings.emailCannotBeLeftBlankErrortext: 'Email cannot be left blank!',

  // LoginScreen

  Strings.invalidEmailErrortext: 'Invalid Email!',
  Strings.passwordMustBeAtLeast6CharactersErrortext:
      'Password must be at least 6 characters!',
  Strings.passwordHintext: 'Password',
  Strings.login: 'Login',
  Strings.keepMeLoggedIn: 'Keep me logged in',
  Strings.newUser: 'New user?',
  Strings.signUp: 'Sign up',

  // RegisterScreen
  Strings.invalidPhoneNumberErrortext: 'Invalid phone number!',
  Strings.fullnameMustBeAtLeast3CharactersErrortext:
      'Full name must be at least 3 characters!',
  Strings.addressCannotBeLeftBlankErrortext: 'Address cannot be left blank!',
  Strings.alreadyHaveAnAccount: 'Already have an account?',

  // RegisterSocialScreen
  Strings.nameCannotBeLeftBlankErrortext: 'Name cannot be left blank!',
  Strings.passwordCannotBeLeftBlankErrortext: 'Password cannot be left blank!',
  Strings.phoneNumberCannotBeLeftBlankErrortext: 'invalid phone number!',
  Strings.pressSignUpMean: 'Press "sign up" means you agree with ',
  Strings.rules: 'Rules',
  Strings.ofUs: ' of us',

  // ExperienceCard

  // LanguageBottomSheet
  Strings.clearAll: 'Clear all!',
  Strings.ok: 'OK',

  // RegisterExpertHeader

  Strings.createProfile: 'Create profile',
  Strings.skillsOfYou: 'Your skills',
  Strings.step: 'Step',

  // SpecialtyBottomSheet

  Strings.sectorOfRequestAppbartext: 'Sector of request',
  // title in contructor must be const!!!
  // AuthFooter
  Strings.or: 'or',
  // ConSultServiceInfoScreen

  // Strings. overviewOfThisServiceAppbartext :
  //     'Overview of this Service',
  // Strings. totalNumberOfParticipants :
  //     'Total number of participants: ',

  // CalendarScreen

  Strings.seeByMonthOrWeekSuggestion:
      "Choice between seeing the calendar by month and week.",
  Strings.swipeLeftorRightToSeeOtherMonthSuggestion:
      "Swipe the calendar left or right to see other months",

  // CalendarHeader

  Strings.monthlyCalendar: 'Monthly Calendar',
  Strings.weeklyCalendar: 'Weekly Calendar',
  Strings.welcomeBack: 'Welcome back, ',

  // TimelineMonthEmpty

  Strings.dontHaveAnyConsultationBooked: 'You have no consultation booked yet!',
  Strings.findExpertSuggestion: 'Find an Expert who can resolve your problems!',

  // CategoryScreen
  Strings.categoryAppbartext: 'Category',
  Strings.findAnExpertInEverySectorsWhoCanResolveYourProblems:
      'Find experts in every sector who can help you solve your problems!',
  // DetailsCategoryScreen
  Strings.unfortunatelyNoExpertInThisSectorSuggestion:
      'Unfortunately there are no experts in this sector yet.',
  Strings.noExpertInThisSectorSuggestion:
      'There are no experts in this sector yet!',
  // DetailsExpertScreen

  Strings.sendMessages: 'Send message',
  Strings.contactNow: 'Contact now',
  Strings.infomationTabbartext: 'Information',
  Strings.reviewTabbartext: 'Review',
  Strings.minimum: 'Minimum',
  Strings.block: 'Block',
  Strings.afterblockedSuggestion:
      'After blocked, you will be unable to locate this user on Askany.',
  Strings.blockUser: 'Block user',
  Strings.currentPosition: 'Current position',
  Strings.currentCompany: 'Currrent company',
  Strings.otherExperts: 'Other Experts',
  Strings.noneRecommendationsSuggestion:
      'None of the recommendations are compatible!',
  // FilterExpertScreen

  Strings.filter: 'Filter',
  Strings.soft: 'Soft',
  Strings.latest: 'Latest',
  Strings.mostPopular: 'Most popular',
  Strings.rating: 'Rating',
  Strings.price: 'Price',
  Strings.contactForm: 'Contact form',
  Strings.accoutType: 'Account\'s type',
  Strings.all: 'All',
  Strings.verifiedAccoutFilter: 'Verified account',
  Strings.noReviews: 'There\'re no reviews yet',

  // ChatScreen
  Strings.yourInboxIsEmptyNoti: 'Your inbox is empty!',
  Strings.delete: 'Delete',

  // BottomChatOptions
  Strings.editName: 'Edit name',
  Strings.markAsUnread: 'Mark as Unread',
  Strings.markAsRead: 'Mark as Read',
  Strings.muteNotification: 'Mute notification',
  Strings.unmuteNotification: 'Unmute notification',
  Strings.deleteChat: 'Delete chat',
  Strings.editNameOfChat: 'Edit name of Chat',
  Strings.areYouSureToDeleteThisChat:
      'Are you sure that\nyou want to delete this chat?',

  // CallingBar

  Strings.tapToReturnTheCall: 'Tap to return',

  // CustomImagePicker

  Strings.uploadANewPhoto: 'Upload a new photo',

  // DialogInput

  Strings.enterChatName: 'Enter Chat\'s name',
  Strings.chatNameMustBeAtLeast3CharactersErrortext:
      'Chat\s name must be at least 3 characters',
  // TitleAndSeeMore
  Strings.viewAll: 'View all',

  // ConversationScreen
  Strings.copy: 'Copy',
  Strings.areYourSureToDeleteThisMessage:
      'Are your sure to delete this message?',
  // AppointmentChatCard
  Strings.servicePackage: 'Service package',
  Strings.hasBeenCanceled: 'has been canceled',
  Strings.hasOrderedTheServicePackageNamed: 'has orderd the service named',
  Strings.thisServiceHasBennCanceledBecauseOfExpertIsBusyAtThatTime:
      'This service has been canceled because Expert is busy at that time!',
  Strings.time: 'Time',
  Strings.viewAllDetails: 'View all details',

  // ConversationHeaderSelectedMessage

  Strings.selectedMessage: 'selected message',
  Strings.selectedMessages: 'selected messages',
  Strings.thisChatOnAskAny: 'This chat on AskAny',

  // InputMessage
  Strings.yourMessageHintext: 'Your message',

  // MessageCallCard
  Strings.thisCallIsEnd: 'This call is end',
  Strings.youMissedThisCall: 'You missed this call',
  Strings.atTime: 'at',

  // MessageCallCard

  Strings.sending: 'Sending...',
  Strings.failedRetry: 'Failed! Retry',

  // HomeScreen
  Strings.discover: 'Discover',
  Strings.expertCommunity: 'Expert community',
  Strings.monthExpert: 'Month\'s Expert',
  Strings.seeMore: 'See more',
  Strings.popularServices: 'Popular services',
  Strings.plentifulAndProfessionalExpertCommunity:
      'Plentiful and professional\nExpert community',
  Strings.solutionForAllSector: 'Solution for every sectors',
  Strings.findSomething: 'Find something',

  // CancelDescription

  Strings.customer: 'Customer',
  Strings.thisServiceHasBeenCanceled: 'This service has been canceled!',
  Strings.cancelBy: 'Canceled by',
  Strings.cancelReason: 'Reason',
  Strings.refundCompletedNoti:
      'We have completed your refund to the Ask Any wallet.',
  Strings.refundIn24HoursNoti:
      'This service package is being in process of refund, We will repaid you within 24 working hours at the latest',
  Strings.refundDetails: 'Details',
  Strings.refundAt: 'Refund at',
  Strings.askAnyWallet: 'AskAny wallet',
  Strings.refundAmount: 'Refund amount',
  Strings.repaidTime: 'Repaid time',

  // CancelServiceScreen

  Strings.selectAReason: 'Seclect a reason',
  Strings.cancelThisServiceAppbartext: 'Cancel this service',
  Strings.pleaseTellUsTheReasonWhyYouCancelThisServicePart1:
      'Please tell us the reason why you cancel the ',
  Strings.pleaseTellUsTheReasonWhyYouCancelThisServicePart2: 'service?',
  Strings.reason: 'Reason',
  Strings.reasonCannotBeLeftBlankErrortext: 'Reason cannot be left blank!',
  Strings.details: 'Details',
  Strings.contentHintext: 'Content',

  // ContinuesPaymentScreen

  Strings.orderingService: 'Ordering Service',
  Strings.enteringInformation: 'Entering infor',
  Strings.selectingTime: 'Selecting time',
  Strings.payment: 'Payment',
  Strings.totalPrice: 'Total price',
  Strings.youHaveNotSelectedAnyTimeErrortext:
      'You have not selected any appointment time, please choose an appointment time to go to the next step',

  // PaymentInfoServiceCard

  Strings.paymentInformation: 'Payment details',
  Strings.serviceType: 'Service type',
  Strings.participants: 'Participants',
  Strings.people: 'people',
  Strings.costFor: 'Cost for',
  Strings.adviseDuration: 'Duration',
  Strings.extraFee: 'Extra fee',

  // ReplyFeedbackScreen

  Strings.replyFeedbackAppbartext: 'Reply feedback',
  Strings.replyFeedbackOf: 'Reply feedback of',

  // ReportCompletedView

  Strings.complain: 'Complain',
  Strings.complaint: 'Complaint',
  Strings.complainReason: 'Reason',
  Strings.thisComplainHasBeenClosed: 'This complain has been closed',
  Strings.rpClosed: 'Closed',
  Strings.complaintIsBeingVerify:
      'This complaint is being verified and handled by us. Maximum claim processing time is 7 working days, please check in service details for further updates.',

  // ServiceComplaintScreen

  Strings.serviceComplaining: 'Service complaining',
  Strings.pleaseTellUsTheReasonWhyYouComplainPart1:
      'Please tell us the reason why you want to complain the',
  Strings.pleaseTellUsTheReasonWhyYouComplainPart2: 'service?',

  // ServiceDescriptionCard
  Strings.service: 'Service',
  Strings.contacter: 'Customer',

  // ServiceDetailsScreen

  Strings.buyingService: 'Buying',
  Strings.confirmingService: 'Confirming',
  Strings.advising: 'Advising',
  Strings.complete: 'Complete',
  Strings.cancelService: 'Cancel service',
  Strings.serviceDetailsAppbartext: 'Service\'s details',
  // ServiceFeedbackScreen

  Strings.serviceReview: 'Service Reviews',
  Strings.areYouSatisfiedWith: 'Are your satisfied with',
  Strings.areYouSatisfiedWithPart2: ' service?',
  Strings.postReview: 'Post',

  // ContinuesPaymentSteps
  Strings.yourContactInformation: 'Your contact information',
  Strings.fullName: 'Name',
  Strings.note: 'Note',
  Strings.noteHintext: 'Enter note',
  Strings.intoPrice: 'Price',
  Strings.choosePaymentMethod: 'Chose a payment method',
  Strings.paymentbyVNPay: 'Payment by VNPay',
  Strings.paymentByAskAnyWallet: 'Payment by AskAny Wallet',
  Strings.walletBalance: 'Wallet balance',
  Strings.notEnoughBalanceNoti:
      'Your wallet account does not have enough balance. Please top up to proceed with the payment.',
  Strings.invalidTimeErrortext: 'Invalid time! Please choose again!',
  Strings.chooseAdviseDate: 'Chose advise date',
  Strings.chooseAdviceTime: 'Choose advice time',
  Strings.morning: 'Morning',
  Strings.afternoon: 'Afternoon',

  // ServiceDetailsBottomButton
  Strings.youConfirmAdvisingTheServiceP1: 'You confirm advising the ',
  Strings.youConfirmAdvisingTheServiceP2: 'service?',
  Strings.youComfirmCompletion: 'Do you confirm completion of this service?',

  // ServiceFeedbackView
  Strings.thereNoReviewYet: 'There \'re no reviews yet.',
  Strings.advised: 'Advised',
  Strings.canceled: 'Canceled',
  Strings.complained: 'Complained',

  // NotificationScreen

  Strings.markAllAsReadNoti: 'Mark all as read?',
  Strings.readAll: 'Read all',
  Strings.youDontHaveAnyNoti: 'You have no notifications',

  // CreateRequestScreen

  Strings.createNewRequest: 'Create new request',
  Strings.editRequestDetails: 'Edit request details',
  Strings.myRequest: 'My requests',
  Strings.requestTitleHintext: 'Enter request title',
  Strings.requestTitleCannotBeLeftBlankErrortext:
      'Request title cannot be left blank!',
  Strings.requestContent: 'Request content',
  Strings.requestContentHintext: 'Enter request content',
  Strings.requestContentCannotBeLeftBlankErrortext:
      'Request content cannot be left blank!',
  Strings.budget: 'Budget',
  Strings.availableBudget: 'Available budget',
  Strings.budgetCannotBeLeftBlankErrortext: 'Budget cannot be left blank!',
  Strings.participantHintext: 'Enter participants number',
  Strings.participantsCannotBeLeftBlankErrortext:
      'Participants cannot be left blank!',
  Strings.bidingClosingDate: 'Bidding closing date',
  Strings.placeCannotBeLeftBlankErrortext: 'Place cannot be left blank!',
  Strings.placeNameHintext: 'Address',
  Strings.placeNameCannotBeLeftBlankErrortext: 'Address cannot be left blank!',
  Strings.postRequest: 'Post request',
  Strings.priceHintext: 'Price',

  // DetailsRequestScreen
  Strings.noBidsYet: 'No bids yet!',
  Strings.bidingEnd: 'Biding end',
  Strings.congratulationYouAreTheChosenOne:
      'congratulations you are the chosen one!',
  Strings.unfortunalyYouAreNotTheChosen: 'unfortunaly you are not the chosen!',
  Strings.selectedExpert: 'Selected Expert',
  Strings.bidExperts: 'Bid Experts',
  Strings.yourBids: 'Your bids',
  Strings.offer: 'Offer',
  Strings.cancelOffer: 'Cancel offer',
  Strings.areYouSureToCancelP1: 'Are you sure to cancel',
  Strings.areYouSureToCancelP2: 'service?',
  Strings.customerInformation: 'Customer infomation',

  // FilterRequestScreen

  Strings.searchRequest: 'Search',
  Strings.searchHintext: 'Search',
  Strings.currency: '\$',

  // MyRequestScreen
  Strings.beingInprogress: 'Being in progress',
  Strings.closed: 'Closed',

  // OrderServiceScreen

  // PaymentResultScreen

  Strings.paymentResult: 'Payment result',
  Strings.paymentSuccess: 'Success',
  Strings.paymentFailure: 'Failure',
  Strings.paymentSuccessNoti:
      'You have successfully paid for the service package. Please go to "Calendar" for details.',
  Strings.paymentFailureNoti: 'Payment failed, please try again later.',

  // RequestServiceScreen

  Strings.requestSearchingHintext: 'Search',

  // CreateOffer

  Strings.offerForThisRequest: 'Offer for this request!',
  Strings.yourOfferForThisRequest: 'Your offer',
  Strings.timeHintext: 'E.x: 2',
  Strings.adviseDurationMustBeDivisibleBy15Errortext:
      'Advise duration must be divisible by 15!',
  Strings.adviseDurationCannotBeLeftBlankErrortext:
      'Advise duration cannot be left blank!',
  Strings.hoursUperCase: 'Hours',
  Strings.hourUperCase: 'Hour',

  Strings.minutesUperCase: 'Minutes',
  Strings.minuteUperCase: 'Minute',

  Strings.convinceCustomers: 'Convince customers of what you can do',
  Strings.postOffer: 'Post offer',

  // OfferRequestCard

  Strings.offerContent: 'Offer\'s content',
  Strings.offerCost: 'Offer',
  Strings.showMore: 'Show more',
  Strings.showLess: 'Show less',
  Strings.chooseExpert: 'Choose Expert',
  Strings.yourDecidedToChoose: 'You decided to choose',
  Strings.forThisRequest: 'for this request?',
  Strings.selected: 'Selected',

  Strings.select: 'Select',

  // RequestCard

  Strings.expirationTime: 'Expiration time',
  Strings.noOffersNoti: 'There\'re no offers!',
  Strings.offers: 'offers',
  Strings.bidding: 'Bidding',
  Strings.bidSuccess: 'Bid success',
  Strings.bid: 'Bid',
  Strings.noExpertSelected: 'No Expert selected yet',
  Strings.selectedAnExpert: 'An Expert has been selected',

  // RequestDetailsCard

  Strings.requestHasBidNoti:
      'This request already has a bid and can\'t be edited anymore, do you want to delete this request?',
  Strings.deepInfor: 'Details',
  Strings.thereAre: 'There\'re',
  Strings.thereIs: 'There\'s',
  Strings.expirationDate: 'Expiration date',
  Strings.endRequest: 'End request',
  Strings.youDecidedToEndRequest:
      'You decided to end this request before chosen time?',
  Strings.makeACall: 'Make a call',
  Strings.makeAAppointment: 'Make a Appointment',
  Strings.notUpdated: 'Not updated',
  Strings.requestsAppbartext: 'Requests',
  Strings.bidRequests: 'Bid requests',
  Strings.myRequests: 'My requests',
  Strings.pressToCreateNewRequest: 'Press to create a new request',

  // BottomSheetContactExpert

  Strings.youCannotMakeAAppointmentNoti:
      'You can\'t book a face-to-face appointment, this specialist can only book a phone call.',
  Strings.youCannotMakeACallNoti:
      'Bạn không thể đặt lịch gọi điện, chuyên gia này chỉ có thể đặt lịch gặp mặt.',
  Strings.reviewsCount: 'reviews',
  Strings.reviewCount: 'review',

  // DetailsSkillSearchScreen

  Strings.otherServices: 'Other services',
  Strings.thereNoRecommendServicesNoti: 'There\'s no recommend services!',

  // NoResultScreen
  Strings.thereNoResultNoti: 'There\'re no results.',
  Strings.youWantToFindSomeAdviseForMoreSpecialProblem:
      'If you\'re looking for some advises for a special problem,\n',
  Strings.let: 'let\'s ',
  Strings.now: ' now!',

  // ResultScreen

  Strings.readyServicesCount: 'ready services',
  Strings.readyServiceCount: 'ready service',

  // BottomSheetInfoExpert
  Strings.anExpertIn: 'An Expert in',

  // NearbySearch
  Strings.recentSearching: 'Recent searching history',

  Strings.whatAreYouLookingFor: 'What are you looking for?',

  Strings.trending: 'Trending',

  // Wallet style

  Strings.failure: 'Failure',
  Strings.waitingForProcessing: 'Waiting for Processing',
  Strings.success: 'Success',
  Strings.userReported: 'User reported',
  Strings.userPayOutByVNPay: 'User payment\n by VNPay',
  Strings.userPayOutByAskany: 'User payment\n by AskAny wallet',
  Strings.adminTransferForExpert: 'Admin transfer money\nto Expert',
  Strings.adminRefundForUser: 'Admin refund money\nto User',
  Strings.userPayInByVNPay: 'User pay in \n by VNPay',
  Strings.expertPayInByVNPay: 'Expert pay in \n by VNPay',
  Strings.userWithDrawToBank: 'User regain money to bank',
  Strings.expertWithDrawToBank: 'Expert regain money to bank',

  // ChatInCallScreen
  Strings.chatInCall: 'Chat in call',

  // IncomingCallScreen
  Strings.incomingCall: 'Incoming call',
  Strings.reject: 'Reject',
  Strings.answer: 'Answer',
  // ParticipantScreen
  Strings.callParticipants: 'Participants',

  // TabMenuVertical

  Strings.startSharingNoti:
      'Ask Any will start sharing anything displayed on your screen.',
  Strings.startSharing: 'START SHARING',
  Strings.shareMyScreen: 'Share my screen',
  Strings.stopSharing: 'Stop sharing',
  Strings.raiseYourHand: 'Raise your hand',
  Strings.stopSharingMyScreen: 'Stop sharing',
  Strings.fullScreen: 'Full screen',
  Strings.record: 'Record',

  // RechargeScreen

  Strings.listPrice[0]: '2',
  Strings.listPrice[1]: '5',

  Strings.listPrice[2]: '10',

  Strings.listPrice[3]: '20',

  Strings.listPrice[4]: '50',

  Strings.listPrice[5]: '100',

  Strings.payInAppbartext: 'Pay in',
  Strings.amountToPayIn: 'Amount to pay in',
  Strings.amountCannotBeLeftBlankErrortext: 'Amount cannot be left blank!',
  Strings.minimumAmountIs: 'Minimum amount is 2\$',
  Strings.payInSucessNotiP1: 'You have successfully deposited',
  Strings.payInSucessNotiP2:
      'into Askany wallet, please go to transaction history to see more details.',
  Strings.checkMyWallet: 'My Wallet',

  // AddDetailCardPayment

  Strings.specialtiesTitle[0]: 'Vietnam',
  Strings.addCardP1: 'Add',
  Strings.addCardP2: ' card',
  Strings.editPaymentCard: 'Edit payment card',
  Strings.cardP1: '',
  Strings.cardP2: ' card',
  Strings.accountName: 'Account name',
  Strings.accountNameHintext: 'Enter account name',
  Strings.accountNameCannotBeLeftBlankErrortext:
      'Account name cannot be left blank!',
  Strings.invoiceAddress: 'Invoice address',
  Strings.invoiceAddressHintext: 'Enter invoice address',
  Strings.invoiceAddressCannotBeLeftBlankErrortext:
      'Invoice address cannot be left blank!',
  Strings.city: 'City',
  Strings.cityHintext: 'Enter city',
  Strings.cityCannotBeLeftBlankErrortext: 'City cannot be left blank!',
  Strings.zipCode: 'ZIP code',
  Strings.zipCodeHintext: 'Enter ZIP code',
  Strings.zipCodeCannotBeLeftBlankErrortext: 'ZIP code cannot be left blank!',
  Strings.country: 'Country',
  Strings.countryHintext: 'Enter country name',
  Strings.countryCannotBeLeftBlankErrortext:
      'Country name cannot be left blank!',
  Strings.creditCardInformation: 'Credit card information',
  Strings.creditCardNumber: 'Card number',
  Strings.creditCardNumberHintext: 'Enter credit card number',
  Strings.creditCardNumberCannotBeLeftBlankErrortext:
      'Card number cannot be left blank!',
  Strings.creditCardExpirationDate: 'Expiration date',
  Strings.creditCardExpirationDateHintext: 'MM/YY',
  Strings.creditCardExpirationDateCannotBeLeftBlankErrortext:
      'Expiration date cannot be left blank!',
  Strings.cvv: 'CVV',
  Strings.expiretionDateCannotBeLeftBlankErrortext:
      'Expiration date cannot be left blank!',
  Strings.cvvCannotBeLeftBlankErrortext: 'CVV cannot be left blank!',
  Strings.deleteThisCreditCard: 'Delete this credit card',
  Strings.areYouSureToDeleteThisCreditCard:
      'Have you confirmed that you want to remove this payment card from the list of saved cards?',
  Strings.deleteThisCard: 'Delete this card',

  // AddInfoCardPayment

  Strings.addCreditInforAppbartext: 'Add credit infor',
  Strings.international: 'International',
  Strings.nationalATM: 'National ATM',

  // SaveCreditCard

  Strings.rememberCreditCardAppbartext: 'Remember Credit card',
  Strings.rememberCreditCardInforSuggestion:
      'Save payment card information for quick deposit and withdrawal.',
  Strings.savedCards: 'Saved cards',

  // TransactionDetails

  Strings.transactionDetailsAppbartext: 'Transaction Details',
  Strings.transAmount: 'Amount',
  Strings.transFrom: 'From',
  Strings.transTo: 'To',
  Strings.paymentMethod: 'Payment method',
  Strings.transFee: 'Transaction fee',

  // TransactionHistory
  Strings.transHistoryAppbartext: 'Transaction history',

  // WithdrawATMCard
  Strings.bankName: 'Bank\'s name',
  Strings.bankNameHinttext: 'ACB bank',
  Strings.supportedType: 'Supported types',
  Strings.creditCard: 'Credit Card',
  // WithdrawMoney
  Strings.payOutAppbartext: 'Pay out',
  Strings.payOutProcessingTimeNoti:
      'Note: Pay out requests will be processed by us within 30 working days.',
  Strings.receiveMoneyMethod: 'Via',
  Strings.atmNumber: 'ATM card\'s number',
  Strings.bankAccount: 'Bank account',
  // WithDrawMoneyToTheAccount

  Strings.withdrawToBankAppbartext: 'Pay out to bank account',
  Strings.withdrawAmount: 'Pay out amount',
  Strings.withdrawAmountCannotBeLeftblankErrortext:
      'Pay out amount cannot be left blank!',
  Strings.minimumWithdrawAmountIs2bucks: 'Minimum amount is 2\$',
  Strings.receivingAccountInfor: 'Receiving account information',
  Strings.bankBranchName: 'Bank branch\'s name',
  Strings.accountNumber: 'Account number',

  // WithdrawMoneyToTheCard
  Strings.withdrawToCard: 'Pay out to card',
  Strings.cardType: 'Card type',
  Strings.cardTypeHintext: 'Select card type',
  Strings.cardTypeCannotBeLeftBlankErrortext: 'Card type cannot be lefr blank!',
  // WithdrawOver10
  Strings.withdrawVerifyNoti:
      'You need to verify one of the following documents to complete the withdrawal request: Bank card, Bill (electricity, water). License.',
  Strings.uploadPhoto: 'Upload photos',

  // WithdrawUnder10

  Strings.withdrawVerifyIDNoti:
      'You need to verify your Identity Card to complete the withdrawal request.',
  Strings.frontOfId: 'Front of ID',
  Strings.backsideOfId: 'Backside of ID',
  Strings.edit: 'Edit',

  // ConfirmationWithdraw

  Strings.withdrawConfirminationAppbartext: 'Pay out confirmination',
  Strings.verifyByPersonalInfor: 'Verify by personal Information',
  Strings.total: 'Total',
  Strings.receivedMoneyFrom: 'Received money from',
  Strings.recentTrans: 'Recent transaction',

  // blocs

  Strings.connectionNoti:
      'You have no network connection, please check the connection again!',
  Strings.retry: 'Retry',
  Strings.registerNow: 'Register Now',
  Strings.verifyEmailNoti:
      'Please verify your email\before proceeding to login!',
  Strings.verifyEmailNoti2:
      ' and follow the instructions to authenticate the email. (Note to check the Spam folder if you do not receive the verification email)',
  Strings.pleaseAccess: 'Please access ',
  Strings.loginFailed: 'Login failed!',
  Strings.cannotDeleteChatNoti:
      'Can\'t delete the conversation,\nPlease try again later!',
  Strings.cannotSendMessgaeNoti: 'Cannot send message',
  Strings.cannotSendMessageReason:
      'You cannot message this expert because you have not scheduled a consultation with the expert.',
  Strings.cannotDeleteExperienceNoti:
      'You cannot delete this experience\nbecause your profile must have at least\n1 experience.',

  Strings.userCancelServiceRequestNoti: 'User canceled the service request',
  Strings.youAreChosenForRequest:
      'You have been selected as a consultant for the request',
  Strings.revokedMessageNoti: 'Message revoked',
  Strings.cannotEditMessageNoti:
      'Messages sent over 2 minutes cannot be deleted or edited!',

  Strings.errorOccurredNoti: 'An error occurred',
  Strings.errorOccurredTryAgain:
      'There is an error in the system, please try again later!',

  Strings.successfulBidNoti: 'Successful bid!',
  Strings.successfulBidCongratulations:
      'Congratulations on your successful bid',
  Strings.failedBid: 'Bid failed!',
  Strings.failedBidTryAgain: 'Please try again later',
  Strings.cancelBidFailed: 'Cancel bid failed!',

  Strings.cancelServiceSuccessNoti: 'Cancel service package successfully.',
  Strings.refundIn24Hours:
      'We will refund you within 24 working hours, please check in Service Details for more updates.',

  Strings.complainSuccessNoti: 'You have submitted your claim successfully!',
  Strings.verifyComplaintIn7Days:
      'We will verify and process your claim within 7 days, please check in service details for further updates',

  Strings.unpaidServicesNoti: 'You have\nan unpaid service!',
  Strings.unpaidSercicesDetailsP1: 'The',
  Strings.unpaidSercicesDetailsP2: 'service pack still not paid, check now!',

  Strings.cannotDeteteThisSkillNoti:
      'You cannot delete this skill because your profile\nmust have at least 1 skill.',

  Strings.withdrawSuccessNoti: 'Withdrawal request successful!',
  Strings.withdrawProcessIn30Days:
      'We will process your request within 30 business days.',
  Strings.withdrawFailedNoti: 'Withdrawal request failed!',

  Strings.notBookedYetNoti:
      'Cannot call to user because not exists appointment with this user.',
  Strings.doItNow: 'Do it now!',

  // fix

  Strings.requests: 'Requests',
  Strings.requestsOfme: 'My requests',

  // authentication

  Strings.notRegistrationExpertNoti:
      'You have not registered the expert information. Please register information to start becoming an expert on Askany.',
  Strings.emailNotExistNoti:
      'Email does not exist, please check your login email!',
  Strings.wrongPasswordNoti: 'Password is incorrect, try again!',
  Strings.unConfirmedAccount: 'Email not verified!',
  Strings.phoneExistedNoti:
      'This phone number is already registered, please use another number!',
  Strings.emailExistedNoti:
      'Email already registered, please use another email!',
  Strings.socialExistedNoti:
      'This social network account is already registered, please use another account!',

  // helpers
  Strings.askAnyWantToAccessYourCamera: 'AskAny want to access your Camera',
  Strings.allowAskAnyToAccessYourCamera:
      'You can take photos, record video, video call during using this app.',

  // helpers

  Strings.monthNames[0]: 'January',
  Strings.monthNames[1]: 'February',
  Strings.monthNames[2]: 'March',
  Strings.monthNames[3]: 'April',
  Strings.monthNames[4]: 'May',
  Strings.monthNames[5]: 'June',
  Strings.monthNames[6]: 'Juluy',
  Strings.monthNames[7]: 'August',
  Strings.monthNames[8]: 'September',
  Strings.monthNames[9]: 'October',
  Strings.monthNames[10]: 'November',
  Strings.monthNames[11]: 'December',

  Strings.dayNames[0]: 'Mon',
  Strings.dayNames[1]: 'Tue',
  Strings.dayNames[2]: 'Wed',
  Strings.dayNames[3]: 'Thu',
  Strings.dayNames[4]: 'Fri',
  Strings.dayNames[5]: 'Sat',
  Strings.dayNames[6]: 'Sun',

  Strings.invalid: 'Invalid!',

  Strings.MY_MESSAGE_OPTIONS[0]: 'Copy',
  Strings.MY_MESSAGE_OPTIONS[1]: 'Edit',
  Strings.MY_MESSAGE_OPTIONS[2]: 'Quote',
  Strings.MY_MESSAGE_OPTIONS[3]: 'Select Message',
  Strings.MY_MESSAGE_OPTIONS[4]: 'Delete',

  Strings.FRIEND_MESSAGE_OPTIONS[0]: 'Copy',
  Strings.FRIEND_MESSAGE_OPTIONS[1]: 'Quote',
  Strings.FRIEND_MESSAGE_OPTIONS[2]: 'Select Message',
  Strings.FRIEND_MESSAGE_OPTIONS[3]: 'Report',

  Strings.CHAT_OPTIONS[0]: 'Edit Name',
  Strings.CHAT_OPTIONS[1]: 'Expert\'s information',
  Strings.CHAT_OPTIONS[2]: 'Mark',
  Strings.CHAT_OPTIONS[3]: 'notification',
  Strings.CHAT_OPTIONS[4]: 'Delete chat',
  // Connection dialog

  Strings.connectionFailed: 'Connection failed',
  Strings.connectAgain: 'Please try again later!',

  // Download dialog

  Strings.downloadImageFailed: 'Not saved!',
  Strings.downloadImageSuccess: 'Saved!',

  // Fix Bugs April

  Strings.rateNow: 'Rate now',
  Strings.justSendPictures: 'has just sent some pictures.',
  Strings.descriptionCancelBookingExpert:
      'Are you sure you want to cancel this expert appointment?',

          Strings.adviseDurationMustBeLessThan240m: 'Advise duration must be less than 240 minutes',
      Strings.adviseDurationMustBeLessThan4h: 'Advise duration must be less than 4 hours'
};
