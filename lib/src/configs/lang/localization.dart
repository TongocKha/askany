import 'package:askany/src/configs/lang/en_US.dart';
import 'package:askany/src/configs/lang/vi_VN.dart';
import 'package:i18n_extension/i18n_extension.dart';

class Strings {
  // Language
  static final String vietnamese = 'vietnamese';
  static final String english = 'english';

  // Home
  static final String home = 'home';
  static final String category = 'category';
  static final String calendar = 'calendar';
  static final String request = 'request';
  static final String message = 'message';
  static final String notification = 'notification';
  static final String account = 'account';

  // Request
  static final String serviceRequest = 'serviceRequest';
  static final String bidServiceRequest = 'bidServiceRequest';
  static final String happening = 'happening';
  static final String finished = 'finished';

  // Video call
  static final String permissionTitleMic = 'Permission title micro';
  static final String permissionTitleCamera = 'Permission title camera';

  ///
  ///

  // static final String textStopShare = 'Dừng chia sẻ';
  static final String sharingScreenNoti = 'sharingScreenNoti';

  // Account Screen
  static final String verifiedAccount = 'verifiedAccount';
  static final String unVerifiedAccount = 'unVerifiedAccount';
  static final String myServiceManagementTitle = 'myServiceManaGamentTitle';
  static final String expertProfile = 'expertProfile';
  static final String personalInfor = 'personalInfor';
  static final String changePassword = 'changePassword';
  static final String verifyAccountAppbartext = 'verifyAccountAppbartext';
  static final String languages = 'languages';

  static final String mySkills = 'mySkills';
  static final String bodyOfAddSkillDialog = 'bodyOfAddSkillDialog';
  static final String addMoreSkillsWithPlus = 'addMoreSkillsWithPlus';
  static final String yearsOfExperience = 'yearsOfExperience';
  static final String yearOfExperience = 'yearOfExperience';
  static final String createFirstSkillSugesstion = 'createFirstSkillSugesstion';
  static final String createNewSkill = 'createNewSkill';
  static final String verifyEmailAppbarText = 'verifyEmailAppbarText';
  static final String verifyEmailSuggestion = 'verifyEmailSuggestion';

  // Account verification

  static final String yourEmailHasBeenVerified = 'yourEmailHasBeenVerified';
  static final String pleaseOpenInboxOf = 'pleaseOpenInboxOf';
  static final String openInboxSuggestion = 'openInboxSuggestion';
  static final String verifyNow = 'verifyNow';
  static final String changeYourEmail = 'changeYourEmail';

  // AccountVerificationEmailSuccessScreen

  static final String congratulationYouHasSuccesfullyVerifiedYourEmailPart1 =
      'congratulationYouHasSuccesfullyVerifiedYourEmailPart1';
  static final String congratulationYouHasSuccesfullyVerifiedYourEmailPart2 =
      'congratulationYouHasSuccesfullyVerifiedYourEmailPart2';
  static final String nowYouCanExperiencingOurAmazingFeaturePart1 =
      'nowYouCanExperiencingOurAmazingFeaturePart1';
  static final String nowYouCanExperiencingOurAmazingFeaturePart2 =
      'nowYouCanExperiencingOurAmazingFeaturePart2';
  static final String backToHomePage = 'backToHomePage';

  // AccountVerificationPhoneScreen
  static final String verifyYourPhoneAppbarText = 'verifyYourPhoneAppbarText';
  static final String verifyPhoneSuggestion = 'verifyPhoneSuggestion';
  static final String yourPhoneNumberHasBeenVerified =
      'yourPhoneNumberHasBeenVerified';
  static final String changeYourPhoneNumber = 'changeYourPhoneNumber';

  // AccountVerificationScreen

  static final String verifyYourCompanyAndPositionAppbartext =
      'verifyYourCompanyAndPositionAppbartext';

  // AccountVertifyCompanyScreen
  static final String processing = 'processing';
  static final String confirmed = 'confirmed';
  static final String rejected = 'rejected';
  // reject or refuse
  static final String yourCompanyAndPositionHaveBeenVerified =
      'yourCompanyAndPositionHaveBeenVerified';
  static final String showEmploymentContractSuggestion =
      'showEmploymentContractSuggestion';
  static final String linkedinAccount = 'linkedinAccount';
  static final String linkedinLinkHintext = 'linkedinLinkHintext';
  static final String atLeast3Characters = 'atLeast3Characters';
  // 'Kí tự phải có ít nhất 3 ký tự' ???
  static final String eitherUploadPhoto = 'eitherUploadPhoto';
  static final String notice = 'notice!';
  static final String privideEnoughInformationNotice =
      'privideEnoughInformationNotice';
  static final String verificationInformationSentSuccessfully =
      'verificationInformationSentSuccessfully';
  static final String checkWithin7Days = 'checkWithin7Days';

  // AddSkillScreen
  static final String vnd = 'vnd';
  static final String usd = 'usd';
  static final String addMoreAppbarText = 'addMoreAppbarText';
  static final String editAppbarText = 'editAppbarText';
  static final String skillLowerKeyAppbarText = 'skillLowerKeyAppbarText';
  static final String selecrASectorSuggestionTitle =
      'selecrASectorSuggestionTitle';
  static final String selectASectorHintext = 'selectASectorHintext';
  static final String haveNotSelectedAnySectorErrortext =
      'haveNotSelectedAnySectorErrortext';
  static final String nameOfYourSkillTitle = 'nameOfYourSkillTitle';
  static final String skillNameHintext = 'skillNameHintext';
  static final String skillNameCannotBeLeftBlankErrortext =
      'skillNameCannotBeLeftBlankErrortext';
  static final String yearsCountOfExprieceTitle = 'yearsCountOfExprieceTitle';
  static final String enterYearsCountOfExperienceHintext =
      'enterYearsCountOfExperienceHintext';
  static final String yearsCountOfExperienceCannotBeBlankErrortext =
      'yearsCountOfExperienceCannotBeBlankErrortext';
  static final String whatYourHighestPositionTitle =
      'whatYourHighestPositionTitle';
  static final String selectYourHighestPositionHintext =
      'selectYourHighestPositionHintext';
  static final String positionCannotBeLeftBlankErrortext =
      'positionCannotBeLeftBlankErrortext';
  static final String whatYourCurrentCompanyTitle =
      'whatYourCurrentCompanyTitle';
  static final String companyNameHintext = 'companyNameHintext';
  static final String companyNameCannotBeLeftBlankErrortext =
      'companyNameCannotBeLeftBlankErrortext';
  static final String skillDescriptionTitle = 'skillDescriptionTitle';
  static final String describeYourSkillHintext = 'describeYourSkillHintext';
  static final String skillDescribeCannotBeLeftBlankErrorText =
      'skillDescribeCannotBeLeftBlankErrorText';
  static final String costOfASingleCallingServiceTitle =
      'costOfASingleCallingServiceTitle';
  static final String fifteenMinutes = 'fifteenMinutes';
  static final String costCannotBeLeftblankErrortext =
      'costCannotBeLeftblankErrortext';
  static final String costOfASingleMeetingServiceTitle =
      'costOfASingleMeetingServiceTitle';
  static final String sixtyMinutes = 'sixtyMinutes';
  static final String participantsCountSuggetion = 'participantsCountSuggetion';
  // static final String schedulerForAPhoneCall = 'Scheduler for a phone call';
  static final String call = 'call';
  static final String meet = 'meet';
  static final String serviceKeywordTitle = 'serviceKeywordTitle';
  static final String serviceKeywordHintext = 'serviceKeywordHintext';
  static final String serviceKeyWordBlankErrortext =
      'serviceKeyWordBlankErrortext';
  static final String addMoreSkillWithoutPlus = 'addMoreSkillWithoutPlus';
  static final String save = 'save';
  static final String clear = 'clear';
  static final String areYouSureToSlearThisSkill = 'areYouSureToSlearThisSkill';
  static final String cancel = 'cancel';
  static final String deleteThisSkill = 'deleteThisSkill';

  // ChangeEmailScreen

  static final String changeEmailAppbarText = 'changeEmailAppbarText';
  static final String enterYourEmailSugestion = 'enterYourEmailSugestion';
  static final String emailHintext = 'emailHintext';
  static final String emailMustBeAtLeast3CharactersErrortext =
      'emailMustBeAtLeast3CharactersErrortext';
  static final String next = 'next';

  // ChangePasswordScreen
  static final String changePasswordAppbarText = 'changePasswordAppbarText';
  static final String currentPasswordTitle = 'currentPasswordTitle';
  static final String yourPassWordMustBeAtLeast6CharactersErrortext =
      'yourPassWordMustBeAtLeast6CharactersErrortext';
  static final String newPasswordTitle = 'newPasswordTitle';
  static final String confirmPasswordTitle = 'confirmPasswordTitle';
  static final String passwordDoesNotMatchErrortext =
      'passwordDoesNotMatchErrortext';

  // ChangePhoneScreen

  static final String changePhoneNumberAppbarText =
      'changePhoneNumberAppbarText';
  static final String enterPhoneNumberSuggestion = 'enterPhoneNumberSuggestion';
  static final String phoneNumberHintext = 'phoneNumberHintext';
  static final String phoneNumberMustBeAtLeast9CharactersErrortext =
      'phoneNumberMustBeAtLeast9CharactersErrortext';

  // EditProfileScreen

  static final String editProfileAppbartext = 'editProfileAppbartext';
  static final String update = 'update';
  static final String nameTitle = 'nameTitle';
  static final String nameMustBeAtLeast3CharactersErrortext =
      'nameMustBeAtLeast3CharactersErrortext';
  static final String addressTitle = 'addressTitle';
  static final String address = 'address';
  static final String phoneNumberTitle = 'phoneNumberTitle';
  static final String verifyYourPhoneNumber = 'verifyYourPhoneNumber';
  static final String logout = 'logout';

  // ExperienceScreen
  static final String exprienceAppbarText = 'exprienceAppbarText';
  static final String createExperienceSuggestion = 'createExperienceSuggestion';
  static final String createNewExprience = 'createNewExprience';
  static final String createNewExprienceWithPlus = 'createNewExprienceWithPlus';

  // ExpertEditPriceScreen

  static final String priceOfASingleServiceAppbartext =
      'priceOfASingleServiceAppbartext';
  static final String enterPriceErrortext = 'enterPriceErrortext';
  static final String availableTimeTitle = 'availableTimeTitle';
  static final String availableTimeSelectionNoti = "availableTimeSelectionNoti";
  static final String availableTimeNotValid = 'availableTimeNotValid';
  static final String availavleTimeIsAllDayNoti = 'availavleTimeIsAllDayNoti';
  static final String addMoreAvailableDayWithPlus =
      'addMoreAvailableDayWithPlus';
  static final String priceMustOverHaflBuckErorrtext = 'priceMustOverHaflBuck';
  static final String priceMustbeOver12k = 'priceMustbeOver12k';
  static final String noTimeSelectedYet = 'noTimeSelectedYet';
  static final String noTimeSelectedYetNoti = 'noTimeSelectedYetNoti';

  // InfoExpertScreen

  static final String expertInfomationAppbartext = 'expertInfomationAppbartext';
  static final String howManyLanguages = 'howManyLanguages';
  static final String blankLanguageErrortext = 'blankLanguageErrortext';
  static final String whatSectorYouHaveExprence = 'whatSectorYouHaveExprence';
  static final String sectorOfExpertise = 'sectorOfExpertise';
  static final String sector = 'sector';
  static final String sectorCannotBeLeftBlankErrortext =
      'sectorCannotBeLeftBlankErrortext';
  static final String howManyYearsOfExprience = 'howManyYearsOfExprience';
  static final String yearsCountOfExperienceHinttext =
      'yearsCountOfExperienceHinttext';
  static final String yearsCountofExperienceBlankErrortext =
      'yearsCountofExperienceBlankErrortext';
  static final String yourCompanyYouWorkForTitle = 'yourCompanyYouWorkForTitle';
  static final String descriptionAboutYourExperienceAndSkills =
      'descriptionAboutYourExperienceAndSkills';
  static final String enterDescriptionAboutYouHintext =
      'enterDescriptionAboutYouHintext';
  static final String descriptionAboutYouCannotBeLeftBlankErrortext =
      'descriptionAboutYouCannotBeLeftBlankErrortext';

  // InputOTPScreen

  static final String enterOTPsuggestion = 'enterOTPsuggestion';
  static final String verify = 'verify';

  // ProfileExpertScreen

  static final String viewInPage = 'viewInPage';
  static final String reviews = 'reviews';
  static final String reviewsAppbartext = 'reviewsAppbartext';

  static final String review = 'review';

  // ServiceManaGamentScreen

  static final String youDontHavenAnyRequest = 'youDontHavenAnyRequest';
  static final String confirm = 'confirm';

  // Components

  // BottomSheetPosition

  static final String yourPosition = 'yourPosition';

  // ContainerManageAccount

  static final String consultingSchedule = 'consultingSchedule';
  static final String myWallet = 'myWallet';

  // ManageServiceArea

  static final String confirming = 'confirming';
  static final String advise = 'advised';
  static final String completed = 'completed';

  // ServiceManaGamentCard
  static final String areYouSureYouWouldLikeToConsultService =
      'areYouSureYouWouldLikeToConsultService';
  static final String agree = 'agree';
  static final String expert = 'expert';
  static final String experts = 'experts';
  static final String callingPackage = 'callingPackage';
  static final String meetingPackage = 'meetingPackage';
  static final String place = 'place';
  static final String waitForPayment = 'waitForPayment';
  static final String reportProcessing = 'reportProcessing';

  // PeriodContainer

  static final String monday = 'monday';
  static final String sunday = 'sunday';
  static final List<String> month = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec',
  ];
  static final String repeatEveryWeek = 'repeatEveryWeek';
  static final String availableForCalling = 'availableForCalling';
  static final String availableForMeeting = 'availableForMeeting';
  static final String addMoreAvailableTimeWithPlus =
      'addMoreAvailableTimeWithPlus';
  static final String cannotAddThisAvailableTime = 'cannotAddThisAvailableTime';
  static final String pleaseSelecteAvailableDayFirst =
      'pleaseSelecteAvailableDayFirst';

  // SkillCard

  static final String minute = 'minute';
  static final String minutes = 'minutes';

  // AddExperienceScreen

  static final String add = 'add';
  static final String experienceInWork = 'experienceInWork';

  // FormAddExperience

  static final String yourExperience = 'yourExperience';
  static final String company = 'company';
  static final String specialization = 'specialization';
  static final String specializationCannotBeLeftBlankErrortext =
      'specializationCannotBeLeftBlankErrortext';

  static final String underStood = 'underStood';
  static final String send = 'send';
  static final String positionInWork = 'positionInWork';
  static final String fromTime = 'fromTime';
  static final String monthCannotBeLeftBlankErrortext =
      'monthCannotBeLeftBlankErrortext';
  static final String year = 'year';
  static final String yearCannotBeLeftBlankErrortext =
      'yearCannotBeLeftBlankErrortext';
  static final String toTime = 'toTime';
  static final String detailsOfWork = 'detailsOfWork';
  static final String detailsCannotBeLeftblankErrortext =
      'detailsCannotBeLeftblankErrortext';
  static final String areYouSureToClearThisExperience =
      'areYouSureToClearThisExperience';
  static final String deleteThisExperience = 'deleteThisExperience';

  // FormAddSkill
  static final String earnMoneySuggestion = 'earnMoneySuggestion';

  // RegisterExpertStep1

  static final String createYourSelfAIncredibleProfileSuggestion =
      'createYourSelfAIncredibleProfileSuggestion';

  // RegisterExpertStep3
  static final String costOfASingleService = 'costOfASingleService';

  // RegisterExpertScreen
  static final String nextStep = 'nextStep';

  // ChooseAccountScreen
  static final String welcomeToAskAny = 'welcomeToAskAny';
  static final String tellUsWhatAreYouLookingFor = 'tellUsWhatAreYouLookingFor';
  static final String iNeedSomeAdvisse = 'iNeedSomeAdvisse';
  static final String imAnExpert = 'imAnExpert';
  // ForgotPasswordScreen

  static final String forgotPasswordAppbartext = 'forgotPasswordAppbartext';
  static final String enterEmailToGetLinkSuggestion =
      'enterEmailToGetLinkSuggestion';
  static final String emailCannotBeLeftBlankErrortext =
      'emailCannotBeLeftBlankErrortext';

  // LoginScreen

  static final String invalidEmailErrortext = 'invalidEmailErrortext';
  static final String passwordMustBeAtLeast6CharactersErrortext =
      'passwordMustBeAtLeast6CharactersErrortext';
  static final String passwordHintext = 'passwordHintext';
  static final String login = 'login';
  static final String keepMeLoggedIn = 'keepMeLoggedIn';
  static final String newUser = 'newUser';
  static final String signUp = 'signUp';

  // RegisterScreen
  static final String invalidPhoneNumberErrortext =
      'invalidPhoneNumberErrortext';
  static final String fullnameMustBeAtLeast3CharactersErrortext =
      'fullnameMustBeAtLeast3CharactersErrortext';
  static final String addressCannotBeLeftBlankErrortext =
      'addressCannotBeLeftBlankErrortext';
  static final String alreadyHaveAnAccount = 'alreadyHaveAnAccount';

  // RegisterSocialScreen
  static final String nameCannotBeLeftBlankErrortext =
      'nameCannotBeLeftBlankErrortext';
  static final String passwordCannotBeLeftBlankErrortext =
      'passwordCannotBeLeftBlankErrortext';
  static final String phoneNumberCannotBeLeftBlankErrortext =
      'phoneNumberCannotBeLeftBlankErrortext';
  static final String pressSignUpMean = 'pressSignUpMean';
  static final String rules = 'rules';
  static final String ofUs = 'ofUs';

  // ExperienceCard

  // LanguageBottomSheet
  static final String clearAll = 'clearAll';
  static final String ok = 'ok';

  // RegisterExpertHeader

  static final String createProfile = 'createProfile';
  static final String skillsOfYou = 'skillsOfYou';
  static final String step = 'step';

  // SpecialtyBottomSheet

  static final String sectorOfRequestAppbartext = 'sectorOfRequestAppbartext';
  // title in contructor must be const!!!
  // AuthFooter
  static final String or = 'or';
  // ConSultServiceInfoScreen

  // static final String overviewOfThisServiceAppbartext =
  //     'Overview of this Service';
  // static final String totalNumberOfParticipants =
  //     'Total number of participants: ';

  // CalendarScreen

  static final seeByMonthOrWeekSuggestion = "seeByMonthOrWeekSuggestion";
  static final swipeLeftorRightToSeeOtherMonthSuggestion =
      "swipeLeftorRightToSeeOtherMonthSuggestion";

  // CalendarHeader

  static final monthlyCalendar = 'monthlyCalendar';
  static final weeklyCalendar = 'weeklyCalendar';
  static final welcomeBack = 'welcomback';

  // TimelineMonthEmpty

  static final dontHaveAnyConsultationBooked = 'dontHaveAnyConsultationBooked';
  static final String findExpertSuggestion = 'findExpertSuggestion';

  // CategoryScreen
  static final String categoryAppbartext = 'categoryAppbartext';
  static final String findAnExpertInEverySectorsWhoCanResolveYourProblems =
      'findAnExpertInEverySectorsWhoCanResolveYourProblems';
  // DetailsCategoryScreen
  static final String unfortunatelyNoExpertInThisSectorSuggestion =
      'unfortunatelyNoExpertInThisSectorSuggestion';
  static final noExpertInThisSectorSuggestion =
      'noExpertInThisSectorSuggestion';
  // DetailsExpertScreen

  static final String sendMessages = 'sendMessages';
  static final String contactNow = 'contactNow';
  static final String infomationTabbartext = 'infomationTabbartext';
  static final String reviewTabbartext = 'reviewTabbartext';
  static final String minimum = 'minimum';
  static final String block = 'block';
  static final String afterblockedSuggestion = 'afterblockedSuggestion';
  static final String blockUser = 'blockUser';
  static final String currentPosition = 'currentPosition';
  static final String currentCompany = 'currentCompany';
  static final String otherExperts = 'otherExperts';
  static final String noneRecommendationsSuggestion =
      'noneRecommendationsSuggestion';
  // FilterExpertScreen

  static final String filter = 'filter';
  static final String soft = 'soft';
  static final String latest = 'latest';
  static final String mostPopular = 'mostPopular';
  static final String rating = 'rating';
  static final String price = 'price';
  static final String contactForm = 'contactForm';
  static final String accoutType = 'accoutType';
  static final String all = 'all';
  static final String verifiedAccoutFilter = 'verifiedAccoutFilter';
  static final String noReviews = 'noReviews';

  // ChatScreen
  static final String yourInboxIsEmptyNoti = 'yourInboxIsEmptyNoti';
  static final String delete = 'delete';

  // BottomChatOptions
  static final String editName = 'editName';
  static final String markAsUnread = 'markAsUnread';
  static final String markAsRead = 'markAsRead';
  static final String muteNotification = 'muteNotification';
  static final String unmuteNotification = 'unmuteNotification';
  static final String deleteChat = 'deleteChat';
  static final String editNameOfChat = 'editNameOfChat';
  static final String areYouSureToDeleteThisChat = 'areYouSureToDeleteThisChat';

  // CallingBar

  static final String tapToReturnTheCall = 'tapToReturnTheCall';

  // CustomImagePicker

  static final String uploadANewPhoto = 'uploadANewPhoto';

  // DialogInput

  static final String enterChatName = 'enterChatName';
  static final String chatNameMustBeAtLeast3CharactersErrortext =
      'chatNameMustBeAtLeast3CharactersErrortext';
  // TitleAndSeeMore
  static final String viewAll = 'viewAll';

  // ConversationScreen
  static final String copy = 'copy';
  static final String areYourSureToDeleteThisMessage =
      'areYourSureToDeleteThisMessage';
  // AppointmentChatCard
  static final String servicePackage = 'servicePackage';
  static final String hasBeenCanceled = 'hasBeenCanceled';
  static final String hasOrderedTheServicePackageNamed =
      'hasOrderedTheServicePackageNamed';
  static final String
      thisServiceHasBennCanceledBecauseOfExpertIsBusyAtThatTime =
      'thisServiceHasBennCanceledBecauseOfExpertIsBusyAtThatTime';
  static final String time = 'time';
  static final String viewAllDetails = 'viewAllDetails';

  // ConversationHeaderSelectedMessage

  static final String selectedMessage = 'selectedMessage';
  static final String selectedMessages = 'selectedMessages';
  static final String thisChatOnAskAny = 'thisChatOnAskAny';

  // InputMessage
  static final String yourMessageHintext = 'yourMessageHintext';

  // MessageCallCard
  static final String thisCallIsEnd = 'thisCallIsEnd';
  static final String youMissedThisCall = 'youMissedThisCall';
  static final String atTime = 'atTime';

  // MessageCallCard

  static final String sending = 'sending';
  static final String failedRetry = 'failedRetry';

  // HomeScreen
  static final String discover = 'discover';
  static final String expertCommunity = 'expertCommunity';
  static final String monthExpert = 'monthExpert';
  static final String seeMore = 'seeMore';
  static final String popularServices = 'popularServices';
  static final String plentifulAndProfessionalExpertCommunity =
      'plentifulAndProfessionalExpertCommunity';
  static final String solutionForAllSector = 'solutionForAllSector';
  static final String findSomething = 'findSomething';

  // CancelDescription

  static final String customer = 'customer';
  static final String thisServiceHasBeenCanceled = 'thisServiceHasBeenCanceled';
  static final String cancelBy = 'cancelBy';
  static final String cancelReason = 'cancelReason';
  static final String refundCompletedNoti = 'refundCompletedNoti';
  static final String refundIn24HoursNoti = 'refundIn24HoursNoti';
  static final String refundDetails = 'refundDetails';
  static final String refundAt = 'refundAt';
  static final String askAnyWallet = 'askAnyWallet';
  static final String refundAmount = 'refundAmount';
  static final String repaidTime = 'repaidTime';

  // CancelServiceScreen

  static final String selectAReason = 'selectAReason';
  static final String cancelThisServiceAppbartext =
      'cancelThisServiceAppbartext';
  static final String pleaseTellUsTheReasonWhyYouCancelThisServicePart1 =
      'pleaseTellUsTheReasonWhyYouCancelThisServicePart1';
  static final String pleaseTellUsTheReasonWhyYouCancelThisServicePart2 =
      'pleaseTellUsTheReasonWhyYouCancelThisServicePart2';
  static final String reason = 'reason';
  static final String reasonCannotBeLeftBlankErrortext =
      'reasonCannotBeLeftBlankErrortext';
  static final String details = 'details';
  static final String contentHintext = 'contentHintext';

  // ContinuesPaymentScreen

  static final String orderingService = 'orderingService';
  static final String enteringInformation = 'enteringInformation';
  static final String selectingTime = 'selectingTime';
  static final String payment = 'payment';
  static final String totalPrice = 'totalPrice';
  static final String youHaveNotSelectedAnyTimeErrortext =
      'youHaveNotSelectedAnyTimeErrortext';

  // PaymentInfoServiceCard

  static final String paymentInformation = 'paymentInformation';
  static final String serviceType = 'serviceType';
  static final String participants = 'participants';
  static final String people = 'people';
  static final String costFor = 'costFor';
  static final String adviseDuration = 'adviseDuration';
  static final String extraFee = 'extraFee';

  // ReplyFeedbackScreen

  static final String replyFeedbackAppbartext = 'replyFeedbackAppbartext';
  static final String replyFeedbackOf = 'replyFeedbackOf';

  // ReportCompletedView

  static final String complain = 'complain';
  static final String complaint = 'complaint';
  static final String complainReason = 'complainReason';
  static final String thisComplainHasBeenClosed = 'thisComplainHasBeenClosed';
  static final String rpClosed = 'rpClosed';
  static final String complaintIsBeingVerify = 'complaintIsBeingVerify';

  // ServiceComplaintScreen

  static final String serviceComplaining = 'serviceComplaining';
  static final String pleaseTellUsTheReasonWhyYouComplainPart1 =
      'pleaseTellUsTheReasonWhyYouComplainPart1';
  static final String pleaseTellUsTheReasonWhyYouComplainPart2 =
      'pleaseTellUsTheReasonWhyYouComplainPart2';

  // ServiceDescriptionCard
  static final String service = 'service';
  static final String contacter = 'contacter';

  // ServiceDetailsScreen

  static final String buyingService = 'buyingService';
  static final String confirmingService = 'confirmingService';
  static final String advising = 'advising';
  static final String complete = 'complete';
  static final String cancelService = 'cancelService';
  static final String serviceDetailsAppbartext = 'serviceDetailsAppbartext';
  // ServiceFeedbackScreen

  static final String serviceReview = 'serviceReview';
  static final String areYouSatisfiedWith = 'areYouSatisfiedWith';
  static final String areYouSatisfiedWithPart2 = 'areYouSatisfiedWithPart2';
  static final String postReview = 'postReview';

  // ContinuesPaymentSteps
  static final String yourContactInformation = 'yourContactInformation';
  static final String fullName = 'fullName';
  static final String note = 'note';
  static final String noteHintext = 'noteHintext';
  static final String intoPrice = 'intoPrice';
  static final String choosePaymentMethod = 'choosePaymentMethod';
  static final String paymentbyVNPay = 'paymentbyVNPay';
  static final String paymentByAskAnyWallet = 'paymentByAskAnyWallet';
  static final String walletBalance = 'walletBalance';
  static final String notEnoughBalanceNoti = 'notEnoughBalanceNoti';
  static final String invalidTimeErrortext = 'invalidTimeErrortext';
  static final String chooseAdviseDate = 'chooseAdviseDate';
  static final String chooseAdviceTime = 'chooseAdviceTime';
  static final String morning = 'morning';
  static final String afternoon = 'afternoon';

  // ServiceDetailsBottomButton
  static final String youConfirmAdvisingTheServiceP1 =
      'youConfirmAdvisingTheServiceP1';
  static final String youConfirmAdvisingTheServiceP2 =
      'youConfirmAdvisingTheServiceP2';
  static final String youComfirmCompletion = 'youComfirmCompletion';

  // ServiceFeedbackView
  static final String thereNoReviewYet = 'thereNoReviewYet';
  static final String advised = 'advised';
  static final String canceled = 'canceled';
  static final String complained = 'complained';

  // NotificationScreen

  static final String markAllAsReadNoti = 'markAllAsReadNoti';
  static final String readAll = 'readAll';
  static final String youDontHaveAnyNoti = 'youDontHaveAnyNoti';

  // CreateRequestScreen

  static final String createNewRequest = 'createNewRequest';
  static final String editRequestDetails = 'editRequestDetails';
  static final String myRequest = 'myRequest';
  static final String requestTitleHintext = 'requestTitleHintext';
  static final String requestTitleCannotBeLeftBlankErrortext =
      'requestTitleCannotBeLeftBlankErrortext';
  static final String requestContent = 'requestContent';
  static final String requestContentHintext = 'requestContentHintext';
  static final String requestContentCannotBeLeftBlankErrortext =
      'requestContentCannotBeLeftBlankErrortext';
  static final String budget = 'budget';
  static final String availableBudget = 'availableBudget';
  static final String budgetCannotBeLeftBlankErrortext =
      'budgetCannotBeLeftBlankErrortext';
  static final String participantHintext = 'participantHintext';
  static final String participantsCannotBeLeftBlankErrortext =
      'participantsCannotBeLeftBlankErrortext';
  static final String bidingClosingDate = 'bidingClosingDate';
  static final String placeCannotBeLeftBlankErrortext =
      'placeCannotBeLeftBlankErrortext';
  static final String placeNameHintext = 'placeNameHintext';
  static final String placeNameCannotBeLeftBlankErrortext =
      'placeNameCannotBeLeftBlankErrortext';
  static final String postRequest = 'postRequest';
  static final String priceHintext = 'priceHintext';

  // DetailsRequestScreen
  static final String noBidsYet = 'noBidsYet';
  static final String bidingEnd = 'bidingEnd';
  static final String congratulationYouAreTheChosenOne =
      'congratulationYouAreTheChosenOne';
  static final String unfortunalyYouAreNotTheChosen =
      'unfortunalyYouAreNotTheChosen';
  static final String selectedExpert = 'selectedExpert';
  static final String bidExperts = 'bidExperts';
  static final String yourBids = 'yourBids';
  static final String offer = 'offer';
  static final String cancelOffer = 'cancelOffer';
  static final String areYouSureToCancelP1 = 'areYouSureToCancelP1';
  static final String areYouSureToCancelP2 = 'areYouSureToCancelP2';
  static final String customerInformation = 'customerInformation';

  // FilterRequestScreen

  static final String searchRequest = 'searchRequest';
  static final String searchHintext = 'searchHintext';
  static final String currency = 'currency';

  // MyRequestScreen
  static final String beingInprogress = 'beingInprogress';
  static final String closed = 'closed';

  // OrderServiceScreen

  // PaymentResultScreen

  static final String paymentResult = 'paymentResult';
  static final String paymentSuccess = 'paymentSuccess';
  static final String paymentFailure = 'paymentFailure';
  static final String paymentSuccessNoti = 'paymentSuccessNoti';
  static final String paymentFailureNoti = 'paymentFailureNoti';

  // RequestServiceScreen

  static final String requestSearchingHintext = 'requestSearchingHintext';

  // CreateOffer

  static final String offerForThisRequest = 'offerForThisRequest';
  static final String yourOfferForThisRequest = 'yourOfferForThisRequest';
  static final String timeHintext = 'timeHintext';
  static final String adviseDurationMustBeDivisibleBy15Errortext =
      'adviseDurationMustBeDivisibleBy15Errortext';
  static final String adviseDurationCannotBeLeftBlankErrortext =
      'adviseDurationCannotBeLeftBlankErrortext';
  static final String hoursUperCase = 'hours';
  static final String hourUperCase = 'hour';

  static final String minutesUperCase = 'minutesUperCase';
  static final String minuteUperCase = 'minuteUperCase';

  static final String convinceCustomers = 'convinceCustomers';
  static final String postOffer = 'postOffer';

  // OfferRequestCard

  static final String offerContent = 'offerContent';
  static final String offerCost = 'offerCost';
  static final String showMore = 'showMore';
  static final String showLess = 'showLess';
  static final String chooseExpert = 'chooseExpert';
  static final String yourDecidedToChoose = 'yourDecidedToChoose';
  static final String forThisRequest = 'forThisRequest';
  static final String selected = 'selected';

  static final String select = 'select';

  // RequestCard

  static final String expirationTime = 'expirationTime';
  static final String noOffersNoti = 'noOffersNoti';
  static final String offers = 'offers';
  static final String bidding = 'bidding';
  static final String bidSuccess = 'bidSuccess';
  static final String bid = 'bid';
  static final String noExpertSelected = 'noExpertSelected';
  static final String selectedAnExpert = 'selectedAnExpert';

  // RequestDetailsCard

  static final String requestHasBidNoti = 'requestHasBidNoti';
  static final String deepInfor = 'deepInfor';
  static final String thereIs = 'thereIs';
  static final String thereAre = 'thereAre';

  static final String expirationDate = 'expirationDate';
  static final String endRequest = 'endRequest';
  static final String youDecidedToEndRequest = 'youDecidedToEndRequest';
  static final String makeACall = 'makeACall';
  static final String makeAAppointment = 'makeAAppointment';
  static final String notUpdated = 'notUpdated';
  static final String requestsAppbartext = 'requestsAppbartext';
  static final String bidRequests = 'bidRequests';
  static final String myRequests = 'myRequests';
  static final String pressToCreateNewRequest = 'pressToCreateNewRequest';

  // BottomSheetContactExpert

  static final String youCannotMakeAAppointmentNoti =
      'youCannotMakeAAppointmentNoti';
  static final String youCannotMakeACallNoti = 'youCannotMakeACallNoti';
  static final String reviewsCount = 'reviewsCount';
  static final String reviewCount = 'reviewCount';

  // DetailsSkillSearchScreen

  static final String otherServices = 'otherServices';
  static final String thereNoRecommendServicesNoti =
      'thereNoRecommendServicesNoti';

  // NoResultScreen
  static final String thereNoResultNoti = 'thereNoResultNoti';
  static final String youWantToFindSomeAdviseForMoreSpecialProblem =
      'youWantToFindSomeAdviseForMoreSpecialProblem';
  static final String let = 'let';
  static final String now = 'now';

  // ResultScreen

  static final String readyServicesCount = 'readyServicesCount';
  static final String readyServiceCount = 'readyServiceCount';

  // BottomSheetInfoExpert
  static final String anExpertIn = 'anExpertIn';

  // NearbySearch
  static final String recentSearching = 'recentSearching';

  static final String whatAreYouLookingFor = 'whatAreYouLookingFor';

  static final String trending = 'trending';

  // Wallet style

  static final String failure = 'failure';
  static final String waitingForProcessing = 'waitingForProcessing';
  static final String success = 'success';
  static final String userReported = 'userReported';
  static final String userPayOutByVNPay = 'userPayOutByVNPay';
  static final String userPayOutByAskany = 'userPayOutByAskany';
  static final String adminTransferForExpert = 'adminTransferForExpert';
  static final String adminRefundForUser = 'adminRefundForUser';
  static final String userPayInByVNPay = 'userPayInByVNPay';
  static final String expertPayInByVNPay = 'expertPayInByVNPay';
  static final String userWithDrawToBank = 'userWithDrawToBank';
  static final String expertWithDrawToBank = 'expertWithDrawToBank';

  // ChatInCallScreen
  static final String chatInCall = 'chatInCall';

  // IncomingCallScreen
  static final String incomingCall = 'incomingCall';
  static final String reject = 'reject';
  static final String answer = 'answer';
  // ParticipantScreen
  static final String callParticipants = 'callParticipants';

  // TabMenuVertical

  static final String startSharingNoti = 'startSharingNoti';
  static final String startSharing = 'startSharing';
  static final String shareMyScreen = 'shareMyScreen';
  static final String stopSharing = 'stopSharing';
  static final String raiseYourHand = 'raiseYourHand';
  static final String stopSharingMyScreen = 'stopSharingMyScreen';
  static final String fullScreen = 'fullScreen';
  static final String record = 'record';

  // RechargeScreen

  static final List<String> listPrice = [
    'listPrice2',
    'listPrice5',
    'listPrice10',
    'listPrice20',
    'listPrice50',
    'listPrice100',
  ];
  static final String payInAppbartext = 'payInAppbartext';
  static final String amountToPayIn = 'amountToPayIn';
  static final String amountCannotBeLeftBlankErrortext =
      'amountCannotBeLeftBlankErrortext';
  static final String minimumAmountIs = 'minimumAmountIs';
  static final String payInSucessNotiP1 = 'payInSucessNotiP1';
  static final String payInSucessNotiP2 = 'payInSucessNotiP2';
  static final String checkMyWallet = 'checkMyWallet';

  // AddDetailCardPayment

  static final List<String> specialtiesTitle = [
    'specialtiesTitle0',
  ];
  static final String addCardP1 = 'addCardP1';
  static final String addCardP2 = ' addCardP2';
  static final String editPaymentCard = 'editPaymentCard';
  static final String cardP1 = 'cardP1';
  static final String cardP2 = 'cardP2';
  static final String accountName = 'accountName';
  static final String accountNameHintext = 'accountNameHintext';
  static final String accountNameCannotBeLeftBlankErrortext =
      'accountNameCannotBeLeftBlankErrortext';
  static final String invoiceAddress = 'invoiceAddress';
  static final String invoiceAddressHintext = 'invoiceAddressHintext';
  static final String invoiceAddressCannotBeLeftBlankErrortext =
      'invoiceAddressCannotBeLeftBlankErrortext';
  static final String city = 'city';
  static final String cityHintext = 'cityHintext';
  static final String cityCannotBeLeftBlankErrortext =
      'cityCannotBeLeftBlankErrortext';
  static final String zipCode = 'zipCode';
  static final String zipCodeHintext = 'zipCodeHintext';
  static final String zipCodeCannotBeLeftBlankErrortext =
      'zipCodeCannotBeLeftBlankErrortext';
  static final String country = 'country';
  static final String countryHintext = 'countryHintext';
  static final String countryCannotBeLeftBlankErrortext =
      'countryCannotBeLeftBlankErrortext';
  static final String creditCardInformation = 'creditCardInformation';
  static final String creditCardNumber = 'creditCardNumber';
  static final String creditCardNumberHintext = 'creditCardNumberHintext';
  static final String creditCardNumberCannotBeLeftBlankErrortext =
      'creditCardNumberCannotBeLeftBlankErrortext';
  static final String creditCardExpirationDate = 'creditCardExpirationDate';
  static final String creditCardExpirationDateHintext =
      'creditCardExpirationDateHintext';
  static final String creditCardExpirationDateCannotBeLeftBlankErrortext =
      'creditCardExpirationDateCannotBeLeftBlankErrortext';
  static final String cvv = 'cvv';
  static final String expiretionDateCannotBeLeftBlankErrortext =
      'expiretionDateCannotBeLeftBlankErrortext';
  static final String cvvCannotBeLeftBlankErrortext =
      'cvvCannotBeLeftBlankErrortext';
  static final String deleteThisCreditCard = 'deleteThisCreditCard';
  static final String areYouSureToDeleteThisCreditCard =
      'areYouSureToDeleteThisCreditCard';
  static final String deleteThisCard = 'deleteThisCard';

  // AddInfoCardPayment

  static final String addCreditInforAppbartext = 'addCreditInforAppbartext';
  static final String international = 'international';
  static final String nationalATM = 'nationalATM';

  // SaveCreditCard

  static final String rememberCreditCardAppbartext =
      'rememberCreditCardAppbartext';
  static final String rememberCreditCardInforSuggestion =
      'rememberCreditCardInforSuggestion';
  static final String savedCards = 'savedCards';

  // TransactionDetails

  static final String transactionDetailsAppbartext =
      'transactionDetailsAppbartext';
  static final String transAmount = 'transAmount';
  static final String transFrom = 'transFrom';
  static final String transTo = 'transTo';
  static final String paymentMethod = 'paymentMethod';
  static final String transFee = 'transFee';

  // TransactionHistory
  static final String transHistoryAppbartext = 'transHistoryAppbartext';

  // WithdrawATMCard
  static final String bankName = 'bankName';
  static final String bankNameHinttext = 'bankNameHinttext';
  static final String supportedType = 'supportedType';
  static final String creditCard = 'creditCard';
  // WithdrawMoney
  static final String payOutAppbartext = 'payOutAppbartext';
  static final String payOutProcessingTimeNoti = 'payOutProcessingTimeNoti';
  static final String receiveMoneyMethod = 'receiveMoneyMethod';
  static final String atmNumber = 'atmNumber';
  static final String bankAccount = 'bankAccount';
  // WithDrawMoneyToTheAccount

  static final String withdrawToBankAppbartext = 'withdrawToBankAppbartext';
  static final String withdrawAmount = 'withdrawAmount';
  static final String withdrawAmountCannotBeLeftblankErrortext =
      'withdrawAmountCannotBeLeftblankErrortext';
  static final String minimumWithdrawAmountIs2bucks =
      'minimumWithdrawAmountIs2bucks';
  static final String receivingAccountInfor = 'receivingAccountInfor';
  static final String bankBranchName = 'bankBranchName';
  static final String accountNumber = 'accountNumber';

  // WithdrawMoneyToTheCard
  static final String withdrawToCard = 'withdrawToCard';
  static final String cardType = 'cardType';
  static final String cardTypeHintext = 'cardTypeHintext';
  static final String cardTypeCannotBeLeftBlankErrortext =
      'cardTypeCannotBeLeftBlankErrortext';
  // WithdrawOver10
  static final String withdrawVerifyNoti = 'withdrawVerifyNoti';
  static final String uploadPhoto = 'uploadPhoto';

  // WithdrawUnder10

  static final String withdrawVerifyIDNoti = 'withdrawVerifyIDNoti';
  static final String frontOfId = 'frontOfId';
  static final String backsideOfId = 'backsideOfId';
  static final String edit = 'edit';

  // ConfirmationWithdraw

  static final String withdrawConfirminationAppbartext =
      'withdrawConfirminationAppbartext';
  static final String verifyByPersonalInfor = 'verifyByPersonalInfor';
  static final String total = 'total';
  static final String receivedMoneyFrom = 'receivedMoneyFrom';
  static final String recentTrans = 'recentTrans';

  // blocs

  static final String connectionNoti = 'connectionNoti';
  static final String retry = 'retry';
  static final String registerNow = 'registerNow';
  static final String verifyEmailNoti = 'verifyEmailNoti';
  static final String verifyEmailNoti2 = 'verifyEmailNoti2';
  static final String pleaseAccess = 'pleaseAccess';
  static final String loginFailed = 'loginFailed';
  static final String cannotDeleteChatNoti = 'cannotDeleteChatNoti';
  static final String cannotSendMessgaeNoti = 'cannotSendMessgaeNoti';
  static final String cannotSendMessageReason = 'cannotSendMessageReason';
  static final String cannotDeleteExperienceNoti = 'cannotDeleteExperienceNoti';

  static final String userCancelServiceRequestNoti =
      'userCancelServiceRequestNoti';
  static final String youAreChosenForRequest = 'youAreChosenForRequest';
  static final String revokedMessageNoti = 'revokedMessageNoti';
  static final String cannotEditMessageNoti = 'cannotEditMessageNoti';

  static final String errorOccurredNoti = 'errorOccurredNoti';
  static final String errorOccurredTryAgain = 'errorOccurredTryAgain';

  static final String successfulBidNoti = 'successfulBidNoti';
  static final String successfulBidCongratulations =
      'successfulBidCongratulations';
  static final String failedBid = 'failedBid';
  static final String failedBidTryAgain = 'failedBidTryAgain';
  static final String cancelBidFailed = 'cancelBidFailed';

  static final String cancelServiceSuccessNoti = 'cancelServiceSuccessNoti';
  static final String refundIn24Hours = 'refundIn24Hours';

  static final String complainSuccessNoti = 'complainSuccessNoti';
  static final String verifyComplaintIn7Days = 'verifyComplaintIn7Days';

  static final String unpaidServicesNoti = 'unpaidServicesNoti';
  static final String unpaidSercicesDetailsP1 = 'unpaidSercicesDetailsP1';
  static final String unpaidSercicesDetailsP2 = 'unpaidSercicesDetailsP2';

  static final String cannotDeteteThisSkillNoti = 'cannotDeteteThisSkillNoti';

  static final String withdrawSuccessNoti = 'withdrawSuccessNoti';
  static final String withdrawProcessIn30Days = 'withdrawProcessIn30Days';
  static final String withdrawFailedNoti = 'withdrawFailedNoti';

  static final String notBookedYetNoti = 'notBookedYetNoti';
  static final String doItNow = 'doItNow';

  // fix

  static final String requests = 'requests';
  static final String requestsOfme = 'requestsOfme';

  // authenBloc

  static final String notRegistrationExpertNoti = 'notRegistrationExpertNoti';
  static final String emailNotExistNoti = 'emailNotExistNoti';
  static final String wrongPasswordNoti = 'wrongPasswordNoti';
  static final String unConfirmedAccount = 'unConfirmedAccount';
  static final String phoneExistedNoti = 'phoneExistedNoti';
  static final String emailExistedNoti = 'emailExistedNoti';
  static final String socialExistedNoti = 'socialExistedNoti';

  // helpers

  static final String askAnyWantToAccessYourCamera =
      'askAnyWantToAccessYourCamera';
  static final String allowAskAnyToAccessYourCamera =
      'allowAskAnyToAccessYourCamera';

  static final List<String> monthNames = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ];
  static final List<String> dayNames = [
    'Mon',
    'Tue',
    'Wed',
    'Thu',
    'Fri',
    'Sat',
    'Sun',
  ];

  static final String invalid = 'invalid';
  // ignore: non_constant_identifier_names
  static final List<String> MY_MESSAGE_OPTIONS = [
    'copyMyMessage',
    'editMyMessage',
    'quoteMessage',
    'selectMyMessage',
    'deleteMyMessage',
  ];
  // ignore: non_constant_identifier_names
  static final List<String> FRIEND_MESSAGE_OPTIONS = [
    'copyFrMessage',
    'quoteMessage',
    'selectMyMessage',
    'reportFrMessage',
  ];
  // ignore: non_constant_identifier_names
  static final List<String> CHAT_OPTIONS = [
    'editNameInChat',
    'expertInforInChat',
    'markInChat',
    'notificationLowerCaseInChat',
    'deleteChatInChat',
  ];
  // Connection dialog
  static final String connectionFailed = 'connectionFailed';
  static final String connectAgain = 'connectAgain';

  // Download Dialog
  static final String downloadImageSuccess = 'downloadImageSuccess';
  static final String downloadImageFailed = 'downloadImageFailed';

  // FixBugs April

  static final String rateNow = 'rateNow';
  static final String justSendPictures = 'justSendPictures';
  static final String descriptionCancelBookingExpert =
      'descriptionCancelBookingExpert';


  static final String adviseDurationMustBeLessThan240m = 'adviseDurationMustBeLessThan240m';
  static final String adviseDurationMustBeLessThan4h = 'adviseDurationMustBeLessThan4h';
}

class MyI18n {
  static List<String> keys = vietnamese.keys.toList();

  static Map<String, Map<String, String>> getTranslation() {
    Map<String, Map<String, String>> value = {};
    keys.forEach((element) {
      value[element] = {
        'vi_vn': vietnamese[element]!,
        'en_us': english[element]!,
      };
    });

    return value;
  }
}

extension Localization on String {
  static final _t = Translations.from('vi_vn', MyI18n.getTranslation());

  String get i18n => localize(this, _t);

  String fill(List<Object> params) => localizeFill(this, params);

  String plural(int value) => localizePlural(value, this, _t);

  String version(Object modifier) => localizeVersion(modifier, this, _t);
}
