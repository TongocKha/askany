import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/payment_card/payment_card_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/helpers/input_formatter/bank_number_formatter.dart';
import 'package:askany/src/helpers/input_formatter/upper_case_formatter.dart';
import 'package:askany/src/helpers/utils/validator_utils.dart';
import 'package:askany/src/models/payment_card_model.dart';

import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_with_text_and_pop_button.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:askany/src/ui/common/widgets/pagination_list_view.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_form.dart';
import 'package:askany/src/ui/style/calendar_style.dart';
import 'package:askany/src/ui/style/wallet_style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_form_request.dart';
import 'package:flutter/services.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class WithdrawCreditCard extends StatefulWidget {
  final bool visible;
  final String amount;
  final GlobalKey<FormState> formKey;
  WithdrawCreditCard({
    Key? key,
    required this.visible,
    required this.amount,
    required this.formKey,
  }) : super(key: key);

  @override
  _WithdrawCreditCardState createState() => _WithdrawCreditCardState();
}

class _WithdrawCreditCardState extends State<WithdrawCreditCard> {
  TextEditingController acountNameController = TextEditingController();
  TextEditingController cardNumberController = TextEditingController();

  String? bankCode;
  PaymentCardModel? paymentCardModel;
  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: widget.visible,
      child: BlocBuilder<PaymentCardBloc, PaymentCardState>(
        builder: (context, state) {
          List<PaymentCardModel> paymentCardList =
              (state.props[0] as List).toList().cast();
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Visibility(
                visible: paymentCardList.isNotEmpty,
                child: Padding(
                  padding: EdgeInsets.only(
                    left: 17.sp,
                  ),
                  child: Text(
                    'Thẻ đã lưu',
                    style: walletTitleStyle13,
                  ),
                ),
              ),
              Visibility(
                visible: paymentCardList.isNotEmpty,
                child: Container(
                  height: 75.sp,
                  child: PaginationListView(
                    scrollDirection: Axis.horizontal,
                    itemCount: paymentCardList.length,
                    itemBuilder: (context, index) {
                      final String number = paymentCardList[index].cardNumber;
                      final result = number.replaceAllMapped(
                          RegExp(r".{4}"), (match) => "${match.group(0)}\t\t");
                      return paymentCardList[index].cardType == 0
                          ? TouchableOpacity(
                              onTap: () {
                                setState(() {
                                  paymentCardModel = paymentCardList[index];
                                  acountNameController.text =
                                      paymentCardList[index].accountName;
                                  cardNumberController.text = result;
                                  bankCode = paymentCardList[index].bankCode;
                                });
                              },
                              child: Row(
                                children: [
                                  SizedBox(
                                    width: 15.sp,
                                  ),
                                  Container(
                                    height: 75.sp,
                                    width: 190.sp,
                                    margin: EdgeInsets.only(
                                        right: 10, top: 10.sp, bottom: 10.sp),
                                    decoration: BoxDecoration(
                                        border: Border.all(color: Colors.grey),
                                        borderRadius:
                                            BorderRadius.circular(10.sp)),
                                    child: Row(
                                      children: [
                                        Container(
                                          padding: EdgeInsets.only(left: 5.sp),
                                          child: Image.asset(
                                            imageBank(
                                                bankName: paymentCardList[index]
                                                    .bankName),
                                            height: 36.sp,
                                            width: 36.sp,
                                            fit: BoxFit.fitWidth,
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 10.sp),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                paymentCardList[index]
                                                    .accountName,
                                                style: TextStyle(
                                                  color: colorGray1,
                                                  fontSize: 11.sp,
                                                  fontWeight: FontWeight.w600,
                                                ),
                                              ),
                                              SizedBox(
                                                height: 6.sp,
                                              ),
                                              Text(
                                                paymentCardList[index]
                                                    .cardNumber
                                                    .substring(
                                                        paymentCardList[index]
                                                                .cardNumber
                                                                .length -
                                                            4),
                                                style: TextStyle(
                                                  color: colorGray1,
                                                  fontSize: 11.sp,
                                                  fontWeight: FontWeight.w400,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            )
                          : SizedBox();
                    },
                    childShimmer: SizedBox(),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 16.sp),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      Strings.accountName.i18n,
                      style: walletTitleStyle13,
                    ),
                    TextFieldForm(
                      inputFormatters: [
                        UpperCaseFormatter(),
                      ],
                      controller: acountNameController,
                      validatorForm: (val) =>
                          val!.isEmpty ? 'Bạn chưa nhập chủ thẻ' : null,
                      hintText: Strings.accountName.i18n.toUpperCase(),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10.sp),
                      child: Text(
                        Strings.creditCardNumber.i18n,
                        style: walletTitleStyle13,
                      ),
                    ),
                    TextFieldFormRequest(
                      controller: cardNumberController,
                      validatorForm: (val) {
                        bool isValidId =
                            ValidatorUtils.isValidCardNumber(val ?? "");
                        if (!isValidId) return "Số tài khoản không hợp lệ";

                        if (cardNumberController.text.length <= 21 ||
                            cardNumberController.text.length >= 36) {
                          if (cardNumberController.text.length <= 21)
                            return 'Số thẻ phải lớn hơn 15 số';

                          if (cardNumberController.text.length >= 36)
                            return 'Số thẻ không đúng';
                        }
                        return null;
                      },
                      hintText: '1111 2222 3333 4444',
                      textInputType: TextInputType.number,
                      inputFormatters: [
                        FilteringTextInputFormatter.digitsOnly,
                        BankNumberFormatter(),
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10.sp),
                      child: Text(
                        Strings.supportedType.i18n,
                        style: walletTitleStyle13,
                      ),
                    ),
                    SizedBox(
                      height: 18.sp,
                    ),
                    Row(
                      children: [
                        TouchableOpacity(
                          onTap: () {
                            setState(() {
                              bankCode = bankCodes(bank: 0);
                            });
                          },
                          child: bankCode != 'VISA'
                              ? Container(
                                  height: 40.sp,
                                  width: 79.sp,
                                  margin: EdgeInsets.only(
                                    bottom: 4.sp,
                                    right: 20.sp,
                                  ),
                                  decoration: BoxDecoration(
                                      border: Border.all(color: Colors.grey),
                                      borderRadius:
                                          BorderRadius.circular(4.sp)),
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(
                                        vertical: 10.sp, horizontal: 9.sp),
                                    child: Image.asset(
                                      'assets/images/img_visa.png',
                                      height: 20.sp,
                                    ),
                                  ),
                                )
                              : Container(
                                  height: 40.sp,
                                  width: 79.sp,
                                  margin: EdgeInsets.only(
                                    bottom: 4.sp,
                                    right: 20.sp,
                                  ),
                                  decoration: BoxDecoration(
                                      border: Border.all(color: Colors.blue),
                                      borderRadius:
                                          BorderRadius.circular(4.sp)),
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(
                                        vertical: 10.sp, horizontal: 9.sp),
                                    child: Image.asset(
                                      'assets/images/img_visa.png',
                                      height: 20.sp,
                                    ),
                                  ),
                                ),
                        ),
                        TouchableOpacity(
                          onTap: () {
                            setState(() {
                              bankCode = bankCodes(bank: 1);
                            });
                          },
                          child: bankCode != 'Mastercard'
                              ? Container(
                                  height: 40.sp,
                                  width: 79.sp,
                                  margin: EdgeInsets.only(
                                    bottom: 4.sp,
                                    right: 20.sp,
                                  ),
                                  decoration: BoxDecoration(
                                      border: Border.all(color: Colors.grey),
                                      borderRadius:
                                          BorderRadius.circular(4.sp)),
                                  child: Padding(
                                    padding:
                                        EdgeInsets.symmetric(vertical: 4.sp),
                                    child: Image.asset(
                                      'assets/images/img_mastercard.png',
                                    ),
                                  ),
                                )
                              : Container(
                                  height: 40.sp,
                                  width: 79.sp,
                                  margin: EdgeInsets.only(
                                    bottom: 4.sp,
                                    right: 20.sp,
                                  ),
                                  decoration: BoxDecoration(
                                      border: Border.all(color: Colors.blue),
                                      borderRadius:
                                          BorderRadius.circular(4.sp)),
                                  child: Padding(
                                    padding:
                                        EdgeInsets.symmetric(vertical: 4.sp),
                                    child: Image.asset(
                                      'assets/images/img_mastercard.png',
                                    ),
                                  ),
                                ),
                        ),
                      ],
                    ),
                    SizedBox(height: 24.sp),
                    ButtonPrimary(
                      onPressed: () {
                        if (widget.formKey.currentState!.validate()) {
                          double amountFormatter =
                              double.parse(widget.amount.replaceAll(',', ''));
                          if (bankCode == null) {
                            dialogAnimationWrapper(
                              child: DialogWithTextAndPopButton(
                                bodyAfter: 'Bạn chưa chọn loại thẻ',
                              ),
                            );
                          } else if (amountFormatter >
                              (AppBloc.userBloc.getAccount.wallet ?? 0)) {
                            dialogAnimationWrapper(
                              child: DialogWithTextAndPopButton(
                                bodyAfter: 'Số tiền trong App của bạn không đủ',
                              ),
                            );
                          } else {
                            showDialogLoading();
                            AppBloc.paymentCard.add(
                              SavePayOutPaymentCardEvent(
                                amount: widget.amount,
                                type: Strings.creditCard.i18n,
                                bankName: bankCode!,
                                cardNumber: cardNumberController.text,
                                cardType: 0,
                                accountName: acountNameController.text,
                                bankCode: bankCode!,
                              ),
                            );
                          }
                        }
                      },
                      text: Strings.nextStep.i18n,
                    ),
                  ],
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
