import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:askany/src/ui/wallet/widgets/payment_card_list.dart';
import 'package:flutter/material.dart';

class SaveCreditCard extends StatefulWidget {
  const SaveCreditCard({Key? key}) : super(key: key);

  @override
  _SaveCreditCardState createState() => _SaveCreditCardState();
}

class _SaveCreditCardState extends State<SaveCreditCard> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarTitleBack(
        context,
        Strings.rememberCreditCardAppbartext.i18n,
        brightness: Brightness.light,
      ),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            dividerChat,
            Container(
              margin: EdgeInsets.only(left: 17.sp, right: 16.sp, bottom: 20.sp, top: 20.sp),
              child: Text(
                Strings.rememberCreditCardInforSuggestion.i18n,
                style: TextStyle(fontSize: 13.sp, fontWeight: FontWeight.w400),
              ),
            ),
            TouchableOpacity(
                onTap: () => AppNavigator.push(Routes.ADD_INFO_CARD_PAYMENT),
                child: Container(
                  margin: EdgeInsets.only(left: 13.sp, right: 19.sp),
                  decoration: BoxDecoration(
                    color: colorAddButton,
                    borderRadius: BorderRadius.circular(8.sp),
                  ),
                  padding: EdgeInsets.only(left: 14.sp, top: 14.sp, bottom: 14.sp),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Image.asset(
                              iconAddCreditCard,
                              width: 40.sp,
                              height: 40.sp,
                            ),
                            SizedBox(
                              width: 10.sp,
                            ),
                            Text(
                              Strings.addCreditInforAppbartext.i18n,
                              style: TextStyle(color: colorGreen2, fontSize: 12.sp),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(10.sp),
                        child: Image.asset(
                          iconBackArrowGreen,
                          width: 15.sp,
                          height: 15.sp,
                        ),
                      ),
                    ],
                  ),
                )),
            Container(
              // color: Colors.amber,
              margin: EdgeInsets.only(left: 16.sp, bottom: 16.sp, top: 27.sp),
              child: Text(
                Strings.savedCards.i18n,
                style: TextStyle(fontSize: 14.sp, fontWeight: FontWeight.w600),
              ),
            ),
            Expanded(child: PaymentCardList()),
          ],
        ),
      ),
    );
  }
}
