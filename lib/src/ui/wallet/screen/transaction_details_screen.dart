import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/models/transaction_model.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:askany/src/ui/style/wallet_style.dart';
import 'package:askany/src/ui/wallet/widgets/transaction_details_information.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:askany/src/helpers/extentions/string_extention.dart';

class TransactionDetails extends StatelessWidget {
  final TransactionModel transaction;
  const TransactionDetails({Key? key, required this.transaction})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double serviceCharge = 0;
    double totalMoney = transaction.amount - serviceCharge;
    return Scaffold(
      appBar:
          appBarTitleBack(context, Strings.transactionDetailsAppbartext.i18n),
      body: Container(
        color: backgroundDetails,
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.symmetric(vertical: 10.sp),
              height: 93.sp,
              width: MediaQuery.of(context).size.width,
              color: Colors.white,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    getTextTransaction(transaction.status),
                    style: TextStyle(
                      fontSize: 13.sp,
                      fontWeight: FontWeight.w600,
                      color: getColorTransaction(transaction.status),
                    ),
                  ),
                  SizedBox(
                    height: 10.sp,
                  ),
                  Text(
                    getTextDesciption(transaction.type),
                    style: TextStyle(
                      color: colorGray1,
                      fontSize: 13.sp,
                      fontWeight: FontWeight.w400,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
            Expanded(
              child: Container(
                color: Colors.white,
                padding: EdgeInsets.symmetric(
                  horizontal: 18.sp,
                  vertical: 12.sp,
                ),
                child: Column(
                  children: [
                    TransactionDetailsInformation(
                      content: Strings.time.i18n,
                      value: DateFormat('dd/MM/yyyy')
                          .format(transaction.createdAt),
                    ),
                    TransactionDetailsInformation(
                      content: Strings.transAmount.i18n,
                      value: transaction.costString,
                    ),
                    SizedBox(
                      height: 20.sp,
                      child: dividerChat,
                    ),
                    Visibility(
                      visible: [ADMIN_SEND_MONEY_EXPERT, ADMIN_SEND_MONEY_USER]
                          .contains(transaction.type),
                      child: Column(
                        children: [
                          TransactionDetailsInformation(
                            content: Strings.transFrom.i18n,
                            value: 'Admin',
                          ),
                          TransactionDetailsInformation(
                            content: Strings.transTo.i18n,
                            value: (transaction.expert?.fullname ??
                                    transaction.user?.fullname) ??
                                'UNKNOWN',
                          ),
                        ],
                      ),
                    ),
                    TransactionDetailsInformation(
                        content: Strings.paymentMethod.i18n,
                        value: getTextDesciption(transaction.type)),
                    TransactionDetailsInformation(
                      content: Strings.transFee.i18n,
                      value:
                          '${serviceCharge.toStringAsFixed(0).formatMoney()}${Strings.currency.i18n}',
                    ),
                    SizedBox(
                      height: 20.sp,
                      child: dividerChat,
                    ),
                    TransactionDetailsInformation(
                      content: Strings.totalPrice.i18n,
                      value:
                          '${totalMoney.toStringAsFixed(0).formatMoney()}${Strings.currency.i18n}',
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
