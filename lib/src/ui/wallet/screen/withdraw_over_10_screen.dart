import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/ui/wallet/widgets/confirmation_withdraw.dart';
import 'package:flutter/material.dart';

class WithdrawOver10 extends StatelessWidget {
  final String amount;
  final String type;
  final String id;

  const WithdrawOver10({
    Key? key,
    required this.id,
    required this.amount,
    required this.type,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ConfirmationWithdraw(
      id: id,
      titleDescription: Strings.withdrawVerifyNoti.i18n,
      titleImage1: Strings.uploadPhoto.i18n,
      titleImage2: Strings.uploadPhoto.i18n,
      amount: amount,
      type: type,
    );
  }
}
