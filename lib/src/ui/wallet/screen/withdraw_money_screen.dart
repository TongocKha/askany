import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/style/wallet_style.dart';
import 'package:askany/src/ui/wallet/widgets/choose_withdraw_money.dart';
import 'package:askany/src/ui/wallet/widgets/money_in_wallet.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class WithdrawMoney extends StatefulWidget {
  const WithdrawMoney({Key? key}) : super(key: key);

  @override
  State<WithdrawMoney> createState() => _WithdrawMoneyState();
}

class _WithdrawMoneyState extends State<WithdrawMoney> {
  int selectIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: appBarTitleBack(context, Strings.payOutAppbartext.i18n),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 16.sp, vertical: 18.sp),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                MoneyInWallet(),
                Padding(
                  padding: const EdgeInsets.only(
                    top: 18,
                    bottom: 28,
                  ),
                  child: Text(
                    Strings.payOutProcessingTimeNoti.i18n,
                    style: walletTitleStyleW400,
                  ),
                ),
                Text(
                  Strings.receiveMoneyMethod.i18n,
                  style: walletTitleStyle13,
                ),
                SizedBox(height: 4.sp),
                ChooseWithdrawMoney(
                  text: Strings.atmNumber.i18n,
                  index: TOTHECARD,
                  selectedIndex: selectIndex,
                  handlePressed: () {
                    setState(() {
                      selectIndex = TOTHECARD;
                    });
                  },
                ),
                ChooseWithdrawMoney(
                  text: Strings.bankAccount.i18n,
                  index: TOTHEACCOUNT,
                  selectedIndex: selectIndex,
                  handlePressed: () {
                    setState(() {
                      selectIndex = TOTHEACCOUNT;
                    });
                  },
                ),
              ],
            ),
            ButtonPrimary(
              onPressed: () => selectIndex == TOTHECARD
                  ? AppNavigator.push(Routes.WITHDRAWMONEYTOTHECARD)
                  : AppNavigator.push(Routes.WITHDRAWMONEYTOTHEACCOUNT),
              text: Strings.nextStep.i18n,
            )
          ],
        ),
      ),
    );
  }
}
