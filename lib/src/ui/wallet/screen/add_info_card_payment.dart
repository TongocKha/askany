import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/chat/widgets/search_box.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/account_style.dart';
import 'package:askany/src/ui/style/calendar_style.dart';
import 'package:askany/src/ui/style/wallet_style.dart';
import 'package:askany/src/ui/wallet/widgets/box_credit_card.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class AddInfoCardPayment extends StatefulWidget {
  const AddInfoCardPayment({
    Key? key,
  }) : super(key: key);

  @override
  State<AddInfoCardPayment> createState() => _AddInfoCardPaymentState();
}

class _AddInfoCardPaymentState extends State<AddInfoCardPayment> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarTitleBack(
        context,
        Strings.addCreditInforAppbartext.i18n,
        brightness: Brightness.light,
      ),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(top: 29.sp, bottom: 18.sp),
              child: SearchBox(),
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                          left: 16.sp, right: 16.sp, top: 10.sp),
                      child: Text(
                        Strings.international.i18n,
                        style: TextStyle(
                            fontSize: 13.sp,
                            fontWeight: FontWeight.w600,
                            color: colorBlack2),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 10.sp, bottom: 10.sp),
                      child: Row(
                        children: [
                          TouchableOpacity(
                            onTap: () => AppNavigator.push(
                              Routes.ADD_DETAIL_CARD_PAYMENT,
                              arguments: {
                                'nameCreditCard': "VISA",
                                'imageCreditCard': iconVisaLogo,
                                'isCreating': true,
                              },
                            ),
                            child: Container(
                              width: 90.sp,
                              height: 85.sp,
                              child: BoxCreditCard(
                                imageCreditCard: iconVisaLogo,
                                nameCreditCard: "VISA",
                                fontSize: 11.sp,
                                marginBottomImage: 50,
                                marginBoxCreditCard:
                                    EdgeInsets.only(top: 8.sp, left: 4.sp),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 4.sp,
                          ),
                          TouchableOpacity(
                            onTap: () => AppNavigator.push(
                              Routes.ADD_DETAIL_CARD_PAYMENT,
                              arguments: {
                                'nameCreditCard': "Mastercard",
                                'imageCreditCard': iconMasterCardLogo,
                                'isCreating': true,
                              },
                            ),
                            child: Container(
                              width: 90.sp,
                              height: 85.sp,
                              child: BoxCreditCard(
                                imageCreditCard: iconMasterCardLogo,
                                nameCreditCard: "Mastercard",
                                fontSize: 11.sp,
                                marginBottomImage: 50,
                                marginBoxCreditCard:
                                    EdgeInsets.only(top: 8.sp, left: 4.sp),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                        left: 16.sp,
                        right: 16.sp,
                        top: 10.sp,
                        bottom: 10.sp,
                      ),
                      child: Text(
                        Strings.nationalATM.i18n,
                        style: TextStyle(
                          fontSize: 13.sp,
                          fontWeight: FontWeight.w600,
                          color: colorBlack2,
                        ),
                      ),
                    ),
                    GridView.count(
                      physics: BouncingScrollPhysics(),
                      padding: EdgeInsets.only(
                        left: 12.sp,
                        right: 16.sp,
                      ),
                      crossAxisSpacing: 4.sp,
                      childAspectRatio: 1,
                      crossAxisCount: 3.itemCountGridViewMoney,
                      shrinkWrap: true,
                      children: List.generate(
                        banks.length,
                        (index) {
                          return TouchableOpacity(
                            onTap: () => AppNavigator.push(
                              Routes.ADD_DETAIL_CARD_PAYMENT,
                              arguments: {
                                'nameCreditCard': banks[index]['bank_code'],
                                'imageCreditCard': banks[index]['image'],
                                'isCreating': true,
                              },
                            ),
                            child: BoxCreditCard(
                              imageCreditCard: banks[index]['image'] ?? '',
                              nameCreditCard: banks[index]['bank_code'] ?? '',
                              fontSize: 11.sp,
                              marginBottomImage: 50,
                              marginBoxCreditCard:
                                  EdgeInsets.only(top: 8.sp, left: 4.sp),
                            ),
                          );
                        },
                      ),
                    ),
                    SizedBox(
                      height: 20.sp,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
