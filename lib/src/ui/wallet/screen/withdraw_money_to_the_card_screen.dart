import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/helpers/currency_helper.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_form_request.dart';
import 'package:askany/src/ui/request/widgets/request_dropdown_button.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:askany/src/ui/style/wallet_style.dart';
import 'package:askany/src/ui/wallet/screen/withdraw_credit_card_screen.dart';
import 'package:askany/src/ui/wallet/screen/withdraw_atm_card_screen.dart';
import 'package:askany/src/ui/wallet/widgets/money_in_wallet.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class WithdrawMoneyToTheCard extends StatefulWidget {
  const WithdrawMoneyToTheCard({Key? key}) : super(key: key);

  @override
  _WithdrawMoneyToTheCardState createState() => _WithdrawMoneyToTheCardState();
}

class _WithdrawMoneyToTheCardState extends State<WithdrawMoneyToTheCard> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String? typeCard;
  final _controllerWithdraw = TextEditingController();
  String _currency = CURRENCY_VND;
  void changeAmount(val) {
    setState(() {
      val = _controllerWithdraw;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: appBarTitleBack(
        context,
        Strings.withdrawToCard.i18n,
      ),
      body: Container(
        height: 100.h,
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                  physics: BouncingScrollPhysics(),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 16.sp),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 18.sp,
                            ),
                            MoneyInWallet(),
                            Padding(
                              padding: EdgeInsets.only(
                                top: 22.sp,
                              ),
                              child: Text(
                                Strings.withdrawAmount.i18n,
                                style: walletTitleStyle13,
                              ),
                            ),
                            TextFieldFormRequest(
                              validatorForm: (val) => val!.isEmpty ||
                                      double.tryParse(
                                              val.trim().replaceAll(',', '')) ==
                                          null
                                  ? Strings
                                      .withdrawAmountCannotBeLeftblankErrortext
                                      .i18n
                                  : double.parse(
                                              val.trim().replaceAll(',', '')) <
                                          50000
                                      ? Strings
                                          .minimumWithdrawAmountIs2bucks.i18n
                                      : null,
                              hintText: Strings.transAmount.i18n,
                              controller: _controllerWithdraw,
                              onChanged: changeAmount,
                              textInputType: TextInputType.number,
                              inputFormatters: _currency == CURRENCY_VND
                                  ? CurrencyHelper.vndInputFormat
                                  : CurrencyHelper.usdInputFormat,
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                top: 28.sp,
                                bottom: 18.sp,
                              ),
                              child: Text(
                                Strings.receivingAccountInfor.i18n,
                                style: walletTitleStyle14,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(bottom: 10.0),
                              child: Text(
                                Strings.cardType.i18n,
                                style: walletTitleStyle13,
                              ),
                            ),
                            RequestDropdownButton(
                              hintText: Strings.cardTypeHintext.i18n,
                              items: cards,
                              onChanged: (value) {
                                setState(() {
                                  if (value == cards[0]) {
                                    typeCard = cards[0];
                                  } else if (value == cards[1]) {
                                    typeCard = cards[1];
                                  }
                                });
                              },
                              validatorDropDown: (val) => typeCard == null
                                  ? Strings
                                      .cardTypeCannotBeLeftBlankErrortext.i18n
                                  : null,
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 20.sp),
                        child: dividerChat,
                      ),
                      WithdrawCreditCard(
                        visible: typeCard == cards[0],
                        amount: _controllerWithdraw.text,
                        formKey: _formKey,
                      ),
                      WithdrawATMCard(
                        visible: typeCard == cards[1],
                        amount: _controllerWithdraw.text,
                        formKey: _formKey,
                      ),
                      SizedBox(
                        height: typeCard != null ? 10.sp : 0,
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 16.sp),
                margin: EdgeInsets.only(bottom: 18.sp),
                child: Visibility(
                  visible: typeCard == null,
                  child: ButtonPrimary(
                    onPressed: () {
                      if (_formKey.currentState!.validate()) {}
                    },
                    text: Strings.nextStep.i18n,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
