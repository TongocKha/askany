import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/payment_card/payment_card_bloc.dart';
import 'package:askany/src/helpers/input_formatter/bank_number_formatter.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/helpers/input_formatter/upper_case_formatter.dart';
import 'package:askany/src/helpers/utils/validator_utils.dart';
import 'package:askany/src/models/payment_card_model.dart';
import 'package:askany/src/models/slide_mode.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/account/widgets/text_title.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_confirm_cancel.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_form.dart';
import 'package:askany/src/ui/request/widgets/request_dropdown_button.dart';
import 'package:askany/src/ui/style/calendar_style.dart';
import 'package:askany/src/ui/style/wallet_style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:flutter/services.dart';

class AddDetailCardPayment extends StatefulWidget {
  final PaymentCardModel? paymentCardModel;
  final String? nameCreditCard;
  final String? imageCreditCard;
  const AddDetailCardPayment({
    this.nameCreditCard,
    this.imageCreditCard,
    this.paymentCardModel,
  });

  @override
  State<AddDetailCardPayment> createState() => _AddDetailCardPaymentState();
}

class _AddDetailCardPaymentState extends State<AddDetailCardPayment> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  late bool _isCreating = widget.paymentCardModel == null;

  TextEditingController accountNameController = TextEditingController();
  TextEditingController cardNumberController = TextEditingController();
  TextEditingController billAddressController = TextEditingController();
  TextEditingController cityController = TextEditingController();
  TextEditingController postOfficeCodeController = TextEditingController();
  List<String> specialtiesTitle = [
    Strings.specialtiesTitle[0].i18n,
  ];
  String? country = 'Việt Nam';
  bool checkEditCardPayment() {
    return widget.nameCreditCard == 'VISA' ||
        widget.nameCreditCard == "Mastercard" ||
        widget.paymentCardModel?.bankName == 'VISA' ||
        widget.paymentCardModel?.bankName == "Mastercard";
  }

  @override
  void initState() {
    super.initState();
    final String number = widget.paymentCardModel?.cardNumber ?? '';
    final value = number.replaceAllMapped(
        RegExp(r".{4}"), (match) => "${match.group(0)}\t\t");

    String? result = value;
    // Update => Initial value
    if (widget.paymentCardModel != null) {
      accountNameController.text = widget.paymentCardModel!.accountName;
      cardNumberController.text = result;
      cityController.text = widget.paymentCardModel!.city ?? '';
      billAddressController.text = widget.paymentCardModel!.billAddress ?? '';
      postOfficeCodeController.text =
          widget.paymentCardModel!.postOfficeCode?.toString() ?? '';
      country = widget.paymentCardModel!.country ?? '';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarTitleBack(
        context,
        _isCreating
            ? '${Strings.addCardP1.i18n} ${widget.nameCreditCard!}'
            : Strings.editPaymentCard.i18n,
        // backgroundColor: headerCalendarColor,
        brightness: Brightness.light,
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(left: 16.sp, right: 16.sp, top: 10.sp),
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  // mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        color: colorGreen2,
                        border: Border.all(
                          color: colorGray2, // Set border color
                          width: 0.5.sp,
                        ),
                        borderRadius: BorderRadius.circular(8.sp),
                      ),
                      padding: EdgeInsets.all(10.sp),
                      child: Row(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(8.0),
                            child: Container(
                              color: Colors.white,
                              padding: EdgeInsets.all(2.sp),
                              child: Image.asset(
                                widget.imageCreditCard ??
                                    imageBank(
                                      bankName:
                                          widget.paymentCardModel!.bankName,
                                    ),
                                width: 50.sp,
                                height: 50.sp,
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 10.sp),
                            child: Text(
                              _isCreating
                                  ? '${Strings.cardP1.i18n}${widget.nameCreditCard}'
                                  : '${Strings.cardP1.i18n}${widget.paymentCardModel?.bankName}',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 12.sp,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 28.sp,
                    ),
                    TextTitle(textTitle: Strings.accountName.i18n),
                    TextFieldForm(
                      controller: accountNameController,
                      inputFormatters: [
                        UpperCaseFormatter(),
                      ],
                      fontSize: 13.sp,
                      contentPadding: EdgeInsets.all(10.sp),
                      hintText: _isCreating == true
                          ? Strings.accountNameHintext.i18n
                          : null,
                      textInputAction: TextInputAction.go,
                      validatorForm: (val) => val!.isEmpty ||
                              val.trim().length == 0
                          ? Strings.accountNameCannotBeLeftBlankErrortext.i18n
                          : null,
                    ),
                    SizedBox(
                      height: 22.sp,
                    ),
                    Visibility(
                      visible: checkEditCardPayment(),
                      child: TextTitle(
                        textTitle: Strings.invoiceAddress.i18n,
                      ),
                    ),
                    Visibility(
                      visible: checkEditCardPayment(),
                      child: TextFieldForm(
                        controller: billAddressController,
                        fontSize: 13.sp,
                        contentPadding: EdgeInsets.all(10.sp),
                        hintText: Strings.invoiceAddressHintext.i18n,
                        textInputAction: TextInputAction.go,
                        validatorForm: (val) =>
                            val!.isEmpty || val.trim().length == 0
                                ? Strings
                                    .invoiceAddressCannotBeLeftBlankErrortext
                                    .i18n
                                : null,
                      ),
                    ),
                    Visibility(
                      visible: checkEditCardPayment(),
                      child: SizedBox(
                        height: 22.sp,
                      ),
                    ),
                    Visibility(
                        visible: checkEditCardPayment(),
                        child: TextTitle(textTitle: Strings.city.i18n)),
                    Visibility(
                        visible: checkEditCardPayment(),
                        child: TextFieldForm(
                          controller: cityController,
                          fontSize: 13.sp,
                          contentPadding: EdgeInsets.all(10.sp),
                          hintText: Strings.cityHintext.i18n,
                          textInputAction: TextInputAction.go,
                          validatorForm: (val) =>
                              val!.isEmpty || val.trim().length == 0
                                  ? Strings.cityCannotBeLeftBlankErrortext.i18n
                                  : null,
                        )),
                    Visibility(
                      visible: checkEditCardPayment(),
                      child: SizedBox(
                        height: 22.sp,
                      ),
                    ),
                    Visibility(
                      visible: checkEditCardPayment(),
                      child: TextTitle(
                        textTitle: Strings.zipCode.i18n,
                      ),
                    ),
                    Visibility(
                        visible: checkEditCardPayment(),
                        child: TextFieldForm(
                          textInputType: TextInputType.number,
                          controller: postOfficeCodeController,
                          fontSize: 13.sp,
                          contentPadding: EdgeInsets.all(10.sp),
                          hintText: Strings.zipCodeHintext.i18n,
                          textInputAction: TextInputAction.go,
                          validatorForm: (val) => val!.isEmpty ||
                                  val.trim().length == 0
                              ? Strings.zipCodeCannotBeLeftBlankErrortext.i18n
                              : null,
                        )),
                    Visibility(
                      visible: checkEditCardPayment(),
                      child: SizedBox(
                        height: 22.sp,
                      ),
                    ),
                    Visibility(
                      visible: checkEditCardPayment(),
                      child: TextTitle(
                        textTitle: Strings.country.i18n,
                      ),
                    ),
                    Visibility(
                      visible: checkEditCardPayment(),
                      child: SizedBox(
                        height: 10.sp,
                      ),
                    ),
                    Visibility(
                      visible: checkEditCardPayment(),
                      child: RequestDropdownButton(
                        hintText: 'Việt Nam',
                        onChanged: (val) {
                          setState(() {
                            country = val!.trim();
                          });
                        },
                        items: specialtiesTitle,
                        validatorDropDown: (val) => (_isCreating && val == null)
                            ? Strings.countryCannotBeLeftBlankErrortext.i18n
                            : null,
                      ),
                    ),
                    Visibility(
                      visible: checkEditCardPayment(),
                      child: SizedBox(
                        height: 22.sp,
                      ),
                    ),
                    Visibility(
                      visible: checkEditCardPayment(),
                      child: TextTitle(
                        textTitle: Strings.creditCardInformation.i18n,
                      ),
                    ),
                    Visibility(
                      visible: checkEditCardPayment(),
                      child: SizedBox(
                        height: 22.sp,
                      ),
                    ),
                    TextTitle(textTitle: "Số thẻ"),
                    TextFieldForm(
                      controller: cardNumberController,
                      fontSize: 13.sp,
                      contentPadding: EdgeInsets.all(10.sp),
                      hintText: checkEditCardPayment()
                          ? '5555 5555 5555 5555'
                          : Strings.creditCardNumberHintext.i18n,
                      inputFormatters: [
                        FilteringTextInputFormatter.digitsOnly,
                        BankNumberFormatter(),
                      ],
                      textInputAction: TextInputAction.go,
                      textInputType: TextInputType.number,
                      validatorForm: (val) {
                        if (widget.nameCreditCard! == "VISA" ||
                            widget.nameCreditCard! == "Mastercard") {
                          bool isValidId =
                              ValidatorUtils.isValidCardNumber(val ?? "");
                          if (!isValidId) return "Số tài khoản không hợp lệ";
                        }

                        if (cardNumberController.text.length <= 21 ||
                            cardNumberController.text.length >= 36) {
                          if (cardNumberController.text.length <= 21)
                            return 'Số thẻ phải lớn hơn 15 số';

                          if (cardNumberController.text.length >= 36)
                            return 'Số thẻ không đúng';
                        }
                        return null;
                      },
                    ),
                  ],
                ),
                SizedBox(height: 24.sp),
                Container(
                    child: ButtonPrimary(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      showDialogLoading();
                      if (_isCreating) {
                        AppBloc.paymentCard.add(
                          SavePaymentCardEvent(
                            bankName: widget.nameCreditCard!,
                            cardNumber: cardNumberController.text,
                            cardType: checkEditCardPayment() ? 0 : 1,
                            accountName: accountNameController.text,
                            bankCode: widget.nameCreditCard!,
                            country: country,
                            billAddress: billAddressController.text,
                            postOfficeCode: postOfficeCodeController.text,
                            city: cityController.text,
                          ),
                        );
                        AppNavigator.popUntil(Routes.ADD_DETAIL_CARD_PAYMENT);
                      } else {
                        AppBloc.paymentCard.add(
                          UpdatePaymentCardEvent(
                            id: widget.paymentCardModel!.id,
                            bankName: widget.nameCreditCard!,
                            cardNumber: cardNumberController.text,
                            cardType: checkEditCardPayment() ? 0 : 1,
                            accountName: accountNameController.text,
                            bankCode: widget.nameCreditCard!,
                            country: country,
                            billAddress: billAddressController.text,
                            postOfficeCode: postOfficeCodeController.text,
                            city: cityController.text,
                          ),
                        );
                      }
                    }
                  },
                  text: Strings.save.i18n,
                )),
                Visibility(
                    visible: _isCreating != true,
                    child: TouchableOpacity(
                      onTap: () {
                        dialogAnimationWrapper(
                          slideFrom: SlideMode.bot,
                          borderRadius: 8.5.sp,
                          paddingHorizontal: 18.sp,
                          child: DialogConfirmCancel(
                            title: Strings.deleteThisCreditCard.i18n,
                            bodyBefore:
                                Strings.areYouSureToDeleteThisCreditCard.i18n,
                            bodyColor: colorBlack2,
                            cancelText: Strings.cancel.i18n,
                            onConfirmed: () {
                              AppNavigator.pop();
                              showDialogLoading();
                              AppBloc.paymentCard.add(
                                DeletePaymentCard(
                                  id: widget.paymentCardModel!.id,
                                ),
                              );
                            },
                          ),
                        );
                      },
                      child: Container(
                        width: 100.w,
                        height: 40.sp,
                        margin: EdgeInsets.symmetric(
                          vertical: 6.sp,
                          horizontal: 16.sp,
                        ),
                        padding: EdgeInsets.symmetric(vertical: 8.sp),
                        child: Center(
                          child: Text(
                            Strings.deleteThisCreditCard.i18n,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: colorGray1,
                              fontSize: 13.sp,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                      ),
                    )),
                Visibility(
                  visible: checkEditCardPayment(),
                  child: SizedBox(
                    height: 18.sp,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
