import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/payment_card/payment_card_bloc.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/helpers/currency_helper.dart';
import 'package:askany/src/helpers/input_formatter/bank_number_formatter.dart';
import 'package:askany/src/helpers/input_formatter/upper_case_formatter.dart';
import 'package:askany/src/helpers/utils/validator_utils.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_with_text_and_pop_button.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_form.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_form_request.dart';
import 'package:askany/src/ui/request/widgets/request_dropdown_button.dart';
import 'package:askany/src/ui/style/wallet_style.dart';
import 'package:askany/src/ui/wallet/widgets/money_in_wallet.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:flutter/services.dart';

class WithDrawMoneyToTheAccount extends StatefulWidget {
  const WithDrawMoneyToTheAccount({Key? key}) : super(key: key);

  @override
  _WithDrawMoneyToTheAccountState createState() =>
      _WithDrawMoneyToTheAccountState();
}

class _WithDrawMoneyToTheAccountState extends State<WithDrawMoneyToTheAccount> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController acountNameController = TextEditingController();
  TextEditingController cardNumberController = TextEditingController();
  String? typeCard;
  TextEditingController _controllerWithdraw = TextEditingController();
  String _currency = CURRENCY_VND;
  String? bankName;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarTitleBack(
        context,
        'Rút tiền về tài khoản ngân hàng',
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16.sp, vertical: 18.sp),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  MoneyInWallet(),
                  Padding(
                    padding: EdgeInsets.only(
                      top: 22.sp,
                    ),
                    child: Text('Số tiền cần rút', style: walletTitleStyle13),
                  ),
                  TextFieldFormRequest(
                    validatorForm: (val) => val!.isEmpty ||
                            double.tryParse(val.trim().replaceAll(',', '')) ==
                                null
                        ? 'Bạn chưa nhập số tiền của bạn'
                        : double.parse(val.trim().replaceAll(',', '')) < 50000
                            ? "Số tiền tối thiểu là 50.000đ"
                            : null,
                    hintText: 'Số tiền',
                    controller: _controllerWithdraw,
                    textInputType: TextInputType.number,
                    inputFormatters: _currency == CURRENCY_VND
                        ? CurrencyHelper.vndInputFormat
                        : CurrencyHelper.usdInputFormat,
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                      top: 22.sp,
                    ),
                    child: Text(
                      'Thông tin tài khoản nhận tiền',
                      style: walletTitleStyle13,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                      top: 18.sp,
                      bottom: 10.sp,
                    ),
                    child: Text(
                      'Tên ngân hàng',
                      style: walletTitleStyle13,
                    ),
                  ),
                  RequestDropdownButton(
                    validatorDropDown: (val) =>
                        bankName == null ? 'Bạn chưa chọn ngân hàng' : null,
                    hintText: bankName ?? 'Tên ngân hàng',
                    items: bankCode,
                    onChanged: (value) {
                      setState(() {
                        bankName = value!.trim();
                      });
                    },
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                      top: 18.sp,
                      bottom: 10.sp,
                    ),
                    child: Text(
                      'Tên chủ tài khoản',
                      style: walletTitleStyle13,
                    ),
                  ),
                  TextFieldForm(
                    inputFormatters: [
                      UpperCaseFormatter(),
                    ],
                    controller: acountNameController,
                    validatorForm: (val) =>
                        val!.isEmpty ? 'Bạn chưa nhập tên chủ tài khoản' : null,
                    hintText: 'Tên chủ tài khoản',
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                      top: 18.sp,
                      bottom: 10.sp,
                    ),
                    child: Text(
                      'Số tài khoản',
                      style: walletTitleStyle13,
                    ),
                  ),
                  TextFieldFormRequest(
                    controller: cardNumberController,
                    validatorForm: (val) {
                      if (bankName == "VISA" || bankName == "Mastercard") {
                        bool isValidId =
                            ValidatorUtils.isValidCardNumber(val ?? "");
                        if (!isValidId) return "Số tài khoản không hợp lệ";
                      }
                      if (cardNumberController.text.length <= 21 ||
                          cardNumberController.text.length >= 36) {
                        if (cardNumberController.text.length <= 21)
                          return 'Số thẻ phải lớn hơn 15 số';

                        if (cardNumberController.text.length >= 36)
                          return 'Số thẻ không đúng';
                      }
                      return null;
                    },
                    hintText: 'Số tài khoản',
                    textInputType: TextInputType.number,
                    inputFormatters: [
                      FilteringTextInputFormatter.digitsOnly,
                      BankNumberFormatter(),
                    ],
                  ),
                  SizedBox(
                    height: 44.sp,
                  ),
                  ButtonPrimary(
                    onPressed: () {
                      if (_formKey.currentState!.validate()) {
                        double amountFormatter = double.parse(
                            _controllerWithdraw.text.replaceAll(',', ''));
                        if (amountFormatter >
                            (AppBloc.userBloc.getAccount.wallet ?? 0)) {
                          dialogAnimationWrapper(
                            child: DialogWithTextAndPopButton(
                              bodyAfter: 'Số tiền trong App của bạn không đủ',
                            ),
                          );
                        } else {
                          showDialogLoading();
                          AppBloc.paymentCard.add(
                            SavePayOutPaymentCardEvent(
                              amount: _controllerWithdraw.text,
                              type: 'Tài khoản ngân\nhàng',
                              bankName: bankName!,
                              cardNumber: cardNumberController.text,
                              cardType: (bankName == 'VISA' ||
                                      bankName == 'Mastercard')
                                  ? 0
                                  : 1,
                              accountName: acountNameController.text,
                              bankCode: bankName!,
                            ),
                          );
                        }
                      }
                    },
                    text: 'Tiếp tục',
                  )
                ],
              )),
        ),
      ),
    );
  }
}
