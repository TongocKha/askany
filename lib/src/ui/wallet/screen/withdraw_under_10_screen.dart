import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/ui/wallet/widgets/confirmation_withdraw.dart';
import 'package:flutter/material.dart';

class WithdrawUnder10 extends StatelessWidget {
  final String amount;
  final String type;
  final String id;

  const WithdrawUnder10({
    Key? key,
    required this.id,
    required this.amount,
    required this.type,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ConfirmationWithdraw(
      id: id,
      titleDescription: Strings.withdrawVerifyIDNoti.i18n,
      titleImage1: Strings.frontOfId.i18n,
      titleImage2: Strings.backsideOfId.i18n,
      amount: amount,
      type: type,
    );
  }
}
