import 'package:askany/src/bloc/transaction/transaction_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/style/calendar_style.dart';
import 'package:askany/src/ui/wallet/widgets/transaction_list.dart';
import 'package:askany/src/ui/wallet/widgets/transaction_shimmer_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TransactionHistory extends StatefulWidget {
  const TransactionHistory({
    Key? key,
  }) : super(key: key);

  @override
  _TransactionHistoryState createState() => _TransactionHistoryState();
}

class _TransactionHistoryState extends State<TransactionHistory> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarTitleBack(
        context,
        Strings.transHistoryAppbartext.i18n,
      ),
      body: BlocBuilder<TransactionBloc, TransactionState>(
        builder: (context, state) {
          return Container(
            color: backgroundDetails,
            child: state is TransactionInitial
                ? TransactionShimmerList()
                : TransactionList(
                    isShowDate: true,
                  ),
          );
        },
      ),
    );
  }
}
