import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/ui/style/chat_style.dart';
import 'package:askany/src/ui/wallet/widgets/transaction_history_date.dart';
import 'package:askany/src/ui/wallet/widgets/transaction_shimmer_card.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class TransactionShimmerList extends StatelessWidget {
  final bool isShowDate;
  const TransactionShimmerList({
    this.isShowDate = false,
  });

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      itemCount: LIMIT_API_5,
      itemBuilder: (context, index) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          isShowDate &&
                  (index == 0 ||
                      AppBloc.transactionBloc.transactions[index].createdAt.month !=
                          AppBloc.transactionBloc.transactions[index - 1].createdAt.month)
              ? TransactionHistoryDate(
                  transaction: AppBloc.transactionBloc.transactions[index].createdAt,
                )
              : Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16.sp),
                  child: dividerChat,
                ),
          TransactionShimmerCard()
        ],
      ),
    );
  }
}
