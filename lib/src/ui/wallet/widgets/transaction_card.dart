import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/models/transaction_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:askany/src/ui/style/wallet_style.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class TransactionCard extends StatelessWidget {
  final TransactionModel transaction;
  const TransactionCard({required this.transaction});
  @override
  Widget build(BuildContext context) {
    return TouchableOpacity(
      onTap: () => AppNavigator.push(
        Routes.TRANSACTION_DETAILS,
        arguments: {
          'transaction': transaction,
        },
      ),
      child: Container(
        color: Colors.white,
        padding: EdgeInsets.symmetric(
          vertical: 14.sp,
          horizontal: 16.sp,
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    Strings.receivedMoneyFrom.i18n,
                    style: TextStyle(
                      fontSize: 12.sp,
                      color: colorBlack1,
                    ),
                  ),
                  SizedBox(height: 4.sp),
                  Text(
                    getTextDesciption(transaction.type),
                    style: TextStyle(
                      fontSize: 11.sp,
                      color: colorGray1,
                    ),
                  ),
                  SizedBox(height: 4.sp),
                  Text(
                    DateFormat('dd/MM/yyyy').format(transaction.createdAt),
                    style: TextStyle(
                      fontSize: 11.sp,
                      color: colorGray1,
                    ),
                  ),
                ],
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  transaction.costString,
                  style: TextStyle(
                    fontSize: 12.sp,
                    color: colorBlack1,
                  ),
                ),
                SizedBox(height: 4.sp),
                Text(
                  getTextTransaction(transaction.status),
                  style: TextStyle(
                    fontSize: 11.sp,
                    color: getColorTransaction(transaction.status),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
