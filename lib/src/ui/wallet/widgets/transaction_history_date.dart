import 'package:askany/src/ui/style/calendar_style.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class TransactionHistoryDate extends StatelessWidget {
  final DateTime transaction;

  const TransactionHistoryDate({Key? key, required this.transaction}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 16.sp, top: 15.sp),
      height: 40.sp,
      child: Text(
        DateFormat(" 'Tháng' MM/yyyy").format(transaction),
        style: TextStyle(
          fontSize: 10.sp,
          fontWeight: FontWeight.w400,
          color: colorGray1,
        ),
      ),
    );
  }
}
