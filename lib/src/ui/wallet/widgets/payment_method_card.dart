import 'package:askany/src/models/enums/payment_mothods.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:askany/src/ui/style/calendar_style.dart';

class PaymentMethodCard extends StatelessWidget {
  final PaymentMethods thisMethod;
  final PaymentMethods currentMethod;
  final Function handlePressed;
  const PaymentMethodCard({
    Key? key,
    required this.thisMethod,
    required this.handlePressed,
    required this.currentMethod,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TouchableOpacity(
      onTap: handlePressed,
      child: Container(
        margin: EdgeInsets.only(bottom: 10.sp),
        padding: EdgeInsets.symmetric(vertical: 10.sp),
        decoration: BoxDecoration(
          border: Border.all(
              color: thisMethod == currentMethod ? headerCalendarColor : Colors.grey,
              width: 0.5.sp),
          borderRadius: BorderRadius.circular(8.sp),
          color: thisMethod == currentMethod ? backgroundNotification : null,
        ),
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 13.sp),
          child: Row(
            children: [
              thisMethod == currentMethod
                  ? Icon(Icons.radio_button_on_sharp, color: headerCalendarColor)
                  : Icon(
                      Icons.radio_button_off_sharp,
                    ),
              SizedBox(width: 12.sp),
              Container(
                height: 30.sp,
                width: 30.sp,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(thisMethod.assetImage),
                    fit: BoxFit.fitWidth,
                  ),
                ),
              ),
              SizedBox(width: 12.sp),
              Text(
                thisMethod.textDescription,
                style: TextStyle(
                  color: colorBlack1,
                  fontWeight: FontWeight.w600,
                  fontSize: 12.sp,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
