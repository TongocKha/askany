import 'package:askany/src/models/payment_card_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:askany/src/ui/style/wallet_style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class BoxCreditCardSaved extends StatefulWidget {
  final PaymentCardModel? paymentCard;

  const BoxCreditCardSaved({
    required this.paymentCard,
  });

  @override
  _BoxCreditCardSavedState createState() => _BoxCreditCardSavedState();
}

class _BoxCreditCardSavedState extends State<BoxCreditCardSaved> {
  @override
  Widget build(BuildContext context) {
    final String result = widget.paymentCard?.cardNumber ?? '';

    return Container(
      height: 60.sp,
      margin: EdgeInsets.only(
        left: 16.sp,
        right: 16.sp,
        bottom: 10.sp,
      ),
      decoration: BoxDecoration(
        border: Border.all(
          color: colorGray2, // Set border color
          width: 0.5.sp,
        ),
        borderRadius: BorderRadius.circular(8.sp),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                margin: EdgeInsets.only(
                  top: 10.sp,
                  bottom: 10.sp,
                  left: 11.sp,
                  right: 12.sp,
                ),
                child: Image.asset(
                  imageBank(bankName: widget.paymentCard!.bankName),
                  width: 36.sp,
                  height: 36.sp,
                  fit: BoxFit.fitWidth,
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    widget.paymentCard!.accountName,
                    style: TextStyle(
                      fontSize: 12.sp,
                      fontWeight: FontWeight.w600,
                      color: colorBlack2,
                    ),
                  ),
                  SizedBox(height: 5.sp),
                  Text(
                    result.substring(result.length - 4),
                    style: TextStyle(
                      fontSize: 11.sp,
                      fontWeight: FontWeight.w400,
                      color: colorGray1,
                    ),
                  ),
                ],
              ),
            ],
          ),
          TouchableOpacity(
            onTap: () => AppNavigator.push(
              Routes.ADD_DETAIL_CARD_PAYMENT,
              arguments: {
                'nameCreditCard': widget.paymentCard!.bankName,
                'imageCreditCard': imageBank(bankName: widget.paymentCard!.bankName),
                'paymentCardModel': widget.paymentCard,
              },
            ),
            child: Container(
              margin: EdgeInsets.only(
                bottom: 15.sp,
                right: 12.sp,
              ),
              child: Text(
                'Sửa',
                style: TextStyle(
                  fontSize: 12.sp,
                  fontWeight: FontWeight.w400,
                  color: colorFinished,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
