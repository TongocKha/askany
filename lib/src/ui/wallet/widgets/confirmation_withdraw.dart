import 'dart:io';
import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/transaction/transaction_bloc.dart';
import 'package:askany/src/bloc/user/user_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/models/account_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/custom_image/custom_image_picker.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_with_text_and_pop_button.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:askany/src/ui/style/wallet_style.dart';
import 'package:askany/src/ui/wallet/widgets/information_withdraw.dart';
import 'package:askany/src/ui/wallet/widgets/select_image.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ConfirmationWithdraw extends StatefulWidget {
  final String titleDescription;
  final String titleImage1;
  final String titleImage2;
  final String amount;
  final String type;
  final String id;
  const ConfirmationWithdraw({
    Key? key,
    required this.id,
    required this.titleDescription,
    required this.titleImage1,
    required this.titleImage2,
    required this.amount,
    required this.type,
  }) : super(key: key);

  @override
  State<ConfirmationWithdraw> createState() => _ConfirmationWithdrawState();
}

class _ConfirmationWithdrawState extends State<ConfirmationWithdraw> {
  File? _image1;
  File? _image2;
  double? amountWallet;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserBloc, UserState>(
      builder: (context, state) {
        double amount = double.parse(widget.amount.replaceAll(',', ''));
        AccountModel user = state is UserGetDone
            ? state.accountModel
            : AppBloc.userBloc.getAccount;
        amountWallet = user.wallet!;
        return Scaffold(
          appBar: appBarTitleBack(
              context, Strings.withdrawConfirminationAppbartext.i18n),
          body: Container(
            padding: EdgeInsets.symmetric(
              vertical: 18.sp,
              horizontal: 16.sp,
            ),
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      InformationWithdraw(
                        amount: widget.amount,
                        type: widget.type,
                      ),
                      SizedBox(
                        height: 28,
                      ),
                      Text(
                        Strings.verifyByPersonalInfor.i18n,
                        style: walletTitleStyle14,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        widget.titleDescription,
                        style: TextStyle(
                          color: headerCalendarColor,
                          fontWeight: FontWeight.w400,
                          fontSize: 12.5.sp,
                        ),
                      ),
                      SizedBox(
                        height: 18.sp,
                      ),
                      Row(
                        children: [
                          TouchableOpacity(
                            onTap: () {
                              CustomImagePicker().openImagePicker(
                                context: context,
                                isRequireCrop: false,
                                handleFinish: (File val) {
                                  setState(() {
                                    _image1 = val;
                                  });
                                },
                              );
                            },
                            child: SelectImage(
                              titleImage: widget.titleImage1,
                              imagePicked: _image1,
                            ),
                          ),
                          SizedBox(
                            width: 17.sp,
                          ),
                          TouchableOpacity(
                            onTap: () {
                              CustomImagePicker().openImagePicker(
                                context: context,
                                isRequireCrop: false,
                                handleFinish: (File val) {
                                  setState(() {
                                    _image2 = val;
                                  });
                                },
                              );
                            },
                            child: SelectImage(
                              titleImage: widget.titleImage2,
                              imagePicked: _image2,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 76.sp,
                  ),
                  Column(
                    children: [
                      ButtonPrimary(
                        text: Strings.confirm.i18n,
                        onPressed: () {
                          if (_image1 == null || _image2 == null) {
                            dialogAnimationWrapper(
                                child: DialogWithTextAndPopButton(
                              bodyAfter: 'Vui lòng chọn ảnh xác nhận',
                            ));
                          } else {
                            showDialogLoading();
                            AppBloc.transactionBloc.add(
                              SendRequestWithDrawEvent(
                                bank: widget.id,
                                amount: amount,
                                images: [_image1!, _image2!],
                              ),
                            );

                            AppBloc.userBloc.add(
                              UpdateUserWalletEvent(
                                updatedWallet: amountWallet! - amount,
                              ),
                            );
                          }
                        },
                      ),
                      SizedBox(
                        height: 20.sp,
                      ),
                      TouchableOpacity(
                        onTap: () {
                          AppNavigator.pop();
                        },
                        child: Container(
                          color: Colors.transparent,
                          child: Text(
                            Strings.cancel.i18n,
                            style: TextStyle(
                              color: colorGray1,
                              fontSize: 13.sp,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
