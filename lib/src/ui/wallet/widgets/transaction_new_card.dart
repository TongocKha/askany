import 'package:askany/src/configs/lang/localization.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

import 'package:askany/src/helpers/extentions/string_extention.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:askany/src/ui/style/wallet_style.dart';

class TransactionNewCard extends StatelessWidget {
  final int type;
  final DateTime createdAt;
  final double amount;
  final int status;
  const TransactionNewCard({
    Key? key,
    required this.type,
    required this.createdAt,
    required this.amount,
    required this.status,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TouchableOpacity(
      onTap: () => AppNavigator.push(
        Routes.TRANSACTION_DETAILS,
        arguments: {
          'transaction': '',
        },
      ),
      child: Container(
        color: Colors.white,
        padding: EdgeInsets.symmetric(
          vertical: 14.sp,
          horizontal: 16.sp,
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    Strings.receivedMoneyFrom.i18n,
                    style: TextStyle(
                      fontSize: 12.sp,
                      color: colorBlack1,
                    ),
                  ),
                  SizedBox(height: 4.sp),
                  Text(
                    getTextDesciption(type),
                    style: TextStyle(
                      fontSize: 11.sp,
                      color: colorGray1,
                    ),
                  ),
                  SizedBox(height: 4.sp),
                  Text(
                    DateFormat('dd/MM/yyyy').format(createdAt),
                    style: TextStyle(
                      fontSize: 11.sp,
                      color: colorGray1,
                    ),
                  ),
                ],
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  amount.toStringAsFixed(0).formatMoney() + 'đ',
                  style: TextStyle(
                    fontSize: 12.sp,
                    color: colorBlack1,
                  ),
                ),
                SizedBox(height: 4.sp),
                Text(
                  getTextTransaction(status),
                  style: TextStyle(
                    fontSize: 11.sp,
                    color: getColorTransaction(status),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
