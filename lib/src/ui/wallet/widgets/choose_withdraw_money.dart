import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:askany/src/ui/style/calendar_style.dart';

class ChooseWithdrawMoney extends StatelessWidget {
  final String text;
  final int index;
  final int selectedIndex;
  final Function handlePressed;
  const ChooseWithdrawMoney({
    Key? key,
    required this.text,
    required this.index,
    required this.handlePressed,
    required this.selectedIndex,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TouchableOpacity(
      onTap: handlePressed,
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 6.5.sp),
        padding: EdgeInsets.symmetric(vertical: 12.sp),
        decoration: BoxDecoration(
          border: Border.all(
            color: selectedIndex == index ? headerCalendarColor : Colors.grey,
            width: 0.5,
          ),
          borderRadius: BorderRadius.circular(8.5.sp),
          color: selectedIndex == index ? backgroundNotification : null,
        ),
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 13.sp),
          child: Row(
            children: [
              selectedIndex == index
                  ? Icon(Icons.radio_button_on_sharp, color: headerCalendarColor)
                  : Icon(
                      Icons.radio_button_off_sharp,
                    ),
              SizedBox(
                width: 9.sp,
              ),
              Text(
                text,
                style: TextStyle(
                  color: colorBlack1,
                  fontWeight: FontWeight.w600,
                  fontSize: 13.sp,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
