import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/payment_card/payment_card_bloc.dart';
import 'package:askany/src/models/payment_card_model.dart';
import 'package:askany/src/ui/common/widgets/pagination_list_view.dart';
import 'package:askany/src/ui/wallet/widgets/box_credit_card_saved.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PaymentCardList extends StatelessWidget {
  const PaymentCardList({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PaymentCardBloc, PaymentCardState>(
      builder: (context, state) {
        List<PaymentCardModel> paymentCardList =
            (state.props[0] as List).toList().cast();
        return PaginationListView(
            callBackRefresh: (Function handleFinished) {
              AppBloc.paymentCard
                  .add(RefreshPaymentCardEvent(handleFinished: handleFinished));
            },
            itemCount: paymentCardList.length,
            itemBuilder: (context, index) {
              return BoxCreditCardSaved(
                paymentCard: paymentCardList[index],
              );
            },
            childShimmer: SizedBox());
      },
    );
  }
}
