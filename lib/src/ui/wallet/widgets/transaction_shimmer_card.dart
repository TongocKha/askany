import 'package:askany/src/ui/common/widgets/shimmers/fade_simmer.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class TransactionShimmerCard extends StatelessWidget {
  const TransactionShimmerCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.symmetric(
        vertical: 14.sp,
        horizontal: 16.sp,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                FadeShimmer(
                  height: 17.sp,
                  width: 50.w,
                  fadeTheme: FadeTheme.lightReverse,
                ),
                SizedBox(height: 4.sp),
                FadeShimmer(
                  height: 13.sp,
                  width: 122.sp,
                  fadeTheme: FadeTheme.lightReverse,
                ),
                SizedBox(height: 4.sp),
                FadeShimmer(
                  height: 13.sp,
                  width: 73.sp,
                  fadeTheme: FadeTheme.lightReverse,
                ),
              ],
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              FadeShimmer(
                height: 17.sp,
                width: 25.w,
                fadeTheme: FadeTheme.lightReverse,
              ),
              SizedBox(height: 4.sp),
              FadeShimmer(
                height: 13.sp,
                width: 15.w,
                fadeTheme: FadeTheme.lightReverse,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
