import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class OptionBarWallet extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _OptionBarWalletState();
}

class _OptionBarWalletState extends State<OptionBarWallet> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: TouchableOpacity(
            onTap: () => AppNavigator.push(Routes.RECHARGE_VNPAY),
            child: _buildSelection(
              title: Strings.payInAppbartext.i18n,
              iconAsset: iconPayIn,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 16.sp),
          child: VerticalDivider(
            color: colorDividerTimeline,
            thickness: 0.3.sp,
            width: 0.3.sp,
          ),
        ),
        Expanded(
          child: TouchableOpacity(
            onTap: () => AppNavigator.push(Routes.WITHDRAWMONEY),
            child: _buildSelection(
              title: Strings.payOutAppbartext.i18n,
              iconAsset: iconPayOut,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 16.sp),
          child: VerticalDivider(
            color: colorDividerTimeline,
            thickness: 0.3.sp,
            width: 0.3.sp,
          ),
        ),
        Expanded(
          child: TouchableOpacity(
            onTap: () => AppNavigator.push(Routes.SAVE_CREDIT_CARD),
            child: _buildSelection(
              title: 'Lưu thẻ',
              iconAsset: iconSaveCard,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildSelection({required String title, required String iconAsset}) {
    return Container(
      color: Colors.transparent,
      alignment: Alignment.center,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            iconAsset,
            width: 20.sp,
            height: 20.sp,
          ),
          SizedBox(height: 10.sp),
          Text(
            title,
            style: TextStyle(
              color: colorBlack2,
              fontSize: 11.sp,
            ),
          )
        ],
      ),
    );
  }
}
