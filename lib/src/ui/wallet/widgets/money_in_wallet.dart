import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/user/user_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/models/account_model.dart';
import 'package:askany/src/ui/style/calendar_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class MoneyInWallet extends StatelessWidget {
  const MoneyInWallet({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100.w,
      padding: EdgeInsets.symmetric(horizontal: 12.sp, vertical: 10.sp),
      decoration: BoxDecoration(
        color: backgroundDetails,
        borderRadius: BorderRadius.all(Radius.circular(5.sp)),
      ),
      child: BlocBuilder<UserBloc, UserState>(
        builder: (context, state) {
          AccountModel user = state is UserGetDone
              ? state.accountModel
              : AppBloc.userBloc.getAccount;
          return Text(
            "${Strings.walletBalance.i18n}: ${user.walletString}",
            style: TextStyle(fontSize: 12.sp, fontWeight: FontWeight.w400),
          );
        },
      ),
    );
  }
}
