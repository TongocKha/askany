import 'package:askany/src/ui/style/calendar_style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class BoxCreditCard extends StatelessWidget {
  final String imageCreditCard;
  final String nameCreditCard;
  final double? fontSize;
  final double? marginBottomImage;
  final EdgeInsets? marginBoxCreditCard;
  const BoxCreditCard(
      {required this.imageCreditCard,
      required this.nameCreditCard,
      this.fontSize,
      this.marginBottomImage,
      this.marginBoxCreditCard});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 105.sp,
      height: 90.sp,
      margin: marginBoxCreditCard ?? EdgeInsets.only(left: 10.sp, top: 10.sp),
      decoration: BoxDecoration(
        border: Border.all(
          color: colorGray2, // Set border color
          width: 0.5.sp,
        ),
        borderRadius: BorderRadius.circular(8.sp),
      ),
      child: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: marginBottomImage ?? 50.sp),
            child: Image.asset(
              imageCreditCard,
              width: 52.sp,
              height: 25.sp,
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 10.sp),
            child: Text(nameCreditCard,
                style: TextStyle(
                    fontSize: fontSize ?? 13.sp,
                    fontWeight: FontWeight.w400,
                    color: colorBlack2)),
          )
        ],
      ),
    );
  }
}
