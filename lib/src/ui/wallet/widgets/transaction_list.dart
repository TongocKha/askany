import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/transaction/transaction_bloc.dart';
import 'package:askany/src/models/transaction_model.dart';
import 'package:askany/src/ui/common/widgets/pagination_list_view.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:askany/src/ui/wallet/widgets/transaction_history_date.dart';
import 'package:askany/src/ui/wallet/widgets/transaction_card.dart';
import 'package:askany/src/ui/wallet/widgets/transaction_shimmer_card.dart';
import 'package:askany/src/ui/wallet/widgets/transaction_shimmer_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class TransactionList extends StatelessWidget {
  final bool isShowDate;
  final int? limitItem;
  const TransactionList({this.isShowDate = false, this.limitItem});
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TransactionBloc, TransactionState>(
      builder: (context, state) {
        List<TransactionModel> transactions =
            (state.props[0] as List).toList().cast();

        return state is TransactionInitial
            ? TransactionShimmerList()
            : PaginationListView(
                childShimmer: TransactionShimmerCard(),
                callBackLoadMore: () {
                  if (limitItem == null) {
                    AppBloc.transactionBloc.add(GetTransactionEvent());
                  }
                },
                callBackRefresh: (Function handleFinished) {
                  AppBloc.transactionBloc.add(RefreshTransactionEvent(
                    handleFinished: handleFinished,
                  ));
                },
                isLoadMore: state is GettingTransaction,
                padding: EdgeInsets.zero,
                physics: BouncingScrollPhysics(),
                itemCount: limitItem != null && transactions.length > limitItem!
                    ? limitItem!
                    : transactions.length,
                itemBuilder: (context, index) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      isShowDate &&
                              (index == 0 ||
                                  transactions[index].createdAt.month !=
                                      transactions[index - 1].createdAt.month)
                          ? TransactionHistoryDate(
                              transaction: transactions[index].createdAt,
                            )
                          : Padding(
                              padding: EdgeInsets.symmetric(horizontal: 16.sp),
                              child: dividerChat,
                            ),
                      TransactionCard(transaction: transactions[index]),
                    ],
                  );
                },
              );
      },
    );
  }
}
