import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/ui/style/calendar_style.dart';
import 'package:askany/src/ui/style/chat_style.dart';
import 'package:askany/src/ui/style/wallet_style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:askany/src/helpers/extentions/string_extention.dart';

class InformationWithdraw extends StatelessWidget {
  final String amount;
  final String type;
  const InformationWithdraw(
      {Key? key, required this.amount, required this.type})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final int _transactionFee = 0;
    return Container(
      padding: EdgeInsets.only(
        top: 20.sp,
        right: 15.sp,
        left: 15.sp,
        bottom: 10.sp,
      ),
      decoration: BoxDecoration(
          border: Border.all(
            width: 0.5.sp,
            color: colorDividerTimeline,
          ),
          borderRadius: BorderRadius.circular(10.sp)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                Strings.withdrawAmount.i18n,
                style: walletTitleStyleS13W400,
              ),
              Text(
                "$amount${Strings.currency.i18n}",
                style: walletTitleStyleS13W400,
              ),
            ],
          ),
          SizedBox(
            height: 21,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                Strings.receiveMoneyMethod.i18n,
                style: walletTitleStyleS13W400,
              ),
              Text(
                type,
                style: walletTitleStyleS13W400,
                textAlign: TextAlign.end,
              ),
            ],
          ),
          SizedBox(
            height: 21,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                Strings.transFee.i18n,
                style: walletTitleStyleS13W400,
              ),
              Text(
                "${_transactionFee.toString().formatMoney()}${Strings.currency.i18n}",
                style: walletTitleStyleS13W400,
              ),
            ],
          ),
          SizedBox(
            height: 21,
          ),
          dividerChat,
          SizedBox(
            height: 21,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                Strings.total.i18n,
                style: walletTitleStyleS13W400,
              ),
              Text(
                '${(int.parse(amount.replaceAll(',', '')) - _transactionFee).toString().formatMoney()}${Strings.currency.i18n}',
                style: walletTitleStyleS13W400,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
