import 'package:askany/src/ui/style/calendar_style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class TransactionDetailsInformation extends StatelessWidget {
  const TransactionDetailsInformation({
    Key? key,
    required this.content,
    required this.value,
  }) : super(key: key);
  final String content;
  final String value;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 16),
      child: Row(
        children: [
          Text(
            content,
            style: TextStyle(
              color: colorGray1,
              fontSize: 12.sp,
              fontWeight: FontWeight.w400,
            ),
            textAlign: TextAlign.start,
          ),
          Spacer(),
          Text(
            value,
            style: TextStyle(
              color: colorGray1,
              fontSize: 12.sp,
              fontWeight: FontWeight.w400,
            ),
            textAlign: TextAlign.end,
          )
        ],
      ),
    );
  }
}
