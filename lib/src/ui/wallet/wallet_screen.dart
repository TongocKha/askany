import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/transaction/transaction_bloc.dart';
import 'package:askany/src/bloc/user/user_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/models/account_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/text_ui/title_and_seemore.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:askany/src/ui/wallet/widgets/option_bar_wallet.dart';
import 'package:askany/src/ui/wallet/widgets/transaction_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class WalletScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _WalletScreenState();
}

class _WalletScreenState extends State<WalletScreen> {
  @override
  void initState() {
    super.initState();
    AppBloc.transactionBloc.add(OnTransactionEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarTitleBack(
        context,
        Strings.myWallet.i18n,
        backgroundColor: headerCalendarColor,
        brightness: Brightness.dark,
      ),
      body: Container(
        child: Stack(
          children: [
            Column(
              children: [
                Container(
                  height: 76.sp,
                  width: 100.w,
                  alignment: Alignment.topCenter,
                  decoration: BoxDecoration(
                    color: headerCalendarColor,
                  ),
                  child: BlocBuilder<UserBloc, UserState>(
                    builder: (context, state) {
                      AccountModel user = state is UserGetDone
                          ? state.accountModel
                          : AppBloc.userBloc.getAccount;
                      return Text(
                        user.walletString,
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                          fontSize: 18.sp,
                        ),
                      );
                    },
                  ),
                ),
                SizedBox(height: 60.sp),
                Padding(
                  padding: EdgeInsets.only(
                    left: 16.sp,
                    right: 10.sp,
                    bottom: 16.sp,
                  ),
                  child: TitleAndSeeMore(
                    title: Strings.recentTrans.i18n,
                    handlePressed: () => AppNavigator.push(
                      Routes.TRANSACTION_HISTORY,
                    ),
                  ),
                ),
                Expanded(
                  child: TransactionList(
                    limitItem: 5,
                  ),
                ),
              ],
            ),
            Positioned(
              top: 76.sp - 37.sp,
              left: 0,
              right: 0,
              child: Container(
                height: 74.sp,
                width: 100.w,
                margin: EdgeInsets.symmetric(horizontal: 16.sp),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(12.sp),
                  boxShadow: [
                    BoxShadow(
                      offset: Offset(1, 1),
                      blurRadius: 4.sp,
                      color: Colors.black.withOpacity(.08),
                    ),
                  ],
                ),
                child: OptionBarWallet(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
