import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/transaction/transaction_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/helpers/currency_helper.dart';
import 'package:askany/src/models/enums/payment_mothods.dart';
import 'package:askany/src/ui/account/widgets/text_title.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_form.dart';
import 'package:askany/src/ui/style/account_style.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:askany/src/ui/wallet/widgets/money_in_wallet.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class RechargeScreen extends StatefulWidget {
  const RechargeScreen({Key? key}) : super(key: key);

  @override
  State<RechargeScreen> createState() => _RechargeScreenState();
}

class _RechargeScreenState extends State<RechargeScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  PaymentMethods _paymentMethod = PaymentMethods.vnpay;

  List<String> listPrice = [
    Strings.listPrice[0].i18n + Strings.currency.i18n,
    Strings.listPrice[1].i18n + Strings.currency.i18n,
    Strings.listPrice[2].i18n + Strings.currency.i18n,
    Strings.listPrice[3].i18n + Strings.currency.i18n,
    Strings.listPrice[4].i18n + Strings.currency.i18n,
    Strings.listPrice[5].i18n + Strings.currency.i18n,
  ];
  final _controllerRecharge = TextEditingController();

  convertStringPrice(int index) {
    switch (index) {
      case 0:
        _controllerRecharge.text = Strings.listPrice[0].i18n;
        break;
      case 1:
        _controllerRecharge.text = Strings.listPrice[1].i18n;
        break;
      case 2:
        _controllerRecharge.text = Strings.listPrice[2].i18n;
        break;
      case 3:
        _controllerRecharge.text = Strings.listPrice[3].i18n;
        break;
      case 4:
        _controllerRecharge.text = Strings.listPrice[4].i18n;
        break;
      case 5:
        _controllerRecharge.text = Strings.listPrice[5].i18n;
        break;
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: appBarTitleBack(
        context,
        Strings.payInAppbartext.i18n,
        brightness: Brightness.light,
      ),
      body: Container(
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: SingleChildScrollView(
                  physics: BouncingScrollPhysics(),
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 16.sp),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              width: 100.w,
                              margin: EdgeInsets.only(top: 18.sp, bottom: 22.sp),
                              child: MoneyInWallet(),
                            ),
                            TextTitle(textTitle: Strings.amountToPayIn.i18n),
                            TextFieldForm(
                              controller: _controllerRecharge,
                              fontSize: 13.sp,
                              textInputType: TextInputType.number,
                              contentPadding: EdgeInsets.all(10.sp),
                              hintText: Strings.amountToPayIn.i18n,
                              inputFormatters: CurrencyHelper.vndInputFormat,
                              textInputAction: TextInputAction.go,
                              validatorForm: (val) => val!.isEmpty || val.trim().length == 0
                                  ? Strings.amountCannotBeLeftBlankErrortext.i18n
                                  : int.tryParse(val.trim()) != null &&
                                          int.parse(val.trim()) > 10000
                                      ? Strings.minimumAmountIs.i18n
                                      : null,
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 18.sp),
                              child: GridView.count(
                                padding: EdgeInsets.zero,
                                physics: BouncingScrollPhysics(),
                                crossAxisSpacing: 8.sp,
                                mainAxisSpacing: 8.sp,
                                childAspectRatio: 5 / 2,
                                crossAxisCount: 0.itemCountGridViewMoney,
                                shrinkWrap: true,
                                children: List.generate(
                                  6,
                                  (index) {
                                    return TouchableOpacity(
                                      onTap: () {
                                        convertStringPrice(index);
                                      },
                                      child: buildBoxMoney(index),
                                    );
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      // SizedBox(height: 16.sp),
                      // dividerThinkness6NotMargin,
                      // SizedBox(height: 16.sp),
                      // Padding(
                      //   padding: EdgeInsets.symmetric(horizontal: 16.sp),
                      //   child: Column(
                      //     crossAxisAlignment: CrossAxisAlignment.start,
                      //     children: [
                      //       TextTitle(textTitle: "Phương thức thanh toán"),
                      //       SizedBox(height: 10.sp),
                      //       PaymentMethodCard(
                      //         thisMethod: PaymentMethods.vnpay,
                      //         currentMethod: _paymentMethod,
                      //         handlePressed: () {
                      //           setState(() {
                      //             _paymentMethod = PaymentMethods.vnpay;
                      //           });
                      //         },
                      //       ),
                      //       PaymentMethodCard(
                      //         thisMethod: PaymentMethods.momo,
                      //         currentMethod: _paymentMethod,
                      //         handlePressed: () {
                      //           setState(() {
                      //             _paymentMethod = PaymentMethods.momo;
                      //           });
                      //         },
                      //       ),
                      //     ],
                      //   ),
                      // ),
                    ],
                  ),
                ),
              ),
              // dividerChat,
              Container(
                margin: EdgeInsets.only(bottom: 18.sp, top: 6.sp),
                padding: EdgeInsets.symmetric(horizontal: 16.sp),
                child: ButtonPrimary(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      showDialogLoading();
                      if (_paymentMethod == PaymentMethods.vnpay) {
                        AppBloc.transactionBloc.add(
                          PayInVNPAYEvent(
                            amount: double.parse(
                              _controllerRecharge.text.replaceAll(',', ''),
                            ),
                          ),
                        );
                      } else if (_paymentMethod == PaymentMethods.momo) {
                        // MomoAppPayment().handlePaymentMomo(
                        //   int.parse(
                        //     _controllerRecharge.text.replaceAll(',', ''),
                        //   ),
                        // );
                      } else if (_paymentMethod == PaymentMethods.native_pay) {}
                    }
                  },
                  text: Strings.nextStep.i18n,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildBoxMoney(int index) {
    return Container(
      height: 46.sp,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5.sp),
        border: Border.all(
          width: 1,
          color: colorBorderTextField,
        ),
      ),
      child: Center(
        child: Text(
          listPrice[index],
          style: TextStyle(
            fontSize: 13.sp,
            fontWeight: FontWeight.w400,
          ),
        ),
      ),
    );
  }
}
