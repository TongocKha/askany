import 'package:askany/src/bloc/user/user_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/style/calendar_style.dart';
import 'package:askany/src/ui/style/wallet_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class SuccessfulRechargeVNPAY extends StatelessWidget {
  final String money;
  const SuccessfulRechargeVNPAY({Key? key, required this.money})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<UserBloc, UserState>(
        builder: (context, state) {
          return Container(
            padding: EdgeInsets.symmetric(horizontal: 16.sp),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset(
                  imagesSuccessfulRecharge,
                  width: 40,
                  height: 40,
                ),
                SizedBox(
                  height: 28.sp,
                ),
                Text(
                  Strings.success.i18n,
                  style: TextStyle(
                      fontSize: 17.sp,
                      fontWeight: FontWeight.w600,
                      color: colorBlack1),
                ),
                SizedBox(
                  height: 18.sp,
                ),
                Text(
                  '${Strings.payInSucessNotiP1.i18n} $money${Strings.currency.i18n} ${Strings.payInSucessNotiP2.i18n}',
                  style: TextStyle(
                      color: colorBlack2,
                      fontWeight: FontWeight.w400,
                      fontSize: 12.sp),
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 48.sp,
                ),
                ButtonPrimary(
                  onPressed: () {
                    AppNavigator.popUntil(Routes.WALLET);
                  },
                  text: Strings.checkMyWallet.i18n,
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
