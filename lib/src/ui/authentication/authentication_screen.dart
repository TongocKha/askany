import 'package:askany/src/ui/authentication/screens/login_screen.dart';
import 'package:askany/src/ui/authentication/screens/register_screen.dart';
import 'package:flutter/material.dart';

class AuthenticateScreen extends StatefulWidget {
  final bool isExpert;
  const AuthenticateScreen({required this.isExpert});
  @override
  State<StatefulWidget> createState() => _AuthenticateScreenState();
}

class _AuthenticateScreenState extends State<AuthenticateScreen> {
  bool signIn = true;
  @override
  void initState() {
    super.initState();
  }

  switchScreen() {
    setState(() {
      signIn = !signIn;
    });
  }

  @override
  Widget build(BuildContext context) {
    return signIn
        ? LoginScreen(
            toggleView: switchScreen,
            isExpert: widget.isExpert,
          )
        : RegisterScreen(
            toggleView: switchScreen,
            isExpert: widget.isExpert,
          );
  }
}
