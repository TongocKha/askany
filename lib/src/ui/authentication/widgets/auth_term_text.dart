import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/calendar_style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class AuthTermText extends StatelessWidget {
  const AuthTermText({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          Strings.pressSignUpMean.i18n,
          style: TextStyle(fontSize: 10.sp, color: Colors.black),
        ),
        TouchableOpacity(
          onTap: () {},
          child: Text(
            Strings.rules.i18n,
            style: TextStyle(
              fontSize: 11.sp,
              color: colorGreen2,
              decoration: TextDecoration.underline,
            ),
          ),
        ),
        Text(
          Strings.ofUs.i18n,
          style: TextStyle(fontSize: 10.sp, color: Colors.black),
        )
      ],
    );
  }
}
