import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/authentication/bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/models/social_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/services/firebase_auth/firebase_authentication.dart';
import 'package:askany/src/ui/authentication/widgets/auth_method_button.dart';
import 'package:askany/src/ui/authentication/widgets/auth_tapable_text.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:flutter/foundation.dart';

class AuthFooter extends StatelessWidget {
  const AuthFooter({
    Key? key,
    required Function onTap,
    required String normalText,
    required String tapableText,
    required bool isExpert,
  })  : _onTap = onTap,
        _normalText = normalText,
        _tapableText = tapableText,
        _isExpert = isExpert,
        super(key: key);

  final Function _onTap;
  final String _normalText;
  final String _tapableText;
  final bool _isExpert;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(top: 15.sp, bottom: 15.sp),
          child: Text(
            Strings.or.i18n,
            style: TextStyle(
              fontSize: 11.sp,
              color: colorGray1,
            ),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            AuthMethodButton(
                imageSize: Size(17.5.sp, 17.5.sp),
                onTap: () async {
                  showDialogLoading();
                  SocialModel? social = await signInWithGoogle();
                  _handleLoginWithSocial(social: social);
                },
                image: 'assets/icons/ic_google.png'),
            AuthMethodButton(
              imageSize: Size(9.5.sp, 17.5.sp),
              onTap: () async {
                showDialogLoading();
                SocialModel? social =
                    await signInWithFacebook(LoginBehavior.nativeOnly);
                _handleLoginWithSocial(social: social);
              },
              image: 'assets/icons/ic_facebook.png',
            ),
            Visibility(
              visible: defaultTargetPlatform == TargetPlatform.iOS,
              replacement: Container(),
              child: AuthMethodButton(
                padding: EdgeInsets.only(bottom: 1.5.sp),
                image: iconApple,
                imageSize: Size(
                  18.sp,
                  18.sp,
                ),
                onTap: () async {
                  showDialogLoading();
                  SocialModel? social = await signInWithApple();
                  _handleLoginWithSocial(social: social);
                },
              ),
            ),
          ],
        ),
        SizedBox(
          height: 15.sp,
        ),
        AuthTapableText(
          normalText: _normalText,
          tapableText: _tapableText,
          onTap: _onTap,
        )
      ],
    );
  }

  _handleLoginWithSocial({required SocialModel? social}) {
    if (social == null) {
      // Show Popup
      AppNavigator.pop();
    } else {
      AppBloc.authBloc.add(
        LoginWithSocialEvent(
          social: social,
          isExpert: _isExpert,
        ),
      );
    }
  }
}
