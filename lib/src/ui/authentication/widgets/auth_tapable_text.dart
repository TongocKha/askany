import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class AuthTapableText extends StatelessWidget {
  const AuthTapableText({
    Key? key,
    required onTap,
    required normalText,
    required tapableText,
  })  : _onTap = onTap,
        _normalText = normalText,
        _tapableText = tapableText,
        super(key: key);

  final Function _onTap;
  final String _normalText;
  final String _tapableText;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          _normalText,
          style: TextStyle(
            color: colorBlack2,
            fontSize: 12.sp,
          ),
        ),
        SizedBox(width: 6.sp),
        TouchableOpacity(
          onTap: _onTap,
          child: Text(
            _tapableText,
            style: TextStyle(
              color: colorFontGreen,
              fontSize: 12.sp,
              fontWeight: FontWeight.w600,
            ),
          ),
        )
      ],
    );
  }
}
