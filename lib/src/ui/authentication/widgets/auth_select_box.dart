import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class AuthSelectBox extends StatelessWidget {
  const AuthSelectBox({
    Key? key,
    required TextEditingController controller,
    required String? Function(String?) validator,
    required Function onTap,
  })  : _controller = controller,
        _validator = validator,
        _onTap = onTap,
        super(key: key);

  final TextEditingController _controller;
  final String? Function(String?) _validator;
  final Function _onTap;

  @override
  Widget build(BuildContext context) {
    return TouchableOpacity(
      onTap: _onTap,
      child: TextFormField(
        enabled: false,
        controller: _controller,
        keyboardType: TextInputType.text,
        validator: _validator,
        style: TextStyle(color: Colors.black, fontSize: 12.sp),
        decoration: InputDecoration(
          hintText: Strings.address.i18n,
          hintStyle: TextStyle(color: colorBlack2, fontSize: 12.sp),
          disabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: colorGray2,
            ),
          ),
          suffixIconConstraints: BoxConstraints(
            minWidth: 16.sp,
            minHeight: 16.sp,
          ),
          suffixIcon: Padding(
            padding: EdgeInsets.only(right: 8.sp),
            child: Image.asset(
              iconArrowDown,
              width: 14.sp,
            ),
          ),
        ),
        // onChanged: (value) => password = value.trim(),
      ),
    );
  }
}
