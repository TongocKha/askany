import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/configs/themes/theme_service.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_none.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ChooseAccountScreen extends StatefulWidget {
  final VoidCallback? toggleView;

  ChooseAccountScreen({this.toggleView});

  @override
  _ChooseAccountScreenState createState() => _ChooseAccountScreenState();
}

class _ChooseAccountScreenState extends State<ChooseAccountScreen> {
  @override
  void initState() {
    super.initState();
    ThemeService().setStatusColor(false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarBrighnessDark(brightness: Brightness.light),
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 16.sp),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset(
                imageIntroduction,
                width: 75.w,
                height: 75.w,
              ),
              SizedBox(
                height: 40.sp,
              ),
              Text(
                Strings.welcomeToAskAny.i18n,
                style: TextStyle(
                  color: colorBlack1,
                  fontSize: 15.sp,
                  fontWeight: FontWeight.w600,
                ),
              ),
              SizedBox(
                width: 10.sp,
                height: 10.sp,
              ),
              Container(
                child: Text(
                  Strings.tellUsWhatAreYouLookingFor.i18n,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: colorGray1,
                    fontSize: 13.sp,
                    height: 1.25.sp,
                  ),
                ),
              ),
              SizedBox(
                width: 40.sp,
                height: 40.sp,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    child: TouchableOpacity(
                      onTap: () {
                        AppNavigator.push(Routes.AUTHENTICATION, arguments: {
                          'isExpert': false,
                        });
                      },
                      child: Container(
                        height: 36.sp,
                        padding: EdgeInsets.symmetric(horizontal: 8.sp),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(6.5.sp),
                          border: Border.all(
                            color: colorFontGreen, // Set border color
                            width: 1.0,
                          ),
                          color: Colors.white,
                        ),
                        alignment: Alignment.center,
                        child: Text(
                          Strings.iNeedSomeAdvisse.i18n,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: colorFontGreen,
                            fontSize: 12.5.sp,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: 10.sp),
                  Expanded(
                    child: TouchableOpacity(
                      onTap: () {
                        AppNavigator.push(Routes.AUTHENTICATION, arguments: {
                          'isExpert': true,
                        });
                      },
                      child: Container(
                        height: 36.sp,
                        padding: EdgeInsets.symmetric(horizontal: 8.sp),
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: colorFontGreen, // Set border color
                            width: 1.0,
                          ),
                          borderRadius: BorderRadius.circular(6.5.sp),
                          color: Color(0xff1C4843),
                        ),
                        alignment: Alignment.center,
                        child: Text(
                          Strings.imAnExpert.i18n,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 12.5.sp,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
