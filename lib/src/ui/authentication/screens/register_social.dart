import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class RegisterSocialScreen extends StatefulWidget {
  final VoidCallback? toggleView;

  RegisterSocialScreen({this.toggleView});
  @override
  _RegisterSocialScreenState createState() => _RegisterSocialScreenState();
}

class _RegisterSocialScreenState extends State<RegisterSocialScreen> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _nameController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();
  TextEditingController _provinceController = TextEditingController();
  FocusNode textFieldFocus = FocusNode();
  String name = '';
  String email = '';
  String password = '';
  String phone = '';
  String province = '';
  bool _passwordVisible = false;
  bool hidePassword = true;

  hideKeyboard() => textFieldFocus.unfocus();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(left: 20.sp, right: 20.sp),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(top: 50.sp, bottom: 50.sp),
                            child: Center(
                              child: Image.asset(
                                'assets/icons/ic_logo.png',
                                width: 100.sp,
                              ),
                            ),
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: TextFormField(
                                  keyboardType: TextInputType.text,
                                  controller: _nameController,
                                  style: TextStyle(color: Colors.black, fontSize: 12.sp),
                                  decoration: InputDecoration(
                                    hintText: Strings.nameTitle.i18n,
                                  ),
                                  // obscureText: true,
                                  validator: (value) {
                                    if (value!.trim().isEmpty) {
                                      return Strings.nameCannotBeLeftBlankErrortext.i18n;
                                    }
                                    return null;
                                  },
                                  onChanged: (value) => name = value,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 10.sp),
                          Row(
                            children: [
                              Expanded(
                                child: TextFormField(
                                  keyboardType: TextInputType.text,
                                  controller: _emailController,
                                  style: TextStyle(color: Colors.black, fontSize: 12.sp),
                                  decoration: InputDecoration(
                                    hintText: Strings.emailHintext.i18n,
                                  ),
                                  // obscureText: true,
                                  validator: (value) {
                                    if (value!.trim().isEmpty) {
                                      return Strings.emailCannotBeLeftBlankErrortext.i18n;
                                    }
                                    return null;
                                  },
                                  onChanged: (value) => email = value,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 10.sp),
                          Container(
                            width: double.infinity,
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    Expanded(
                                      child: TextFormField(
                                        keyboardType: TextInputType.text,
                                        controller: _passwordController,
                                        obscureText: !_passwordVisible,
                                        style: TextStyle(color: Colors.black, fontSize: 12.sp),
                                        decoration: InputDecoration(
                                          hintText: Strings.passwordHintext.i18n,
                                          suffixIcon: IconButton(
                                            icon: Icon(
                                              _passwordVisible
                                                  ? Icons.visibility
                                                  : Icons.visibility_off,
                                              color: Colors.grey,
                                            ),
                                            onPressed: () {
                                              setState(() {
                                                _passwordVisible = !_passwordVisible;
                                              });
                                            },
                                          ),
                                        ),
                                        // obscureText: true,
                                        validator: (value) {
                                          if (value!.trim().isEmpty) {
                                            return Strings
                                                .passwordMustBeAtLeast6CharactersErrortext.i18n;
                                          }
                                          if (value.trim().length < 6) {
                                            return Strings
                                                .passwordMustBeAtLeast6CharactersErrortext.i18n;
                                          }
                                          return null;
                                        },
                                        onChanged: (value) => password = value,
                                      ),
                                    ),
                                    SizedBox(
                                      width: 5.sp,
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 10.sp),
                          Row(
                            children: [
                              Expanded(
                                child: TextFormField(
                                  keyboardType: TextInputType.text,
                                  controller: _phoneController,
                                  // obscureText: !_passwordVisible,
                                  style: TextStyle(color: Colors.black, fontSize: 12.sp),
                                  decoration: InputDecoration(
                                    hintText: Strings.phoneNumberHintext.i18n,
                                  ),
                                  // obscureText: true,
                                  validator: (value) {
                                    if (value!.trim().isEmpty) {
                                      return Strings.phoneNumberCannotBeLeftBlankErrortext.i18n;
                                    }
                                    return null;
                                  },
                                  onChanged: (value) => phone = value,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 10.sp),
                          Stack(
                            children: [
                              Row(
                                children: [
                                  Expanded(
                                    child: TextFormField(
                                      keyboardType: TextInputType.text,
                                      controller: _provinceController,
                                      style: TextStyle(color: Colors.black, fontSize: 12.sp),
                                      decoration: InputDecoration(
                                        hintText: Strings.address.i18n,
                                      ),
                                      validator: (value) {
                                        if (value!.trim().isEmpty) {
                                          return Strings.addressCannotBeLeftBlankErrortext.i18n;
                                        }
                                        return null;
                                      },
                                      onChanged: (value) => province = value,
                                    ),
                                  ),
                                ],
                              ),
                              Positioned(
                                  right: 12.sp,
                                  bottom: 10.sp,
                                  child: TouchableOpacity(
                                    onTap: () {},
                                    child: Image.asset(
                                      'assets/icons/ic_down.png',
                                      width: 15.sp,
                                    ),
                                  )),
                            ],
                          ),
                        ],
                      ),
                      Container(
                        width: double.infinity,
                        child: TouchableOpacity(
                          onTap: () {
                            AppNavigator.pop();
                          },
                          child: Container(
                            margin: EdgeInsets.only(top: 30.sp),
                            padding: EdgeInsets.symmetric(horizontal: 95.sp, vertical: 8.sp),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8.sp),
                              color: Color(0xff1C4843),
                            ),
                            child: Text(
                              Strings.signUp.i18n,
                              style: TextStyle(color: Colors.white, fontSize: 14.sp),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 15.sp,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            Strings.pressSignUpMean.i18n,
                            style: TextStyle(fontSize: 10.sp, color: Colors.black),
                          ),
                          TouchableOpacity(
                            onTap: () {},
                            child: Text(
                              Strings.rules.i18n,
                              style: TextStyle(fontSize: 10.sp, color: Color(0xff1C4843)),
                            ),
                          ),
                          Text(
                            Strings.ofUs.i18n,
                            style: TextStyle(fontSize: 10.sp, color: Colors.black),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 20.sp,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Bạn đã có tài khoản?',
                            style: TextStyle(color: Colors.black, fontSize: 11.sp),
                          ),
                          SizedBox(
                            width: 2.sp,
                          ),
                          TouchableOpacity(
                            onTap: () {
                              AppNavigator.pop();
                            },
                            child: Text(
                              'Đăng nhập',
                              style: TextStyle(
                                  color: Color(0XFF1C4843),
                                  fontSize: 11.sp,
                                  fontWeight: FontWeight.bold),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 20.sp,
                      ),
                      // GestureDetector(
                      //   onTap: () async {
                      //     if (_formKey.currentState!.validate()) {
                      //       showDialogLoading(context);
                      //       AppBloc.authBloc.add(
                      //         RegisterEvent(
                      //           username: email,
                      //           password: password,
                      //           firstName: firstName,
                      //           lastName: lastName,
                      //         ),
                      //       );
                      //     }
                      //   },
                      //   child: Container(
                      //     height: 40.sp,
                      //     margin: EdgeInsets.symmetric(
                      //       horizontal: 12.w,
                      //     ),
                      //     decoration: BoxDecoration(
                      //       borderRadius: BorderRadius.circular(8.sp),
                      //       color: Theme.of(context).primaryColor,
                      //     ),
                      //     child: Center(
                      //       child: Text(
                      //         'Đăng kí ngay',
                      //         style: TextStyle(
                      //           color: mC,
                      //           fontSize: 10.sp,
                      //           fontWeight: FontWeight.w600,
                      //         ),
                      //       ),
                      //     ),
                      //   ),
                      // ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
