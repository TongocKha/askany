import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/authentication/bloc.dart';
import 'package:askany/src/bloc/experiences/experiences_bloc.dart';
import 'package:askany/src/bloc/register_expert/register_expert_bloc.dart';
import 'package:askany/src/bloc/skill/skill_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/models/experience_model.dart';
import 'package:askany/src/models/expert_model.dart';
import 'package:askany/src/models/skill_model.dart';
import 'package:askany/src/models/slide_mode.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/authentication/screens/register_expert/screens/register_expert_step_1.dart';
import 'package:askany/src/ui/authentication/screens/register_expert/screens/register_expert_step_2.dart';
import 'package:askany/src/ui/authentication/screens/register_expert/screens/register_expert_step_3.dart';
import 'package:askany/src/ui/authentication/screens/register_expert/screens/register_expert_step_4.dart';
import 'package:askany/src/ui/authentication/screens/register_expert/widgets/register_expert_header.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_with_text_and_pop_button.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class RegisterExpertScreen extends StatefulWidget {
  final ExpertModel? expertModel;
  const RegisterExpertScreen({this.expertModel});
  @override
  State<StatefulWidget> createState() => _RegisterExpertScreenState();
}

class _RegisterExpertScreenState extends State<RegisterExpertScreen> {
  late PageController _pageController;
  late List<Widget> _steps;
  late List<GlobalKey<FormState>> _formKeys;
  int _currentStep = 0;

  ExpertModel expertModel = ExpertModel(
    id: '',
    expertname: '',
    fullname: '',
    province: 1,
    gender: 1,
    status: 1,
    isUser: false,
    experienceConfirm: null,
    specialties: [],
    wallet: 0.0,
    stepInfo: 1,
    ratingCount: 0,
    stars: 0,
    companyName: '',
    experienceYears: 0,
    highestPosition: '',
    introduce: '',
    languages: [],
    workingExperience: [],
    skills: [],
  );

  ExperienceModel experience = ExperienceModel(
    id: '',
    companyName: '',
    specialties: [],
    position: '',
    timeStartWork: DateTime.now(),
    timeEndWork: DateTime.now(),
    description: '',
    status: 1,
    author: '',
    createdAt: DateTime.now(),
  );

  SkillModel skill = SkillModel(
    status: 1,
    description: '',
    name: '',
    keywords: [],
    companyName: '',
    expExperience: 0,
    meetPrice: null,
    callPrice: null,
    highestPosition: null,
    specialty: null,
  );

  @override
  void initState() {
    super.initState();
    _initialValue();

    _pageController = PageController(
      initialPage: _currentStep,
    );

    _formKeys = [1, 2, 3, 4].map((e) => GlobalKey<FormState>()).toList();

    _steps = [
      RegisterExpertStep1(
        formKey: _formKeys[0],
        onChanged: _handleChangeExpertValue,
        expert: expertModel,
      ),
      RegisterExpertStep2(
        formKey: _formKeys[1],
        experience: experience,
        onChanged: (experienceModel) {
          setState(() {
            experience = experienceModel;
          });
        },
      ),
      RegisterExpertStep3(
        formKey: _formKeys[2],
        onChanged: _handleChangeExpertValue,
        expert: expertModel,
      ),
      RegisterExpertStep4(
        formKey: _formKeys[3],
        skill: skill,
        onChanged: (skillModel) {
          setState(() {
            skill = skillModel;
          });
        },
      ),
    ];
  }

  _initialValue() {
    AppBloc.experienceBloc.add(CleanExperienceEvent());
    AppBloc.skillBloc.add(CleanSkillEvent());
    AppBloc.skillBloc
        .add(GetSkillEvent(expertId: widget.expertModel?.id ?? ''));
    AppBloc.experienceBloc.add(OnExperiencesEvent());
    if (widget.expertModel != null) {
      ExpertModel _expert = widget.expertModel!;
      expertModel = _expert;
      if (_expert.introduce.isNotEmpty) {
        _currentStep = 1;
      }

      if ((_expert.workingExperience ?? []).isNotEmpty) {
        _currentStep = 2;
      }

      if (_expert.callPrice != null) {
        _currentStep = 3;
      }
    }
  }

  _handleChangeExpertValue(ExpertModel expert) {
    setState(() {
      expertModel = expert;

      _steps = [
        RegisterExpertStep1(
          formKey: _formKeys[0],
          onChanged: _handleChangeExpertValue,
          expert: expertModel,
        ),
        RegisterExpertStep2(
          formKey: _formKeys[1],
          experience: experience,
          onChanged: (experienceModel) {
            setState(() {
              experience = experienceModel;
            });
          },
        ),
        RegisterExpertStep3(
          formKey: _formKeys[2],
          onChanged: _handleChangeExpertValue,
          expert: expertModel,
        ),
        RegisterExpertStep4(
          formKey: _formKeys[3],
          skill: skill,
          onChanged: (skillModel) {
            setState(() {
              skill = skillModel;
            });
          },
        ),
      ];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: appBarTitleBack(
        context,
        '',
        onBackPressed: () {
          if (_currentStep != 0) {
            _pageController.previousPage(
              duration: const Duration(milliseconds: LIMIT_RESPONSE_TIME),
              curve: Curves.easeInOut,
            );
          } else {
            AppNavigator.pop();
          }
        },
      ),
      body: SafeArea(
        top: false,
        child: Container(
          child: Column(
            children: [
              SizedBox(height: 2.sp),
              RegisterExpertHeader(currentStep: _currentStep),
              SizedBox(height: 18.sp),
              dividerThinkness6,
              Expanded(
                child: PageView(
                  physics: NeverScrollableScrollPhysics(),
                  controller: _pageController,
                  children: _steps,
                  onPageChanged: (index) {
                    setState(() {
                      _currentStep = index;
                    });
                  },
                ),
              ),
              dividerChat,
              Container(
                padding: EdgeInsets.fromLTRB(
                  16.sp,
                  12.sp,
                  16.sp,
                  .0,
                ),
                child: ButtonPrimary(
                  onPressed: _buttonOnPressedHandle,
                  text: _currentStep % 2 != 0
                      ? Strings.save.i18n
                      : Strings.nextStep.i18n,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _buttonOnPressedHandle() {
    debugPrint(_formKeys[_currentStep].currentState?.validate().toString());
    if (_formKeys[_currentStep].currentState?.validate() ?? true) {
      switch (_currentStep) {
        case 0:
          showDialogLoading();
          AppBloc.registerExpertBloc.add(
            UpdateRegisterStep1Event(
              expert: expertModel,
              handleFinished: () {
                _pageController.nextPage(
                  duration: Duration(milliseconds: DELAY_200_MS),
                  curve: Curves.easeIn,
                );
              },
            ),
          );
          break;
        case 1:
          if (AppBloc.experienceBloc.experiences.isEmpty) {
            showDialogLoading();
            AppBloc.experienceBloc.add(
              AddExperienceEvent(
                experience: experience,
                handleFinished: () {},
              ),
            );
          } else {
            _pageController.nextPage(
              duration: Duration(milliseconds: DELAY_200_MS),
              curve: Curves.easeIn,
            );
          }
          break;
        case 2:
          if ((expertModel.timelinePriceModel ?? []).isEmpty) {
            debugPrint(expertModel.timelinePriceModel.toString());
            dialogAnimationWrapper(
              slideFrom: SlideMode.bot,
              child: DialogWithTextAndPopButton(
                title: Strings.noTimeSelectedYet.i18n,
                bodyAfter: Strings.noTimeSelectedYetNoti.i18n,
              ),
            );
          } else if ((expertModel.timelinePriceModel?.first.weeklySchedules ??
                  [])
              .isEmpty) {
                            debugPrint(expertModel.timelinePriceModel.toString());

            dialogAnimationWrapper(
              slideFrom: SlideMode.bot,
              child: DialogWithTextAndPopButton(
                title: Strings.noTimeSelectedYet.i18n,
                bodyAfter: Strings.noTimeSelectedYetNoti.i18n,
              ),
            );
          } else if ((expertModel.timelinePriceModel?.first.weeklySchedules
                      .first.timesCall ??
                  [])
              .isEmpty) {
                            debugPrint(expertModel.timelinePriceModel.toString());

            dialogAnimationWrapper(
              slideFrom: SlideMode.bot,
              child: DialogWithTextAndPopButton(
                title: Strings.noTimeSelectedYet.i18n,
                bodyAfter: Strings.noTimeSelectedYetNoti.i18n,
              ),
            );
          }else{
            showDialogLoading();

          AppBloc.registerExpertBloc.add(
            UpdateRegisterStep3Event(
              expert: expertModel,
              handleFinished: () {
                _pageController.nextPage(
                  duration: Duration(milliseconds: DELAY_200_MS),
                  curve: Curves.easeIn,
                );
              },
            ),
          );

          }          
          break;
        case 3:
          if (AppBloc.skillBloc.skills.isEmpty) {
            showDialogLoading();
            AppBloc.skillBloc.add(
              AddSkillEvent(
                skill: skill,
                handleFinished: () {},
              ),
            );
          } else {
            showDialogLoading();
            UserLocal().saveAccessToken(UserLocal().getBackupToken());
            UserLocal().clearBackupToken();
            AppNavigator.pop();
            AppNavigator.pushNamedAndRemoveUntil(Routes.ROOT);
            AppBloc.authBloc.add(OnAuthCheck());
            // Future.delayed(Duration(milliseconds: DELAY_HALF_SECOND * 3), () {
            //   dialogAnimationWrapper(
            //     slideFrom: SlideMode.bot,
            //     child: DialogWithImageAndText(
            //       image: Image.asset(
            //         imageRegisterSuccess,
            //         width: 80.sp,
            //         height: 80.sp,
            //       ),
            //       padding: EdgeInsets.symmetric(
            //         vertical: 26.sp,
            //         horizontal: 20.sp,
            //       ),
            //       text: 'Bạn đã xác thực số điện thoại thành công và hoàn tất đăng ký chuyên gia.',
            //       textFontSize: 12.5.sp,
            //     ),
            //   );
            // });
          }
          break;
      }
    }
  }
}
