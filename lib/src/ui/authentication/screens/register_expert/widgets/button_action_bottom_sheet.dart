import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ButtonActionBottomSheet extends StatelessWidget {
  final String text;
  final bool isPrimary;
  final Function handlePressed;
  const ButtonActionBottomSheet({
    required this.handlePressed,
    this.isPrimary = false,
    required this.text,
  });
  @override
  Widget build(BuildContext context) {
    return TouchableOpacity(
      onTap: handlePressed,
      child: Container(
        width: 100.sp,
        height: 34.sp,
        decoration: BoxDecoration(
          color: isPrimary ? headerCalendarColor : Colors.white,
          border: Border.all(
            width: 0.5,
            color: colorGray1,
          ),
          borderRadius: BorderRadius.all(
            Radius.circular(6.sp),
          ),
        ),
        child: Center(
          child: Text(
            text,
            style: TextStyle(
              fontSize: 14.sp,
              fontWeight: FontWeight.w600,
              color: isPrimary ? colorWhiteCard : colorGray1,
            ),
          ),
        ),
      ),
    );
  }
}
