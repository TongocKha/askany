import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class RegisterExpertHeader extends StatefulWidget {
  final int currentStep;
  const RegisterExpertHeader({required this.currentStep});
  @override
  State<StatefulWidget> createState() => _RegisterExpertHeaderState();
}

class _RegisterExpertHeaderState extends State<RegisterExpertHeader> {
  List<String> _iconAssets = [
    iconStep1,
    iconStep2,
    iconStep3,
    iconStep4,
  ];

  List<String> _titleStep = [
    Strings.createProfile.i18n,
    Strings.experienceInWork.i18n,
    Strings.costOfASingleService.i18n,
    Strings.skillsOfYou.i18n,
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16.sp),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Image.asset(
                _iconAssets[widget.currentStep],
                width: 20.sp,
                height: 20.sp,
              ),
              SizedBox(width: 8.sp),
              Text(
                '${Strings.step.i18n} ${widget.currentStep + 1}: ${_titleStep[widget.currentStep]}',
                style: TextStyle(
                  color: colorBlack2,
                  fontSize: 14.sp,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ],
          ),
          SizedBox(height: 16.sp),
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30.sp),
              color: colorGrey3,
            ),
            child: Row(children: [
              Container(
                height: 5.sp,
                width: (100.w - 16.sp * 2) / 4 * (widget.currentStep + 1),
                decoration: BoxDecoration(
                  color: colorGreen3,
                  borderRadius: BorderRadius.circular(30.sp),
                ),
              ),
            ]),
          ),
        ],
      ),
    );
  }
}
