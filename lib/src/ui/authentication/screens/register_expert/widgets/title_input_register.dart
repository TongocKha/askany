import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class TitleInputRegister extends StatelessWidget {
  final String text;
  const TitleInputRegister({required this.text});
  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        color: colorBlack2,
        fontSize: 13.sp,
        fontWeight: FontWeight.w600,
      ),
    );
  }
}
