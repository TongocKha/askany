import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/models/language_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/ui/authentication/screens/register_expert/widgets/button_action_bottom_sheet.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/checkbox/custom_check_box.dart';
import 'package:askany/src/ui/common/widgets/divider_bottom_sheet.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class LanguageBottomSheet extends StatefulWidget {
  final List<LanguageModel> languageModel;
  final Function(List<LanguageModel>) handleFinished;

  const LanguageBottomSheet({
    Key? key,
    required this.languageModel,
    required this.handleFinished,
  }) : super(key: key);

  @override
  _LanguageBottomSheet createState() => new _LanguageBottomSheet();
}

class _LanguageBottomSheet extends State<LanguageBottomSheet> {
  late List<LanguageModel> _languages;

  @override
  void initState() {
    super.initState();
    _languages = widget.languageModel;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      clipBehavior: Clip.antiAlias,
      decoration: BoxDecoration(
        color: colorWhite,
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(18.sp),
        ),
      ),
      child: Container(
        height: 80.h,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(height: 5.sp),
            DividerBottomSheet(),
            SizedBox(height: 20.sp),
            Container(
              alignment: Alignment.topLeft,
              margin: EdgeInsets.only(left: 16.sp),
              child: Text(
                Strings.languages.i18n,
                style: TextStyle(
                  fontSize: 15.sp,
                  fontWeight: FontWeight.w700,
                  color: colorBlack2,
                ),
              ),
            ),
            SizedBox(height: 10.sp),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 16.sp, vertical: 4.sp),
              child: dividerChat,
            ),
            Expanded(
              child: ListView.builder(
                itemCount: AppBloc.userBloc.languages.length,
                physics: BouncingScrollPhysics(),
                itemBuilder: (BuildContext context, int index) {
                  return TouchableOpacity(
                    onTap: () {
                      itemChange(AppBloc.userBloc.languages[index]);
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(
                        horizontal: 16.sp,
                        vertical: 14.sp,
                      ),
                      color: Colors.transparent,
                      child: Row(
                        children: [
                          Expanded(
                            child: Text(
                              AppBloc.userBloc.languages[index].getLocaleTitle,
                              style: TextStyle(
                                fontSize: 13.sp,
                              ),
                            ),
                          ),
                          CustomCheckBox(
                            isChecked: _languages.indexWhere((language) =>
                                    AppBloc.userBloc.languages[index].id == language.id) !=
                                -1,
                            onChanged: (val) {
                              itemChange(AppBloc.userBloc.languages[index]);
                            },
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
            SizedBox(height: 12.sp),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.sp),
              child: Row(
                children: [
                  Expanded(
                    child: ButtonActionBottomSheet(
                      handlePressed: () => itemResetAll(),
                      text: Strings.clearAll.i18n,
                    ),
                  ),
                  SizedBox(width: 12.sp),
                  Expanded(
                    child: ButtonActionBottomSheet(
                      handlePressed: () {
                        widget.handleFinished(_languages);
                        AppNavigator.pop();
                      },
                      isPrimary: true,
                      text: Strings.ok.i18n,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 30.sp),
          ],
        ),
      ),
    );
  }

  void itemChange(LanguageModel languageModel) {
    int indexOfLanguage = _languages.indexWhere((language) => language.id == languageModel.id);

    if (indexOfLanguage == -1) {
      setState(() {
        _languages.add(languageModel);
      });
    } else {
      setState(() {
        _languages.removeAt(indexOfLanguage);
      });
    }
  }

  void itemResetAll() {
    setState(() {
      _languages.clear();
    });
  }
}
