import 'package:askany/src/configs/lang/localization.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'package:askany/src/models/experience_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ExperienceCard extends StatefulWidget {
  final ExperienceModel experience;
  const ExperienceCard({
    Key? key,
    required this.experience,
  }) : super(key: key);
  @override
  State<StatefulWidget> createState() => _ExperienceCardState();
}

class _ExperienceCardState extends State<ExperienceCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 6.sp),
      padding: EdgeInsets.symmetric(vertical: 14.sp, horizontal: 8.sp),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(6.5.sp),
        gradient: LinearGradient(
          stops: [0.02, 0.02],
          colors: [colorGreen2, colorBackgroundExperience],
        ),
      ),
      child: Padding(
        padding: EdgeInsets.only(left: 100.w * 0.02),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Container(
                    margin: EdgeInsets.fromLTRB(0, 0, 7.sp, 0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          '${Strings.anExpertIn.i18n} ${widget.experience.specialtyTitle}',
                          style: TextStyle(
                            fontSize: 14.sp,
                            fontWeight: FontWeight.w600,
                            color: colorBlack2,
                          ),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                        ),
                        SizedBox(height: 4.sp),
                        Text(
                          widget.experience.companyName,
                          style: TextStyle(
                            fontSize: 12.sp,
                            color: colorBlack2,
                          ),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(width: 10.sp),
                TouchableOpacity(
                  onTap: () => AppNavigator.push(
                    Routes.ADD_EXPERIENCE,
                    arguments: {
                      'experienceModel': widget.experience,
                    },
                  ),
                  child: Container(
                    height: 24.sp,
                    width: 24.sp,
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: colorGreen2, // Set border color
                        width: 0.5.sp,
                      ),
                      borderRadius: BorderRadius.circular(4.5.sp),
                    ),
                    alignment: Alignment.center,
                    child: Image.asset(
                      iconPenEdit2,
                      width: 10.sp,
                      color: colorGreen2,
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 12.sp),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  '${DateFormat('MM/yyyy').format(widget.experience.timeStartWork)} - ${DateFormat('MM/yyyy').format(widget.experience.timeEndWork)}',
                  style: TextStyle(
                    fontSize: 11.sp,
                    color: colorGray1,
                  ),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                ),
                Image.asset(
                  iconMoreOption,
                  width: 20.sp,
                  color: colorGreen2,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
