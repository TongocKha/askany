import 'package:askany/src/helpers/date_time_helper.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class DateStep3Row extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _DateStep3RowState();
}

class _DateStep3RowState extends State<DateStep3Row> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 6.sp),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          ...getCalendarThisWeek(dateTime: DateTime.now()).map(
            (date) => Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5.sp),
                color: colorGreen3,
              ),
              padding: EdgeInsets.symmetric(
                horizontal: 8.65.sp,
                vertical: 12.sp,
              ),
              child: Column(
                children: [
                  Text(
                    dayNames[date.weekday - 1],
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 10.sp,
                    ),
                  ),
                  SizedBox(height: 6.sp),
                  Text(
                    addZeroPrefix(date.day),
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 13.sp,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
