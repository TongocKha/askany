import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/models/specialty_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/ui/authentication/screens/register_expert/widgets/button_action_bottom_sheet.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/checkbox/custom_check_box.dart';
import 'package:askany/src/ui/common/widgets/divider_bottom_sheet.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class SpecialtyBottomSheet extends StatefulWidget {
  final String title;
  final List<SpecialtyModel> specialtyModel;
  final Function(List<SpecialtyModel>) handleFinished;

  SpecialtyBottomSheet({
    Key? key,
    this.title = 'Lĩnh vực yêu cầu',
    required this.specialtyModel,
    required this.handleFinished,
  }) : super(key: key);

  @override
  _SpecialtyBottomSheet createState() => new _SpecialtyBottomSheet();
}

class _SpecialtyBottomSheet extends State<SpecialtyBottomSheet> {
  late List<SpecialtyModel> _specialties;

  @override
  void initState() {
    super.initState();
    _specialties = widget.specialtyModel;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      clipBehavior: Clip.antiAlias,
      decoration: BoxDecoration(
        color: colorWhite,
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(18.sp),
        ),
      ),
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(height: 5.sp),
            DividerBottomSheet(),
            SizedBox(height: 20.sp),
            Container(
              alignment: Alignment.topLeft,
              margin: EdgeInsets.only(left: 16.sp),
              child: Text(
                widget.title,
                style: TextStyle(
                  fontSize: 15.sp,
                  fontWeight: FontWeight.w700,
                  color: colorBlack2,
                ),
              ),
            ),
            SizedBox(height: 10.sp),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 16.sp, vertical: 4.sp),
              child: dividerChat,
            ),
            Container(
              height: 62.h,
              child: ListView.builder(
                itemCount: AppBloc.requestBloc.specialties.length,
                physics: BouncingScrollPhysics(),
                itemBuilder: (BuildContext context, int index) {
                  return TouchableOpacity(
                    onTap: () {
                      itemChange(AppBloc.requestBloc.specialties[index]);
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(
                        horizontal: 16.sp,
                        vertical: 14.sp,
                      ),
                      color: Colors.transparent,
                      child: Row(
                        children: [
                          Expanded(
                            child: Text(
                              AppBloc.requestBloc.specialties[index]
                                      .getLocaleTitle ??
                                  '',
                              style: TextStyle(
                                fontSize: 13.sp,
                              ),
                            ),
                          ),
                          CustomCheckBox(
                            isChecked: _specialties.indexWhere((element) =>
                                    AppBloc.requestBloc.specialties[index].id ==
                                    element.id) !=
                                -1,
                            onChanged: (val) {
                              itemChange(
                                  AppBloc.requestBloc.specialties[index]);
                            },
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
            SizedBox(height: 8.sp),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.sp),
              child: Row(
                children: [
                  Expanded(
                    child: ButtonActionBottomSheet(
                      handlePressed: () => itemResetAll(),
                      text: Strings.clearAll.i18n,
                    ),
                  ),
                  SizedBox(width: 12.sp),
                  Expanded(
                    child: ButtonActionBottomSheet(
                      handlePressed: () {
                        widget.handleFinished(_specialties);
                        AppNavigator.pop();
                      },
                      isPrimary: true,
                      text: Strings.ok.i18n,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 30.sp),
          ],
        ),
      ),
    );
  }

  void itemChange(SpecialtyModel specialtyModel) {
    int indexOfSpecialty = _specialties.indexWhere(
      (specialty) => specialty.id == specialtyModel.id,
    );

    if (indexOfSpecialty == -1) {
      setState(() {
        _specialties.add(specialtyModel);
      });
    } else {
      setState(() {
        _specialties.removeAt(indexOfSpecialty);
      });
    }
  }

  void itemResetAll() {
    setState(() {
      _specialties.clear();
    });
  }
}
