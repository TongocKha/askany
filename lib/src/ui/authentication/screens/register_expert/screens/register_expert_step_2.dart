import 'package:askany/src/configs/lang/localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/experiences/experiences_bloc.dart';
import 'package:askany/src/models/experience_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/account/widgets/skill_shimmer_card.dart';
import 'package:askany/src/ui/authentication/screens/register_expert/screens/form_add_experience.dart';
import 'package:askany/src/ui/authentication/screens/register_expert/widgets/experience_card.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/pagination_list_view.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class RegisterExpertStep2 extends StatefulWidget {
  final Function()? onFocusToTheLastTextField;
  final GlobalKey<FormState> formKey;
  final ExperienceModel experience;
  final Function(ExperienceModel) onChanged;
  const RegisterExpertStep2({
    Key? key,
    this.onFocusToTheLastTextField,
    required this.formKey,
    required this.experience,
    required this.onChanged,
  }) : super(key: key);

  @override
  _RegisterExpertStep2State createState() => _RegisterExpertStep2State();
}

class _RegisterExpertStep2State extends State<RegisterExpertStep2> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Form(
        child: Container(
          padding: EdgeInsets.only(
            left: 16.sp,
            right: 16.sp,
            bottom: 18.sp,
          ),
          child: BlocBuilder<ExperiencesBloc, ExperiencesState>(
            builder: (context, state) {
              List<ExperienceModel> _experiences =
                  (state.props[0] as List).cast();
              return _experiences.isEmpty
                  ? SingleChildScrollView(
                      child: FormAddExperience(
                        onFocusToLastTextField:
                            widget.onFocusToTheLastTextField ?? () {},
                        formKey: widget.formKey,
                        onChanged: widget.onChanged,
                        experience: widget.experience,
                      ),
                    )
                  : Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: 12.sp),
                        Text(
                          Strings.yourExperience.i18n,
                          style: TextStyle(
                            color: colorBlack2,
                            fontSize: 18.sp,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        SizedBox(height: 12.sp),
                        Expanded(
                          child: PaginationListView(
                            callBackRefresh: (handleFinished) {
                              AppBloc.experienceBloc.add(
                                RefreshExperienceEvent(
                                    handleFinished: handleFinished),
                              );
                            },
                            padding: EdgeInsets.only(bottom: 40.sp),
                            childShimmer: SkillShimmerCard(),
                            physics: BouncingScrollPhysics(),
                            itemCount: _experiences.length + 1,
                            itemBuilder: (context, index) {
                              return index == _experiences.length
                                  ? TouchableOpacity(
                                      onTap: () {
                                        AppNavigator.push(
                                          Routes.ADD_EXPERIENCE,
                                        );
                                      },
                                      child: Container(
                                        margin: EdgeInsets.symmetric(
                                            vertical: 12.sp),
                                        child: Text(
                                          Strings
                                              .createNewExprienceWithPlus.i18n,
                                          style: TextStyle(
                                            color: colorFontGreen,
                                            fontSize: 13.sp,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    )
                                  : ExperienceCard(
                                      experience: _experiences[index],
                                    );
                            },
                          ),
                        ),
                      ],
                    );
            },
          ),
        ),
      ),
    );
  }
}
