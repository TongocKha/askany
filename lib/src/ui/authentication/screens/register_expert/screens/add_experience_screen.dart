import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/models/experience_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/scaffold_wrapper.dart';
import 'package:askany/src/ui/authentication/screens/register_expert/screens/form_add_experience.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_confirm_cancel.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class AddExperienceScreen extends StatefulWidget {
  final ExperienceModel? experience;
  const AddExperienceScreen({this.experience});
  @override
  State<StatefulWidget> createState() => _AddExperienceScreenState();
}

class _AddExperienceScreenState extends State<AddExperienceScreen> {
  final _scrollController = ScrollController();
  @override
  void initState() {
    super.initState();
  }

  _scrollToMaxExtent() {
    Future.delayed(
      Duration(milliseconds: DELAY_100_MS),
      () {
        SchedulerBinding.instance?.addPostFrameCallback(
          (_) {
            _scrollController.animateTo(
                _scrollController.position.maxScrollExtent,
                duration: Duration(milliseconds: DELAY_100_MS),
                curve: Curves.ease);
          },
        );
      },
    );
  }

  Future<void> onBackPressed() {
    return dialogAnimationWrapper(
      child: DialogConfirmCancel(
        cancelText: 'Huỷ',
        bodyAfter:
            'Bạn có chắc chắn muốn rời khỏi màn hình này, thông tin bạn nhập sẽ không được lưu lại?',
        onConfirmed: () {
          AppNavigator.pop();
          AppNavigator.pop();
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ScaffoldWrapper(
      child: Scaffold(
        appBar: appBarTitleBack(
          context,
          (widget.experience == null
                  ? Strings.add.i18n
                  : Strings.editAppbarText.i18n) +
              ' ${Strings.experienceInWork.i18n}',
          onBackPressed: onBackPressed,
        ),
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 16.sp),
          child: SingleChildScrollView(
            controller: _scrollController,
            child: Column(
              children: [
                FormAddExperience(
                  onFocusToLastTextField: _scrollToMaxExtent,
                  experience: widget.experience,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
