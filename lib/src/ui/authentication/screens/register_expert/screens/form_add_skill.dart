import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/models/budget_model.dart';
import 'package:askany/src/models/position_model.dart';
import 'package:askany/src/models/skill_model.dart';
import 'package:askany/src/models/specialty_model.dart';
import 'package:askany/src/ui/account/widgets/bottom_sheet_position.dart';
import 'package:askany/src/ui/account/widgets/bottom_sheet_specialty.dart';
import 'package:askany/src/ui/account/widgets/money_service_with_time_card.dart';
import 'package:askany/src/ui/account/widgets/text_title.dart';
import 'package:askany/src/ui/common/widgets/button_ui/switch_button.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_form.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_form_request.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:flutter/scheduler.dart';

class FormAddSkill extends StatefulWidget {
  final GlobalKey<FormState> formKey;
  final SkillModel skill;
  final Function(SkillModel) onChanged;
  const FormAddSkill(
      {required this.formKey, required this.onChanged, required this.skill});
  @override
  State<StatefulWidget> createState() => _FormAddSkillState();
}

class _FormAddSkillState extends State<FormAddSkill> {
  late final SkillModel _skill;
  String _price15DropDown = Strings.vnd.i18n;
  String _price60DropDown = Strings.vnd.i18n;
  bool _isDirectSupport = true;
  List<String> _listKeyWordService = [];
  PositionModel? _positionModel;
  String _specialty = '';

  List<String> _listPrice = [Strings.vnd.i18n, Strings.usd.i18n];
  final _controller15Minute = TextEditingController();
  final _controller60Minute = TextEditingController();
  final _controllerNameSkill = TextEditingController();
  final _controllerNumYearSkill = TextEditingController();
  final _controllerNameNowCompany = TextEditingController();
  final _controllerDescribe = TextEditingController();
  final _scrollController = ScrollController();
  TextEditingController _specialtyController = TextEditingController();
  TextEditingController _positionController = TextEditingController(
      text: Strings.selectYourHighestPositionHintext.i18n);

  List<String> specialtiesTitle = [];
  List<SpecialtyModel> myFilterSpecialties = [];

  @override
  void initState() {
    super.initState();
    // Initial specialties
    List<SpecialtyModel> filterSpecialties = AppBloc.requestBloc.specialties
        .where((item) => item.getLocaleTitle != null)
        .toList();

    specialtiesTitle =
        filterSpecialties.map((item) => item.getLocaleTitle!).toSet().toList();

    _skill = widget.skill;

    _controller15Minute.addListener(() {
      _skill.callPrice = BudgetModel(
        currency: _price15DropDown.toLowerCase(),
        cost:
            double.tryParse(_controller15Minute.text.replaceAll(',', '')) ?? 0,
        totalMinutes: 60,
      );

      widget.onChanged(_skill);
    });

    _controller60Minute.addListener(() {
      _skill.meetPrice = BudgetModel(
        currency: _price60DropDown.toLowerCase(),
        cost: _isDirectSupport
            ? double.tryParse(_controller60Minute.text.replaceAll(',', '')) ?? 0
            : 0.0,
        totalMinutes: 60,
      );

      widget.onChanged(_skill);
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      controller: _scrollController,
      child: Form(
        key: widget.formKey,
        child: Container(
          padding:
              EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
          child: Column(
            children: [
              Container(
                color: colorWhite,
                margin: EdgeInsets.only(bottom: 6.sp),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 12.sp),
                    Text(
                      Strings.earnMoneySuggestion.i18n,
                      style: TextStyle(
                        color: colorBlack2,
                        fontSize: 18.sp,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    SizedBox(height: 18.sp),
                    TextTitle(
                        textTitle: Strings.selecrASectorSuggestionTitle.i18n),
                    SizedBox(height: 10.sp),
                    TextFieldForm(
                      onTap: () {
                        showModalBottomSheet(
                          context: context,
                          isScrollControlled: true,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.vertical(
                              top: Radius.circular(20.sp),
                            ),
                          ),
                          builder: (context) => BottomSheetSpecialty(
                            handlePressed: (val) {
                              _specialty = val;
                              setState(() {
                                _specialtyController.text = _specialty;
                              });

                              int indexSpecialty = AppBloc
                                  .requestBloc.specialties
                                  .indexWhere((spec) =>
                                      spec.getLocaleTitle == _specialty);

                              if (indexSpecialty != -1) {
                                SpecialtyModel _specialtyModel = AppBloc
                                    .requestBloc.specialties[indexSpecialty];

                                _skill.specialty = _specialtyModel;
                                widget.onChanged(_skill);
                              }
                            },
                            specialtiesTitle: specialtiesTitle,
                            specialty: _specialty,
                          ),
                        );
                      },
                      controller: _specialtyController,
                      hintText: Strings.selectASectorHintext.i18n,
                      isActive: true,
                      readOnly: true,
                      suffixIcon: Container(
                        color: Colors.transparent,
                        child: Image.asset(
                          iconArrowDown,
                          width: 10.sp,
                        ),
                      ),
                      validatorForm: (val) => _specialty.isEmpty
                          ? Strings.sectorCannotBeLeftBlankErrortext.i18n
                          : null,
                    ),
                    SizedBox(height: 22.sp),
                    TextTitle(textTitle: Strings.nameOfYourSkillTitle.i18n),
                    TextFieldForm(
                      controller: _controllerNameSkill,
                      fontSize: 13.sp,
                      onChanged: (val) {
                        if (val.trim().isNotEmpty) {
                          _skill.name = val.trim();
                          widget.onChanged(_skill);
                        }
                      },
                      contentPadding: EdgeInsets.all(10.sp),
                      hintText: Strings.skillNameHintext.i18n,
                      textInputAction: TextInputAction.go,
                      validatorForm: (val) =>
                          val!.isEmpty || val.trim().length == 0
                              ? Strings.skillNameCannotBeLeftBlankErrortext.i18n
                              : null,
                    ),
                    SizedBox(height: 22.sp),
                    TextTitle(
                        textTitle: Strings.yearsCountOfExprieceTitle.i18n),
                    TextFieldForm(
                      controller: _controllerNumYearSkill,
                      fontSize: 13.sp,
                      hintText: Strings.enterYearsCountOfExperienceHintext.i18n,
                      contentPadding: EdgeInsets.all(10.sp),
                      textInputType: TextInputType.number,
                      textInputAction: TextInputAction.go,
                      validatorForm: (val) => val!.isEmpty
                          ? Strings
                              .yearsCountOfExperienceCannotBeBlankErrortext.i18n
                          : null,
                      onChanged: (val) {
                        int? years = int.tryParse(val.trim());

                        if (years != null && years > 0) {
                          _skill.expExperience = years;
                          widget.onChanged(_skill);
                        }
                      },
                    ),
                    SizedBox(height: 22.sp),
                    TextTitle(
                        textTitle: Strings.whatYourHighestPositionTitle.i18n),
                    TextFieldForm(
                      onTap: () {
                        showModalBottomSheet(
                          context: context,
                          isScrollControlled: true,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.vertical(
                              top: Radius.circular(20.sp),
                            ),
                          ),
                          builder: (context) => BottomSheetPosition(
                            handlePressed: (val) {
                              int indexOfPosition = AppBloc.skillBloc.positions
                                  .indexWhere(
                                      (item) => item.getLocaleTitle == val);

                              if (indexOfPosition != -1) {
                                _positionModel = AppBloc
                                    .skillBloc.positions[indexOfPosition];
                                setState(() {
                                  _positionController.text =
                                      _positionModel?.getLocaleTitle ?? '';
                                });

                                _skill.highestPosition = _positionModel;
                                widget.onChanged(_skill);
                              }
                            },
                            listPosition: AppBloc.skillBloc.positions
                                .map((item) => item.getLocaleTitle!)
                                .toList(),
                            position: _positionModel?.getLocaleTitle,
                          ),
                        );
                      },
                      controller: _positionController,
                      hintText: Strings.selectYourHighestPositionHintext.i18n,
                      readOnly: true,
                      suffixIcon: Container(
                        color: Colors.transparent,
                        child: Image.asset(
                          iconArrowDown,
                          width: 10.sp,
                        ),
                      ),
                      validatorForm: (val) =>
                          _positionModel?.getLocaleTitle == null
                              ? Strings.positionCannotBeLeftBlankErrortext.i18n
                              : null,
                    ),
                    SizedBox(height: 22.sp),
                    TextTitle(
                        textTitle: Strings.whatYourCurrentCompanyTitle.i18n),
                    TextFieldForm(
                      contentPadding: EdgeInsets.all(10.sp),
                      controller: _controllerNameNowCompany,
                      fontSize: 13.sp,
                      hintText: Strings.companyNameHintext.i18n,
                      textInputAction: TextInputAction.go,
                      validatorForm: (val) => val == null || val.isEmpty ||
                              val.trim().length == 0
                          ? Strings.companyNameCannotBeLeftBlankErrortext.i18n
                          : null,
                      onChanged: (val) {
                        if (val.trim().isNotEmpty) {
                          _skill.companyName = val.trim();
                          widget.onChanged(_skill);
                        }
                      },
                    ),
                    SizedBox(height: 22.sp),
                    TextTitle(textTitle: Strings.skillDescriptionTitle.i18n),
                    TextFieldFormRequest(
                      controller: _controllerDescribe,
                      hintText: Strings.describeYourSkillHintext.i18n,
                      maxLines: 6,
                      validatorForm: (val) => val!.isEmpty ||
                              val.trim().length == 0
                          ? Strings.skillDescribeCannotBeLeftBlankErrorText.i18n
                          : null,
                      onChanged: (val) {
                        _skill.description = val.trim();
                        widget.onChanged(_skill);
                      },
                    ),
                  ],
                ),
              ),
              Container(
                width: 100.w,
                color: colorWhite,
                padding: EdgeInsets.symmetric(vertical: 30.sp),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    TextTitle(
                      textTitle: Strings.costOfASingleCallingServiceTitle.i18n,
                    ),
                    MoneyServiceWithTimeCard(
                      isVND: _price15DropDown == _listPrice[0],
                      margin: EdgeInsets.only(top: 10.sp),
                      textEditingController: _controller15Minute,
                      textMinute: Strings.fifteenMinutes.i18n,
                      onChangeTextForm: (value) => value!,
                      validatorTextForm: (val) {
                        if (val!.isEmpty) {
                          return Strings.enterPriceErrortext.i18n;
                        } else
                          return null;
                      },
                      onChangeDropDown: (value) {
                        if (value != _price15DropDown) {
                          setState(() {
                            _price15DropDown = value!;
                            _controller15Minute.text = '';
                          });
                        }
                        return _price15DropDown = value!;
                      },
                      hintDropdown: '',
                      valueDropDown: _price15DropDown,
                      listDropDown: _listPrice,
                    ),
                    SizedBox(height: 22.sp),
                    Visibility(
                      visible: _isDirectSupport,
                      child: Column(
                        children: [
                          TextTitle(
                            textTitle:
                                Strings.costOfASingleMeetingServiceTitle.i18n,
                          ),
                          MoneyServiceWithTimeCard(
                            isVND: _price60DropDown == _listPrice[0],
                            margin: EdgeInsets.only(top: 10.sp),
                            textEditingController: _controller60Minute,
                            onChangeTextForm: (value) => value!,
                            textMinute: Strings.sixtyMinutes.i18n,
                            hintDropdown: '',
                            valueDropDown: _price60DropDown,
                            validatorTextForm: (val) {
                              if (_isDirectSupport && val!.isEmpty) {
                                return Strings.enterPriceErrortext.i18n;
                              } else
                                return null;
                            },
                            onChangeDropDown: (value) {
                              if (value != _price60DropDown) {
                                setState(() {
                                  _price60DropDown = value!;
                                  _controller60Minute.text = '';
                                });
                              }
                              return value!;
                            },
                            listDropDown: _listPrice,
                          ),
                          SizedBox(height: 20.sp),
                        ],
                      ),
                    ),
                    Container(
                      child: Text(
                        Strings.participantsCountSuggetion.i18n,
                        textAlign: TextAlign.justify,
                        style: TextStyle(
                          height: 1.sp,
                          fontStyle: FontStyle.italic,
                          fontSize: 12.sp,
                          color: colorFontGreen,
                        ),
                      ),
                    ),
                    SizedBox(height: 20.sp),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SwitchButton(
                            value: true,
                            text: Strings.call.i18n,
                          ),
                          SizedBox(width: 20.sp),
                          SwitchButton(
                            value: _isDirectSupport,
                            text: Strings.meet.i18n,
                            onToggle: (val) {
                              setState(() {
                                _isDirectSupport = val;
                              });
                            },
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 28.sp),
                      child: dividerChat,
                    ),
                    TextTitle(textTitle: Strings.serviceKeywordTitle.i18n),
                    SizedBox(height: 10.sp),
                    Container(
                      margin: EdgeInsets.only(left: 1.sp),
                      child: Wrap(
                        spacing: 6.0,
                        runSpacing: 6.0,
                        children: List<Widget>.generate(
                            _listKeyWordService.length, (index) {
                          return Container(
                            child: Chip(
                              padding: EdgeInsets.all(8.sp),
                              backgroundColor: colorAddButton,
                              label: Text(
                                _listKeyWordService[index],
                                style: TextStyle(fontSize: 12.sp),
                              ),
                              onDeleted: () {
                                setState(() {
                                  _listKeyWordService.removeAt(index);
                                });

                                _skill.keywords.clear();
                                _skill.keywords.addAll(_listKeyWordService);
                                widget.onChanged(_skill);
                              },
                              deleteIcon: ImageIcon(
                                AssetImage(iconRemove),
                                size: 15,
                                color: colorGray1,
                              ),
                              materialTapTargetSize:
                                  MaterialTapTargetSize.shrinkWrap,
                            ),
                          );
                        }),
                      ),
                    ),
                    SizedBox(height: 10.sp),
                    TextFieldForm(
                      onTap: () {
                        Future.delayed(
                          Duration(milliseconds: DELAY_100_MS),
                          () {
                            SchedulerBinding.instance!.addPostFrameCallback(
                              (_) {
                                _scrollController.animateTo(
                                    _scrollController.position.maxScrollExtent,
                                    duration:
                                        Duration(milliseconds: DELAY_100_MS),
                                    curve: Curves.ease);
                              },
                            );
                          },
                        );
                      },
                      initialValue: "",
                      hintText: Strings.serviceKeywordHintext.i18n,
                      textInputAction: TextInputAction.go,
                      submitForm: (value) {
                        if (_listKeyWordService.length < 3) {
                          setState(() {
                            _listKeyWordService.add(value!.trim());
                          });

                          _skill.keywords.clear();
                          _skill.keywords.addAll(_listKeyWordService);
                          widget.onChanged(_skill);
                        }
                      },
                      fontSize: 13.sp,
                      contentPadding: EdgeInsets.all(10.sp),
                      validatorForm: (val) {
                        if (_listKeyWordService.isNotEmpty) {
                          return null;
                        } else
                          return Strings.serviceKeyWordBlankErrortext.i18n;
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
