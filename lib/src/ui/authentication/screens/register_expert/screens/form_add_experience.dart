import 'package:askany/src/configs/lang/localization.dart';
import 'package:flutter/material.dart';

import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/experiences/experiences_bloc.dart';
import 'package:askany/src/helpers/date_time_helper.dart';
import 'package:askany/src/models/experience_model.dart';
import 'package:askany/src/models/position_model.dart';
import 'package:askany/src/models/slide_mode.dart';
import 'package:askany/src/models/specialty_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/ui/account/widgets/bottom_sheet_position.dart';
import 'package:askany/src/ui/authentication/screens/register_expert/widgets/specialty_bottom_sheet.dart';
import 'package:askany/src/ui/authentication/screens/register_expert/widgets/title_input_register.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_confirm_cancel.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_form.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_form_request.dart';
import 'package:askany/src/ui/request/widgets/request_dropdown_button.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class FormAddExperience extends StatefulWidget {
  final Function() onFocusToLastTextField;
  final ExperienceModel? experience;
  final GlobalKey<FormState>? formKey;
  final Function(ExperienceModel)? onChanged;
  const FormAddExperience({
    Key? key,
    required this.onFocusToLastTextField,
    this.experience,
    this.formKey,
    this.onChanged,
  }) : super(key: key);
  @override
  State<FormAddExperience> createState() => _FormAddExperienceState();
}

class _FormAddExperienceState extends State<FormAddExperience> {
  late final GlobalKey<FormState> _formKey;
  late ExperienceModel _experience;
  TextEditingController _companyController = TextEditingController();
  TextEditingController _descriptionController = TextEditingController();
  TextEditingController _positionController =
      TextEditingController(text: Strings.selectYourHighestPositionHintext.i18n);
  List<SpecialtyModel> _specialties = [];
  PositionModel? _positionModel;
  String _monthStart = '';
  String _yearStart = '';
  String _monthEnd = '';
  String _yearEnd = '';

  @override
  void initState() {
    super.initState();

    if (widget.formKey != null) {
      _formKey = widget.formKey!;
    } else {
      _formKey = GlobalKey<FormState>();
    }

    if (widget.experience != null) {
      _experience = widget.experience!;
      _companyController.text = _experience.companyName;
      _descriptionController.text = _experience.description;

      // Position
      int indexOfPosition = AppBloc.skillBloc.positions.indexWhere(
        (position) => position.id == _experience.position,
      );

      if (indexOfPosition != -1) {
        _positionModel = AppBloc.skillBloc.positions[indexOfPosition];
        _positionController.text = _positionModel?.getLocaleTitle ?? '';
      }

      // Specialties
      _experience.specialties.forEach((specialtyId) {
        int indexOfSpecialty = AppBloc.requestBloc.specialties.indexWhere(
          (specialty) => specialty.id == specialtyId,
        );

        if (indexOfSpecialty != -1) {
          _specialties.add(AppBloc.requestBloc.specialties[indexOfSpecialty]);
        }
      });

      // DateTime
      if (widget.formKey == null) {
        _monthStart = addZeroPrefix(_experience.timeStartWork.month.toString());
        _yearStart = _experience.timeStartWork.year.toString();
        _monthEnd = addZeroPrefix(_experience.timeEndWork.month.toString());
        _yearEnd = _experience.timeEndWork.year.toString();
      }
    }
  }

  _onChangedDate() {
    if (widget.onChanged != null) {
      _experience.timeStartWork = DateTime(
        int.tryParse(_yearStart) ?? 2,
        int.tryParse(_monthStart) ?? 2021,
      );

      _experience.timeEndWork = DateTime(
        int.tryParse(_yearEnd) ?? 2,
        int.tryParse(_monthEnd) ?? 2022,
      );

      widget.onChanged!(_experience);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        bottom: widget.formKey == null ? 0 : MediaQuery.of(context).viewInsets.bottom,
      ),
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Visibility(
              visible: widget.formKey != null,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 12.sp),
                  Text(
                    Strings.yourExperience.i18n,
                    style: TextStyle(
                      color: colorBlack2,
                      fontSize: 18.sp,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 16.sp),
            TitleInputRegister(text: Strings.company.i18n),
            TextFieldFormRequest(
              controller: _companyController,
              hintText: Strings.company.i18n,
              validatorForm: (val) => val!.isEmpty || val.trim().length == 0
                  ? Strings.companyNameCannotBeLeftBlankErrortext.i18n
                  : null,
              onChanged: (val) {
                if (val.isNotEmpty && widget.onChanged != null) {
                  _experience.companyName = val.trim();
                  widget.onChanged!(_experience);
                }
              },
            ),
            SizedBox(height: 16.sp),
            TitleInputRegister(text: Strings.specialization.i18n),
            Visibility(
              visible: _specialties.isNotEmpty,
              child: Padding(
                padding: EdgeInsets.only(top: 10.sp),
                child: Wrap(
                  spacing: 6.0,
                  runSpacing: 6.0,
                  children: List.generate(_specialties.length, (index) {
                    return _itemPicked(
                      text: _specialties[index].getLocaleTitle ?? '',
                      handleDelete: () {
                        setState(() {
                          _specialties.removeAt(index);
                        });

                        if (widget.onChanged != null) {
                          _experience.specialties.clear();
                          _experience.specialties.addAll(_specialties.map((e) => e.id).toList());
                          widget.onChanged!(_experience);
                        }
                      },
                    );
                  }),
                ),
              ),
            ),
            TextFieldForm(
              onTap: () {
                showModalBottomSheet<void>(
                  context: context,
                  backgroundColor: Colors.transparent,
                  isScrollControlled: true,
                  builder: (context) {
                    return SpecialtyBottomSheet(
                      title: Strings.sectorOfExpertise.i18n,
                      specialtyModel: _specialties,
                      handleFinished: (result) {
                        setState(() {
                          _specialties = result;
                        });

                        if (widget.onChanged != null) {
                          _experience.specialties.clear();
                          _experience.specialties.addAll(_specialties.map((e) => e.id).toList());
                          widget.onChanged!(_experience);
                        }
                      },
                    );
                  },
                );
              },
              contentPadding: EdgeInsets.fromLTRB(
                10.sp,
                20.sp,
                10.sp,
                0.sp,
              ),
              readOnly: true,
              hintText: '',
              controller: TextEditingController(text: Strings.specialization.i18n),
              validatorForm: (val) => _specialties.isEmpty
                  ? Strings.specializationCannotBeLeftBlankErrortext.i18n
                  : null,
              suffixIcon: TouchableOpacity(
                onTap: () {},
                child: Image.asset(
                  iconArrowDown,
                  width: 10.sp,
                ),
              ),
            ),
            SizedBox(height: 16.sp),
            TitleInputRegister(text: Strings.positionInWork.i18n),
            TextFieldForm(
              onTap: () {
                showModalBottomSheet(
                  context: context,
                  isScrollControlled: true,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.vertical(
                      top: Radius.circular(20.sp),
                    ),
                  ),
                  builder: (context) => BottomSheetPosition(
                    handlePressed: (val) {
                      int indexOfPosition = AppBloc.skillBloc.positions
                          .indexWhere((item) => item.getLocaleTitle == val);

                      if (indexOfPosition != -1) {
                        _positionModel = AppBloc.skillBloc.positions[indexOfPosition];
                        setState(() {
                          _positionController.text = _positionModel?.getLocaleTitle ?? '';
                        });

                        if (widget.onChanged != null) {
                          _experience.position = _positionModel!.id;
                          widget.onChanged!(_experience);
                        }
                      }
                    },
                    listPosition:
                        AppBloc.skillBloc.positions.map((item) => item.getLocaleTitle!).toList(),
                    position: _positionModel?.getLocaleTitle,
                  ),
                );
              },
              controller: _positionController,
              hintText: Strings.selectYourHighestPositionHintext.i18n,
              readOnly: true,
              suffixIcon: Container(
                color: Colors.transparent,
                child: Image.asset(
                  iconArrowDown,
                  width: 10.sp,
                ),
              ),
              validatorForm: (val) =>
                  _positionModel == null ? Strings.positionCannotBeLeftBlankErrortext.i18n : null,
            ),
            SizedBox(height: 16.sp),
            TitleInputRegister(text: Strings.fromTime.i18n),
            SizedBox(height: 10.sp),
            Row(
              children: [
                Expanded(
                  child: RequestDropdownButton(
                    hintText: widget.experience == null || widget.experience!.description.isEmpty
                        ? 'Tháng'
                        : _monthStart,
                    onChanged: (val) {
                      setState(() {
                        _monthStart = val!;
                      });

                      _onChangedDate();
                    },
                    items: month,
                    centerTextItem: true,
                    validatorDropDown: (val) =>
                        widget.experience == null && (val == null || val.isEmpty)
                            ? Strings.monthCannotBeLeftBlankErrortext.i18n
                            : null,
                  ),
                ),
                SizedBox(width: 10.sp),
                Expanded(
                  child: RequestDropdownButton(
                    hintText: widget.experience == null || widget.experience!.description.isEmpty
                        ? Strings.year.i18n
                        : _yearStart,
                    onChanged: (val) {
                      setState(() {
                        _yearStart = val!;
                      });

                      _onChangedDate();
                    },
                    items: year,
                    centerTextItem: true,
                    validatorDropDown: (val) =>
                        widget.experience == null && (val == null || val.isEmpty)
                            ? Strings.yearCannotBeLeftBlankErrortext.i18n
                            : null,
                  ),
                ),
              ],
            ),
            SizedBox(height: 16.sp),
            TitleInputRegister(text: Strings.toTime.i18n),
            SizedBox(height: 10.sp),
            Row(
              children: [
                Expanded(
                  child: RequestDropdownButton(
                    hintText: widget.experience == null || widget.experience!.description.isEmpty
                        ? 'Tháng'
                        : _monthEnd,
                    onChanged: (val) {
                      setState(() {
                        _monthEnd = val!;
                      });

                      _onChangedDate();
                    },
                    items: month,
                    centerTextItem: true,
                    validatorDropDown: (val) =>
                        widget.experience == null && (val == null || val.isEmpty)
                            ? Strings.monthCannotBeLeftBlankErrortext.i18n
                            : null,
                  ),
                ),
                SizedBox(width: 10.sp),
                Expanded(
                  child: RequestDropdownButton(
                    hintText: widget.experience == null || widget.experience!.description.isEmpty
                        ? 'Năm'
                        : _yearEnd,
                    onChanged: (val) {
                      setState(() {
                        _yearEnd = val!;
                      });

                      _onChangedDate();
                    },
                    items: year,
                    centerTextItem: true,
                    validatorDropDown: (val) =>
                        widget.experience == null && (val == null || val.isEmpty)
                            ? Strings.yearCannotBeLeftBlankErrortext.i18n
                            : null,
                  ),
                ),
              ],
            ),
            SizedBox(height: 16.sp),
            TitleInputRegister(text: Strings.detailsOfWork.i18n),
            TextFieldFormRequest(
              onTap: widget.onFocusToLastTextField,
              controller: _descriptionController,
              maxLines: 7,
              hintText: Strings.enterDescriptionAboutYouHintext.i18n,
              validatorForm: (val) => val!.trim().length == 0
                  ? Strings.descriptionAboutYouCannotBeLeftBlankErrortext.i18n
                  : null,
              onChanged: (val) {
                if (val.isNotEmpty && widget.onChanged != null) {
                  _experience.description = val.trim();
                  widget.onChanged!(_experience);
                }
              },
            ),
            Visibility(
              visible: widget.formKey == null,
              child: Column(
                children: [
                  Container(
                      margin: EdgeInsets.only(top: 24.sp),
                      child: ButtonPrimary(
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            showDialogLoading();
                            ExperienceModel experienceModel = ExperienceModel(
                              id: widget.experience == null ? '' : widget.experience!.id,
                              companyName: _companyController.text,
                              specialties: _specialties.map((e) => e.id).toList(),
                              position: _positionModel!.id,
                              timeStartWork:
                                  DateTime(int.parse(_yearStart), int.parse(_monthStart)),
                              timeEndWork: DateTime(int.parse(_yearEnd), int.parse(_monthEnd)),
                              description: _descriptionController.text,
                              status: 1,
                              author: AppBloc.userBloc.getAccount.id!,
                              createdAt: DateTime.now(),
                            );
                            if (widget.experience == null) {
                              // Create
                              AppBloc.experienceBloc.add(
                                AddExperienceEvent(experience: experienceModel),
                              );
                            } else {
                              // Edit
                              AppBloc.experienceBloc.add(
                                UpdateExperienceEvent(experience: experienceModel),
                              );
                            }
                          }
                        },
                        text: 'Lưu',
                      )),
                  Visibility(
                      visible: widget.experience != null,
                      child: TouchableOpacity(
                        onTap: () {
                          dialogAnimationWrapper(
                            slideFrom: SlideMode.bot,
                            child: DialogConfirmCancel(
                              onConfirmed: () {
                                AppNavigator.pop();
                                showDialogLoading();
                                AppBloc.experienceBloc.add(
                                  DeleteExperienceEvent(
                                    experienceId: widget.experience!.id,
                                  ),
                                );
                              },
                              confirmText: Strings.clear.i18n,
                              bodyBefore: Strings.areYouSureToClearThisExperience.i18n,
                              cancelText: Strings.cancel.i18n,
                            ),
                          );
                        },
                        child: Container(
                          width: 100.w,
                          height: 40.sp,
                          margin: EdgeInsets.symmetric(vertical: 6.sp, horizontal: 16.sp),
                          padding: EdgeInsets.symmetric(vertical: 8.sp),
                          color: Colors.transparent,
                          child: Center(
                            child: Text(
                              Strings.deleteThisExperience.i18n,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: colorGray1,
                                fontSize: 13.sp,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        ),
                      )),
                ],
              ),
            ),
            SizedBox(
              height: 22.sp,
            ),
          ],
        ),
      ),
    );
  }

  Widget _itemPicked({required String text, required Function handleDelete}) {
    return Container(
      decoration: BoxDecoration(
        color: colorGreen2,
        borderRadius: BorderRadius.circular(6.5.sp),
      ),
      padding: EdgeInsets.symmetric(
        vertical: 8.sp,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(width: 12.sp),
          Text(
            text,
            style: TextStyle(
              fontSize: 12.sp,
              color: colorWhiteCard,
            ),
          ),
          SizedBox(width: 8.sp),
          TouchableOpacity(
            onTap: () {
              handleDelete();
            },
            child: Image.asset(
              iconRemove,
              width: 11.25.sp,
              height: 11.25.sp,
              color: colorWhiteCard,
            ),
          ),
          SizedBox(width: 8.sp),
        ],
      ),
    );
  }
}
