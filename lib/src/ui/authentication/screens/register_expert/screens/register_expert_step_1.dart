import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/models/expert_model.dart';
import 'package:askany/src/models/language_model.dart';
import 'package:askany/src/models/position_model.dart';
import 'package:askany/src/models/specialty_model.dart';
import 'package:askany/src/ui/account/widgets/bottom_sheet_position.dart';
import 'package:askany/src/ui/authentication/screens/register_expert/widgets/language_bottom_sheet.dart';
import 'package:askany/src/ui/authentication/screens/register_expert/widgets/specialty_bottom_sheet.dart';
import 'package:askany/src/ui/authentication/screens/register_expert/widgets/title_input_register.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_form.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_form_request.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class RegisterExpertStep1 extends StatefulWidget {
  final GlobalKey<FormState> formKey;
  final ExpertModel expert;
  final Function(ExpertModel) onChanged;
  const RegisterExpertStep1(
      {required this.formKey, required this.onChanged, required this.expert});
  @override
  _RegisterExpertStep1State createState() => _RegisterExpertStep1State();
}

class _RegisterExpertStep1State extends State<RegisterExpertStep1> {
  TextEditingController _yearExperienceController = TextEditingController();
  TextEditingController _companyNameController = TextEditingController();
  TextEditingController _descriptionController = TextEditingController();
  ScrollController _scrollController = ScrollController();
  List<LanguageModel> _languages = [];
  List<SpecialtyModel> _specialties = [];
  PositionModel? _positionModel;
  String _enteredText = '';
  late ExpertModel _expert;
  TextEditingController _positionController =
      TextEditingController(text: Strings.positionInWork.i18n);

  @override
  void initState() {
    super.initState();
    _expert = widget.expert;

    // Position
    int indexOfPosition = AppBloc.skillBloc.positions.indexWhere(
      (position) => position.id == _expert.highestPosition,
    );

    if (indexOfPosition != -1) {
      _positionModel = AppBloc.skillBloc.positions[indexOfPosition];
    }

    // Others
    if (_expert.experienceYears > 0) {
      _yearExperienceController.text = _expert.experienceYears.toString();
    }
    _companyNameController.text = _expert.companyName;
    _descriptionController.text = _expert.introduce;
    _enteredText = _expert.introduce;
    _languages.addAll(_expert.languages ?? []);
    _specialties.addAll(_expert.specialties);
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      controller: _scrollController,
      physics: BouncingScrollPhysics(),
      child: Container(
        child: Form(
          key: widget.formKey,
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: 16.sp),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 12.sp),
                    Text(
                      Strings.createYourSelfAIncredibleProfileSuggestion.i18n,
                      style: TextStyle(
                        color: colorBlack2,
                        fontSize: 18.sp,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    SizedBox(height: 16.sp),
                    Container(
                      alignment: Alignment.topLeft,
                      child: TitleInputRegister(
                          text: Strings.howManyLanguages.i18n),
                    ),
                    Container(
                      alignment: Alignment.topLeft,
                      child: Visibility(
                        visible: _languages.isNotEmpty,
                        child: Padding(
                          padding: EdgeInsets.only(top: 10.sp),
                          child: Wrap(
                            spacing: 6.0,
                            runSpacing: 6.0,
                            children: List.generate(_languages.length, (index) {
                              return _itemPicked(
                                text: _languages[index].getLocaleTitle,
                                handleDelete: () {
                                  setState(() {
                                    _languages.removeAt(index);
                                  });

                                  _expert.languages = _languages;
                                  widget.onChanged(_expert);
                                },
                              );
                            }),
                          ),
                        ),
                      ),
                    ),
                    TextFieldForm(
                      onTap: () {
                        showModalBottomSheet<void>(
                          context: context,
                          backgroundColor: Colors.transparent,
                          isScrollControlled: true,
                          builder: (context) {
                            return LanguageBottomSheet(
                              languageModel: _languages,
                              handleFinished: (result) {
                                setState(() {
                                  _languages = result;
                                });

                                _expert.languages = _languages;
                                widget.onChanged(_expert);
                              },
                            );
                          },
                        );
                      },
                      initialValue: Strings.languages.i18n,
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      readOnly: true,
                      contentPadding: EdgeInsets.all(10.sp),
                      fontSize: 13.sp,
                      validatorForm: (val) => _languages.isEmpty
                          ? Strings.blankLanguageErrortext.i18n
                          : null,
                      suffixIcon: Image.asset(
                        iconArrowDown,
                        width: 10.sp,
                      ),
                    ),
                    SizedBox(
                      height: 21.sp,
                    ),
                    Container(
                      alignment: Alignment.topLeft,
                      child: TitleInputRegister(
                          text: Strings.whatSectorYouHaveExprence.i18n),
                    ),
                    Container(
                      alignment: Alignment.topLeft,
                      child: Visibility(
                        visible: _specialties.isNotEmpty,
                        child: Padding(
                          padding: EdgeInsets.only(top: 10.sp),
                          child: Wrap(
                            spacing: 6.0,
                            runSpacing: 6.0,
                            children:
                                List.generate(_specialties.length, (index) {
                              return _itemPicked(
                                text: _specialties[index].getLocaleTitle ?? '',
                                handleDelete: () {
                                  setState(() {
                                    _specialties.removeAt(index);
                                  });

                                  _expert.specialties = _specialties;
                                  widget.onChanged(_expert);
                                },
                              );
                            }),
                          ),
                        ),
                      ),
                    ),
                    TextFieldForm(
                      onTap: () {
                        showModalBottomSheet<void>(
                          context: context,
                          backgroundColor: Colors.transparent,
                          isScrollControlled: true,
                          builder: (context) {
                            return SpecialtyBottomSheet(
                              title: Strings.sectorOfExpertise.i18n,
                              specialtyModel: _specialties,
                              handleFinished: (result) {
                                setState(() {
                                  _specialties = result;
                                });

                                _expert.specialties = _specialties;
                                widget.onChanged(_expert);
                              },
                            );
                          },
                        );
                      },
                      initialValue: Strings.sector.i18n,
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      readOnly: true,
                      contentPadding: EdgeInsets.all(10.sp),
                      fontSize: 13.sp,
                      validatorForm: (val) => _specialties.isEmpty
                          ? Strings.sectorCannotBeLeftBlankErrortext.i18n
                          : null,
                      suffixIcon: Image.asset(
                        iconArrowDown,
                        width: 10.sp,
                      ),
                    ),
                    SizedBox(
                      height: 21.sp,
                    ),
                    Container(
                      alignment: Alignment.topLeft,
                      child: TitleInputRegister(
                          text: Strings.howManyYearsOfExprience.i18n),
                    ),
                    TextFieldFormRequest(
                      controller: _yearExperienceController,
                      hintText: Strings.yearsCountOfExperienceHinttext.i18n,
                      textInputType: TextInputType.number,
                      inputFormatters: [
                        LengthLimitingTextInputFormatter(2),
                      ],
                      validatorForm: (val) => val!.isEmpty ||
                              val.trim().length == 0
                          ? Strings
                              .yearsCountOfExperienceCannotBeBlankErrortext.i18n
                          : int.parse(_yearExperienceController.text) == 0
                              ? 'Số kinh nghiệm phải hơn 1 năm'
                              : int.parse(_yearExperienceController.text) > 40
                                  ? 'Giới hạn số năm kinh nghiệm là 40'
                                  : null,
                      onChanged: (val) {
                        int? years = int.tryParse(val.trim());
                        if (years != null && years > 0) {
                          _expert.experienceYears = years;
                          widget.onChanged(_expert);
                        }
                      },
                    ),
                    SizedBox(
                      height: 21.sp,
                    ),
                    Container(
                      alignment: Alignment.topLeft,
                      child: TitleInputRegister(
                          text: Strings.whatYourHighestPositionTitle.i18n),
                    ),
                    TextFieldForm(
                      onTap: () {
                        showModalBottomSheet(
                          context: context,
                          isScrollControlled: true,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.vertical(
                              top: Radius.circular(20.sp),
                            ),
                          ),
                          builder: (context) => BottomSheetPosition(
                            handlePressed: (val) {
                              int indexOfPosition = AppBloc.skillBloc.positions
                                  .indexWhere(
                                      (item) => item.getLocaleTitle == val);

                              if (indexOfPosition != -1) {
                                _positionModel = AppBloc
                                    .skillBloc.positions[indexOfPosition];
                                setState(() {
                                  _positionController.text =
                                      _positionModel?.getLocaleTitle ?? '';
                                });

                                _expert.highestPosition = _positionModel!.id;
                                widget.onChanged(_expert);
                              }
                            },
                            listPosition: AppBloc.skillBloc.positions
                                .map((item) => item.getLocaleTitle!)
                                .toList(),
                            position: _positionModel?.getLocaleTitle,
                          ),
                        );
                      },
                      controller: _positionController,
                      hintText: Strings.selectYourHighestPositionHintext.i18n,
                      readOnly: true,
                      suffixIcon: Container(
                        color: Colors.transparent,
                        child: Image.asset(
                          iconArrowDown,
                          width: 10.sp,
                        ),
                      ),
                      validatorForm: (val) => _positionModel == null
                          ? Strings.positionCannotBeLeftBlankErrortext.i18n
                          : null,
                    ),
                    SizedBox(
                      height: 21.sp,
                    ),
                    Container(
                      alignment: Alignment.topLeft,
                      child: TitleInputRegister(
                          text: Strings.yourCompanyYouWorkForTitle.i18n),
                    ),
                    TextFieldFormRequest(
                      controller: _companyNameController,
                      hintText: Strings.companyNameHintext.i18n,
                      validatorForm: (val) => val!.isEmpty ||
                              val.trim().length == 0
                          ? Strings.companyNameCannotBeLeftBlankErrortext.i18n
                          : null,
                      onChanged: (val) {
                        if (val.trim().isNotEmpty) {
                          _expert.companyName = val.trim();
                          widget.onChanged(_expert);
                        }
                      },
                    ),
                    SizedBox(
                      height: 21.sp,
                    ),
                    Container(
                      alignment: Alignment.topLeft,
                      child: TitleInputRegister(
                        text: Strings
                            .descriptionAboutYourExperienceAndSkills.i18n,
                      ),
                    ),
                    SizedBox(height: 10.sp),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6.sp),
                        border: Border.all(
                          color: colorBorderTextField,
                          width: 0.5.sp,
                        ),
                      ),
                      child: Column(
                        children: [
                          SizedBox(height: 4.sp),
                          TextFieldFormRequest(
                            onTap: () {
                              Future.delayed(
                                Duration(milliseconds: DELAY_100_MS),
                                () {
                                  SchedulerBinding.instance
                                      ?.addPostFrameCallback(
                                    (_) {
                                      _scrollController.animateTo(
                                          _scrollController
                                              .position.maxScrollExtent,
                                          duration: Duration(
                                              milliseconds: DELAY_100_MS),
                                          curve: Curves.ease);
                                    },
                                  );
                                },
                              );
                            },
                            controller: _descriptionController,
                            maxLines: 7,
                            maxLength: 300,
                            hintText:
                                Strings.enterDescriptionAboutYouHintext.i18n,
                            validatorForm: (val) => val!.isEmpty
                                ? Strings
                                    .descriptionAboutYouCannotBeLeftBlankErrortext
                                    .i18n
                                : null,
                            onChanged: (value) {
                              setState(() {
                                _enteredText = value;
                              });

                              if (value.trim().isNotEmpty) {
                                _expert.introduce = value.trim();
                                widget.onChanged(_expert);
                              }
                            },
                            inputFormatters: [
                              LengthLimitingTextInputFormatter(300),
                            ],
                          ),
                          Container(
                            padding: EdgeInsets.only(
                              right: 12.sp,
                              bottom: 8.sp,
                            ),
                            alignment: Alignment.bottomRight,
                            child: Text(
                              '${_enteredText.length.toString()}/300',
                              style: TextStyle(
                                color: colorGray2,
                                fontSize: 11.sp,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                  height: 18.sp + MediaQuery.of(context).viewInsets.bottom),
            ],
          ),
        ),
      ),
    );
  }

  Widget _itemPicked({required String text, required Function handleDelete}) {
    return Container(
      decoration: BoxDecoration(
        color: colorGreen2,
        borderRadius: BorderRadius.circular(6.5.sp),
      ),
      padding: EdgeInsets.symmetric(
        vertical: 8.sp,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(width: 12.sp),
          Text(
            text,
            style: TextStyle(
              fontSize: 12.sp,
              color: colorWhiteCard,
            ),
          ),
          SizedBox(width: 8.sp),
          TouchableOpacity(
            onTap: () {
              handleDelete();
            },
            child: Image.asset(
              iconRemove,
              width: 11.25.sp,
              height: 11.25.sp,
              color: colorWhiteCard,
            ),
          ),
          SizedBox(width: 8.sp),
        ],
      ),
    );
  }
}
