import 'package:askany/src/bloc/skill/skill_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/models/skill_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/account/widgets/skill_card.dart';
import 'package:askany/src/ui/authentication/screens/register_expert/screens/form_add_skill.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class RegisterExpertStep4 extends StatefulWidget {
  final GlobalKey<FormState> formKey;
  final SkillModel skill;
  final Function(SkillModel) onChanged;
  const RegisterExpertStep4(
      {required this.formKey, required this.skill, required this.onChanged});
  @override
  _RegisterExpertStep4State createState() => _RegisterExpertStep4State();
}

class _RegisterExpertStep4State extends State<RegisterExpertStep4> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Form(
        child: Container(
          padding: EdgeInsets.only(
            left: 16.sp,
            right: 16.sp,
            bottom: 18.sp,
          ),
          child: BlocBuilder<SkillBloc, SkillState>(
            builder: (context, state) {
              List<SkillModel> skills =
                  (state.props.isEmpty ? [] : (state.props[0] as List?) ?? [])
                      .cast();
              return skills.isEmpty
                  ? FormAddSkill(
                      formKey: widget.formKey,
                      skill: widget.skill,
                      onChanged: widget.onChanged,
                    )
                  : Column(
                      children: [
                        SizedBox(height: 12.sp),
                        Text(
                          Strings.earnMoneySuggestion.i18n,
                          style: TextStyle(
                            color: colorBlack2,
                            fontSize: 18.sp,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        SizedBox(height: 12.sp),
                        Expanded(
                          child: ListView.builder(
                            itemCount: skills.length + 1,
                            itemBuilder: (context, index) {
                              return index == skills.length
                                  ? TouchableOpacity(
                                      onTap: () {
                                        AppNavigator.push(Routes.ADD_SKILL);
                                      },
                                      child: Container(
                                        margin: EdgeInsets.only(top: 12.sp),
                                        child: Text(
                                          Strings.addMoreSkillsWithPlus.i18n,
                                          style: TextStyle(
                                            color: colorFontGreen,
                                            fontSize: 13.sp,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    )
                                  : SkillCard(
                                      title: skills[index].name,
                                      price:
                                          skills[index].callPrice!.costString,
                                      time: skills[index].meetPrice != null
                                          ? skills[index]
                                              .meetPrice!
                                              .totalMinutes
                                              .toString()
                                          : skills[index]
                                              .callPrice!
                                              .totalMinutes
                                              .toString(),
                                      bullet1: skills[index]
                                              .expExperience
                                              .toString() +
                                          (skills[index].expExperience >= 2
                                              ? Strings.yearsOfExperience.i18n
                                              : Strings.yearOfExperience.i18n),
                                      bullet2:
                                          skills[index].highestPosition != null
                                              ? skills[index]
                                                  .highestPosition!
                                                  .getLocaleTitle!
                                              : '',
                                      bullet3: skills[index].companyName,
                                      onTap: () {
                                        AppNavigator.push(Routes.ADD_SKILL,
                                            arguments: {
                                              'skillModel': skills[index],
                                            });
                                      },
                                    );
                            },
                          ),
                        ),
                      ],
                    );
            },
          ),
        ),
      ),
    );
  }
}
