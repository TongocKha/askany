import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/authentication/authentication_event.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/helpers/province_helper.dart';
import 'package:askany/src/helpers/utils/validator_utils.dart';
import 'package:askany/src/ui/authentication/widgets/auth_footer.dart';
import 'package:askany/src/ui/authentication/widgets/auth_form_field.dart';
import 'package:askany/src/ui/authentication/widgets/auth_term_text.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:askany/src/ui/filter/widgets/bottom_sheet_province.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class RegisterScreen extends StatefulWidget {
  final VoidCallback toggleView;
  final bool isExpert;
  const RegisterScreen({required this.toggleView, this.isExpert = false});

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _locationController = TextEditingController();
  FocusNode _emailFocus = FocusNode();
  FocusNode _passwordFocus = FocusNode();
  FocusNode _nameFocus = FocusNode();
  FocusNode _phoneFocus = FocusNode();
  String name = '';
  String email = '';
  String password = '';
  int? _textProvince;
  String phone = '';
  int? province;
  bool _passwordVisible = false;
  bool hidePassword = true;
  var _emailErr = Strings.invalidEmailErrortext.i18n;
  var _passErr = Strings.passwordMustBeAtLeast6CharactersErrortext.i18n;
  var _phoneErr = Strings.invalidEmailErrortext.i18n;
  var _fullnameErr = Strings.fullnameMustBeAtLeast3CharactersErrortext.i18n;
  var _addressErr = Strings.addressCannotBeLeftBlankErrortext.i18n;

  hideKeyboard() => _emailFocus.unfocus();

  _trySubmitForm() {
    setState(() {
      _passwordVisible = _passwordVisible;
    });
    showDialogLoading();
    AppBloc.authBloc.add(
      RegisterEvent(
        email: email,
        password: password,
        province: _textProvince!,
        fullname: name,
        phone: phone,
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _locationController.text = Strings.address.i18n;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Padding(
              padding: EdgeInsets.only(left: 20.sp, right: 20.sp),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 50.sp, bottom: 50.sp),
                    child: Center(
                      child: Image.asset(
                        'assets/icons/ic_logo.png',
                        width: 100.sp,
                      ),
                    ),
                  ),
                  AuthFormField(
                    textVisible: true,
                    focusNode: _nameFocus,
                    onSubmitted: (value) => FocusScope.of(context).requestFocus(_emailFocus),
                    keyboardType: TextInputType.text,
                    hintText: Strings.nameTitle.i18n,
                    onChanged: (value) => name = value,
                    validator: (value) => value!.trim().length < 3 ? _fullnameErr : null,
                  ),
                  SizedBox(height: 10.sp),
                  AuthFormField(
                    textVisible: true,
                    focusNode: _emailFocus,
                    onSubmitted: (_) => FocusScope.of(context).requestFocus(_passwordFocus),
                    keyboardType: TextInputType.text,
                    validator: (value) => ValidatorUtils.isEmail(value!.trim()) ? null : _emailErr,
                    hintText: Strings.emailHintext.i18n,
                    onChanged: (value) => email = value.trim(),
                  ),
                  SizedBox(height: 10.sp),
                  AuthFormField(
                    focusNode: _passwordFocus,
                    onSubmitted: (_) => FocusScope.of(context).requestFocus(_phoneFocus),
                    keyboardType: TextInputType.text,
                    hintText: Strings.passwordHintext.i18n,
                    textVisible: _passwordVisible,
                    validator: (value) => value!.trim().length < 6 ? _passErr : null,
                    suffixIcon: TouchableOpacity(
                      child: Padding(
                        padding: EdgeInsets.only(right: 8.sp),
                        child: Image.asset(
                          _passwordVisible ? iconEye : iconEyeOff,
                          width: 16.sp,
                        ),
                      ),
                      onTap: () {
                        setState(() {
                          _passwordVisible = !_passwordVisible;
                        });
                      },
                    ),
                    onChanged: (value) => password = value.trim(),
                  ),
                  SizedBox(height: 10.sp),
                  AuthFormField(
                    focusNode: _phoneFocus,
                    onSubmitted: (_) async {
                      await _trySubmitForm();
                    },
                    textVisible: true,
                    keyboardType: TextInputType.text,
                    validator: (value) => value!.trim().length < 9 ? _phoneErr : null,
                    hintText: Strings.phoneNumberHintext.i18n,
                    onChanged: (value) => phone = value.trim(),
                  ),
                  SizedBox(height: 10.sp),
                  AuthFormField(
                    onChanged: (val) => null,
                    textVisible: true,
                    onSubmitted: (val) => null,
                    hintText: '',
                    keyboardType: TextInputType.text,
                    onTap: () {
                      showModalBottomSheet(
                        isScrollControlled: true,
                        context: context,
                        backgroundColor: Colors.transparent,
                        builder: (context) {
                          return BottomSheetProvince(
                            province: _locationController.text,
                            handPressed: (value) {
                              setState(() {
                                _textProvince = ProvinceHelper().getCodeByProvince(value!)!;
                                _locationController.text =
                                    ProvinceHelper().getProvinceByCode(_textProvince!)!;
                              });
                            },
                          );
                        },
                      );
                    },
                    controller: _locationController,
                    suffixIcon: Image.asset(
                      iconArrowDown,
                      width: 10.sp,
                    ),
                    validator: (val) => _textProvince == null ? _addressErr : null,
                  ),
                  SizedBox(height: 30.sp),
                  ButtonPrimary(
                    onPressed: () {
                      if (_formKey.currentState!.validate()) {
                        _trySubmitForm();
                      }
                    },
                    text: Strings.signUp.i18n,
                    fontWeight: FontWeight.w700,
                  ),
                  SizedBox(height: 15.sp),
                  AuthTermText(),
                  AuthFooter(
                    isExpert: widget.isExpert,
                    onTap: () => widget.toggleView(),
                    normalText: Strings.alreadyHaveAnAccount.i18n,
                    tapableText: Strings.login.i18n,
                  ),
                  SizedBox(
                    height: 30.sp,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
