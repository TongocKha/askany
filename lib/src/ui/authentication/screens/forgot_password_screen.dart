import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/authentication/bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ForgotPasswordScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  String _email = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarTitleBack(
        context,
        Strings.forgotPasswordAppbartext.i18n,
      ),
      body: SafeArea(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 16.sp),
          child: Column(
            children: [
              SizedBox(height: 30.sp),
              Text(
                Strings.enterEmailToGetLinkSuggestion.i18n,
                textAlign: TextAlign.justify,
                style: TextStyle(
                  fontSize: 13.sp,
                  color: colorBlack2,
                  height: 1.35.sp,
                ),
              ),
              SizedBox(height: 18.sp),
              Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: TextFormField(
                          keyboardType: TextInputType.text,
                          style:
                              TextStyle(color: Colors.black, fontSize: 12.sp),
                          cursorColor: Theme.of(context).primaryColor,
                          decoration: InputDecoration(
                            hintText: Strings.emailHintext,
                            hintStyle:
                                TextStyle(color: colorBlack2, fontSize: 13.sp),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                color: colorGray2,
                              ),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                color: Theme.of(context).primaryColor,
                              ),
                            ),
                          ),
                          validator: (value) {
                            if (value!.trim().isEmpty) {
                              return Strings
                                  .emailCannotBeLeftBlankErrortext.i18n;
                            }
                            return null;
                          },
                          onChanged: (val) {
                            setState(() {
                              _email = val.trim();
                            });
                          },
                        ),
                      ),
                      SizedBox(
                        width: 5.sp,
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(height: 30.sp),
              ButtonPrimary(
                onPressed: () async {
                  showDialogLoading();
                  AppBloc.authBloc.add(
                    ForgotPasswordEvent(email: _email),
                  );
                },
                text: Strings.send.i18n,
              )
            ],
          ),
        ),
      ),
    );
  }
}
