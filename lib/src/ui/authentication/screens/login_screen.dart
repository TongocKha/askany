import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/authentication/bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/helpers/utils/validator_utils.dart';
import 'package:askany/src/models/account_remember.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/authentication/widgets/auth_footer.dart';
import 'package:askany/src/ui/authentication/widgets/auth_form_field.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/checkbox/custom_check_box.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class LoginScreen extends StatefulWidget {
  final VoidCallback toggleView;
  final bool isExpert;

  LoginScreen({required this.toggleView, required this.isExpert});

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  FocusNode usernameFocus = FocusNode();
  FocusNode passwordFocus = FocusNode();
  final _controllerEmail = TextEditingController();
  final _controllerPass = TextEditingController();
  String _email = '';
  String _password = '';
  bool _isRemember = true;
  bool _passwordVisible = false;
  var _emailErr = Strings.invalidEmailErrortext.i18n;
  var _passErr = Strings.passwordMustBeAtLeast6CharactersErrortext.i18n;
  List<Account>? listOptions = [];
  hideKeyboard() => usernameFocus.unfocus();

  @override
  void initState() {
    super.initState();
    _passwordVisible = false;
    listOptions = UserLocal().getAccountRemember() != null
        ? UserLocal().getAccountRemember()!.account
        : <Account>[];
  }

  Future<void> _trySubmitForm() async {
    final isValid = _formKey.currentState!.validate();
    if (isValid) {
      setState(() {
        _passwordVisible = _passwordVisible;
      });
      showDialogLoading();
      AppBloc.authBloc.add(
        LoginEvent(
          isExpert: widget.isExpert,
          isRemember: _isRemember,
          email: _email,
          password: _password,
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        height: 100.h,
        width: 100.w,
        margin: EdgeInsets.only(left: 20.sp, right: 20.sp),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                  child: Padding(
                    padding: EdgeInsets.only(
                      bottom: MediaQuery.of(context).viewInsets.bottom,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(top: 80.sp, bottom: 50.sp),
                          child: Center(
                            child: Image.asset(
                              'assets/icons/ic_logo.png',
                              width: 100.sp,
                            ),
                          ),
                        ),
                        RawAutocomplete<Account>(
                          textEditingController: _controllerEmail,
                          focusNode: usernameFocus,
                          optionsBuilder: (TextEditingValue textEditingValue) {
                            return listOptions!
                                .where((Account county) => county.email
                                    .toLowerCase()
                                    .startsWith(textEditingValue.text.toLowerCase()))
                                .toList();
                          },
                          displayStringForOption: (Account option) => option.email,
                          fieldViewBuilder: (BuildContext context,
                              TextEditingController fieldTextEditingController,
                              FocusNode fieldFocusNode,
                              VoidCallback onFieldSubmitted) {
                            return AuthFormField(
                              controller: fieldTextEditingController,
                              textVisible: true,
                              focusNode: usernameFocus,
                              // usernameFocus,
                              onSubmitted: (value) =>
                                  FocusScope.of(context).requestFocus(passwordFocus),
                              keyboardType: TextInputType.text,
                              hintText: Strings.emailHintext.i18n,
                              onChanged: (value) => _email = value.trim(),
                              validator: (value) =>
                                  ValidatorUtils.isEmail(value!.trim()) ? null : _emailErr,
                            );
                          },
                          onSelected: (Account selection) {},
                          optionsViewBuilder: (BuildContext context,
                              AutocompleteOnSelected<Account> onSelected,
                              Iterable<Account> options) {
                            return Align(
                                alignment: Alignment.topLeft,
                                child: Material(
                                  child: Container(
                                    width: 100.w - 38.sp,
                                    height: options.length <= 3 ? 52.sp * options.length : 180.sp,
                                    margin: EdgeInsets.only(top: 5.sp),
                                    decoration: BoxDecoration(
                                      color: colorWhite,
                                      borderRadius: BorderRadius.all(Radius.circular(4.sp)),
                                      boxShadow: [
                                        BoxShadow(
                                          offset: Offset(-1, -1),
                                          color: Colors.black.withOpacity(.1),
                                          blurRadius: 1,
                                        ),
                                        BoxShadow(
                                          offset: Offset(2, 2),
                                          color: Colors.black.withOpacity(.2),
                                          blurRadius: 2,
                                        ),
                                      ],
                                    ),
                                    child: ListView.builder(
                                      physics: BouncingScrollPhysics(),
                                      padding: EdgeInsets.all(10.sp),
                                      itemCount: options.length,
                                      itemBuilder: (BuildContext context, int index) {
                                        final Account option = options.elementAt(index);
                                        return TouchableOpacity(
                                          onTap: () {
                                            onSelected(option);
                                            _email = option.email;
                                            _password = option.password;
                                            _controllerPass.text = option.password;
                                          },
                                          child: Container(
                                            color: Colors.transparent,
                                            child: Column(
                                              children: [
                                                index > 0
                                                    ? Container(
                                                        margin:
                                                            EdgeInsets.symmetric(vertical: 8.sp),
                                                        child: dividerChat,
                                                      )
                                                    : SizedBox(),
                                                Row(
                                                  children: [
                                                    Expanded(
                                                      child: Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment.start,
                                                        children: [
                                                          Text(
                                                            option.email,
                                                            style: TextStyle(
                                                              color: colorBlack1,
                                                              fontSize: 12.sp,
                                                            ),
                                                          ),
                                                          SizedBox(height: 4.sp),
                                                          Text(
                                                            '***********',
                                                            style: TextStyle(
                                                              color: colorGray1,
                                                              fontSize: 12.sp,
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    TouchableOpacity(
                                                      onTap: () {
                                                        UserLocal()
                                                            .deleteAccountRemember(option.email);

                                                        int indexOfOptions =
                                                            listOptions?.indexWhere((opt) =>
                                                                    opt.email == option.email) ??
                                                                -1;

                                                        if (indexOfOptions != -1) {
                                                          listOptions?.removeAt(indexOfOptions);
                                                          setState(() {
                                                            _controllerEmail.text = '';
                                                          });
                                                        }
                                                      },
                                                      child: Icon(
                                                        Icons.close,
                                                        size: 18.sp,
                                                        color: colorGray2,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                        );
                                      },
                                    ),
                                  ),
                                ));
                          },
                        ),
                        SizedBox(
                          height: 10.sp,
                        ),
                        AuthFormField(
                          controller: _controllerPass,
                          focusNode: passwordFocus,
                          onSubmitted: (_) async {
                            await _trySubmitForm();
                          },
                          keyboardType: TextInputType.text,
                          hintText: Strings.passwordHintext.i18n,
                          textVisible: _passwordVisible,
                          suffixIcon: TouchableOpacity(
                            child: Padding(
                              padding: EdgeInsets.only(right: 8.sp),
                              child: Image.asset(
                                _passwordVisible ? iconEye : iconEyeOff,
                                width: 16.sp,
                              ),
                            ),
                            onTap: () {
                              setState(() {
                                _passwordVisible = !_passwordVisible;
                              });
                            },
                          ),
                          validator: (value) => value!.trim().length < 6 ? _passErr : null,
                          onChanged: (value) => _password = value.trim(),
                        ),
                        SizedBox(height: 15.sp),
                        TouchableOpacity(
                          onTap: () {
                            AppNavigator.push(Routes.FORGOT_PASSWORD);
                          },
                          child: Align(
                            alignment: Alignment.centerRight,
                            child: Text(
                              Strings.forgotPasswordAppbartext.i18n,
                              style: TextStyle(
                                fontSize: 11.sp,
                                color: colorGreen2,
                                decoration: TextDecoration.underline,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 25.sp),
                        ButtonPrimary(
                          onPressed: () async {
                            await _trySubmitForm();
                          },
                          text: Strings.login.i18n,
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 12.sp),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  CustomCheckBox(
                                    isChecked: _isRemember,
                                    onChanged: (val) {
                                      setState(() {
                                        _isRemember = val;
                                      });
                                    },
                                  ),
                                  SizedBox(width: 8.sp),
                                  TouchableOpacity(
                                    onTap: () {
                                      setState(() {
                                        _isRemember = !_isRemember;
                                      });
                                    },
                                    child: Text(
                                      Strings.keepMeLoggedIn.i18n,
                                      style: TextStyle(
                                        fontSize: 12.sp,
                                        color: colorBlack2,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                        AuthFooter(
                          isExpert: widget.isExpert,
                          normalText: Strings.newUser.i18n,
                          tapableText: Strings.signUp.i18n,
                          onTap: () {
                            widget.toggleView();
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
