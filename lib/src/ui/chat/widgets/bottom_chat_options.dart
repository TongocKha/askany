import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/chat/chat_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/models/conversation_model.dart';
import 'package:askany/src/models/slide_mode.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/bottom_sheet/bottom_delete.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_input.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class BottomChatOptions extends StatefulWidget {
  final ConversationModel conversation;
  const BottomChatOptions({required this.conversation});
  @override
  State<StatefulWidget> createState() => _BottomChatOptionsState();
}

class _BottomChatOptionsState extends State<BottomChatOptions> {
  late final List<String> chatOptions;

  @override
  void initState() {
    super.initState();
    chatOptions = [
      Strings.editName.i18n,
      Strings.expertProfile.i18n,
      if (!widget.conversation.latestMessage.isMe)
        widget.conversation.latestMessage.isSeen
            ? Strings.markAsUnread.i18n
            : Strings.markAsRead.i18n,
      widget.conversation.ignoreNotis.isEmpty
          ? Strings.muteNotification.i18n
          : Strings.unmuteNotification.i18n,
      Strings.deleteChat.i18n,
    ];

    if (!widget.conversation.isGroup) {
      chatOptions.removeAt(0);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 12.sp, horizontal: 10.sp),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            decoration: BoxDecoration(
              color: Colors.grey.shade100,
              borderRadius: BorderRadius.circular(8.sp),
            ),
            child: ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              padding: EdgeInsets.zero,
              shrinkWrap: true,
              itemCount: chatOptions.length,
              itemBuilder: (context, index) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    index > 0 ? dividerChat : SizedBox(),
                    _buildOption(
                      text: chatOptions[index],
                      isDanger: index == chatOptions.length - 1,
                      handlePressed: () {
                        if (chatOptions[index] == Strings.editName.i18n) {
                          dialogAnimationWrapper(
                            dismissible: true,
                            slideFrom: SlideMode.bot,
                            borderRadius: 9.sp,
                            child: DialogInput(
                              handleFinish: (input) {
                                AppBloc.chatBloc.add(
                                  ChangeConversationNameEvent(
                                    conversationId: widget.conversation.id,
                                    conversationName: input,
                                  ),
                                );
                              },
                              title: Strings.editNameOfChat.i18n,
                              buttonTitle: Strings.save.i18n.toUpperCase(),
                              initialValue: widget.conversation.conversationTitle,
                            ),
                          );
                        } else if (chatOptions[index] == Strings.expertProfile.i18n) {
                          AppNavigator.push(
                            Routes.DETAILS_SPECIALIST,
                            arguments: {
                              'expertId': widget.conversation.expert.id,
                            },
                          );
                        } else if ([Strings.markAsUnread.i18n, Strings.markAsRead.i18n]
                            .contains(chatOptions[index])) {
                          AppBloc.chatBloc.add(
                            MarkReadEvent(
                              conversationId: widget.conversation.id,
                              isSeen: !widget.conversation.latestMessage.isSeen,
                            ),
                          );
                        } else if ([Strings.muteNotification.i18n, Strings.unmuteNotification.i18n]
                            .contains(chatOptions[index])) {
                          AppBloc.chatBloc.add(
                            IgnoreConversationEvent(
                              conversationId: widget.conversation.id,
                            ),
                          );
                        } else if (chatOptions[index] == Strings.deleteChat.i18n) {
                          showModalBottomSheet(
                            context: context,
                            backgroundColor: Colors.transparent,
                            isDismissible: true,
                            isScrollControlled: true,
                            barrierColor: Colors.black38,
                            builder: (context) {
                              return BottomDelete(
                                handlePressed: () {
                                  AppBloc.chatBloc.add(
                                    DeleteConversationEvent(
                                      conversationId: widget.conversation.id,
                                    ),
                                  );
                                },
                              );
                            },
                          );
                        }
                      },
                    ),
                  ],
                );
              },
            ),
          ),
          SizedBox(height: 4.sp),
          Container(
            height: 40.sp,
            decoration: BoxDecoration(
              color: Colors.grey.shade100,
              borderRadius: BorderRadius.circular(8.sp),
            ),
            child: _buildOption(
              text: Strings.cancel.i18n,
              isCancel: true,
              handlePressed: () {},
            ),
          ),
          SizedBox(height: 4.sp),
        ],
      ),
    );
  }

  Widget _buildOption({
    required String text,
    required Function handlePressed,
    bool isDanger = false,
    bool isCancel = false,
  }) {
    return TouchableOpacity(
      onTap: () {
        AppNavigator.pop();
        handlePressed();
      },
      child: Container(
        height: 42.sp,
        alignment: Alignment.center,
        color: Colors.transparent,
        child: Text(
          text,
          style: TextStyle(
            color: isDanger ? Colors.red.shade700 : colorBlack2,
            fontSize: 13.sp,
            fontWeight: isCancel ? FontWeight.w700 : FontWeight.w600,
          ),
        ),
      ),
    );
  }
}
