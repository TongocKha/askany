import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/ui/chat/widgets/chat_shimmer_card.dart';
import 'package:flutter/material.dart';

class ChatListShimmer extends StatelessWidget {
  const ChatListShimmer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      itemCount: LIMIT_API_10,
      itemBuilder: (_, __) => ChatShimmerCard(),
    );
  }
}
