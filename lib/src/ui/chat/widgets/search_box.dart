import 'dart:async';

import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/chat/chat_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class SearchBox extends StatefulWidget {
  final EdgeInsetsGeometry? margin;
  SearchBox({this.margin});

  @override
  State<StatefulWidget> createState() => _SearchBoxState();
}

class _SearchBoxState extends State<SearchBox> {
  TextEditingController searchKey = TextEditingController();
  Timer? _debounce;

  @override
  void dispose() {
    _debounce?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 35.sp,
      width: 100.w,
      margin: widget.margin ?? EdgeInsets.symmetric(horizontal: 16.sp),
      decoration: BoxDecoration(
        border: Border.all(color: colorGrayBorder, width: .5.sp),
        borderRadius: BorderRadius.circular(8.sp),
      ),
      child: TextFormField(
        controller: searchKey,
        style: TextStyle(
          color: colorCaptionSearch,
          fontSize: 12.sp,
        ),
        keyboardType: TextInputType.multiline,
        maxLines: 1,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.only(
            top: 0.sp,
            right: 10.sp,
          ),
          hintText: Strings.searchHintext.i18n,
          hintStyle: TextStyle(
            color: colorCaptionSearch,
            fontSize: 12.sp,
            fontWeight: FontWeight.w400,
          ),
          filled: true,
          fillColor: Colors.transparent,
          border: OutlineInputBorder(
            borderSide: BorderSide.none,
          ),
          prefixIcon: Container(
            margin: EdgeInsets.all(10.sp),
            child: Image.asset(
              iconSearch,
            ),
          ),
          suffixIcon: searchKey.text.isEmpty
              ? SizedBox()
              : IconButton(
                  onPressed: () {
                    AppBloc.chatBloc.add(
                      SearchConversationEvent(
                        searchKey: '',
                      ),
                    );
                    searchKey.text = '';
                  },
                  icon: Icon(
                    Icons.close,
                    color: Color(0xFFA4A4A4),
                  ),
                ),
        ),
        onChanged: (val) {
          if (_debounce?.isActive ?? false) _debounce?.cancel();
          _debounce =
              Timer(const Duration(milliseconds: DELAY_HALF_SECOND), () {
            AppBloc.chatBloc.add(
              SearchConversationEvent(
                searchKey: val.trim(),
              ),
            );
          });

          setState(() {});
        },
      ),
    );
  }
}
