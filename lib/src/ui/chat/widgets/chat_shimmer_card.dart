import 'package:askany/src/ui/common/widgets/shimmers/fade_simmer.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ChatShimmerCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16.sp),
      color: Theme.of(context).scaffoldBackgroundColor,
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(vertical: 12.sp),
            child: Row(
              children: [
                FadeShimmer.round(
                  size: 42.sp,
                  fadeTheme: FadeTheme.lightReverse,
                ),
                SizedBox(width: 12),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        height: 20.sp,
                        alignment: Alignment.centerLeft,
                        child: FadeShimmer(
                          height: 13.sp,
                          width: 40.w,
                          fadeTheme: FadeTheme.lightReverse,
                        ),
                      ),
                      SizedBox(height: 3.5.sp),
                      Container(
                        height: 20.sp,
                        alignment: Alignment.centerLeft,
                        child: FadeShimmer(
                          height: 12.sp,
                          width: 50.w,
                          fadeTheme: FadeTheme.lightReverse,
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(width: 10.sp),
                FadeShimmer(
                  width: 35.sp,
                  height: 9.sp,
                  fadeTheme: FadeTheme.lightReverse,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
