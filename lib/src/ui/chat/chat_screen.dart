import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/chat/chat_bloc.dart';
import 'package:askany/src/bloc/video_call/video_call_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/helpers/permission_helper.dart';
import 'package:askany/src/models/account_model.dart';
import 'package:askany/src/models/conversation_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/chat/widgets/bottom_chat_options.dart';
import 'package:askany/src/ui/chat/widgets/chat_card.dart';
import 'package:askany/src/ui/chat/widgets/chat_list_shimmer.dart';
import 'package:askany/src/ui/chat/widgets/chat_shimmer_card.dart';
import 'package:askany/src/ui/chat/widgets/search_box.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/bottom_sheet/bottom_delete.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_slidable.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/empty/empty.dart';
import 'package:askany/src/ui/common/widgets/pagination_list_view.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class ChatScreen extends StatefulWidget {
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  final SlidableController slidableController = new SlidableController();

  _showBottomSheetOptions(ConversationModel conversation) {
    _closeSlidable();
    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      isDismissible: true,
      isScrollControlled: true,
      barrierColor: Colors.black38,
      builder: (context) {
        return BottomChatOptions(
          conversation: conversation,
        );
      },
    );
  }

  _closeSlidable() {
    slidableController.activeState?.close();
  }

  _startCalling(ConversationModel conversationModel) async {
    bool isCameraGranted =
        await PermissionHelper().checkPermissionAndRequest(Permission.camera);
    bool isMicGranted = await PermissionHelper()
        .checkPermissionAndRequest(Permission.microphone);
    if (isCameraGranted && isMicGranted) {
      AccountModel? receiverUser = conversationModel.receiverUser;
      if (receiverUser != null) {
        AppBloc.videoCallBloc.add(RequestVideoCallEvent(
          receiverId: receiverUser.id!,
          conversation: conversationModel,
        ));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarTitleBack(context, Strings.message.i18n),
      body: Container(
        child: Column(
          children: [
            dividerChat,
            SizedBox(height: 12.sp),
            SearchBox(),
            SizedBox(height: 8.sp),
            Expanded(
              child: BlocBuilder<ChatBloc, ChatState>(
                builder: (context, state) {
                  if (state is ConversationInitial) {
                    return ChatListShimmer();
                  }

                  List<ConversationModel> conversations =
                      (state.props[0] as List).cast();
                  List<String> isTypings = (state.props[1] as List).cast();

                  return conversations.isEmpty
                      ? Empty(
                          image: Image.asset(
                            imageChatEmpty,
                            width: 135.sp,
                            height: 93.sp,
                          ),
                          text: Strings.yourInboxIsEmptyNoti.i18n,
                        )
                      : PaginationListView(
                          padding: EdgeInsets.only(bottom: 20.sp),
                          itemCount: conversations.length,
                          childShimmer: ChatShimmerCard(),
                          isLoadMore: state is GettingConversation,
                          callBackLoadMore: () =>
                              AppBloc.chatBloc.add(GetConversationEvent()),
                          callBackRefresh: (Function handleFinished) {
                            AppBloc.chatBloc.add(
                              RefreshConversationEvent(
                                handleFinished: handleFinished,
                              ),
                            );
                          },
                          itemBuilder: (context, index) {
                            return Slidable(
                              actionPane: SlidableScrollActionPane(),
                              actionExtentRatio: 0.205,
                              controller: slidableController,
                              closeOnScroll: true,
                              actions: [
                                ButtonSlidable(
                                  title: Strings.delete.i18n,
                                  imageAsset: iconDelete,
                                  handlePressed: () {
                                    _closeSlidable();
                                    showModalBottomSheet(
                                      context: context,
                                      backgroundColor: Colors.transparent,
                                      isDismissible: true,
                                      isScrollControlled: true,
                                      barrierColor: Colors.black38,
                                      builder: (context) {
                                        return BottomDelete(
                                          handlePressed: () {
                                            AppBloc.chatBloc.add(
                                              DeleteConversationEvent(
                                                conversationId:
                                                    conversations[index].id,
                                              ),
                                            );
                                          },
                                        );
                                      },
                                    );
                                  },
                                  backgroundcolor: colorDelete,
                                  colorIcon: Colors.white,
                                ),
                              ],
                              secondaryActions: [
                                ButtonSlidable(
                                  title: Strings.add.i18n,
                                  handlePressed: () {
                                    _showBottomSheetOptions(
                                      conversations[index],
                                    );
                                  },
                                  backgroundcolor: colorMoreChat,
                                  imageAsset: iconMoreConversation,
                                  colorIcon: Colors.white,
                                ),
                                ButtonSlidable(
                                  title: Strings.call.i18n,
                                  handlePressed: () {
                                    _closeSlidable();
                                    _startCalling(conversations[index]);
                                  },
                                  backgroundcolor: colorBackgroundCall,
                                  imageAsset: iconPhoneRounded,
                                  colorIcon: colorFontGreen,
                                  colorText: colorFontGreen,
                                ),
                              ],
                              child: TouchableOpacity(
                                onLongPress: () => _showBottomSheetOptions(
                                  conversations[index],
                                ),
                                onTap: () {
                                  _closeSlidable();
                                  AppNavigator.push(Routes.CONVERSATION,
                                      arguments: {
                                        'conversationModel':
                                            conversations[index],
                                      });
                                },
                                child: ChatCard(
                                  isTyping: isTypings
                                      .contains(conversations[index].id),
                                  conversationModel: conversations[index],
                                ),
                              ),
                            );
                          },
                        );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
