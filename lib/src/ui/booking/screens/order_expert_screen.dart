import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/request/request_bloc.dart';
import 'package:askany/src/bloc/timeline/timeline_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/helpers/extentions/string_extention.dart';
import 'package:askany/src/models/budget_model.dart';
import 'package:askany/src/models/expert_model.dart';
import 'package:askany/src/models/skill_model.dart';
import 'package:askany/src/models/slide_mode.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/ui/booking/widgets/order_expert_step_one.dart';
import 'package:askany/src/ui/booking/widgets/order_expert_step_three.dart';
import 'package:askany/src/ui/booking/widgets/order_expert_step_two.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_confirm_cancel.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_with_text_and_pop_button.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:askany/src/ui/common/widgets/step_order_service.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class OrderExpertScreen extends StatefulWidget {
  final ExpertModel? expertModel;
  final SkillModel? skillModel;
  final String contactForm;
  final bool isCheckChatScreen;

  OrderExpertScreen({
    this.expertModel,
    this.skillModel,
    required this.contactForm,
    this.isCheckChatScreen = false,
  });

  @override
  State<OrderExpertScreen> createState() => _OrderExpertScreenState();
}

class _OrderExpertScreenState extends State<OrderExpertScreen> {
  late TimelineBloc _timelineBloc;
  late PageController _pageController;
  late List<Widget> _steps;
  int _currentIndex = 0;
  final _formKeys = [GlobalKey<FormState>(), GlobalKey<FormState>()];
  final TextEditingController _authorNameController =
      TextEditingController(text: AppBloc.userBloc.getAccount.fullname);
  final TextEditingController _authorPhoneController =
      TextEditingController(text: AppBloc.userBloc.getAccount.phone);
  final TextEditingController _noteController = TextEditingController();
  final TextEditingController _participantCountController =
      TextEditingController(text: '1');

  final TextEditingController _locationNameController =
      TextEditingController(text: '');
  final TextEditingController _locationAddressController =
      TextEditingController(text: '');

  final TextEditingController _adviseDurationController =
      TextEditingController(text: '60');
  DateTime? _startTime;
  DateTime? _endTime;
  DateTime _selectedDate = DateTime.now();

  int _participationPeople = 1;
  int _participationTime = 60;
  PaymentMethod _paymentMethod = PaymentMethod.byVNPay;

  int _buildTotalMinutes() {
    if (widget.skillModel != null) {
      if (widget.contactForm == CONTACT_CALL) {
        return widget.skillModel!.callPrice!.totalMinutes;
      } else {
        return widget.skillModel!.meetPrice!.totalMinutes;
      }
    } else if (widget.expertModel != null) {
      if (widget.contactForm == CONTACT_CALL) {
        return widget.expertModel!.callPrice!.totalMinutes;
      } else {
        return widget.expertModel!.meetPrice!.totalMinutes;
      }
    }
    return 1;
  }

  String _buildCurrencyString() {
    if (widget.skillModel != null) {
      if (widget.contactForm == CONTACT_CALL) {
        return widget.skillModel!.callPrice!.currency;
      } else {
        return widget.skillModel!.meetPrice!.currency;
      }
    } else if (widget.expertModel != null) {
      if (widget.contactForm == CONTACT_CALL) {
        return widget.expertModel!.callPrice!.currency;
      } else {
        return widget.expertModel!.meetPrice!.currency;
      }
    }
    return '';
  }

  String descriptionDialog() {
    if (widget.skillModel != null) {
      return 'Bạn có chắc chắn muốn dừng đặt lịch kĩ năng này?';
    } else if (widget.expertModel != null) {
      return Strings.descriptionCancelBookingExpert.i18n;
    }
    return '';
  }

  String _buildMinutesString() {
    if (widget.skillModel != null) {
      if (widget.contactForm == CONTACT_CALL) {
        return widget.skillModel!.callPrice!.totalMinutes.toString();
      } else {
        return widget.skillModel!.meetPrice!.totalMinutes.toString();
      }
    } else if (widget.expertModel != null) {
      if (widget.contactForm == CONTACT_CALL) {
        return widget.expertModel!.callPrice!.totalMinutes.toString();
      } else {
        return widget.expertModel!.callPrice!.totalMinutes.toString();
      }
    }
    return '';
  }

  double _buildCost() {
    if (widget.skillModel != null) {
      if (widget.contactForm == CONTACT_CALL) {
        return widget.skillModel!.callPrice!.cost;
      } else {
        return widget.skillModel!.meetPrice!.cost;
      }
    } else if (widget.expertModel != null) {
      if (widget.contactForm == CONTACT_CALL) {
        return widget.expertModel!.callPrice!.cost;
      } else {
        return widget.expertModel!.meetPrice!.cost;
      }
    }
    return 0.0;
  }

  @override
  void initState() {
    super.initState();

    _timelineBloc = TimelineBloc();

    _pageController = PageController(
      initialPage: 0,
    );
    _steps = [
      OrderExpertStepOne(
        formKey: _formKeys[0],
        authorNameController: _authorNameController,
        authorPhoneController: _authorPhoneController,
        noteController: _noteController,
        contactForm: widget.contactForm,
        skillModel: widget.skillModel,
        expertModel: widget.expertModel,
        numberOfPeopleController: _participantCountController,
        addressController: _locationAddressController,
        placeController: _locationNameController,
        changeNumberPeople: (val) {
          _changeNumberPeople(val);
        },
      ),
      OrderExpertStepTwo(
        adviseDurationController: _adviseDurationController,
        selectedDate: _selectedDate,
        formKey: _formKeys[1],
        totalMinus: _buildTotalMinutes(),
        initStartTime: _startTime,
        initEndTime: _endTime,
        onDateSelected: (val) {
          setState(() {
            _startTime = DateTime.now();
            _endTime = null;
            _selectedDate = val;
            _update(val);
          });
        },
        onTimeSelected: (startTime, endTime) {
          setState(() {
            _startTime = DateTime(
              _selectedDate.year,
              _selectedDate.month,
              _selectedDate.day,
              startTime.hour,
              startTime.minute,
            );
            _endTime = DateTime(
              _selectedDate.year,
              _selectedDate.month,
              _selectedDate.day,
              endTime.hour,
              endTime.minute,
            );
          });
        },
        contactForm: widget.contactForm,
        changeTime: (val) => _changeTime(val),
      ),
      OrderExperStepThree(
        onSelectedValueChanged: (val) {
          _paymentMethod = val;
        },
        contactForm: widget.contactForm,
        skillModel: widget.skillModel,
        expertModel: widget.expertModel,
        numberPeople: _participationPeople,
        timeParticipation: _participationTime,
      ),
    ];
  }

  _update(DateTime startTime) {
    _steps = [
      OrderExpertStepOne(
        formKey: _formKeys[0],
        authorNameController: _authorNameController,
        authorPhoneController: _authorPhoneController,
        noteController: _noteController,
        contactForm: widget.contactForm,
        skillModel: widget.skillModel,
        expertModel: widget.expertModel,
        numberOfPeopleController: _participantCountController,
        addressController: _locationAddressController,
        placeController: _locationNameController,
        changeNumberPeople: (val) {
          _changeNumberPeople(val);
        },
      ),
      OrderExpertStepTwo(
        selectedDate: _selectedDate,
        formKey: _formKeys[1],
        totalMinus: _buildTotalMinutes(),
        initStartTime: startTime,
        initEndTime: _endTime,
        onDateSelected: (val) {
          setState(() {
            _selectedDate = val;
            _startTime = DateTime.now();
            _endTime = null;
          });
        },
        onTimeSelected: (startTime, endTime) {
          setState(() {
            _startTime = DateTime(
              _selectedDate.year,
              _selectedDate.month,
              _selectedDate.day,
              startTime.hour,
              startTime.minute,
            );
            _endTime = DateTime(
              _selectedDate.year,
              _selectedDate.month,
              _selectedDate.day,
              endTime.hour,
              endTime.minute,
            );
          });
        },
        contactForm: widget.contactForm,
        adviseDurationController: _adviseDurationController,
        changeTime: (val) => _changeTime(val),
      ),
      OrderExperStepThree(
        onSelectedValueChanged: (val) {
          _paymentMethod = val;
        },
        contactForm: widget.contactForm,
        skillModel: widget.skillModel,
        expertModel: widget.expertModel,
        numberPeople: _participationPeople,
        timeParticipation: _participationTime,
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      lazy: false,
      create: (context) => _timelineBloc
        ..add(
          GetDateAvailable(
            selectedDate: DateTime.now(),
            contactType: widget.contactForm == CONTACT_MEET ? 0 : 1,
            expertId: widget.skillModel?.author?.id ??
                widget.skillModel?.expertId ??
                widget.expertModel?.id ??
                '',
          ),
        ),
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        appBar: appBarTitleBack(
          context,
          'Đặt gói dịch vụ',
          onBackPressed: () {
            if (_currentIndex != 0) {
              if (_currentIndex == 2) {
                _update(_startTime ?? DateTime.now());
              }
              _pageController.previousPage(
                duration: const Duration(milliseconds: LIMIT_RESPONSE_TIME),
                curve: Curves.easeInOut,
              );
            } else {
              dialogAnimationWrapper(
                borderRadius: 10.sp,
                slideFrom: SlideMode.bot,
                child: DialogConfirmCancel(
                  bodyBefore: descriptionDialog(),
                  confirmText: 'Xác nhận',
                  cancelText: 'Hủy',
                  onConfirmed: () async {
                    AppNavigator.pop();
                    AppNavigator.pop();
                  },
                ),
              );
            }
          },
        ),
        body: Container(
          child: Column(
            children: [
              Container(
                width: 100.w,
                height: 48.sp,
                margin: EdgeInsets.only(top: 18.sp, bottom: 14.sp),
                padding: EdgeInsets.symmetric(horizontal: 4.sp),
                child: StepOrderService(
                  steps: [
                    'Nhập thông tin',
                    'Chọn thời gian',
                    'Thanh toán',
                  ],
                  currentStep: _currentIndex,
                  onStepChanged: (index) => _pageController.jumpToPage(index),
                ),
              ),
              dividerThinkness6NotMargin,
              Expanded(
                child: PageView(
                  physics: NeverScrollableScrollPhysics(),
                  controller: _pageController,
                  children: _steps,
                  onPageChanged: (index) {
                    setState(() {
                      _currentIndex = index;
                    });
                  },
                ),
              ),
              dividerChat,
              Visibility(
                visible: MediaQuery.of(context).viewInsets.bottom == 0,
                child: Container(
                  width: 100.w,
                  padding: EdgeInsets.only(
                    left: 16.sp,
                    right: 16.sp,
                    bottom: 16.sp,
                    top: _currentIndex == 2 ? 8.sp : 16.sp,
                  ),
                  child: Column(
                    children: [
                      _currentIndex == 2
                          ? Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text(
                                  'Tổng cộng',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 13.sp,
                                    color: colorBlack2,
                                    height: 1.625,
                                  ),
                                ),
                                Text(
                                  convertMoneyToString(
                                    calculateTotalExpert(
                                      widget.skillModel,
                                      widget.expertModel,
                                      widget.contactForm,
                                      _participationTime,
                                      _participationPeople,
                                    ),
                                    _buildCurrencyString(),
                                  ),
                                  style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 20.sp,
                                    color: colorFontGreen,
                                  ),
                                ),
                              ],
                            )
                          : SizedBox(),
                      _currentIndex == 2 ? SizedBox(height: 12.sp) : SizedBox(),
                      ButtonPrimary(
                        onPressed: _buttonOnPressedHandle,
                        fontWeight: FontWeight.w700,
                        text: _currentIndex == _steps.length - 1
                            ? 'Thanh toán'
                            : 'Tiếp tục',
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  _buttonOnPressedHandle() async {
    switch (_currentIndex) {
      case 0:
        final _isValid = _formKeys[0].currentState!.validate();
        if (_isValid) {
          _pageController.nextPage(
            duration: Duration(milliseconds: LIMIT_RESPONSE_TIME),
            curve: Curves.easeIn,
          );
        }
        break;
      case 1:
        final _isValid = _formKeys[1].currentState!.validate();
        if (_isValid) {
          if (_startTime == null || _endTime == null) {
            dialogAnimationWrapper(
              slideFrom: SlideMode.fade,
              child: DialogWithTextAndPopButton(
                bodyBefore:
                    'Bạn chưa chọn thời gian hẹn, vui lòng chọn thời gian hẹn để đến bước tiếp theo',
                bodyAlign: TextAlign.center,
                bodyFontSize: 13.sp,
                bodyColor: colorBlack1,
              ),
            );
            return;
          }
          _update(_startTime ?? DateTime.now());
          _pageController.nextPage(
            duration: Duration(milliseconds: LIMIT_RESPONSE_TIME),
            curve: Curves.easeIn,
          );
        }

        break;
      case 2:
        final _amount = calculateTotalExpert(
          widget.skillModel,
          widget.expertModel,
          widget.contactForm,
          _participationTime,
          _participationPeople,
        );

        showDialogLoading();
        if (widget.skillModel != null) {
          final _price = BudgetModel(
              currency: _buildCurrencyString(),
              cost: _buildCost(),
              totalMinutes: _buildTotalMinutes());

          AppBloc.requestBloc.add(
            UserPayOfferSkillEvent(
              expert: widget.skillModel!,
              price: _price,
              paymentMethod: _paymentMethod,
              amount: _amount,
              updatedWallet: AppBloc.userBloc.getAccount.wallet! - _amount,
              skillModel: widget.skillModel!,
              contactForm: widget.contactForm,
              authorName: _authorNameController.text,
              authorPhone: _authorPhoneController.text,
              endTime: _endTime!,
              localAddress: _locationAddressController.text,
              localName: _locationNameController.text,
              note: _noteController.text,
              participantsCount: _participationPeople,
              startTime: _startTime!,
            ),
          );
        } else if (widget.expertModel != null) {
          final _price = BudgetModel(
              currency: _buildCurrencyString(),
              cost: _buildCost(),
              totalMinutes: _buildTotalMinutes());

          AppBloc.requestBloc.add(
            BookingByTargetExpertEvent(
              price: _price,
              expert: widget.expertModel!,
              paymentMethod: _paymentMethod,
              contactForm: widget.contactForm,
              updatedWallet: AppBloc.userBloc.getAccount.wallet! - _amount,
              startTime: _startTime!,
              endTime: _endTime!,
              authorName: _authorNameController.text,
              authorPhone: _authorPhoneController.text,
              locationName: _locationNameController.text,
              locationAddress: _locationAddressController.text,
              participantNumber: _participationPeople,
              amount: _amount,
              totalMinutes: _endTime!.difference(_startTime!).inMinutes,
              cost: _amount,
              isCheckChatScreen: widget.isCheckChatScreen,
            ),
          );
        }

        break;
      default:
        break;
    }
  }

  String convertMoneyToString(double money, String currency) {
    switch (currency) {
      case 'vnd':
        return money.toStringAsFixed(0).formatMoney() + 'đ';
      case 'usd':
        NumberFormat format = NumberFormat('#,###.##');
        return format.format(money) + '\$';
      default:
        return '';
    }
  }

  void _changeNumberPeople(String val) {
    int? number = int.tryParse(val.trim());
    if (number != null) {
      setState(() {
        _participationPeople = number;
        _steps[2] = OrderExperStepThree(
          skillModel: widget.skillModel,
          expertModel: widget.expertModel,
          contactForm: widget.contactForm,
          onSelectedValueChanged: (val) {
            _paymentMethod = val;
          },
          numberPeople: _participationPeople,
          timeParticipation: _participationTime,
        );
      });
    }
  }

  void _changeTime(String val) {
    int? time = int.tryParse(val.trim());
    if (time != null) {
      setState(() {
        _startTime = DateTime.now();
        _endTime = null;
        _participationTime = time;
        _steps[2] = OrderExperStepThree(
          skillModel: widget.skillModel,
          expertModel: widget.expertModel,
          contactForm: widget.contactForm,
          onSelectedValueChanged: (val) {
            _paymentMethod = val;
          },
          numberPeople: _participationPeople,
          timeParticipation: _participationTime,
        );
      });
    }
  }

  double calculateTotalExpert(SkillModel? skillModel, ExpertModel? expertModel,
      String contactForm, int time, int people) {
    final _subTotal =
        calculateSubTotal(skillModel, expertModel, contactForm, time);
    final _extraFee = calculateExtraFee(_subTotal, people);
    return _subTotal + _extraFee;
  }

  double calculateSubTotal(SkillModel? skillModel, ExpertModel? expertModel,
      String contactForm, int time) {
    double _subTotal = 0;
    if (skillModel != null) {
      if (contactForm == CONTACT_CALL) {
        _subTotal = skillModel.callPrice!.cost *
            time /
            int.parse(_buildMinutesString());
      } else if (contactForm == CONTACT_MEET) {
        _subTotal = skillModel.meetPrice!.cost *
            time /
            int.parse(_buildMinutesString());
      }
    } else if (expertModel != null) {
      if (contactForm == CONTACT_CALL) {
        _subTotal = expertModel.callPrice!.cost *
            time /
            int.parse(_buildMinutesString());
      } else if (contactForm == CONTACT_MEET) {
        _subTotal = expertModel.meetPrice!.cost *
            time /
            int.parse(_buildMinutesString());
      }
    }

    return _subTotal;
  }

  double calculateExtraFee(double subTotal, int numParticipants) {
    if (numParticipants >= 2) {
      return .5 * subTotal;
    }
    return 0;
  }
}
