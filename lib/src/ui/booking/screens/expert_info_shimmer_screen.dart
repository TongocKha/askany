import 'package:askany/src/ui/common/widgets/custom_line/dash_line.dart';
import 'package:askany/src/ui/common/widgets/shimmers/fade_simmer.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ExpertInfoShimmerScreen extends StatelessWidget {
  const ExpertInfoShimmerScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Expanded(
          child: Container(
            width: 100.w,
            padding: EdgeInsets.symmetric(vertical: 22.sp, horizontal: 16.sp),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                FadeShimmer.round(
                  size: 86.sp,
                  fadeTheme: FadeTheme.lightReverse,
                ),
                SizedBox(height: 16.sp),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    FadeShimmer(
                      width: 105.sp,
                      height: 18.sp,
                      fadeTheme: FadeTheme.lightReverse,
                    ),
                    SizedBox(width: 6.sp),
                    FadeShimmer.round(
                      size: 12.sp,
                      fadeTheme: FadeTheme.lightReverse,
                    ),
                  ],
                ),
                SizedBox(height: 4.sp),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    FadeShimmer.round(
                      size: 13.sp,
                      fadeTheme: FadeTheme.lightReverse,
                    ),
                    SizedBox(width: 6.sp),
                    FadeShimmer(
                      width: 75.sp,
                      height: 11.sp,
                      fadeTheme: FadeTheme.lightReverse,
                    ),
                  ],
                ),
                SizedBox(height: 28.sp),
                DashLine(
                  direction: Axis.horizontal,
                  lineThickness: .5.sp,
                  dashColor: colorDividerTimeline,
                  dashLength: 4.sp,
                  dashGapLength: 5.sp,
                ),
                SizedBox(height: 30.sp),
                _SingleRowOfText(
                  width: 65.sp,
                  value: Row(
                    children: [
                      FadeShimmer(
                        width: 35.sp,
                        height: 14.sp,
                        fadeTheme: FadeTheme.lightReverse,
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 18.sp),
                _SingleRowOfText(
                  width: 68.sp,
                  value: FadeShimmer(
                    width: 85.sp,
                    height: 14.sp,
                    fadeTheme: FadeTheme.lightReverse,
                  ),
                ),
                SizedBox(height: 18.sp),
                _SingleRowOfText(
                  width: 105.sp,
                  value: FadeShimmer(
                    width: 35.sp,
                    height: 14.sp,
                    fadeTheme: FadeTheme.lightReverse,
                  ),
                ),
                SizedBox(height: 18.sp),
                _SingleRowOfText(
                  width: 45.sp,
                  value: FadeShimmer(
                    width: 105.sp,
                    height: 14.sp,
                    fadeTheme: FadeTheme.lightReverse,
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class _SingleRowOfText extends StatelessWidget {
  const _SingleRowOfText({
    Key? key,
    required this.value,
    required this.width,
  }) : super(key: key);

  final Widget value;
  final double width;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        FadeShimmer(
          width: width,
          height: 14.sp,
          fadeTheme: FadeTheme.lightReverse,
        ),
        value,
      ],
    );
  }
}
