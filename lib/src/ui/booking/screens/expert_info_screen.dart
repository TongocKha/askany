import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/expert_info/expert_info_bloc.dart';
import 'package:askany/src/helpers/province_helper.dart';
import 'package:askany/src/models/expert_model.dart';

import 'package:askany/src/ui/booking/screens/expert_info_shimmer_screen.dart';

import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/common/widgets/custom_image/network_image/cached_image.dart';
import 'package:askany/src/ui/common/widgets/custom_line/dash_line.dart';
import 'package:askany/src/ui/review_service/widgets/bottom_sheet_contact_expert.dart';

import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ExpertInfoScreen extends StatefulWidget {
  final String expertId;
  ExpertInfoScreen({required this.expertId});

  @override
  State<ExpertInfoScreen> createState() => _ExpertInfoScreenState();
}

class _ExpertInfoScreenState extends State<ExpertInfoScreen> {
  @override
  void initState() {
    AppBloc.expertInfoBloc.add(GetInfoExpertEvent(expertId: widget.expertId));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarTitleBack(context, ''),
      resizeToAvoidBottomInset: false,
      body: BlocBuilder<ExpertInfoBloc, ExpertInfoState>(
        buildWhen: (previous, current) => previous != current,
        builder: (context, state) {
          if (state is GetInfoExpert && state.experts[widget.expertId] != null) {
            ExpertModel expertModel = state.experts[widget.expertId] as ExpertModel;
            return Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Expanded(
                  child: Container(
                    width: 100.w,
                    padding: EdgeInsets.symmetric(vertical: 22.sp, horizontal: 16.sp),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        CustomNetworkImage(
                          height: 86.sp,
                          width: 86.sp,
                          urlToImage: expertModel.avatar?.urlToImage ?? '',
                        ),
                        SizedBox(height: 16.sp),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              expertModel.fullname,
                              style: TextStyle(
                                fontWeight: FontWeight.w700,
                                fontSize: 17.sp,
                                height: 1.35,
                                color: colorBlack1,
                              ),
                            ),
                            SizedBox(width: 6.sp),
                            Image.asset(
                              iconVerified,
                              width: 12.sp,
                              height: 12.sp,
                              color: colorGreen2,
                            ),
                          ],
                        ),
                        SizedBox(height: 4.sp),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset(
                              iconLocation,
                              color: colorGray1,
                              width: 9.sp,
                              height: 13.sp,
                            ),
                            SizedBox(width: 6.sp),
                            Text(
                              '${ProvinceHelper().getProvinceByCode(expertModel.province)}',
                              style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 11.sp,
                                color: colorGray1,
                                height: 1.5,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 28.sp),
                        DashLine(
                          direction: Axis.horizontal,
                          lineThickness: .5.sp,
                          dashColor: colorDividerTimeline,
                          dashLength: 4.sp,
                          dashGapLength: 5.sp,
                        ),
                        SizedBox(height: 30.sp),
                        _SingleRowOfText(
                          title: 'Reviews:',
                          value: Row(
                            children: [
                              Image.asset(iconStar, width: 18.sp, height: 18.sp),
                              SizedBox(width: 6.5.sp),
                              Text(
                                '${expertModel.stars}',
                                style: TextStyle(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 13.sp,
                                  color: colorBlack1,
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 18.sp),
                        _SingleRowOfText(
                          title: 'Giá tư vấn:',
                          value: RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                  text: expertModel.callPrice?.costString15Minutes ?? '0đ',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 14.sp,
                                    color: colorBlack1,
                                  ),
                                ),
                                TextSpan(
                                  text: ' /${expertModel.callPrice?.totalMinutes ?? 15} phút',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 11.sp,
                                    color: colorBlack2,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(height: 18.sp),
                        _SingleRowOfText(
                          title: 'Số năm kinh nghiệm:',
                          value: Text(
                            '${expertModel.experienceYears} năm',
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 13.sp,
                              color: colorBlack2,
                            ),
                          ),
                        ),
                        SizedBox(height: 18.sp),
                        _SingleRowOfText(
                          title: 'Công ty:',
                          value: Text(
                            '${expertModel.companyName}',
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 13.sp,
                              color: colorBlack2,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                dividerChat,
                Container(
                  width: 100.w,
                  padding: EdgeInsets.all(16.sp),
                  child: ButtonPrimary(
                    onPressed: () {
                      showModalBottomSheet(
                        context: context,
                        backgroundColor: Colors.transparent,
                        isScrollControlled: true,
                        builder: (context) {
                          return BottomSheetContactExpert(
                            expertModel: expertModel,
                            isCheckChatScreen: true,
                          );
                        },
                      );
                    },
                    text: 'Đặt lịch ngay',
                  ),
                ),
              ],
            );
          } else
            return ExpertInfoShimmerScreen();
        },
      ),
    );
  }
}

class _SingleRowOfText extends StatelessWidget {
  const _SingleRowOfText({
    Key? key,
    required this.title,
    required this.value,
  }) : super(key: key);

  final String title;
  final Widget value;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          title,
          style: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: 13.sp,
            color: colorBlack2,
          ),
        ),
        value,
      ],
    );
  }
}
