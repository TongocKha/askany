import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/common/widgets/custom_line/dash_line.dart';
import 'package:askany/src/ui/common/widgets/custom_line/semicircles_line.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ConSultServiceInfoScreen extends StatelessWidget {
  const ConSultServiceInfoScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarTitleBack(
        context,
        // Strings.overviewOfThisServiceAppbartext.i18n,

        'Tổng quan về gói tư vấn này',
      ),
      resizeToAvoidBottomInset: false,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                width: 100.w,
                margin: EdgeInsets.symmetric(
                  vertical: 26.sp,
                  horizontal: 16.sp,
                ),
                padding: EdgeInsets.only(
                  left: 10.sp,
                  top: 22.sp,
                  right: 10.sp,
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10.sp),
                    topRight: Radius.circular(10.sp),
                  ),
                  color: colorWhiteCard,
                  boxShadow: [
                    BoxShadow(
                      color: colorBlackShadow.withOpacity(.05),
                      offset: Offset(-2.sp, -4.sp),
                      blurRadius: 4.sp,
                    ),
                    BoxShadow(
                      color: colorBlackShadow.withOpacity(.05),
                      offset: Offset(2.sp, 4.sp),
                      blurRadius: 2.sp,
                    ),
                  ],
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      'Cách xin visa du học Mỹ bao đậu 100%',
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 15.sp,
                        height: 1.363,
                        color: colorBlack2,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 14.sp),
                      child: dividerColorGrey2,
                    ),
                    _SingleRowOfTextValue(
                      title: 'Số người tham gia:',
                      value: '4 người',
                    ),
                    SizedBox(height: 8.sp),
                    _SingleRowOfTextValue(
                      title: 'Giá tiền:',
                      value: '1.000.000đ',
                    ),
                    SizedBox(height: 8.sp),
                    _SingleRowOfTextValue(
                      title: 'Thời lượng tư vấn:',
                      value: '2 giờ',
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 16.sp),
                      child: DashLine(
                        direction: Axis.horizontal,
                        lineThickness: .5.sp,
                        dashColor: colorGray2,
                        dashLength: 4.sp,
                        dashGapLength: 5.sp,
                      ),
                    ),
                    _SingleRowOfText(
                      text: 'Loại gói: Gói gặp mặt trực tiếp',
                    ),
                    SizedBox(height: 8.sp),
                    _SingleRowOfText(
                      text:
                          'Địa điểm: Rose Cafe - 743 Trần Hưng Đạo, Phường 1, Quận 1, TP HCM',
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 16.sp),
                      child: DashLine(
                        direction: Axis.horizontal,
                        lineThickness: .5.sp,
                        dashColor: colorGray2,
                        dashLength: 4.sp,
                        dashGapLength: 5.sp,
                      ),
                    ),
                    _SingleRowOfText(text: 'Chuyên gia tư vấn'),
                    SizedBox(height: 15.sp),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        CircleAvatar(
                          radius: 22.sp,
                          foregroundImage: NetworkImage(
                            'https://avatars.githubusercontent.com/u/60530946?v=4',
                          ),
                        ),
                        SizedBox(width: 10.sp),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Hùng Nguyễn',
                              style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 13.sp,
                                color: colorBlack2,
                                height: 1.5,
                              ),
                            ),
                            SizedBox(height: 2.sp),
                            Text(
                              'Hồ Chí Minh',
                              style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 11.sp,
                                color: colorGray1,
                                height: 1.57,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(height: 34.sp),
                    SemicirclesLine(
                      direction: Axis.horizontal,
                      lineThickness: 4.sp,
                      semicircleColor: colorBlackShadow.withOpacity(.05),
                      gapLength: 6.sp,
                    )
                  ],
                ),
              ),
            ),
          ),
          dividerChat,
          Container(
            width: 100.w,
            padding: EdgeInsets.all(16.sp),
            child: ButtonPrimary(
              onPressed: () {
                // AppNavigator.push(
                //   Routes.ORDER_SERVICE,
                //   arguments: {
                //     'request': null,
                //     'offer': null,
                //   },
                // );
              },
              text: 'Đặt lịch ngay',
            ),
          ),
        ],
      ),
    );
  }
}

class _SingleRowOfText extends StatelessWidget {
  const _SingleRowOfText({
    Key? key,
    required this.text,
  }) : super(key: key);
  final String text;
  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        fontWeight: FontWeight.w400,
        fontSize: 13.sp,
        height: 1.625,
        color: colorBlack2,
      ),
    );
  }
}

class _SingleRowOfTextValue extends StatelessWidget {
  const _SingleRowOfTextValue({
    Key? key,
    required this.title,
    required this.value,
  }) : super(key: key);
  final String title;
  final String value;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          title,
          style: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: 13.sp,
            height: 1.625,
            color: colorBlack2,
          ),
        ),
        Text(
          value,
          style: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: 13.sp,
            height: 1.625,
            color: colorBlack2,
          ),
        ),
      ],
    );
  }
}
