import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/helpers/currency_helper.dart';
import 'package:askany/src/models/expert_model.dart';
import 'package:askany/src/models/skill_model.dart';
import 'package:askany/src/models/slide_mode.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_with_text_and_pop_button.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:askany/src/ui/request/widgets/radio_card.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:askany/src/helpers/extentions/string_extention.dart';

class OrderExperStepThree extends StatefulWidget {
  OrderExperStepThree({
    Key? key,
    this.expertModel,
    this.skillModel,
    required this.contactForm,
    required this.onSelectedValueChanged,
    required this.numberPeople,
    required this.timeParticipation,
  }) : super(key: key);
  final ExpertModel? expertModel;

  final SkillModel? skillModel;
  final String contactForm;
  final Function(PaymentMethod) onSelectedValueChanged;
  final int numberPeople;
  final int timeParticipation;

  @override
  _OrderExperStepThreeState createState() => _OrderExperStepThreeState();
}

class _OrderExperStepThreeState extends State<OrderExperStepThree> {
  PaymentMethod _paymentMethod = PaymentMethod.byVNPay;
  double _wallet = AppBloc.userBloc.getAccount.wallet ?? 0;
  double _subTotal = 0;
  double _extraFee = 0;
  double _total = 0;

  @override
  void initState() {
    super.initState();
    _subTotal = calculateSubTotal(widget.skillModel, widget.expertModel,
        widget.contactForm, widget.timeParticipation);
    _extraFee = calculateExtraFee(_subTotal, widget.numberPeople);
    _total = _subTotal + _extraFee;
  }

  String _buildCostString() {
    if (widget.skillModel != null) {
      if (widget.contactForm == CONTACT_CALL) {
        return widget.skillModel!.callPrice!.costString;
      } else {
        return widget.skillModel!.meetPrice!.costString;
      }
    } else if (widget.expertModel != null) {
      if (widget.contactForm == CONTACT_CALL) {
        return widget.expertModel!.callPrice!.costString;
      } else {
        return widget.expertModel!.callPrice!.costString;
      }
    }
    return '';
  }

  String _buildCurrencyString() {
    if (widget.skillModel != null) {
      if (widget.contactForm == CONTACT_CALL) {
        return widget.skillModel!.callPrice!.currency;
      } else {
        return widget.skillModel!.meetPrice!.currency;
      }
    } else if (widget.expertModel != null) {
      if (widget.contactForm == CONTACT_CALL) {
        return widget.expertModel!.callPrice!.currency;
      } else {
        return widget.expertModel!.callPrice!.currency;
      }
    }
    return '';
  }

  String _buildMinutesString() {
    if (widget.skillModel != null) {
      if (widget.contactForm == CONTACT_CALL) {
        return widget.skillModel!.callPrice!.totalMinutes.toString();
      } else {
        return widget.skillModel!.meetPrice!.totalMinutes.toString();
      }
    } else if (widget.expertModel != null) {
      if (widget.contactForm == CONTACT_CALL) {
        return widget.expertModel!.callPrice!.totalMinutes.toString();
      } else {
        return widget.expertModel!.callPrice!.totalMinutes.toString();
      }
    }
    return '';
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(
              left: 16.sp,
              top: 22.sp,
              right: 16.sp,
              bottom: 28.sp,
            ),
            child: _buildPaymentMethod(),
          ),
          dividerThinkness6NotMargin,
          Container(
            padding: EdgeInsets.only(
              left: 16.sp,
              top: 20.5.sp,
              right: 16.sp,
              bottom: 30.sp,
            ),
            child: _buildPaymentInfo(),
          )
        ],
      ),
    );
  }

  Widget _buildPaymentInfo() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        _SingleRowOfText(
          title: 'Loại gói',
          value: widget.contactForm == CONTACT_CALL
              ? 'Gói gọi điện'
              : 'Gói gặp mặt',
        ),
        SizedBox(height: 12.sp),
        _SingleRowOfText(
          title: 'Số người tham gia',
          value: '${widget.numberPeople} người',
        ),
        SizedBox(height: 12.sp),
        _SingleRowOfText(
          title: 'Giá tiền ' + _buildMinutesString() + ' phút',
          value: _buildCostString(),
        ),
        SizedBox(height: 20.sp),
        dividerColorGrey2,
        SizedBox(height: 20.sp),
        _SingleRowOfText(
          title: 'Thời lượng tư vấn',
          value: '${widget.timeParticipation} phút',
        ),
        SizedBox(height: 12.sp),
        _SingleRowOfText(
          title: 'Thành tiền',
          value: _convertMoneyToString(
            _subTotal,
            _buildCurrencyString(),
          ),
        ),
        SizedBox(height: 12.sp),
        _SingleRowOfText(
          title: 'Phí dịch vụ cộng thêm',
          value: _convertMoneyToString(_extraFee, _buildCurrencyString()),
        ),
      ],
    );
  }

  Widget _buildPaymentMethod() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Text(
          'Chọn phương thức thanh toán',
          style: TextStyle(
            fontSize: 14.sp,
            color: colorFontGreen,
            height: 1.53,
            fontWeight: FontWeight.w600,
          ),
        ),
        SizedBox(height: 15.sp),
        RadioCard<PaymentMethod>(
          title: 'Thanh toán bằng VNPay',
          paymentMethod: _paymentMethod,
          value: PaymentMethod.byVNPay,
          onChanged: (val) {
            setState(() {
              _paymentMethod = val;
            });

            widget.onSelectedValueChanged(_paymentMethod);
          },
        ),
        SizedBox(height: 18.sp),
        RadioCard<PaymentMethod>(
          title: 'Thanh Toán bằng ví Ask Any',
          subTitle: 'Số dư còn lại trong ví: ' +
              _convertMoneyToString(
                CurrencyHelper().convertMoney(
                  _wallet,
                  'vnd',
                  _buildCurrencyString(),
                ),
                _buildCurrencyString(),
              ),
          paymentMethod: _paymentMethod,
          value: PaymentMethod.byAppWallet,
          onChanged: (val) {
            setState(() {
              _paymentMethod = val;
            });

            widget.onSelectedValueChanged(val);
          },
          disable: _wallet < _total,
          onDisableTap: (val) {
            dialogAnimationWrapper(
              slideFrom: SlideMode.fade,
              child: DialogWithTextAndPopButton(
                bodyBefore:
                    'Tài khoản ví không đủ số dư. Vui lòng nạp thêm tiền để tiến hành thanh toán.',
                bodyColor: colorBlack1,
                bodyAlign: TextAlign.center,
                bodyFontSize: 13.sp,
              ),
            );
          },
        ),
      ],
    );
  }

  String _convertMoneyToString(double money, String currency) {
    switch (currency) {
      case 'vnd':
        return money.toStringAsFixed(0).formatMoney() + 'đ';
      case 'usd':
        NumberFormat format = NumberFormat('#,###.##');
        return format.format(money) + '\$';
      default:
        return '';
    }
  }

  double calculateTotalExpert(SkillModel? skillModel, ExpertModel? expertModel,
      String contactForm, int time, int people) {
    final _subTotal =
        calculateSubTotal(skillModel, expertModel, contactForm, time);
    final _extraFee = calculateExtraFee(_subTotal, people);
    return _subTotal + _extraFee;
  }

  double calculateSubTotal(SkillModel? skillModel, ExpertModel? expertModel,
      String contactForm, int time) {
    double _subTotal = 0;
    if (skillModel != null) {
      if (contactForm == CONTACT_CALL) {
        _subTotal = skillModel.callPrice!.cost *
            time /
            int.parse(_buildMinutesString());
      } else if (contactForm == CONTACT_MEET) {
        _subTotal = skillModel.meetPrice!.cost *
            time /
            int.parse(_buildMinutesString());
      }
    } else if (expertModel != null) {
      if (contactForm == CONTACT_CALL) {
        _subTotal = expertModel.callPrice!.cost *
            time /
            int.parse(_buildMinutesString());
      } else if (contactForm == CONTACT_MEET) {
        _subTotal = expertModel.meetPrice!.cost *
            time /
            int.parse(_buildMinutesString());
      }
    }

    return _subTotal;
  }

  double calculateExtraFee(double subTotal, int numParticipants) {
    if (numParticipants >= 2) {
      return .5 * subTotal;
    }
    return 0;
  }
}

class _SingleRowOfText extends StatelessWidget {
  const _SingleRowOfText({
    Key? key,
    required String title,
    required String value,
  })  : _title = title,
        _value = value,
        super(key: key);

  final String _title;
  final String _value;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          _title,
          style: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: 13.sp,
            color: colorBlack2,
            height: 1.625,
          ),
        ),
        Text(
          _value,
          style: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: 13.sp,
            color: colorBlack2,
            height: 1.625,
          ),
        ),
      ],
    );
  }
}
