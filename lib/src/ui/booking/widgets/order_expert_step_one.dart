import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/models/expert_model.dart';
import 'package:askany/src/models/skill_model.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_form.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_form_request.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class OrderExpertStepOne extends StatelessWidget {
  OrderExpertStepOne({
    Key? key,
    required this.formKey,
    required this.authorNameController,
    required this.authorPhoneController,
    required this.noteController,
    required this.contactForm,
    this.skillModel,
    this.expertModel,
    required this.numberOfPeopleController,
    required this.placeController,
    required this.addressController,
    required this.changeNumberPeople,
  }) : super(key: key);

  final Key formKey;
  final TextEditingController authorNameController;
  final TextEditingController authorPhoneController;
  final TextEditingController noteController;
  final TextEditingController numberOfPeopleController;
  final TextEditingController placeController;
  final TextEditingController addressController;
  final String contactForm;
  final SkillModel? skillModel;
  final ExpertModel? expertModel;
  final void Function(String) changeNumberPeople;

  String _buildText(
      final SkillModel? skillModel, final ExpertModel? expertModel) {
    if (skillModel != null) {
      if (contactForm == CONTACT_CALL) {
        return '${skillModel.callPrice!.costString}/ ${skillModel.callPrice!.totalMinutes} phút';
      } else {
        return '${skillModel.meetPrice!.costString}/ ${skillModel.meetPrice!.totalMinutes} phút';
      }
    } else if (expertModel != null) {
      if (contactForm == CONTACT_CALL) {
        return '${expertModel.callPrice!.costString}/ ${expertModel.callPrice!.totalMinutes} phút';
      } else {
        return '${expertModel.meetPrice!.costString}/ ${expertModel.meetPrice!.totalMinutes} phút';
      }
    }
    return '';
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Container(
        padding: EdgeInsets.only(
          left: 16.sp,
          top: 22.sp,
          right: 16.sp,
          bottom: 30.sp,
        ),
        child: Form(
          key: formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                'Thông tin liên hệ của bạn (*)',
                style: TextStyle(
                  fontSize: 14.sp,
                  color: colorFontGreen,
                  height: 1.53,
                  fontWeight: FontWeight.w600,
                ),
              ),
              SizedBox(height: 15.sp),
              Text(
                'Họ tên',
                style: TextStyle(
                  fontSize: 13.sp,
                  color: colorBlack2,
                  height: 1.5,
                  fontWeight: FontWeight.w600,
                ),
              ),
              TextFieldForm(
                submitForm: (val) {},
                controller: authorNameController,
                validatorForm: (val) =>
                    val!.length == 0 ? 'Tên không hợp lệ' : null,
              ),
              SizedBox(height: 18.sp),
              Text(
                'Số điện thoại',
                style: TextStyle(
                  fontSize: 13.sp,
                  color: colorBlack2,
                  height: 1.5,
                  fontWeight: FontWeight.w600,
                ),
              ),
              TextFieldForm(
                textInputType: TextInputType.number,
                controller: authorPhoneController,
                validatorForm: (value) =>
                    value!.trim().length > 11 || value.trim().length < 9
                        ? 'Số điện thoại không hợp lệ'
                        : null,
              ),
              SizedBox(height: 18.sp),
              Text(
                'Lời nhắn',
                style: TextStyle(
                  fontSize: 13.sp,
                  color: colorBlack2,
                  height: 1.5,
                  fontWeight: FontWeight.w600,
                ),
              ),
              TextFieldFormRequest(
                controller: noteController,
                hintText: 'Nhập lời nhắn',
                maxLines: 6,
                validatorForm: (val) {},
              ),
              SizedBox(height: 18.sp),
              dividerChat,
              SizedBox(height: 18.sp),
              Text(
                'Số lượng người tham gia (*)',
                style: TextStyle(
                  color: colorFontGreen,
                  fontSize: 14.sp,
                  fontWeight: FontWeight.w600,
                ),
              ),
              TextFieldForm(
                controller: numberOfPeopleController,
                textInputType: TextInputType.number,
                onChanged: changeNumberPeople,
                validatorForm: (val) => val!.trim().length == 0
                    ? 'Nhập số lượng người tham gia'
                    : (int.tryParse(val.trim().toString())! > 0 &&
                            int.tryParse(val.trim().toString())! <= 1000)
                        ? null
                        : "Số lượng người phải <=1000",
              ),
              SizedBox(height: 16.sp),
              Container(
                width: 100.w,
                padding: EdgeInsets.symmetric(vertical: 10.sp),
                decoration: BoxDecoration(
                  color: colorGreen3,
                  borderRadius: BorderRadius.circular(8.sp),
                ),
                alignment: Alignment.center,
                child: Text(
                  _buildText(skillModel, expertModel),
                  style: TextStyle(
                    color: colorWhiteCard,
                    fontSize: 13.sp,
                  ),
                ),
              ),
              SizedBox(height: 16.sp),
              Text(
                'Lưu ý: Nếu có 2 người tham gia thì mức giá không đổi, nếu có nhiều hơn 2 người tham gia thì phí dịch vụ sẽ cộng thêm 50%.',
                textAlign: TextAlign.justify,
                style: TextStyle(
                  color: colorFontGreen,
                  fontSize: 12.sp,
                ),
              ),
              Visibility(
                visible: contactForm == CONTACT_MEET,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 18.sp),
                    dividerChat,
                    SizedBox(height: 18.sp),
                    Text(
                      'Địa điểm tư vấn (*)',
                      style: TextStyle(
                        color: colorFontGreen,
                        fontSize: 14.sp,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    SizedBox(height: 14.sp),
                    Text(
                      'Lưu ý: Chi phí hẹn gặp mặt sẽ do người hẹn thanh toán.',
                      textAlign: TextAlign.justify,
                      style: TextStyle(
                        color: colorFontGreen,
                        fontSize: 12.sp,
                      ),
                    ),
                    SizedBox(height: 14.sp),
                    Text(
                      'Địa điểm',
                      style: TextStyle(
                        color: colorBlack2,
                        fontSize: 13.sp,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    TextFieldForm(
                      controller: placeController,
                      hintText: 'Tên địa điểm',
                      validatorForm: (val) => val!.trim().length == 0
                          ? 'Nhập địa điểm gặp mặt!'
                          : null,
                    ),
                    SizedBox(height: 17.sp),
                    Text(
                      'Địa chỉ',
                      style: TextStyle(
                        color: colorBlack2,
                        fontSize: 13.sp,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    TextFieldForm(
                      controller: addressController,
                      hintText: 'Tên địa chỉ',
                      maxLine: 2,
                      validatorForm: (val) => val!.trim().length == 0
                          ? 'Nhập địa chỉ gặp mặt!'
                          : null,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
