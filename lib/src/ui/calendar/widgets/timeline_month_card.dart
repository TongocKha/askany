import 'package:askany/src/helpers/date_time_helper.dart';
import 'package:askany/src/models/request_model.dart';
import 'package:askany/src/ui/common/widgets/custom_image/network_image/cached_image.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class TimelineMonthCard extends StatelessWidget {
  final RequestModel request;
  final DateTime? previousTask;
  TimelineMonthCard({required this.request, this.previousTask});

  _isShowTimeline() {
    return previousTask == null || !isEqualTwoDate(request.startTime!, previousTask!);
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          width: 8.w,
          margin: EdgeInsets.only(left: 10.sp),
          alignment: Alignment.center,
          child: Text(
            _isShowTimeline() ? getDayName(request.startTime!) : '',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 10.5.sp,
              color: colorGray1,
            ),
          ),
        ),
        Expanded(
          child: Column(
            children: [
              _isShowTimeline()
                  ? Padding(
                      padding: EdgeInsets.symmetric(vertical: 6.sp),
                      child: dividerCalendar,
                    )
                  : SizedBox(),
              Container(
                width: 100.w,
                margin: EdgeInsets.fromLTRB(12.sp, _isShowTimeline() ? 0.sp : 6.sp, 14.sp, 0.sp),
                padding: EdgeInsets.symmetric(vertical: 16.sp, horizontal: 12.sp),
                decoration: BoxDecoration(
                  color: isEqualTwoDate(DateTime.now(), request.startTime!)
                      ? colorGreen5
                      : backgroundCalendarCardYellow,
                  borderRadius: BorderRadius.circular(8.sp),
                ),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    CustomNetworkImage(
                      width: 35.sp,
                      height: 35.sp,
                      urlToImage: request.authorExpert?.avatar?.urlToImage,
                    ),
                    SizedBox(width: 10.sp),
                    Expanded(
                      child: Text(
                        request.title,
                        style: TextStyle(
                          fontSize: 12.sp,
                          color: colorBlack2,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
