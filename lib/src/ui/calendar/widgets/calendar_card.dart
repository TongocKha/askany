import 'package:askany/src/bloc/calendar/calendar_bloc.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/helpers/date_time_helper.dart';
import 'package:askany/src/ui/calendar/widgets/calendar_row_line.dart';
import 'package:askany/src/ui/calendar/widgets/calendar_title.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class CalendarCard extends StatefulWidget {
  final bool isWeekMode;
  final DateTime selectedDate;
  final GlobalKey? calendarKey;
  CalendarCard({
    required this.isWeekMode,
    required this.selectedDate,
    this.calendarKey,
  });
  @override
  State<StatefulWidget> createState() => _CalendarCardState();
}

class _CalendarCardState extends State<CalendarCard> {
  int _currentIndex = UNLIMITED_QUANTITY;
  final controller = new PageController(initialPage: UNLIMITED_QUANTITY);

  @override
  Widget build(BuildContext context) {
    return Container(
      key: widget.calendarKey,
      height: widget.isWeekMode
          ? 92.sp
          : (getCalendarThisMonth(dateTime: widget.selectedDate).length + 1) *
                  (isLargeDevice ? 36.sp : 26.sp) +
              18.sp,
      child: PageView.builder(
        pageSnapping: true,
        controller: controller,
        onPageChanged: (index) {
          if (index < _currentIndex) {
            BlocProvider.of<CalendarBloc>(context).add(PreviousMonthEvent());
          } else {
            BlocProvider.of<CalendarBloc>(context).add(NextMonthEvent());
          }
          _currentIndex = index;
        },
        itemBuilder: (context, index) => Opacity(
          opacity: _currentIndex != index ? 0.6 : 1,
          child: Container(
            decoration: BoxDecoration(
              color: backgroundCalendarCardGray,
              borderRadius: BorderRadius.vertical(
                bottom: Radius.circular(18.sp),
              ),
            ),
            padding: isLargeDevice
                ? EdgeInsets.only(top: 16.sp, bottom: 12.sp)
                : EdgeInsets.only(top: 8.sp, bottom: 4.sp),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                CalendarTitle(
                  textStyle: TextStyle(
                    fontSize: 11.sp,
                    color: colorGray2,
                  ),
                ),
                SizedBox(height: isLargeDevice ? 8.sp : 4.sp),
                ...(widget.isWeekMode
                        ? [getCalendarThisWeek(dateTime: widget.selectedDate)]
                        : getCalendarThisMonth(dateTime: widget.selectedDate))
                    .map(
                  (weeks) => CalendarRowLine(
                    weekDays: weeks,
                    selectedDate: widget.selectedDate,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
