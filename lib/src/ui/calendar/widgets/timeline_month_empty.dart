import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class TimelineMonthEmpty extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(height: 30.sp),
          Text(
            Strings.dontHaveAnyConsultationBooked.i18n,
            style: TextStyle(
              fontSize: 12.5.sp,
              color: colorBlack2,
            ),
          ),
          SizedBox(height: 24.sp),
          Container(
            width: 100.w,
            margin: EdgeInsets.symmetric(horizontal: 16.sp),
            padding: EdgeInsets.symmetric(horizontal: 12.sp, vertical: 16.sp),
            decoration: BoxDecoration(
              color: colorGreen5,
              borderRadius: BorderRadius.circular(8.sp),
            ),
            child: Row(
              children: [
                Image.asset(
                  imageTimelineMonthEmpty,
                  width: 120.sp,
                ),
                SizedBox(width: 10.sp),
                Expanded(
                  child: Text(
                    Strings.findExpertSuggestion.i18n,
                    style: TextStyle(
                      fontSize: 13.5.sp,
                      color: colorGreen2,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
