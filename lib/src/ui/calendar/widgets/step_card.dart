import 'package:askany/src/models/step_model.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:timeline_tile/timeline_tile.dart';

class StepCard extends StatefulWidget {
  final List<StepModel> steps;
  StepCard({
    required this.steps,
  });
  @override
  State<StatefulWidget> createState() => _StepCardState();
}

class _StepCardState extends State<StepCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.fromLTRB(16.sp, 14.sp, 16.sp, 0.sp),
      child: ListView.builder(
        padding: EdgeInsets.all(0.sp),
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: widget.steps.length,
        itemBuilder: (context, index) {
          bool isLast = index == widget.steps.length - 1;

          return _buildContent(
            context: context,
            index: index + 1,
            isFirst: index == 0,
            isLast: isLast,
            step: widget.steps[index],
          );
        },
      ),
    );
  }

  Widget _buildContent({
    required BuildContext context,
    required int index,
    required bool isFirst,
    required bool isLast,
    required StepModel step,
  }) {
    return TimelineTile(
      alignment: TimelineAlign.manual,
      lineXY: 0,
      beforeLineStyle: LineStyle(
        color: colorGreenTimeline,
        thickness: 1.2.sp,
      ),
      indicatorStyle: IndicatorStyle(
        indicatorXY: 0,
        drawGap: true,
        width: 9.sp,
        height: 9.sp,
        color: colorGreenTimeline,
      ),
      // isFirst: isFirst,
      // isLast: isLast,
      endChild: Container(
        margin: EdgeInsets.only(
          bottom: isLast ? 2.sp : 12.sp,
        ),
        color: Colors.transparent,
        padding: EdgeInsets.only(
          left: 12.sp,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              step.title,
              style: TextStyle(
                fontSize: 13.sp,
                fontWeight: FontWeight.w600,
                color: step.isDanger ? colorFinished : colorBlack1,
              ),
            ),
            SizedBox(height: 4.sp),
            Text(
              DateFormat('dd/MM/yyyy').format(step.createdAt),
              style: TextStyle(
                fontSize: 11.sp,
                fontWeight: FontWeight.w400,
                color: colorGray1,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
