import 'package:askany/src/bloc/calendar/calendar_bloc.dart';
import 'package:askany/src/bloc/user/user_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/configs/themes/app_colors.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/helpers/date_time_helper.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/extentions/string_extention.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class CalendarHeader extends StatelessWidget {
  final GlobalKey? popUpKey;

  const CalendarHeader({Key? key, this.popUpKey}) : super(key: key);

  void _showPopupMenu(context) async {
    await showMenu(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.sp),
      ),
      context: context,
      position: RelativeRect.fromLTRB(150, 72.sp, 20.sp, 20.sp),
      items: [
        _buildOptionMenu(
          title: Strings.monthlyCalendar.i18n,
          icon: iconCalendarMonth,
          onTap: () {
            BlocProvider.of<CalendarBloc>(context).add(ChooseMonthModeEvent());
          },
        ),
        _buildOptionMenu(
          title: Strings.weeklyCalendar.i18n,
          icon: iconCalendarWeek,
          onTap: () {
            BlocProvider.of<CalendarBloc>(context).add(ChooseWeekModeEvent());
          },
        ),
      ],
      elevation: 8.0,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: headerCalendarColor,
      padding: EdgeInsets.only(
        left: 15.sp,
        bottom: 12.sp,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              BlocBuilder<UserBloc, UserState>(
                builder: (context, state) {
                  return Text(
                    Strings.welcomeBack.i18n +
                        '${(state is UserGetDone ? state.accountModel.fullname : UserLocal().getUser().fullname)?.formatName()}!',
                    style: TextStyle(
                      color: mC,
                      fontSize: 10.5.sp,
                    ),
                  );
                },
              ),
              SizedBox(height: 6.sp),
              BlocBuilder<CalendarBloc, CalendarState>(
                builder: (context, calendar) {
                  DateTime date = calendar is! CalendarLoaded
                      ? DateTime.now()
                      : calendar.props[1];
                  return Text(
                    '${date.year} ${Strings.month[(int.parse(addZeroPrefix(date.month)) - 1)].i18n}',
                    style: TextStyle(
                      color: mC,
                      fontSize: 14.5.sp,
                      fontWeight: FontWeight.w600,
                    ),
                  );
                },
              ),
            ],
          ),
          TouchableOpacity(
            onTap: () {
              _showPopupMenu(context);
            },
            child: Container(
              key: popUpKey,
              height: 40.sp,
              width: 40.sp,
              color: Colors.transparent,
              alignment: Alignment.center,
              child: Image.asset(
                iconMoreVertical,
                width: 3.sp,
              ),
            ),
          ),
        ],
      ),
    );
  }

  PopupMenuItem _buildOptionMenu(
      {required String title, required String icon, required Function onTap}) {
    return PopupMenuItem(
      enabled: false,
      child: TouchableOpacity(
        onTap: () {
          onTap();
          AppNavigator.pop();
        },
        child: Container(
          padding: EdgeInsets.only(
              left: 4.sp, right: 6.sp, bottom: 12.sp, top: 12.sp),
          color: Colors.transparent,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Image.asset(
                icon,
                width: 16.sp,
              ),
              SizedBox(width: 6.5.sp),
              Text(
                title,
                style: TextStyle(
                  color: colorBlack2,
                  fontSize: 12.sp,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
