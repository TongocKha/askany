import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/helpers/date_time_helper.dart';
import 'package:askany/src/models/request_model.dart';
import 'package:askany/src/ui/common/widgets/stack_avatar.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class TimelineWeekCard extends StatelessWidget {
  final RequestModel request;
  TimelineWeekCard({required this.request});

  @override
  Widget build(BuildContext context) {
    int status = getStatusTimeline(request.startTime!, request.endTime!);

    return Container(
      width: 100.w,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            width: 10.w,
            margin: EdgeInsets.only(left: 10.sp),
            alignment: Alignment.center,
            child: Text(
              getTimeString(request.startTime!),
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 10.5.sp,
                color: colorGray1,
              ),
            ),
          ),
          Expanded(
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 6.sp),
                  child: dividerCalendar,
                ),
                Container(
                  width: 100.w,
                  margin: EdgeInsets.fromLTRB(12.sp, 0.sp, 14.sp, 0.sp),
                  padding: EdgeInsets.symmetric(vertical: 16.sp, horizontal: 12.sp),
                  decoration: BoxDecoration(
                    color: status == 1 ? backgroundDetails : colorGreen5,
                    borderRadius: BorderRadius.circular(8.sp),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        getTimelineTitleCard(
                          request.startTime!,
                          request.endTime!,
                        ),
                        style: TextStyle(
                          fontSize: 10.5.sp,
                          color: colorGray1,
                        ),
                      ),
                      SizedBox(height: 4.sp),
                      Text(
                        request.title,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontSize: 12.sp,
                          color: colorBlack2,
                        ),
                      ),
                      SizedBox(height: 6.sp),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          StackAvatar(
                            images: [
                              request.authorUser?.avatar?.urlToImage,
                              request.authorExpert?.avatar?.urlToImage
                            ],
                            size: 22.sp,
                          ),
                          Text(
                            statusTimeline[status],
                            style: TextStyle(
                              fontSize: 10.5.sp,
                              color: colorGreen2,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
