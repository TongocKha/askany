import 'package:askany/src/ui/common/widgets/shimmers/fade_simmer.dart';
import 'package:askany/src/ui/style/calendar_style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class TimelineCardShimmer extends StatelessWidget {
  const TimelineCardShimmer({Key? key, required int index})
      : _index = index,
        super(key: key);
  final int _index;
  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          width: 8.w,
          margin: EdgeInsets.only(left: 10.sp),
          alignment: Alignment.center,
          child: Text(
            _index == 0 ? 'Hôm nay' : '',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 10.5.sp,
              color: colorGray1,
            ),
          ),
        ),
        Expanded(
          child: Column(
            children: [
              _index == 0
                  ? Padding(
                      padding: EdgeInsets.symmetric(vertical: 6.sp),
                      child: dividerCalendar,
                    )
                  : SizedBox(),
              Container(
                decoration: BoxDecoration(
                  color: colorTimelineCardShimmer,
                  borderRadius: BorderRadius.circular(8.sp),
                ),
                width: 100.w,
                margin: EdgeInsets.fromLTRB(12.sp, _index == 0 ? 0.sp : 6.sp, 14.sp, 0.sp),
                padding: EdgeInsets.symmetric(vertical: 16.sp, horizontal: 12.sp),
                child: Row(
                  children: [
                    FadeShimmer.round(
                      size: 35.sp,
                      fadeTheme: FadeTheme.lightReverse,
                    ),
                    SizedBox(width: 10.sp),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        FadeShimmer(
                          width: 165.sp,
                          height: 12.sp,
                          fadeTheme: FadeTheme.lightReverse,
                        ),
                        SizedBox(height: 4.sp),
                        FadeShimmer(
                          width: 100.sp,
                          height: 12.sp,
                          fadeTheme: FadeTheme.lightReverse,
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
