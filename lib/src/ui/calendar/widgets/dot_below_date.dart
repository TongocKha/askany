import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/widgets.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class DotBelowDate extends StatelessWidget {
  final int quantity;
  const DotBelowDate({required this.quantity});
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        ...List.generate(
          quantity,
          (index) => Padding(
            padding: EdgeInsets.only(right: quantity == 1 || index == quantity - 1 ? 0 : 1.25.sp),
            child: Image.asset(
              iconDot,
              width: 2.5.sp,
              color: colorDotBelowDate,
            ),
          ),
        ),
      ],
    );
  }
}
