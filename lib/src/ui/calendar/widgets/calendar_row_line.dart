import 'package:askany/src/bloc/calendar/calendar_bloc.dart';
import 'package:askany/src/helpers/date_time_helper.dart';
import 'package:askany/src/ui/calendar/widgets/dot_below_date.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class CalendarRowLine extends StatefulWidget {
  final List<DateTime> weekDays;
  final DateTime selectedDate;
  CalendarRowLine({required this.weekDays, required this.selectedDate});
  @override
  State<StatefulWidget> createState() => _CalendarRowLineState();
}

class _CalendarRowLineState extends State<CalendarRowLine> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: isLargeDevice ? 6.sp : 1.sp),
      child: Row(
        children: [
          ...widget.weekDays.map((date) => _buildDateButton(date)),
        ],
      ),
    );
  }

  Widget _buildDateButton(DateTime date) {
    final int quantity = BlocProvider.of<CalendarBloc>(context).quantityPerDate(date);
    return Expanded(
      child: TouchableOpacity(
        onTap: () {
          BlocProvider.of<CalendarBloc>(context).add(PickDateEvent(date: date));
        },
        child: Container(
          width: isLargeDevice ? 30.sp : 25.sp,
          height: isLargeDevice ? 30.sp : 25.sp,
          margin: EdgeInsets.symmetric(horizontal: 1.5.sp),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: isEqualTwoDate(widget.selectedDate, date) ? colorGreen5 : Colors.transparent,
            border: Border.all(
              color: isEqualTwoDate(widget.selectedDate, date)
                  ? colorBorderPicked
                  : Colors.transparent,
              width: 0.75.sp,
            ),
          ),
          alignment: Alignment.center,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: isLargeDevice ? 2.5.sp : 0.5.sp),
              Text(
                date.day.toString(),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: locatedInThisMonth(
                    date,
                    compareDate: BlocProvider.of<CalendarBloc>(context).selectedDate,
                  )
                      ? colorBlack2
                      : colorGray2,
                  fontSize: 11.5.sp,
                ),
              ),
              quantity > 0
                  ? Column(
                      children: [
                        SizedBox(height: 2.sp),
                        DotBelowDate(
                          quantity: quantity,
                        ),
                      ],
                    )
                  : SizedBox(height: 3.sp),
            ],
          ),
        ),
      ),
    );
  }
}
