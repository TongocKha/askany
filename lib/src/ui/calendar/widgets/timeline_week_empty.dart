import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class TimelineWeekEmpty extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: 100.w,
            alignment: Alignment.center,
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.only(right: 12.sp),
                  child: Image.asset(
                    imageLeafTimelineWeekEmpty,
                    width: 40.w,
                    fit: BoxFit.contain,
                  ),
                ),
                SizedBox(height: 12.sp),
                Image.asset(
                  imageBoxTimelineWeekEmpty,
                  width: 40.w,
                  fit: BoxFit.contain,
                ),
              ],
            ),
          ),
          SizedBox(height: 30.sp),
          Text(
            Strings.dontHaveAnyConsultationBooked.i18n,
            style: TextStyle(
              fontSize: 12.5.sp,
              color: colorBlack2,
            ),
          ),
        ],
      ),
    );
  }
}
