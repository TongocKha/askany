import 'package:askany/src/constants/constants.dart';
import 'package:flutter/material.dart';

class CalendarTitle extends StatelessWidget {
  final TextStyle textStyle;
  CalendarTitle({required this.textStyle});
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          ...calendarTitle.map((title) => _buildTitle(title)),
        ],
      ),
    );
  }

  Widget _buildTitle(String title) {
    return Container(
      child: Expanded(
        child: Text(
          title,
          textAlign: TextAlign.center,
          style: textStyle,
        ),
      ),
    );
  }
}
