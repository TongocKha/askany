import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/calendar/calendar_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/models/request_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/calendar/widgets/calendar_card.dart';
import 'package:askany/src/ui/calendar/widgets/calendar_header.dart';
import 'package:askany/src/ui/calendar/widgets/timeline_card_shimmer.dart';
import 'package:askany/src/ui/calendar/widgets/timeline_month_card.dart';
import 'package:askany/src/ui/calendar/widgets/timeline_month_empty.dart';
import 'package:askany/src/ui/calendar/widgets/timeline_week_card.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/highlight_coachmark/highlight_coachmark.dart';
import 'package:askany/src/ui/common/widgets/pagination_list_view.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class CalendarScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _CalendarScreenState();
}

class _CalendarScreenState extends State<CalendarScreen> {
  final GlobalKey _popUpKey = GlobalKey(debugLabel: "pop_up");
  final GlobalKey _calendarKey = GlobalKey(debugLabel: "calendar");

  @override
  void initState() {
    super.initState();
    final DateTime? _accountCreateTime = DateTime.tryParse(
        AppBloc.userBloc.getAccount.createdAt ?? DATE_TIME_DEFAULT);

    if (_accountCreateTime != null) {
      if (UserLocal().getShouldShowTutorial() &&
          DateTime.now().difference(_accountCreateTime).inHours < 24) {
        WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
          Future.delayed(Duration(milliseconds: DELAY_200_MS), () {
            _showSwipeTutorial();
          });
        });
      }
    }
  }

  @override
  void dispose() {
    super.dispose();
    AppBloc.calendarBloc.add(CleanCalendarEvent());
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<CalendarBloc, CalendarState>(
      listener: (context, state) {
        if (state is CalendarLoaded) {
          final _cachecService = state.props[4] as List;
          if (_cachecService.length > 0) {
            setState(() {});
          }
        }
      },
      child: Scaffold(
        appBar: appBarTitleBack(
          context,
          '',
          backgroundColor: headerCalendarColor,
          brightness: Brightness.dark,
          paddingLeft: 0,
        ),
        body: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CalendarHeader(popUpKey: _popUpKey),
              BlocBuilder<CalendarBloc, CalendarState>(
                builder: (context, state) {
                  return CalendarCard(
                    calendarKey: _calendarKey,
                    isWeekMode:
                        state is CalendarLoaded ? state.isWeekMode : false,
                    selectedDate: state is CalendarLoaded
                        ? state.selectedDate
                        : DateTime.now(),
                  );
                },
              ),
              BlocBuilder<CalendarBloc, CalendarState>(
                builder: (context, state) {
                  if (state is! CalendarInitial) {
                    return Expanded(
                      child: state.props[0].isEmpty
                          ? TimelineMonthEmpty()
                          : PaginationListView(
                              padding:
                                  EdgeInsets.only(top: 12.sp, bottom: 20.sp),
                              itemCount: state.props[0].length,
                              callBackLoadMore: () => null,
                              callBackRefresh: (Function handleFinished) {
                                AppBloc.calendarBloc.add(
                                  RefreshCalendarEvent(
                                    handleFinished: handleFinished,
                                  ),
                                );
                              },
                              childShimmer: TimelineCardShimmer(index: -1),
                              isLoadMore: state is CalendarLoading,
                              itemBuilder: (context, index) {
                                return TouchableOpacity(
                                  onTap: () {
                                    final RequestModel service =
                                        state.props[0][index];

                                    AppNavigator.push(
                                      Routes.DETAILS_SERVICE,
                                      arguments: {
                                        'requestModel': service,
                                      },
                                    );
                                  },
                                  child: state.props[2]
                                      ? TimelineWeekCard(
                                          request: state.props[0][index],
                                        )
                                      : TimelineMonthCard(
                                          request: state.props[0][index],
                                          previousTask: index == 0
                                              ? null
                                              : state.props[0][index - 1]
                                                  .startTime,
                                        ),
                                );
                              },
                            ),
                    );
                  }
                  return Expanded(
                    child: ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      padding: EdgeInsets.symmetric(vertical: 12.sp),
                      itemCount: ITEM_COUNT_SHIMMER,
                      itemBuilder: (_, index) =>
                          TimelineCardShimmer(index: index),
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _showPopUpMenuTutorial() {
    final CoachMark _coachMark = CoachMark();
    RenderBox _target =
        _popUpKey.currentContext!.findRenderObject() as RenderBox;
    Rect _markRect = _target.localToGlobal(Offset.zero) & _target.size;
    _markRect = Rect.fromCircle(
      center: _markRect.center,
      radius: _markRect.height / 3,
    );
    _coachMark.show(
      targetContext: _popUpKey.currentContext!,
      children: [
        Positioned(
          top: _markRect.top - _markRect.height / 3,
          right: (100.w - _markRect.center.dx) + _markRect.width / 2,
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 18.sp),
            height: 43.sp,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  width: 46.4.w,
                  child: Text(
                    Strings.seeByMonthOrWeekSuggestion.i18n,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 13.sp,
                      fontWeight: FontWeight.w600,
                      height: 1.625,
                      color: colorWhiteCard,
                    ),
                  ),
                ),
                SizedBox(width: 13.sp),
                Image.asset(imageArrowTutorial, width: 12.w),
              ],
            ),
          ),
        ),
      ],
      markRect: _markRect,
      onClose: () {},
    );
  }

  void _showSwipeTutorial() {
    final CoachMark _coachMark = CoachMark();
    RenderBox _target =
        _calendarKey.currentContext!.findRenderObject() as RenderBox;
    Rect _markRect = _target.localToGlobal(Offset.zero) & _target.size;
    _markRect = Rect.fromCenter(
      center: _markRect.center,
      height: 0,
      width: 0,
    );
    _coachMark.show(
      targetContext: _calendarKey.currentContext!,
      children: [
        Positioned(
          top: _markRect.center.dy - (6.7.h + 43.sp + 23.sp) / 2,
          left: _markRect.center.dx - 31.2.w,
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset(imageSwipeTutorial, width: 52.w, height: 6.7.h),
                SizedBox(height: 23.sp),
                SizedBox(
                  width: 62.4.w,
                  height: 43.sp,
                  child: Text(
                    Strings.swipeLeftorRightToSeeOtherMonthSuggestion.i18n,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 13.sp,
                      fontWeight: FontWeight.w600,
                      height: 1.625,
                      color: colorWhiteCard,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
      markRect: _markRect,
      onClose: () {
        _showPopUpMenuTutorial();
      },
    );
  }
}
