import 'package:flutter/material.dart';

import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:askany/src/ui/style/calendar_style.dart';
import 'package:askany/src/ui/style/request_style.dart';

class ReportCompletedView extends StatelessWidget {
  final String reportContent;
  const ReportCompletedView({
    Key? key,
    required this.reportContent,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 18.sp),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            Strings.complain.i18n,
            style: text14w700cBlack1,
          ),
          SizedBox(
            height: 16.sp,
          ),
          Text(
            '${Strings.complainReason.i18n}:',
            style: text13w400cBlack2,
          ),
          SizedBox(
            height: 6.sp,
          ),
          Text(
            reportContent,
            style: text13w400cBlack2,
          ),
          SizedBox(
            height: 18.sp,
          ),
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: 8.sp,
              vertical: 11.sp,
            ),
            height: 40.sp,
            width: 100.w,
            color: colorAddButton,
            child: Text(
              Strings.thisComplainHasBeenClosed.i18n,
              style: text13w400cFontGreen,
            ),
          ),
          SizedBox(
            height: 28.sp,
          ),
        ],
      ),
    );
  }
}
