import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/service_managament/service_management_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/models/service_feedback_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

import 'package:askany/src/models/request_model.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/like_button/like_button.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_form.dart';
import 'package:askany/src/ui/style/style.dart';

class ServiceFeedbackScreen extends StatefulWidget {
  final RequestModel service;
  const ServiceFeedbackScreen({
    Key? key,
    required this.service,
  }) : super(key: key);

  @override
  _AnswerServiceFeedbackScreenState createState() => _AnswerServiceFeedbackScreenState();
}

class _AnswerServiceFeedbackScreenState extends State<ServiceFeedbackScreen> {
  final DateTime now = DateTime.now();
  final List<GlobalKey<LikeButtonState>> _globalKeys = [
    GlobalKey<LikeButtonState>(),
    GlobalKey<LikeButtonState>(),
    GlobalKey<LikeButtonState>(),
    GlobalKey<LikeButtonState>(),
    GlobalKey<LikeButtonState>(),
  ];
  final TextEditingController textEditingController = TextEditingController();

  late RatingModel feedback;
  @override
  void initState() {
    super.initState();
    feedback = RatingModel(
      id: widget.service.myOfferId.toString(),
      stars: 3,
      content: '',
      createdAt: now,
      modifiedAt: now,
    );
  }

  void _postFeedback(BuildContext context) {
    final RatingModel newFeedback = RatingModel(
      id: feedback.id,
      stars: feedback.stars,
      content: textEditingController.text,
      createdAt: DateTime.now(),
      modifiedAt: DateTime.now(),
    );

    showDialogLoading();

    AppBloc.serviceManagamentBloc.add(
      UserRatingAndFeedBackEvent(service: widget.service, serviceFeedbackModel: newFeedback),
    );
  }

  Future<bool> _onLikeButtonTapped(int index) async {
    setState(() {
      feedback = RatingModel(
        id: feedback.id,
        stars: index + 1,
        content: '',
        createdAt: DateTime.now(),
        modifiedAt: DateTime.now(),
      );
    });

    return index < feedback.stars;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: appBarTitleBack(
        context,
        Strings.serviceReview.i18n,
        leading: Container(),
        actions: [
          TouchableOpacity(
            onTap: () {
              AppNavigator.pop();
            },
            child: Image.asset(
              iconRemove,
              width: 14.sp,
              height: 14.sp,
            ),
          ),
          SizedBox(
            width: 17.sp,
          )
        ],
      ),
      body: Column(
        children: [
          Expanded(
            child: Column(
              children: [
                dividerColorGrey2,
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: 16.sp,
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 16.sp),
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(
                          top: 24.sp,
                          bottom: 10.sp,
                        ),
                        child: Text(
                          '${Strings.areYouSatisfiedWith.i18n} ${widget.service.title}${Strings.areYouSatisfiedWithPart2.i18n}',
                          style: text13w400cBlack2,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                          vertical: 4.sp,
                        ),
                        child: Container(
                          height: 18.sp,
                          child: Row(
                            children: [
                              ...List.generate(
                                5,
                                (index) {
                                  return LikeButton(
                                    key: _globalKeys[index],
                                    isLiked: index < feedback.stars,
                                    likeCountAnimationType: LikeCountAnimationType.part,
                                    size: 18.sp,
                                    circleColor: CircleColor(
                                      start: Color(0xff00ddff),
                                      end: Color(0xff0099cc),
                                    ),
                                    bubblesColor: BubblesColor(
                                      dotPrimaryColor: colorStar,
                                      dotSecondaryColor: colorStar,
                                    ),
                                    likeBuilder: (bool isLiked) {
                                      return Container(
                                        height: 40.sp,
                                        width: 40.sp,
                                        padding: EdgeInsets.symmetric(
                                          horizontal: 2.sp,
                                        ),
                                        child: Image.asset(
                                          index < feedback.stars ? iconStar : iconStarGrey,
                                          width: 18.sp,
                                          height: 18.sp,
                                        ),
                                      );
                                    },
                                    likeCountPadding: EdgeInsets.only(left: 6.sp),
                                    onTap: (val) async {
                                      await _onLikeButtonTapped(index);
                                      return !val;
                                    },
                                  );
                                },
                              )
                            ],
                          ),
                        ),
                      ),
                      TextFieldForm(
                        controller: textEditingController,
                        textInputAction: TextInputAction.done,
                        validatorForm: null,
                        maxLine: 7,
                        hintText: Strings.contentHintext.i18n,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.sp),
            child: ButtonPrimary(
              onPressed: () {
                _postFeedback(context);
              },
              text: Strings.postReview.i18n,
            ),
          ),
          SizedBox(
            height: 18.sp,
          ),
        ],
      ),
    );
  }
}
