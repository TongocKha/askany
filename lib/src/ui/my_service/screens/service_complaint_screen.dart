import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/service_managament/service_management_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/models/request_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_form_request.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:askany/src/ui/style/wallet_style.dart';

class ServiceComplaintScreen extends StatefulWidget {
  final RequestModel service;
  const ServiceComplaintScreen({
    Key? key,
    required this.service,
  }) : super(key: key);

  @override
  State<ServiceComplaintScreen> createState() => _ServiceComplaintScreenState();
}

class _ServiceComplaintScreenState extends State<ServiceComplaintScreen> {
  ScrollController scrollController = ScrollController();
  TextEditingController textEditingController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: appBarTitleBack(
        context,
        Strings.serviceComplaining.i18n,
        leading: Container(),
        actions: [
          TouchableOpacity(
            onTap: () {
              AppNavigator.pop();
            },
            child: Image.asset(
              iconRemove,
              height: 14.sp,
              width: 14.sp,
            ),
          ),
          SizedBox(
            width: 17.sp,
          )
        ],
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            children: [
              SizedBox(
                height: 8.5.sp,
              ),
              dividerChat,
              SizedBox(
                height: 22.sp,
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 18.sp),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '${Strings.pleaseTellUsTheReasonWhyYouComplainPart1.i18n} ${widget.service.title} ${Strings.pleaseTellUsTheReasonWhyYouComplainPart2.i18n}',
                      style: walletTitleStyleS13W400,
                    ),
                    SizedBox(
                      height: 22.sp,
                    ),
                    Text(
                      Strings.complainReason.i18n,
                      style: walletTitleStyle13,
                    ),
                    SizedBox(
                      height: 10.sp,
                    ),
                    TextFieldFormRequest(
                      controller: textEditingController,
                      hintText: Strings.enterDescriptionAboutYouHintext.i18n,
                      validatorForm: (val) => null,
                      maxLines: 7,
                    ),
                  ],
                ),
              ),
            ],
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 18.sp, vertical: 18.sp),
            child: ButtonPrimary(
              onPressed: () {
                AppBloc.serviceManagamentBloc.add(
                  ReportOfferEvent(
                    service: widget.service,
                    offerId: widget.service.myOfferId.toString(),
                    reportContent: textEditingController.text,
                  ),
                );
              },
              text: Strings.send.i18n,
            ),
          ),
        ],
      ),
    );
  }
}
