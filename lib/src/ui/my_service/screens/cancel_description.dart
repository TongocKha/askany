import 'package:askany/src/bloc/service_managament/service_management_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/models/cancel_service_reason_model.dart';
import 'package:askany/src/models/request_model.dart';
import 'package:askany/src/ui/request/widgets/order_service_step_three.dart';
import 'package:askany/src/ui/style/request_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

import 'package:askany/src/ui/style/calendar_style.dart';

class CancelDescription extends StatelessWidget {
  String canceledBy(String data) {
    switch (data) {
      case 'user':
        return Strings.customer.i18n;
      case 'expert':
        return Strings.expert.i18n;
      default:
        return 'Unknown';
    }
  }

  final bool pay = false;
  final RequestModel service;

  CancelDescription({
    Key? key,
    required this.service,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    late double _cost;
    late double _price;
    late double _extraPrice;
    late double _totalPrice;
    late DateTime _startTime;
    late DateTime _endTime;
    late int _adviseDuration;
    _startTime = service.startTime!;
    _endTime = service.endTime!;
    _adviseDuration = _endTime.difference(_startTime).inMinutes;
    _cost = service.price!.cost;

    _price = _cost *
        _adviseDuration /
        (service.contactForm == CONTACT_CALL ? 15 : 60);
    _extraPrice = calculateExtraFee(_price, service.participantsCount);
    _totalPrice = _extraPrice + _price;
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          dividerThinkness6,
          Padding(
            padding: EdgeInsets.all(16.sp),
            child: Text(
              Strings.thisServiceHasBeenCanceled.i18n,
              style: text14w700cBlack2,
            ),
          ),
          Padding(
            padding: EdgeInsets.all(16.sp),
            child: Row(
              children: [
                Text(
                  '${Strings.cancelBy.i18n}: ',
                  style: text12w600cBlack2,
                ),
                Text(
                  canceledBy(service.cancelledBy.toString()),
                  style: text12w600cBlack2,
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.sp),
            child: Row(
              children: [
                BlocBuilder<ServiceManagamentBloc, ServiceManagamentState>(
                  builder: (context, state) {
                    String reasonName = '';
                    int indexOfReason =
                        (state.props[1] as List<ReasonModel>).indexWhere(
                      (reason) => reason.id == service.cancel!.reason.id,
                    );
                    if (indexOfReason != -1) {
                      reasonName =
                          (state.props[1] as List<ReasonModel>)[indexOfReason]
                              .name;
                    }
                    return Flexible(
                      child: Text(
                        '${Strings.cancelReason.i18n}: ' + reasonName,
                        style: text13w400cBlack2,
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
          pay
              ? Padding(
                  padding: EdgeInsets.all(16.sp),
                  child: Container(
                    color: colorChosenCard,
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: 16.sp, horizontal: 11.sp),
                      child: Text(
                        Strings.refundCompletedNoti.i18n,
                        style: text12w400cFontGreen,
                      ),
                    ),
                  ),
                )
              : Padding(
                  padding: EdgeInsets.all(16.sp),
                  child: Container(
                    color: colorNotChosenCard,
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: 16.sp, horizontal: 11.sp),
                      child: Text(
                        Strings.refundIn24HoursNoti.i18n,
                        style: TextStyle(
                          fontSize: 12.sp,
                          fontWeight: FontWeight.w400,
                          color: colorFinished,
                          height: 1.25.sp,
                        ),
                        textAlign: TextAlign.justify,
                      ),
                    ),
                  ),
                ),
          service.cancelledBy.toString() == 'user'
              ? Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    dividerThinkness6NotMargin,
                    SizedBox(height: 20.sp),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 16.sp),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            child: Text(
                              Strings.refundDetails.i18n,
                              style: TextStyle(
                                color: colorBlack1,
                                fontSize: 14.sp,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          ),
                          SizedBox(height: 16.sp),
                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  Strings.refundAt.i18n,
                                  style: TextStyle(
                                    color: colorBlack2,
                                    fontSize: 12.sp,
                                  ),
                                ),
                                Text(
                                  Strings.askAnyWallet.i18n,
                                  style: TextStyle(
                                    color: colorBlack2,
                                    fontSize: 12.sp,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 16.sp),
                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  Strings.refundAmount.i18n,
                                  style: TextStyle(
                                    color: colorBlack2,
                                    fontSize: 12.sp,
                                  ),
                                ),
                                Text(
                                  service.price!.priceString(_totalPrice),
                                  style: TextStyle(
                                    color: colorBlack2,
                                    fontSize: 12.sp,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 16.sp),
                          Text(
                            Strings.repaidTime.i18n,
                            style: TextStyle(
                              color: colorBlack2,
                              fontSize: 12.sp,
                            ),
                          ),
                          SizedBox(height: 20.sp),
                        ],
                      ),
                    ),
                  ],
                )
              : Container(),
        ],
      ),
    );
  }
}
