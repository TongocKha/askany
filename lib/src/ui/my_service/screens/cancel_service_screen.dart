import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/service_managament/service_management_bloc.dart';
import 'package:askany/src/helpers/extentions/string_extention.dart';
import 'package:askany/src/models/cancel_service_reason_model.dart';
import 'package:askany/src/models/request_model.dart';
import 'package:askany/src/ui/my_service/widgets/cancel_bottom_sheet.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_form.dart';
import 'package:askany/src/ui/style/style.dart';

class CancelServiceScreen extends StatefulWidget {
  final int tabNumber;
  final RequestModel service;
  const CancelServiceScreen({
    Key? key,
    required this.tabNumber,
    required this.service,
  }) : super(key: key);

  @override
  _CancelServiceScreenState createState() => _CancelServiceScreenState();
}

class _CancelServiceScreenState extends State<CancelServiceScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  late FocusNode focusNode;
  late ScrollController scrollController;
  late TextEditingController _reasonController;
  late TextEditingController _contentController;
  late RequestModel _service;
  ReasonModel? _reason;

  @override
  void initState() {
    super.initState();
    _reasonController = TextEditingController(text: Strings.selectAReason.i18n);
    _contentController = TextEditingController(text: '');
    WidgetsBinding.instance?.addPostFrameCallback(_onLayoutDone);
    focusNode = FocusNode();

    scrollController = ScrollController();
    focusNode.addListener(() {
      if (focusNode.hasFocus) {
        scrollController.animateTo(scrollController.position.maxScrollExtent,
            duration: Duration(milliseconds: 500), curve: Curves.ease);
      }
    });
    _service = widget.service;
  }

  _onLayoutDone(_) {
    FocusScope.of(context).requestFocus(focusNode);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: appBarTitleBack(
        context,
        Strings.cancelThisServiceAppbartext.i18n,
        leading: Container(),
        actions: [
          TouchableOpacity(
            onTap: () {
              AppNavigator.pop();
            },
            child: Image.asset(
              iconRemove,
              width: 14.sp,
              height: 14.sp,
            ),
          ),
          SizedBox(
            width: 17.sp,
          )
        ],
      ),
      body: Column(
        children: [
          Divider(),
          Expanded(
            child: SingleChildScrollView(
              controller: scrollController,
              physics: NeverScrollableScrollPhysics(),
              child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    SizedBox(
                      height: 24.sp,
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 18.sp),
                      child: Column(
                        children: [
                          Text(
                            '${Strings.pleaseTellUsTheReasonWhyYouCancelThisServicePart1.i18n} ${_service.title} ${Strings.pleaseTellUsTheReasonWhyYouCancelThisServicePart2.i18n}',
                            style: text13w400cBlack2,
                          ),
                          SizedBox(
                            height: 24.sp,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                '${Strings.reason.i18n} (*)',
                                style: text13w600cBlack2,
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 12.sp,
                          ),
                          TextFieldForm(
                            onTap: () {
                              showModalBottomSheet(
                                context: context,
                                isScrollControlled: true,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.vertical(
                                    top: Radius.circular(20.sp),
                                  ),
                                ),
                                builder: (context) => CancelBottomSheet(
                                  reason: _reason,
                                  handlePressed: (ReasonModel reason) {
                                    _reasonController.text = reason.name.limitString(limit: 30);
                                    _reason = reason;
                                  },
                                ),
                              );
                            },
                            controller: _reasonController,
                            isActive: true,
                            readOnly: true,
                            suffixIcon: Container(
                              color: Colors.transparent,
                              child: Image.asset(
                                iconArrowDown,
                                width: 10.sp,
                              ),
                            ),
                            validatorForm: (val) =>
                                _reason == null ? Strings.reasonCannotBeLeftBlankErrortext : null,
                          ),
                          SizedBox(
                            height: 24.sp,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                Strings.details.i18n,
                                style: text13w600cBlack2,
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 12.sp,
                          ),
                          TextFieldForm(
                            controller: _contentController,
                            textInputAction: TextInputAction.done,
                            validatorForm: (val) => null,
                            maxLine: 7,
                            hintText: Strings.contentHintext.i18n,
                          ),
                          SizedBox(
                            height: 30.sp,
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.sp),
            child: ButtonPrimary(
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  showDialogLoading();
                  AppBloc.serviceManagamentBloc.add(
                    CancelOfferEvent(
                      service: widget.service,
                      tabNumber: widget.tabNumber,
                      offerId: _service.myOfferId!,
                      reason: _reason?.id ?? '',
                      content: _contentController.text,
                    ),
                  );
                }
              },
              text: Strings.confirm.i18n,
              fontWeight: FontWeight.w700,
            ),
          ),
          SizedBox(height: 18.sp),
        ],
      ),
    );
  }
}
