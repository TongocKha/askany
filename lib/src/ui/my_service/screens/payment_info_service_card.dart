import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/helpers/extentions/string_extention.dart';
import 'package:askany/src/models/request_model.dart';
import 'package:askany/src/ui/request/widgets/order_service_step_three.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:intl/intl.dart';

class PaymentInfoServiceCard extends StatefulWidget {
  final RequestModel requestModel;
  const PaymentInfoServiceCard({required this.requestModel});
  @override
  State<StatefulWidget> createState() => _PaymentInfoServiceCardState();
}

class _PaymentInfoServiceCardState extends State<PaymentInfoServiceCard> {
  late DateTime _startTime;
  late DateTime _endTime;
  late int _adviseDuration;

  late double _price;
  late double _extraPrice;

  @override
  void initState() {
    super.initState();
    _startTime = widget.requestModel.startTime!;
    _endTime = widget.requestModel.endTime!;
    _adviseDuration = _endTime.difference(_startTime).inMinutes;
    if (_adviseDuration == 0) {
      _adviseDuration = widget.requestModel.myOffer!.price.totalMinutes;
    }

    _price = calculateSubTotal(widget.requestModel);
    _extraPrice = calculateExtraFee(_price, widget.requestModel.participantsCount);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.fromLTRB(16.sp, 14.sp, 16.sp, 26.sp),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            Strings.paymentInformation.i18n,
            style: TextStyle(
              fontSize: 14.sp,
              fontWeight: FontWeight.w700,
              color: colorBlack1,
            ),
          ),
          SizedBox(height: 16.sp),
          _buildRowTile(
              title: Strings.serviceType.i18n,
              content: widget.requestModel.contactForm == CONTACT_MEET
                  ? Strings.meet.i18n
                  : Strings.call.i18n),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 20.sp),
            child: dividerChat,
          ),
          _buildRowTile(
            title: Strings.participants.i18n,
            content: '${widget.requestModel.participantsCount} ${Strings.people.i18n}',
          ),
          SizedBox(height: 12.sp),
          _buildRowTile(
            title:
                '${Strings.costFor.i18n} ${widget.requestModel.price!.totalMinutes} ${Strings.minutes.i18n}',
            content: widget.requestModel.price!.costString,
          ),
          SizedBox(height: 12.sp),
          _buildRowTile(
            title: Strings.adviseDuration.i18n,
            content: '$_adviseDuration ${Strings.minutes.i18n}',
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 20.sp),
            child: dividerChat,
          ),
          _buildRowTile(
            title: Strings.intoPrice.i18n,
            content:
                '${convertMoneyToString(calculateSubTotal(widget.requestModel), widget.requestModel.price!.currency)}',
          ),
          SizedBox(height: 12.sp),
          _buildRowTile(
            title: Strings.extraFee.i18n,
            content: widget.requestModel.price!.priceString(_extraPrice),
          ),
          SizedBox(height: 12.sp),
          _buildRowTile(
            title: Strings.totalPrice.i18n,
            content:
                '${convertMoneyToString(calculateTotal(widget.requestModel), widget.requestModel.price!.currency)}',
          ),
        ],
      ),
    );
  }

  Widget _buildRowTile({required String title, required String content}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          title,
          style: TextStyle(
            fontSize: 13.sp,
            fontWeight: FontWeight.w400,
            color: colorBlack2,
          ),
        ),
        SizedBox(width: 16.sp),
        Expanded(
          child: Text(
            content,
            textAlign: TextAlign.end,
            style: TextStyle(
              fontSize: 13.sp,
              fontWeight: FontWeight.w400,
              color: colorBlack2,
            ),
          ),
        ),
      ],
    );
  }

  double calculateTotal(RequestModel request) {
    final _subTotal = calculateSubTotal(request);
    final _extraFee = calculateExtraFee(_subTotal, request.participantsCount);
    return _subTotal + _extraFee;
  }

  double calculateSubTotal(RequestModel request) {
    double _subTotal = 0;
    if (request.contactForm == CONTACT_CALL) {
      _subTotal = request.price!.cost / request.price!.totalMinutes * _adviseDuration;
    } else if (request.contactForm == CONTACT_MEET) {
      _subTotal = request.price!.cost / request.price!.totalMinutes * _adviseDuration;
    }
    return _subTotal;
  }

  String convertMoneyToString(double money, String currency) {
    switch (currency) {
      case 'vnd':
        return money.toStringAsFixed(0).formatMoney() + 'đ';
      case 'usd':
        NumberFormat format = NumberFormat('#,###.##');
        return format.format(money) + '\$';
      default:
        return '';
    }
  }
}
