import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/service_managament/service_management_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

import 'package:askany/src/models/request_model.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_form.dart';
import 'package:askany/src/ui/style/style.dart';

class ReplyFeedbackScreen extends StatefulWidget {
  final RequestModel service;
  const ReplyFeedbackScreen({
    Key? key,
    required this.service,
  }) : super(key: key);

  @override
  _ReplyFeedbackScreenState createState() => _ReplyFeedbackScreenState();
}

class _ReplyFeedbackScreenState extends State<ReplyFeedbackScreen> {
  final TextEditingController textEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: appBarTitleBack(
        context,
        Strings.replyFeedbackAppbartext.i18n,
        leading: Container(),
        actions: [
          TouchableOpacity(
            onTap: () {
              AppNavigator.pop();
            },
            child: Image.asset(
              iconRemove,
              width: 14.sp,
              height: 14.sp,
            ),
          ),
          SizedBox(
            width: 17.sp,
          )
        ],
      ),
      body: Container(
        child: Column(
          children: [
            dividerColorGrey2,
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: 16.sp,
              ),
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 16.sp),
                child: Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(
                        vertical: 20.sp,
                      ),
                      child: Text(
                        '${Strings.replyFeedbackOf.i18n} ${widget.service.authorUser?.fullname}',
                        style: text13w400cBlack2,
                      ),
                    ),
                    TextFieldForm(
                      controller: textEditingController,
                      textInputAction: TextInputAction.done,
                      validatorForm: null,
                      maxLine: 7,
                      hintText: Strings.contentHintext.i18n,
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.sp),
              child: ButtonPrimary(
                onPressed: () {
                  final ratingId = widget.service.rating!.id;
                  final content = textEditingController.text;

                  AppBloc.serviceManagamentBloc.add(
                    ReplyFeedbackEvent(ratingId: ratingId, content: content),
                  );
                },
                text: Strings.send.i18n,
              ),
            ),
            SizedBox(height: 18.sp)
          ],
        ),
      ),
    );
  }
}
