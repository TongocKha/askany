import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/ui/my_service/screens/payment_info_service_card.dart';
import 'package:askany/src/ui/my_service/widgets/service_details_bottom_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:askany/src/bloc/service_managament/service_management_bloc.dart';
import 'package:askany/src/models/request_model.dart';
import 'package:askany/src/models/step_model.dart';
import 'package:askany/src/ui/my_service/screens/report_completed_view.dart';
import 'package:askany/src/ui/my_service/screens/reporting_view.dart';
import 'package:askany/src/ui/my_service/screens/cancel_description.dart';
import 'package:askany/src/ui/my_service/screens/service_description_card.dart';
import 'package:askany/src/ui/calendar/widgets/step_card.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/my_service/widgets/service_feedback_view.dart';
import 'package:askany/src/ui/style/style.dart';

class ServiceDetailsScreen extends StatefulWidget {
  final RequestModel service;
  const ServiceDetailsScreen({
    required this.service,
  });
  @override
  State<ServiceDetailsScreen> createState() => _ServiceDetailsScreenState();
}

class _ServiceDetailsScreenState extends State<ServiceDetailsScreen> {
  late int _tabNumber;
  late RequestModel _service;
  late bool _notAbleToDisplayFeedback;
  late bool _hasNotCanceledYet;
  late bool _hasNotReported;
  late List<StepModel> _steps;

  @override
  void initState() {
    super.initState();
    update();
  }

  void update() {
    _service = widget.service;
    _notAbleToDisplayFeedback = _service.myOffer == null
        ? true
        : (_service.myOffer!.status == 3 ||
            _service.myOffer!.status == 2 ||
            _service.myOffer!.status == 4);
    _hasNotCanceledYet = _service.cancel == null;
    _hasNotReported = _service.report == null;

    _service = widget.service;
    _notAbleToDisplayFeedback = _service.myOffer == null
        ? true
        : (_service.myOffer!.status == 3 ||
            _service.myOffer!.status == 2 ||
            _service.myOffer!.status == 4);

    _steps = [
      StepModel(
        title: Strings.buyingService.i18n,
        createdAt: _service.askanyPaymentTime!,
      ),
      StepModel(
        title: _service.report != null ||
                ((widget.service.myOffer?.status ?? OFFER_ACTIVE) >=
                    OFFER_CONFIRMED)
            ? Strings.confirmingService.i18n
            : '',
        createdAt: _service.confirmedTime!,
      ),
      StepModel(
        title: _service.report != null ||
                ((widget.service.myOffer?.status ?? OFFER_ACTIVE) >=
                        OFFER_CONFIRMED &&
                    DateTime.now()
                        .isAfter(widget.service.startTime ?? DateTime.now()))
            ? Strings.advising.i18n
            : '',
        createdAt: _service.startTime!,
      ),
      StepModel(
        title: _service.report != null ||
                ((widget.service.myOffer?.status ?? OFFER_ACTIVE) >=
                    OFFER_COMPLETED)
            ? Strings.complete.i18n
            : '',
        createdAt: _service.endTime!,
      ),
      StepModel(
        title: widget.service.cancel != null ? Strings.cancelService.i18n : '',
        createdAt: DateTime.now(),
        isDanger: true,
      ),
    ].reversed.toList();

    _steps = _steps.where((element) => element.title.isNotEmpty).toList();

    if (_service.report != null) {
      _tabNumber = SERVICE_COMPLAINED;
    } else if (widget.service.cancel != null) {
      _tabNumber = SERVICE_CANCELED;
    } else if (widget.service.report != null ||
        ((widget.service.myOffer?.status ?? OFFER_ACTIVE) >= OFFER_COMPLETED)) {
      _tabNumber = SERVICE_COMPLETED;
    } else if ((widget.service.myOffer?.status ?? OFFER_ACTIVE) >=
            OFFER_CONFIRMED &&
        DateTime.now().isAfter(widget.service.startTime ?? DateTime.now())) {
      _tabNumber = SERVICE_ADVISED;
    } else if ((widget.service.myOffer?.status ?? OFFER_ACTIVE) >=
        OFFER_CONFIRMED) {
      _tabNumber = SERVICE_CONFIRMED;
    } else {
      _tabNumber = SERVICE_CONFIRMING;
    }
  }

  Widget _buildReportView() {
    final int _reportStatus = _service.report?.status ?? -1;
    if (_reportStatus == 0) {
      return Column(
        children: [
          dividerThinkness6,
          Reporting(
            content: _service.report?.content ?? '',
          ),
        ],
      );
    } else if (_reportStatus == 1) {
      return Column(
        children: [
          dividerThinkness6,
          ReportCompletedView(
            reportContent: _service.report?.content ?? '',
          ),
        ],
      );
    }
    return dividerThinkness6;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarTitleBack(context, Strings.serviceDetailsAppbartext.i18n),
      body: BlocListener<ServiceManagamentBloc, ServiceManagamentState>(
        listener: (context, state) {
          if (state is GetDoneService) {
            List<RequestModel> _currentServices = [];
            state.services.values.forEach((element) {
              _currentServices.addAll(element);
            });

            final int indexOfModifiedService = _currentServices.indexWhere(
              (service) => service.id == widget.service.id,
            );

            if (indexOfModifiedService != -1) {
              setState(
                () {
                  _service = _currentServices[indexOfModifiedService];
                  update();
                },
              );
            }
          }
        },
        child: Container(
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              children: [
                dividerThinkness6,
                StepCard(
                  steps: _steps,
                ),
                SizedBox(height: 18.sp),
                Visibility(
                  visible: !_hasNotCanceledYet,
                  child: CancelDescription(
                    service: _service,
                  ),
                ),
                Visibility(
                  visible: !_hasNotReported,
                  child: _buildReportView(),
                ),
                dividerThinkness6NotMargin,
                ServiceDescriptionCard(
                  requestModel: _service,
                ),
                Visibility(
                  visible: _notAbleToDisplayFeedback,
                  child: ServiceFeedbackView(
                    service: _service,
                  ),
                ),
                SizedBox(height: 5.sp),
                dividerThinkness6NotMargin,
                SizedBox(height: 5.sp),
                PaymentInfoServiceCard(
                  requestModel: _service,
                ),
                SizedBox(height: 30.sp),
                ServiceDetailsBottomButton(
                  offerStatus: _service.myOffer?.status ?? -10,
                  service: _service,
                  tabNumber: _tabNumber,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
