import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/chat/chat_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/constants/provinces.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/models/request_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/custom_image/network_image/cached_image.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ServiceDescriptionCard extends StatefulWidget {
  final RequestModel requestModel;
  ServiceDescriptionCard({required this.requestModel});
  @override
  State<StatefulWidget> createState() => _ServiceDescriptionCardState();
}

class _ServiceDescriptionCardState extends State<ServiceDescriptionCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.fromLTRB(
        16.sp,
        14.sp,
        16.sp,
        26.sp,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            Strings.service.i18n,
            style: TextStyle(
              fontSize: 14.sp,
              fontWeight: FontWeight.w700,
              color: colorBlack1,
            ),
          ),
          SizedBox(height: 20.sp),
          Text(
            widget.requestModel.title,
            style: TextStyle(
              fontSize: 13.sp,
              fontWeight: FontWeight.w600,
              color: colorBlack1,
            ),
          ),
          SizedBox(height: 12.sp),
          Text(
            '${Strings.serviceType.i18n}: ${widget.requestModel.contactForm == CONTACT_MEET ? Strings.meet.i18n : Strings.call.i18n}',
            style: TextStyle(
              fontSize: 12.sp,
              fontWeight: FontWeight.w400,
              color: colorBlack2,
            ),
          ),
          SizedBox(height: 12.sp),
          Text(
            '${Strings.adviseDuration.i18n}: ${DateFormat('dd/MM/yyyy - HH:mm').format(widget.requestModel.startTime!)} - ${DateFormat('HH:mm').format(widget.requestModel.endTime!)}',
            style: TextStyle(
              fontSize: 12.sp,
              fontWeight: FontWeight.w400,
              color: colorBlack2,
            ),
          ),
          Visibility(
            visible: widget.requestModel.contactForm == CONTACT_MEET,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 12.sp),
                Text(
                  '${Strings.place.i18n}: ${widget.requestModel.locationAddress}',
                  style: TextStyle(
                    fontSize: 12.sp,
                    fontWeight: FontWeight.w400,
                    color: colorBlack2,
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
              top: 30.sp,
              bottom: 10.sp,
            ),
            child: dividerChat,
          ),
          Container(
            child: UserLocal().getIsExpert()
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 20.sp),
                        child: Text(
                          Strings.contacter.i18n,
                          style: text14w700cBlack1,
                        ),
                      ),
                      TouchableOpacity(
                        onTap: () {
                          showDialogLoading();
                          AppBloc.chatBloc.add(CheckCanChatEvent(
                              expertId: widget.requestModel.authorUser!.id));
                        },
                        child: Row(
                          children: [
                            CustomNetworkImage(
                              height: 40.sp,
                              width: 40.sp,
                              urlToImage: widget
                                  .requestModel.authorUser!.avatar?.urlToImage,
                            ),
                            SizedBox(
                              width: 12.sp,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  widget.requestModel.authorUser?.fullname ??
                                      '',
                                  style: text13w600cBlack2,
                                ),
                                Text(
                                  Province()
                                      .getProvinceByCode(widget
                                          .requestModel.authorUser!.province)
                                      .toString(),
                                  style: text11w400cGrey1,
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ],
                  )
                : Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        Strings.expert.i18n,
                        style: TextStyle(
                          fontSize: 14.sp,
                          fontWeight: FontWeight.w700,
                          color: colorBlack1,
                        ),
                      ),
                      SizedBox(height: 20.sp),
                      TouchableOpacity(
                        onTap: () {
                          AppNavigator.push(
                            Routes.DETAILS_SPECIALIST,
                            arguments: {
                              'expertId': widget.requestModel.authorExpert!.id,
                            },
                          );
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            CustomNetworkImage(
                              height: 40.sp,
                              width: 40.sp,
                              urlToImage: widget.requestModel.authorExpert!
                                  .avatar?.urlToImage,
                            ),
                            SizedBox(width: 12.sp),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  widget.requestModel.authorExpert!.fullname,
                                  style: TextStyle(
                                    fontSize: 13.sp,
                                    fontWeight: FontWeight.w600,
                                    color: colorBlack2,
                                  ),
                                ),
                                SizedBox(height: 4.sp),
                                Text(
                                  Province().getProvinceByCode(widget
                                      .requestModel.authorExpert!.province)!,
                                  style: TextStyle(
                                    fontSize: 11.sp,
                                    fontWeight: FontWeight.w600,
                                    color: colorGray1,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
          ),
        ],
      ),
    );
  }
}
