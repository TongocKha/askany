import 'package:flutter/material.dart';

import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:askany/src/ui/style/calendar_style.dart';
import 'package:askany/src/ui/style/request_style.dart';

class Reporting extends StatelessWidget {
  final String content;
  const Reporting({
    Key? key,
    required this.content,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 18.sp),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            Strings.complaint.i18n,
            style: text14w700cBlack1,
          ),
          SizedBox(
            height: 16.sp,
          ),
          Text(
            '${Strings.complainReason.i18n}:',
            style: text13w400cBlack2,
          ),
          SizedBox(
            height: 6.sp,
          ),
          Text(
            content,
            style: text13w400cBlack2,
          ),
          SizedBox(
            height: 18.sp,
          ),
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: 8.sp,
              vertical: 14.sp,
            ),
            color: colorNotChosenCard,
            child: Text(
              Strings.complaintIsBeingVerify.i18n,
              style: TextStyle(
                fontSize: 13.sp,
                fontWeight: FontWeight.w400,
                color: Color(0xffB31D1D),
                height: 1.28.sp,
              ),
              textAlign: TextAlign.justify,
            ),
          ),
          SizedBox(
            height: 28.sp,
          ),
        ],
      ),
    );
  }
}
