import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/service_managament/service_management_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/ui/account/screens/service_managament_screen.dart';
import 'package:askany/src/ui/account/widgets/service_managament_shimmer_card.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class MyServiceScreen extends StatefulWidget {
  final int initialIndex;
  const MyServiceScreen({required this.initialIndex});
  @override
  State<StatefulWidget> createState() => _MyServiceScreenState();
}

class _MyServiceScreenState extends State<MyServiceScreen>
    with TickerProviderStateMixin {
  late TabController _tabController;
  late int _currentIndex;

  List<int> _tabNumber = [
    SERVICE_CONFIRMING,
    SERVICE_CONFIRMED,
    SERVICE_ADVISED,
    SERVICE_COMPLETED,
    SERVICE_CANCELED,
    SERVICE_COMPLAINED
  ];

  @override
  void initState() {
    super.initState();
    _currentIndex = widget.initialIndex;
    AppBloc.serviceManagamentBloc
        .add(OnServiceEvent(status: _tabNumber[_currentIndex]));
    _tabController = TabController(
      initialIndex: widget.initialIndex,
      length: _tabNumber.length,
      vsync: this,
    );

    _tabController.animation?.addListener(() {
      _changeTab(
          _tabController.animation?.value.round() ?? _tabController.index);
    });
  }

  _changeTab(int index) {
    if (_currentIndex != index) {
      setState(() {
        _currentIndex = index;
      });
      AppBloc.serviceManagamentBloc
          .add(OnServiceEvent(status: _tabNumber[_currentIndex]));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarTitleBack(
        context,
        Strings.myServiceManagementTitle.i18n,
        brightness: Brightness.light,
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(42.sp),
          child: Padding(
            padding: EdgeInsets.only(left: 4.sp),
            child: Align(
              alignment: Alignment.centerLeft,
              child: TabBar(
                isScrollable: true,
                physics: BouncingScrollPhysics(),
                labelStyle: TextStyle(
                  fontSize: 12.sp,
                  color: colorBlack2,
                ),
                indicatorColor: colorGreen2,
                indicatorSize: TabBarIndicatorSize.label,
                indicatorPadding: EdgeInsets.symmetric(horizontal: 20.sp),
                indicatorWeight: 1.5.sp,
                controller: _tabController,
                labelColor: colorBlack2,
                unselectedLabelColor: colorGray1,
                unselectedLabelStyle: TextStyle(
                  fontSize: 12.sp,
                  color: colorBlack2,
                ),
                onTap: _changeTab,
                tabs: <Widget>[
                  Tab(
                    child: Text(Strings.confirming.i18n),
                  ),
                  Tab(
                    child: Text(Strings.confirmed.i18n),
                  ),
                  Tab(
                    child: Text(Strings.advised.i18n),
                  ),
                  Tab(
                    child: Text(Strings.completed.i18n),
                  ),
                  Container(
                    width: 60.sp,
                    child: Tab(
                      child: Text(Strings.canceled.i18n),
                    ),
                  ),
                  Container(
                    width: 75.sp,
                    child: Tab(
                      child: Text(Strings.complained.i18n),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      body: Container(
        color: backgroundDetails,
        child: Column(
          children: [
            dividerThinkness6NotMargin,
            Expanded(
              child: TabBarView(
                physics: ClampingScrollPhysics(),
                controller: _tabController,
                children: <Widget>[
                  ..._tabNumber.map(
                    (tabNumber) {
                      return BlocBuilder<ServiceManagamentBloc,
                          ServiceManagamentState>(
                        builder: (context, state) {
                          return state is ServiceManagamentInitial
                              ? ListView.builder(
                                  physics: NeverScrollableScrollPhysics(),
                                  itemCount: ITEM_COUNT_SHIMMER,
                                  itemBuilder: (context, index) {
                                    return ServiceShimmerCard(
                                      index: index,
                                    );
                                  },
                                )
                              : ServiceManagamentScreen(
                                  currentState: state,
                                  tabNumber: tabNumber,
                                  services: state.props[0]?[tabNumber] ?? [],
                                );
                        },
                      );
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
