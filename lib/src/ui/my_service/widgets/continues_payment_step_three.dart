import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/helpers/currency_helper.dart';
import 'package:askany/src/models/offer_model.dart';
import 'package:askany/src/models/request_model.dart';
import 'package:askany/src/models/slide_mode.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_with_text_and_pop_button.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:askany/src/ui/request/widgets/radio_card.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:askany/src/helpers/extentions/string_extention.dart';

class ContinuesPaymentStepThree extends StatefulWidget {
  const ContinuesPaymentStepThree({
    Key? key,
    required this.request,
    required this.offer,
    required this.onSelectedValueChanged,
  }) : super(key: key);

  final RequestModel request;
  final OfferModel offer;
  final Function(PaymentMethod) onSelectedValueChanged;

  @override
  _ContinuesPaymentStepThreeState createState() =>
      _ContinuesPaymentStepThreeState();
}

class _ContinuesPaymentStepThreeState extends State<ContinuesPaymentStepThree> {
  PaymentMethod _paymentMethod = PaymentMethod.byVNPay;
  double _wallet = AppBloc.userBloc.getAccount.wallet ?? 0;
  double _subTotal = 0;
  double _extraFee = 0;
  double _total = 0;
  late int _adviseDuration;

  @override
  void initState() {
    super.initState();

    _adviseDuration =
        widget.request.endTime!.difference(widget.request.startTime!).inMinutes;
    _subTotal =
        calculateSubTotal(widget.request, widget.offer, _adviseDuration);
    _extraFee = calculateExtraFee(_subTotal, widget.request.participantsCount);
    _total = _subTotal + _extraFee;
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(
              left: 16.sp,
              top: 22.sp,
              right: 16.sp,
              bottom: 28.sp,
            ),
            child: _buildPaymentMethod(),
          ),
          dividerThinkness6NotMargin,
          Container(
            padding: EdgeInsets.only(
              left: 16.sp,
              top: 20.5.sp,
              right: 16.sp,
              bottom: 30.sp,
            ),
            child: _buildPaymentInfo(),
          )
        ],
      ),
    );
  }

  Widget _buildPaymentInfo() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        _SingleRowOfText(
          title: Strings.serviceType.i18n,
          value: widget.request.contactForm == CONTACT_CALL
              ? Strings.call.i18n
              : Strings.meet.i18n,
        ),
        SizedBox(height: 12.sp),
        _SingleRowOfText(
          title: Strings.participants.i18n,
          value: '${widget.request.participantsCount} ${Strings.people.i18n}',
        ),
        SizedBox(height: 12.sp),
        _SingleRowOfText(
          title: '${Strings.price.i18n} ' +
              '${widget.offer.price.totalMinutes} ${Strings.minutes.i18n}',
          value: widget.offer.price.costString,
        ),
        SizedBox(height: 20.sp),
        dividerColorGrey2,
        SizedBox(height: 20.sp),
        _SingleRowOfText(
          title: Strings.adviseDuration.i18n,
          value: '${widget.offer.price.totalMinutes} ${Strings.minutes.i18n}',
        ),
        SizedBox(height: 12.sp),
        _SingleRowOfText(
          title: Strings.intoPrice.i18n,
          value: convertMoneyToString(
            _subTotal,
            widget.request.budget!.currency,
          ),
        ),
        SizedBox(height: 12.sp),
        _SingleRowOfText(
          title: Strings.extraFee.i18n,
          value: convertMoneyToString(
            _extraFee,
            widget.request.budget!.currency,
          ),
        ),
      ],
    );
  }

  Widget _buildPaymentMethod() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Text(
          Strings.choosePaymentMethod.i18n,
          style: TextStyle(
            fontSize: 14.sp,
            color: colorFontGreen,
            height: 1.53,
            fontWeight: FontWeight.w600,
          ),
        ),
        SizedBox(height: 15.sp),
        RadioCard<PaymentMethod>(
          title: Strings.paymentbyVNPay.i18n,
          paymentMethod: _paymentMethod,
          value: PaymentMethod.byVNPay,
          onChanged: (val) {
            setState(() {
              _paymentMethod = val;
            });
            widget.onSelectedValueChanged(_paymentMethod);
          },
        ),
        SizedBox(height: 18.sp),
        RadioCard<PaymentMethod>(
          title: Strings.paymentByAskAnyWallet.i18n,
          subTitle: '${Strings.walletBalance.i18n}: ' +
              convertMoneyToString(
                CurrencyHelper().convertMoney(
                  _wallet,
                  Strings.vnd.i18n.toLowerCase(),
                  widget.request.budget!.currency,
                ),
                widget.request.budget!.currency,
              ),
          paymentMethod: _paymentMethod,
          value: PaymentMethod.byAppWallet,
          onChanged: (val) {
            setState(() {
              _paymentMethod = val;
            });
            widget.onSelectedValueChanged(_paymentMethod);
          },
          disable: _wallet < _total,
          onDisableTap: (val) {
            dialogAnimationWrapper(
              slideFrom: SlideMode.fade,
              child: DialogWithTextAndPopButton(
                bodyBefore: Strings.notEnoughBalanceNoti.i18n,
                bodyColor: colorBlack1,
                bodyAlign: TextAlign.center,
                bodyFontSize: 13.sp,
              ),
            );
          },
        ),
      ],
    );
  }
}

class _SingleRowOfText extends StatelessWidget {
  const _SingleRowOfText({
    Key? key,
    required String title,
    required String value,
  })  : _title = title,
        _value = value,
        super(key: key);

  final String _title;
  final String _value;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          _title,
          style: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: 13.sp,
            color: colorBlack2,
            height: 1.625,
          ),
        ),
        Text(
          _value,
          style: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: 13.sp,
            color: colorBlack2,
            height: 1.625,
          ),
        ),
      ],
    );
  }
}

String convertMoneyToString(double money, String currency) {
  switch (currency) {
    case 'vnd':
      return money.toStringAsFixed(0).formatMoney() + 'đ';
    case 'usd':
      NumberFormat format = NumberFormat('#,###.##');
      return format.format(money) + '\$';
    default:
      return '';
  }
}

double calculateTotal(RequestModel request, OfferModel offer, int minutes) {
  final _subTotal = calculateSubTotal(request, offer, minutes);
  final _extraFee = calculateExtraFee(_subTotal, request.participantsCount);
  return _subTotal + _extraFee;
}

double calculateSubTotal(RequestModel request, OfferModel offer, int minutes) {
  double _subTotal = 0;
  if (request.contactForm == CONTACT_CALL) {
    _subTotal = offer.price.cost / offer.price.totalMinutes * minutes;
  } else if (request.contactForm == CONTACT_MEET) {
    _subTotal = offer.price.cost / offer.price.totalMinutes * minutes;
  }
  return _subTotal;
}

double calculateExtraFee(double subTotal, int numParticipants) {
  if (numParticipants >= 2) {
    return .5 * subTotal;
  }
  return 0;
}
