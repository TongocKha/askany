import 'package:askany/src/configs/lang/localization.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

import 'package:askany/src/models/request_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';

class ServiceCancelBottomButton extends StatelessWidget {
  final int tabNumber;
  final RequestModel service;

  const ServiceCancelBottomButton({
    Key? key,
    required this.tabNumber,
    required this.service,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: backgroundDetails,
      padding:
          EdgeInsets.only(left: 16.sp, right: 16.sp, top: 20.sp, bottom: 30.sp),
      child: TouchableOpacity(
        onTap: () {
          AppNavigator.push(
            Routes.SERVICE_CANCEL,
            arguments: {
              'service': service,
              'tabNumber': tabNumber,
            },
          );
        },
        child: Container(
          height: 40.sp,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(
              Radius.circular(8.sp),
            ),
            border: Border.all(
              color: colorGray2,
            ),
          ),
          child: Center(
            child: Text(
              Strings.cancelService.i18n,
              style: text13w700cGray1,
            ),
          ),
        ),
      ),
    );
  }
}
