import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:askany/src/helpers/date_time_helper.dart';
import 'package:askany/src/models/request_model.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/common/widgets/custom_image/network_image/cached_image.dart';
import 'package:askany/src/ui/style/style.dart';

class ServiceFeedbackView extends StatefulWidget {
  final RequestModel service;
  const ServiceFeedbackView({
    Key? key,
    required this.service,
  }) : super(key: key);

  @override
  _ServiceFeedbackViewState createState() => _ServiceFeedbackViewState();
}

class _ServiceFeedbackViewState extends State<ServiceFeedbackView> {
  late RequestModel service;
  @override
  void initState() {
    super.initState();
    service = widget.service;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.topLeft,
      padding: EdgeInsets.symmetric(
        horizontal: 16.sp,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(
            Strings.rating.i18n,
            style: text14w700cBlack1,
          ),
          Container(
            child: widget.service.rating == null
                ? Padding(
                    padding: EdgeInsets.symmetric(vertical: 10.sp),
                    child: Text(
                      Strings.thereNoReviewYet.i18n,
                      style: text12w400cBlack2,
                    ),
                  )
                : Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            ...List.generate(5, (index) {
                              return Container(
                                  height: 20.sp,
                                  width: 20.sp,
                                  child: Padding(
                                    padding: EdgeInsets.only(right: 4.sp),
                                    child: index < widget.service.rating!.stars
                                        ? Image.asset(
                                            iconStar,
                                            width: 18.sp,
                                            height: 18.sp,
                                          )
                                        : Image.asset(
                                            iconStarGrey,
                                            width: 18.sp,
                                            height: 18.sp,
                                          ),
                                  ));
                            }),
                            SizedBox(
                              width: 22.sp,
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(vertical: 15.sp),
                              child: Text(
                                getDatetimeString(
                                  widget.service.rating!.createdAt.toLocal(),
                                ),
                                style: text10w400cGray2,
                              ),
                            )
                          ],
                        ),
                        Visibility(
                          visible: widget.service.rating != null &&
                              widget.service.rating!.content.isNotEmpty,
                          child: Padding(
                            padding: EdgeInsets.only(right: 20.sp),
                            child: Text(
                              widget.service.rating!.content,
                              style: text12w400cBlack2,
                            ),
                          ),
                        ),
                        Container(
                          child: widget.service.rating != null &&
                                  widget.service.rating!.reply == null
                              ? Visibility(
                                  visible: UserLocal().getIsExpert(),
                                  child: Container(
                                    margin: EdgeInsets.only(top: 10.sp),
                                    padding: EdgeInsets.only(right: 50.w),
                                    child: ButtonPrimary(
                                      onPressed:
                                          // _postFeedback(),

                                          () {
                                        AppNavigator.push(
                                          Routes.REPLY_FEEDBACK,
                                          arguments: {
                                            'service': widget.service
                                          },
                                        );
                                      },
                                      text:
                                          Strings.replyFeedbackAppbartext.i18n,
                                    ),
                                  ),
                                )
                              : Container(
                                  margin: EdgeInsets.symmetric(vertical: 10.sp),
                                  child: Row(
                                    children: [
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              CustomNetworkImage(
                                                height: 32.sp,
                                                width: 32.sp,
                                                urlToImage: widget
                                                    .service
                                                    .authorExpert
                                                    ?.avatar
                                                    ?.urlToImage,
                                              ),
                                              SizedBox(
                                                width: 8.sp,
                                              ),
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Row(
                                                    children: [
                                                      Text(
                                                        widget
                                                            .service
                                                            .authorExpert!
                                                            .fullname,
                                                        style:
                                                            text13w600cBlack2,
                                                      ),
                                                      SizedBox(
                                                        width: 8.sp,
                                                      ),
                                                      Text(
                                                        getDatetimeString(widget
                                                            .service
                                                            .rating!
                                                            .reply!
                                                            .createdAt
                                                            .toLocal()),
                                                        style: text10w400cGray2,
                                                      )
                                                    ],
                                                  ),
                                                  Text(
                                                    widget.service.rating?.reply
                                                            ?.content ??
                                                        '',
                                                    style: text12w400cBlack2,
                                                  )
                                                ],
                                              )
                                            ],
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                        ),
                      ],
                    ),
                  ),
          ),
        ],
      ),
    );
  }
}
