import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';

import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/service_managament/service_management_bloc.dart';
import 'package:askany/src/models/request_model.dart';
import 'package:askany/src/models/slide_mode.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/my_service/widgets/service_cancel_button.dart';
import 'package:askany/src/ui/category/widgets/two_buttons_bottom_bar.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_confirm_cancel.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ServiceDetailsBottomButton extends StatelessWidget {
  final int offerStatus;
  final int tabNumber;
  final RequestModel service;
  const ServiceDetailsBottomButton({
    Key? key,
    required this.offerStatus,
    required this.tabNumber,
    required this.service,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return UserLocal().getIsExpert()
        ? _buildForExPert(context)
        : _buildForUser(context);
  }

  Widget _buildForExPert(BuildContext context) {
    switch (offerStatus) {
      case WAIT_FOR_CONFIRM:
        return TwoButonsBottomBar(
          leftButtonContent: Strings.cancelService.i18n,
          rightButtonContent: Strings.confirm.i18n,
          service: service,
          leftButtonCallback: () {
            AppNavigator.push(
              Routes.SERVICE_CANCEL,
              arguments: {
                'service': service,
                'tabNumber': tabNumber,
              },
            );
          },
          rightButtonCallback: () {
            dialogAnimationWrapper(
              slideFrom: SlideMode.bot,
              child: DialogConfirmCancel(
                bodyBefore:
                    '${Strings.youConfirmAdvisingTheServiceP1.i18n} ${service.title} ${Strings.youConfirmAdvisingTheServiceP2.i18n}',
                cancelText: Strings.cancel.i18n,
                onConfirmed: () {
                  AppBloc.serviceManagamentBloc.add(
                    ExpertConfirmOfferEvent(
                      offerId: service.myOfferId.toString(),
                      requestId: service.id!,
                    ),
                  );
                },
              ),
            );
          },
        );

      case CONFIRMED:
        return Container(
          child: tabNumber == ADVISED
              ? SizedBox()
              : ServiceCancelBottomButton(
                  tabNumber: tabNumber,
                  service: service,
                ),
        );
      case ADVISED:
        return ButtonPrimary(onPressed: () {}, text: Strings.completed.i18n);

      default:
        return Container();
    }
  }

  Widget _buildForUser(BuildContext context) {
    final bool _hasFeedback = service.rating != null;

    switch (offerStatus) {
      case WAIT_FOR_CONFIRM:
        return ServiceCancelBottomButton(
          service: service,
          tabNumber: tabNumber,
        );
      case WAIT_FOR_PAYMENT:
        return TwoButonsBottomBar(
          leftButtonContent: Strings.cancelService.i18n,
          rightButtonContent: Strings.payment.i18n,
          service: service,
          leftButtonCallback: () {
            AppNavigator.push(
              Routes.SERVICE_CANCEL,
              arguments: {
                'service': service,
                'tabNumber': tabNumber,
              },
            );
          },
          rightButtonCallback: () {
            AppNavigator.push(
              Routes.CONTINUES_PAYMENT,
              arguments: {
                'request': service,
              },
            );
          },
        );
      case CONFIRMED:
        return Container(
          child: service.endTime!.isBefore(DateTime.now())
              ? TwoButonsBottomBar(
                  leftButtonContent: Strings.complain.i18n,
                  leftButtonCallback: () {
                    AppNavigator.push(
                      Routes.SERVICE_COMPLAINT,
                      arguments: {'service': service},
                    );
                  },
                  rightButtonContent: Strings.completed.i18n,
                  rightButtonCallback: () {
                    dialogAnimationWrapper(
                      borderRadius: 10.sp,
                      slideFrom: SlideMode.bot,
                      child: DialogConfirmCancel(
                        bodyBefore: Strings.youComfirmCompletion.i18n,
                        confirmText: Strings.confirm.i18n,
                        cancelText: Strings.cancel.i18n,
                        onConfirmed: () {
                          AppNavigator.pop();
                          showDialogLoading();
                          AppBloc.serviceManagamentBloc.add(
                            UserCompleteOfferEvent(service: service),
                          );
                        },
                      ),
                    );
                  },
                )
              : ServiceCancelBottomButton(
                  service: service,
                  tabNumber: tabNumber,
                ),
        );

      case ADVISED:
        return TwoButonsBottomBar(
          leftButtonContent: Strings.complain.i18n,
          leftButtonCallback: () {
            AppNavigator.push(
              Routes.SERVICE_COMPLAINT,
              arguments: {'service': service},
            );
          },
          rightButtonContent: Strings.completed.i18n,
          rightButtonCallback: () {
            dialogAnimationWrapper(
              borderRadius: 10.sp,
              slideFrom: SlideMode.bot,
              child: DialogConfirmCancel(
                bodyBefore: Strings.youComfirmCompletion.i18n,
                confirmText: Strings.confirm.i18n,
                cancelText: Strings.cancel.i18n,
                onConfirmed: () {
                  AppBloc.serviceManagamentBloc.add(
                    UserCompleteOfferEvent(service: service),
                  );
                },
              ),
            );
          },
        );
      case COMPLETED:
        return Container(
          child: _hasFeedback
              ? SizedBox()
              : Column(
                  children: [
                    dividerColorGrey2,
                    Padding(
                      padding: EdgeInsets.only(
                          top: 16.sp, bottom: 20.sp, left: 16.sp, right: 16.sp),
                      child: ButtonPrimary(
                        onPressed: () {
                          AppNavigator.push(
                            Routes.SERVICE_FEEDBACK,
                            arguments: {'service': service},
                          );
                        },
                        text: Strings.rateNow.i18n,
                      ),
                    ),
                  ],
                ),
        );
      // case REPORTED:
      //   return Container(
      //     child: _hasFeedback
      //         ? SizedBox()
      //         : Column(
      //             children: [
      //               dividerColorGrey2,
      //               Padding(
      //                 padding: EdgeInsets.only(
      //                     top: 16.sp, bottom: 20.sp, left: 16.sp, right: 16.sp),
      //                 child: ButtonPrimary(
      //                   onPressed: () {
      //                     AppNavigator.push(
      //                       Routes.SERVICE_FEEDBACK,
      //                       arguments: {'service': service},
      //                     );
      //                   },
      //                   text: Strings.rateNow.i18n,
      //                 ),
      //               ),
      //             ],
      //           ),
      //   );

      default:
        return Container();
    }
  }
}
