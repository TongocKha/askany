import 'package:askany/src/bloc/service_managament/service_management_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/models/cancel_service_reason_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';

class CancelBottomSheet extends StatefulWidget {
  const CancelBottomSheet({
    Key? key,
    required this.handlePressed,
    required this.reason,
  }) : super(key: key);
  final Function(ReasonModel) handlePressed;
  final ReasonModel? reason;

  @override
  State<CancelBottomSheet> createState() => _CancelBottomSheetState();
}

class _CancelBottomSheetState extends State<CancelBottomSheet> {
  late ReasonModel? _reason;
  @override
  void initState() {
    super.initState();
    _reason = widget.reason;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        height: 50.h,
        child: Column(
          children: [
            SizedBox(
              height: 6.sp,
            ),
            Container(
              width: 60.sp,
              height: 3.sp,
              decoration:
                  BoxDecoration(color: colorGray4, borderRadius: BorderRadius.circular(1.5.sp)),
            ),
            SizedBox(
              height: 21.sp,
            ),
            Container(
              padding: EdgeInsets.only(left: 24.sp),
              alignment: Alignment.centerLeft,
              height: 24.sp,
              child: Text(
                Strings.cancelReason.i18n,
                style: text15w700cBlack2,
              ),
            ),
            SizedBox(height: 12.sp),
            Divider(
              color: colorGray2,
              thickness: 0.5.sp,
            ),
            SizedBox(height: 12.sp),
            Expanded(
              child: BlocBuilder<ServiceManagamentBloc, ServiceManagamentState>(
                builder: (context, state) {
                  List<ReasonModel> reasons = state.props[1] as List<ReasonModel>;
                  return Padding(
                    padding: EdgeInsets.only(right: 4.sp),
                    child: RawScrollbar(
                      isAlwaysShown: true,
                      thumbColor: colorGreen4,
                      radius: Radius.circular(30.sp),
                      thickness: 4.sp,
                      child: ListView.builder(
                        shrinkWrap: true,
                        padding: EdgeInsets.zero,
                        itemCount: reasons.length,
                        itemBuilder: (context, index) {
                          return Container(
                            color: reasons[index] == _reason ? colorChosenCard : Colors.transparent,
                            padding: EdgeInsets.symmetric(vertical: 12.sp),
                            child: TouchableOpacity(
                              onTap: () {
                                if (reasons[index] != _reason) {
                                  widget.handlePressed(reasons[index]);
                                  setState(() {
                                    _reason = reasons[index];
                                  });
                                }
                              },
                              child: Row(
                                children: [
                                  SizedBox(
                                    width: 24.sp,
                                  ),
                                  Expanded(
                                    child: Container(
                                      child: Text(
                                        reasons[index].name,
                                        style: reasons[index] == _reason
                                            ? text13w400cGreen2
                                            : text13w400cBlack2,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    width: 64.sp,
                                    child: Center(
                                      child: reasons[index] == _reason
                                          ? Image.asset(
                                              iconCheckGreen,
                                              width: 14.58.sp,
                                              height: 12.5.sp,
                                            )
                                          : null,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                  );
                },
              ),
            ),
            SizedBox(height: 12.sp),
          ],
        ),
      ),
    );
  }
}
