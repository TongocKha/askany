import 'package:askany/src/bloc/timeline/timeline_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/models/expert_timeline_model.dart';
import 'package:askany/src/models/slide_mode.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/custom_scrollbar/custom_scrollbar.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_date_picker.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_with_text_and_pop_button.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ContinuesPaymentStepTwo extends StatefulWidget {
  const ContinuesPaymentStepTwo({
    Key? key,
    required this.onTimeSelected,
    required this.onDateSelected,
    this.initStartTime,
    this.initEndTime,
    this.totalMinus = 0,
    required this.selectedDate,
  }) : super(key: key);

  final Function(DateTime, DateTime) onTimeSelected;
  final Function(DateTime) onDateSelected;
  final int totalMinus;
  final DateTime? initStartTime;
  final DateTime? initEndTime;
  final DateTime selectedDate;
  @override
  _ContinuesPaymentStepTwoState createState() =>
      _ContinuesPaymentStepTwoState();
}

class _ContinuesPaymentStepTwoState extends State<ContinuesPaymentStepTwo>
    with AutomaticKeepAliveClientMixin {
  final ScrollController _scrollController = ScrollController();
  DateTime _startDateTime = DateTime.now();
  DateTime? _endDateTime;
  bool _isMorning = true;

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    if (widget.initEndTime != null) {
      _startDateTime = DateTime(
        DateTime.now().year,
        DateTime.now().month,
        DateTime.now().day,
        widget.initStartTime?.hour ?? 0,
        widget.initStartTime?.minute ?? 0,
      );
      _endDateTime = _calculateEndDateTime(_startDateTime);
    }
    _scrollController.addListener(() {
      final bool _newIsMorning = _scrollController.offset <
          _scrollController.position.maxScrollExtent / 2;
      if (_isMorning != _newIsMorning) {
        setState(() {
          _isMorning = _newIsMorning;
        });
      }
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: BlocBuilder<TimelineBloc, TimelineState>(
        builder: (context, state) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.only(left: 16.sp, top: 22.sp, right: 16.sp),
                child: _buildDatePicker(
                  state is! TimelineInitial
                      ? (state.props.first as ExpertTimelineModel)
                          .dateAvailableInMonth
                      : [],
                ),
              ),
              Container(
                height: 290.sp,
                padding:
                    EdgeInsets.only(left: 16.sp, top: 10.sp, bottom: 24.sp),
                child: state is TimelineInitial
                    ? SizedBox()
                    : _buildTimePicker(
                        state.props.first as ExpertTimelineModel),
              ),
            ],
          );
        },
      ),
    );
  }

  Widget _buildTimePicker(ExpertTimelineModel expertTimelineModel) {
    final List<List<DateTime>> listBusy = expertTimelineModel.getBusyTime();
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        _buildTimePickerTitle(),
        Expanded(
          child: SizedBox(
            child: ScrollConfiguration(
              behavior: _MyBehavior(),
              child: CustomScrollbar.rect(
                controller: _scrollController,
                scrollThumbLength: 50.w - 16.sp,
                scrollThumbThickness: 3.sp,
                scrollThumbColor: colorGreen3,
                backgroundColor: colorGrey3,
                scrollbarAlign: ScrollbarAlign.top,
                twoHeadSpacing: EdgeInsets.only(right: 16.sp),
                child: GridView.builder(
                  physics: BouncingScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 4,
                    mainAxisSpacing: 8.sp,
                    crossAxisSpacing: 8.sp,
                    mainAxisExtent: 112.sp,
                    childAspectRatio: 2.9,
                  ),
                  padding: EdgeInsets.only(top: 18.sp),
                  controller: _scrollController,
                  scrollDirection: Axis.horizontal,
                  itemCount: listTime.length,
                  itemBuilder: (context, index) {
                    TimeOfDay _thisTimeValue =
                        _convertStringToTime(listTime[index]);
                    DateTime _thisDateTimeValue = _addTimeToDate(
                      _convertStringToTime(listTime[index]),
                      _startDateTime,
                    );

                    bool _isBusy = !expertTimelineModel.dateAvailableInMonth
                            .contains(DateTime(
                      widget.initStartTime?.year ?? DateTime.now().year,
                      widget.initStartTime?.month ?? DateTime.now().month,
                      widget.initStartTime?.day ?? DateTime.now().day,
                      _startDateTime.hour,
                      _startDateTime.minute,
                    ).day)
                        ? true
                        : _isExpertBusy(
                            time: _thisDateTimeValue,
                            listBusy: listBusy,
                          );

                    return _buildTimePickerButton(
                      _isBusy,
                      _thisDateTimeValue,
                      _thisTimeValue,
                      index,
                      listBusy,
                    );
                  },
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Container _buildTimePickerTitle() {
    return Container(
      padding: EdgeInsets.only(right: 16.sp, bottom: 9.sp),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Flexible(
            flex: 1,
            child: TouchableOpacity(
              onTap: () {
                setState(() {
                  _scrollController.jumpTo(0);
                });
              },
              child: Container(
                alignment: Alignment.center,
                width: 50.w,
                color: Colors.transparent,
                child: Text(
                  'Sáng',
                  style: TextStyle(
                    fontSize: 12.sp,
                    color: _isMorning ? colorFontGreen : colorGray1,
                    fontWeight: _isMorning ? FontWeight.w600 : FontWeight.w400,
                    height: 1.6,
                  ),
                ),
              ),
            ),
          ),
          Flexible(
            flex: 1,
            child: TouchableOpacity(
              onTap: () {
                setState(() {
                  _scrollController.jumpTo(
                    _scrollController.position.maxScrollExtent / 2,
                  );
                });
              },
              child: Container(
                color: Colors.transparent,
                alignment: Alignment.center,
                child: Text(
                  'Chiều',
                  style: TextStyle(
                    fontSize: 12.sp,
                    color: !_isMorning ? colorFontGreen : colorGray1,
                    fontWeight: !_isMorning ? FontWeight.w600 : FontWeight.w400,
                    height: 1.6,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  TouchableOpacity _buildTimePickerButton(
    bool isBusy,
    DateTime thisDateTimeValue,
    TimeOfDay thisTimeValue,
    int index,
    List<List<DateTime>> listBusy,
  ) {
    return TouchableOpacity(
      onTap: () {
        if (!isBusy) {
          if (_isStartTimeValid(
              startTime: thisDateTimeValue, listBusy: listBusy)) {
            widget.onTimeSelected(
                thisDateTimeValue, _calculateEndDateTime(thisDateTimeValue));
            _startDateTime = thisDateTimeValue;
            _endDateTime = _calculateEndDateTime(_startDateTime);
            setState(() {});
          } else {
            dialogAnimationWrapper(
              slideFrom: SlideMode.bot,
              child: DialogWithTextAndPopButton(
                bodyBefore: Strings.invalidTimeErrortext.i18n,
                bodyAlign: TextAlign.center,
                bodyFontSize: 13.sp,
                bodyColor: colorBlack1,
              ),
            );
          }
        }
      },
      child: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: isBusy
              ? colorDisalbeTimeButton
              : _isInTimeRange(
                  thisDateTimeValue,
                  _startDateTime,
                  _endDateTime,
                )
                  ? colorGreen3
                  : colorWhite,
          border: Border.all(
            color: isBusy
                ? colorDisalbeTimeButton
                : _isInTimeRange(
                    thisDateTimeValue,
                    _startDateTime,
                    _endDateTime,
                  )
                    ? colorGreen3
                    : colorGrey3,
            width: .5.sp,
          ),
          borderRadius: BorderRadius.circular(8.sp),
        ),
        child: Text(
          listTime[index],
          style: TextStyle(
            color: isBusy
                ? colorGray2
                : _isInTimeRange(
                    thisDateTimeValue,
                    _startDateTime,
                    _endDateTime,
                  )
                    ? colorWhiteCard
                    : colorBlack2,
            fontSize: 12.sp,
            fontWeight: FontWeight.w400,
          ),
        ),
      ),
    );
  }

  Widget _buildDatePicker(List<int> dateAvailableInMonth) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Text(
          '${Strings.chooseAdviseDate.i18n} (*)',
          style: TextStyle(
            fontSize: 13.sp,
            color: colorFontGreen,
            height: 1.53,
            fontWeight: FontWeight.w600,
          ),
        ),
        SizedBox(height: 15.sp),
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.sp),
            border: Border.all(color: colorGray2, width: .5.sp),
          ),
          child: DialogDatePicker(
            dateAvailableInMonth: dateAvailableInMonth,
            selectedDate: widget.selectedDate,
            onDateSelected: (date) {
              setState(() {
                _startDateTime = DateTime.now();
                _endDateTime = null;
              });
              widget.onDateSelected(date);
            },
          ),
        ),
        SizedBox(height: 28.sp),
        dividerColorGrey2,
        SizedBox(height: 28.sp),
        Text(
          '${Strings.chooseAdviceTime.i18n} (*)',
          style: TextStyle(
            fontSize: 14.sp,
            color: colorFontGreen,
            fontWeight: FontWeight.w600,
            height: 1.53,
          ),
        ),
      ],
    );
  }

  bool _isStartTimeValid(
      {required DateTime startTime, required List<List<DateTime>> listBusy}) {
    final DateTime _endTime = _calculateEndDateTime(startTime);
    DateTime _time = startTime;
    while (_time.isBefore(_endTime) || _time.isAtSameMomentAs(_endTime)) {
      if (_isExpertBusy(time: _time, listBusy: listBusy)) {
        return false;
      }
      _time = _time.add(Duration(minutes: 15));
    }
    return true;
  }

  // List<List<DateTime>> _concatListTimeBusy() {
  //   final _newListBusy = [...listBusyTime];
  //   for (int i = 0; i < listBusyTime.length - 1; i++) {
  //     if (listBusyTime[i + 1][0].difference(listBusyTime[i][1]).inMinutes < widget.totalMinus) {
  //       _newListBusy.insert(i + 1, [listBusyTime[i][1], listBusyTime[i + 1][0]]);
  //     }
  //   }
  //   return _newListBusy;
  // }

  bool _isExpertBusy(
      {required DateTime time, required List<List<DateTime>> listBusy}) {
    for (final timeRange in listBusy) {
      if (_isInTimeRange(time, timeRange[0], timeRange[1])) {
        return true;
      }
    }
    return false;
  }

  bool _isInTimeRange(DateTime time, DateTime timeStart, DateTime? timeEnd) {
    if (timeEnd == null) return false;
    return (time.isAfter(timeStart) || time.isAtSameMomentAs(timeStart)) &&
        (time.isBefore(timeEnd) || time.isAtSameMomentAs(timeEnd));
  }

  DateTime _calculateEndDateTime(DateTime startDateTime) {
    final TimeOfDay _timeAdded = TimeOfDay(
      hour: startDateTime.hour,
      minute: startDateTime.minute + widget.totalMinus,
    );
    final DateTime _endDateTime = _addTimeToDate(_timeAdded, startDateTime);
    return _endDateTime;
  }

  TimeOfDay _convertStringToTime(String timeString) {
    return TimeOfDay(
      hour: int.parse(timeString.split(':')[0]),
      minute: int.parse(timeString.split(':')[1]),
    );
  }

  DateTime _addTimeToDate(TimeOfDay time, DateTime date) {
    return DateTime(date.year, date.month, date.day, time.hour, time.minute);
  }
}

class _MyBehavior extends ScrollBehavior {}
