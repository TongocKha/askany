import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_form.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_form_request.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ContinuesPaymentStepOne extends StatelessWidget {
  ContinuesPaymentStepOne({
    Key? key,
    required this.formKey,
    required this.authorNameController,
    required this.authorPhoneController,
    required this.noteController,
  }) : super(key: key);

  final Key formKey;
  final TextEditingController authorNameController;
  final TextEditingController authorPhoneController;
  final TextEditingController noteController;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Container(
        padding: EdgeInsets.only(
          left: 16.sp,
          top: 22.sp,
          right: 16.sp,
          bottom: 30.sp,
        ),
        child: Form(
          key: formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                '${Strings.yourContactInformation.i18n} (*)',
                style: TextStyle(
                  fontSize: 14.sp,
                  color: colorFontGreen,
                  height: 1.53,
                  fontWeight: FontWeight.w600,
                ),
              ),
              SizedBox(height: 15.sp),
              Text(
                Strings.fullName.i18n,
                style: TextStyle(
                  fontSize: 13.sp,
                  color: colorBlack2,
                  height: 1.5,
                  fontWeight: FontWeight.w600,
                ),
              ),
              TextFieldForm(
                submitForm: (val) {},
                controller: authorNameController,
                validatorForm: (val) => val == null
                    ? Strings.nameCannotBeLeftBlankErrortext.i18n
                    : null,
              ),
              SizedBox(height: 18.sp),
              Text(
                Strings.phoneNumberTitle.i18n,
                style: TextStyle(
                  fontSize: 13.sp,
                  color: colorBlack2,
                  height: 1.5,
                  fontWeight: FontWeight.w600,
                ),
              ),
              TextFieldForm(
                textInputType: TextInputType.number,
                controller: authorPhoneController,
                validatorForm: (value) =>
                    value!.trim().length > 11 || value.trim().length < 9
                        ? Strings.phoneNumberCannotBeLeftBlankErrortext.i18n
                        : null,
              ),
              SizedBox(height: 18.sp),
              Text(
                Strings.note.i18n,
                style: TextStyle(
                  fontSize: 13.sp,
                  color: colorBlack2,
                  height: 1.5,
                  fontWeight: FontWeight.w600,
                ),
              ),
              TextFieldFormRequest(
                controller: noteController,
                hintText: Strings.noteHintext.i18n,
                maxLines: 6,
                validatorForm: (val) {},
              ),
            ],
          ),
        ),
      ),
    );
  }
}
