import 'package:flutter/material.dart';

const colorBackgroundRemoteCamera = Color(0xFF232635);
const colorBackgroundVideoCall = Color(0xFF1C1F2E);
const colorBackgroundBottomSheet = Color(0xff393D4F);
const colorBackgroundGrey = Color(0xffE5E5E5);
const colorBackgroundGrey2 = Color(0xff6E6D7A);

const colorWhiteShareScreen = Color(0xFFFDFDFD);

// Assets
const String iconEnableShareScreen = 'assets/images/ic_enable_sharescreen.png';
const String iconShareScreen = 'assets/images/ic_share_screen.png';
const String iconEnableRecord = 'assets/images/ic_enable_record.png';
const String iconDeniedRecord = 'assets/images/ic_denied_record.png';
const String iconEnableVideo = 'assets/images/ic_enable_video.png';
const String iconDeniedVideo = 'assets/images/ic_denied_video.png';
const String iconDeniedFullScreen = 'assets/images/ic_denied_full_screen.png';
const String iconEndCall = 'assets/images/ic_end_call.png';
const String iconVideoCamera = 'assets/images/ic_video_camera.png';
const String iconMenuShareScreen = 'assets/icons/ic_more_horizontal.png';
const String iconMenuVertical = 'assets/icons/ic_more_vertical.png';
const String iconRaiseHand = 'assets/icons/ic_raise_hand.png';
const String iconRaiseHand2 = 'assets/icons/ic_raise_hand_yelow.png';
const String iconArrowBack = 'assets/icons/ic_back.png';
const String iconFlipCamera = 'assets/icons/ic_flip_camera.png';
const String iconFullScreen = 'assets/icons/ic_full_screen.png';
const String iconBubbleChat = 'assets/images/ic_message.png';
const String iconPersonOutline = 'assets/images/ic_account.png';
const String iconMenuPopup = 'assets/icons/ic_menu_popup.png';

// const String iconShareScreen = 'assets/images/ic_share_screen.png';

// const String iconEnableShareScreen = 'assets/images/ic_share_screen.png';
// const String iconEnableShareScreen = 'assets/images/ic_share_screen.png';
// const String iconEnableShareScreen = 'assets/images/ic_share_screen.png';
