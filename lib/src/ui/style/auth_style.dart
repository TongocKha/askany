import 'package:flutter/material.dart';

const Color colorBackgroundExperience = Color(0xFFF0F7F4);
const Color colorDividerBottomSheet = Color(0xFFC4C4C4);

// Icons
const String iconStep1 = 'assets/icons/ic_register_step1.png';
const String iconStep2 = 'assets/icons/ic_register_step2.png';
const String iconStep3 = 'assets/icons/ic_register_step3.png';
const String iconStep4 = 'assets/icons/ic_register_step4.png';
const String iconMoreOption = 'assets/icons/ic_more_option.png';
