import 'package:flutter/material.dart';

const Color colorIconInactive = Color(0xFF6E6D7A);
const Color colorStar = Color(0xFFF5B353);
const Color colorGray4 = Color(0xFFC4C4C4);

// Assets
const String iconHomeSelected = 'assets/icons/ic_home_selected.png';
const String iconRequestSelected = 'assets/icons/ic_request_selected.png';
const String iconCategorySelected = 'assets/icons/ic_category_selected.png';
const String iconNotificationSelected = 'assets/icons/ic_notification_selected.png';
const String iconAccountSelected = 'assets/icons/ic_account_selected.png';
const String iconSearchHome = 'assets/icons/ic_search.png';
const String iconNextHome = 'assets/icons/ic_next.png';
const String iconStudyAbroad = 'assets/icons/ic_study_abroad.png';
const String iconDigitalMarketing = 'assets/icons/ic_digital_marketing.png';
const String iconTechnology = 'assets/icons/ic_technology.png';
const String iconPlay = 'assets/icons/ic_play.png';
const String iconPlayVideo = 'assets/icons/ic_play_video.png';
const String iconClear = 'assets/icons/ic_clear.png';
const String iconFilter = 'assets/icons/ic_filter.png';
const String iconFire = 'assets/icons/ic_fire.png';
const String iconScrollBottomSheet = 'assets/icons/ic_scroll_bottom_sheet.png';
const String iconCheckConfirm = 'assets/icons/ic_check_confirm.png';

const String iconHome = 'assets/icons/ic_home.png';
const String iconRequest = 'assets/icons/ic_request.png';
const String iconCategory = 'assets/icons/ic_category.png';
const String iconNotification = 'assets/icons/ic_notification.png';
const String iconAccount = 'assets/icons/ic_account.png';
const String imageRegisterSuccess = 'assets/images/register_success.png';
const String imageAdmin = 'assets/images/img_admin.png';
const String imageDefault = 'assets/images/img_default.png';
const String imageSearchDefault = 'assets/images/img_search_default.png';
const String imageLoading = 'assets/images/img_loading.gif';
