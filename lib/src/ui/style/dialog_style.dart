import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

const colorDialogBackGround = Color(0xFFFFFFFF);
const colorDialogError = Color(0xFFE89F2F);

const colorWhite = Color(0xFFFFFFFF);
const colorDisalbeTimeButton = Color(0xFFF5F5F5);

const String iconExclamationMark = 'assets/icons/ic_exclamation_mark.png';
const String iconCheckSuccess = 'assets/icons/ic_check_success.png';
const String iconCheckFaild = 'assets/icons/ic_check_faild.png';

final dividerDialog = Container(
  height: .5.sp,
  color: Color(0xFFE3E3E3),
);

final dividerColorGrey2 = Container(
  height: .5.sp,
  color: Color(0xFFACACB9),
);
