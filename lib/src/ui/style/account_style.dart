import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

const Color colorInactiveTextField = Color(0xFFF5F5F5);
const Color colorBorderTextField = Color(0xFFC5D0CF);

// Assets
const String iconCamera = 'assets/icons/ic_camera.png';
const String iconPhoto = 'assets/icons/ic_photo.png';
const String iconPhoto2 = 'assets/icons/ic_photo2.png';
const String iconPenEdit = 'assets/icons/ic_pen_edit.png';
const String iconPenEdit2 = 'assets/icons/ic_pen_edit2.png';
const String iconButtonPen = 'assets/images/ic_button_pencil.png';
const String iconEye = 'assets/icons/ic_eye.png';
const String iconEyeOff = 'assets/icons/ic_eye_splash.png';
const String iconCheckInBox = 'assets/icons/ic_check_in_box.png';
const String iconRevertImage = 'assets/icons/ic_revert_image.png';
const String iconUserSkill = 'assets/icons/ic_user_skill.png';
const String iconRemove = 'assets/icons/ic_remove.png';
const String iconBagCompany = 'assets/icons/ic_bagcompany.png';
const String iconMyCalendar = 'assets/icons/ic_my_calendar.png';
const String iconMyWallet = 'assets/icons/ic_my_wallet.png';
const String iconInfo = 'assets/icons/ic_info.png';
const String iconChangePassword = 'assets/icons/ic_change_password.png';
const String iconVerifyAccount = 'assets/icons/ic_acc_verified.png';
const String iconLanguages = 'assets/icons/ic_language.png';
const String iconWaittingConfirm = 'assets/icons/ic_waitting_confirm.png';
const String iconConfirm = 'assets/icons/ic_confirm.png';
const String iconAdvise = 'assets/icons/ic_advise.png';
const String iconServiceFinished = 'assets/icons/ic_service_finished.png';
const String iconMessageHeader = 'assets/icons/ic_message_header.png';
const String iconMessageHeaderBlackLine = 'assets/icons/ic_message_header_blackline.png';
const String iconPayIn = 'assets/icons/ic_pay_in.png';
const String iconPayOut = 'assets/icons/ic_pay_out.png';
const String iconSaveCard = 'assets/icons/ic_save_card.png';

const String iconAddCreditCard = 'assets/icons/ic_add_credit_card.png';
const String iconBackArrowGreen = 'assets/icons/ic_back_arrow_green.png';
const String iconVisaLogo = 'assets/icons/ic_visa_logo.png';
const String iconMasterCardLogo = 'assets/icons/ic_mastercard_logo.png';
const String iconTechcombankLogo = 'assets/icons/ic_techcombank_logo.png';

const String avatarDefault = 'assets/icons/avatar_default.png';
const String imagePhoneVerified = 'assets/icons/ic_verified_phone.png';
const String imageEmailVerified = 'assets/icons/ic_verified_email.png';
const String imageIntroduction = 'assets/images/img_introduct_account.png';
const String imageSkillEmpty = 'assets/images/img_skill_empty.png';
const String imageCategoryEmpty = 'assets/images/img_empty_category.png';

const String imageNotificationEmpty = 'assets/images/img_notification_empty.png';
const String iconClearCircle = 'assets/icons/ic_clear_circle.png';
const String iconCheckSquare = 'assets/icons/ic_check_square.png';
const String iconApple = 'assets/icons/ic_apple.png';
const String iconRedNoti = 'assets/icons/ic_red_noti.png';

// Payment Methods

const String momoLogo = 'assets/icons/momo_logo.png';
const String vnpayLogo = 'assets/icons/vnpay_logo.png';
const String zaloLogo = 'assets/icons/zalo_logo.png';
const String askanyLogo = 'assets/icons/ic_logo.png';
const String googlePayLogo = 'assets/icons/google_pay_logo.png';
const String applePayLogo = 'assets/icons/apple_pay_logo.png';

final TextStyle text13w600cBlack1 = TextStyle(
  fontSize: 13.sp,
  fontWeight: FontWeight.w600,
  color: colorBlack1,
);
final TextStyle text11w600cGreen = TextStyle(
  fontSize: 11.sp,
  fontWeight: FontWeight.w600,
  color: Color(0xff1C4843),
);
final TextStyle text11w600cWhite = TextStyle(
  fontSize: 11.sp,
  fontWeight: FontWeight.w600,
  color: colorWhite,
);
final TextStyle text12w600cWhite = TextStyle(
  fontSize: 12.sp,
  fontWeight: FontWeight.w600,
  color: colorWhite,
);
final TextStyle text11w600cGreen2 = TextStyle(
  fontSize: 11.sp,
  fontWeight: FontWeight.w600,
  color: colorGreen2,
);

final TextStyle text11w600cRed = TextStyle(
  fontSize: 11.sp,
  fontWeight: FontWeight.w600,
  color: Color(0xffB31D1D),
);
final TextStyle text11w600cBlue = TextStyle(
  fontSize: 11.sp,
  fontWeight: FontWeight.w600,
  color: colorChosen,
);
final TextStyle text11w400cGrey1 = TextStyle(
  fontSize: 11.sp,
  fontWeight: FontWeight.w400,
  color: colorGrey1,
);
final TextStyle text11w400cBlack2 = TextStyle(
  fontSize: 11.sp,
  fontWeight: FontWeight.w400,
  color: colorBlack2,
);
final TextStyle text11w600cBlack2 = TextStyle(
  fontSize: 11.sp,
  fontWeight: FontWeight.w600,
  color: colorBlack2,
);
final TextStyle text12w400cGrey1 = TextStyle(
  fontSize: 12.sp,
  fontWeight: FontWeight.w400,
  color: colorGrey1,
);
final TextStyle text12w400cBlack2 = TextStyle(
  fontSize: 12.sp,
  fontWeight: FontWeight.w400,
  color: colorBlack2,
);
final TextStyle text17w700cBlack2 = TextStyle(
  fontSize: 17.sp,
  fontWeight: FontWeight.w700,
  color: colorBlack2,
);
