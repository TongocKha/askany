import 'package:askany/src/ui/style/calendar_style.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

final Divider dividerChat = Divider(
  color: colorDividerTimeline,
  thickness: 0.3.sp,
  height: 0.3.sp,
);

const colorBackgroundBadges = Color(0xFFF6C8AB);
const colorGrayBorder = Color(0xFFC5C5D0);
const colorBlack4 = Color(0xFFA9AAB8);
const colorBlack3 = Color(0xFF333333);
const colorCaptionSearch = Color(0xFFA3A3A3);
const colorTextChatCard = Color(0xFF53596F);
const colorTimeChat = Color(0xFF9897A0);
const colorBackgroundReceiver = Color(0xFFE2F0E9);
const colorBackgroundSender = Color(0xFFF1F1F1);
const colorTextSender = Color(0xFF53596F);
const colorTextAppointmentCard = Color(0xFFFDFDFD);
const colorFontGreen = Color(0xFF1C4843);
const colorDelete = Color(0xFFFE5252);
const colorMoreChat = Color(0xFF6E6E7A);
const colorBackgroundCall = Color(0xFFE9F4EE);
const colorBackgroundReceiverInCall = Color(0xFF393D4F);
const colorHintTextInCall = Color(0xFF9897A0);

// Assets
const String iconPhone = 'assets/icons/ic_phone.png';
const String iconVideoCall = 'assets/icons/ic_video_call.png';
const String iconMoreChat = 'assets/icons/ic_more_conversation.png';
const String iconSendMessage = 'assets/icons/ic_send_message.png';
const String iconPhoneMessage = 'assets/icons/ic_phone_message.png';
const String imageChatEmpty = 'assets/images/img_chat_empty.png';
const String iconSearch = 'assets/icons/ic_search.png';
const String iconPhoneRounded = 'assets/icons/ic_phone_rounded.png';
const String iconMoreConversation = 'assets/icons/ic_more_slidable.png';
const String iconDelete = 'assets/icons/ic_delete.png';
const String iconCopy = 'assets/icons/ic_copy.png';
const String iconPhotoChat = 'assets/icons/ic_photo_chat.png';
const String imgTyping = 'assets/images/img_typing.gif';

const String imgFilterImage = 'assets/images/img_filter_image.png';
const String imgCrop = 'assets/images/img_crop.png';
const String imgPaint = 'assets/images/img_paint.png';
const String imgRotate = 'assets/images/img_rotate.png';
const String imgText = 'assets/images/img_text.png';
const String imgUndo = 'assets/images/img_undo.png';
const String imgPencil = 'assets/images/img_pencil.png';

Map<String, double> ratio = {
  "1 : 1": CropAspectRatios.ratio1_1,
  '3 : 4': CropAspectRatios.ratio3_4,
  '4 : 3': CropAspectRatios.ratio4_3,
  '9 : 16': CropAspectRatios.ratio9_16,
  '16 : 9': CropAspectRatios.ratio16_9,
};
const String iconDownloads = 'assets/icons/ic_downloads_ios.png';

final List<List<double>> filters = [
  Filter1,
  Filter2,
  Filter3,
  Filter4,
  Filter5,
  Filter6,
  Filter7,
  Filter8,
  Filter9,
];

const Filter1 = [
  0.39,
  0.769,
  0.189,
  0.0,
  0.0,
  0.349,
  0.686,
  0.168,
  0.0,
  0.0,
  0.272,
  0.534,
  0.131,
  0.0,
  0.0,
  0.0,
  0.0,
  0.0,
  1.0,
  0.0
];

const Filter2 = [
  0.2126,
  0.7152,
  0.0722,
  0.0,
  0.0,
  0.2126,
  0.7152,
  0.0722,
  0.0,
  0.0,
  0.2126,
  0.7152,
  0.0722,
  0.0,
  0.0,
  0.0,
  0.0,
  0.0,
  1.0,
  0.0
];

const Filter3 = [
  0.9,
  0.5,
  0.1,
  0.0,
  0.0,
  0.3,
  0.8,
  0.1,
  0.0,
  0.0,
  0.2,
  0.3,
  0.5,
  0.0,
  0.0,
  0.0,
  0.0,
  0.0,
  1.0,
  0.0
];

const Filter4 = [
  1.0,
  0.0,
  0.2,
  0.0,
  0.0,
  0.0,
  1.0,
  0.0,
  0.0,
  0.0,
  0.0,
  0.0,
  1.0,
  0.0,
  0.0,
  0.0,
  0.0,
  0.0,
  1.0,
  0.0
];

const Filter5 = [
  1.0,
  0.0,
  0.0,
  0.0,
  0.0,
  0.0,
  1.0,
  0.0,
  0.0,
  0.0,
  0.0,
  0.0,
  1.0,
  0.0,
  0.0,
  0.0,
  0.0,
  0.0,
  1.0,
  0.0,
];

const Filter6 = [
  1.5,
  0.0,
  0.0,
  0.0,
  0.0,
  0.0,
  1.5,
  0.0,
  0.0,
  0.0,
  0.0,
  0.0,
  1.5,
  0.0,
  0.0,
  0.0,
  0.0,
  0.0,
  1.0,
  0.0,
];

const Filter7 = [
  1.0,
  0.0,
  0.0,
  0.0,
  0.0,
  1.0,
  0.0,
  0.0,
  0.0,
  0.0,
  1.0,
  0.0,
  0.0,
  0.0,
  0.0,
  0.0,
  0.0,
  0.0,
  1.0,
  0.0
];

const Filter8 = [
  1.0,
  0.0,
  0.0,
  0.0,
  0.0,
  0.0,
  1.0,
  0.0,
  0.0,
  0.5,
  0.0,
  0.0,
  1.0,
  0.0,
  0.0,
  0.0,
  0.0,
  0.0,
  1.0,
  0.0,
];

const Filter9 = [
  1.0,
  0.75,
  0.0,
  0.0,
  0.0,
  0.0,
  1.0,
  0.75,
  0.0,
  0.0,
  0.75,
  0.0,
  1.0,
  0.0,
  0.0,
  0.0,
  0.0,
  0.0,
  1.0,
  0.0,
];
