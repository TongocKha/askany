import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

final TextStyle walletTitleStyle14 = TextStyle(
  fontSize: 14.sp,
  fontWeight: FontWeight.w700,
  color: colorBlack2,
);
final TextStyle walletTitleStyle13 = TextStyle(
  color: colorBlack2,
  fontSize: 13.sp,
  fontWeight: FontWeight.w600,
);
final TextStyle walletTitleStyleW400 = TextStyle(
  fontSize: 13.sp,
  fontWeight: FontWeight.w400,
  color: headerCalendarColor,
);
final TextStyle walletTitleStyleS13W400 = TextStyle(
  fontSize: 13.sp,
  fontWeight: FontWeight.w400,
  color: colorBlack2,
);

const Color colorProcess = Color(0xFFFF9700);

Color getColorTransaction(int status) {
  switch (status) {
    case -1:
      return colorFinished;
    case 0:
      return colorProcess;
    case 1:
      return colorGreen2;
    case 2:
      return colorProcess;
    default:
      return colorProcess;
  }
}

String getTextTransaction(int status) {
  switch (status) {
    case -1:
      return Strings.failure.i18n;
    case 0:
      return Strings.waitingForProcessing.i18n;
    case 1:
      return Strings.success.i18n;
    case 2:
      return Strings.userReported.i18n;
    default:
      return Strings.waitingForProcessing.i18n;
  }
}

String getTextDesciption(int type) {
  switch (type) {
    case 0:
      return Strings.userPayOutByVNPay.i18n;
    case 1:
      return Strings.userPayOutByAskany.i18n;
    case 2:
      return Strings.adminTransferForExpert.i18n;
    case 3:
      return Strings.adminRefundForUser.i18n;
    case 4:
      return Strings.userPayInByVNPay.i18n;
    case 5:
      return Strings.expertPayInByVNPay.i18n;
    case 6:
      return Strings.userWithDrawToBank.i18n;
    case 7:
      return Strings.expertWithDrawToBank;
    default:
      return Strings.userPayOutByVNPay.i18n;
  }
}

const TOTHECARD = 0;
const TOTHEACCOUNT = 1;
const String imagesACBBankWallet = 'assets/images/img_acbbank.png';
const String imagesVPBankWallet = 'assets/images/img_vpbank.png';
const String imagesVisaWallet = 'assets/images/img_visa.png';
const String imagesMasterCardWallet = 'assets/images/img_mastercard.png';
const String imagesSuccessfulRecharge = 'assets/icons/ic_verified_success.png';

List<String> cards = ['Thẻ Credit', 'Thẻ ATM nội địa'];

final banks = [
  {'image': 'assets/images/bank_vietcombank.png', 'bank_code': 'Vietcombank'},
  {'image': 'assets/images/bank_viettinbank.png', 'bank_code': 'Vietinbank'},
  {'image': 'assets/icons/ic_techcombank_logo.png', 'bank_code': 'Techcombank'},
  {'image': 'assets/images/bank_bidv.png', 'bank_code': 'BIDV'},
  {'image': 'assets/images/bank_agribank.png', 'bank_code': 'Agribank'},
  {'image': 'assets/images/bank_sacombank.png', "bank_code": "Sacombank"},
  {'image': 'assets/images/bank_acb.png', "bank_code": "ACB"},
  {'image': 'assets/images/bank_vpbank.png', "bank_code": "VPbank"},
  {'image': 'assets/images/bank_dongabank.png', "bank_code": "DongAbank"},
  {'image': 'assets/images/bank_eximbank.png', "bank_code": "Eximbank"},
  {'image': 'assets/images/bank_tpbank.png', "bank_code": "TPbank"},
  {'image': 'assets/images/bank_ncb.png', "bank_code": "NCB"},
  {'image': 'assets/images/bank_ocean.png', "bank_code": "OJB"},
  {'image': 'assets/images/bank_msb.png', "bank_code": "MSbank"},
  {'image': 'assets/images/bank_hdbank.png', "bank_code": "HDbank"},
  {'image': 'assets/images/bank_namabank.png', "bank_code": "NamAbank"},
  {'image': 'assets/images/bank_ocb.png', "bank_code": "OCB"},
  {'image': 'assets/images/bank_mbbank.png', "bank_code": "MBbank"},
  {'image': 'assets/images/bank_shinhan.png', "bank_code": "Shinhan"},
  {'image': 'assets/images/bank_vib.png', "bank_code": "VIB"},
  {'image': 'assets/images/bank_bacabank.png', "bank_code": "BacAbank"},
  {'image': 'assets/images/bank_vieta.png', "bank_code": "VietAbank"},
  {'image': 'assets/images/bank_banviet.png', "bank_code": "Bản Việt"},
  {'image': 'assets/images/bank_seabank.png', "bank_code": "SEAbank"},
  {'image': 'assets/images/bank_woori.png', "bank_code": "WOORIbank"},
  {'image': 'assets/images/bank_pvcom.png', "bank_code": "PVcombank"},
  {'image': 'assets/images/bank_saigon.png', "bank_code": "Saigonbank"},
  {'image': 'assets/images/bank_lienviet.png', "bank_code": "Liên Việt"},
  {'image': 'assets/images/bank_kienlong.png', "bank_code": "Kiên Long"},
  {'image': 'assets/images/bank_baoviet.png', "bank_code": "Bảo Việt"},
  {'image': 'assets/images/bank_publishbank.png', "bank_code": "VIDBANK"},
  {'image': 'assets/images/bank_pgbank.png', "bank_code": "PGbank"},
  {'image': 'assets/images/bank_vrb.png', "bank_code": "VRB"},
  {'image': 'assets/images/bank_gb.png', "bank_code": "GPBank"},
  {'image': 'assets/images/bank_uob.png', "bank_code": "UOB"},
];

String imageBank({required String bankName}) {
  Map<String, String> map = {
    'Vietcombank': 'assets/images/bank_vietcombank.png',
    'Vietinbank': 'assets/images/bank_viettinbank.png',
    'Techcombank': 'assets/icons/ic_techcombank_logo.png',
    'BIDV': 'assets/images/bank_bidv.png',
    'Agribank': 'assets/images/bank_agribank.png',
    'Sacombank': 'assets/images/bank_sacombank.png',
    'ACB': 'assets/images/bank_acb.png',
    'VPbank': 'assets/images/bank_vpbank.png',
    'DongAbank': 'assets/images/bank_dongabank.png',
    'Eximbank': 'assets/images/bank_eximbank.png',
    'TPbank': 'assets/images/bank_tpbank.png',
    'NCB': 'assets/images/bank_ncb.png',
    'OJB': 'assets/images/bank_ocean.png',
    'MSbank': 'assets/images/bank_msb.png',
    'HDbank': 'assets/images/bank_hdbank.png',
    'NamAbank': 'assets/images/bank_namabank.png',
    'OCB': 'assets/images/bank_ocb.png',
    'MBbank': 'assets/images/bank_mbbank.png',
    'Shinhan': 'assets/images/bank_shinhan.png',
    'VIB': 'assets/images/bank_vib.png',
    'BacAbank': 'assets/images/bank_bacabank.png',
    'VietAbank': 'assets/images/bank_vieta.png',
    'SEAbank': 'assets/images/bank_seabank.png',
    'WOORIbank': 'assets/images/bank_woori.png',
    'PVcombank': 'assets/images/bank_pvcom.png',
    'Saigonbank': 'assets/images/bank_saigon.png',
    'VIDBANK': 'assets/images/bank_publishbank.png',
    'PGbank': 'assets/images/bank_pgbank.png',
    'VRB': 'assets/images/bank_vrb.png',
    'GPBank': 'assets/images/bank_gb.png',
    'UOB': 'assets/images/bank_uob.png',
    "Bảo Việt": 'assets/images/bank_baoviet.png',
    "Kiên Long": 'assets/images/bank_kienlong.png',
    "Liên Việt": 'assets/images/bank_lienviet.png',
    "Bản Việt": 'assets/images/bank_banviet.png',
    "VISA": iconVisaLogo,
    "Mastercard": iconMasterCardLogo
  };
  return map[bankName] ?? '';
}

String bankCodes({int bank = 0}) {
  Map<int, String> map = {
    0: "VISA",
    1: "Mastercard",
  };
  return map[bank] ?? "";
}

List<String> bankCode = [
  'VISA',
  'Mastercard',
  'Vietcombank',
  'Vietinbank',
  'Techcombank',
  'BIDV',
  'Agribank',
  'Sacombank',
  'ACB',
  'VPbank',
  'DongAbank',
  'Eximbank',
  'TPbank',
  'NCB',
  'OJB',
  'MSbank',
  'HDbank',
  'NamAbank',
  'OCB',
  'MBbank',
  'Shinhan',
  'VIB',
  'BacAbank',
  'VietAbank',
  'SEAbank',
  'WOORIbank',
  'PVcombank',
  'Saigonbank',
  'VIDBANK',
  'PGbank',
  'VRB',
  'GPBank',
  'UOB',
  "Bảo Việt",
  "Kiên Long",
  "Liên Việt",
  "Bản Việt",
];

List<String> bankNames = [
  'Vietcombank',
  'Vietinbank',
  'Techcombank',
  'BIDV',
  'Agribank',
  'Sacombank',
  'ACB',
  'VPbank',
  'DongAbank',
  'Eximbank',
  'TPbank',
  'NCB',
  'OJB',
  'MSbank',
  'HDbank',
  'NamAbank',
  'OCB',
  'MBbank',
  'Shinhan',
  'VIB',
  'BacAbank',
  'VietAbank',
  'SEAbank',
  'WOORIbank',
  'PVcombank',
  'Saigonbank',
  'VIDBANK',
  'PGbank',
  'VRB',
  'GPBank',
  'UOB',
  "Bảo Việt",
  "Kiên Long",
  "Liên Việt",
  "Bản Việt",
];
