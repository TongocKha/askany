import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/models/request_model.dart';
import 'package:askany/src/ui/common/widgets/text_ui/text_ui.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

const Color colorAddButton = Color(0xFFF0F6F6);
const Color colorFinished = Color(0xFFB31D1D);
const Color colorRequestCard = Color(0xFFFFFFFF);
const Color colorFocusItemDropdown = Color(0xFFE2F0E9);
const Color colorGrey3 = Color(0xFFE3E3E3);
const Color colorChosen = Color(0xFF167BA7);
const Color colorChosenCard = Color(0xFFF0F6F6);
const Color colorNotChosenCard = Color(0xFFF7E8E8);

const int WAIT_FOR_PAYMENT = 2;
const int WAIT_FOR_CONFIRM = 3;
const int CONFIRMED = 4;
const int COMPLETED = 5;
const int CANCELED = -2;
const int REPORTED = -3;
const int ADVISED = 6;

Widget buildRequestStatus({
  required RequestModel request,
  bool isExpert = false,
  bool isExpertChosen = false,
  bool isCommonRequest = false,
  Color textColor = colorFontGreen,
}) {
  if (isCommonRequest) {
    return TextUI(
      isExpert ? Strings.bidding.i18n : Strings.happening.i18n,
      color: textColor,
      fontWeight: FontWeight.w600,
      fontSize: 11.sp,
    );
  }

  if (isExpert) {
    if (isExpertChosen) {
      return TextUI(
        Strings.bidSuccess.i18n,
        color: textColor,
        fontWeight: FontWeight.w600,
        fontSize: 11.sp,
      );
    } else {
      return TextUI(
        Strings.bid.i18n,
        color: colorChosen,
        fontWeight: FontWeight.w600,
        fontSize: 11.sp,
      );
    }
  } else {
    if (request.selectedOffers.length == 0) {
      return TextUI(
        Strings.noExpertSelected.i18n,
        color: colorFinished,
        fontWeight: FontWeight.w600,
        fontSize: 11.sp,
      );
    } else {
      return TextUI(
        Strings.selectedAnExpert.i18n,
        color: colorFontGreen,
        fontWeight: FontWeight.w600,
        fontSize: 11.sp,
      );
    }
  }
}

Color getColorByStatus({
  required RequestModel request,
  bool isExpert = false,
  bool isExpertChosen = false,
  bool isCommonRequest = false,
}) {
  if (isCommonRequest) {
    return colorFontGreen;
  }

  if (isExpert) {
    if (isExpertChosen) {
      return colorFontGreen;
    } else {
      return colorChosen;
    }
  } else {
    if (request.selectedOffers.length > 0) {
      return colorFontGreen;
    } else {
      return colorFinished;
    }
  }
}

// Assets
const String iconCreateRequest = 'assets/icons/ic_add_request.png';
const String iconLocation = 'assets/icons/ic_location.png';
const String iconDot = 'assets/icons/ic_dot.png';
const String iconStar = 'assets/icons/ic_star.png';
const String iconStarEmpty = 'assets/icons/ic_star.png';
const String iconStarGrey = 'assets/icons/ic_star_grey.png';

const String iconArrowUp = 'assets/icons/ic_down.png';
const String iconArrowDown = 'assets/icons/ic_down.png';
const String imageRequestEmpty = 'assets/images/img_request_empty.png';
const String imagePaymentSuccess = 'assets/images/img_payment_success.png';
const String imagePaymentFailed = 'assets/images/img_payment_failed.png';
const String imageArrowTutorial = 'assets/images/img_arrow_tutorial.png';
const String imageSwipeTutorial = 'assets/images/img_swipe_tutorial.png';
const String iconArrowUp2 = 'assets/icons/ic_arrow_up.png';
const String iconTickVector = 'assets/icons/ic_check_vector.png';

final listBusyTime = [
  [DateTime(2022, 1, 19, 2, 15), DateTime(2022, 1, 19, 3, 15)],
  [DateTime(2022, 1, 20, 19, 15), DateTime(2022, 1, 20, 21, 15)],
  [DateTime(2022, 1, 20, 22, 15), DateTime(2022, 1, 20, 23, 15)],
  [DateTime(2022, 1, 21, 0, 15), DateTime(2022, 1, 21, 3, 15)],
];

const listTime = [
  '00:00',
  '00:15',
  '00:30',
  '00:45',
  '01:00',
  '01:15',
  '01:30',
  '01:45',
  '02:00',
  '02:15',
  '02:30',
  '02:45',
  '03:00',
  '03:15',
  '03:30',
  '03:45',
  '04:00',
  '04:15',
  '04:30',
  '04:45',
  '05:00',
  '05:15',
  '05:30',
  '05:45',
  '06:00',
  '06:15',
  '06:30',
  '06:45',
  '07:00',
  '07:15',
  '07:30',
  '07:45',
  '08:00',
  '08:15',
  '08:30',
  '08:45',
  '09:00',
  '09:15',
  '09:30',
  '09:45',
  '10:00',
  '10:15',
  '10:30',
  '10:45',
  '11:00',
  '11:15',
  '11:30',
  '11:45',
  '12:00',
  '12:15',
  '12:30',
  '12:45',
  '13:00',
  '13:15',
  '13:30',
  '13:45',
  '14:00',
  '14:15',
  '14:30',
  '14:45',
  '15:00',
  '15:15',
  '15:30',
  '15:45',
  '16:00',
  '16:15',
  '16:30',
  '16:45',
  '17:00',
  '17:15',
  '17:30',
  '17:45',
  '18:00',
  '18:15',
  '18:30',
  '18:45',
  '19:00',
  '19:15',
  '19:30',
  '19:45',
  '20:00',
  '20:15',
  '20:30',
  '20:45',
  '21:00',
  '21:15',
  '21:30',
  '21:45',
  '22:00',
  '22:15',
  '22:30',
  '22:45',
  '23:00',
  '23:15',
  '23:30',
  '23:45',
];
const List<String> createRequestListSubContents = [
  'Hàn Quốc',
  'Đức',
  'Đài Loan',
  'Úc',
  'Mỹ',
  'Nhật',
  'Pháo',
  'Nga',
  'Ý',
  'Thuỵ Sĩ',
];

const List<String> createRequestListContents = [
  'Du học',
  'Kinh doanh',
  'Đầu tư',
  'Luật',
  'Giáo dục',
  'Làm đẹo',
  'Marketing',
  'SEO và content',
  'Bất động sản',
  'Kiểm tra xe'
];
