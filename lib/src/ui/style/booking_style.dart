import 'package:flutter/material.dart';

const String iconVerified = 'assets/icons/ic_verified.png';

final colorWhiteCard = Color(0xFFFDFDFD);
final colorBlackShadow = Color(0xFF000000);
