// Assets
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

import 'package:askany/src/ui/style/calendar_style.dart';

const String categoryItemBackground = 'assets/images/box_category_item.png';
const String imageCategoryStudyAbroad =
    'assets/images/category_item_study_abroad.png';
const String imageCategorySoftwareAndTech =
    'assets/images/category_item_software_tech.png';
const String imageCategoryLaw = 'assets/images/category_item_law.png';
const String imageCategoryDigitalMarketing =
    'assets/images/category_item_digital_marketing.png';
const String imageCategoryHealth = 'assets/images/category_item_health.png';
const String imageCategoryMakeUp = 'assets/images/category_item_makeup.png';
const String imageCategoryDesign = 'assets/images/category_item_design.png';
const String imageCategoryBusiness = 'assets/images/category_item_business.png';
const String imageCategoryWriteAndTrans =
    'assets/images/category_item_write_and_trans.png';
const String imageCategoryEducation =
    'assets/images/category_item_education.png';
const String imageCategoryTravel = 'assets/images/category_item_travel.png';
const String iconFacebookContent = 'assets/icons/ic_facebook_content.png';
const String iconWebsiteContent = 'assets/icons/ic_website_content.png';
const String iconTiktokContent = 'assets/icons/ic_tiktok_content.png';
const String iconBlock = 'assets/icons/ic_block.png';
const String iconDOtGreen = 'assets/icons/ic_dot_green.png';
const Color colorGrey1 = Color(0xff6E6D7A);

final TextStyle categoryTextStyle3 =
    TextStyle(fontSize: 11.sp, color: colorGrey1, fontWeight: FontWeight.w400);
final TextStyle categoryTitleStyle =
    TextStyle(fontSize: 17.sp, color: colorBlack2, fontWeight: FontWeight.w700);
final TextStyle categoryExpertInforStyle =
    TextStyle(fontSize: 9.sp, color: colorBlack2, fontWeight: FontWeight.w400);
final TextStyle categoryExpertPriceStyle =
    TextStyle(fontSize: 14.sp, color: colorBlack2, fontWeight: FontWeight.w600);
final TextStyle categoryExpertInforStyle2 =
    TextStyle(fontSize: 11.sp, color: colorBlack2, fontWeight: FontWeight.w400);

final TextStyle categoryItemStyle =
    TextStyle(fontSize: 12.sp, color: colorBlack2);
final TextStyle categorySloganStyle =
    TextStyle(fontSize: 13.sp, color: colorBlack2);
final TextStyle contentTextStyle = TextStyle(
  fontSize: 12.sp,
  color: colorBlack2,
  fontWeight: FontWeight.w600,
);
final TextStyle expertExperieceTitleStyle =
    TextStyle(fontSize: 13.sp, color: colorBlack2, fontWeight: FontWeight.w600);
final TextStyle expertExperieceTextStyle =
    TextStyle(fontSize: 13.sp, color: colorBlack2, fontWeight: FontWeight.w400);
final TextStyle expertExperieceSubTextStyle =
    TextStyle(fontSize: 12.sp, color: colorGrey1, fontWeight: FontWeight.w400);
final TextStyle categoryExpertContentStyle =
    TextStyle(fontSize: 14.sp, color: colorBlack2, fontWeight: FontWeight.w700);
final TextStyle text13Grey1Weight400 =
    TextStyle(fontSize: 13.sp, color: colorGrey1, fontWeight: FontWeight.w400);
