import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/provinces.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class BottomSheetProvinceFilter extends StatelessWidget {
  final provinceList = Province().renderProvinceArray();
  final Function(String?) handPressed;
  BottomSheetProvinceFilter({required this.handPressed});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 85.h,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(18.sp),
          topRight: Radius.circular(18.sp),
        ),
      ),
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 4.sp),
            child: Image.asset(
              iconScrollBottomSheet,
              height: 2.sp,
              width: 60.sp,
            ),
          ),
          SizedBox(height: 18.sp),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20.sp),
            alignment: Alignment.centerLeft,
            child: Text(
              Strings.address.i18n,
              style: TextStyle(
                color: colorBlack2,
                fontSize: 13.sp,
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
          SizedBox(height: 13.sp),
          dividerChat,
          SizedBox(height: 10.sp),
          Expanded(
            child: RawScrollbar(
              isAlwaysShown: true,
              thumbColor: colorGreen4,
              radius: Radius.circular(30.sp),
              thickness: 4.sp,
              child: ListView.builder(
                padding: EdgeInsets.symmetric(horizontal: 20.sp),
                physics: BouncingScrollPhysics(),
                itemCount: provinceList.length,
                itemBuilder: (context, index) {
                  return TouchableOpacity(
                    onTap: () {
                      handPressed(provinceList[index]);
                      AppNavigator.pop();
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(vertical: 12.sp),
                      child: Text(
                        provinceList[index].toString(),
                        style: TextStyle(
                          color: colorBlack2,
                          fontSize: 13.sp,
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
