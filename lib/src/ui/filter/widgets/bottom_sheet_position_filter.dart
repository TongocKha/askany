import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/models/position_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class BottomSheetPositionFilter extends StatelessWidget {
  final List<PositionModel> positions = AppBloc.skillBloc.positions;
  final Function(PositionModel) handPressed;
  BottomSheetPositionFilter({required this.handPressed});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.sp),
      height: 35.h,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(18.sp),
          topRight: Radius.circular(18.sp),
        ),
      ),
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 4.sp),
            child: Image.asset(
              iconScrollBottomSheet,
              height: 2.sp,
              width: 60.sp,
            ),
          ),
          SizedBox(height: 18.sp),
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              Strings.positionInWork.i18n,
              style: TextStyle(
                color: colorBlack2,
                fontSize: 13.sp,
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
          SizedBox(height: 13.sp),
          dividerChat,
          SizedBox(height: 10.sp),
          Expanded(
            child: ListView.builder(
              physics: BouncingScrollPhysics(),
              itemCount: positions.length,
              itemBuilder: (context, index) {
                return TouchableOpacity(
                  onTap: () {
                    handPressed(positions[index]);
                    AppNavigator.pop();
                  },
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 10.sp),
                    child: Text(
                      positions[index].vi!.name,
                      style: TextStyle(
                        color: colorBlack2,
                        fontSize: 13.sp,
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
