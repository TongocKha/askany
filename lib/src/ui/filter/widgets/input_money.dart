import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class InputMoney extends StatefulWidget {
  @override
  State<InputMoney> createState() => _InputMoneyState();
}

class _InputMoneyState extends State<InputMoney> {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(child: _textFieldInputMoneyCard()),
        Container(
          alignment: Alignment.center,
          width: 18.sp,
          child: Text(
            ' - ',
            style: TextStyle(
              color: colorBlack2,
              fontSize: 14.sp,
            ),
          ),
        ),
        Expanded(child: _textFieldInputMoneyCard()),
      ],
    );
  }

  Widget _textFieldInputMoneyCard() {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 9.sp,
        vertical: 0.sp,
      ),
      height: 34.sp,
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(
          color: colorBorderTextField,
          width: 0.5.sp,
        ),
        borderRadius: BorderRadius.all(
          Radius.circular(
            5.sp,
          ),
        ),
      ),
      child: TextFormField(
        cursorColor: headerCalendarColor,
        style: TextStyle(
          color: Colors.black,
          fontSize: 12.sp,
        ),
        decoration: InputDecoration(
          prefixText: 'đ\t\t',
          border: InputBorder.none,
          hintStyle: TextStyle(
            color: colorGray2,
            fontSize: 12.sp,
          ),
        ),
      ),
    );
  }
}
