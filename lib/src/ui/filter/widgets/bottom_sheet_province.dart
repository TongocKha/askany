import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/provinces.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class BottomSheetProvince extends StatefulWidget {
  final provinceList = Province().renderProvinceArray();
  final Function(String?) handPressed;
  final String province;
  BottomSheetProvince({required this.handPressed, required this.province});

  @override
  State<BottomSheetProvince> createState() => _BottomSheetProvinceState();
}

class _BottomSheetProvinceState extends State<BottomSheetProvince> {
  late String? _province;
  @override
  void initState() {
    super.initState();
    _province = widget.province;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 85.h,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(18.sp),
          topRight: Radius.circular(18.sp),
        ),
      ),
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 4.sp),
            child: Image.asset(
              iconScrollBottomSheet,
              height: 2.sp,
              width: 60.sp,
            ),
          ),
          SizedBox(height: 18.sp),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20.sp),
            alignment: Alignment.centerLeft,
            child: Text(
              Strings.address.i18n,
              style: TextStyle(
                color: colorBlack2,
                fontSize: 13.sp,
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
          SizedBox(height: 13.sp),
          dividerChat,
          SizedBox(height: 10.sp),
          Expanded(
            child: RawScrollbar(
              isAlwaysShown: true,
              thumbColor: colorGreen4,
              radius: Radius.circular(30.sp),
              thickness: 4.sp,
              child: ListView.builder(
                physics: BouncingScrollPhysics(),
                itemCount: widget.provinceList.length,
                itemBuilder: (context, index) {
                  return TouchableOpacity(
                    onTap: () {
                      widget.handPressed(widget.provinceList[index]);
                      setState(() {
                        _province = widget.provinceList[index];
                      });
                    },
                    child: Container(
                      color: widget.provinceList[index] == _province ? colorChosenCard : Colors.transparent,
                      padding: EdgeInsets.symmetric(
                        vertical: 12.sp,
                        horizontal: 20.sp,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            widget.provinceList[index].toString(),
                            style: TextStyle(
                              color: colorBlack2,
                              fontSize: 13.sp,
                            ),
                          ),
                          Container(
                            width: 64.sp,
                            child: Align(
                              alignment: Alignment.centerRight,
                              child: widget.provinceList[index] == _province
                                  ? Image.asset(
                                      iconCheckGreen,
                                      width: 14.58.sp,
                                      height: 12.5.sp,
                                    )
                                  : null,
                            ),
                          )
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
