import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/user/user_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/helpers/extentions/string_extention.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/account/widgets/button_select.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/custom_image/network_image/cached_image.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ProfileExpertScreen extends StatefulWidget {
  const ProfileExpertScreen({Key? key}) : super(key: key);

  @override
  State<ProfileExpertScreen> createState() => _ProfileExpertScreenState();
}

class _ProfileExpertScreenState extends State<ProfileExpertScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserBloc, UserState>(
      builder: (context, state) {
        if (state is UserGetDone) {
          final ratingCount = AppBloc.userBloc.expertModel?.ratingCount ?? 0.0;
          return Scaffold(
            appBar: appBarTitleBack(
              context,
              '',
              backgroundColor: headerCalendarColor,
              brightness: Brightness.dark,
            ),
            body: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  alignment: Alignment.topLeft,
                  width: double.infinity,
                  decoration: BoxDecoration(color: headerCalendarColor),
                  padding: EdgeInsets.only(bottom: 18.sp, top: 6.sp),
                  child: Column(
                    children: [
                      TouchableOpacity(
                        onTap: () {
                          AppNavigator.push(
                            Routes.DETAILS_SPECIALIST,
                            arguments: {
                              'expertId': AppBloc.userBloc.getAccount.id,
                            },
                          );
                        },
                        child: Container(
                          color: Colors.transparent,
                          width: double.infinity,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              CustomNetworkImage(
                                width: 59.sp,
                                height: 59.sp,
                                margin: EdgeInsets.only(left: 16.sp),
                                urlToImage: state.accountModel.avatar?.urlToImage,
                              ),
                              SizedBox(width: 14.sp),
                              Expanded(
                                child: Container(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        color: Colors.transparent,
                                        child: Row(
                                          children: [
                                            Expanded(
                                              child: Text(
                                                state.accountModel.fullname!.formatName(),
                                                style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 16.sp,
                                                  fontWeight: FontWeight.w700,
                                                ),
                                              ),
                                            ),
                                            Text(
                                              Strings.viewInPage.i18n,
                                              style: TextStyle(
                                                fontSize: 10.sp,
                                                fontWeight: FontWeight.w400,
                                                color: Color(0xFFFDFDFD),
                                              ),
                                            ),
                                            SizedBox(width: 16.sp),
                                          ],
                                        ),
                                      ),
                                      SizedBox(height: 5.sp),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: [
                                          Image.asset(
                                            iconStar,
                                            width: 15.sp,
                                          ),
                                          SizedBox(
                                            width: 5.sp,
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(top: 2.sp),
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              children: [
                                                Text(
                                                  '${AppBloc.userBloc.expertModel?.stars ?? 0.0}',
                                                  style: TextStyle(
                                                    fontSize: 11.sp,
                                                    fontWeight: FontWeight.w400,
                                                    color: Color(
                                                      0xffFDFDFD,
                                                    ),
                                                  ),
                                                ),
                                                SizedBox(width: 5.sp),
                                                Text(
                                                  ratingCount >= 2
                                                      ? '($ratingCount ${Strings.reviews.i18n})'
                                                      : '($ratingCount ${Strings.review.i18n})',
                                                  style: TextStyle(
                                                    fontSize: 11.sp,
                                                    fontWeight: FontWeight.w400,
                                                    color: Color(
                                                      0xffFDFDFD,
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10.sp),
                  child: Column(
                    children: [
                      ButtonSelect(
                        content: Strings.expertInfomationAppbartext.i18n,
                        icon: iconInfo,
                        handlePressed: () {
                          AppNavigator.push(Routes.INFO_EXPERT);
                        },
                      ),
                      ButtonSelect(
                        content: Strings.exprienceAppbarText.i18n,
                        icon: 'assets/icons/ic_workings.png',
                        handlePressed: () {
                          AppNavigator.push(Routes.EXPERIENCE);
                        },
                      ),
                      ButtonSelect(
                        content: Strings.priceOfASingleServiceAppbartext.i18n,
                        icon: 'assets/icons/costPackage.png',
                        handlePressed: () {
                          AppNavigator.push(Routes.EXPERT_EDIT_PRICE);
                        },
                      ),
                      ButtonSelect(
                        content: Strings.mySkills.i18n,
                        icon: 'assets/icons/ic_skills.png',
                        handlePressed: () {
                          AppNavigator.push(Routes.ACCOUNT_SKILL);
                        },
                      ),
                    ],
                  ),
                )
              ],
            ),
          );
        }
        return Scaffold(
          body: Container(),
        );
      },
    );
  }
}
