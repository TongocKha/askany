import 'dart:io';
import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/authentication/authentication_event.dart';
import 'package:askany/src/bloc/user/user_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/provinces.dart';
import 'package:askany/src/helpers/province_helper.dart';
import 'package:askany/src/models/avatar_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/services/socket/socket_emit.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/custom_image/custom_image_picker.dart';
import 'package:askany/src/ui/common/widgets/custom_image/network_image/cached_image.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_form.dart';
import 'package:askany/src/ui/common/widgets/text_ui/title_text_field.dart';
import 'package:askany/src/ui/filter/widgets/bottom_sheet_province.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class EditProfileScreen extends StatefulWidget {
  @override
  _EditProfileScreenState createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _locationController = TextEditingController();
  int? _textProvince;
  String? _textFullname;
  File? _imagePicked;
  bool _isVerifyPhone = AppBloc.userBloc.getAccount.isVerifyPhone ?? false;

  Future<void> _trySubmitForm() async {
    if (_formKey.currentState!.validate()) {
      showDialogLoading();
      if (_imagePicked != null) {
        AppBloc.userBloc.add(
          UpdateInfoUserEvent(
            fullname: _textFullname!,
            province: _textProvince!.toString(),
            avatar: _imagePicked,
          ),
        );
      } else {
        AppBloc.userBloc.add(
          UpdateInfoUserEvent(
            fullname: _textFullname!,
            province: _textProvince!.toString(),
            avatar: null,
          ),
        );
      }
    }
  }

  @override
  void initState() {
    super.initState();
    _textFullname = AppBloc.userBloc.getAccount.fullname;
    _textProvince = AppBloc.userBloc.getAccount.province;
    _locationController.text = Province().getProvinceByCode(_textProvince!)!;
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserBloc, UserState>(
      builder: (context, state) {
        if (state is UserGetDone) {
          return Scaffold(
            resizeToAvoidBottomInset: false,
            appBar: appBarTitleBack(
              context,
              Strings.editProfileAppbartext.i18n,
              actions: [
                TouchableOpacity(
                  onTap: () async {
                    await _trySubmitForm();
                  },
                  child: Container(
                    alignment: Alignment.center,
                    color: Colors.transparent,
                    padding: EdgeInsets.only(
                      right: 10.sp,
                      // top: 12.sp,
                    ),
                    child: Text(
                      Strings.update.i18n,
                      style: TextStyle(
                        color: colorGreen2,
                        fontSize: 12.sp,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            body: Container(
              child: Container(
                height: 100.h,
                width: 100.w,
                padding: EdgeInsets.only(
                  left: 16.sp,
                  right: 16.sp,
                  bottom: 18.sp,
                ),
                child: Form(
                  key: _formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: SingleChildScrollView(
                          physics: BouncingScrollPhysics(),
                          child: Padding(
                            padding: EdgeInsets.only(bottom: 30.sp),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Center(
                                  child: TouchableOpacity(
                                    onTap: () {
                                      CustomImagePicker().openImagePicker(
                                        context: context,
                                        handleFinish: _handlePickImage,
                                      );
                                    },
                                    child: Stack(
                                      children: [
                                        _imagePicked == null
                                            ? CustomNetworkImage(
                                                width: 84.sp,
                                                height: 84.sp,
                                                margin: EdgeInsets.symmetric(
                                                  vertical: 16.sp,
                                                ),
                                                urlToImage: state.accountModel
                                                    .avatar?.urlToImage,
                                              )
                                            : Container(
                                                width: 84.sp,
                                                height: 84.sp,
                                                margin: EdgeInsets.symmetric(
                                                  vertical: 16.sp,
                                                ),
                                                decoration: BoxDecoration(
                                                  shape: BoxShape.circle,
                                                  image: DecorationImage(
                                                    image: getImage(
                                                      state.accountModel.avatar,
                                                    ),
                                                    fit: BoxFit.cover,
                                                  ),
                                                ),
                                              ),
                                        Positioned(
                                          bottom: 24.sp,
                                          right: 4.sp,
                                          child: TouchableOpacity(
                                            onTap: () {
                                              CustomImagePicker()
                                                  .openImagePicker(
                                                context: context,
                                                handleFinish: _handlePickImage,
                                              );
                                            },
                                            child: Image.asset(
                                              iconPenEdit,
                                              width: 16.sp,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                TitleTextField(
                                  title: Strings.nameTitle.i18n,
                                  padding: EdgeInsets.zero,
                                ),
                                TextFieldForm(
                                  initialValue: _textFullname!,
                                  validatorForm: (val) => val!.trim().length < 3
                                      ? Strings
                                          .nameMustBeAtLeast3CharactersErrortext
                                          .i18n
                                      : null,
                                  onChanged: (val) =>
                                      _textFullname = val.toString().trim(),
                                ),
                                TitleTextField(
                                    title: Strings.addressTitle.i18n),
                                TextFieldForm(
                                  onTap: () {
                                    showModalBottomSheet(
                                      isScrollControlled: true,
                                      context: context,
                                      backgroundColor: Colors.transparent,
                                      builder: (context) {
                                        return BottomSheetProvince(
                                          province: _locationController.text,
                                          handPressed: (value) {
                                            setState(() {
                                              _textProvince = ProvinceHelper()
                                                  .getCodeByProvince(value!)!;
                                              _locationController.text =
                                                  ProvinceHelper()
                                                      .getProvinceByCode(
                                                          _textProvince!)!;
                                            });
                                          },
                                        );
                                      },
                                    );
                                  },
                                  readOnly: true,
                                  initialValue: _textProvince == null
                                      ? Strings.address.i18n
                                      : ProvinceHelper()
                                          .getProvinceByCode(_textProvince!),
                                  controller: _locationController,
                                  suffixIcon: Image.asset(
                                    iconArrowDown,
                                    width: 10.sp,
                                  ),
                                  validatorForm: null,
                                ),
                                TitleTextField(
                                  title: Strings.emailHintext.i18n,
                                  option: (state.accountModel.isSocial ?? false)
                                      ? ''
                                      : Strings.changeYourEmail.i18n,
                                  handlePressed: () {
                                    AppNavigator.push(Routes.CHANGE_EMAIL);
                                  },
                                ),
                                TextFieldForm(
                                  initialValue: state.accountModel.email!,
                                  validatorForm: (val) => null,
                                  isAvailable: false,
                                ),
                                _isVerifyPhone == false
                                    ? TitleTextField(
                                        title: Strings.phoneNumberTitle.i18n,
                                        option:
                                            Strings.verifyYourPhoneNumber.i18n,
                                        handlePressed: () async {
                                          await AppNavigator.push(
                                              Routes.VERIFICATION_OTP,
                                              arguments: {
                                                'phone': state
                                                    .accountModel.phone
                                                    .toString()
                                              });
                                          AppBloc.authBloc.add(
                                            VerifyPhoneEvent(),
                                          );
                                        },
                                      )
                                    : TitleTextField(
                                        title: Strings.phoneNumberTitle.i18n,
                                        option:
                                            Strings.changeYourPhoneNumber.i18n,
                                        handlePressed: () {
                                          AppNavigator.push(
                                              Routes.CHANGE_PHONE);
                                        },
                                      ),
                                TextFieldForm(
                                  initialValue: state.accountModel.phone!,
                                  validatorForm: (val) => null,
                                  isAvailable: false,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      TouchableOpacity(
                        onTap: () {
                          showDialogLoading();
                          SocketEmit().sendLogout();
                        },
                        child: Container(
                          color: Colors.transparent,
                          padding: EdgeInsets.only(
                            bottom: 4.sp,
                          ),
                          alignment: Alignment.bottomCenter,
                          child: Text(
                            Strings.logout.i18n,
                            style: TextStyle(
                              color: colorGray1,
                              fontSize: 14.sp,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        } else {
          return Scaffold(
            body: Container(),
          );
        }
      },
    );
  }

  void _handlePickImage(File image) async {
    setState(() {
      _imagePicked = image;
    });
  }

  ImageProvider getImage(AvatarModel? avatar) {
    return FileImage(_imagePicked!);
  }
}
