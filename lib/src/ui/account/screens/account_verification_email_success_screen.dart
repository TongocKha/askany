import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/home/home_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:askany/src/ui/style/style.dart';

class AccountVerificationEmailSuccessScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'assets/icons/ic_check_success.png',
              width: 37.sp,
            ),
            SizedBox(
              height: 20.sp,
            ),
            Text(
              Strings
                  .congratulationYouHasSuccesfullyVerifiedYourEmailPart1.i18n,
              style: TextStyle(
                  fontSize: 17.sp,
                  fontWeight: FontWeight.w600,
                  color: colorBlack1),
            ),
            Text(
              Strings
                  .congratulationYouHasSuccesfullyVerifiedYourEmailPart2.i18n,
              style: TextStyle(
                  fontSize: 17.sp,
                  fontWeight: FontWeight.w600,
                  color: colorBlack1),
            ),
            SizedBox(
              height: 15.sp,
            ),
            Text(
              Strings.nowYouCanExperiencingOurAmazingFeaturePart1.i18n,
              style: TextStyle(
                  fontSize: 12.sp,
                  color: colorBlack2,
                  fontWeight: FontWeight.w400),
            ),
            Text(Strings.nowYouCanExperiencingOurAmazingFeaturePart2.i18n,
                style: TextStyle(
                    fontSize: 12.sp,
                    color: colorBlack2,
                    fontWeight: FontWeight.w400)),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20.sp, vertical: 40.sp),
              child: ButtonPrimary(
                onPressed: () {
                  AppBloc.homeBloc.add(OnChangeIndexEvent());
                  AppNavigator.pushNamedAndRemoveUntil(Routes.ROOT);
                },
                text: Strings.backToHomePage.i18n,
              ),
            ),
          ],
        ),
      ),
    ));
  }
}
