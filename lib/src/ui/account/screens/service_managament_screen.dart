import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/ui/account/widgets/service_managament_shimmer_card.dart';
import 'package:askany/src/ui/common/widgets/empty/empty.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/service_managament/service_management_bloc.dart';
import 'package:askany/src/models/request_model.dart';
import 'package:askany/src/ui/account/widgets/service_managament_card.dart';
import 'package:askany/src/ui/common/widgets/pagination_list_view.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ServiceManagamentScreen extends StatefulWidget {
  final int tabNumber;
  final List<RequestModel> services;
  final ServiceManagamentState currentState;
  ServiceManagamentScreen({
    Key? key,
    required this.tabNumber,
    required this.services,
    required this.currentState,
  }) : super(key: key);

  @override
  _ServiceManagamentScreenState createState() =>
      _ServiceManagamentScreenState();
}

class _ServiceManagamentScreenState extends State<ServiceManagamentScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return widget.services.isEmpty
        ? Empty(
            image: Image.asset(
              imageRequestEmpty,
              width: 140.sp,
              height: 82.sp,
            ),
            text: Strings.youDontHavenAnyRequest.i18n)
        : PaginationListView(
            padding: EdgeInsets.only(bottom: 10.h),
            isLoadMore: widget.currentState is GettingService,
            itemCount: widget.services.length,
            itemBuilder: (context, index) {
              return ServiceManagamentCard(
                indexOfOffer: index,
                service: widget.services[index],
                tabNumber: widget.tabNumber,
                isFirst: index == 0,
              );
            },
            callBackLoadMore: () {
              AppBloc.serviceManagamentBloc.add(GetServiceEvent());
            },
            callBackRefresh: (Function handleFinished) {
              AppBloc.serviceManagamentBloc.add(
                RefreshServiceEvent(
                  handleFinished: handleFinished,
                ),
              );
            },
            childShimmer: ServiceShimmerCard(index: 0),
          );
  }
}
