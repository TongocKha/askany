import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/authentication/authentication_event.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_form.dart';
import 'package:askany/src/ui/common/widgets/text_ui/title_text_field.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ChangePasswordScreen extends StatefulWidget {
  const ChangePasswordScreen({Key? key}) : super(key: key);

  @override
  _ChangePasswordScreenState createState() => _ChangePasswordScreenState();
}

class _ChangePasswordScreenState extends State<ChangePasswordScreen> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String _oldPassword = '';
  String _newPassword = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: appBarTitleBack(context, Strings.changePasswordAppbarText.i18n,),
      body: Container(
        height: 100.h,
        width: 100.w,
        padding: EdgeInsets.only(
          left: 16.sp,
          right: 16.sp,
          bottom: 18.sp,
        ),
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                children: [
                  SizedBox(height: 16.sp),
                  TitleTextField(title: Strings.currentPasswordTitle.i18n),
                  TextFieldForm(
                    isObscure: true,
                    initialValue: '',
                    validatorForm: (val) => val!.trim().length < 6
                        ? Strings
                            .yourPassWordMustBeAtLeast6CharactersErrortext.i18n
                        : null,
                    onChanged: (val) => _oldPassword = val.trim(),
                  ),
                  TitleTextField(title: Strings.newPasswordTitle.i18n),
                  TextFieldForm(
                    isObscure: true,
                    initialValue: '',
                    validatorForm: (val) => val!.trim().length < 6
                        ? Strings
                            .yourPassWordMustBeAtLeast6CharactersErrortext.i18n
                        : null,
                    onChanged: (val) => _newPassword = val.trim(),
                  ),
                  TitleTextField(title: Strings.confirmPasswordTitle.i18n),
                  TextFieldForm(
                    isObscure: true,
                    initialValue: '',
                    validatorForm: (val) => val!.trim() != _newPassword
                        ? Strings.passwordDoesNotMatchErrortext.i18n
                        : null,
                  ),
                ],
              ),
              ButtonPrimary(
                onPressed: () async {
                  if (_formKey.currentState!.validate()) {
                    showDialogLoading();
                    AppBloc.authBloc.add(
                      ChangePasswordEvent(
                        password: _oldPassword,
                        newPassword: _newPassword,
                      ),
                    );
                  }
                },
                text: Strings.save.i18n,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
