import 'package:askany/src/bloc/user/user_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/models/slide_mode.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_with_text_and_pop_button.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class AccountVerificationEmailScreen extends StatefulWidget {
  const AccountVerificationEmailScreen({Key? key}) : super(key: key);

  @override
  _AccountVerificationEmailScreenState createState() => _AccountVerificationEmailScreenState();
}

class _AccountVerificationEmailScreenState extends State<AccountVerificationEmailScreen> {
  bool isVerified = !true;
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserBloc, UserState>(builder: (context, state) {
      if (state is UserGetDone) {
        return Scaffold(
          appBar: appBarTitleBack(context, Strings.verifyEmailAppbarText.i18n),
          body: SafeArea(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16.sp),
              child: Column(
                children: [
                  SizedBox(height: 130.sp),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      isVerified == true
                          ? Container(
                              width: double.infinity,
                              alignment: Alignment.center,
                              margin: EdgeInsets.symmetric(horizontal: 12.sp),
                              child: Text(
                                Strings.verifyEmailSuggestion.i18n,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 12.5.sp,
                                  color: colorBlack2,
                                  height: 1.35.sp,
                                ),
                              ),
                            )
                          : Container(
                              width: double.infinity,
                              alignment: Alignment.center,
                              child: Column(
                                children: [
                                  Image.asset(
                                    imageEmailVerified,
                                    width: 50.sp,
                                  ),
                                  SizedBox(height: 25.sp),
                                  Text(
                                    state.accountModel.email.toString(),
                                    style: TextStyle(
                                      color: colorBlack1,
                                      fontWeight: FontWeight.w600,
                                      fontSize: 15.sp,
                                    ),
                                  ),
                                  SizedBox(height: 8.sp),
                                  Text(
                                    Strings.yourEmailHasBeenVerified.i18n,
                                    style: TextStyle(
                                      fontSize: 12.sp,
                                      fontWeight: FontWeight.w400,
                                      color: colorGray1,
                                    ),
                                  )
                                ],
                              ),
                            ),
                      SizedBox(height: 35.sp),
                      isVerified == true
                          ? Container(
                              margin: EdgeInsets.symmetric(horizontal: 25.sp),
                              child: ButtonPrimary(
                                onPressed: () async {
                                  await dialogAnimationWrapper(
                                    slideFrom: SlideMode.bot,
                                    paddingTop: 18.sp,
                                    child: DialogWithTextAndPopButton(
                                        title: Strings.verifyEmailAppbarText.i18n,
                                        bodyBefore: Strings.pleaseOpenInboxOf.i18n,
                                        highlightText: 'hoanglam345@gmail.com',
                                        bodyAfter: Strings.openInboxSuggestion.i18n),
                                    borderRadius: 12.sp,
                                  );
                                },
                                text: Strings.verifyNow.i18n,
                              ),
                            )
                          : Container(
                              margin: EdgeInsets.symmetric(horizontal: 5.sp),
                              child: ButtonPrimary(
                                onPressed: () {
                                  AppNavigator.push(Routes.CHANGE_EMAIL);
                                },
                                text: Strings.changeYourEmail.i18n,
                              ),
                            ),
                    ],
                  )
                ],
              ),
            ),
          ),
        );
      }
      return Scaffold(
        body: Container(),
      );
    });
  }
}
