import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/experiences/experiences_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/models/experience_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/account/widgets/skill_shimmer_card.dart';
import 'package:askany/src/ui/authentication/screens/register_expert/widgets/experience_card.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/empty/empty.dart';
import 'package:askany/src/ui/common/widgets/pagination_list_view.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ExperienceScreen extends StatefulWidget {
  const ExperienceScreen({Key? key}) : super(key: key);

  @override
  State<ExperienceScreen> createState() => _ExperienceScreenState();
}

class _ExperienceScreenState extends State<ExperienceScreen> {
  @override
  void initState() {
    super.initState();
    AppBloc.experienceBloc.add(OnExperiencesEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarTitleBack(
        context,
        Strings.exprienceAppbarText.i18n,
      ),
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            dividerChat,
            SizedBox(height: 17.sp),
            Expanded(
              child: BlocBuilder<ExperiencesBloc, ExperiencesState>(
                builder: (context, state) {
                  if (state is ExperiencesInitial) {
                    return Container(
                      padding: EdgeInsets.symmetric(horizontal: 16.sp),
                      child: ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        padding: EdgeInsets.only(bottom: 30.sp),
                        itemCount: ITEM_COUNT_SHIMMER,
                        itemBuilder: (context, index) {
                          return SkillShimmerCard();
                        },
                      ),
                    );
                  }

                  List<ExperienceModel> experiences =
                      (state.props[0] as List).cast();

                  return Container(
                    padding: EdgeInsets.symmetric(horizontal: 16.sp),
                    child: experiences.isEmpty
                        ? Empty(
                            image: Image.asset(
                              imageSkillEmpty,
                              width: 134.33.sp,
                              height: 93.72.sp,
                            ),
                            text: Strings.createExperienceSuggestion.i18n,
                            hasButton: true,
                            buttonText: Strings.createNewExprience.i18n,
                            buttonOnPressed: () {
                              AppNavigator.push(
                                Routes.ADD_EXPERIENCE,
                              );
                            },
                          )
                        : PaginationListView(
                            callBackRefresh: (handleFinished) {
                              AppBloc.experienceBloc.add(
                                RefreshExperienceEvent(
                                    handleFinished: handleFinished),
                              );
                            },
                            padding: EdgeInsets.only(bottom: 40.sp),
                            childShimmer: SkillShimmerCard(),
                            physics: BouncingScrollPhysics(),
                            itemCount: experiences.length + 1,
                            itemBuilder: (context, index) {
                              return index == experiences.length
                                  ? TouchableOpacity(
                                      onTap: () {
                                        AppNavigator.push(
                                          Routes.ADD_EXPERIENCE,
                                        );
                                      },
                                      child: Container(
                                        margin: EdgeInsets.symmetric(
                                            vertical: 12.sp),
                                        child: Text(
                                          Strings
                                              .createNewExprienceWithPlus.i18n,
                                          style: TextStyle(
                                            color: colorFontGreen,
                                            fontSize: 13.sp,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    )
                                  : ExperienceCard(
                                      experience: experiences[index],
                                    );
                            },
                          ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
