import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/ui/account/widgets/skill_list.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class AccountSkillScreen extends StatefulWidget {
  const AccountSkillScreen({Key? key}) : super(key: key);

  @override
  _AccountSkillScreenState createState() => _AccountSkillScreenState();
}

class _AccountSkillScreenState extends State<AccountSkillScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarTitleBack(context, Strings.mySkills.i18n),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 16.sp),
        child: Column(
          children: [
            SizedBox(height: 10.sp),
            Expanded(
              child: SkillList(),
            ),
          ],
        ),
      ),
    );
  }
}
