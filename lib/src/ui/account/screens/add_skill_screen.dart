import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/skill/skill_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/models/author_model.dart';
import 'package:askany/src/models/budget_model.dart';
import 'package:askany/src/models/position_model.dart';
import 'package:askany/src/models/skill_model.dart';
import 'package:askany/src/models/slide_mode.dart';
import 'package:askany/src/models/specialty_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/scaffold_wrapper.dart';
import 'package:askany/src/ui/account/widgets/bottom_sheet_position.dart';
import 'package:askany/src/ui/account/widgets/bottom_sheet_specialty.dart';
import 'package:askany/src/ui/account/widgets/money_service_with_time_card.dart';
import 'package:askany/src/ui/account/widgets/text_title.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_ui/switch_button.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_confirm_cancel.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_form.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_form_request.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:flutter/services.dart';

class AddSkillScreen extends StatefulWidget {
  final SkillModel? skill;
  const AddSkillScreen({required this.skill});
  @override
  _AddSkillScreenState createState() => _AddSkillScreenState();
}

class _AddSkillScreenState extends State<AddSkillScreen> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  // State

  bool _isCreating = true;
  String _specialty = "";
  String _price15DropDown = Strings.vnd.i18n;
  String _price60DropDown = Strings.vnd.i18n;
  bool _isDirectSupport = true;
  List<String> _listKeyWordService = [];
  PositionModel? _positionModel;

  List<String> _listPrice = [Strings.vnd.i18n, Strings.usd.i18n];
  final _controller15Minute = TextEditingController();
  final _controller60Minute = TextEditingController();
  final _controllerNameSkill = TextEditingController();
  final _controllerNumYearSkill = TextEditingController();
  final _controllerNameNowCompany = TextEditingController();
  final _controllerDescribe = TextEditingController();
  TextEditingController _positionController = TextEditingController();
  TextEditingController _specialtyController = TextEditingController();

  List<String> lsMySpecialties = [];
  List<String> specialtiesTitle = [];
  List<SpecialtyModel> myFilterSpecialties = [];

  @override
  void initState() {
    super.initState();
    _isCreating = widget.skill == null;

    // Initial specialties
    List<SpecialtyModel> filterSpecialties = AppBloc.requestBloc.specialties
        .where((item) => item.getLocaleTitle != null)
        .toList();

    specialtiesTitle =
        filterSpecialties.map((item) => item.getLocaleTitle!).toSet().toList();

    // Initial Form for Editting
    if (!_isCreating) {
      _positionModel = widget.skill!.highestPosition;
      _positionController.text = _positionModel?.getLocaleTitle ?? '';

      _isDirectSupport =
          widget.skill!.meetPrice != null && widget.skill!.meetPrice!.cost > 0;
      int indexOfSpecialty = filterSpecialties
          .indexWhere((spec) => spec.id == widget.skill!.specialty!.id);

      if (indexOfSpecialty != -1) {
        _specialty = filterSpecialties[indexOfSpecialty].getLocaleTitle!;
      } else {
        _specialty = specialtiesTitle[0];
      }

      _price15DropDown = _controllerNameSkill.text = widget.skill!.name;
      _controllerNumYearSkill.text = widget.skill!.expExperience.toString();
      _controllerNameNowCompany.text = widget.skill!.companyName;
      _controllerDescribe.text = widget.skill!.description;
      _controller15Minute.text =
          widget.skill!.callPrice!.costStringWithoutCurrency;
      _price15DropDown = widget.skill!.callPrice!.currency.toUpperCase();

      if (_isDirectSupport) {
        _controller60Minute.text =
            widget.skill!.meetPrice!.costStringWithoutCurrency;
        _price60DropDown = widget.skill!.meetPrice!.currency.toUpperCase();
      }

      _listKeyWordService = widget.skill!.keywords;

      _specialtyController.text = _specialty;
    }
  }

  Future<void> onBackPressed() {
    return dialogAnimationWrapper(
      child: DialogConfirmCancel(
        cancelText: 'Huỷ',
        bodyBefore:
            'Bạn có chắc chắn muốn rời khỏi màn hình này, thông tin bạn nhập sẽ không được lưu lại?',
        onConfirmed: () {
          AppNavigator.pop();
          AppNavigator.pop();
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ScaffoldWrapper(
      child: Scaffold(
        appBar: appBarTitleBack(
          context,
          (_isCreating
                  ? Strings.addMoreAppbarText.i18n
                  : Strings.editAppbarText.i18n) +
              Strings.skillLowerKeyAppbarText.i18n,
          onBackPressed: onBackPressed,
        ),
        body: SingleChildScrollView(
          child: Form(
              key: _formKey,
              child: Container(
                color: backgroundDetails,
                child: Column(
                  children: [
                    Container(
                      color: colorWhite,
                      margin: EdgeInsets.symmetric(vertical: 6.sp),
                      padding: EdgeInsets.symmetric(horizontal: 16.sp),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 18.sp),
                          TextTitle(
                            textTitle:
                                Strings.selecrASectorSuggestionTitle.i18n,
                          ),
                          SizedBox(height: 10.sp),
                          TextFieldForm(
                            onTap: () {
                              showModalBottomSheet(
                                context: context,
                                isScrollControlled: true,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.vertical(
                                    top: Radius.circular(20.sp),
                                  ),
                                ),
                                builder: (context) => BottomSheetSpecialty(
                                  handlePressed: (val) {
                                    _specialty = val;
                                    setState(() {
                                      _specialtyController.text = _specialty;
                                    });
                                  },
                                  specialtiesTitle: specialtiesTitle,
                                  specialty: _specialty,
                                ),
                              );
                            },
                            controller: _specialtyController,
                            hintText: Strings.selectASectorHintext.i18n,
                            isActive: true,
                            readOnly: true,
                            suffixIcon: Container(
                              color: Colors.transparent,
                              child: Image.asset(
                                iconArrowDown,
                                width: 10.sp,
                              ),
                            ),
                            validatorForm: (val) => _specialty.isEmpty
                                ? Strings.haveNotSelectedAnySectorErrortext.i18n
                                : null,
                          ),
                          SizedBox(height: 22.sp),
                          TextTitle(
                            textTitle: Strings.nameOfYourSkillTitle.i18n,
                          ),
                          TextFieldForm(
                            controller: _controllerNameSkill,
                            fontSize: 13.sp,
                            contentPadding: EdgeInsets.all(10.sp),
                            hintText: Strings.skillNameHintext.i18n,
                            textInputAction: TextInputAction.go,
                            validatorForm: (val) =>
                                val!.isEmpty || val.trim().length == 0
                                    ? Strings
                                        .skillNameCannotBeLeftBlankErrortext
                                        .i18n
                                    : null,
                          ),
                          SizedBox(height: 22.sp),
                          TextTitle(
                              textTitle:
                                  Strings.yearsCountOfExprieceTitle.i18n),
                          TextFieldForm(
                            controller: _controllerNumYearSkill,
                            fontSize: 13.sp,
                            hintText:
                                Strings.enterYearsCountOfExperienceHintext.i18n,
                            contentPadding: EdgeInsets.all(10.sp),
                            textInputType: TextInputType.number,
                            textInputAction: TextInputAction.go,
                            inputFormatters: [
                              LengthLimitingTextInputFormatter(2),
                            ],
                            validatorForm: (val) => val!.isEmpty
                                ? Strings
                                    .yearsCountOfExperienceCannotBeBlankErrortext
                                    .i18n
                                : int.parse(_controllerNumYearSkill.text) == 0
                                    ? 'Số kinh nghiệm phải hơn 1 năm'
                                    : int.parse(_controllerNumYearSkill.text) >
                                            40
                                        ? 'Giới hạn số năm kinh nghiệm là 40'
                                        : null,
                          ),
                          SizedBox(height: 22.sp),
                          TextTitle(
                              textTitle:
                                  Strings.whatYourHighestPositionTitle.i18n),
                          TextFieldForm(
                            onTap: () {
                              showModalBottomSheet(
                                context: context,
                                isScrollControlled: true,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.vertical(
                                    top: Radius.circular(20.sp),
                                  ),
                                ),
                                builder: (context) => BottomSheetPosition(
                                  handlePressed: (val) {
                                    int indexOfPosition = AppBloc
                                        .skillBloc.positions
                                        .indexWhere((item) =>
                                            item.getLocaleTitle == val);

                                    if (indexOfPosition != -1) {
                                      _positionModel = AppBloc
                                          .skillBloc.positions[indexOfPosition];
                                      setState(() {
                                        _positionController.text =
                                            _positionModel?.getLocaleTitle ??
                                                '';
                                      });
                                    }
                                  },
                                  listPosition: AppBloc.skillBloc.positions
                                      .map((item) => item.getLocaleTitle!)
                                      .toList(),
                                  position: _positionModel?.getLocaleTitle,
                                ),
                              );
                            },
                            controller: _positionController,
                            hintText:
                                Strings.selectYourHighestPositionHintext.i18n,
                            isActive: true,
                            readOnly: true,
                            suffixIcon: Container(
                              color: Colors.transparent,
                              child: Image.asset(
                                iconArrowDown,
                                width: 10.sp,
                              ),
                            ),
                            validatorForm: (val) => _positionModel == null
                                ? Strings
                                    .positionCannotBeLeftBlankErrortext.i18n
                                : null,
                          ),
                          SizedBox(height: 22.sp),
                          TextTitle(
                            textTitle: Strings.whatYourCurrentCompanyTitle.i18n,
                          ),
                          TextFieldForm(
                              contentPadding: EdgeInsets.all(10.sp),
                              controller: _controllerNameNowCompany,
                              fontSize: 13.sp,
                              hintText: Strings.companyNameHintext.i18n,
                              textInputAction: TextInputAction.go,
                              validatorForm: (val) =>
                                  val!.isEmpty || val.trim().length == 0
                                      ? Strings
                                          .companyNameCannotBeLeftBlankErrortext
                                          .i18n
                                      : null),
                          SizedBox(height: 22.sp),
                          TextTitle(
                            textTitle: Strings.skillDescriptionTitle.i18n,
                          ),
                          TextFieldFormRequest(
                            controller: _controllerDescribe,
                            hintText: Strings.describeYourSkillHintext.i18n,
                            maxLines: 6,
                            validatorForm: (val) =>
                                val!.isEmpty || val.trim().length == 0
                                    ? Strings
                                        .skillDescribeCannotBeLeftBlankErrorText
                                        .i18n
                                    : null,
                          ),
                          SizedBox(height: 28.sp),
                        ],
                      ),
                    ),
                    Container(
                      width: 100.w,
                      color: colorWhite,
                      padding: EdgeInsets.symmetric(
                          horizontal: 16.sp, vertical: 30.sp),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          TextTitle(
                            textTitle:
                                Strings.costOfASingleCallingServiceTitle.i18n,
                          ),
                          MoneyServiceWithTimeCard(
                            isVND: _price15DropDown == _listPrice[0],
                            margin: EdgeInsets.only(top: 10.sp),
                            textEditingController: _controller15Minute,
                            textMinute: Strings.fifteenMinutes.i18n,
                            onChangeTextForm: (value) => value!,
                            validatorTextForm: (val) {
                              if (val!.isEmpty) {
                                return Strings
                                    .costCannotBeLeftblankErrortext.i18n;
                              } else
                                return null;
                            },
                            onChangeDropDown: (value) {
                              if (value != _price15DropDown) {
                                setState(() {
                                  _price15DropDown = value!;
                                  _controller15Minute.text = '';
                                });
                              }
                              return _price15DropDown = value!;
                            },
                            hintDropdown: '',
                            valueDropDown: _price15DropDown,
                            listDropDown: _listPrice,
                          ),
                          SizedBox(height: 22.sp),
                          AnimatedSwitcher(
                            duration: const Duration(
                                milliseconds: DURATION_DEFAULT_ANIMATION),
                            transitionBuilder:
                                (Widget child, Animation<double> animation) {
                              return ScaleTransition(
                                  scale: animation, child: child);
                            },
                            child: _isDirectSupport
                                ? Column(
                                    children: [
                                      TextTitle(
                                          textTitle: Strings
                                              .costOfASingleMeetingServiceTitle
                                              .i18n),
                                      MoneyServiceWithTimeCard(
                                        isVND:
                                            _price60DropDown == _listPrice[0],
                                        margin: EdgeInsets.only(top: 10.sp),
                                        textEditingController:
                                            _controller60Minute,
                                        onChangeTextForm: (value) => value!,
                                        textMinute: Strings.sixtyMinutes.i18n,
                                        hintDropdown: '',
                                        valueDropDown: _price60DropDown,
                                        validatorTextForm: (val) {
                                          if (_isDirectSupport &&
                                              val!.isEmpty) {
                                            return Strings
                                                .costCannotBeLeftblankErrortext
                                                .i18n;
                                          } else
                                            return null;
                                        },
                                        onChangeDropDown: (value) {
                                          if (value != _price60DropDown) {
                                            setState(() {
                                              _price60DropDown = value!;
                                              _controller60Minute.text = '';
                                            });
                                          }
                                          return value!;
                                        },
                                        listDropDown: _listPrice,
                                      ),
                                      SizedBox(height: 20.sp),
                                    ],
                                  )
                                : SizedBox(),
                          ),
                          Container(
                            child: Text(
                              Strings.participantsCountSuggetion.i18n,
                              textAlign: TextAlign.justify,
                              style: TextStyle(
                                height: 1.sp,
                                fontStyle: FontStyle.italic,
                                fontSize: 12.sp,
                                color: colorFontGreen,
                              ),
                            ),
                          ),
                          SizedBox(height: 20.sp),
                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                SwitchButton(
                                  value: true,
                                  text: Strings.call.i18n,
                                ),
                                SizedBox(width: 20.sp),
                                SwitchButton(
                                  value: _isDirectSupport,
                                  text: Strings.meet.i18n,
                                  onToggle: (val) {
                                    setState(() {
                                      _isDirectSupport = val;
                                    });
                                  },
                                ),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(vertical: 28.sp),
                            child: dividerChat,
                          ),
                          TextTitle(
                              textTitle: Strings.serviceKeywordTitle.i18n),
                          SizedBox(height: 10.sp),
                          Container(
                            margin: EdgeInsets.only(left: 1.sp),
                            child: Wrap(
                              spacing: 6.0,
                              runSpacing: 6.0,
                              children: List<Widget>.generate(
                                  _listKeyWordService.length, (index) {
                                return Container(
                                  child: Chip(
                                    padding: EdgeInsets.all(8.sp),
                                    backgroundColor: colorAddButton,
                                    label: Text(
                                      _listKeyWordService[index],
                                      style: TextStyle(fontSize: 12.sp),
                                    ),
                                    onDeleted: () {
                                      setState(() {
                                        _listKeyWordService.removeAt(index);
                                      });
                                    },
                                    deleteIcon: ImageIcon(
                                      AssetImage(iconRemove),
                                      size: 15,
                                      color: colorGray1,
                                    ),
                                    materialTapTargetSize:
                                        MaterialTapTargetSize.shrinkWrap,
                                  ),
                                );
                              }),
                            ),
                          ),
                          SizedBox(height: 10.sp),
                          TextFieldForm(
                            initialValue: "",
                            hintText: Strings.serviceKeywordHintext.i18n,
                            textInputAction: TextInputAction.go,
                            submitForm: (value) {
                              if (_listKeyWordService.length < 3 &&
                                  value!.trim().isNotEmpty) {
                                setState(() {
                                  _listKeyWordService.add(value.trim());
                                });
                              }
                            },
                            fontSize: 13.sp,
                            contentPadding: EdgeInsets.all(10.sp),
                            validatorForm: (val) {
                              if (_listKeyWordService.isNotEmpty) {
                                return null;
                              } else
                                return Strings
                                    .serviceKeyWordBlankErrortext.i18n;
                            },
                          ),
                        ],
                      ),
                    ),
                    Container(
                      width: 100.w,
                      color: colorWhite,
                      child: Column(
                        children: [
                          Container(
                            margin: EdgeInsets.only(bottom: 18.sp),
                            child: dividerChat,
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(horizontal: 16.sp),
                            child: ButtonPrimary(
                              onPressed: () {
                                if (!_formKey.currentState!.validate()) {
                                  return;
                                }

                                showDialogLoading();
                                int indexSpecialty = AppBloc
                                    .requestBloc.specialties
                                    .indexWhere((spec) =>
                                        spec.getLocaleTitle == _specialty);

                                SpecialtyModel _specialtyModel = AppBloc
                                    .requestBloc.specialties[indexSpecialty];

                                SkillModel _skillModel = SkillModel(
                                  status: 1,
                                  description: _controllerDescribe.text,
                                  name: _controllerNameSkill.text,
                                  keywords: _listKeyWordService,
                                  companyName: _controllerNameNowCompany.text,
                                  expExperience:
                                      int.parse(_controllerNumYearSkill.text),
                                  meetPrice: BudgetModel(
                                    cost: !_isDirectSupport ||
                                            _controller60Minute.text.length == 0
                                        ? 0
                                        : double.parse(_controller60Minute.text
                                            .replaceAll(',', '')),
                                    currency: _price60DropDown.toLowerCase(),
                                    totalMinutes: 15,
                                  ),
                                  callPrice: BudgetModel(
                                    cost: _controller15Minute.text.length == 0
                                        ? 0
                                        : double.parse(_controller15Minute.text
                                            .replaceAll(',', '')),
                                    currency: _price15DropDown.toLowerCase(),
                                    totalMinutes: 60,
                                  ),
                                  highestPosition: _positionModel,
                                  specialty: _specialtyModel,
                                  author: AuthorModel(
                                    province:
                                        AppBloc.userBloc.getAccount.province!,
                                    id: AppBloc.userBloc.getAccount.id!,
                                    fullname:
                                        AppBloc.userBloc.getAccount.fullname!,
                                  ),
                                  createdAt: DateTime.now(),
                                );

                                if (!_isCreating) {
                                  _skillModel.id = widget.skill!.id;
                                  AppBloc.skillBloc.add(
                                    UpdateSkillEvent(
                                      skill: _skillModel,
                                    ),
                                  );
                                } else {
                                  AppBloc.skillBloc.add(
                                    AddSkillEvent(
                                      skill: _skillModel,
                                    ),
                                  );
                                }
                              },
                              text: _isCreating
                                  ? Strings.addMoreSkillWithoutPlus.i18n
                                  : Strings.save.i18n,
                            ),
                          ),
                          _isCreating
                              ? SizedBox()
                              : TouchableOpacity(
                                  onTap: () {
                                    dialogAnimationWrapper(
                                      slideFrom: SlideMode.bot,
                                      child: DialogConfirmCancel(
                                          onConfirmed: () {
                                            AppNavigator.pop();
                                            showDialogLoading();
                                            AppBloc.skillBloc.add(
                                              DeleteSkillEvent(
                                                  id: widget.skill!.id!),
                                            );
                                          },
                                          confirmText: Strings.clear.i18n,
                                          bodyBefore: Strings
                                              .areYouSureToSlearThisSkill.i18n,
                                          cancelText: Strings.cancel.i18n),
                                    );
                                  },
                                  child: Container(
                                    width: 100.w,
                                    height: 40.sp,
                                    margin: EdgeInsets.symmetric(
                                        vertical: 6.sp, horizontal: 16.sp),
                                    padding:
                                        EdgeInsets.symmetric(vertical: 8.sp),
                                    child: Center(
                                      child: Text(
                                        Strings.deleteThisSkill.i18n,
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: colorGray1,
                                            fontSize: 13.sp,
                                            fontWeight: FontWeight.w600),
                                      ),
                                    ),
                                  ),
                                ),
                          SizedBox(height: 30.sp),
                        ],
                      ),
                    ),
                  ],
                ),
              )),
        ),
      ),
    );
  }
}
