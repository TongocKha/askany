import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/authentication/authentication_event.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_otp.dart';
import 'package:askany/src/ui/style/calendar_style.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class InputOTPScreen extends StatefulWidget {
  final String phone;
  const InputOTPScreen({required this.phone});
  @override
  _InputOTPScreenState createState() => _InputOTPScreenState();
}

class _InputOTPScreenState extends State<InputOTPScreen> {
  String code = "";

  @override
  void initState() {
    super.initState();
    AppBloc.authBloc.add(
      UpdatePhoneEvent(phone: widget.phone),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: appBarTitleBack(context, Strings.verifyYourPhoneAppbarText.i18n),
      body: SafeArea(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 16.sp),
          child: Column(
            children: [
              SizedBox(height: 36.sp),
              Text(
                Strings.enterOTPsuggestion.i18n,
                textAlign: TextAlign.justify,
                style: TextStyle(
                  color: colorBlack2,
                  fontSize: 13.sp,
                  fontWeight: FontWeight.w400,
                  height: 1.25.sp,
                ),
              ),
              SizedBox(height: 23.sp),
              TextFieldOTP(onChanged: (value) {
                setState(() {
                  code = value;
                });
              }),
              SizedBox(height: 21.sp),
              ButtonPrimary(
                onPressed: () {
                  if (code.length == 6) {
                    AppBloc.authBloc.add(
                      VerifyOtpEvent(phone: widget.phone, code: code),
                    );
                  }
                },
                text: Strings.verify.i18n,
              )
            ],
          ),
        ),
      ),
    );
  }
}
