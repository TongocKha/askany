import 'dart:io';
import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/authentication/authentication_event.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/custom_image/custom_image_picker.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_fullscreen_onebutton.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_form.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class AccountVertifyCompanyScreen extends StatefulWidget {
  const AccountVertifyCompanyScreen({Key? key}) : super(key: key);

  @override
  _AccountVertifyCompanyScreenState createState() =>
      _AccountVertifyCompanyScreenState();
}

class _AccountVertifyCompanyScreenState
    extends State<AccountVertifyCompanyScreen> {
  File? _imagePicked;
  bool isVerify = false;
  String textLink = '';
  bool _isVerifiedCompany =
      AppBloc.userBloc.getAccount.isVerifiedCompany ?? false;
  int isConfirm = AppBloc.userBloc.accountModel!.experienceConfirm != null
      ? AppBloc.userBloc.getAccount.experienceConfirm!.status
      : 2;

  String title = 'Gửi';
  void _handlePickImage(File image) async {
    setState(() {
      _imagePicked = image;
    });
  }

  @override
  void initState() {
    super.initState();
    switch (isConfirm) {
      case 0:
        title = Strings.processing.i18n;
        break;
      case 1:
        title = Strings.confirmed.i18n;
        break;
      case -1:
        title = Strings.rejected.i18n;
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: appBarTitleBack(
          context, Strings.verifyYourCompanyAndPositionAppbartext.i18n),
      body: _isVerifiedCompany
          ? buildSuccessVertify()
          : buildVertifyImageScreen(),
    );
  }

  Widget buildSuccessVertify() {
    return Container(
      width: 100.w,
      padding: EdgeInsets.symmetric(horizontal: 16.sp),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(height: 20.h),
          Container(
            height: 50.sp,
            width: 50.sp,
            child: Image.asset(
              iconBagCompany,
              height: 20.sp,
              width: 20.sp,
            ),
          ),
          SizedBox(height: 20.sp),
          Text(
            Strings.yourCompanyAndPositionHaveBeenVerified.i18n,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: colorGray1,
              fontSize: 11.5.sp,
              fontWeight: FontWeight.w400,
            ),
          )
        ],
      ),
    );
  }

  Widget buildVertifyImageScreen() {
    return Container(
      padding: EdgeInsets.only(left: 16.sp, right: 16.sp, bottom: 18.sp),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.symmetric(vertical: 20.sp),
            child: Text(
              Strings.showEmploymentContractSuggestion.i18n,
              style: TextStyle(
                height: 1.5,
                fontSize: 13.sp,
                color: colorBlack2,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 8.sp),
            child: Text(
              Strings.linkedinAccount.i18n,
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 13.sp,
                color: colorBlack2,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 22.sp),
            child: TextFieldForm(
              hintText: Strings.linkedinLinkHintext.i18n,
              initialValue: textLink,
              validatorForm: (val) => val!.trim().length < 3
                  ? Strings.atLeast3Characters
                  //  'Kí tự phải có ít nhất 3 ký tự' ???
                  : null,
              onChanged: (val) => textLink = val.toString().trim(),
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 12.sp),
            child: Text(
              Strings.eitherUploadPhoto.i18n,
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 13.sp,
                color: colorBlack2,
              ),
            ),
          ),
          TouchableOpacity(
              onTap: () {
                CustomImagePicker().openImagePicker(
                  context: context,
                  isRequireCrop: false,
                  handleFinish: _handlePickImage,
                );
              },
              child: Container(
                margin: EdgeInsets.symmetric(vertical: 8.sp),
                child: DottedBorder(
                    color: colorGreen3,
                    // gap: 3,
                    strokeWidth: 1,
                    dashPattern: [8, 4],
                    borderType: BorderType.RRect,
                    radius: Radius.circular(12),
                    child: _imagePicked != null
                        ? Container(
                            width: 77.sp,
                            height: 77.sp,
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8.sp)),
                              shape: BoxShape.rectangle,
                              image: _imagePicked == null
                                  ? null
                                  : DecorationImage(
                                      image: FileImage(_imagePicked!),
                                      fit: BoxFit.cover,
                                    ),
                            ),
                          )
                        : Container(
                            width: 77.sp,
                            height: 77.sp,
                            padding: EdgeInsets.all(23.sp),
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8.sp)),
                              color: colorAddButton,
                            ),
                            child: Image.asset(
                              iconPhoto2,
                              width: 25.sp,
                              height: 25.sp,
                            ),
                          )),
              )),
          Spacer(),
          isConfirm != 2
              ? ButtonPrimary(
                  onPressed: () {},
                  text: title,
                )
              : (textLink == '' || _imagePicked == null)
                  ? ButtonPrimary(
                      onPressed: () async {
                        await showAlertDialogFullScreenOneButton(
                          title: Strings.notice.i18n,
                          content: Strings.privideEnoughInformationNotice.i18n,
                          textButton: Strings.underStood.i18n,
                          onPressedButton: () {},
                          context: context,
                        );
                      },
                      text: isConfirm != 2 ? title : Strings.send.i18n,
                    )
                  : ButtonPrimary(
                      onPressed: () async {
                        await showAlertDialogFullScreenOneButton(
                          title: Strings
                              .verificationInformationSentSuccessfully.i18n,
                          content: Strings.checkWithin7Days.i18n,
                          textButton: Strings.underStood.i18n,
                          onPressedButton: () {},
                          context: context,
                        );
                        AppBloc.authBloc.add(
                          VerifyCompanyEvent(
                              linkedinURL: textLink,
                              images: _imagePicked.toString()),
                        );
                      },
                      text: isConfirm != 2 ? title : Strings.send.i18n,
                    )
        ],
      ),
    );
  }
}
