import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/authentication/bloc.dart';
import 'package:askany/src/bloc/user/user_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_form.dart';
import 'package:askany/src/ui/style/calendar_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ChangeEmailScreen extends StatefulWidget {
  @override
  _ChangeEmailScreenState createState() => _ChangeEmailScreenState();
}

class _ChangeEmailScreenState extends State<ChangeEmailScreen> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String textEmail = '';
  bool isC = true;
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserBloc, UserState>(builder: (context, state) {
      if (state is UserGetDone) {
        return Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: appBarTitleBack(
            context,
            Strings.changeEmailAppbarText.i18n,
          ),
          body: SafeArea(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16.sp),
              child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    SizedBox(
                      height: 30.sp,
                    ),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        Strings.enterYourEmailSugestion.i18n,
                        textAlign: TextAlign.justify,
                        style: TextStyle(
                          color: colorBlack2,
                          fontSize: 13.sp,
                          fontWeight: FontWeight.w400,
                          height: 1.25.sp,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10.sp,
                    ),
                    TextFieldForm(
                      hintText: Strings.emailHintext.i18n,
                      initialValue: '',
                      validatorForm: (val) => val!.trim().length < 3
                          ? Strings.emailMustBeAtLeast3CharactersErrortext.i18n
                          : null,
                      onChanged: (val) => textEmail = val.toString().trim(),
                    ),
                    SizedBox(
                      height: 20.sp,
                    ),
                    ButtonPrimary(
                      onPressed: () async {
                        if (_formKey.currentState!.validate()) {
                          showDialogLoading();
                          AppBloc.authBloc.add(
                            UpdateEmailEvent(email: textEmail),
                          );
                          // AppNavigator.pop();
                        }
                      },
                      text: Strings.next.i18n,
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      }
      return Scaffold(
        body: Container(),
      );
    });
  }
}
