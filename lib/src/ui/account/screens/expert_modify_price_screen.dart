import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/user/user_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/helpers/date_time_helper.dart';
import 'package:askany/src/models/budget_model.dart';
import 'package:askany/src/models/expert_model.dart';
import 'package:askany/src/models/slide_mode.dart';
import 'package:askany/src/models/time_line_price_model.dart';
import 'package:askany/src/models/weekly_schedule_model.dart';
import 'package:askany/src/ui/account/widgets/set_time_available_view.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_with_text_and_pop_button.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:askany/src/ui/account/widgets/money_service_with_time_card.dart';
import 'package:askany/src/ui/account/widgets/text_title.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_ui/switch_button.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';

enum PriceError {
  vi,
  en,
  valid,
  empty,
}

class ExpertEditPriceScreen extends StatefulWidget {
  const ExpertEditPriceScreen({Key? key}) : super(key: key);

  @override
  _ExpertEditPriceScreenState createState() => _ExpertEditPriceScreenState();
}

class _ExpertEditPriceScreenState extends State<ExpertEditPriceScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String _price15DropDown = Strings.vnd.i18n;
  String _price60DropDown = Strings.vnd.i18n;
  bool _isDirectSupport = true;

  List<String> _listPrice = [Strings.vnd.i18n, Strings.usd.i18n];
  final _controller15Minute = TextEditingController();
  final _controller60Minute = TextEditingController();

  List<TimelinePriceModel> timelines = [];
  List<DateTime> datePicked = [];
  List<DateTime> weeks = getCalendarThisWeek();
  late PriceError _callPriceError;
  late PriceError _meetPriceError;

  @override
  void initState() {
    super.initState();
    if (AppBloc.userBloc.expertModel != null) {
      ExpertModel _expert = AppBloc.userBloc.expertModel!;
      _isDirectSupport = _expert.meetPrice != null && _expert.meetPrice!.cost > 0;

      if (_expert.callPrice != null) {
        _controller15Minute.text = _expert.callPrice!.costStringWithoutCurrency;
        _price15DropDown = _expert.callPrice!.currency.toUpperCase();
      }

      if (_isDirectSupport) {
        _controller60Minute.text = _expert.meetPrice!.costStringWithoutCurrency;
        _price60DropDown = _expert.meetPrice!.currency.toUpperCase();
      }
      if (_controller15Minute.text == '') {
        _callPriceError = PriceError.empty;
      } else {
        if (_price15DropDown == _listPrice[0]) {
          final String _value = _controller15Minute.text.replaceAll(new RegExp(r'[^\w\s]+'), '');
          if (double.parse(_value) < 12000) {
            _callPriceError = PriceError.vi;
          } else {
            _callPriceError = PriceError.valid;
          }
        } else {
          final String _value = _controller15Minute.text.replaceAll(new RegExp(','), '');

          if (double.parse(_value) < 0.5) {
            _callPriceError = PriceError.en;
          } else {
            _callPriceError = PriceError.valid;
          }
        }
      }
      if (_controller60Minute.text == '') {
        _meetPriceError = PriceError.empty;
      } else {
        if (_price60DropDown == _listPrice[0]) {
          final String _value = _controller60Minute.text.replaceAll(new RegExp(r'[^\w\s]+'), '');
          if (double.parse(_value) < 12000) {
            _meetPriceError = PriceError.vi;
          } else {
            _meetPriceError = PriceError.valid;
          }
        } else {
          final String _value = _controller60Minute.text.replaceAll(new RegExp(','), '');
          if ((double.tryParse(_value) ?? 0) < 0.5) {
            _meetPriceError = PriceError.en;
          } else {
            _meetPriceError = PriceError.valid;
          }
        }
      }

      if (_expert.timelinePriceModel != null) {
        timelines = _expert.timelinePriceModel!;

        timelines.forEach((timeline) {
          if (timeline.isRepeat == IS_REPEAT) {
            timeline.weeklySchedules.forEach((schedule) {
              datePicked.add(weeks[schedule.dayOfWeek == 0 ? 6 : schedule.dayOfWeek - 1]);
            });
          }
        });

        datePicked.sort((a, b) => a.compareTo(b));
      }
    }

    _controller15Minute.addListener(
      () {
        setState(
          () {
            {
              if (_controller15Minute.text == '') {
                _callPriceError = PriceError.empty;
              } else {
                if (_price15DropDown == _listPrice[0]) {
                  final String _value =
                      _controller15Minute.text.replaceAll(new RegExp(r'[^\w\s]+'), '');
                  if (double.parse(_value) < 12000) {
                    _callPriceError = PriceError.vi;
                  } else {
                    _callPriceError = PriceError.valid;
                  }
                } else {
                  final String _value = _controller15Minute.text.replaceAll(new RegExp(','), '');

                  if (double.parse(_value) < 0.5) {
                    _callPriceError = PriceError.en;
                  } else {
                    _callPriceError = PriceError.valid;
                  }
                }
              }
            }
          },
        );
      },
    );
    _controller60Minute.addListener(
      () {
        setState(
          () {
            {
              if (_controller60Minute.text == '') {
                _meetPriceError = PriceError.empty;
              } else {
                if (_price60DropDown == _listPrice[0]) {
                  final String _value =
                      _controller60Minute.text.replaceAll(new RegExp(r'[^\w\s]+'), '');
                  if (double.parse(_value) < 12000) {
                    _meetPriceError = PriceError.vi;
                  } else {
                    _meetPriceError = PriceError.valid;
                  }
                } else {
                  final String _value = _controller60Minute.text.replaceAll(new RegExp(','), '');
                  if ((double.tryParse(_value) ?? 0) < 0.5) {
                    _meetPriceError = PriceError.en;
                  } else {
                    _meetPriceError = PriceError.valid;
                  }
                }
              }
            }
          },
        );
      },
    );
  }

  String _buildPriceErrorText({required PriceError priceError}) {
    switch (priceError) {
      case PriceError.en:
        return Strings.priceMustOverHaflBuckErorrtext.i18n;
      case PriceError.vi:
        return Strings.priceMustbeOver12k.i18n;
      case PriceError.empty:
        return Strings.enterPriceErrortext.i18n;

      default:
        return '';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarTitleBack(context, Strings.priceOfASingleServiceAppbartext.i18n),
      body: Column(
        children: [
          Divider(
            height: 0.5.sp,
            color: colorDividerTimeline,
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: 100.w,
                      color: colorWhite,
                      padding: EdgeInsets.symmetric(horizontal: 18.sp, vertical: 15.sp),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          TextTitle(
                            textTitle: Strings.costOfASingleCallingServiceTitle.i18n,
                          ),
                          MoneyServiceWithTimeCard(
                            isVND: _price15DropDown == _listPrice[0],
                            margin: EdgeInsets.only(top: 10.sp),
                            textEditingController: _controller15Minute,
                            textMinute: Strings.fifteenMinutes.i18n,
                            onChangeTextForm: (value) => value!,
                            validatorTextForm: (val) {
                              if (val == null || val.isEmpty) {
                                return '';
                              } else {
                                if (_price15DropDown == _listPrice[0]) {
                                  final String _value = val.replaceAll(new RegExp(r'[^\w\s]+'), '');
                                  if (double.parse(_value) < 12000) {
                                    return '';
                                  }
                                } else {
                                  final String _value = val.replaceAll(new RegExp(','), '');

                                  if (double.parse(_value) < 0.5) {
                                    return '';
                                  }
                                }
                                return null;
                              }
                            },
                            onChangeDropDown: (value) {
                              if (value != _price15DropDown) {
                                setState(() {
                                  _price15DropDown = value!;
                                  _controller15Minute.text = '';
                                });
                              }
                              return _price15DropDown = value!;
                            },
                            hintDropdown: '',
                            valueDropDown: _price15DropDown,
                            listDropDown: _listPrice,
                          ),
                          Visibility(
                            visible: _callPriceError != PriceError.valid,
                            child: Row(
                              children: [
                                Image.asset(
                                  iconRedNoti,
                                  width: 16.sp,
                                  height: 16.sp,
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: 8.sp),
                                  child: Text(
                                    _buildPriceErrorText(
                                      priceError: _callPriceError,
                                    ),
                                    style: TextStyle(
                                      color: colorFinished,
                                      fontSize: 11.sp,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          SizedBox(height: 22.sp),
                          AnimatedSwitcher(
                            duration: const Duration(milliseconds: DURATION_DEFAULT_ANIMATION),
                            transitionBuilder: (Widget child, Animation<double> animation) {
                              return ScaleTransition(scale: animation, child: child);
                            },
                            child: _isDirectSupport
                                ? Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      TextTitle(
                                        textTitle: Strings.costOfASingleMeetingServiceTitle.i18n,
                                      ),
                                      MoneyServiceWithTimeCard(
                                        isVND: _price60DropDown == _listPrice[0],
                                        margin: EdgeInsets.only(top: 10.sp),
                                        textEditingController: _controller60Minute,
                                        onChangeTextForm: (value) => value!,
                                        textMinute: Strings.sixtyMinutes.i18n,
                                        hintDropdown: '',
                                        valueDropDown: _price60DropDown,
                                        validatorTextForm: (val) {
                                          if (val == null || val.isEmpty) {
                                            return '';
                                          } else {
                                            if (_price60DropDown == _listPrice[0]) {
                                              final String _value =
                                                  val.replaceAll(new RegExp(r'[^\w\s]+'), '');
                                              if (double.parse(_value) < 12000) {
                                                return '';
                                              }
                                            } else {
                                              final String _value =
                                                  val.replaceAll(new RegExp(','), '');

                                              if (double.parse(_value) < 0.5) {
                                                return '';
                                              }
                                            }
                                            return null;
                                          }
                                        },
                                        onChangeDropDown: (value) {
                                          if (value != _price60DropDown) {
                                            setState(() {
                                              _price60DropDown = value!;
                                              _controller60Minute.text = '';
                                            });
                                          }
                                          return value!;
                                        },
                                        listDropDown: _listPrice,
                                      ),
                                      Visibility(
                                        visible: _meetPriceError != PriceError.valid,
                                        child: Row(
                                          children: [
                                            Image.asset(
                                              iconRedNoti,
                                              width: 16.sp,
                                              height: 16.sp,
                                            ),
                                            Padding(
                                              padding: EdgeInsets.only(left: 8.sp),
                                              child: Text(
                                                _buildPriceErrorText(
                                                  priceError: _meetPriceError,
                                                ),
                                                style: TextStyle(
                                                  color: colorFinished,
                                                  fontSize: 11.sp,
                                                  fontWeight: FontWeight.w400,
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                      SizedBox(height: 20.sp),
                                    ],
                                  )
                                : SizedBox(),
                          ),
                          Container(
                            child: Text(
                              Strings.participantsCountSuggetion.i18n,
                              textAlign: TextAlign.justify,
                              style: TextStyle(
                                height: 1.sp,
                                fontStyle: FontStyle.italic,
                                fontSize: 12.sp,
                                color: colorFontGreen,
                              ),
                            ),
                          ),
                          SizedBox(height: 20.sp),
                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                SwitchButton(
                                  value: true,
                                  text: Strings.call.i18n,
                                ),
                                SizedBox(width: 20.sp),
                                SwitchButton(
                                  value: _isDirectSupport,
                                  text: Strings.meet.i18n,
                                  onToggle: (val) {
                                    setState(() {
                                      _isDirectSupport = val;
                                    });
                                  },
                                ),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(vertical: 28.sp),
                            child: dividerChat,
                          ),
                          TextTitle(
                            textTitle: Strings.availableTimeTitle.i18n,
                          ),
                          SizedBox(height: 12.sp),
                          Container(
                            child: Text(
                              Strings.availableTimeSelectionNoti.i18n,
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                height: 1.sp,
                                fontSize: 12.sp,
                                color: colorFontGreen,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 14.sp,
                          ),
                          ...timelines
                              .asMap()
                              .map(
                                (index, e) {
                                  return MapEntry(
                                      index,
                                      PeriodContainer(
                                        timeline: timelines[index],
                                        datePicked: datePicked,
                                        handleChange: (val) {
                                          setState(() {
                                            timelines[index] = val;
                                            _filterTimeline();
                                          });
                                        },
                                        handleChangeDate: (val) {
                                          setState(() {
                                            datePicked = val;
                                            datePicked = datePicked.toSet().toList();
                                            _filterTimeline();
                                          });
                                        },
                                      ));
                                },
                              )
                              .values
                              .toList(),
                          TouchableOpacity(
                            onTap: () {
                              if (datePicked.length == 7) {
                                dialogAnimationWrapper(
                                  slideFrom: SlideMode.bot,
                                  child: DialogWithTextAndPopButton(
                                    title: Strings.availableTimeNotValid.i18n,
                                    bodyAfter: Strings.availableTimeSelectionNoti.i18n,
                                  ),
                                );
                              } else {
                                List<DateTime> _nextWeeks = getCalendarThisWeek(
                                    dateTime:
                                        timelines.isEmpty || timelines.last.weeklySchedules.isEmpty
                                            ? DateTime.now()
                                            : timelines.last.weeklySchedules.last.date);
                                List<DateTime> _dateTimes = [];

                                _nextWeeks.forEach((date) {
                                  if (!datePicked
                                      .map((e) => e.weekday)
                                      .toSet()
                                      .toList()
                                      .contains(date.weekday == 7 ? 0 : date.weekday)) {
                                    _dateTimes.add(date);
                                    datePicked.add(date);
                                  }
                                });
                                TimelinePriceModel _timeline = TimelinePriceModel(
                                  id: DateTime.now().microsecondsSinceEpoch.toString(),
                                  isRepeat: IS_REPEAT,
                                  specialFreeDates: null,
                                  status: 1,
                                  weeklySchedules: _dateTimes
                                      .map(
                                        (date) => WeeklyScheduleModel(
                                          id: '',
                                          dayOfWeek: date.weekday == 7 ? 0 : date.weekday,
                                          date: date,
                                          timesMeet: [],
                                          timesCall: [],
                                        ),
                                      )
                                      .toList(),
                                );

                                setState(() {
                                  timelines.add(_timeline);
                                });
                              }
                            },
                            child: Container(
                              height: 40.sp,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(
                                  8.sp,
                                ),
                                border: Border.all(
                                  width: 0.5.sp,
                                  color: colorGray2,
                                ),
                                color: Colors.transparent,
                              ),
                              child: Center(
                                child: Text(
                                  Strings.addMoreAvailableDayWithPlus.i18n,
                                  style: text13w400cFontGreen,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: 10.sp)
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 16.sp),
                      child: ButtonPrimary(
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            if (timelines.isEmpty) {
                              dialogAnimationWrapper(
                                slideFrom: SlideMode.bot,
                                child: DialogWithTextAndPopButton(
                                  title: Strings.noTimeSelectedYet.i18n,
                                  bodyAfter: Strings.noTimeSelectedYetNoti.i18n,
                                ),
                              );
                            } else if (timelines.first.weeklySchedules.isEmpty) {
                              dialogAnimationWrapper(
                                slideFrom: SlideMode.bot,
                                child: DialogWithTextAndPopButton(
                                  title: Strings.noTimeSelectedYet.i18n,
                                  bodyAfter: Strings.noTimeSelectedYetNoti.i18n,
                                ),
                              );
                            } else if (timelines.first.weeklySchedules.first.timesCall.isEmpty) {
                              dialogAnimationWrapper(
                                slideFrom: SlideMode.bot,
                                child: DialogWithTextAndPopButton(
                                  title: Strings.noTimeSelectedYet.i18n,
                                  bodyAfter: Strings.noTimeSelectedYetNoti.i18n,
                                ),
                              );
                            } else {
                              showDialogLoading();
                              AppBloc.userBloc.add(
                                UpdateServicePriceEvent(
                                  callPrice: BudgetModel(
                                    currency: _price15DropDown.toLowerCase(),
                                    cost:
                                        double.parse(_controller15Minute.text.replaceAll(',', '')),
                                    totalMinutes: 60,
                                  ),
                                  meetPrice: BudgetModel(
                                    currency: _price60DropDown.toLowerCase(),
                                    cost: _isDirectSupport
                                        ? double.parse(_controller60Minute.text.replaceAll(',', ''))
                                        : 0.0,
                                    totalMinutes: 60,
                                  ),
                                  timelines: timelines,
                                ),
                              );
                            }
                          }
                        },
                        text: 'Lưu',
                      ),
                    ),
                    SizedBox(height: 28.sp),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  _filterTimeline() {
    List<DateTime> _dayAvailable = [];
    _dayAvailable.addAll(datePicked);
    List<int> _weekenAvailable =
        _dayAvailable.map((e) => e.weekday == 7 ? 0 : e.weekday).toList().toSet().toList();

    timelines.asMap().forEach((index, timeline) {
      if (timeline.isRepeat == IS_REPEAT) {
        List<WeeklyScheduleModel> timelineFormat = [];
        timeline.weeklySchedules.forEach((schedule) {
          int indexOfWeeken = _weekenAvailable.indexOf(schedule.dayOfWeek);
          if (indexOfWeeken != -1) {
            _weekenAvailable.removeAt(indexOfWeeken);
            timelineFormat.add(schedule);
          }
        });
        timelines[index].weeklySchedules = [];
        timelines[index].weeklySchedules.addAll(timelineFormat);
      }
    });

    timelines = timelines.where((timeline) => timeline.weeklySchedules.isNotEmpty).toList();
  }
}
