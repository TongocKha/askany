// ignore_for_file: unused_local_variable

import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/category/category_bloc.dart';
import 'package:askany/src/bloc/service_managament/service_management_bloc.dart';
import 'package:askany/src/configs/lang/language_service.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class LanguageScreen extends StatefulWidget {
  @override
  _LanguageScreenState createState() => _LanguageScreenState();
}

class _LanguageScreenState extends State<LanguageScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarTitleBack(
        context,
        Strings.languages.i18n,
      ),
      body: SafeArea(
        child: Container(
          margin: EdgeInsets.only(left: 8.sp, right: 16.sp, bottom: 30.sp),
          child: Column(
            children: [
              SizedBox(height: 18.sp),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 10.sp),
                child: Column(
                  children: [
                    TouchableOpacity(
                      onTap: () {
                        LanguageService().changeLanguage();
                        AppBloc.serviceManagamentBloc
                            .add(GetCancelServiceReasonsEvent());
                        AppBloc.categoryBloc.add(CleanCategoriesInfoEvent());
                        AppBloc.categoryBloc.add(GetCategoriesInfoEvent());
                      },
                      child: Container(
                        color: Colors.transparent,
                        child: Row(
                          children: [
                            Icon(
                              LanguageService.getIsLanguage('vi')
                                  ? Icons.check
                                  : null,
                              size: 18.sp,
                              color: Color(0xff2B6B64),
                            ),
                            SizedBox(
                              width: 10.sp,
                            ),
                            Text(
                              Strings.vietnamese.i18n,
                              style: TextStyle(
                                  fontSize: 13.sp, color: Colors.black),
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 12.sp),
                      child: dividerChat,
                    ),
                    TouchableOpacity(
                      onTap: () {
                        LanguageService().changeLanguage(isEnglish: true);
                        AppBloc.serviceManagamentBloc
                            .add(GetCancelServiceReasonsEvent());
                        AppBloc.categoryBloc.add(CleanCategoriesInfoEvent());
                        AppBloc.categoryBloc.add(GetCategoriesInfoEvent());
                      },
                      child: Container(
                        color: Colors.transparent,
                        child: Row(
                          children: [
                            Icon(
                              LanguageService.getIsLanguage('en')
                                  ? Icons.check
                                  : null,
                              size: 18.sp,
                              color: Color(0xff2B6B64),
                            ),
                            SizedBox(
                              width: 10.sp,
                            ),
                            Text(
                              Strings.english.i18n,
                              style: TextStyle(
                                  fontSize: 13.sp, color: Colors.black),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
