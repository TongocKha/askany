import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class AccountVerificationScreen extends StatefulWidget {
  const AccountVerificationScreen({Key? key}) : super(key: key);

  @override
  _AccountVerificationScreenState createState() => _AccountVerificationScreenState();
}

class _AccountVerificationScreenState extends State<AccountVerificationScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarTitleBack(context, Strings.verifyAccountAppbartext.i18n),
      body: SafeArea(
        child: Column(
          children: [
            dividerThinkness6,
            Container(
              padding: EdgeInsets.only(left: 16.sp, right: 16.sp, bottom: 30.sp),
              child: Column(
                children: [
                  Column(
                    children: [
                      SizedBox(
                        height: 15.sp,
                      ),
                      TouchableOpacity(
                        onTap: () {
                          AppNavigator.push(Routes.ACCOUNT_VERIFICATION_EMAIL);
                        },
                        child: Container(
                          color: Colors.transparent,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                Strings.verifyEmailAppbarText.i18n,
                                style: TextStyle(
                                  color: colorBlack2,
                                  fontSize: 13.sp,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                              Image.asset(
                                'assets/icons/ic_next.png',
                                width: 7.sp,
                              )
                            ],
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 15.sp, bottom: 15.sp),
                        child: dividerChat,
                      ),
                      TouchableOpacity(
                        onTap: () {
                          AppNavigator.push(Routes.ACCOUNT_VERIFICATION_PHONE);
                        },
                        child: Container(
                          color: Colors.transparent,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                Strings.verifyYourPhoneAppbarText.i18n,
                                style: TextStyle(
                                    color: colorBlack2,
                                    fontSize: 13.sp,
                                    fontWeight: FontWeight.w600),
                              ),
                              Image.asset(
                                'assets/icons/ic_next.png',
                                width: 7.sp,
                              )
                            ],
                          ),
                        ),
                      ),
                      Visibility(
                        visible: AppBloc.userBloc.getAccount.isExpert!,
                        child: Container(
                          margin: EdgeInsets.only(top: 15.sp, bottom: 15.sp),
                          child: dividerChat,
                        ),
                      ),
                      Visibility(
                        visible: AppBloc.userBloc.getAccount.isExpert!,
                        child: TouchableOpacity(
                          onTap: () {
                            AppNavigator.push(Routes.ACCOUNT_VERTIFY_COMPANY_POSITTION);
                          },
                          child: Container(
                            color: Colors.transparent,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  Strings.verifyYourCompanyAndPositionAppbartext.i18n,
                                  style: TextStyle(
                                      color: colorBlack2,
                                      fontSize: 13.sp,
                                      fontWeight: FontWeight.w600),
                                ),
                                Image.asset(
                                  'assets/icons/ic_next.png',
                                  width: 7.sp,
                                )
                              ],
                            ),
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
