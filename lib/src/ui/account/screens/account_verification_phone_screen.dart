import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/authentication/authentication_event.dart';
import 'package:askany/src/bloc/user/user_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class AccountVerificationPhoneScreen extends StatefulWidget {
  @override
  _AccountVerificationPhoneScreenState createState() =>
      _AccountVerificationPhoneScreenState();
}

class _AccountVerificationPhoneScreenState
    extends State<AccountVerificationPhoneScreen> {
  bool _isVerifyPhone = AppBloc.userBloc.getAccount.isVerifyPhone ?? false;
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserBloc, UserState>(builder: (context, state) {
      if (state is UserGetDone) {
        return Scaffold(
          appBar:
              appBarTitleBack(context, Strings.verifyYourPhoneAppbarText.i18n),
          body: SafeArea(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16.sp),
              child: Column(
                children: [
                  SizedBox(height: 130.sp),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                     
                      _isVerifyPhone == false
                          ? Container(
                              width: double.infinity,
                              alignment: Alignment.center,
                              margin: EdgeInsets.symmetric(horizontal: 12.sp),
                              child: Text(
                                Strings.verifyPhoneSuggestion.i18n,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 12.5.sp,
                                  color: colorBlack2,
                                  height: 1.35.sp,
                                ),
                              ),
                            )
                          : Container(
                              width: double.infinity,
                              alignment: Alignment.center,
                              child: Column(
                                children: [
                                  Image.asset(
                                    imagePhoneVerified,
                                    width: 50.sp,
                                  ),
                                  SizedBox(height: 25.sp),
                                  Text(
                                    state.accountModel.phone.toString(),
                                    style: TextStyle(
                                      color: colorBlack1,
                                      fontWeight: FontWeight.w600,
                                      fontSize: 15.sp,
                                    ),
                                  ),
                                  SizedBox(height: 8.sp),
                                  Text(
                                    Strings.yourPhoneNumberHasBeenVerified.i18n,
                                    style: TextStyle(
                                      fontSize: 12.sp,
                                      fontWeight: FontWeight.w400,
                                      color: colorGray1,
                                    ),
                                  )
                                ],
                              ),
                            ),
                      SizedBox(height: 35.sp),
                       state.accountModel.phone != null &&  state.accountModel.phone!.isNotEmpty ?
                      _isVerifyPhone == false
                          ? Container(
                              margin: EdgeInsets.symmetric(horizontal: 25.sp),
                              child: ButtonPrimary(
                                onPressed: () async {
                                  await AppNavigator.push(
                                      Routes.VERIFICATION_OTP,
                                      arguments: {
                                        'phone':
                                            state.accountModel.phone.toString()
                                      });
                                  AppBloc.authBloc.add(
                                    VerifyPhoneEvent(),
                                  );
                                },
                                text: Strings.verifyNow.i18n,
                              ),
                            )
                          : Container(
                              margin: EdgeInsets.symmetric(horizontal: 5.sp),
                              child: ButtonPrimary(
                                onPressed: () async {
                                  await AppNavigator.push(Routes.CHANGE_PHONE);
                                },
                                text: Strings.changeYourPhoneNumber.i18n,
                              ),
                            ):Container(
                              margin: EdgeInsets.symmetric(horizontal: 5.sp),
                              child: ButtonPrimary(
                                onPressed: () async {
                                  await AppNavigator.push(Routes.CHANGE_PHONE);
                                },
                                text: Strings.changeYourPhoneNumber.i18n,
                              ),
                            ) ,
                    ],
                  )
                ],
              ),
            ),
          ),
        );
      }
      return Scaffold(
        body: Container(),
      );
    });
  }
}
