import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/user/user_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/models/expert_model.dart';
import 'package:askany/src/models/language_model.dart';
import 'package:askany/src/models/position_model.dart';
import 'package:askany/src/models/specialty_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/scaffold_wrapper.dart';
import 'package:askany/src/ui/account/widgets/bottom_sheet_position.dart';
import 'package:askany/src/ui/authentication/screens/register_expert/widgets/language_bottom_sheet.dart';
import 'package:askany/src/ui/authentication/screens/register_expert/widgets/specialty_bottom_sheet.dart';
import 'package:askany/src/ui/authentication/screens/register_expert/widgets/title_input_register.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_confirm_cancel.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_form.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_form_request.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class InfoExpertScreen extends StatefulWidget {
  @override
  State<InfoExpertScreen> createState() => _InfoExpertScreenState();
}

class _InfoExpertScreenState extends State<InfoExpertScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _yearExperienceController = TextEditingController();
  TextEditingController _companyNameController = TextEditingController();
  TextEditingController _descriptionController = TextEditingController();
  List<LanguageModel> _languages = [];
  List<SpecialtyModel> _specialties = [];
  PositionModel? _positionModel;
  String _enteredText = '';
  TextEditingController _positionController = TextEditingController();

  @override
  void initState() {
    super.initState();
    if (AppBloc.userBloc.expertModel != null) {
      ExpertModel expert = AppBloc.userBloc.expertModel!;
      // Position
      int indexOfPosition = AppBloc.skillBloc.positions.indexWhere(
        (position) => position.id == AppBloc.userBloc.expertModel?.highestPosition,
      );

      if (indexOfPosition != -1) {
        _positionModel = AppBloc.skillBloc.positions[indexOfPosition];
        _positionController.text = _positionModel?.getLocaleTitle ?? '';
      }

      // Others
      _yearExperienceController.text = expert.experienceYears.toString();
      _companyNameController.text = expert.companyName;
      _descriptionController.text = expert.introduce;
      _enteredText = expert.introduce;
      _languages.addAll(expert.languages ?? []);
      _specialties.addAll(expert.specialties);
    }
  }

  Future<void> onBackPressed() {
    return dialogAnimationWrapper(
      child: DialogConfirmCancel(
        cancelText: 'Huỷ',
        bodyBefore:
            'Bạn có chắc chắn muốn rời khỏi màn hình này, thông tin bạn nhập sẽ không được lưu lại?',
        onConfirmed: () {
          AppNavigator.pop();
          AppNavigator.pop();
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ScaffoldWrapper(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        extendBodyBehindAppBar: true,
        extendBody: true,
        appBar: appBarTitleBack(context, Strings.expertInfomationAppbartext.i18n,
            onBackPressed: onBackPressed),
        body: SafeArea(
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                dividerChat,
                Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        SizedBox(height: 17.sp),
                        Container(
                          alignment: Alignment.topLeft,
                          margin: EdgeInsets.symmetric(horizontal: 16.sp),
                          child: Column(
                            children: [
                              Container(
                                alignment: Alignment.topLeft,
                                child: TitleInputRegister(text: Strings.howManyLanguages.i18n),
                              ),
                              Container(
                                alignment: Alignment.topLeft,
                                child: Visibility(
                                  visible: _languages.isNotEmpty,
                                  child: Padding(
                                    padding: EdgeInsets.only(top: 10.sp),
                                    child: Wrap(
                                      spacing: 6.0,
                                      runSpacing: 6.0,
                                      children: List.generate(_languages.length, (index) {
                                        return _itemPicked(
                                          text: _languages[index].getLocaleTitle,
                                          handleDelete: () {
                                            setState(() {
                                              _languages.removeAt(index);
                                            });
                                          },
                                        );
                                      }),
                                    ),
                                  ),
                                ),
                              ),
                              TouchableOpacity(
                                onTap: () {
                                  showModalBottomSheet<void>(
                                    context: context,
                                    backgroundColor: Colors.transparent,
                                    isScrollControlled: true,
                                    builder: (context) {
                                      return LanguageBottomSheet(
                                        languageModel: _languages,
                                        handleFinished: (result) {
                                          setState(() {
                                            _languages = result;
                                          });
                                        },
                                      );
                                    },
                                  );
                                },
                                child: TextFieldForm(
                                  initialValue: Strings.languages.i18n,
                                  autovalidateMode: AutovalidateMode.always,
                                  isActive: false,
                                  contentPadding: EdgeInsets.all(10.sp),
                                  fontSize: 13.sp,
                                  validatorForm: (val) => _languages.isEmpty
                                      ? Strings.blankLanguageErrortext.i18n
                                      : null,
                                  suffixIcon: Image.asset(
                                    iconArrowDown,
                                    width: 10.sp,
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 21.sp,
                              ),
                              Container(
                                alignment: Alignment.topLeft,
                                child: TitleInputRegister(
                                  text: Strings.whatSectorYouHaveExprence.i18n,
                                ),
                              ),
                              Container(
                                alignment: Alignment.topLeft,
                                child: Visibility(
                                  visible: _specialties.isNotEmpty,
                                  child: Padding(
                                    padding: EdgeInsets.only(top: 10.sp),
                                    child: Wrap(
                                      spacing: 6.0,
                                      runSpacing: 6.0,
                                      children: List.generate(_specialties.length, (index) {
                                        return _itemPicked(
                                          text: _specialties[index].getLocaleTitle ?? '',
                                          handleDelete: () {
                                            setState(() {
                                              _specialties.removeAt(index);
                                            });
                                          },
                                        );
                                      }),
                                    ),
                                  ),
                                ),
                              ),
                              TouchableOpacity(
                                onTap: () {
                                  showModalBottomSheet<void>(
                                    context: context,
                                    backgroundColor: Colors.transparent,
                                    isScrollControlled: true,
                                    builder: (context) {
                                      return SpecialtyBottomSheet(
                                        title: Strings.sectorOfExpertise.i18n,
                                        specialtyModel: _specialties,
                                        handleFinished: (result) {
                                          setState(() {
                                            _specialties = result;
                                          });
                                        },
                                      );
                                    },
                                  );
                                },
                                child: TextFieldForm(
                                  initialValue: Strings.sector.i18n,
                                  autovalidateMode: AutovalidateMode.always,
                                  isActive: false,
                                  contentPadding: EdgeInsets.all(10.sp),
                                  fontSize: 13.sp,
                                  validatorForm: (val) => _specialties.isEmpty
                                      ? Strings.sectorCannotBeLeftBlankErrortext.i18n
                                      : null,
                                  suffixIcon: Image.asset(
                                    iconArrowDown,
                                    width: 10.sp,
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 21.sp,
                              ),
                              Container(
                                alignment: Alignment.topLeft,
                                child: TitleInputRegister(
                                  text: Strings.howManyYearsOfExprience.i18n,
                                ),
                              ),
                              TextFieldFormRequest(
                                controller: _yearExperienceController,
                                hintText: Strings.yearsCountOfExperienceHinttext.i18n,
                                textInputType: TextInputType.number,
                                inputFormatters: [
                                  LengthLimitingTextInputFormatter(2),
                                ],
                                validatorForm: (val) => val!.isEmpty || val.trim().length == 0
                                    ? Strings.yearsCountOfExperienceCannotBeBlankErrortext.i18n
                                    : int.parse(_yearExperienceController.text) == 0
                                        ? 'Số kinh nghiệm phải hơn 1 năm'
                                        : int.parse(_yearExperienceController.text) > 40
                                            ? 'Giới hạn số năm kinh nghiệm là 40'
                                            : null,
                              ),
                              SizedBox(
                                height: 21.sp,
                              ),
                              Container(
                                alignment: Alignment.topLeft,
                                child: TitleInputRegister(
                                  text: Strings.whatYourHighestPositionTitle.i18n,
                                ),
                              ),
                              TextFieldForm(
                                onTap: () {
                                  showModalBottomSheet(
                                    context: context,
                                    isScrollControlled: true,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.vertical(
                                        top: Radius.circular(20.sp),
                                      ),
                                    ),
                                    builder: (context) => BottomSheetPosition(
                                      handlePressed: (val) {
                                        int indexOfPosition = AppBloc.skillBloc.positions
                                            .indexWhere((item) => item.getLocaleTitle == val);

                                        if (indexOfPosition != -1) {
                                          _positionModel =
                                              AppBloc.skillBloc.positions[indexOfPosition];
                                          setState(() {
                                            _positionController.text =
                                                _positionModel?.getLocaleTitle ?? '';
                                          });
                                        }
                                      },
                                      listPosition: AppBloc.skillBloc.positions
                                          .map((item) => item.getLocaleTitle!)
                                          .toList(),
                                      position: _positionModel?.getLocaleTitle,
                                    ),
                                  );
                                },
                                controller: _positionController,
                                hintText: Strings.selectYourHighestPositionHintext.i18n,
                                readOnly: true,
                                suffixIcon: Container(
                                  color: Colors.transparent,
                                  child: Image.asset(
                                    iconArrowDown,
                                    width: 10.sp,
                                  ),
                                ),
                                validatorForm: (val) => _positionModel == null
                                    ? Strings.positionCannotBeLeftBlankErrortext.i18n
                                    : null,
                              ),
                              SizedBox(
                                height: 21.sp,
                              ),
                              Container(
                                alignment: Alignment.topLeft,
                                child: TitleInputRegister(
                                    text: Strings.yourCompanyYouWorkForTitle.i18n),
                              ),
                              TextFieldFormRequest(
                                controller: _companyNameController,
                                hintText: Strings.companyNameHintext.i18n,
                                validatorForm: (val) => val!.isEmpty || val.trim().length == 0
                                    ? Strings.companyNameCannotBeLeftBlankErrortext.i18n
                                    : null,
                              ),
                              SizedBox(
                                height: 21.sp,
                              ),
                              Container(
                                alignment: Alignment.topLeft,
                                child: TitleInputRegister(
                                    text: Strings.descriptionAboutYourExperienceAndSkills.i18n),
                              ),
                              SizedBox(height: 10.sp),
                              Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(6.sp),
                                  border: Border.all(
                                    color: colorBorderTextField,
                                    width: 0.5.sp,
                                  ),
                                ),
                                child: Column(
                                  children: [
                                    SizedBox(height: 4.sp),
                                    TextFieldFormRequest(
                                      controller: _descriptionController,
                                      maxLines: 7,
                                      maxLength: 300,
                                      hintText: Strings.enterDescriptionAboutYouHintext.i18n,
                                      validatorForm: (val) => val!.isEmpty
                                          ? Strings
                                              .descriptionAboutYouCannotBeLeftBlankErrortext.i18n
                                          : null,
                                      onChanged: (value) {
                                        setState(() {
                                          _enteredText = value;
                                        });
                                      },
                                      inputFormatters: [
                                        LengthLimitingTextInputFormatter(300),
                                      ],
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(
                                        right: 12.sp,
                                        bottom: 8.sp,
                                      ),
                                      alignment: Alignment.bottomRight,
                                      child: Text(
                                        '${_enteredText.length.toString()}/300',
                                        style: TextStyle(
                                          color: colorGray2,
                                          fontSize: 11.sp,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 27.sp,
                              ),
                              ButtonPrimary(
                                onPressed: () async {
                                  if (_formKey.currentState!.validate()) {
                                    showDialogLoading();
                                    AppBloc.userBloc.add(
                                      UpdateInfoExpertEvent(
                                        languagues: _languages,
                                        specialties: _specialties,
                                        experienceYears: _yearExperienceController.text,
                                        positionId: _positionModel!.id,
                                        companyName: _companyNameController.text,
                                        introduce: _descriptionController.text,
                                      ),
                                    );
                                  }
                                },
                                text: Strings.save.i18n,
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 50.sp,
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _itemPicked({required String text, required Function handleDelete}) {
    return Container(
      decoration: BoxDecoration(
        color: colorGreen2,
        borderRadius: BorderRadius.circular(6.5.sp),
      ),
      padding: EdgeInsets.symmetric(
        vertical: 8.sp,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(width: 12.sp),
          Text(
            text,
            style: TextStyle(
              fontSize: 12.sp,
              color: colorWhiteCard,
            ),
          ),
          SizedBox(width: 8.sp),
          TouchableOpacity(
            onTap: () {
              handleDelete();
            },
            child: Image.asset(
              iconRemove,
              width: 11.25.sp,
              height: 11.25.sp,
              color: colorWhiteCard,
            ),
          ),
          SizedBox(width: 8.sp),
        ],
      ),
    );
  }
}
