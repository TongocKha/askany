import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_form.dart';
import 'package:askany/src/ui/style/calendar_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ChangePhoneScreen extends StatefulWidget {
  @override
  _ChangePhoneScreenState createState() => _ChangePhoneScreenState();
}

class _ChangePhoneScreenState extends State<ChangePhoneScreen> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String textPhone = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: appBarTitleBack(
        context,
        Strings.changePhoneNumberAppbarText.i18n,
      ),
      body: SafeArea(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 16.sp),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                SizedBox(
                  height: 30.sp,
                ),
                Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    Strings.enterPhoneNumberSuggestion.i18n,
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                      color: colorBlack2,
                      fontSize: 13.sp,
                      fontWeight: FontWeight.w400,
                      height: 1.25.sp,
                    ),
                  ),
                ),
                SizedBox(height: 10.sp),
                TextFieldForm(
                  textInputType: TextInputType.phone,
                  inputFormatters: [LengthLimitingTextInputFormatter(10)],
                  hintText: Strings.phoneNumberHintext.i18n,
                  initialValue: '',
                  validatorForm: (val) => val!.trim().length < 9
                      ? Strings
                          .phoneNumberMustBeAtLeast9CharactersErrortext.i18n
                      : null,
                  onChanged: (val) => textPhone = val.toString().trim(),
                ),
                SizedBox(
                  height: 20.sp,
                ),
                ButtonPrimary(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      AppNavigator.push(Routes.VERIFICATION_OTP,
                          arguments: {'phone': textPhone});
                    }
                  },
                  text: Strings.next.i18n,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
