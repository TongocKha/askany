import 'package:askany/src/bloc/user/user_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/account/widgets/button_select.dart';
import 'package:askany/src/ui/account/widgets/container_manage_account.dart';
import 'package:askany/src/ui/account/widgets/manage_service_area.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_none.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_message.dart';
import 'package:askany/src/ui/common/widgets/custom_image/network_image/cached_image.dart';
import 'package:askany/src/ui/common/widgets/text_ui/title_and_seemore.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:askany/src/helpers/extentions/string_extention.dart';

class AccountScreen extends StatefulWidget {
  @override
  _AccountScreenState createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  bool _verified = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserBloc, UserState>(
      builder: (context, state) {
        if (state is UserGetDone) {
          return Scaffold(
            extendBodyBehindAppBar: true,
            appBar: appBarBrighnessDark(),
            body: Stack(
              children: [
                Column(
                  children: [
                    Container(
                      width: double.infinity,
                      height: 150.sp,
                      decoration: BoxDecoration(color: headerCalendarColor),
                      padding: EdgeInsets.only(top: 20.sp, bottom: 26.sp),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                CustomNetworkImage(
                                  width: 62.sp,
                                  height: 62.sp,
                                  margin: EdgeInsets.only(left: 16.sp),
                                  urlToImage:
                                      state.accountModel.avatar?.urlToImage,
                                ),
                                SizedBox(width: 14.sp),
                                Container(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        state.accountModel.fullname!
                                            .formatName(),
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 16.sp,
                                          fontWeight: FontWeight.w700,
                                        ),
                                      ),
                                      SizedBox(height: 7.sp),
                                      Row(
                                        children: [
                                          Image.asset(
                                            'assets/icons/ic_verified.png',
                                            width: 10.sp,
                                          ),
                                          SizedBox(
                                            width: 5.sp,
                                          ),
                                          _verified == true
                                              ? Text(
                                                  Strings.verifiedAccount.i18n,
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 11.sp,
                                                  ),
                                                )
                                              : Text(
                                                  Strings
                                                      .unVerifiedAccount.i18n,
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 11.sp,
                                                  ),
                                                ),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          ButtonMessage(
                            iconHeader: iconMessageHeader,
                            padding: EdgeInsets.only(
                              right: 16.sp,
                              bottom: 20.sp,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 55.sp),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 16.sp),
                            child: Column(
                              children: [
                                TitleAndSeeMore(
                                  title: Strings.myServiceManagementTitle.i18n,
                                  handlePressed: () {
                                    AppNavigator.push(Routes.MY_SERVICE,
                                        arguments: {
                                          'index': 0,
                                        });
                                  },
                                ),
                                SizedBox(height: 15.sp),
                                ManageServiceArea(),
                                SizedBox(height: 16.sp),
                              ],
                            ),
                          ),
                          Visibility(
                            visible: state.accountModel.isExpert!,
                            child: dividerThinkness6NotMargin,
                          ),
                          Expanded(
                            child: SingleChildScrollView(
                              physics: BouncingScrollPhysics(),
                              child: Column(
                                children: [
                                  Visibility(
                                    visible: state.accountModel.isExpert!,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        ButtonSelect(
                                          content: Strings.expertProfile.i18n,
                                          icon: iconUserSkill,
                                          handlePressed: () {
                                            AppNavigator.push(
                                                Routes.PROFILE_EXPERT);
                                          },
                                        ),
                                        SizedBox(height: 4.sp),
                                      ],
                                    ),
                                  ),
                                  dividerThinkness6NotMargin,
                                  ButtonSelect(
                                    content: Strings.personalInfor.i18n,
                                    icon: iconInfo,
                                    handlePressed: () {
                                      AppNavigator.push(Routes.EDIT_PROFILE);
                                    },
                                  ),
                                  ButtonSelect(
                                    content: Strings.changePassword.i18n,
                                    icon: iconChangePassword,
                                    handlePressed: () {
                                      AppNavigator.push(Routes.CHANGE_PASSWORD);
                                    },
                                  ),
                                  ButtonSelect(
                                    content:
                                        Strings.verifyAccountAppbartext.i18n,
                                    icon: iconVerifyAccount,
                                    handlePressed: () {
                                      AppNavigator.push(
                                          Routes.ACCOUNT_VERIFICATION);
                                    },
                                  ),
                                  ButtonSelect(
                                    content: Strings.languages.i18n,
                                    icon: iconLanguages,
                                    handlePressed: () {
                                      AppNavigator.push(Routes.LANGUAGE);
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Positioned(
                  top: 150.sp - 37.sp,
                  left: 0,
                  right: 0,
                  child: Container(
                    height: 74.sp,
                    width: 100.w,
                    margin: EdgeInsets.symmetric(horizontal: 16.sp),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(12.sp),
                      boxShadow: [
                        BoxShadow(
                          offset: Offset(1, 1),
                          blurRadius: 4.sp,
                          color: Colors.black.withOpacity(.08),
                        ),
                      ],
                    ),
                    child: ContainerManageAccount(),
                  ),
                ),
              ],
            ),
          );
        }
        return Scaffold(
          body: Container(),
        );
      },
    );
  }
}
