import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/service_managament/service_management_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/models/offer_model.dart';
import 'package:askany/src/models/slide_mode.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_confirm_cancel.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/helpers/date_time_helper.dart';
import 'package:askany/src/models/request_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';

class ServiceManagamentCard extends StatelessWidget {
  ServiceManagamentCard({
    Key? key,
    required this.service,
    required this.tabNumber,
    this.isFirst = false,
    required this.indexOfOffer,
  }) : super(key: key);
  final RequestModel service;
  final int tabNumber;
  final bool isFirst;
  final int indexOfOffer;
  Widget buildTextByOfferStatus(
      {required int tabNumber, required OfferModel? myOffer}) {
    int offerStatus = myOffer?.status ?? -100;
    switch (offerStatus) {
      case WAIT_FOR_CONFIRM:
        return Text(
          Strings.confirming.i18n,
          style: text11w600cGreen,
        );
      case WAIT_FOR_PAYMENT:
        return Text(
          Strings.waitForPayment.i18n,
          style: text11w600cRed,
        );
      case CONFIRMED:
        return Text(
          (myOffer!.endTime.isBefore(DateTime.now()) && tabNumber != 2)
              ? Strings.advised.i18n
              : Strings.confirmed.i18n,
          style: text11w600cBlue,
        );
      case COMPLETED:
        return Text(
          Strings.completed.i18n,
          style: text11w600cGreen2,
        );
      case CANCELED:
        return Text(
          Strings.canceled.i18n,
          style: text11w600cRed,
        );
      case REPORTED:
        return buildReportText(
          reportStatus: service.report?.status ?? 0,
        );
      case ADVISED:
        return Text(
          Strings.advised.i18n,
          style: text11w600cBlue,
        );

      default:
        return SizedBox(
          width: 25.sp,
        );
    }
  }

  Widget buildReportText({required int reportStatus}) {
    if (reportStatus == 0) {
      return Text(
        Strings.reportProcessing.i18n,
        style: text11w600cRed,
      );
    } else {
      return Text(
        Strings.rpClosed.i18n,
        style: text11w600cGreen,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return TouchableOpacity(
      onTap: () {
        AppNavigator.push(
          Routes.DETAILS_SERVICE,
          arguments: {
            'requestModel': service,
          },
        );
      },
      child: Container(
        child: Column(
          children: [
            isFirst ? SizedBox() : dividerThinkness6NotMargin,
            Container(
              color: colorWhite,
              padding: EdgeInsets.symmetric(horizontal: 18.sp, vertical: 20.sp),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.only(bottom: 10.sp),
                          child: Container(
                            child: Text(
                              service.title,
                              maxLines: 2,
                              style: text13w600cBlack1,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 10.w,
                      ),
                      UserLocal().getIsExpert() &&
                              tabNumber == SERVICE_CONFIRMING
                          ? TouchableOpacity(
                              onTap: () {
                                dialogAnimationWrapper(
                                  slideFrom: SlideMode.bot,
                                  child: DialogConfirmCancel(
                                    bodyBefore: Strings
                                            .areYouSureYouWouldLikeToConsultService
                                            .i18n +
                                        '${service.title}?',
                                    cancelText: Strings.cancel.i18n,
                                    onConfirmed: () {
                                      showDialogLoading();
                                      AppBloc.serviceManagamentBloc.add(
                                        ExpertConfirmOfferEvent(
                                          offerId: service.myOfferId.toString(),
                                          requestId: service.id!,
                                        ),
                                      );
                                    },
                                  ),
                                );
                              },
                              child: Container(
                                width: 80.sp,
                                height: 32.sp,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8.sp),
                                    color: colorFontGreen),
                                child: Center(
                                  child: Text(
                                    Strings.confirm.i18n,
                                    style: text11w600cWhite,
                                  ),
                                ),
                              ),
                            )
                          : buildTextByOfferStatus(
                              tabNumber: tabNumber,
                              myOffer: service.myOffer,
                            )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        height: 24.sp,
                        child: Text(
                          Strings.expert.i18n +
                              ': ${service.authorExpert?.fullname}',
                          style: text12w400cGrey1,
                        ),
                      )
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 4.sp, bottom: 10.sp),
                    child: Divider(
                      height: 0.5.sp,
                      thickness: 0.5.sp,
                      color: colorGrey3,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: 9.sp,
                        padding: EdgeInsets.only(right: 4.sp),
                        child: Image.asset(
                          iconDot,
                          width: 5.sp,
                          height: 5.sp,
                        ),
                      ),
                      Container(
                        // height: 22.sp,
                        child: service.contactForm == CONTACT_CALL
                            ? Text(
                                Strings.callingPackage.i18n,
                                style: text11w400cGrey1,
                              )
                            : Text(
                                Strings.meetingPackage.i18n,
                                style: text11w400cGrey1,
                              ),
                      ),
                      Spacer(),
                      Container(
                        width: 9.sp,
                        padding: EdgeInsets.only(right: 4.sp),
                        child: Image.asset(
                          iconDot,
                          width: 5.sp,
                          height: 5.sp,
                        ),
                      ),
                      Text(
                        DateFormat('dd/MM/yyyy').format(service.startTime!) +
                            ' - ' +
                            getTimeString(service.startTime!) +
                            ' - ' +
                            getTimeString(service.endTime!),
                        style: text11w400cGrey1,
                      ),
                    ],
                  ),
                  Visibility(
                    visible: service.contactForm == CONTACT_MEET &&
                        service.locationName.isNotEmpty,
                    child: Container(
                      padding: EdgeInsets.only(top: 6.sp),
                      alignment: Alignment.centerLeft,
                      child: Text(
                        Strings.place.i18n +
                            ': ${service.locationName} - ${service.locationAddress}',
                        style: text11w400cGrey1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
