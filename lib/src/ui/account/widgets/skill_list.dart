import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/skill/skill_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/models/skill_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/account/widgets/skill_card.dart';
import 'package:askany/src/ui/account/widgets/skill_shimmer_card.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_with_image_text_and_pop_button.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:askany/src/ui/common/widgets/empty/empty.dart';
import 'package:askany/src/ui/common/widgets/pagination_list_view.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class SkillList extends StatefulWidget {
  const SkillList({Key? key}) : super(key: key);

  @override
  _SkillListState createState() => _SkillListState();
}

class _SkillListState extends State<SkillList> {
  @override
  void initState() {
    super.initState();
    AppBloc.skillBloc.add(CleanSkillEvent());
    AppBloc.skillBloc
        .add(GetSkillEvent(expertId: AppBloc.userBloc.getAccount.id!));
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SkillBloc, SkillState>(
      builder: (context, state) {
        if (state is GettingSkillFinished || state is GetDoneSkillFinished) {
          List<SkillModel> skills = state.props[0] as List<SkillModel>;
          return skills.length > 0
              ? PaginationListView(
                  itemCount: skills.length + 1,
                  callBackLoadMore: () => null,
                  callBackRefresh: (Function handleFinished) {
                    AppBloc.skillBloc.add(
                      RefreshSkillEvent(
                        handleFinished: handleFinished,
                      ),
                    );
                  },
                  padding: EdgeInsets.only(bottom: 48.sp),
                  isLoadMore: state is GettingSkillFinished,
                  childShimmer: Container(),
                  itemBuilder: (context, index) {
                    return index == skills.length
                        ? TouchableOpacity(
                            onTap: () {
                              dialogAnimationWrapper(
                                child: DialogWithImageTextAndPopButton(
                                  image: 'assets/icons/ic_popup_skill.png',
                                  bodyAlign: TextAlign.center,
                                  bodyColor: colorBlack1,
                                  bodyAfter: Strings.bodyOfAddSkillDialog.i18n,
                                  route: Routes.ADD_SKILL,
                                ),
                              );
                            },
                            child: Container(
                              margin: EdgeInsets.only(top: 12.sp),
                              child: Text(
                                Strings.addMoreSkillsWithPlus.i18n,
                                style: TextStyle(
                                  color: colorFontGreen,
                                  fontSize: 13.sp,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                          )
                        : SkillCard(
                            title: skills[index].name,
                            price: skills[index].callPrice!.costString,
                            time: skills[index].meetPrice != null
                                ? skills[index]
                                    .meetPrice!
                                    .totalMinutes
                                    .toString()
                                : skills[index]
                                    .callPrice!
                                    .totalMinutes
                                    .toString(),
                            bullet1: skills[index].expExperience.toString() +
                                (skills[index].expExperience >= 2
                                    ? Strings.yearsOfExperience.i18n
                                    : Strings.yearOfExperience.i18n),
                            bullet2: skills[index].highestPosition != null
                                ? skills[index].highestPosition!.getLocaleTitle!
                                : '',
                            bullet3: skills[index].companyName,
                            onTap: () {
                              AppNavigator.push(Routes.ADD_SKILL, arguments: {
                                'skillModel': skills[index],
                              });
                            },
                          );
                  },
                )
              : Empty(
                  image: Image.asset(
                    imageSkillEmpty,
                    width: 134.33.sp,
                    height: 93.72.sp,
                  ),
                  text: Strings.createFirstSkillSugesstion.i18n,
                  hasButton: true,
                  buttonText: Strings.createNewSkill,
                  buttonOnPressed: () {
                    AppNavigator.push(Routes.ADD_SKILL);
                  },
                );
        }

        // Show shimmer list
        return ListView.builder(
          physics: BouncingScrollPhysics(),
          padding: EdgeInsets.only(bottom: 30.sp),
          itemCount: ITEM_COUNT_SHIMMER,
          itemBuilder: (context, index) {
            return SkillShimmerCard();
          },
        );
      },
    );
  }
}
