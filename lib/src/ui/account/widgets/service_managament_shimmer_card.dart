import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:askany/src/ui/common/widgets/shimmers/fade_simmer.dart';
import 'package:askany/src/ui/style/style.dart';

class ServiceShimmerCard extends StatelessWidget {
  final int index;
  const ServiceShimmerCard({
    Key? key,
    required this.index,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          dividerThinkness6NotMargin,
          Container(
            color: colorWhite,
            padding: EdgeInsets.only(
              top: 20.sp,
              bottom: 10.sp,
              left: 16.sp,
              right: 16.sp,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        FadeShimmer(
                          height: 15.sp,
                          width: index.isEven ? 55.w : 45.w,
                          fadeTheme: FadeTheme.lightReverse,
                        ),
                        Container(
                          child: index.isEven
                              ? Padding(
                                  padding: EdgeInsets.only(top: 6.sp),
                                  child: FadeShimmer(
                                    height: 15.sp,
                                    width: 30.w,
                                    fadeTheme: FadeTheme.lightReverse,
                                  ),
                                )
                              : Container(),
                        ),
                      ],
                    ),
                    FadeShimmer(
                      height: 11.sp,
                      width: 25.w,
                      fadeTheme: FadeTheme.lightReverse,
                    ),
                  ],
                ),
                SizedBox(
                  height: 15.sp,
                ),
                Row(
                  children: [
                    FadeShimmer(
                      height: 12.sp,
                      width: 45.w,
                      fadeTheme: FadeTheme.lightReverse,
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(top: 10.sp),
                  child: FadeShimmer(
                    height: 1.sp,
                    width: 90.w,
                    fadeTheme: FadeTheme.lightReverse,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 12.sp),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          FadeShimmer(
                            height: 11.sp,
                            width: 25.w,
                            fadeTheme: FadeTheme.lightReverse,
                          ),
                          FadeShimmer(
                            height: 11.sp,
                            width: 50.w,
                            fadeTheme: FadeTheme.lightReverse,
                          ),
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 10.sp),
                        child: index.isEven
                            ? Container()
                            : Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  FadeShimmer(
                                    height: 11.sp,
                                    width: 85.w,
                                    fadeTheme: FadeTheme.lightReverse,
                                  ),
                                ],
                              ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
