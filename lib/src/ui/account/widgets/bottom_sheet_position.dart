import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class BottomSheetPosition extends StatefulWidget {
  final Function(String) handlePressed;
  final String? position;
  final List<String> listPosition;
  BottomSheetPosition({
    required this.handlePressed,
    required this.position,
    required this.listPosition,
  });

  @override
  State<BottomSheetPosition> createState() => _BottomSheetPositionState();
}

class _BottomSheetPositionState extends State<BottomSheetPosition> {
  late String? _position;
  @override
  void initState() {
    super.initState();
    _position = widget.position;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        height: 50.h,
        child: Column(
          children: [
            SizedBox(
              height: 6.sp,
            ),
            Container(
              width: 60.sp,
              height: 3.sp,
              decoration: BoxDecoration(
                  color: colorGray4,
                  borderRadius: BorderRadius.circular(1.5.sp)),
            ),
            SizedBox(
              height: 21.sp,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 16.sp),
              alignment: Alignment.centerLeft,
              height: 24.sp,
              child: Text(
                Strings.yourPosition.i18n,
                style: text15w700cBlack2,
              ),
            ),
            SizedBox(height: 12.sp),
            Divider(
              color: colorGray2,
              thickness: 0.5.sp,
            ),
            SizedBox(height: 12.sp),
            Expanded(
              child: RawScrollbar(
                isAlwaysShown: true,
                thumbColor: colorGreen4,
                radius: Radius.circular(30.sp),
                thickness: 4.sp,
                child: ListView.builder(
                  shrinkWrap: true,
                  padding: EdgeInsets.zero,
                  itemCount: widget.listPosition.length,
                  itemBuilder: (context, index) {
                    return Container(
                      color: widget.listPosition[index] == _position
                          ? colorChosenCard
                          : Colors.transparent,
                      padding: EdgeInsets.symmetric(vertical: 12.sp),
                      child: TouchableOpacity(
                        onTap: () {
                          if (widget.listPosition[index] != _position) {
                            widget.handlePressed(widget.listPosition[index]);
                            setState(() {
                              _position = widget.listPosition[index];
                            });
                          }
                        },
                        child: Row(
                          children: [
                            SizedBox(
                              width: 16.sp,
                            ),
                            Expanded(
                              child: Container(
                                child: Text(
                                  widget.listPosition[index],
                                  style: widget.listPosition[index] == _position
                                      ? text13w400cGreen2
                                      : text13w400cBlack2,
                                ),
                              ),
                            ),
                            Container(
                              width: 64.sp,
                              child: Center(
                                child: widget.listPosition[index] == _position
                                    ? Image.asset(
                                        iconCheckGreen,
                                        width: 14.58.sp,
                                        height: 12.5.sp,
                                      )
                                    : null,
                              ),
                            )
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
            SizedBox(height: 12.sp),
          ],
        ),
      ),
    );
  }
}
