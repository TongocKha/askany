import 'package:askany/src/helpers/currency_helper.dart';
import 'package:askany/src/ui/common/widgets/custom_dropdown_button/dropdown_button_2.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_form.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class MoneyServiceWithTimeCard extends StatelessWidget {
  final bool isVND;
  final EdgeInsets? margin;
  final TextEditingController textEditingController;
  final String textMinute;
  final String Function(String?)? onChangeTextForm;
  final String hintDropdown;
  final String valueDropDown;
  final List<String> listDropDown;
  final String Function(String?)? onChangeDropDown;
  final String? Function(String?)? validatorTextForm;
  MoneyServiceWithTimeCard({
    required this.isVND,
    this.margin,
    required this.textEditingController,
    required this.textMinute,
    this.onChangeTextForm,
    this.validatorTextForm,
    required this.hintDropdown,
    required this.valueDropDown,
    required this.listDropDown,
    this.onChangeDropDown,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Container(
              height: 35.sp,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6.sp),
                color: colorGreen5,
              ),
              child: Center(
                child: Text(
                  textMinute,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 13.sp,
                    color: colorBlack2,
                  ),
                ),
              ),
            ),
          ),
          SizedBox(width: 10.sp),
          Expanded(
            child: TextFieldForm(
              fontSize: 13.sp,
              controller: textEditingController,
              textInputType: TextInputType.multiline,
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: colorGreen2, width: 1.sp),
                borderRadius: BorderRadius.circular(6.sp),
              ),
              contentPadding: EdgeInsets.all(11.sp),
              margin: EdgeInsets.zero,
              onChanged: onChangeTextForm,
              validatorForm: validatorTextForm,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              inputFormatters:
                  isVND ? CurrencyHelper.vndInputFormat : CurrencyHelper.usdInputFormat,
            ),
          ),
          SizedBox(width: 10.sp),
          Expanded(
            child: DropdownButtonFormField2(
              icon: Image.asset(iconArrowDown, width: 10.sp),
              dropdownElevation: 0,
              buttonWidth: 100.w,
              dropdownPadding: EdgeInsets.all(0),
              offset: Offset(0, -3.sp),
              buttonHeight: 35.sp,
              dropdownMaxHeight: 120.sp,
              itemWidth: 100.w / 3 - 16.sp,
              value: valueDropDown,
              decoration: InputDecoration(
                border: InputBorder.none,
                contentPadding: EdgeInsets.zero,
              ),
              buttonPadding: EdgeInsets.symmetric(horizontal: 10.sp),
              dropdownDecoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: colorBorderTextField),
                borderRadius: BorderRadius.circular(8.sp),
              ),
              buttonDecoration: BoxDecoration(
                color: Colors.white,
                borderRadius: const BorderRadius.all(Radius.circular(8)),
                border: Border.all(color: Colors.grey),
              ),
              items: listDropDown
                  .map(
                    (e) => DropdownMenuItem<String>(
                      value: e,
                      child: Text(
                        e,
                        style: TextStyle(
                          color: colorBlack2,
                          fontSize: 12.sp,
                        ),
                      ),
                    ),
                  )
                  .toList(),
              onChanged: onChangeDropDown,
            ),
          )
        ],
      ),
    );
  }
}
