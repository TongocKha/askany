import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ManageServiceArea extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ManageServiceAreaState();
}

class _ManageServiceAreaState extends State<ManageServiceArea> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final List<Map<String, dynamic>> _services = [
      {
        'title': Strings.confirming.i18n,
        'icon': iconWaittingConfirm,
        'index': 0,
      },
      {
        'title': Strings.confirmed.i18n,
        'icon': iconConfirm,
        'index': 1,
      },
      {
        'title': Strings.advise.i18n,
        'icon': iconAdvise,
        'index': 2,
      },
      {
        'title': Strings.completed.i18n,
        'icon': iconServiceFinished,
        'index': 3,
      },
    ];
    return Row(
      children: [
        ..._services.map((item) => _buildServiceSelection(item: item)),
      ],
    );
  }

  Widget _buildServiceSelection({required dynamic item}) {
    return Expanded(
      child: TouchableOpacity(
        onTap: () {
          if (item['index'] != null) {
            AppNavigator.push(Routes.MY_SERVICE, arguments: {
              'index': item['index'],
            });
          }
        },
        child: Container(
          color: Colors.transparent,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                item['icon'],
                width: 18.sp,
                height: 18.sp,
              ),
              SizedBox(height: 12.sp),
              Text(
                item['title'],
                style: TextStyle(
                  color: colorBlack2,
                  fontSize: 10.sp,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
