import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/calendar_style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ButtonSelect extends StatelessWidget {
  final String icon;
  final String content;
  final VoidCallback handlePressed;
  ButtonSelect({
    required this.icon,
    required this.content,
    required this.handlePressed,
  });

  @override
  Widget build(BuildContext context) {
    return TouchableOpacity(
      onTap: handlePressed,
      child: Container(
        color: Colors.transparent,
        padding: EdgeInsets.only(
          left: 16.sp,
          top: 16.sp,
          bottom: 10.sp,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset(
              icon,
              width: 18.sp,
              height: 18.sp,
            ),
            SizedBox(width: 14.sp),
            Text(
              content,
              style: TextStyle(
                color: colorBlack2,
                fontSize: 13.sp,
              ),
            )
          ],
        ),
      ),
    );
  }
}
