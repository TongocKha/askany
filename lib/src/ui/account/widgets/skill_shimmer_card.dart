import 'package:askany/src/ui/common/widgets/shimmers/fade_simmer.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class SkillShimmerCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 7.5.sp),
      padding: EdgeInsets.symmetric(vertical: 16.sp, horizontal: 8.sp),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(6.5.sp),
        border: Border.all(
          color: colorGray2, // Set border color
          width: 0.5.sp,
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                margin: EdgeInsets.fromLTRB(0, 0, 7.sp, 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    FadeShimmer(
                      height: 12.5.sp,
                      width: 60.w,
                      fadeTheme: FadeTheme.lightReverse,
                    ),
                    SizedBox(height: 4.sp),
                    FadeShimmer(
                      height: 12.sp,
                      width: 80.sp,
                      fadeTheme: FadeTheme.lightReverse,
                    ),
                  ],
                ),
              ),
              FadeShimmer(
                width: 24.sp,
                height: 24.sp,
                fadeTheme: FadeTheme.lightReverse,
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 10.sp),
            child: dividerChat,
          ),
          FadeShimmer(
            width: 40.w,
            height: 11.sp,
            fadeTheme: FadeTheme.lightReverse,
          ),
          SizedBox(height: 6.sp),
          FadeShimmer(
            width: 40.w,
            height: 11.sp,
            fadeTheme: FadeTheme.lightReverse,
          )
        ],
      ),
    );
  }
}
