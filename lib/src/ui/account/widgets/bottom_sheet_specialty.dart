import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class BottomSheetSpecialty extends StatefulWidget {
  final Function(String) handlePressed;
  final String? specialty;
  final List<String> specialtiesTitle;
  BottomSheetSpecialty({
    required this.handlePressed,
    required this.specialty,
    required this.specialtiesTitle,
  });

  @override
  State<BottomSheetSpecialty> createState() => _BottomSheetSpecialtyState();
}

class _BottomSheetSpecialtyState extends State<BottomSheetSpecialty> {
  late String? _specialty;
  @override
  void initState() {
    super.initState();
    _specialty = widget.specialty;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        height: 80.h,
        child: Column(
          children: [
            SizedBox(
              height: 6.sp,
            ),
            Container(
              width: 60.sp,
              height: 3.sp,
              decoration: BoxDecoration(
                  color: colorGray4,
                  borderRadius: BorderRadius.circular(1.5.sp)),
            ),
            SizedBox(
              height: 21.sp,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 16.sp),
              alignment: Alignment.centerLeft,
              height: 24.sp,
              child: Text(
                Strings.selecrASectorSuggestionTitle.i18n,
                style: text15w700cBlack2,
              ),
            ),
            SizedBox(height: 12.sp),
            Divider(
              color: colorGray2,
              thickness: 0.5.sp,
            ),
            SizedBox(height: 12.sp),
            Expanded(
              child: RawScrollbar(
                isAlwaysShown: true,
                thumbColor: colorGreen4,
                radius: Radius.circular(30.sp),
                thickness: 4.sp,
                child: ListView.builder(
                  shrinkWrap: true,
                  padding: EdgeInsets.zero,
                  itemCount: widget.specialtiesTitle.length,
                  itemBuilder: (context, index) {
                    return Container(
                      color: widget.specialtiesTitle[index] == _specialty
                          ? colorChosenCard
                          : Colors.transparent,
                      padding: EdgeInsets.symmetric(vertical: 12.sp),
                      child: TouchableOpacity(
                        onTap: () {
                          if (widget.specialtiesTitle[index] != _specialty) {
                            widget
                                .handlePressed(widget.specialtiesTitle[index]);
                            setState(() {
                              _specialty = widget.specialtiesTitle[index];
                            });
                          }
                        },
                        child: Row(
                          children: [
                            SizedBox(
                              width: 16.sp,
                            ),
                            Expanded(
                              child: Container(
                                child: Text(
                                  widget.specialtiesTitle[index],
                                  style: widget.specialtiesTitle[index] ==
                                          _specialty
                                      ? text13w400cGreen2
                                      : text13w400cBlack2,
                                ),
                              ),
                            ),
                            Container(
                              width: 64.sp,
                              child: Center(
                                child:
                                    widget.specialtiesTitle[index] == _specialty
                                        ? Image.asset(
                                            iconCheckGreen,
                                            width: 14.58.sp,
                                            height: 12.5.sp,
                                          )
                                        : null,
                              ),
                            )
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
            SizedBox(height: 12.sp),
          ],
        ),
      ),
    );
  }
}
