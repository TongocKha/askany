import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class TextTitle extends StatelessWidget {
  final textTitle;
  const TextTitle({required this.textTitle});

  @override
  Widget build(BuildContext context) {
    return Text(
      textTitle,
      style: TextStyle(
        fontWeight: FontWeight.w600,
        fontSize: 13.sp,
        color: colorBlack2,
      ),
    );
  }
}
