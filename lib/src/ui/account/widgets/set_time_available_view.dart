import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/models/expert_timeline_model.dart';
import 'package:askany/src/models/slide_mode.dart';
import 'package:askany/src/models/time_line_price_model.dart';
import 'package:askany/src/models/weekly_schedule_model.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_with_text_and_pop_button.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_input_hour.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:askany/src/helpers/date_time_helper.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';

class PeriodContainer extends StatefulWidget {
  final Function()? onTap;
  final TimelinePriceModel timeline;
  final List<DateTime> datePicked;
  final Function(List<DateTime>) handleChangeDate;
  final Function(TimelinePriceModel) handleChange;
  const PeriodContainer({
    Key? key,
    this.onTap,
    required this.timeline,
    required this.datePicked,
    required this.handleChangeDate,
    required this.handleChange,
  }) : super(key: key);

  @override
  State<PeriodContainer> createState() => _PeriodContainerState();
}

class _PeriodContainerState extends State<PeriodContainer> {
  bool isSelecting = false;
  bool isRepeat = false;
  List<TimelineModel> timesMeet = [];
  List<TimelineModel> timesCall = [];
  List<DateTime> _dayOfWeeksPicked = [];
  List<DateTime> _backupDayOfWeeksPicked = [];
  late List<DateTime> weeks;

  @override
  void initState() {
    super.initState();
    weeks = getCalendarThisWeek(
        dateTime: widget.timeline.weeklySchedules.isEmpty
            ? DateTime.now()
            : widget.timeline.weeklySchedules.last.date);
    isRepeat = widget.timeline.isRepeat == IS_REPEAT;
    timesCall = widget.timeline.weeklySchedules.isEmpty
        ? []
        : widget.timeline.weeklySchedules.first.timesCall;
    timesMeet = widget.timeline.weeklySchedules.isEmpty
        ? []
        : widget.timeline.weeklySchedules.first.timesMeet;

    widget.timeline.weeklySchedules.forEach((schedule) {
      DateTime date =
          weeks[schedule.dayOfWeek == 0 ? 6 : schedule.dayOfWeek - 1];
      _dayOfWeeksPicked.add(date);
    });

    _backupDayOfWeeksPicked.addAll(_dayOfWeeksPicked);
  }

  String get titleContainer {
    if (_dayOfWeeksPicked.length == 7) {
      return '${DateFormat('dd/MM/yyyy').format(weeks.first)} (${Strings.monday.i18n}) - ${DateFormat('dd/MM/yyyy').format(weeks.last)} (${Strings.sunday.i18n})';
    }
    List<String> result = [];
    List<List<DateTime>> formatWeek = [];
    List<DateTime> chains = [];
    _dayOfWeeksPicked.asMap().forEach((index, date) {
      if (index == 0) {
        chains.add(date);
      } else {
        if (date.weekday != _dayOfWeeksPicked[index - 1].weekday + 1) {
          formatWeek.add(chains);
          chains = [];
        }
        chains.add(date);
      }

      if (index == _dayOfWeeksPicked.length - 1 && chains.isNotEmpty) {
        formatWeek.add(chains);
      }
    });

    formatWeek.forEach((chains) {
      if (chains.length == 1) {
        int dayOfWeek = weeks
            .indexWhere((element) => element.isAtSameMomentAs(chains.first));
        result.add(
            '${DateFormat('dd/MM/yyyy').format(chains.first)} (${dayNames[dayOfWeek]})');
      } else if (chains.length > 1) {
        int dayOfWeek1 = weeks
            .indexWhere((element) => element.isAtSameMomentAs(chains.first));
        int dayOfWeek2 = weeks
            .indexWhere((element) => element.isAtSameMomentAs(chains.last));
        result.add(
            '${DateFormat('dd/MM/yyyy').format(chains.first)} (${dayNames[dayOfWeek1]}) - ${DateFormat('dd/MM/yyyy').format(chains.last)} (${dayNames[dayOfWeek2]})');
      }
    });

    return result.join(' và ');
  }

  _handleChangeValue() {
    TimelinePriceModel _timeline = widget.timeline;
    _timeline.isRepeat = isRepeat ? IS_REPEAT : 0;
    List<WeeklyScheduleModel> weeklySchedules = _dayOfWeeksPicked
        .map(
          (date) => WeeklyScheduleModel(
            id: '',
            dayOfWeek: date.weekday == 7 ? 0 : date.weekday,
            date: date,
            timesMeet: timesMeet,
            timesCall: timesCall,
          ),
        )
        .toList();
    _timeline.weeklySchedules = weeklySchedules;
    widget.handleChange(_timeline);
  }

  @override
  void didUpdateWidget(covariant PeriodContainer oldWidget) {
    super.didUpdateWidget(oldWidget);
    _dayOfWeeksPicked.clear();
    _backupDayOfWeeksPicked.clear();
    weeks = getCalendarThisWeek(
        dateTime: widget.timeline.weeklySchedules.isEmpty
            ? DateTime.now()
            : widget.timeline.weeklySchedules.last.date);

    widget.timeline.weeklySchedules.forEach((schedule) {
      DateTime date =
          weeks[schedule.dayOfWeek == 0 ? 6 : schedule.dayOfWeek - 1];
      _dayOfWeeksPicked.add(date);
    });

    _backupDayOfWeeksPicked.addAll(_dayOfWeeksPicked);
  }

  @override
  Widget build(BuildContext context) {
    final int month = DateTime.now().month;
    return Container(
      margin: EdgeInsets.only(bottom: 16.sp),
      padding: EdgeInsets.all(10.sp),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(
          8.sp,
        ),
        border: Border.all(
          width: 0.5.sp,
          color: colorGray2,
        ),
      ),
      child: Column(
        children: [
          Container(
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      widget.timeline.weeklySchedules.isEmpty
                          ? Strings.month[month - 1].i18n +
                              ', ${DateTime.now().year}'
                          : Strings.month[month - 1].i18n +
                              ', ${widget.timeline.weeklySchedules.last.date.year}',
                      style: text14w600cBlack2,
                    ),
                    Spacer(),
                    TouchableOpacity(
                      onTap: () {
                        setState(() {
                          isRepeat = !isRepeat;
                        });

                        List<DateTime> _formatPicked = [];
                        _formatPicked.addAll(widget.datePicked);

                        if (isRepeat) {
                          _formatPicked.addAll(_dayOfWeeksPicked);

                          _formatPicked.sort((a, b) => a.compareTo(b));
                        } else {
                          List<DateTime> _beforeSave = [];
                          _beforeSave.addAll(_dayOfWeeksPicked);
                          _beforeSave.forEach((date) {
                            int indexOfPicked = _formatPicked.indexWhere(
                              (item) => item.weekday == date.weekday,
                            );

                            if (indexOfPicked != -1) {
                              _formatPicked.removeAt(indexOfPicked);
                            }
                          });
                        }

                        if (!isRepeat) {
                          _handleChangeValue();
                          widget.handleChangeDate(_formatPicked);
                        } else {
                          widget.handleChangeDate(_formatPicked);
                          _handleChangeValue();
                        }
                      },
                      child: Row(
                        children: [
                          isRepeat
                              ? Image.asset(
                                  iconCheckSquare,
                                  width: 18.sp,
                                  height: 18.sp,
                                )
                              : Container(
                                  height: 18.sp,
                                  width: 18.sp,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(
                                      4.sp,
                                    ),
                                    border: Border.all(
                                      width: 0.5.sp,
                                      color: colorFontGreen,
                                    ),
                                  ),
                                ),
                          SizedBox(
                            width: 6.sp,
                          ),
                          Text(
                            Strings.repeatEveryWeek.i18n,
                            style: text12w400cBlack2,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10.sp,
                ),
                Container(
                  padding: EdgeInsets.only(
                    left: 10.sp,
                    right: 12.sp,
                    top: 10.sp,
                    bottom: 10.sp,
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(6.5.sp),
                    color: colorGreen5,
                  ),
                  child: Row(
                    children: [
                      Expanded(
                        child: Text(
                          titleContainer,
                          style: text11w400cBlack2,
                        ),
                      ),
                      SizedBox(width: 12.sp),
                      Container(
                        alignment: Alignment.centerRight,
                        child: (isSelecting == false)
                            ? TouchableOpacity(
                                onTap: () {
                                  setState(() {
                                    isSelecting = true;
                                  });
                                },
                                child: Image.asset(
                                  iconPenEdit2,
                                  width: 12.sp,
                                  height: 12.sp,
                                ),
                              )
                            : Container(
                                child: Row(
                                  children: [
                                    TouchableOpacity(
                                      onTap: () {
                                        setState(() {
                                          isSelecting = false;
                                          _backupDayOfWeeksPicked.clear();
                                          _backupDayOfWeeksPicked
                                              .addAll(_dayOfWeeksPicked);
                                        });
                                      },
                                      child: Image.asset(
                                        iconRemove,
                                        width: 10.sp,
                                        height: 10.sp,
                                        color: colorGray1,
                                      ),
                                    ),
                                    SizedBox(width: 16.sp),
                                    TouchableOpacity(
                                      onTap: () {
                                        setState(() {
                                          isSelecting = false;
                                          _dayOfWeeksPicked.clear();
                                          _dayOfWeeksPicked
                                              .addAll(_backupDayOfWeeksPicked);
                                        });
                                        if (_backupDayOfWeeksPicked
                                            .isNotEmpty) {
                                          List<DateTime> _formatPicked = [];
                                          List<DateTime> _beforeSave = [];
                                          _formatPicked
                                              .addAll(widget.datePicked);
                                          _beforeSave.addAll(_dayOfWeeksPicked);

                                          _beforeSave.forEach((date) {
                                            int indexOfPicked =
                                                _formatPicked.indexWhere(
                                              (item) =>
                                                  item.isAtSameMomentAs(date),
                                            );

                                            if (indexOfPicked != -1) {
                                              _formatPicked
                                                  .removeAt(indexOfPicked);
                                            }
                                          });

                                          setState(() {
                                            _dayOfWeeksPicked.clear();
                                            _dayOfWeeksPicked.addAll(
                                                _backupDayOfWeeksPicked);
                                          });

                                          _formatPicked
                                              .addAll(_dayOfWeeksPicked);

                                          _formatPicked
                                              .sort((a, b) => a.compareTo(b));

                                          widget
                                              .handleChangeDate(_formatPicked);
                                          _handleChangeValue();
                                        }
                                      },
                                      child: Image.asset(
                                        iconCheckGreen,
                                        width: 12.sp,
                                        height: 12.sp,
                                        color: colorGray1,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 10.sp,
          ),
          Visibility(
            visible: isSelecting,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ...weeks.map(
                  (date) {
                    int indexOfPickedd = _dayOfWeeksPicked.indexWhere(
                      (item) => item.isAtSameMomentAs(date),
                    );
                    bool isActive = !(widget.datePicked.indexWhere(
                                    (item) => item.isAtSameMomentAs(date)) !=
                                -1 &&
                            indexOfPickedd == -1) ||
                        !isRepeat;
                    int indexOfPicked = _backupDayOfWeeksPicked.indexWhere(
                      (item) => item.isAtSameMomentAs(date),
                    );
                    bool isPicked = indexOfPicked != -1;
                    return TouchableOpacity(
                      onTap: () {
                        if (isPicked) {
                          _backupDayOfWeeksPicked.removeAt(indexOfPicked);
                        } else {
                          if (isActive) {
                            _backupDayOfWeeksPicked.add(date);
                          }
                        }
                        setState(() {
                          _backupDayOfWeeksPicked
                              .sort((a, b) => a.compareTo(b));
                        });
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5.sp),
                          color: isPicked ? colorGreen3 : Colors.white,
                          border: Border.all(
                            color: colorGreen3,
                            width: 0.5,
                          ),
                        ),
                        padding: EdgeInsets.symmetric(
                          horizontal: 8.sp,
                          vertical: 7.sp,
                        ),
                        child: Column(
                          children: [
                            Text(
                              dayNames[date.weekday - 1],
                              style: TextStyle(
                                color: isPicked
                                    ? Colors.white
                                    : isActive
                                        ? colorGreen2
                                        : colorGray2,
                                fontSize: 10.sp,
                              ),
                            ),
                            SizedBox(height: 6.sp),
                            Text(
                              addZeroPrefix(date.day),
                              style: TextStyle(
                                color: isPicked
                                    ? Colors.white
                                    : isActive
                                        ? colorGreen2
                                        : colorGray2,
                                fontSize: 13.sp,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
          SizedBox(
            height: 10.sp,
          ),
          Container(
            height: 40.sp +
                ((timesCall.length >= timesMeet.length
                        ? timesCall.length
                        : timesMeet.length) *
                    40.sp),
            child: Row(
              children: [
                Expanded(
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          Strings.availableForCalling.i18n,
                          style: text11w600cBlack2,
                        ),
                        Expanded(
                          child: ListView.builder(
                            padding: EdgeInsets.zero,
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: timesCall.length,
                            itemBuilder: (context, index) {
                              return Row(
                                children: [
                                  Expanded(
                                    child: Container(
                                      margin: EdgeInsets.only(bottom: 6.sp),
                                      child: TextFieldInputHour(
                                        onTap: widget.onTap ?? () {},
                                        initialValue:
                                            timesCall[index].timeString,
                                        onChanged: (timelineModel) {
                                          setState(() {
                                            timesCall[index] = timelineModel;
                                          });
                                        },
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 6.sp,
                                  ),
                                  TouchableOpacity(
                                    onTap: () {
                                      setState(() {
                                        timesCall.removeAt(index);
                                      });
                                      _handleChangeValue();
                                    },
                                    child: Image.asset(
                                      iconClearCircle,
                                      width: 16.sp,
                                      height: 16.sp,
                                    ),
                                  )
                                ],
                              );
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  width: 10.sp,
                ),
                Expanded(
                  child: Container(
                    color: Colors.white,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          Strings.availableForMeeting.i18n,
                          style: text11w600cBlack2,
                        ),
                        Expanded(
                          child: ListView.builder(
                            padding: EdgeInsets.zero,
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: timesMeet.length,
                            itemBuilder: (context, index) {
                              return Row(
                                children: [
                                  Expanded(
                                    child: Container(
                                      margin: EdgeInsets.only(bottom: 6.sp),
                                      child: TextFieldInputHour(
                                        onTap: widget.onTap ?? () {},
                                        initialValue:
                                            timesMeet[index].timeString,
                                        onChanged: (timelineModel) {
                                          setState(() {
                                            timesMeet[index] = timelineModel;
                                          });
                                        },
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 6.sp,
                                  ),
                                  TouchableOpacity(
                                    onTap: () {
                                      setState(() {
                                        timesMeet.removeAt(index);
                                      });
                                      _handleChangeValue();
                                    },
                                    child: Image.asset(
                                      iconClearCircle,
                                      width: 16.sp,
                                      height: 16.sp,
                                    ),
                                  )
                                ],
                              );
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          TouchableOpacity(
            onTap: () {
              if (_backupDayOfWeeksPicked.isEmpty) {
                dialogAnimationWrapper(
                  slideFrom: SlideMode.bot,
                  child: DialogWithTextAndPopButton(
                    title: Strings.cannotAddThisAvailableTime.i18n,
                    bodyAfter: Strings.pleaseSelecteAvailableDayFirst.i18n,
                    bodyAlign: TextAlign.center,
                  ),
                );
              } else {
                List<DateTime> _formatPicked = [];
                List<DateTime> _beforeSave = [];
                _formatPicked.addAll(widget.datePicked);
                _beforeSave.addAll(_dayOfWeeksPicked);

                _beforeSave.forEach((date) {
                  int indexOfPicked = _formatPicked.indexWhere(
                    (item) => item.isAtSameMomentAs(date),
                  );

                  if (indexOfPicked != -1) {
                    _formatPicked.removeAt(indexOfPicked);
                  }
                });

                setState(() {
                  _dayOfWeeksPicked.clear();
                  _dayOfWeeksPicked.addAll(_backupDayOfWeeksPicked);
                });

                _formatPicked.addAll(_dayOfWeeksPicked);

                _formatPicked.sort((a, b) => a.compareTo(b));

                widget.handleChangeDate(_formatPicked);
                _handleChangeValue();
                TimeOfDay defaultTimeStart = TimeOfDay(hour: 8, minute: 0);
                TimeOfDay defaultTimeEnd = TimeOfDay(hour: 20, minute: 0);
                setState(() {
                  timesMeet.add(
                    TimelineModel(
                      startTime: defaultTimeStart,
                      endTime: defaultTimeEnd,
                    ),
                  );
                  timesCall.add(
                    TimelineModel(
                      startTime: defaultTimeStart,
                      endTime: defaultTimeEnd,
                    ),
                  );
                });
                _handleChangeValue();
              }
            },
            child: Container(
              color: colorWhite,
              alignment: Alignment.centerLeft,
              child: Text(
                Strings.addMoreAvailableTimeWithPlus.i18n,
                style: text13w400cFontGreen,
              ),
            ),
          ),
          SizedBox(
            height: 10.sp,
          ),
        ],
      ),
    );
  }
}
