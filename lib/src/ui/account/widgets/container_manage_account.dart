import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/calendar/calendar_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ContainerManageAccount extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ContainerManageAccountState();
}

class _ContainerManageAccountState extends State<ContainerManageAccount> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: TouchableOpacity(
            onTap: () {
              AppBloc.calendarBloc.add(GetCalendarEvent());
              AppNavigator.push(Routes.CALENDAR);
            },
            child: _buildSelection(
              title: Strings.consultingSchedule.i18n,
              iconAsset: iconMyCalendar,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 16.sp),
          child: VerticalDivider(
            color: colorDividerTimeline,
            thickness: 0.3.sp,
            width: 0.3.sp,
          ),
        ),
        Expanded(
          child: TouchableOpacity(
            onTap: () => AppNavigator.push(Routes.WALLET),
            child: _buildSelection(
              title: Strings.myWallet.i18n,
              iconAsset: iconMyWallet,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildSelection({required String title, required String iconAsset}) {
    return Container(
      color: Colors.transparent,
      alignment: Alignment.center,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            iconAsset,
            width: 18.sp,
            height: 18.sp,
          ),
          SizedBox(height: 12.sp),
          Text(
            title,
            style: TextStyle(
              color: colorBlack2,
              fontSize: 11.sp,
            ),
          )
        ],
      ),
    );
  }
}
