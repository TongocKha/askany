import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class SkillCard extends StatelessWidget {
  final String title;
  final String price;
  final String time;
  final String bullet1;
  final String bullet2;
  final String bullet3;
  final Function onTap;

  const SkillCard({
    required this.title,
    required this.price,
    required this.time,
    required this.bullet1,
    required this.bullet2,
    required this.bullet3,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    String text = int.parse(time) >= 2
        ? ' /$time ${Strings.minutes.i18n}'
        : ' /$time ${Strings.minute.i18n}';
    return Container(
      margin: EdgeInsets.symmetric(vertical: 6.sp),
      padding: EdgeInsets.symmetric(vertical: 14.sp, horizontal: 8.sp),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(6.5.sp),
        border: Border.all(
          color: colorGray2, // Set border color
          width: 0.5.sp,
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                  child: Container(
                margin: EdgeInsets.fromLTRB(0, 0, 7.sp, 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      title,
                      style: TextStyle(
                        fontSize: 13.sp,
                        fontWeight: FontWeight.w600,
                        color: colorBlack1,
                      ),
                      overflow: TextOverflow.ellipsis,
                    ),
                    SizedBox(height: 2.sp),
                    RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                            text: price,
                            style: TextStyle(
                              fontSize: 11.sp,
                              color: colorBlack1,
                              height: 1.357,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          TextSpan(
                            text: text,
                            style: TextStyle(
                              fontSize: 9.sp,
                              color: colorGray1,
                              fontWeight: FontWeight.w400,
                              height: 1.364,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              )),
              TouchableOpacity(
                onTap: onTap,
                child: Container(
                  height: 24.sp,
                  width: 24.sp,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: colorGray2, // Set border color
                      width: 0.5.sp,
                    ),
                    borderRadius: BorderRadius.circular(4.5.sp),
                  ),
                  alignment: Alignment.center,
                  child: Image.asset(
                    iconPenEdit2,
                    width: 10.sp,
                  ),
                ),
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 10.sp),
            child: dividerChat,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset(iconDot, width: 5.sp, color: colorGray1),
              Text(
                "\t\t$bullet1",
                style: TextStyle(fontSize: 11.sp, color: colorGray1),
              )
            ],
          ),
          SizedBox(height: 2.5.sp),
          Row(
            children: [
              Visibility(
                visible: bullet2 != '',
                child: Image.asset(iconDot, width: 5.sp, color: colorGray1),
              ),
              Visibility(
                visible: bullet2 != '',
                child: Text(
                  "\t\t$bullet2\t\t\t",
                  style: TextStyle(fontSize: 11.sp, color: colorGray1),
                ),
              ),
              Image.asset(iconDot, width: 5.sp, color: colorGray1),
              Expanded(
                child: Text(
                  "\t\t$bullet3",
                  style: TextStyle(fontSize: 11.sp, color: colorGray1),
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
