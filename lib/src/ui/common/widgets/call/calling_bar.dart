import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/timer/timer_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/helpers/date_time_helper.dart';
import 'package:askany/src/models/slide_mode.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class CallingBar extends StatelessWidget {
  final TimerState? state;
  CallingBar({required this.state});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        AppNavigator.push(Routes.VIDEO_CALL, arguments: {
          'slide': SlideMode.fade,
          'conversationId': AppBloc.videoCallBloc.room,
        });
      },
      child: Align(
        alignment: Alignment.bottomCenter,
        child: Container(
          color: colorGreen3,
          padding: EdgeInsets.symmetric(vertical: 7.sp),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                Strings.tapToReturnTheCall.i18n,
                style: TextStyle(
                    fontSize: 9.sp,
                    color: colorWhite,
                    fontWeight: FontWeight.w400),
              ),
              SizedBox(
                width: 3.sp,
              ),
              Image.asset(iconDot, width: 5.sp, color: colorWhite),
              SizedBox(
                width: 3.sp,
              ),
              Text(
                state is TimerRunInProgress
                    ? getDurationCalling(
                        durationSeconds:
                            (state! as TimerRunInProgress).duration)
                    : "00:00:00",
                style: TextStyle(
                    fontSize: 9.sp,
                    color: colorWhite,
                    fontWeight: FontWeight.w400),
                overflow: TextOverflow.ellipsis,
              )
            ],
          ),
        ),
      ),
    );
  }
}
