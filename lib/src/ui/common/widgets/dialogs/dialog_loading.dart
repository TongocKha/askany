import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

showDialogLoading() {
  showDialog(
    context: AppNavigator.context!,
    builder: (context) {
      return WillPopScope(
        onWillPop: () async => false,
        child: Center(
          child: Image.asset(
            imageLoading,
            height: 100.sp,
            width: 100.sp,
            fit: BoxFit.fitWidth,
            color: colorGreen5,
          ),
        ),
      );
    },
    barrierColor: Color(0x80000000),
    barrierDismissible: false,
  );
}
