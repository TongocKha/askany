import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class DialogChooseOrderServiceType extends StatelessWidget {
  const DialogChooseOrderServiceType({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        left: 16.sp,
        top: 24.sp,
        right: 16.sp,
        bottom: 26.sp,
      ),
      decoration: BoxDecoration(
        color: colorWhite,
        borderRadius: BorderRadius.circular(10.sp),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: 54.sp,
                height: 54.sp,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    image: NetworkImage(
                      'https://avatars.githubusercontent.com/u/60530946?v=4',
                    ),
                  ),
                ),
              ),
              SizedBox(width: 6.sp),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Hùng Nguyễn',
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 13.sp,
                      color: colorBlack1,
                      height: 1.5,
                    ),
                  ),
                  SizedBox(height: 6.sp),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      _SingleRowOfIconAndText(
                        text: 'Hồ Chí Minh',
                        icon: iconLocation,
                        iconHeight: 7.5.sp,
                        iconWidth: 10.sp,
                      ),
                      SizedBox(width: 8.sp),
                      _SingleRowOfIconAndText(
                        icon: iconStar,
                        iconHeight: 16.sp,
                        iconWidth: 16.sp,
                        text: '5.0',
                      ),
                      SizedBox(width: 2.sp),
                      Text(
                        '(234 reviews)',
                        style: TextStyle(
                          fontSize: 10.sp,
                          fontWeight: FontWeight.w400,
                          color: colorGray2,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
          SizedBox(height: 22.sp),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              _ButtonChooseOrderServiceType(
                isPrimary: false,
                text: 'Gọi điện',
              ),
              SizedBox(width: 8.sp),
              _ButtonChooseOrderServiceType(
                isPrimary: true,
                text: 'Gặp mặt trực tiếp',
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class _ButtonChooseOrderServiceType extends StatelessWidget {
  const _ButtonChooseOrderServiceType({
    Key? key,
    required this.text,
    required this.isPrimary,
  }) : super(key: key);

  final String text;
  final bool isPrimary;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: TouchableOpacity(
        onTap: () {
          AppNavigator.pop();
          // AppNavigator.push(
          //   Routes.ORDER_SERVICE,
          //   arguments: {
          //     'request': null,
          //     'offer': null,
          //   },
          // );
        },
        child: Container(
          height: 36.sp,
          padding: EdgeInsets.symmetric(vertical: 8.sp),
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: isPrimary ? colorFontGreen : colorWhite,
            borderRadius: BorderRadius.circular(6.5.sp),
            border: isPrimary
                ? null
                : Border.all(
                    color: colorFontGreen,
                    width: 1.sp,
                  ),
          ),
          child: Text(
            text,
            style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 12.sp,
              color: isPrimary ? colorWhite : colorFontGreen,
            ),
          ),
        ),
      ),
    );
  }
}

class _SingleRowOfIconAndText extends StatelessWidget {
  const _SingleRowOfIconAndText({
    Key? key,
    required this.icon,
    required this.iconHeight,
    required this.iconWidth,
    required this.text,
  }) : super(key: key);
  final String icon;
  final double iconHeight;
  final double iconWidth;
  final String text;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: [
        Image.asset(
          icon,
          width: iconWidth,
          height: iconHeight,
        ),
        SizedBox(width: 2.sp),
        Text(
          text,
          style: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: 10.sp,
            color: colorGray1,
          ),
        ),
      ],
    );
  }
}
