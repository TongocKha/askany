import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

import 'package:askany/src/bloc/timeline/timeline_bloc.dart';
import 'package:askany/src/helpers/date_time_helper.dart';
import 'package:askany/src/ui/calendar/widgets/calendar_title.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';

class DialogDatePicker extends StatefulWidget {
  final double? horizontal;
  final DateTime selectedDate;
  final Function(DateTime) onDateSelected;
  final List<int>? dateAvailableInMonth;
  DialogDatePicker({
    Key? key,
    this.horizontal,
    required this.selectedDate,
    required this.onDateSelected,
    this.dateAvailableInMonth,
  }) : super(key: key);
  @override
  _DialogDatePickerState createState() => _DialogDatePickerState();
}

class _DialogDatePickerState extends State<DialogDatePicker> {
  late int _currentMonth;
  late int _currentYear;
  late DateTime _selectedDate;
  List<List<DateTime>> _thisMonthDates = [];

  @override
  void initState() {
    super.initState();
    _currentMonth = widget.selectedDate.month;
    _currentYear = widget.selectedDate.year;
    _selectedDate = widget.selectedDate;
    _thisMonthDates = getCalendarThisMonth(dateTime: widget.selectedDate);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: widget.horizontal ?? 12.sp,
        vertical: 18.sp,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(
              left: 10.sp,
              right: 6.sp,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Tháng $_currentMonth, $_currentYear',
                  style: TextStyle(
                    color: colorBlack2,
                    fontSize: 15.5.sp,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    _buildButtonCalendar(
                      iconAsset: iconPreviousCalendar,
                    ),
                    SizedBox(width: 6.sp),
                    _buildButtonCalendar(
                      iconAsset: iconNextCalendar,
                      isNext: true,
                    ),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(height: 14.sp),
          CalendarTitle(
            textStyle: TextStyle(
              fontSize: 11.5.sp,
              color: colorBlack2,
              fontWeight: FontWeight.w600,
            ),
          ),
          SizedBox(height: 4.sp),
          ..._thisMonthDates.map(
            (weeks) => _buildCalendarPickerForWeek(weeks),
          ),
        ],
      ),
    );
  }

  Widget _buildCalendarPickerForWeek(List<DateTime> weeks) {
    return Padding(
      padding: EdgeInsets.only(top: 5.sp),
      child: Row(
        children: weeks.map(
          (date) {
            final bool isDateBefore = date.isBefore(DateTime.now().subtract(Duration(days: 1)));
            final bool enableDate = !locatedInThisMonth(date, compareDate: _selectedDate)
                ? true
                : widget.dateAvailableInMonth == null
                    ? true
                    : (widget.dateAvailableInMonth!.indexWhere((day) => day == date.day) != -1);
            return Expanded(
              child: TouchableOpacity(
                onTap: () {
                  if (enableDate && !isDateBefore) {
                    widget.onDateSelected(date);
                    setState(() {
                      _selectedDate = date;
                      _currentMonth = date.month;
                      _currentYear = date.year;
                      _thisMonthDates = getCalendarThisMonth(
                        dateTime: DateTime(
                          date.year,
                          date.month,
                        ),
                      );
                    });
                    if (widget.dateAvailableInMonth != null) {
                      BlocProvider.of<TimelineBloc>(context).add(
                        GetTimeAvailable(
                          day: date.day.toString(),
                        ),
                      );
                    }
                  }
                },
                child: Container(
                  width: 25.sp,
                  height: 25.sp,
                  margin: EdgeInsets.symmetric(horizontal: 5.sp),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5.sp),
                    color: isEqualTwoDate(_selectedDate, date) ? colorGreen3 : Colors.transparent,
                  ),
                  alignment: Alignment.center,
                  child: Text(
                    date.day.toString(),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: isDateBefore
                          ? colorGray2
                          : !enableDate
                              ? colorFinished
                              : locatedInThisMonth(date, compareDate: _selectedDate)
                                  ? isEqualTwoDate(date, _selectedDate)
                                      ? colorTextAppointmentCard
                                      : colorBlack2
                                  : colorGray2,
                      fontSize: 11.5.sp,
                    ),
                  ),
                ),
              ),
            );
          },
        ).toList(),
      ),
    );
  }

  Widget _buildButtonCalendar({required String iconAsset, bool isNext = false}) {
    return TouchableOpacity(
      onTap: () {
        if (isNext) {
          _selectedDate = _selectedDate.add(
            Duration(
              days: dayCountForMonth[_currentMonth - 1],
            ),
          );
        }

        _currentMonth = isNext ? _currentMonth + 1 : _currentMonth - 1;
        if (_currentMonth > 12) {
          _currentMonth = 1;
          _currentYear++;
        } else if (_currentMonth < 1) {
          _currentMonth = 12;
          _currentYear--;
        }

        if (!isNext) {
          _selectedDate = _selectedDate.subtract(
            Duration(
              days: dayCountForMonth[_currentMonth - 1],
            ),
          );
        }

        setState(() {
          _thisMonthDates = getCalendarThisMonth(
            dateTime: DateTime(
              _currentYear,
              _currentMonth,
            ),
          );
        });
      },
      child: Container(
        width: 24.sp,
        height: 24.sp,
        decoration: BoxDecoration(
          color: colorGreen5,
          borderRadius: BorderRadius.circular(5.sp),
        ),
        alignment: Alignment.center,
        child: Image.asset(
          iconAsset,
          width: 4.sp,
        ),
      ),
    );
  }
}
