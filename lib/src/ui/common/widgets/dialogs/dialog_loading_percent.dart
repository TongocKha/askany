import 'package:askany/src/bloc/progress/progress_bloc.dart';
import 'package:askany/src/ui/style/home_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class DialogLoadingPercent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Container(
        constraints: BoxConstraints(maxWidth: double.infinity),
        width: double.infinity,
        margin: EdgeInsets.only(top: 10.sp),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              padding: EdgeInsets.symmetric(
                vertical: 16.sp,
                horizontal: 20.sp,
              ),
              width: double.infinity,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 10.sp),
                    child: Image.asset(
                      imageRegisterSuccess,
                      width: 80.sp,
                      height: 80.sp,
                    ),
                  ),
                  SizedBox(height: 20.sp),
                  BlocBuilder<ProgressBloc, ProgressState>(
                    builder: (context, state) {
                      return Text(
                        state is ProgressLoaded ? '${state.progress.toStringAsFixed(1)}%' : '0%',
                        style: TextStyle(
                          fontSize: 12.sp,
                          fontWeight: FontWeight.w600,
                          color: Theme.of(context).primaryColor,
                        ),
                      );
                    },
                  ),
                  SizedBox(height: 10.sp),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
