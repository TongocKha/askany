import 'dart:ui';

import 'package:askany/src/configs/application.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

Future showDialogDownload({
  dismissible = true,
  timeForDismiss = 1500,
}) {
  if (timeForDismiss != null) {
    Future.delayed(Duration(milliseconds: timeForDismiss), () {
      Application.isShowingError = false;
      AppNavigator.pop();
    });
  }

  return showDialog(
    context: AppNavigator.context!,
    builder: (context) {
      return Material(
        color: Colors.transparent,
        child: SizedBox(
          height: 100.h,
          width: 100.w,
          child: Center(
            child: Container(
              width: 150.sp,
              height: 150.sp,
              child: ClipRRect(
                borderRadius: BorderRadius.zero,
                child: BackdropFilter(
                  filter: ImageFilter.blur(
                    sigmaX: 4.0,
                    sigmaY: 4.0,
                    tileMode: TileMode.decal,
                  ),
                  child: Container(
                    width: 140.sp,
                    height: 140.sp,
                    decoration: BoxDecoration(
                      color: Color(0xff404040).withOpacity(0.95),
                      borderRadius: BorderRadius.circular(12.sp),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: 20.sp,
                        ),
                        Image.asset(
                          iconTickVector,
                          width: 60.sp,
                          height: 45.sp,
                          color: Colors.white,
                          fit: BoxFit.fitHeight,
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                            top: 15.sp,
                          ),
                          child: Text(
                            Strings.downloadImageSuccess.i18n,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 16.sp,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      );
    },
  );
}
