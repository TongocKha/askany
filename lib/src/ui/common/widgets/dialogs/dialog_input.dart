import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/ui/authentication/widgets/auth_form_field.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class DialogInput extends StatefulWidget {
  final Function(String) handleFinish;
  final String title;
  final String buttonTitle;
  final String hideInputField;
  final String initialValue;
  DialogInput({
    required this.handleFinish,
    required this.title,
    required this.buttonTitle,
    required this.initialValue,
    this.hideInputField = 'Nhập tên cho cuộc trò chuyện...',
    // Strings.enterChatName.i18n,
  });

  @override
  State<StatefulWidget> createState() => _DialogInputState();
}

class _DialogInputState extends State<DialogInput> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _inputController = TextEditingController();
  var _inputErr = Strings.chatNameMustBeAtLeast3CharactersErrortext.i18n;
  @override
  void initState() {
    super.initState();
    _inputController.text = widget.initialValue;
  }

  Future<void> _trySubmitForm() async {
    final isValid = _formKey.currentState!.validate();
    if (isValid) {
      AppNavigator.pop();
      widget.handleFinish(_inputController.text);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 20.sp),
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 22.sp),
              child: Text(
                widget.title,
                style: TextStyle(
                  color: colorBlack1,
                  fontWeight: FontWeight.w600,
                  fontSize: 15.sp,
                ),
              ),
            ),
            SizedBox(
              height: 8.5.sp,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 22.sp),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: Container(
                      child: AuthFormField(
                        textVisible: true,
                        onSubmitted: (_) => _trySubmitForm(),
                        keyboardType: TextInputType.text,
                        validator: (val) => val!.trim().length < 3 ? _inputErr : null,
                        hintText: widget.hideInputField,
                        controller: _inputController,
                        onChanged: (val) => null,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 18.sp),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 22.sp),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  GestureDetector(
                    onTap: () {
                      AppNavigator.pop();
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 18.sp),
                      color: Colors.transparent,
                      alignment: Alignment.center,
                      child: Text(
                        Strings.cancel.i18n.toUpperCase(),
                        style: TextStyle(
                          fontSize: 13.sp,
                          color: colorBlack2,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      _trySubmitForm();
                    },
                    child: Container(
                      color: Colors.transparent,
                      alignment: Alignment.center,
                      padding: EdgeInsets.only(left: 18.sp),
                      child: Text(
                        widget.buttonTitle,
                        style: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: 13.sp,
                          color: colorGreen2,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
