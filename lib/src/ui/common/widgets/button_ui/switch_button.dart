import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';

class SwitchButton extends StatefulWidget {
  final TextStyle? style;
  final bool value;
  final String text;
  final Function(bool)? onToggle;

  const SwitchButton({
    Key? key,
    this.style,
    required this.value,
    required this.text,
    this.onToggle,
  }) : super(key: key);

  @override
  _SwitchButtonState createState() => _SwitchButtonState();
}

class _SwitchButtonState extends State<SwitchButton> with TickerProviderStateMixin {
  late AnimationController controller;
  late Animation<Offset> offset;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: DURATION_DEFAULT_ANIMATION),
    );

    offset = Tween<Offset>(
      begin: const Offset(-1.0, 0.0),
      end: Offset.zero,
    ).animate(controller);

    if (widget.value) {
      controller.forward();
    }
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return TouchableOpacity(
      onTap: () {
        if (widget.onToggle != null) {
          if (widget.value) {
            controller.reverse();
          } else {
            controller.forward();
          }
          widget.onToggle!(!widget.value);
        }
      },
      child: Row(
        children: [
          Container(
            decoration: BoxDecoration(
              color: widget.value ? headerCalendarColor : colorGray1,
              borderRadius: BorderRadius.circular(40.sp),
            ),
            padding: EdgeInsets.symmetric(horizontal: 2.sp),
            height: 17.sp,
            width: 31.2.sp,
            alignment: Alignment.centerRight,
            child: SlideTransition(
              position: offset,
              child: Container(
                height: 13.5.sp,
                width: 13.5.sp,
                decoration: BoxDecoration(
                  color: Colors.white,
                  shape: BoxShape.circle,
                ),
              ),
            ),
          ),
          SizedBox(width: 6.sp),
          Text(
            widget.text,
            style: widget.style ??
                TextStyle(
                  fontSize: 12.5.sp,
                  color: colorBlack2,
                ),
          ),
        ],
      ),
    );
  }
}
