import 'package:askany/src/ui/common/widgets/custom_image/network_image/cached_image.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class StackAvatar extends StatefulWidget {
  final List<String?> images;
  final double size;
  StackAvatar({required this.images, this.size = 25});
  @override
  State<StatefulWidget> createState() => _StackAvatarState();
}

class _StackAvatarState extends State<StackAvatar> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        alignment: Alignment.centerLeft,
        children: [
          _buildAvatar(context, 0),
          widget.images.length >= 2 ? _buildAvatar(context, 1) : Container(),
        ].reversed.toList(),
      ),
    );
  }

  Widget _buildAvatar(context, index) {
    return CustomNetworkImage(
      height: widget.size,
      width: widget.size,
      margin: EdgeInsets.only(left: index * 16.sp),
      urlToImage: widget.images[index],
    );
  }
}
