import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class LoadingWidget extends StatelessWidget {
  final Animation<Offset> offset;
  const LoadingWidget({required this.offset});
  @override
  Widget build(BuildContext context) {
    return SlideTransition(
      position: offset,
      child: Container(
        height: 72.sp,
        color: colorGreen2.withOpacity(0.1),
        child: Center(
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(Theme.of(context).primaryColor),
          ),
        ),
      ),
    );
  }
}
