import 'package:flutter/material.dart';

typedef Widget ScrollThumbBuilder(
  Color scrollThumbColor,
  double height,
  double width,
  Axis direction,
  double scrollThumbRadius,
  double scrollThumbElevation,
);

enum ScrollbarAlign { left, right, bottom, top }

class CustomScrollbar extends StatefulWidget {
  final BoxScrollView child;
  final ScrollThumbBuilder scrollThumbBuilder;
  final double scrollThumbLength;
  final double scrollThumbThickness;
  final Color scrollThumbColor;
  final Color backgroundColor;
  final EdgeInsetsGeometry padding;
  final EdgeInsetsGeometry twoHeadSpacing;
  final ScrollController controller;
  final ScrollbarAlign? scrollbarAlign;
  final double scrollThumbRadius;
  final double backgroundRadius;
  final double scrollThumbElevation;

  CustomScrollbar({
    Key? key,
    required this.scrollThumbLength,
    required this.scrollThumbThickness,
    required this.scrollThumbColor,
    required this.scrollThumbBuilder,
    required this.child,
    required this.controller,
    required this.scrollThumbRadius,
    this.backgroundColor = Colors.transparent,
    this.padding = const EdgeInsets.all(0),
    this.twoHeadSpacing = const EdgeInsets.all(0),
    this.scrollbarAlign,
    this.backgroundRadius = 0,
    this.scrollThumbElevation = 0,
  }) : super(key: key);

  CustomScrollbar.rect({
    Key? key,
    Key? scrollThumbKey,
    required this.child,
    required this.controller,
    this.scrollThumbLength = 48.0,
    this.scrollThumbThickness = 16.0,
    this.scrollThumbColor = Colors.grey,
    this.backgroundColor = Colors.transparent,
    this.padding = const EdgeInsets.all(0),
    this.twoHeadSpacing = const EdgeInsets.all(0),
    this.scrollbarAlign,
    this.scrollThumbRadius = 0,
    this.backgroundRadius = 0,
    this.scrollThumbElevation = 0,
  })  : scrollThumbBuilder = _thumbRRectBuilder(scrollThumbKey),
        super(key: key);

  @override
  _CustomScrollbarState createState() => _CustomScrollbarState();

  static ScrollThumbBuilder _thumbRRectBuilder(Key? scrollThumbKey) {
    return (
      Color scrollThumbColor,
      double height,
      double width,
      Axis direction,
      double scrollThumbRadius,
      double scrollThumbElevation,
    ) {
      final scrollThumb = Material(
        elevation: scrollThumbElevation,
        child: Container(
          constraints: direction == Axis.vertical
              ? BoxConstraints.tight(
                  Size(width, height),
                )
              : BoxConstraints.tight(
                  Size(height, width),
                ),
        ),
        color: scrollThumbColor,
        borderRadius: BorderRadius.all(Radius.circular(scrollThumbRadius)),
      );
      return scrollThumb;
    };
  }
}

class _CustomScrollbarState extends State<CustomScrollbar> {
  late double _barOffset;
  late double _viewOffset;
  late bool _isDragInProcess;
  late bool _isVertical;
  @override
  void initState() {
    super.initState();
    _barOffset = 0.0;
    _viewOffset = 0.0;
    _isDragInProcess = false;
    _isVertical = widget.child.scrollDirection == Axis.vertical;
  }

  @override
  void dispose() {
    super.dispose();
  }

  double get barMaxScrollExtent => _isVertical
      ? context.size!.height - widget.scrollThumbLength - widget.twoHeadSpacing.vertical
      : context.size!.width - widget.scrollThumbLength - widget.twoHeadSpacing.horizontal;

  double get barMinScrollExtent => 0.0;

  double get viewMaxScrollExtent => widget.controller.position.maxScrollExtent;

  double get viewMinScrollExtent => widget.controller.position.minScrollExtent;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
      return NotificationListener<ScrollNotification>(
        onNotification: (ScrollNotification notification) {
          changePosition(notification);
          return false;
        },
        child: Stack(
          children: <Widget>[
            RepaintBoundary(
              child: widget.child,
            ),
            RepaintBoundary(
              child: _isVertical
                  ? Align(
                      alignment: widget.scrollbarAlign == ScrollbarAlign.left
                          ? Alignment.centerLeft
                          : Alignment.centerRight,
                      child: Container(
                        color: widget.backgroundColor,
                        width: widget.scrollThumbThickness,
                        height: double.infinity,
                        margin: widget.twoHeadSpacing,
                        child: GestureDetector(
                          onVerticalDragStart: _onVerticalDragStart,
                          onVerticalDragUpdate: _onVerticalDragUpdate,
                          onVerticalDragEnd: _onVerticalDragEnd,
                          child: Container(
                            alignment: Alignment.topCenter,
                            margin: EdgeInsets.only(top: _barOffset),
                            padding: widget.padding,
                            child: widget.scrollThumbBuilder(
                              widget.scrollThumbColor,
                              widget.scrollThumbLength,
                              widget.scrollThumbThickness,
                              widget.child.scrollDirection,
                              widget.scrollThumbRadius,
                              widget.scrollThumbElevation,
                            ),
                          ),
                        ),
                      ),
                    )
                  : Align(
                      alignment: widget.scrollbarAlign == ScrollbarAlign.top
                          ? Alignment.topCenter
                          : Alignment.bottomCenter,
                      child: Container(
                        color: widget.backgroundColor,
                        width: double.infinity,
                        height: widget.scrollThumbThickness,
                        margin: widget.twoHeadSpacing,
                        child: GestureDetector(
                          onHorizontalDragStart: _onHorizontalDragStart,
                          onHorizontalDragUpdate: _onHorizontalDragUpdate,
                          onHorizontalDragEnd: _onHorizontalDragEnd,
                          child: Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.only(left: _barOffset),
                            padding: widget.padding,
                            child: widget.scrollThumbBuilder(
                              widget.scrollThumbColor,
                              widget.scrollThumbLength,
                              widget.scrollThumbThickness,
                              widget.child.scrollDirection,
                              widget.scrollThumbRadius,
                              widget.scrollThumbElevation,
                            ),
                          ),
                        ),
                      ),
                    ),
            ),
          ],
        ),
      );
    });
  }

  changePosition(ScrollNotification notification) {
    if (_isDragInProcess) {
      return;
    }

    setState(() {
      if (notification is ScrollUpdateNotification) {
        _barOffset += getBarDelta(
          notification.scrollDelta!,
          barMaxScrollExtent,
          viewMaxScrollExtent,
        );

        if (_barOffset < barMinScrollExtent) {
          _barOffset = barMinScrollExtent;
        }
        if (_barOffset > barMaxScrollExtent) {
          _barOffset = barMaxScrollExtent;
        }

        _viewOffset += notification.scrollDelta!;
        if (_viewOffset < widget.controller.position.minScrollExtent) {
          _viewOffset = widget.controller.position.minScrollExtent;
        }
        if (_viewOffset > viewMaxScrollExtent) {
          _viewOffset = viewMaxScrollExtent;
        }
      }
    });
  }

  double getBarDelta(
    double scrollViewDelta,
    double barMaxScrollExtent,
    double viewMaxScrollExtent,
  ) {
    return scrollViewDelta * barMaxScrollExtent / viewMaxScrollExtent;
  }

  double getScrollViewDelta(
    double barDelta,
    double barMaxScrollExtent,
    double viewMaxScrollExtent,
  ) {
    return barDelta * viewMaxScrollExtent / barMaxScrollExtent;
  }

  void _onVerticalDragStart(DragStartDetails details) {
    setState(() {
      _isDragInProcess = true;
    });
  }

  void _onHorizontalDragStart(DragStartDetails details) {
    setState(() {
      _isDragInProcess = true;
    });
  }

  void _onVerticalDragUpdate(DragUpdateDetails details) {
    setState(() {
      if (_isDragInProcess) {
        _barOffset += details.delta.dy;

        if (_barOffset < barMinScrollExtent) {
          _barOffset = barMinScrollExtent;
        }
        if (_barOffset > barMaxScrollExtent) {
          _barOffset = barMaxScrollExtent;
        }

        double viewDelta = getScrollViewDelta(
          details.delta.dy,
          barMaxScrollExtent,
          viewMaxScrollExtent,
        );

        _viewOffset = widget.controller.position.pixels + viewDelta;
        if (_viewOffset < widget.controller.position.minScrollExtent) {
          _viewOffset = widget.controller.position.minScrollExtent;
        }
        if (_viewOffset > viewMaxScrollExtent) {
          _viewOffset = viewMaxScrollExtent;
        }
        widget.controller.jumpTo(_viewOffset);
      }
    });
  }

  void _onHorizontalDragUpdate(DragUpdateDetails details) {
    setState(() {
      if (_isDragInProcess) {
        _barOffset += details.delta.dx;

        if (_barOffset < barMinScrollExtent) {
          _barOffset = barMinScrollExtent;
        }
        if (_barOffset > barMaxScrollExtent) {
          _barOffset = barMaxScrollExtent;
        }

        double viewDelta = getScrollViewDelta(
          details.delta.dx,
          barMaxScrollExtent,
          viewMaxScrollExtent,
        );

        _viewOffset = widget.controller.position.pixels + viewDelta;
        if (_viewOffset < widget.controller.position.minScrollExtent) {
          _viewOffset = widget.controller.position.minScrollExtent;
        }
        if (_viewOffset > viewMaxScrollExtent) {
          _viewOffset = viewMaxScrollExtent;
        }
        widget.controller.jumpTo(_viewOffset);
      }
    });
  }

  void _onVerticalDragEnd(DragEndDetails details) {
    setState(() {
      _isDragInProcess = false;
    });
  }

  void _onHorizontalDragEnd(DragEndDetails details) {
    setState(() {
      _isDragInProcess = false;
    });
  }
}
