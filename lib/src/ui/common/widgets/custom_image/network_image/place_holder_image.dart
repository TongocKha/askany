import 'package:askany/src/ui/common/widgets/shimmers/fade_simmer.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';

class PlaceHolderImage extends StatelessWidget {
  final double height;
  final double width;
  final BoxShape shape;
  const PlaceHolderImage({required this.height, required this.width, required this.shape});
  @override
  Widget build(BuildContext context) {
    return shape == BoxShape.circle
        ? FadeShimmer.round(
            size: height,
            highlightColor: colorGreen5,
            baseColor: colorGreen2,
          )
        : FadeShimmer(
            width: width,
            height: height,
            highlightColor: colorGreen5,
            baseColor: colorGreen2,
          );
  }
}
