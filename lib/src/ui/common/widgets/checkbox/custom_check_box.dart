import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class CustomCheckBox extends StatelessWidget {
  final bool isChecked;
  final Function(bool)? onChanged;
  CustomCheckBox({required this.isChecked, this.onChanged});
  @override
  Widget build(BuildContext context) {
    return TouchableOpacity(
      onTap: () {
        if (onChanged != null) {
          onChanged!(!isChecked);
        }
      },
      child: Container(
        height: 14.sp,
        width: 14.sp,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4),
          border: Border.all(color: colorGray1, width: 0.5),
        ),
        alignment: Alignment.center,
        child: isChecked
            ? Image.asset(
                iconCheckInBox,
                width: 6.25.sp,
              )
            : null,
      ),
    );
  }
}
