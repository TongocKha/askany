import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class DefaultCachedImage extends StatelessWidget {
  final double? height;
  final double? width;
  final BorderRadius radius;
  final BoxFit fit;
  DefaultCachedImage({
    this.height,
    this.width,
    required this.radius,
    this.fit = BoxFit.cover,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      width: width ?? null,
      height: height ?? null,
      padding: EdgeInsets.symmetric(horizontal: 12.sp),
      decoration: BoxDecoration(
        color: colorChosenCard,
        borderRadius: radius,
        image: DecorationImage(
          image: AssetImage(imageDefault),
          fit: BoxFit.fitWidth,
        ),
      ),
    );
  }
}
