import 'package:askany/src/ui/common/widgets/shimmers/fade_simmer.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';

class PlaceHolder extends StatelessWidget {
  final double? height;
  final double? width;
  const PlaceHolder({
    this.height,
    this.width,
  });
  @override
  Widget build(BuildContext context) {
    return FadeShimmer(
      width: width ?? null,
      height: height ?? null,
      highlightColor: colorGreen5,
      baseColor: colorGreen2,
    );
  }
}
