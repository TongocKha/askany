import 'package:askany/src/ui/common/widgets/service_image/default_cached_image.dart';
import 'package:askany/src/ui/common/widgets/service_image/place_holder.dart';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class CachedImageCustom extends StatelessWidget {
  final String? urlToImage;
  final double height;
  final double width;
  final BoxFit fit;
  final BorderRadius radius;

  final Widget? childCache;
  final AlignmentGeometry? alignment;
  final EdgeInsetsGeometry? padding;
  CachedImageCustom({
    required this.urlToImage,
    required this.height,
    required this.width,
    this.fit = BoxFit.cover,
    required this.radius,
    this.childCache,
    this.alignment,
    this.padding,
  });

  @override
  Widget build(BuildContext context) {
    return urlToImage == null
        ? DefaultCachedImage(
            height: height,
            width: width,
            fit: fit,
            radius: radius,
          )
        : CachedNetworkImage(
            imageUrl: urlToImage!,
            placeholder: (context, url) => ClipRRect(
                borderRadius: radius,
                child: PlaceHolder(
                  height: height,
                  width: width,
                )),
            imageBuilder: (context, image) => Container(
              width: width,
              height: height,
              decoration: BoxDecoration(
                borderRadius: radius,
                image: DecorationImage(
                  image: image,
                  fit: fit,
                ),
              ),
              alignment: alignment,
              child: childCache,
              padding: padding,
            ),
            errorWidget: (context, url, error) => DefaultCachedImage(
              height: height,
              width: width,
              radius: radius,
              fit: fit,
            ),
          );
  }
}
