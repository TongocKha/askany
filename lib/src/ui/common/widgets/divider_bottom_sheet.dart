import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class DividerBottomSheet extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 3.sp,
      width: 60.sp,
      decoration: BoxDecoration(
        color: colorDividerBottomSheet,
        borderRadius: BorderRadius.circular(30),
      ),
    );
  }
}
