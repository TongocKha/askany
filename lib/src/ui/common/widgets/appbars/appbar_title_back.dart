import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:flutter/services.dart';

AppBar appBarTitleBack(context, String title,
    {List<Widget>? actions,
    Function? onBackPressed,
    Color? backgroundColor,
    Brightness? brightness,
    double? paddingLeft,
    Color? colorChild,
    PreferredSizeWidget? bottom,
    Widget? titleWidget,
    Widget? leading,
    double? elevation}) {
  return AppBar(
    systemOverlayStyle: SystemUiOverlayStyle(
      statusBarBrightness: brightness ?? Theme.of(context).brightness,
    ),
    elevation: elevation ?? 0.0,
    backgroundColor: backgroundColor ?? Colors.transparent,
    automaticallyImplyLeading: false,
    centerTitle: true,
    leadingWidth: 40.sp,
    title: titleWidget ??
        Text(
          title,
          style: TextStyle(
            fontSize: 15.sp,
            fontWeight: FontWeight.w700,
            color: backgroundColor != null ? colorWhiteCard : colorBlack2,
          ),
        ),
    leading: leading ??
        TouchableOpacity(
          onTap: () {
            if (onBackPressed != null) {
              onBackPressed();
            } else {
              AppNavigator.pop();
            }
          },
          child: Padding(
            padding: EdgeInsets.only(left: paddingLeft ?? 3.sp),
            child: Image.asset(
              iconBack,
              width: 33.sp,
              height: 33.sp,
              color: backgroundColor != null ? (colorChild ?? colorWhiteCard) : null,
            ),
          ),
        ),
    actions: actions,
    bottom: bottom,
  );
}
