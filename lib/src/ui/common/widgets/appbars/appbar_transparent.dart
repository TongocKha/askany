// import 'package:askany/src/ui/style/calendar_style.dart';
import 'package:askany/src/ui/style/category_style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:flutter/services.dart';

AppBar appBarCategory({
  required String title,
  List<Widget>? actions,
  PreferredSizeWidget? bottom,
}) {
  return AppBar(
    title: Padding(
      padding: EdgeInsets.only(left: 3.sp),
      child: Text(
        title,
        style: categoryTitleStyle,
      ),
    ),
    backgroundColor: Colors.transparent,
    actions: actions,
    centerTitle: false,
    elevation: 0,
    systemOverlayStyle: SystemUiOverlayStyle(
      statusBarBrightness: Brightness.light,
    ),
    bottom: bottom,
  );
}
