import 'package:askany/src/bloc/timer/timer_bloc.dart';
import 'package:askany/src/helpers/date_time_helper.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

AppBar appbarStartCall({
  required BuildContext context,
  required String title,
  required Function onPressedBack,
  required Function onPressedSwitchCamera,
  required Function onPressedBackChat,
  required Function onPressedPeopleInvited,
  required Function onPressedFullScreen,
  required Orientation orientation,
}) {
  return AppBar(
    // backgroundColor: Colors.transparent,
    backgroundColor: colorBlack1.withOpacity(0.1),
    elevation: 0,
    leading: IconButton(
        icon: Center(
          child: Image.asset(
            iconArrowBack,
            height: 60.sp,
            width: 60.sp,
            color: colorBackgroundGrey,
          ),
        ),
        onPressed: () {
          onPressedBack();
          // showAlert(context);
        }),
    title: Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "$title",
            style: TextStyle(
                fontSize: 12.sp,
                color: (orientation == Orientation.portrait)
                    ? Colors.white
                    : Colors.grey.withOpacity(0.3)),
            overflow: TextOverflow.ellipsis,
          ),
          BlocBuilder<TimerBloc, TimerState>(
            builder: (context, state) {
              return Text(
                state is TimerRunInProgress
                    ? getDurationCalling(
                        durationSeconds: state.duration,
                      )
                    : "00:00:00",
                style: TextStyle(fontSize: 12.sp, color: Colors.grey),
                overflow: TextOverflow.ellipsis,
              );
            },
          )
        ],
      ),
    ),
    centerTitle: false,
    actions: [
      IconButton(
          icon: Center(
            child: Image.asset(
              iconFlipCamera,
              height: 45.sp,
              width: 45.sp,
              color: colorBackgroundGrey,
            ),
          ),
          onPressed: () {
            onPressedSwitchCamera();

            // showAlert(context);
          }),
      Theme(
        data: ThemeData.dark(),
        child: PopupMenuButton(
          offset: const Offset(-25.0, 45.0),
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(15.0))),
          icon: Center(
            child: Image.asset(
              iconMenuVertical,
              height: 18.sp,
              width: 40.sp,
              color: colorBackgroundGrey,
            ),
          ),
          color: colorBlack2,
          onSelected: (value) {
            if (value == 0) {
              onPressedBackChat();
            } else if (value == 1) {
              onPressedPeopleInvited();
            } else if (value == 2) {
              onPressedFullScreen();
            }
          },
          itemBuilder: (context) {
            return [
              PopupMenuItem(
                  value: 0,
                  child: Row(
                    children: [
                      Center(
                        child: Image.asset(
                          iconBubbleChat,
                          height: 20.sp,
                          width: 20.sp,
                          color: colorBackgroundGrey,
                        ),
                      ),
                      Text("    Tin nhắn trong cuộc gọi")
                    ],
                  )),
              PopupMenuItem(
                  value: 1,
                  child: Row(
                    children: [
                      Center(
                        child: Image.asset(
                          iconPersonOutline,
                          height: 20.sp,
                          width: 20.sp,
                          color: colorBackgroundGrey,
                        ),
                      ),
                      Text("    Người tham gia")
                    ],
                  )),
              PopupMenuItem(
                value: 2,
                child: Row(
                  children: [
                    Center(
                      child: Image.asset(
                        iconFullScreen,
                        height: 20.sp,
                        width: 20.sp,
                        color: colorBackgroundGrey,
                      ),
                    ),
                    Text("    Toàn màn hình")
                  ],
                ),
              ),
              // PopupMenuItem(
              //   value: 2,
              //   child: Row(
              //     children: [
              //       Center(
              //         child: Image.asset(
              //           iconCamera,
              //           height: 20.sp,
              //           width: 20.sp,
              //           color: colorBackgroundGrey,
              //         ),
              //       ),
              //       Text("    Quay màn hình")
              //     ],
              //   ),
              // ),
            ];
          },
        ),
      )
    ],
    systemOverlayStyle: SystemUiOverlayStyle(
      statusBarBrightness: Brightness.dark,
    ),
  );
}
