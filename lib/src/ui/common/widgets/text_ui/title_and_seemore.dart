import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class TitleAndSeeMore extends StatelessWidget {
  final String title;
  final bool checkSeeMore;
  final Function? handlePressed;
  const TitleAndSeeMore({
    required this.title,
    this.checkSeeMore = true,
    this.handlePressed,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title,
            style: TextStyle(
              color: colorBlack1,
              fontSize: 14.sp,
              fontWeight: FontWeight.w600,
            ),
          ),
          Visibility(
            visible: checkSeeMore,
            child: TouchableOpacity(
              onTap: () {
                if (UserLocal().getAccessToken() == '') {
                  AppNavigator.push(Routes.CHOOSE_ACCOUNT);
                } else {
                  if (handlePressed != null) {
                    handlePressed!();
                  }
                }
              },
              child: Row(
                children: [
                  Text(
                    Strings.viewAll.i18n,
                    style: TextStyle(
                      color: colorGreen2,
                      fontSize: 11.sp,
                    ),
                  ),
                  SizedBox(
                    width: 4.sp,
                  ),
                  Image.asset(
                    iconNextHome,
                    color: colorGreen2,
                    width: 10.sp,
                    height: 10.sp,
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
