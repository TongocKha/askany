import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class BottomDelete extends StatefulWidget {
  final Function handlePressed;
  final String text;
  const BottomDelete({
    required this.handlePressed,
    this.text = 'Bạn có chắc chắn muốn xoá\ncuộc trò chuyện này?',
    // Strings.areYouSureToDeleteThisChat.i18n,
  });
  @override
  State<StatefulWidget> createState() => _BottomDeleteState();
}

class _BottomDeleteState extends State<BottomDelete> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 12.sp, horizontal: 10.sp),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            decoration: BoxDecoration(
              color: Colors.grey.shade100,
              borderRadius: BorderRadius.circular(8.sp),
            ),
            child: Column(
              children: [
                SizedBox(height: 4.sp),
                Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 16.sp, vertical: 12.sp),
                  child: Text(
                    widget.text,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 12.sp,
                      height: 1.35,
                    ),
                  ),
                ),
                dividerChat,
                _buildOption(
                  text: Strings.delete.i18n,
                  isDanger: true,
                  handlePressed: widget.handlePressed,
                ),
              ],
            ),
          ),
          SizedBox(height: 4.sp),
          Container(
            height: 40.sp,
            decoration: BoxDecoration(
              color: Colors.grey.shade100,
              borderRadius: BorderRadius.circular(8.sp),
            ),
            child: _buildOption(
              text: Strings.cancel.i18n,
              handlePressed: () {},
            ),
          ),
          SizedBox(height: 4.sp),
        ],
      ),
    );
  }

  Widget _buildOption({
    required String text,
    required Function handlePressed,
    bool isDanger = false,
  }) {
    return TouchableOpacity(
      onTap: () {
        AppNavigator.pop();
        handlePressed();
      },
      child: Container(
        height: 42.sp,
        alignment: Alignment.center,
        color: Colors.transparent,
        child: Text(
          text,
          style: TextStyle(
            color: isDanger ? Colors.red.shade700 : headerCalendarColor,
            fontSize: 13.sp,
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
    );
  }
}
