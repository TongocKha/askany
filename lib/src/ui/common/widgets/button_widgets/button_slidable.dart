import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ButtonSlidable extends StatelessWidget {
  final String title;
  final Function handlePressed;
  final Color backgroundcolor;
  final Color colorIcon;
  final String? imageAsset;
  final Color colorText;
  ButtonSlidable({
    required this.title,
    required this.handlePressed,
    required this.backgroundcolor,
    required this.colorIcon,
    required this.imageAsset,
    this.colorText = Colors.white,
  });
  @override
  Widget build(BuildContext context) {
    return TouchableOpacity(
      onTap: handlePressed,
      child: Align(
        alignment: Alignment.centerRight,
        child: Container(
          height: 68.sp,
          width: 68.sp,
          color: backgroundcolor,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: 2.5.sp),
              Image.asset(
                imageAsset!,
                width: 18.sp,
                height: 18.sp,
                color: colorIcon,
              ),
              SizedBox(height: 4.sp),
              Text(
                title,
                style: TextStyle(
                  color: colorText,
                  fontSize: 10.sp,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
