import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

import 'package:askany/src/bloc/badge/badge_bloc.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/badges/badges.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/text_ui/text_ui.dart';
import 'package:askany/src/ui/style/style.dart';

class ButtonMessage extends StatefulWidget {
  final EdgeInsets? padding;
  final String iconHeader;
  const ButtonMessage({
    Key? key,
    this.padding,
    required this.iconHeader,
  }) : super(key: key);

  @override
  _ButtonMessageState createState() => _ButtonMessageState();
}

class _ButtonMessageState extends State<ButtonMessage> with TickerProviderStateMixin {
  late AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(milliseconds: DELAY_50_MS),
      vsync: this,
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return TouchableOpacity(
      onTap: () {
        _controller.forward().then((value) {
          _controller.reverse();
          if (UserLocal().getAccessToken() == '') {
            AppNavigator.push(Routes.CHOOSE_ACCOUNT);
          } else {
            AppNavigator.push(Routes.CHAT);
          }
        });
      },
      child: BlocBuilder<BadgesBloc, BadgesState>(
        builder: (context, state) {
          int badges = state is GetBadgesDone ? state.badgesChat : 0;
          return ScaleTransition(
            scale: Tween(begin: 1.0, end: 0.8).animate(
              CurvedAnimation(
                curve: Curves.bounceOut,
                parent: _controller,
              ),
            ),
            child: Container(
              alignment: Alignment.centerRight,
              padding: widget.padding,
              color: Colors.transparent,
              child: Badge(
                badgeContent: TextUI(
                  '$badges',
                  color: colorBlack1,
                  fontSize: 8.5.sp,
                  fontWeight: FontWeight.w700,
                ),
                showBadge: badges > 0,
                badgeColor: colorBackgroundBadges,
                child: Image.asset(
                  widget.iconHeader,
                  width: 20.sp,
                  height: 20.sp,
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
