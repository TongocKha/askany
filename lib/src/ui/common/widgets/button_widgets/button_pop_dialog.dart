import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ButtonPopDialog extends StatelessWidget {
  final String? route;
  const ButtonPopDialog({
    Key? key,
    required String text,
    this.route,
  })  : _text = text,
        super(key: key);

  final String _text;

  @override
  Widget build(BuildContext context) {
    return TouchableOpacity(
      onTap: () {
        route != null ? AppNavigator.popUntil(Routes.MY_SERVICE) : AppNavigator.pop();
      },
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          color: colorDialogBackGround,
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(12.sp),
            bottomRight: Radius.circular(12.sp),
          ),
        ),
        constraints: BoxConstraints(maxHeight: 46.sp),
        alignment: Alignment.center,
        child: Text(
          _text.toUpperCase(),
          style: TextStyle(
            color: colorGreen2,
            fontSize: 12.sp,
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
    );
  }
}
