import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ButtonPrimary extends StatelessWidget {
  final Function onPressed;
  final String text;
  final FontWeight? fontWeight;
  final double? width;
  ButtonPrimary({
    required this.onPressed,
    required this.text,
    this.fontWeight,
    this.width,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      width: width ?? double.infinity,
      child: TouchableOpacity(
        onTap: () async {
          onPressed();
        },
        child: Container(
          margin: EdgeInsets.only(
            bottom: 8.sp,
          ),
          height: 36.sp,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6.5.sp),
            color: Color(0xff1C4843),
          ),
          alignment: Alignment.center,
          child: Text(
            text,
            style: TextStyle(
              color: Colors.white,
              fontSize: 13.sp,
              fontWeight: fontWeight,
            ),
          ),
        ),
      ),
    );
  }
}
