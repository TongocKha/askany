import 'package:askany/src/ui/common/widgets/cake_avatar/default_cached_cake_image.dart';
import 'package:askany/src/ui/common/widgets/cake_avatar/place_holder_cake.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class CachedCakeImage extends StatelessWidget {
  final String? urlToImage;
  final double size;
  final BoxFit fit;
  final BorderRadius radius;
  const CachedCakeImage({
    required this.urlToImage,
    required this.size,
    required this.radius,
    this.fit = BoxFit.fitHeight,
  });
  @override
  Widget build(BuildContext context) {
    return urlToImage == null
        ? DefaultCachedCakeImage(
            size: size,
            radius: radius,
            fit: fit,
          )
        : CachedNetworkImage(
            imageUrl: urlToImage!,
            imageBuilder: (context, imageProvider) => Container(
              height: size,
              width: size,
              decoration: BoxDecoration(
                borderRadius: radius,
                image: DecorationImage(
                  image: imageProvider,
                  fit: fit,
                ),
              ),
            ),
            placeholder: (context, url) => ClipRRect(
              borderRadius: radius,
              child: PlaceHolderCake(
                size: size,
              ),
            ),
            errorWidget: (context, url, error) => DefaultCachedCakeImage(
              size: size,
              radius: radius,
              fit: fit,
            ),
          );
  }
}
