import 'package:askany/src/ui/common/widgets/cake_avatar/cached_cake_image.dart';
import 'package:askany/src/ui/common/widgets/custom_image/network_image/cached_image.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class CakeAvatar extends StatelessWidget {
  final List<String?> urls;
  final double size;
  const CakeAvatar({required this.urls, required this.size});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: size,
      width: size,
      child: urls.length < 2
          ? CustomNetworkImage(
              height: size,
              width: size,
              urlToImage: urls.isEmpty ? null : urls.first,
            )
          : Row(
              children: [
                Expanded(child: halfLeft()),
                SizedBox(width: 1.sp),
                Expanded(child: halfRight()),
              ],
            ),
    );
  }

  Widget halfLeft() {
    return CachedCakeImage(
      size: size,
      radius: BorderRadius.horizontal(
        left: Radius.circular(21.sp),
      ),
      urlToImage: urls.isEmpty ? null : urls.first,
    );
  }

  Widget halfRight() {
    return urls.length == 2
        ? CachedCakeImage(
            size: size,
            urlToImage: urls.length < 2 ? null : urls[1],
            radius: BorderRadius.horizontal(
              right: Radius.circular(21.sp),
            ),
            fit: BoxFit.cover,
          )
        : Column(
            children: [
              Expanded(
                child: CachedCakeImage(
                  size: size / 2,
                  urlToImage: urls.length < 2 ? null : urls[1],
                  radius: BorderRadius.only(
                    topRight: Radius.circular(21.sp),
                  ),
                  fit: BoxFit.fitWidth,
                ),
              ),
              SizedBox(height: 1.sp),
              Expanded(
                child: CachedCakeImage(
                  size: size / 2,
                  urlToImage: urls.length < 3 ? null : urls[2],
                  radius: BorderRadius.only(
                    bottomRight: Radius.circular(21.sp),
                  ),
                  fit: BoxFit.fitWidth,
                ),
              ),
            ],
          );
  }
}
