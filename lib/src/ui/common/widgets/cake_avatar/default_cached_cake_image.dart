import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';

class DefaultCachedCakeImage extends StatelessWidget {
  final double size;
  final BorderRadius radius;
  final BoxFit fit;
  const DefaultCachedCakeImage({
    required this.size,
    required this.radius,
    required this.fit,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      height: size,
      width: size,
      decoration: BoxDecoration(
        borderRadius: radius,
        image: DecorationImage(
          image: AssetImage(avatarDefault),
          fit: fit,
        ),
      ),
    );
  }
}
