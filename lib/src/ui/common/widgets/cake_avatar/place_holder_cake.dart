import 'package:askany/src/ui/common/widgets/shimmers/fade_simmer.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';

class PlaceHolderCake extends StatelessWidget {
  final double size;
  const PlaceHolderCake({required this.size});
  @override
  Widget build(BuildContext context) {
    return FadeShimmer(
      width: size,
      height: size,
      highlightColor: colorGreen5,
      baseColor: colorGreen2,
    );
  }
}
