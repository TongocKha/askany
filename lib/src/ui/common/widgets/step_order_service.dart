import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class StepOrderService extends StatelessWidget {
  final List<String> steps;
  final int currentStep;
  final Function(int) onStepChanged;
  const StepOrderService({
    required this.steps,
    required this.currentStep,
    required this.onStepChanged,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100.w,
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ...steps
                  .asMap()
                  .entries
                  .map((item) => _buildStep(index: item.key, step: item.value))
                  .toList(),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildStep({required int index, required String step}) {
    return Expanded(
      child: TouchableOpacity(
        onTap: () {
          if (index < currentStep) {
            onStepChanged(index);
          }
        },
        child: _buildCircle(index: index, step: step),
      ),
    );
  }

  Widget _buildCircle({required int index, required String step}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Padding(
          padding: EdgeInsets.only(
            left: index == 0 ? 40.sp : 0,
            right: index == steps.length - 1 ? 40.sp : 0,
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              index > 0 ? _buildDivider(index) : SizedBox(),
              Container(
                height: 16.sp,
                width: 16.sp,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: index == currentStep
                      ? Colors.white
                      : index < currentStep
                          ? colorGreen2
                          : colorGrey3,
                  border: index == currentStep
                      ? Border.all(
                          color: colorGreen2,
                          width: 1.5.sp,
                        )
                      : null,
                ),
                alignment: Alignment.center,
                child: index < currentStep
                    ? Icon(
                        Icons.check,
                        color: Colors.white,
                        size: 10.sp,
                      )
                    : null,
              ),
              index < steps.length - 1 ? _buildDivider(index + 1) : SizedBox(),
            ],
          ),
        ),
        SizedBox(height: 12.sp),
        Text(
          step,
          style: TextStyle(
            color: colorBlack2,
            fontSize: 10.sp,
          ),
        ),
      ],
    );
  }

  Widget _buildDivider(int index) {
    return Expanded(
      child: Divider(
        color: index <= currentStep ? colorGreen2 : colorGrey3,
        thickness: 1.sp,
        height: 1.sp,
      ),
    );
  }
}
