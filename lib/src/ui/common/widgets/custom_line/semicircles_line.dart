import 'package:flutter/material.dart';

class SemicirclesLine extends StatelessWidget {
  const SemicirclesLine({
    Key? key,
    this.direction = Axis.horizontal,
    this.lineLength = double.infinity,
    this.lineThickness = 1.0,
    // this.semicircleDiameter = 4.0,
    this.semicircleColor = Colors.black,
    this.gapLength = 4.0,
    this.gapColor = Colors.transparent,
    this.gapRadius = 0.0,
  }) : super(key: key);

  final Axis direction;
  final double lineLength;
  final double lineThickness;
  // final double semicircleDiameter;
  final Color semicircleColor;
  final double gapLength;
  final Color gapColor;
  final double gapRadius;

  @override
  Widget build(BuildContext context) {
    final isHorizontal = direction == Axis.horizontal;

    return SizedBox(
      width: isHorizontal ? lineLength : lineThickness,
      height: isHorizontal ? lineThickness : lineLength,
      child: LayoutBuilder(
        builder: (context, constraints) {
          final lineLength = _getLineLength(constraints, isHorizontal);
          final dashAndDashGapCount =
              _calculateSemicircleAndGapCount(lineLength);
          final dashCount = dashAndDashGapCount[0];
          final dashGapCount = dashAndDashGapCount[1];

          return Wrap(
            direction: direction,
            children: List.generate(dashCount + dashGapCount, (index) {
              if (index % 2 == 0) {
                final dash = _buildSemicircle(isHorizontal, semicircleColor);
                return dash;
              } else {
                final dashGap = _buildGap(isHorizontal, gapColor);
                return dashGap;
              }
            }).toList(growable: false),
          );
        },
      ),
    );
  }

  double _getLineLength(BoxConstraints constraints, bool isHorizontal) {
    return lineLength == double.infinity
        ? isHorizontal
            ? constraints.maxWidth
            : constraints.maxHeight
        : lineLength;
  }

  List<int> _calculateSemicircleAndGapCount(double lineLength) {
    var dashAndDashGapLength = lineThickness * 2 + gapLength;
    var dashCount = lineLength ~/ dashAndDashGapLength;
    var dashGapCount = lineLength ~/ dashAndDashGapLength;
    if (lineThickness * 2 <= lineLength % dashAndDashGapLength) {
      dashCount += 1;
    }
    return [dashCount, dashGapCount];
  }

  Widget _buildSemicircle(bool isHorizontal, Color color) {
    return Container(
      decoration: BoxDecoration(
        color: color,
        borderRadius: isHorizontal
            ? BorderRadius.only(
                topRight: Radius.circular(lineThickness * 2),
                topLeft: Radius.circular(lineThickness * 2),
              )
            : BorderRadius.only(
                topRight: Radius.circular(lineThickness * 2),
                bottomRight: Radius.circular(lineThickness * 2),
              ),
      ),
      width: isHorizontal ? lineThickness * 2 : lineThickness,
      height: isHorizontal ? lineThickness : lineThickness * 2,
    );
  }

  Widget _buildGap(bool isHorizontal, Color color) {
    return Container(
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(gapRadius),
      ),
      width: isHorizontal ? gapLength : lineThickness,
      height: isHorizontal ? lineThickness : gapLength,
    );
  }
}
