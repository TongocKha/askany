import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/checkbox/custom_check_box.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class TextFieldCheckBox extends StatelessWidget {
  final Widget child;
  final Function onChanged;
  final bool isCheck;
  TextFieldCheckBox({
    required this.child,
    required this.onChanged,
    required this.isCheck,
  });

  @override
  Widget build(BuildContext context) {
    return TouchableOpacity(
      onTap: onChanged,
      child: Container(
        padding: EdgeInsets.symmetric(
          vertical: 8.sp,
          horizontal: 12.sp,
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(6.sp),
          color: Colors.transparent,
          border: Border.all(
            color: colorBorderTextField,
            width: 0.5.sp,
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              child: Container(
                child: child,
              ),
            ),
            CustomCheckBox(
              isChecked: isCheck,
              onChanged: (val) {
                onChanged();
              },
            ),
          ],
        ),
      ),
    );
  }
}
