import 'package:askany/src/helpers/input_formatter/hour_formatter.dart';
import 'package:askany/src/helpers/utils/validator_utils.dart';
import 'package:askany/src/models/expert_timeline_model.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_form.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class TextFieldInputHour extends StatefulWidget {
  final Function() onTap;
  final String initialValue;
  final Function(TimelineModel) onChanged;
  const TextFieldInputHour(
      {required this.onTap,
      required this.initialValue,
      required this.onChanged});
  @override
  State<StatefulWidget> createState() => _TextFieldInputHourState();
}

class _TextFieldInputHourState extends State<TextFieldInputHour> {
  TextEditingController textEditingController = TextEditingController();

  @override
  void initState() {
    super.initState();
    textEditingController.text = widget.initialValue;

    textEditingController.addListener(() {
      String text = textEditingController.text;
      List<String> values = text.split(' - ');
      TimeOfDay startTime = TimeOfDay(
        hour: int.parse(values[0].split(':')[0]),
        minute: int.parse(
          values[0].split(':')[1],
        ),
      );
      TimeOfDay endTime = TimeOfDay(
        hour: int.parse(values[1].split(':')[0]),
        minute: int.parse(
          values[1].split(':')[1],
        ),
      );

      widget.onChanged(
        TimelineModel(
          startTime: startTime,
          endTime: endTime,
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return TextFieldForm(
      fontSize: 11.25.sp,
      contentPadding: EdgeInsets.fromLTRB(10.sp, 12.25.sp, 0, 8.sp),
      controller: textEditingController,
      inputFormatters: [
        HourFormatter(),
      ],
      onTap: () {
        widget.onTap();
        if (textEditingController.text == '00:00 - 00:00') {
          textEditingController.selection = TextSelection.fromPosition(
            TextPosition(offset: 0),
          );
        } else {
          int cursorPosition = textEditingController.selection.base.offset;
          if ([2, 10].contains(cursorPosition)) {
            textEditingController.selection = TextSelection.fromPosition(
              TextPosition(offset: cursorPosition++),
            );
          } else if ([5, 6, 7].contains(cursorPosition)) {
            textEditingController.selection = TextSelection.fromPosition(
              TextPosition(offset: 8),
            );
          }
        }
      },
      initialValue: '',
      validatorForm: (val) => ValidatorUtils.checkStartEndTime(val),
    );
  }
}
