import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class TextFieldOTP extends StatefulWidget {
  final Function(String) onChanged;
  TextFieldOTP({required this.onChanged});
  @override
  State<StatefulWidget> createState() => _TextFieldOTPState();
}

class _TextFieldOTPState extends State<TextFieldOTP> {
  late List<TextEditingController> _controllers;
  late List<FocusNode> _focusNodes;

  @override
  void initState() {
    super.initState();
    _controllers = List.generate(6, (index) => TextEditingController(text: '\u200B'));
    _focusNodes = List.generate(6, (index) => FocusNode());
    _focusNodes[0].requestFocus();

    _focusNodes.asMap().forEach((index, node) {
      node.addListener(() {
        if (node.hasFocus) {
          for (int indexCheck = 0; indexCheck < index; indexCheck++) {
            if (_controllers[indexCheck].text == '\u200B') {
              _focusNodes[indexCheck].requestFocus();
            }
          }

          for (int indexCheck = _focusNodes.length - 1; indexCheck >= index; indexCheck--) {
            if (_controllers[indexCheck].text != '\u200B') {
              _controllers[indexCheck].text = '\u200B';
            }
          }
        }
      });
    });
  }

  @override
  void dispose() {
    _focusNodes.forEach((node) => node.dispose());
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          ...List.generate(6, (index) => _buildTextFormField(index)).toList(),
        ],
      ),
    );
  }

  Widget _buildTextFormField(int index) {
    return Container(
      width: 37.sp,
      height: 37.sp,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(6.5.sp),
        border: Border.all(
          color: colorGray2,
          width: 0.5.sp,
        ),
        color: Colors.white,
      ),
      child: TextFormField(
        controller: _controllers[index],
        focusNode: _focusNodes[index],
        decoration: InputDecoration(
          isDense: true,
          border: InputBorder.none,
          contentPadding: EdgeInsets.only(
            top: 8.5.sp,
            left: 2.5.sp,
          ),
        ),
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: 17.sp,
          fontWeight: FontWeight.w400,
          color: colorBlack2,
        ),
        cursorColor: headerCalendarColor,
        cursorRadius: Radius.circular(30.sp),
        keyboardType: TextInputType.number,
        inputFormatters: [
          FilteringTextInputFormatter.digitsOnly,
          LengthLimitingTextInputFormatter(1),
        ],
        onChanged: (val) {
          _controllers[index].text = val.trimRight();
          String codeOTP = _controllers.map((controller) => controller.text).join();
          if (val.length == 1 && index != 5) {
            _controllers[index + 1].text = '\u200B';
            FocusScope.of(context).nextFocus();
          }

          if ((val.length == 0) && index != 0) {
            _controllers[index].text = '\u200B';
            FocusScope.of(context).previousFocus();
          }

          if (index == 5 && val.length == 1) {
            _focusNodes[index].unfocus();
          }

          widget.onChanged(codeOTP);
        },
      ),
    );
  }
}
