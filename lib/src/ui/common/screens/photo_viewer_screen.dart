import 'dart:io';
import 'dart:typed_data';

import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/message_images/message_images_bloc.dart';
import 'package:askany/src/data/remote_data_source/image_repository.dart';
import 'package:askany/src/helpers/image_downloader_helper.dart';
import 'package:askany/src/models/avatar_model.dart';
import 'package:askany/src/models/message_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

import 'package:path_provider/path_provider.dart';

class PhotoViewScreen extends StatefulWidget {
  final List<ImageProvider>? images;
  final List<String>? urlToImages;
  final int initPosition;
  final MessageModel indexListImage;

  const PhotoViewScreen({
    required this.images,
    required this.initPosition,
    required this.urlToImages,
    required this.indexListImage,
  });

  @override
  _PhotoViewScreenState createState() => _PhotoViewScreenState();
}

class _PhotoViewScreenState extends State<PhotoViewScreen>
    with SingleTickerProviderStateMixin {
  late int currentIndex = widget.initPosition;
  File? file;
  Future<void> _convertUrltoFile(String url) async {
    showDialogLoading();
    Uint8List? imageBuffer = await ImageRepository().getByteArrayFromUrl(url);
    final directory = (await getApplicationDocumentsDirectory()).path;
    await Directory('$directory/sample').create(recursive: true);
    final fullPath =
        '$directory/sample/${DateTime.now().millisecondsSinceEpoch}.png';
    File _file = File(fullPath);
    setState(() {
      _file.writeAsBytesSync(imageBuffer!);
      file = _file;
    });
    AppNavigator.pop();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  List<double> doubleTapScales = [1.0, 2.0];
  int? indexImage;
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MessageImagesBloc, MessageImagesState>(
      builder: (context, state) {
        List<AvatarModel> images = (state.props[1] as List).cast();

        int indexOf = images.indexWhere((indexItem) =>
            widget.indexListImage.images![currentIndex].id == indexItem.id);

        return Scaffold(
            extendBodyBehindAppBar: true,
            appBar: appBarTitleBack(
              context,
              '',
              backgroundColor: Colors.transparent,
              brightness: Brightness.dark,
              actions: [
                TouchableOpacity(
                  onTap: () async {
                    if (indexImage == null) {
                      indexImage = indexOf;
                    }
                    await _convertUrltoFile(
                        images.map((e) => e.urlToImage).toList()[indexImage!]);
                    AppNavigator.push(
                      Routes.EDIT_PHOTO_MESSAGE,
                      arguments: {
                        'file': file,
                      },
                    );
                  },
                  child: Container(
                    alignment: Alignment.center,
                    color: Colors.transparent,
                    padding: EdgeInsets.only(
                      right: 10.sp,
                    ),
                    child: Text(
                      'Edit',
                      style: TextStyle(
                        color: colorWhite,
                        fontSize: 13.sp,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 4.sp),
                Visibility(
                  visible: widget.urlToImages != null &&
                      widget.urlToImages!.isNotEmpty,
                  child: TouchableOpacity(
                    onTap: () {
                      DownloaderHelper()
                          .downloadImage(widget.urlToImages![currentIndex]);
                    },
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: Container(
                        color: Colors.transparent,
                        child: Image.asset(
                          iconDownloads,
                          width: 20.sp,
                          height: 20.sp,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 8.sp),
              ],
            ),
            body: Container(
              color: Colors.black,
              child: Stack(
                children: [
                  PhotoViewGallery.builder(
                      itemCount: images.length,
                      builder: (context, index) {
                        return PhotoViewGalleryPageOptions(
                          imageProvider: getImages(images)[index],
                          initialScale: PhotoViewComputedScale.contained * 1,
                          minScale: PhotoViewComputedScale.contained * .75,
                          maxScale: PhotoViewComputedScale.covered * 1.5,
                        );
                      },
                      loadingBuilder: (context, progress) => Center(
                            child: Container(
                              width: 20.sp,
                              height: 20.sp,
                              child: CupertinoActivityIndicator(
                                radius: 14.sp,
                              ),
                            ),
                          ),
                      pageController: PageController(initialPage: indexOf),
                      onPageChanged: (newIndex) {
                        setState(() {
                          indexImage = newIndex;
                          if (newIndex == images.length - 1) {
                            return AppBloc.messageImageBloc.add(
                              GetMessgesImagesEvent(
                                  conversationId:
                                      widget.indexListImage.conversation),
                            );
                          }
                        });
                      }),
                ],
              ),
            ));
      },
    );
  }

  List<ImageProvider> getImages(List<AvatarModel> images) {
    if (images.isNotEmpty) {
      return images.map((e) => NetworkImage(e.urlToImage)).toList();
    }

    return [];
  }
}
