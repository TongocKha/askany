import 'package:askany/src/ui/common/widgets/shimmers/fade_simmer.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class CategoryContentShimmerCard extends StatelessWidget {
  const CategoryContentShimmerCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        right: 10.sp,
        bottom: 7.sp,
      ),
      padding: EdgeInsets.symmetric(
        horizontal: 18.sp,
        vertical: 15.sp,
      ),
      width: 110.sp,
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(.08),
            blurRadius: 4.sp,
            offset: Offset(2.sp, 2.sp),
          ),
        ],
        borderRadius: BorderRadius.all(
          Radius.circular(12.sp),
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          FadeShimmer(
            width: 30.sp,
            height: 30.sp,
            fadeTheme: FadeTheme.lightReverse,
          ),
          SizedBox(height: 8.sp),
          FadeShimmer(
            width: 55.sp,
            height: 13.sp,
            fadeTheme: FadeTheme.lightReverse,
          )
        ],
      ),
    );
  }
}
