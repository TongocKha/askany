import 'package:askany/src/ui/style/category_style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class CategoryItemLogo extends StatelessWidget {
  final String urlToIcon;
  const CategoryItemLogo({
    Key? key,
    required this.urlToIcon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        SizedBox(
          width: double.infinity,
          height: 60.sp,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Image.asset(
                    categoryItemBackground,
                    width: 50.sp,
                    height: 50.sp,
                  ),
                ],
              ),
              SizedBox(
                width: 3.sp,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Image.asset(
                    categoryItemBackground,
                    width: 20.sp,
                    height: 20.sp,
                  ),
                  SizedBox(
                    height: 5.sp,
                  )
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 5.sp,
                  ),
                  Image.asset(
                    categoryItemBackground,
                    width: 7.sp,
                    height: 7.sp,
                  ),
                ],
              )
            ],
          ),
        ),
        SizedBox(
          width: double.infinity,
          height: 60.sp,
          child: Center(
            child: SizedBox(
              width: 45.sp,
              height: 45.sp,
              child: Image.network(
                urlToIcon,
                fit: BoxFit.fill,
              ),
            ),
          ),
        )
      ],
    );
  }
}
