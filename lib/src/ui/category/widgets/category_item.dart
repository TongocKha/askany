import 'package:askany/src/configs/lang/language_service.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/models/category_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:askany/src/ui/style/style.dart';

class CategoryItem extends StatelessWidget {
  final CategoryInfoModel categories;
  const CategoryItem({
    Key? key,
    required this.categories,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (UserLocal().getAccessToken() == '') {
          AppNavigator.push(Routes.CHOOSE_ACCOUNT);
        } else {
          AppNavigator.push(Routes.SUB_CATEGORY, arguments: {
            'categoryInfo': categories,
          });
        }
      },
      child: Container(
        decoration: BoxDecoration(
          color: colorAddButton,
          borderRadius: BorderRadius.all(
            Radius.circular(12.sp),
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              width: 60.sp,
              height: 60.sp,
              child: Stack(
                children: [
                  Image.asset(
                    categoryItemBackground,
                    width: 60.sp,
                    height: 60.sp,
                  ),
                  Center(
                    child: Image.network(
                        LanguageService.getIsLanguage('vi')
                    ? (categories.categoryInfo.icon?.urlToImage ?? '')
                    : (categories.categoryInfo.thumbnail?.urlToImage ?? ''),
                      fit: BoxFit.fill,
                      width: 35.sp,
                      height: 35.sp,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 8.sp),
            Text(
              categories.categoryInfo.name,
              style: categoryItemStyle,
            )
          ],
        ),
      ),
    );
  }
}
