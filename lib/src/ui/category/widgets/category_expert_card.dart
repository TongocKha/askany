import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/helpers/province_helper.dart';
import 'package:askany/src/models/expert_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/style/category_style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/cake_avatar/cached_cake_image.dart';
import 'package:askany/src/ui/common/widgets/text_ui/text_ui.dart';
import 'package:askany/src/ui/style/calendar_style.dart';
import 'package:askany/src/ui/style/request_style.dart';

class ExpertCard extends StatelessWidget {
  final ExpertModel expertModel;
  ExpertCard({
    Key? key,
    required this.expertModel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TouchableOpacity(
      onTap: () {
        AppNavigator.push(Routes.DETAILS_SPECIALIST, arguments: {
          'expertId': expertModel.id,
          'specialtyId': expertModel.specialties.isEmpty
              ? null
              : expertModel.specialties.first.id,
        });
        // depend on serviceModel
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10.sp, vertical: 20.sp),
        margin: EdgeInsets.only(bottom: 10.sp),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(
              Radius.circular(10.sp),
            ),
            boxShadow: [
              BoxShadow(
                offset: Offset(1, 1),
                color: Colors.black.withOpacity(.08),
                blurRadius: 4,
              )
            ]),
        child: Column(
          children: [
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CachedCakeImage(
                    fit: BoxFit.cover,
                    urlToImage: expertModel.avatar?.urlToImage,
                    size: 36.sp,
                    radius: BorderRadius.all(
                      Radius.circular(20.sp),
                    ),
                  ),
                  SizedBox(
                    width: 10.sp,
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          expertModel.fullname,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            color: colorBlack1,
                            fontSize: 13.sp,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        SizedBox(
                          height: 4.sp,
                        ),
                        Row(
                          children: [
                            Image.asset(
                              iconLocation,
                              width: 7.24.sp,
                              height: 10.sp,
                            ),
                            SizedBox(
                              width: 7.38.sp,
                            ),
                            Text(
                              '${ProvinceHelper().getProvinceByCode(expertModel.province)}',
                              style: categoryExpertInforStyle,
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                  SizedBox(width: 8.sp),
                  Container(
                    padding: EdgeInsets.only(top: 1.sp),
                    alignment: Alignment.topRight,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          expertModel.callPrice!.costString,
                          style: TextStyle(
                            color: colorBlack1,
                            fontSize: 11.sp,
                            fontWeight: FontWeight.w600,
                            height: 1.4,
                          ),
                        ),
                        SizedBox(
                          height: 4.sp,
                        ),
                        Text(
                          '/${expertModel.callPrice!.totalMinutes} ${Strings.minutes.i18n}',
                          style: TextStyle(
                            color: colorGray1,
                            fontSize: 9.sp,
                            fontWeight: FontWeight.w400,
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: 10.sp),
            Container(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        _infoServiceCard(
                          text:
                              '${expertModel.experienceYears > 40 ? 40 : expertModel.experienceYears} ' +
                                  (expertModel.experienceYears >= 2
                                      ? Strings.yearsOfExperience.i18n
                                      : Strings.yearOfExperience.i18n),
                        ),
                        SizedBox(height: 5.sp),
                        _infoServiceCard(text: 'Chuyên viên tư vấn du học'),
                      ],
                    ),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(bottom: 2.5.sp),
                        child: Image.asset(
                          iconStar,
                          height: 12.5.sp,
                          width: 12.5.sp,
                        ),
                      ),
                      SizedBox(width: 3.sp),
                      Text(
                        '${expertModel.stars.toStringAsFixed(1)}',
                        style: TextStyle(
                          color: colorGray1,
                          fontSize: 11.sp,
                        ),
                      )
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _infoServiceCard({required String text}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Image.asset(
          iconDot,
          width: 4.5.sp,
          height: 4.5.sp,
          color: colorGray1,
        ),
        SizedBox(width: 2.5.sp),
        TextUI(
          text,
          fontSize: 11.sp,
          fontWeight: FontWeight.w400,
          color: colorGray1,
        ),
      ],
    );
  }
}
