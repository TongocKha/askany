import 'package:askany/src/ui/common/widgets/shimmers/fade_simmer.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class CategoryLineShimmer extends StatelessWidget {
  const CategoryLineShimmer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10.sp),
      color: Colors.transparent,
      child: Row(
        children: [
          FadeShimmer(
            width: 150.sp,
            height: 13.sp,
            fadeTheme: FadeTheme.lightReverse,
          ),
          Spacer(),
          Image.asset(
            iconNextHome,
            width: 12.sp,
            height: 12.sp,
          ),
        ],
      ),
    );
  }
}
