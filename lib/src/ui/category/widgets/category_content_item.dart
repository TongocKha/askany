import 'package:askany/src/models/content_model.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/custom_image/network_image/cached_image.dart';
import 'package:askany/src/ui/style/category_style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class CategoryContentItem extends StatelessWidget {
  final ContentModel items;
  final Function onTap;
  const CategoryContentItem({Key? key, required this.items, required this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TouchableOpacity(
      onTap: onTap,
      child: Container(
        margin: EdgeInsets.only(
          right: 10.sp,
          bottom: 7.sp,
        ),
        padding: EdgeInsets.symmetric(
          horizontal: 18.sp,
          vertical: 15.sp,
        ),
        width: 110.sp,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(.08),
              blurRadius: 4.sp,
              offset: Offset(2.sp, 2.sp),
            ),
          ],
          borderRadius: BorderRadius.all(
            Radius.circular(12.sp),
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: CustomNetworkImage(
                urlToImage: items.specialty?.thumbnail?.urlToImage,
                width: 30.sp,
                height: 30.sp,
                shape: BoxShape.rectangle,
              ),
            ),
            SizedBox(height: 8.sp),
            Expanded(
              child: Text(
                items.name,
                style: contentTextStyle,
                textAlign: TextAlign.start,
              ),
            )
          ],
        ),
      ),
    );
  }
}
