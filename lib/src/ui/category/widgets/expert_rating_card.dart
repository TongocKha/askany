import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/ui/style/calendar_style.dart';
import 'package:askany/src/ui/style/request_style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ExpertRatingCard extends StatelessWidget {
  final int reviewCount;
  final double rating;
  ExpertRatingCard({required this.rating, required this.reviewCount});
  @override
  Widget build(BuildContext context) {
    final String reviewText = reviewCount >= 2
        ? '($reviewCount ${Strings.reviews.i18n})'
        : '($reviewCount ${Strings.review.i18n})';

    return rating != 0.0
        ? Container(
            alignment: Alignment.center,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                ListView.builder(
                  padding: EdgeInsets.all(0),
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemCount: 5,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: EdgeInsets.only(right: 3.81.sp),
                      child: Image.asset(
                        index + 1 <= rating.round() ? iconStar : iconStarEmpty,
                        width: 15.sp,
                        height: 12.sp,
                        fit: BoxFit.scaleDown,
                        color: index + 1 <= rating.round() ? null : Colors.grey,
                      ),
                    );
                  },
                ),
                SizedBox(width: 3.sp),
                Padding(
                  padding: EdgeInsets.only(top: 5.sp),
                  child: Text(
                    rating.toStringAsFixed(1),
                    style: TextStyle(
                      color: colorBlack1,
                      fontSize: 13.sp,
                    ),
                  ),
                ),
                SizedBox(
                  width: 3.sp,
                ),
                Padding(
                  padding: EdgeInsets.only(top: 5.sp),
                  child: Text(
                    reviewText,
                    style: TextStyle(color: colorGray2, fontSize: 13.sp),
                  ),
                ),
              ],
            ),
          )
        : Container(
            height: 25.sp,
            alignment: Alignment.center,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(width: 2.sp),
                Text(
                  Strings.noReviews.i18n,
                  style: TextStyle(
                    color: colorBlack2,
                    fontSize: 12.5.sp,
                  ),
                )
              ],
            ),
          );
  }
}
