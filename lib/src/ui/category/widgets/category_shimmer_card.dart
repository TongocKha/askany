import 'package:askany/src/ui/common/widgets/shimmers/fade_simmer.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class CategoryShimmerCard extends StatelessWidget {
  const CategoryShimmerCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10.sp),
      decoration: BoxDecoration(
        color: colorAddButton,
        borderRadius: BorderRadius.all(
          Radius.circular(12.sp),
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            width: 70.sp,
            height: 70.sp,
            child: Stack(
              children: [
                FadeShimmer.round(
                  size: 70.sp,
                  fadeTheme: FadeTheme.lightReverse,
                ),
              ],
            ),
          ),
          Spacer(),
          FadeShimmer(
            width: 85.sp,
            height: 13.sp,
            fadeTheme: FadeTheme.lightReverse,
          ),
        ],
      ),
    );
  }
}
