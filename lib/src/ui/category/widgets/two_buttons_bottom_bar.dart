import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

import 'package:askany/src/models/request_model.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/dialog_style.dart';

class TwoButonsBottomBar extends StatelessWidget {
  final String leftButtonContent;
  final Function leftButtonCallback;
  final String rightButtonContent;
  final Function rightButtonCallback;

  final RequestModel? service;

  const TwoButonsBottomBar({
    Key? key,
    required this.leftButtonContent,
    required this.leftButtonCallback,
    required this.rightButtonContent,
    required this.rightButtonCallback,
    this.service,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 85.sp,
      child: Column(
        children: [
          Divider(
            height: 1,
          ),
          Padding(
              child: Row(
                children: [
                  Expanded(
                    child: TouchableOpacity(
                      onTap: leftButtonCallback,
                      child: Container(
                        child: Center(
                          child: Text(
                            leftButtonContent,
                            style: TextStyle(
                              fontWeight: FontWeight.w700,
                              color: Color(0xff1C4843),
                              fontSize: 13.sp,
                            ),
                          ),
                        ),
                        height: 40.sp,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(8.sp),
                          ),
                          border: Border.all(
                            color: Color(0xff1C4843),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 11.sp,
                    height: 40.sp,
                  ),
                  Expanded(
                    child: TouchableOpacity(
                      onTap: rightButtonCallback,
                      child: Container(
                        child: Center(
                          child: Text(
                            rightButtonContent,
                            style: TextStyle(
                              fontWeight: FontWeight.w700,
                              color: colorWhite,
                              fontSize: 13.sp,
                            ),
                          ),
                        ),
                        height: 40.sp,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(8.sp),
                          ),
                          color: Color(0xff1C4843),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              padding: EdgeInsets.only(
                top: 20.sp,
                // divider = 13.5
                bottom: 24.sp,
                left: 18.sp,
                right: 18.sp,
              ))
        ],
      ),
    );
  }
}
