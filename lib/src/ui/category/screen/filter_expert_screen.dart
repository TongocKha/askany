import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/filter_expert/filter_expert_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/helpers/currency_helper.dart';
import 'package:askany/src/helpers/province_helper.dart';
import 'package:askany/src/models/position_model.dart';
import 'package:askany/src/models/search_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_check_box.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_form.dart';
import 'package:askany/src/ui/filter/widgets/bottom_sheet_position_filter.dart';
import 'package:askany/src/ui/filter/widgets/bottom_sheet_province_filter.dart';
import 'package:askany/src/ui/style/style.dart';

import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class FilterExpertScreen extends StatefulWidget {
  FilterExpertScreen({Key? key}) : super(key: key);

  @override
  State<FilterExpertScreen> createState() => _FilterExpertScreenState();
}

class _FilterExpertScreenState extends State<FilterExpertScreen> {
  int? province;
  String positionId = '';
  String? position;
  String experience = '';
  String company = '';
  String minMoney = MIN_MONEY;
  String maxMoney = MAX_MONEY;
  int? contactType;
  String? specialty;
  String? category;
  final List<PositionModel> positions = AppBloc.skillBloc.positions;
  bool isCheckMeet = false;
  bool isCheckCall = false;
  int indexIdPosition = -1;
  String provinceName = "";
  TextEditingController _experienceYearController = TextEditingController();
  TextEditingController _companyNameController = TextEditingController();
  TextEditingController _minMoneyController = TextEditingController();
  TextEditingController _maxMoneyController = TextEditingController();
  List<bool> isCheckStar = [false, false, false, false];
  List<String> listRating = [];
  bool isCheckNew = false;
  bool isCheckTrend = false;
  bool isCheckAll = false;
  bool isCheckConfirm = false;
  int? sortType;
  @override
  void initState() {
    super.initState();
    isCheckMeet = false;
    isCheckCall = false;
    isCheckNew = false;
    isCheckTrend = false;
    isCheckAll = false;
    isCheckConfirm = false;
    indexIdPosition = -1;
    SearchModel? searchModel = AppBloc.filterExpertBloc.searchExpert;

    if (searchModel != null) {
      if (searchModel.category != null) {
        category = searchModel.category;
      }
      if (searchModel.specialty != null) {
        specialty = searchModel.specialty;
      }

      if (searchModel.province != null) {
        province = searchModel.province;
        provinceName = ProvinceHelper().getProvinceByCode(province!)!;
      }

      if (searchModel.position != null) {
        positionId = searchModel.position!;
        indexIdPosition = positions.indexWhere((item) => item.id == positionId);
        position = positions[indexIdPosition].vi!.name;
      }

      if (searchModel.experienceYears != null) {
        experience = searchModel.experienceYears.toString();
        _experienceYearController.text = experience;
      }

      if (searchModel.companyName != null) {
        company = searchModel.companyName!;
        _companyNameController.text = company;
      }

      if (searchModel.cost != null) {
        minMoney = searchModel.cost!.split('-')[0];
        maxMoney = searchModel.cost!.split('-')[1];

        var value = minMoney;
        value = value.replaceAll(RegExp(r'\D'), '');
        value = value.replaceAll(RegExp(r'\B(?=(\d{3})+(?!\d))'), ',');
        _minMoneyController.text = (minMoney == MIN_MONEY ? '' : value);

        var valueMax = maxMoney;
        valueMax = valueMax.replaceAll(RegExp(r'\D'), '');
        valueMax = valueMax.replaceAll(RegExp(r'\B(?=(\d{3})+(?!\d))'), ',');
        _maxMoneyController.text = (maxMoney == MAX_MONEY ? '' : valueMax);
      }

      if (searchModel.contactType != null) {
        isCheckCall = searchModel.contactType == CONTACT_CALL_TYPE;
        isCheckMeet = !isCheckCall;
      }

      if (searchModel.ratings != null) {
        listRating = searchModel.ratings!;
        searchModel.ratings!.forEach((val) {
          switch (val) {
            case '1-2':
              isCheckStar[0] = true;
              return;
            case '2-3':
              isCheckStar[1] = true;
              return;
            case '3-4':
              isCheckStar[2] = true;
              return;
            case '4-5':
              isCheckStar[3] = true;
              return;
          }
        });
      }
      print(searchModel.sortType);
      if (searchModel.sortType != null) {
        sortType = searchModel.sortType;
        if (sortType == 1) {
          isCheckNew = true;
        } else {
          isCheckTrend = true;
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 16.sp),
              width: 100.w,
              height: 50.sp,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: TouchableOpacity(
                      onTap: () {
                        setState(() {
                          _cleanScreen();
                        });
                      },
                      child: Container(
                        color: Colors.transparent,
                        child: Text(
                          Strings.clearAll.i18n,
                          style: TextStyle(
                            color: colorGray1,
                            fontSize: 12.sp,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      alignment: Alignment.center,
                      child: Text(
                        Strings.filter.i18n,
                        style: TextStyle(
                          color: colorBlack2,
                          fontSize: 15.sp,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: TouchableOpacity(
                      onTap: () {
                        AppNavigator.pop();
                      },
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Image.asset(
                          iconRemove,
                          width: 13.sp,
                          height: 13.sp,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            dividerChat,
            Expanded(
              child: Container(
                padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).viewInsets.bottom,
                ),
                child: SingleChildScrollView(
                  physics: BouncingScrollPhysics(),
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 16.sp),
                        child: Column(
                          children: [
                            SizedBox(height: 17.sp),
                            _textTitle(title: Strings.soft.i18n),
                            SizedBox(height: 10.sp),
                            TextFieldCheckBox(
                              onChanged: () {
                                setState(() {
                                  isCheckNew = !isCheckNew;
                                  isCheckNew
                                      ? isCheckTrend = false
                                      : isCheckTrend = true;
                                  if (isCheckNew == true) {
                                    sortType = 1;
                                  }
                                });
                              },
                              isCheck: isCheckNew,
                              child: Text(
                                Strings.latest.i18n,
                                style: TextStyle(
                                  color: colorBlack2,
                                  fontSize: 13.sp,
                                ),
                              ),
                            ),
                            SizedBox(height: 8.sp),
                            TextFieldCheckBox(
                              isCheck: isCheckTrend,
                              onChanged: () {
                                setState(() {
                                  isCheckTrend = !isCheckTrend;
                                  isCheckTrend
                                      ? isCheckNew = false
                                      : isCheckNew = true;
                                  if (isCheckTrend == true) {
                                    sortType = 2;
                                  }
                                });
                              },
                              child: Text(
                                Strings.mostPopular.i18n,
                                style: TextStyle(
                                  color: colorBlack2,
                                  fontSize: 13.sp,
                                ),
                              ),
                            ),
                            SizedBox(height: 17.sp),
                            _textTitle(title: Strings.address.i18n),
                            TouchableOpacity(
                              onTap: () {
                                showModalBottomSheet(
                                  isScrollControlled: true,
                                  context: context,
                                  backgroundColor: Colors.transparent,
                                  builder: (context) {
                                    return BottomSheetProvinceFilter(
                                      handPressed: (value) {
                                        setState(() {
                                          province = ProvinceHelper()
                                              .getCodeByProvince(value!)!;
                                        });
                                      },
                                    );
                                  },
                                );
                              },
                              child: TextFieldForm(
                                initialValue: province == null
                                    ? 'Nơi ở'
                                    : ProvinceHelper()
                                        .getProvinceByCode(province!),
                                isActive: false,
                                autovalidateMode: AutovalidateMode.always,
                                contentPadding: EdgeInsets.fromLTRB(
                                  10.sp,
                                  20.sp,
                                  10.sp,
                                  0.sp,
                                ),
                                fontSize: 13.sp,
                                suffixIcon: TouchableOpacity(
                                  onTap: () async {},
                                  child: Container(
                                    color: Colors.transparent,
                                    child: Image.asset(
                                      iconArrowDown,
                                      width: 10.sp,
                                    ),
                                  ),
                                ),
                                validatorForm: (val) => null,
                              ),
                            ),
                            SizedBox(height: 17.sp),
                            _textTitle(title: 'Chức vụ'),
                            TouchableOpacity(
                              onTap: () {
                                showModalBottomSheet(
                                  isScrollControlled: true,
                                  context: context,
                                  backgroundColor: Colors.transparent,
                                  builder: (context) {
                                    return BottomSheetPositionFilter(
                                      handPressed: (positionModel) {
                                        setState(() {
                                          positionId = positionModel.id;
                                          position = positionModel.vi!.name;
                                        });
                                      },
                                    );
                                  },
                                );
                              },
                              child: TextFieldForm(
                                autovalidateMode: AutovalidateMode.always,
                                isActive: false,
                                initialValue: position == null
                                    ? Strings.positionInWork.i18n
                                    : position,
                                contentPadding: EdgeInsets.fromLTRB(
                                  10.sp,
                                  20.sp,
                                  10.sp,
                                  0.sp,
                                ),
                                fontSize: 13.sp,
                                suffixIcon: TouchableOpacity(
                                  onTap: () {},
                                  child: Image.asset(
                                    iconArrowDown,
                                    width: 10.sp,
                                  ),
                                ),
                                validatorForm: (val) => null,
                              ),
                            ),
                            SizedBox(height: 17.sp),
                            _textTitle(
                                title: Strings
                                    .yearsCountOfExperienceHinttext.i18n),
                            Container(
                              margin: EdgeInsets.only(top: 10.sp),
                              padding: EdgeInsets.symmetric(
                                horizontal: 9.sp,
                                vertical: 0.sp,
                              ),
                              height: 34.sp,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                border: Border.all(
                                  color: colorBlack2.withOpacity(0.35.sp),
                                  width: 0.5.sp,
                                ),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(
                                    5.sp,
                                  ),
                                ),
                              ),
                              child: TextFormField(
                                controller: _experienceYearController,
                                keyboardType: TextInputType.number,
                                cursorColor: headerCalendarColor,
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 13.sp,
                                ),
                                decoration: InputDecoration(
                                  hintText: Strings
                                      .yearsCountOfExperienceHinttext.i18n,
                                  border: InputBorder.none,
                                  hintStyle: TextStyle(
                                    color: colorGray2,
                                    fontSize: 13.sp,
                                  ),
                                ),
                                onChanged: (val) => experience = val.trim(),
                              ),
                            ),
                            SizedBox(height: 17.sp),
                            _textTitle(title: Strings.company.i18n),
                            Container(
                              margin: EdgeInsets.only(top: 10.sp),
                              padding: EdgeInsets.symmetric(
                                horizontal: 9.sp,
                                vertical: 0.sp,
                              ),
                              height: 34.sp,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                border: Border.all(
                                  color: colorBlack2.withOpacity(0.35.sp),
                                  width: 0.5.sp,
                                ),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(
                                    5.sp,
                                  ),
                                ),
                              ),
                              child: TextFormField(
                                controller: _companyNameController,
                                cursorColor: headerCalendarColor,
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 13.sp,
                                ),
                                decoration: InputDecoration(
                                  hintText: Strings.company.i18n,
                                  border: InputBorder.none,
                                  hintStyle: TextStyle(
                                    color: colorGray2,
                                    fontSize: 13.sp,
                                  ),
                                ),
                                onChanged: (val) => company = val,
                              ),
                            ),
                            SizedBox(height: 17.sp),
                            _textTitle(title: Strings.rating.i18n),
                            ...['1-2', '2-3', '3-4', '4-5']
                                .asMap()
                                .map(
                                  (index, item) {
                                    bool isActive = listRating.isEmpty
                                        ? true
                                        : isCheckStar[index]
                                            ? true
                                            : (isCheckStar[index - 1 < 0
                                                    ? 0
                                                    : index - 1] ||
                                                isCheckStar[
                                                    index > 2 ? 3 : index + 1]);

                                    return MapEntry(
                                      index,
                                      Container(
                                        margin: EdgeInsets.only(top: 8.sp),
                                        child: TextFieldCheckBox(
                                          onChanged: () {
                                            if (isActive) {
                                              setState(() {
                                                isCheckStar[index] =
                                                    !isCheckStar[index];

                                                if (!isCheckStar[index]) {
                                                  switch (index) {
                                                    case 1:
                                                      if (isCheckStar[2] &&
                                                          isCheckStar[3]) {
                                                        isCheckStar[0] = false;
                                                      } else {
                                                        isCheckStar[2] = false;
                                                        isCheckStar[3] = false;
                                                      }
                                                      break;
                                                    case 2:
                                                      if (isCheckStar[2] &&
                                                          isCheckStar[1]) {
                                                        isCheckStar[3] = false;
                                                      } else {
                                                        isCheckStar[0] = false;
                                                        isCheckStar[1] = false;
                                                      }
                                                      break;
                                                    default:
                                                      break;
                                                  }
                                                }
                                              });

                                              listRating = [];
                                              isCheckStar
                                                  .asMap()
                                                  .forEach((ind, isCheck) {
                                                if (isCheck) {
                                                  listRating.add([
                                                    '1-2',
                                                    '2-3',
                                                    '3-4',
                                                    '4-5'
                                                  ][ind]);
                                                }
                                              });
                                            }
                                          },
                                          isCheck: isCheckStar[index],
                                          child: Opacity(
                                            opacity: isActive ? 1 : 0.5,
                                            child: Row(
                                              children: [
                                                Text(
                                                  item,
                                                  style: TextStyle(
                                                    color: colorBlack2,
                                                    fontSize: 13.sp,
                                                  ),
                                                ),
                                                SizedBox(width: 5.sp),
                                                Image.asset(
                                                  iconStar,
                                                  width: 12.5.sp,
                                                  height: 12.5.sp,
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                )
                                .values
                                .toList(),
                            SizedBox(height: 17.sp),
                            _textTitle(title: Strings.price.i18n),
                            SizedBox(height: 10.sp),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Expanded(
                                  child: _textFieldInputMoneyCard(
                                    controller: _minMoneyController,
                                    onChanged: (val) {
                                      setState(
                                        () {
                                          minMoney = val.replaceAll(',', '');
                                        },
                                      );
                                    },
                                  ),
                                ),
                                Container(
                                  alignment: Alignment.center,
                                  width: 18.sp,
                                  child: Text(
                                    ' - ',
                                    style: TextStyle(
                                      color: colorBlack2,
                                      fontSize: 14.sp,
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: _textFieldInputMoneyCard(
                                    controller: _maxMoneyController,
                                    onChanged: (val) {
                                      setState(
                                        () {
                                          maxMoney = val.replaceAll(',', '');
                                        },
                                      );
                                    },
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(height: 17.sp),
                            _textTitle(title: Strings.contactForm.i18n),
                            SizedBox(height: 10.sp),
                            TextFieldCheckBox(
                              onChanged: () {
                                setState(() {
                                  isCheckMeet = !isCheckMeet;
                                  isCheckMeet
                                      ? isCheckCall = false
                                      : isCheckCall = true;
                                });
                              },
                              isCheck: isCheckMeet,
                              child: Text(
                                Strings.meet.i18n,
                                style: TextStyle(
                                  color: colorBlack2,
                                  fontSize: 13.sp,
                                ),
                              ),
                            ),
                            SizedBox(height: 8.sp),
                            TextFieldCheckBox(
                              onChanged: () {
                                setState(() {
                                  isCheckCall = !isCheckCall;
                                  isCheckCall
                                      ? isCheckMeet = false
                                      : isCheckMeet = true;
                                });
                              },
                              isCheck: isCheckCall,
                              child: Text(
                                Strings.call.i18n,
                                style: TextStyle(
                                  color: colorBlack2,
                                  fontSize: 13.sp,
                                ),
                              ),
                            ),
                            SizedBox(height: 17.sp),
                            _textTitle(title: Strings.accoutType.i18n),
                            SizedBox(height: 10.sp),
                            TextFieldCheckBox(
                              isCheck: isCheckAll,
                              onChanged: () {
                                setState(() {
                                  isCheckAll = !isCheckAll;
                                  isCheckAll
                                      ? isCheckConfirm = false
                                      : isCheckConfirm = true;
                                });
                              },
                              child: Text(
                                Strings.all.i18n,
                                style: TextStyle(
                                  color: colorBlack2,
                                  fontSize: 13.sp,
                                ),
                              ),
                            ),
                            SizedBox(height: 8.sp),
                            TextFieldCheckBox(
                              onChanged: () {
                                setState(() {
                                  isCheckConfirm = !isCheckConfirm;
                                  isCheckConfirm
                                      ? isCheckAll = false
                                      : isCheckAll = true;
                                });
                              },
                              isCheck: isCheckConfirm,
                              child: Text(
                                Strings.verifiedAccoutFilter.i18n,
                                style: TextStyle(
                                  color: colorBlack2,
                                  fontSize: 13.sp,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 65.sp),
                      dividerChat,
                      SizedBox(height: 14.sp),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 16.sp),
                        child: ButtonPrimary(
                          onPressed: () {
                            SearchModel searchModel = SearchModel(
                              category: category == null ? null : category,
                              specialty: specialty == null ? null : specialty,
                              position: positionId.isEmpty ? null : positionId,
                              cost: (minMoney + '-' + maxMoney),
                              province: province,
                              experienceYears: experience.isEmpty
                                  ? null
                                  : int.parse(experience),
                              companyName: company.isEmpty ? null : company,
                              contactType: !isCheckMeet && !isCheckCall
                                  ? null
                                  : isCheckMeet
                                      ? CONTACT_MEET_TYPE
                                      : CONTACT_CALL_TYPE,
                              ratings: listRating.isEmpty ? null : listRating,
                              sortType: sortType == null ? null : sortType,
                            );

                            AppBloc.filterExpertBloc.add(
                              GetFilterExpertEvent(
                                searchModel: searchModel,
                              ),
                            );
                            AppNavigator.pop();
                          },
                          text: 'Lọc',
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      SizedBox(height: 30.sp),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _textTitle({required String title}) {
    return Container(
      alignment: Alignment.centerLeft,
      child: Text(
        title,
        style: TextStyle(
          color: colorBlack2,
          fontSize: 13.sp,
          fontWeight: FontWeight.w600,
        ),
      ),
    );
  }

  Widget _textFieldInputMoneyCard({
    required Function(String)? onChanged,
    required TextEditingController controller,
  }) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 9.sp,
        vertical: 0.sp,
      ),
      height: 34.sp,
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(
          color: colorBorderTextField,
          width: 0.5.sp,
        ),
        borderRadius: BorderRadius.all(
          Radius.circular(
            5.sp,
          ),
        ),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            'đ',
            style: TextStyle(
              color: colorGray2,
              fontSize: 12.sp,
            ),
          ),
          SizedBox(width: 5.sp),
          Expanded(
            child: Container(
              margin: EdgeInsets.only(bottom: 6.5.sp),
              child: TextFormField(
                controller: controller,
                keyboardType: TextInputType.number,
                inputFormatters: CurrencyHelper.vndInputFormat,
                cursorColor: headerCalendarColor,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 12.sp,
                ),
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintStyle: TextStyle(
                    color: colorGray2,
                    fontSize: 12.sp,
                  ),
                ),
                onChanged: onChanged,
              ),
            ),
          ),
        ],
      ),
    );
  }

  _cleanScreen() {
    province = null;
    positionId = '';
    position = null;
    experience = '';
    company = '';
    minMoney = MIN_MONEY;
    maxMoney = MAX_MONEY;
    contactType = null;
    isCheckMeet = false;
    isCheckCall = false;
    indexIdPosition = -1;
    provinceName = "";
    _companyNameController.text = '';
    _experienceYearController.text = '';
    _minMoneyController.text = '';
    _maxMoneyController.text = '';
    listRating = [];
    isCheckStar = [false, false, false, false];
    isCheckTrend = false;
    isCheckNew = false;
    isCheckConfirm = false;
    isCheckAll = false;
  }
}
