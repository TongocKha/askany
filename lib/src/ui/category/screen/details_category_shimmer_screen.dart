import 'package:askany/src/ui/category/widgets/category_content_shimmer_card.dart';
import 'package:askany/src/ui/common/widgets/pagination_list_view.dart';
import 'package:askany/src/ui/common/widgets/shimmers/fade_simmer.dart';
import 'package:askany/src/ui/search/widgets/search_shimmer_card.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class DetailsCategoryShimmerScreen extends StatelessWidget {
  const DetailsCategoryShimmerScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.symmetric(
            horizontal: 16.sp,
          ),
          height: 40.sp,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              FadeShimmer(
                width: 100.sp,
                height: 18.sp,
                fadeTheme: FadeTheme.lightReverse,
              ),
              FadeShimmer(
                width: 20.sp,
                height: 20.sp,
                fadeTheme: FadeTheme.lightReverse,
              ),
            ],
          ),
        ),
        Container(
          height: 125.sp,
          child: PaginationListView(
            padding: EdgeInsets.symmetric(
              vertical: 10.sp,
              horizontal: 16.sp,
            ),
            scrollDirection: Axis.horizontal,
            itemCount: 3,
            itemBuilder: (context, index) {
              return CategoryContentShimmerCard();
            },
            childShimmer: CategoryContentShimmerCard(),
          ),
        ),
        Expanded(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 16.sp, vertical: 16.sp),
            decoration: BoxDecoration(
              color: backgroundTimelineDone,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(16.sp), topRight: Radius.circular(16.sp)),
            ),
            child: PaginationListView(
              itemCount: 4,
              itemBuilder: (context, index) {
                return SearchShimmerCard();
              },
              childShimmer: SearchShimmerCard(),
            ),
          ),
        ),
      ],
    );
  }
}
