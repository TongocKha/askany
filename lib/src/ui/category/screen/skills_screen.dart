import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/skill/skill_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/models/expert_model.dart';
import 'package:askany/src/models/skill_model.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/pagination_list_view.dart';
import 'package:askany/src/ui/search/widgets/search_list_shimmer.dart';
import 'package:askany/src/ui/search/widgets/search_shimmer_card.dart';
import 'package:askany/src/ui/search/widgets/skill_search_card.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class SkillsScreen extends StatefulWidget {
  final ExpertModel? expertModel;
  const SkillsScreen({required this.expertModel});

  @override
  State<SkillsScreen> createState() => _SkillsScreenState();
}

class _SkillsScreenState extends State<SkillsScreen> {
  @override
  void initState() {
    super.initState();
    AppBloc.skillBloc.add(CleanSkillEvent());
    AppBloc.skillBloc.add(GetSkillEvent(expertId: widget.expertModel!.id));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarTitleBack(context, Strings.mySkills.i18n),
      body: SafeArea(
        child: Container(
          child: Column(
            children: [
              dividerChat,
              SizedBox(height: 16.sp),
              Expanded(
                child: BlocBuilder<SkillBloc, SkillState>(
                  builder: (context, state) {
                    if (state is GettingSkillFinished ||
                        state is GetDoneSkillFinished) {
                      List<SkillModel> skills =
                          state.props[0] as List<SkillModel>;
                      return Container(
                        color: Colors.transparent,
                        padding: EdgeInsets.symmetric(horizontal: 16.sp),
                        child: PaginationListView(
                          itemCount: skills.length,
                          callBackLoadMore: () => AppBloc.skillBloc.add(
                            GetSkillFinishedEvent(
                                expertId: widget.expertModel!.id),
                          ),
                          callBackRefresh: (Function handleFinished) {
                            AppBloc.skillBloc.add(
                              RefreshSkillEvent(
                                handleFinished: handleFinished,
                              ),
                            );
                          },
                          padding: EdgeInsets.only(bottom: 48.sp),
                          isLoadMore: state is GettingSkillFinished,
                          childShimmer: SearchShimmerCard(),
                          itemBuilder: (context, index) {
                            return SkillSearchCard(
                              skillModel: skills[index],
                              expertModel: widget.expertModel,
                            );
                          },
                        ),
                      );
                    }
                    return SearchListShimmer();
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
