import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/chat/chat_bloc.dart';
import 'package:askany/src/bloc/expert_info/expert_info_bloc.dart';
import 'package:askany/src/bloc/rating/rating_bloc.dart';
import 'package:askany/src/bloc/recommend_expert/recommend_expert_bloc.dart';
import 'package:askany/src/bloc/user/user_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/helpers/province_helper.dart';
import 'package:askany/src/models/expert_model.dart';
import 'package:askany/src/models/rating_management_model.dart';
import 'package:askany/src/models/slide_mode.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/category/screen/details_expert_shimmer_screen.dart';
import 'package:askany/src/ui/category/widgets/category_expert_card.dart';
import 'package:askany/src/ui/category/widgets/two_buttons_bottom_bar.dart';
import 'package:askany/src/ui/category/widgets/expert_rating_card.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/custom_image/network_image/cached_image.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_confirm_cancel.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:askany/src/ui/common/widgets/empty/empty.dart';
import 'package:askany/src/ui/common/widgets/text_ui/title_and_seemore.dart';
import 'package:askany/src/ui/home/hot_expert/hot_expert_shimmer_list.dart';
import 'package:askany/src/ui/review_service/widgets/bottom_sheet_contact_expert.dart';
import 'package:askany/src/ui/review_service/widgets/info_card_expert.dart';
import 'package:askany/src/ui/review_service/widgets/rating_and_text_review.dart';
import 'package:askany/src/ui/review_service/widgets/review_card.dart';
import 'package:askany/src/ui/search/widgets/skill_search_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'dart:math' as math;
import 'package:askany/src/ui/common/widgets/cake_avatar/cached_cake_image.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:timeline_tile/timeline_tile.dart';

extension GlobalKeyEx on GlobalKey {
  Rect? get globalPaintBounds {
    final renderObject = currentContext?.findRenderObject();
    var translation = renderObject?.getTransformTo(null).getTranslation();
    if (translation != null && renderObject?.paintBounds != null) {
      return renderObject!.paintBounds
          .shift(Offset(translation.x, translation.y));
    } else {
      return null;
    }
  }
}

class DetailsExpertScreen extends StatefulWidget {
  final String expertId;
  final String? specialtyId;
  DetailsExpertScreen({
    required this.expertId,
    required this.specialtyId,
  });

  @override
  _DetailsExpertScreenState createState() => _DetailsExpertScreenState();
}

class _DetailsExpertScreenState extends State<DetailsExpertScreen> {
  @override
  void initState() {
    super.initState();
    AppBloc.expertInfoBloc.add(GetInfoExpertEvent(expertId: widget.expertId));
    AppBloc.ratingBloc.add(OnRatingExpertEvent(expertId: widget.expertId));

    AppBloc.recommendExpertBloc.add(
      GetListRecommendFinishEvent(specialty: widget.specialtyId ?? ''),
    );
  }

  List<int> ratings = [0, 0, 0, 0, 0];

  bool isBigHeader = true;

  @override
  void dispose() {
    super.dispose();
    // AppBloc.ratingBloc.add(CleanRatingEvent());
    // AppBloc.recommendBloc.add(CleanRecommendEvent());
    // AppBloc.expertInfoBloc.add(CleanExpertInfoEvent());
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ExpertInfoBloc, ExpertInfoState>(
      buildWhen: (previous, current) => previous != current,
      builder: (context, state) {
        if (state is GetInfoExpert && state.experts[widget.expertId] != null) {
          ExpertModel expertModel =
              state.experts[widget.expertId] as ExpertModel;
          return Scaffold(
            appBar: appBarTitleBack(
              context,
              '',
              colorChild: colorGray1,
              backgroundColor: isBigHeader ? colorGreen5 : Colors.white,
              titleWidget: AnimatedContainer(
                height: !isBigHeader ? 50.sp : 0.0,
                duration:
                    const Duration(milliseconds: DURATION_DEFAULT_ANIMATION),
                curve: Curves.decelerate,
                child: TouchableOpacity(
                  onTap: () {
                    AppNavigator.push(Routes.PHOTO_VIEWER, arguments: {
                      'images': null,
                      'initialIndex': 0,
                      'urlToImages': ['${expertModel.avatar?.urlToImage}'],
                    });
                  },
                  child: Row(
                    children: [
                      CustomNetworkImage(
                        urlToImage: expertModel.avatar?.urlToImage,
                        height: 28.sp,
                      ),
                      SizedBox(width: 10.sp),
                      Text(
                        expertModel.fullname,
                        style: TextStyle(
                          fontSize: 13.sp,
                          fontWeight: FontWeight.w600,
                          color: colorTextChatCard,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            body: Container(
              child: Column(
                children: [
                  Expanded(
                    child: CollapsingList(
                      expertModel: expertModel,
                      ratingSingleCounts:
                          expertModel.ratingSingleCounts ?? ratings,
                      handleLogicHeader: (value) {
                        if (value < 0) {
                          if (!isBigHeader) {
                            setState(() {
                              isBigHeader = true;
                            });
                          }
                        } else {
                          if (isBigHeader) {
                            setState(() {
                              isBigHeader = false;
                            });
                          }
                        }
                      },
                    ),
                  ),
                  Visibility(
                    visible: UserLocal().getAccessToken() != '' &&
                        !UserLocal().getIsExpert(),
                    child: TwoButonsBottomBar(
                      leftButtonContent: Strings.sendMessages.i18n,
                      rightButtonContent: Strings.contactNow.i18n,
                      leftButtonCallback: () {
                        showDialogLoading();
                        AppBloc.chatBloc
                            .add(CheckCanChatEvent(expertId: expertModel.id));
                      },
                      rightButtonCallback: () {
                        showModalBottomSheet(
                          context: context,
                          backgroundColor: Colors.transparent,
                          isScrollControlled: true,
                          builder: (context) {
                            return BottomSheetContactExpert(
                              expertModel: expertModel,
                              // urlToImage: fakeServices2[0].imageExpert,
                            );
                          },
                        );
                      },
                    ),
                  )
                ],
              ),
            ),
          );
        } else {
          return DetailsExpertShimmerScreen();
        }
      },
    );
  }
}

class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  _SliverAppBarDelegate({
    required this.minHeight,
    required this.maxHeight,
    required this.child,
  });

  final double minHeight;
  final double maxHeight;
  final Widget child;

  @override
  double get minExtent => minHeight;

  @override
  double get maxExtent => math.max(maxHeight, minHeight);

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return new SizedBox.expand(child: child);
  }

  @override
  bool shouldRebuild(_SliverAppBarDelegate oldDelegate) {
    return maxHeight != oldDelegate.maxHeight ||
        minHeight != oldDelegate.minHeight ||
        child != oldDelegate.child;
  }
}

class CollapsingList extends StatefulWidget {
  final Function(int) handleLogicHeader;
  final ExpertModel expertModel;
  final List<int> ratingSingleCounts;
  const CollapsingList({
    required this.handleLogicHeader,
    required this.expertModel,
    required this.ratingSingleCounts,
  });
  @override
  _CollapsingListState createState() => _CollapsingListState();
}

class _CollapsingListState extends State<CollapsingList>
    with TickerProviderStateMixin {
  late TabController tabController;
  final ScrollController _scrollController = ScrollController();
  final GlobalKey _infoKey = new GlobalKey(debugLabel: 'info');
  final GlobalKey _experienceKey = new GlobalKey(debugLabel: 'experience');
  final GlobalKey _skillKey = new GlobalKey(debugLabel: 'skill');
  final GlobalKey _reviewKey = new GlobalKey(debugLabel: 'review');
  final GlobalKey _expertsKey = new GlobalKey(debugLabel: 'experts');
  int _currentTab = -1;
  @override
  void initState() {
    super.initState();
    tabController = TabController(length: 4, vsync: this);
    if (mounted) {}
    _scrollController.addListener(
      () {
        if (((_expertsKey.globalPaintBounds?.top ?? 0) - 50.h) <= 0) {
          _setTabIndex(3);
        } else if (((_reviewKey.globalPaintBounds?.top ?? 0) - 50.h) <= 0) {
          _setTabIndex(2);
        } else if (((_skillKey.globalPaintBounds?.top ?? 0) - 50.h) <= 0) {
          _setTabIndex(1);
        } else if (((_infoKey.globalPaintBounds?.top ?? 0) - 20.h) <= 0) {
          _setTabIndex(0);
        } else {
          _setTabIndex(-1);
        }
      },
    );
  }

  _setTabIndex(int index) {
    if (_currentTab != index) {
      setState(() {
        _currentTab = index;
      });

      widget.handleLogicHeader(_currentTab);
    }

    if (_currentTab >= 0) {
      tabController.index = index;
    }
  }

  SliverPersistentHeader makeHeader() {
    return SliverPersistentHeader(
      pinned: true,
      delegate: _SliverAppBarDelegate(
        minHeight: 48.sp,
        maxHeight: 48.sp,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              color: Colors.white,
              alignment: 0.isTablet ? Alignment.center : Alignment.centerLeft,
              child: TabBar(
                onTap: (index) {
                  switch (index) {
                    case 0:
                      Scrollable.ensureVisible(_infoKey.currentContext!);
                      break;
                    case 1:
                      Scrollable.ensureVisible(_skillKey.currentContext!);
                      break;
                    case 2:
                      Scrollable.ensureVisible(_reviewKey.currentContext!);
                      break;
                    case 3:
                      Scrollable.ensureVisible(_expertsKey.currentContext!);
                      break;
                    default:
                      break;
                  }
                  _setTabIndex(index);
                },
                physics: BouncingScrollPhysics(),
                indicatorColor: colorGreen2,
                indicatorWeight: 3.sp,
                isScrollable: true,
                labelColor: colorGreen2,
                unselectedLabelColor: colorBlack2,
                controller: tabController,
                indicatorSize: TabBarIndicatorSize.label,
                indicatorPadding: EdgeInsets.symmetric(horizontal: 16.sp),
                labelStyle: TextStyle(
                    fontSize: 12.sp,
                    color: colorGreen2,
                    fontWeight: FontWeight.w400),
                unselectedLabelStyle: TextStyle(
                    fontSize: 12.sp,
                    color: colorBlack2,
                    fontWeight: FontWeight.w400),
                tabs: [
                  Tab(
                    text: Strings.infomationTabbartext.i18n,
                  ),
                  Tab(
                    text: Strings.mySkills.i18n,
                  ),
                  Tab(
                    text: Strings.reviewsAppbartext.i18n,
                  ),
                  Tab(
                    text: Strings.experts.i18n,
                  ),
                ],
              ),
            ),
            Divider(
              height: 1.sp,
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      controller: _scrollController,
      physics: ClampingScrollPhysics(),
      slivers: <Widget>[
        SliverFixedExtentList(
          itemExtent: 74.sp,
          delegate: SliverChildListDelegate(
            [
              Stack(
                children: [
                  Container(
                    height: 74.sp,
                    child: Column(
                      children: [
                        Container(
                          height: 43.sp,
                          color: colorGreen5,
                        ),
                        Container(
                          color: colorWhite,
                          height: 31.sp,
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 74.sp,
                    child: TouchableOpacity(
                      onTap: () {
                        AppNavigator.push(Routes.PHOTO_VIEWER, arguments: {
                          'images': null,
                          'initialIndex': 0,
                          'urlToImages': [
                            '${widget.expertModel.avatar?.urlToImage}'
                          ],
                        });
                      },
                      child: Center(
                        child: CachedCakeImage(
                          fit: BoxFit.cover,
                          urlToImage: widget.expertModel.avatar?.urlToImage,
                          size: 74.sp,
                          radius: BorderRadius.all(
                            Radius.circular(37.sp),
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
        SliverFixedExtentList(
          delegate: SliverChildListDelegate([
            Container(
              color: colorWhite,
              height: !UserLocal().getIsExpert() ? 150.sp : 118.sp,
              child: Column(
                children: [
                  SizedBox(
                    height: 10.sp,
                  ),
                  Container(
                    height: 27.sp,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          widget.expertModel.fullname,
                          style: categoryTitleStyle,
                        ),
                        SizedBox(
                          width: 10.sp,
                        ),
                        Image.asset(
                          iconVerified,
                          width: 14.sp,
                          height: 14.sp,
                          color: colorFontGreen,
                          fit: BoxFit.cover,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10.sp,
                  ),
                  Container(
                    height: 26.sp,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          widget.expertModel.callPrice!.costString15Minutes,
                          style: categoryExpertPriceStyle,
                        ),
                        Text(
                          '/${widget.expertModel.callPrice!.totalMinutes} ${Strings.minutes.i18n} (${Strings.minimum.i18n} ${widget.expertModel.callPrice!.totalMinutes} ${Strings.minutes.i18n})',
                          style: categoryExpertInforStyle2,
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 5.sp,
                  ),
                  Container(
                    height: 26.sp,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ExpertRatingCard(
                          reviewCount: widget.expertModel.ratingCount,
                          rating: widget.expertModel.stars,
                        ),
                      ],
                    ),
                  ),
                  Visibility(
                    visible: UserLocal().getAccessToken() != '' &&
                        !UserLocal().getIsExpert(),
                    child: Column(
                      children: [
                        SizedBox(
                          height: 8.sp,
                        ),
                        Container(
                          height: 19.sp,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.asset(
                                iconBlock,
                                width: 10.sp,
                                height: 10.sp,
                                fit: BoxFit.fill,
                              ),
                              SizedBox(
                                width: 6.sp,
                              ),
                              TouchableOpacity(
                                onTap: () {
                                  dialogAnimationWrapper(
                                    slideFrom: SlideMode.bot,
                                    child: DialogConfirmCancel(
                                      beforeTextAlign: TextAlign.left,
                                      title:
                                          '${Strings.block.i18n} ${widget.expertModel.fullname}?',
                                      bodyBefore:
                                          Strings.afterblockedSuggestion.i18n,
                                      confirmText:
                                          Strings.block.i18n.toUpperCase(),
                                      cancelText:
                                          Strings.cancel.i18n.toUpperCase(),
                                      onConfirmed: () {
                                        AppNavigator.pop();
                                        AppBloc.userBloc.add(
                                          BlockExpertEvent(
                                            expertId: widget.expertModel.id,
                                          ),
                                        );
                                        showDialogLoading();
                                      },
                                    ),
                                  );
                                },
                                child: Text(
                                  Strings.blockUser.i18n,
                                  style: categoryTextStyle3,
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 15.sp,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            )
          ]),
          itemExtent:
              UserLocal().getAccessToken() != '' && !UserLocal().getIsExpert()
                  ? 150.sp
                  : 118.sp,
        ),
        makeHeader(),
        SliverList(
          delegate:
              SliverChildBuilderDelegate((BuildContext context, int index) {
            return new Container(
              child: Column(
                children: [
                  Card(
                    key: _infoKey,
                    elevation: 0,
                    child: Container(
                      padding: EdgeInsets.symmetric(
                        horizontal: 16.sp,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 5.sp),
                          InfoExpertCard(
                            icon: iconLocationRounded,
                            title: '${Strings.addressTitle.i18n}:',
                            description:
                                '${ProvinceHelper().getProvinceByCode(widget.expertModel.province)}',
                          ),
                          SizedBox(height: 15.sp),
                          InfoExpertCard(
                            icon: iconDictionary,
                            title: '${Strings.languages.i18n}:',
                            description: widget.expertModel.languagesTile,
                          ),
                          SizedBox(height: 15.sp),
                          InfoExpertCard(
                            icon: iconExperience,
                            title:
                                '${Strings.yearsCountOfExperienceHinttext.i18n}:',
                            description:
                                '${widget.expertModel.experienceYears}' +
                                    (widget.expertModel.experienceYears >= 2
                                        ? Strings.yearsOfExperience.i18n
                                        : Strings.yearsOfExperience.i18n),
                          ),
                          SizedBox(height: 15.sp),
                          InfoExpertCard(
                            icon: iconPosition,
                            title: '${Strings.currentPosition.i18n}:',
                            description:
                                widget.expertModel.highgestPositionTile,
                          ),
                          SizedBox(height: 15.sp),
                          InfoExpertCard(
                            icon: iconCompany,
                            title: '${Strings.currentCompany.i18n}:',
                            description: '${widget.expertModel.companyName}',
                          ),
                          SizedBox(height: 15.sp),
                          dividerChat,
                          SizedBox(height: 15.sp),
                          Text(
                            '${widget.expertModel.introduce}',
                            textAlign: TextAlign.justify,
                            style: TextStyle(
                              color: colorBlack2,
                              fontSize: 12.sp,
                              height: 1.28.sp,
                            ),
                          ),
                          SizedBox(height: 18.sp),
                          Container(
                            key: _experienceKey,
                            alignment: Alignment.centerLeft,
                            height: 25.sp,
                            child: Text(
                              Strings.experienceInWork.i18n,
                              style: categoryExpertContentStyle,
                            ),
                          ),
                          SizedBox(height: 12.sp),
                          ...List.generate(
                              widget.expertModel.workingExperience!.length,
                              (index) => index).map(
                            (e) {
                              String starWorld = DateFormat('MM/yyyy')
                                  .format(
                                    widget.expertModel.workingExperience?[e]
                                            .timeStartWork ??
                                        DateTime.now(),
                                  )
                                  .toString();

                              String endWorld = DateFormat('MM/yyyy')
                                  .format(
                                    widget.expertModel.workingExperience?[e]
                                            .timeEndWork ??
                                        DateTime.now(),
                                  )
                                  .toString();

                              return Padding(
                                padding:
                                    EdgeInsets.only(bottom: 5.sp, top: 5.sp),
                                child: TimelineTile(
                                  beforeLineStyle: LineStyle(
                                    color: colorWhite,
                                  ),
                                  indicatorStyle: IndicatorStyle(
                                    width: 10.sp,
                                    height: 16.sp,
                                    color: colorGreenTimeline,
                                    indicatorXY: 0.0,
                                    indicator: Container(
                                      height: 16.sp,
                                      color: colorWhite,
                                      child: Center(
                                        child: Image.asset(
                                          iconDOtGreen,
                                          width: 10.sp,
                                          height: 10.sp,
                                        ),
                                      ),
                                    ),
                                  ),
                                  afterLineStyle: LineStyle(
                                    thickness: 0.5.sp,
                                    color: colorGreenTimeline,
                                  ),
                                  alignment: TimelineAlign.manual,
                                  lineXY: 0.0.sp,
                                  endChild: Container(
                                      width: 80.w,
                                      padding: EdgeInsets.only(
                                          left: 12.sp, right: 24.sp),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            widget
                                                .expertModel
                                                .workingExperience![e]
                                                .description,
                                            style: expertExperieceTitleStyle,
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                          SizedBox(
                                            height: 6.sp,
                                          ),
                                          Text(
                                            '${Strings.company.i18n} ${widget.expertModel.workingExperience![e].companyName}',
                                            style: expertExperieceTextStyle,
                                            maxLines: 2,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                          SizedBox(
                                            height: 6.sp,
                                          ),
                                          Text(
                                            '$starWorld - $endWorld',
                                            style: expertExperieceSubTextStyle,
                                          ),
                                          SizedBox(
                                            height: 10.sp,
                                          ),
                                          ...List.generate(
                                              (widget
                                                  .expertModel
                                                  .workingExperience![e]
                                                  .specialties!
                                                  .length),
                                              (index) => index).map(
                                            (item) => Container(
                                              height: 26.sp,
                                              child: Row(
                                                children: [
                                                  SizedBox(
                                                    width: 6.sp,
                                                  ),
                                                  Image.asset(
                                                    iconDot,
                                                    width: 6.sp,
                                                    height: 6.sp,
                                                  ),
                                                  SizedBox(
                                                    width: 6.sp,
                                                  ),
                                                  Text(
                                                    '${widget.expertModel.workingExperience![e].specialties?[item].getLocaleTitle}',
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      )),
                                  startChild: Container(),
                                ),
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 20.sp),
                  dividerThinkness6NotMargin,
                  SizedBox(height: 26.sp),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 16.sp),
                    child: Column(
                      children: [
                        Card(
                          elevation: 0.0,
                          key: _skillKey,
                          child: TitleAndSeeMore(
                            title: Strings.mySkills.i18n,
                            checkSeeMore: true,
                            handlePressed: () {
                              AppNavigator.push(Routes.SKILLS, arguments: {
                                'expertModel': widget.expertModel
                              });
                            },
                          ),
                        ),
                        SizedBox(height: 12.sp),
                        ListView.builder(
                          padding: EdgeInsets.zero,
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: widget.expertModel.skills!.length >= 3
                              ? 3
                              : widget.expertModel.skills!.length,
                          itemBuilder: (context, index) {
                            return SkillSearchCard(
                              skillModel: widget.expertModel.skills![index],
                              expertModel: widget.expertModel,
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 8.sp),
                  dividerThinkness6NotMargin,
                  SizedBox(height: 26.sp),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 16.sp),
                    child: Column(
                      children: [
                        Card(
                          elevation: 0.0,
                          key: _reviewKey,
                          child: TitleAndSeeMore(
                            title: Strings.reviewsAppbartext.i18n,
                            checkSeeMore: widget.expertModel.ratingCount > 0,
                            handlePressed: () {
                              if (widget.expertModel.ratingCount > 0) {
                                AppNavigator.push(
                                  Routes.REVIEWSEXPERT,
                                  arguments: {
                                    'expertModel': widget.expertModel,
                                    'ratingSingleCounts':
                                        widget.ratingSingleCounts,
                                  },
                                );
                              }
                            },
                          ),
                        ),
                        RatingAndTextReview(
                          ratingCount: widget.expertModel.ratingCount,
                          stars: widget.expertModel.stars,
                        ),
                        SizedBox(height: 18.sp),
                        BlocBuilder<RatingBloc, RatingState>(
                          buildWhen: (previous, current) => previous != current,
                          builder: (context, state) {
                            if (state is GetDoneRatingExpert &&
                                state.ratings[widget.expertModel.id] != null) {
                              RatingStoreModel listRating =
                                  state.ratings[widget.expertModel.id]
                                      as RatingStoreModel;

                              return listRating.joinRatings.ratings.isNotEmpty
                                  ? Container(
                                      color: Colors.transparent,
                                      child: ListView.builder(
                                        padding: EdgeInsets.zero,
                                        physics: NeverScrollableScrollPhysics(),
                                        itemCount: listRating.joinRatings
                                                    .ratings.length >=
                                                3
                                            ? 3
                                            : listRating
                                                .joinRatings.ratings.length,
                                        shrinkWrap: true,
                                        itemBuilder: (context, index) {
                                          return ReviewCard(
                                            ratingModel: listRating
                                                .joinRatings.ratings[index],
                                            isCheckLastReview: index ==
                                                (listRating.joinRatings.ratings
                                                            .length >=
                                                        3
                                                    ? 2
                                                    : listRating.joinRatings
                                                            .ratings.length -
                                                        1),
                                          );
                                        },
                                      ),
                                    )
                                  : Container();
                            }
                            return Container();
                          },
                        )
                      ],
                    ),
                  ),
                  dividerThinkness6NotMargin,
                  SizedBox(height: 26.sp),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 16.sp),
                    child: Column(
                      children: [
                        Card(
                          elevation: 0.0,
                          key: _expertsKey,
                          child: TitleAndSeeMore(
                            title: Strings.otherExperts.i18n,
                            checkSeeMore: false,
                          ),
                        ),
                        SizedBox(height: 12.sp),
                        BlocBuilder<RecommendExpertBloc, RecommendExpertState>(
                          builder: (context, state) {
                            if (state is GettingExpertRecommend ||
                                state is GetDoneExpertRecommend) {
                              List<ExpertModel> listExpert =
                                  (state.props[0] as List).cast();
                              int indexOfExpert = listExpert.indexWhere(
                                (expert) => expert.id == widget.expertModel.id,
                              );
                              if (indexOfExpert != -1) {
                                listExpert.removeAt(indexOfExpert);
                              }

                              return listExpert.isNotEmpty
                                  ? Container(
                                      color: Colors.transparent,
                                      child: ListView.builder(
                                        padding: EdgeInsets.zero,
                                        physics: NeverScrollableScrollPhysics(),
                                        itemCount: listExpert.length >= 3
                                            ? 3
                                            : listExpert.length,
                                        shrinkWrap: true,
                                        itemBuilder: (context, index) {
                                          return ExpertCard(
                                            expertModel: listExpert[index],
                                          );
                                        },
                                      ),
                                    )
                                  : Container(
                                      height: 280.sp,
                                      padding: EdgeInsets.only(top: 40.sp),
                                      child: Empty(
                                        image: Image.asset(
                                          imageSkillEmpty,
                                          width: 120.sp,
                                          height: 80.sp,
                                        ),
                                        text: Strings
                                            .noneRecommendationsSuggestion.i18n,
                                        backgroundColor: Colors.transparent,
                                      ),
                                    );
                            }
                            return HotExpertShimmerList();
                          },
                        ),
                        SizedBox(height: 18.sp),
                      ],
                    ),
                  ),
                ],
              ),
            );
          }, childCount: 1),
        ),
      ],
    );
  }
}
