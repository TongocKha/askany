import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/content/content_bloc.dart';
import 'package:askany/src/models/category_model.dart';
import 'package:askany/src/models/search_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/category/widgets/category_item_logo.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class SubCategoryScreen extends StatelessWidget {
  final CategoryInfoModel categoryInfo;
  SubCategoryScreen({Key? key, required this.categoryInfo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarTitleBack(context, ""),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 16.sp),
        child: Column(
          children: [
            CategoryItemLogo(
              urlToIcon: categoryInfo.categoryInfo.icon!.urlToImage,
            ),
            Container(
              height: 40.sp,
              child: Center(
                child: Text(
                  categoryInfo.categoryInfo.name,
                  style: categoryTitleStyle,
                ),
              ),
            ),
            SizedBox(
              height: 10.sp,
            ),
            Expanded(
              child: ListView.builder(
                shrinkWrap: true,
                itemCount: categoryInfo.listSubCates.length,
                itemBuilder: (context, index) {
                  return Column(
                    children: [
                      index > 0 ? dividerChat : SizedBox(),
                      TouchableOpacity(
                        onTap: () async {
                          AppBloc.contentBloc.add(
                            GetExpertCategoryEvent(
                              slug: categoryInfo.listSubCates[index].slug,
                            ),
                          );
                          AppNavigator.push(
                            Routes.DETAILS_CATEGORY,
                            arguments: {
                              'categoryModel': categoryInfo.listSubCates[index],
                            },
                          );
                          AppBloc.filterExpertBloc.searchExpert =
                              SearchModel(category: categoryInfo.listSubCates[index].slug);
                        },
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical: 10.sp),
                          color: Colors.transparent,
                          child: Row(
                            children: [
                              Text(
                                categoryInfo.listSubCates[index].name,
                                style: categorySloganStyle,
                              ),
                              Spacer(),
                              Image.asset(
                                iconNextHome,
                                width: 12.sp,
                                height: 12.sp,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
