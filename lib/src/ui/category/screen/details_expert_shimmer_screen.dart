import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/shimmers/fade_simmer.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class DetailsExpertShimmerScreen extends StatelessWidget {
  const DetailsExpertShimmerScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarTitleBack(
        context,
        '',
        backgroundColor: colorGreen5,
        colorChild: colorGray1,
      ),
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                child: Stack(
                  children: [
                    Container(
                      height: 74.sp,
                      child: Column(
                        children: [
                          Container(
                            height: 43.sp,
                            color: colorGreen5,
                          ),
                          Container(
                            color: colorWhite,
                            height: 31.sp,
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 74.sp,
                      child: Center(
                        child: FadeShimmer.round(
                          size: 74.sp,
                          fadeTheme: FadeTheme.lightReverse,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                color: colorWhite,
                height: 150.sp,
                child: Column(
                  children: [
                    SizedBox(
                      height: 5.sp,
                    ),
                    Container(
                      height: 27.sp,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          FadeShimmer(
                            width: 85.sp,
                            height: 17.sp,
                            fadeTheme: FadeTheme.lightReverse,
                          ),
                          SizedBox(
                            width: 5.sp,
                          ),
                          FadeShimmer.round(
                            size: 14.sp,
                            fadeTheme: FadeTheme.lightReverse,
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 5.sp,
                    ),
                    Container(
                      height: 26.sp,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          FadeShimmer(
                            width: 45.sp,
                            height: 15.sp,
                            fadeTheme: FadeTheme.lightReverse,
                          ),
                          SizedBox(width: 5.sp),
                          FadeShimmer(
                            width: 85.sp,
                            height: 12.sp,
                            fadeTheme: FadeTheme.lightReverse,
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 5.sp,
                    ),
                    Container(
                      height: 26.sp,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          FadeShimmer(
                            width: 150.sp,
                            height: 17.sp,
                            fadeTheme: FadeTheme.lightReverse,
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 5.sp,
                    ),
                    Container(
                      height: 19.sp,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          FadeShimmer.round(
                            size: 10.sp,
                            fadeTheme: FadeTheme.lightReverse,
                          ),
                          SizedBox(
                            width: 6.sp,
                          ),
                          FadeShimmer(
                            width: 70.sp,
                            height: 12.sp,
                            fadeTheme: FadeTheme.lightReverse,
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 7.sp,
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 16.sp),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    FadeShimmer(
                      width: 50.sp,
                      height: 12.sp,
                      fadeTheme: FadeTheme.lightReverse,
                    ),
                    FadeShimmer(
                      width: 50.sp,
                      height: 12.sp,
                      fadeTheme: FadeTheme.lightReverse,
                    ),
                    FadeShimmer(
                      width: 50.sp,
                      height: 12.sp,
                      fadeTheme: FadeTheme.lightReverse,
                    ),
                    FadeShimmer(
                      width: 50.sp,
                      height: 12.sp,
                      fadeTheme: FadeTheme.lightReverse,
                    ),
                  ],
                ),
              ),
              dividerChat,
              SizedBox(height: 18.sp),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 16.sp),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    _shimmerInfoCard(),
                    SizedBox(height: 15.sp),
                    _shimmerInfoCard(),
                    SizedBox(height: 15.sp),
                    _shimmerInfoCard(),
                    SizedBox(height: 15.sp),
                    _shimmerInfoCard(),
                    SizedBox(height: 15.sp),
                    _shimmerInfoCard(),
                    SizedBox(height: 15.sp),
                    dividerChat,
                    SizedBox(height: 15.sp),
                    FadeShimmer(
                      width: 100.w,
                      height: 13.sp,
                      fadeTheme: FadeTheme.lightReverse,
                    ),
                    SizedBox(height: 5.sp),
                    FadeShimmer(
                      width: 100.w,
                      height: 13.sp,
                      fadeTheme: FadeTheme.lightReverse,
                    ),
                    SizedBox(height: 5.sp),
                    FadeShimmer(
                      width: 100.w,
                      height: 13.sp,
                      fadeTheme: FadeTheme.lightReverse,
                    ),
                    SizedBox(height: 5.sp),
                    FadeShimmer(
                      width: 100.w,
                      height: 13.sp,
                      fadeTheme: FadeTheme.lightReverse,
                    ),
                    SizedBox(height: 18.sp),
                    FadeShimmer(
                      width: 40.w,
                      height: 25.sp,
                      fadeTheme: FadeTheme.lightReverse,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _shimmerInfoCard() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        FadeShimmer(
          width: 24.sp,
          height: 24.sp,
          fadeTheme: FadeTheme.lightReverse,
        ),
        SizedBox(width: 13.sp),
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              FadeShimmer(
                width: 60.sp,
                height: 13.sp,
                fadeTheme: FadeTheme.lightReverse,
              ),
              SizedBox(height: 5.sp),
              FadeShimmer(
                width: 90.sp,
                height: 12.sp,
                fadeTheme: FadeTheme.lightReverse,
              ),
            ],
          ),
        )
      ],
    );
  }
}
