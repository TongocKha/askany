import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/content/content_bloc.dart';
import 'package:askany/src/bloc/filter_expert/filter_expert_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/models/category_model.dart';
import 'package:askany/src/models/content_model.dart';
import 'package:askany/src/models/expert_model.dart';
import 'package:askany/src/models/search_model.dart';
import 'package:askany/src/ui/category/screen/details_category_shimmer_screen.dart';
import 'package:askany/src/ui/category/widgets/category_content_item.dart';
import 'package:askany/src/ui/category/widgets/category_content_shimmer_card.dart';
import 'package:askany/src/ui/category/widgets/category_expert_card.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/empty/empty.dart';
import 'package:askany/src/ui/common/widgets/pagination_list_view.dart';
import 'package:askany/src/ui/search/widgets/search_list_shimmer.dart';
import 'package:askany/src/ui/search/widgets/search_shimmer_card.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class DetailsCategoryScreen extends StatefulWidget {
  final CategoryModel categoryModel;
  const DetailsCategoryScreen({
    required this.categoryModel,
  });

  @override
  State<DetailsCategoryScreen> createState() => _DetailsCategoryScreenState();
}

class _DetailsCategoryScreenState extends State<DetailsCategoryScreen> {
  bool isClickContent = false;
  SearchModel? searchExpert;
  SearchModel? search;
  @override
  void initState() {
    super.initState();
    isClickContent = false;
  }

  @override
  void dispose() {
    super.dispose();
    AppBloc.contentBloc.add(CleanContentEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarTitleBack(context, ""),
      body: BlocBuilder<ContentBloc, ContentState>(
        builder: (context, state) {
          List<ContentModel> _content = (state.props[0] as List).cast();
          List<ExpertModel> _experts = (state.props[1] as List).cast();
          if (state is GettingExpertCategory ||
              state is GetDoneExpertCategory) {
            return Column(
              children: [
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: 16.sp,
                  ),
                  height: 40.sp,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        widget.categoryModel.name,
                        style: categoryTitleStyle,
                      ),
                      TouchableOpacity(
                        onTap: () {
                          AppBloc.filterExpertBloc.add(OnFilterExpertEvent(
                            slug: widget.categoryModel.slug,
                          ));
                          setState(() {
                            if (!isClickContent) {
                              isClickContent = true;
                            }
                          });
                        },
                        child: Container(
                          color: Colors.transparent,
                          height: 20.sp,
                          width: 20.sp,
                          child: Image.asset(iconFilter),
                        ),
                      )
                    ],
                  ),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: _content.isNotEmpty ? 0 : 10.sp),
                      Visibility(
                        visible: _content.isNotEmpty,
                        child: Container(
                          height: 125.sp,
                          child: PaginationListView(
                            padding: EdgeInsets.symmetric(
                              vertical: 10.sp,
                              horizontal: 16.sp,
                            ),
                            scrollDirection: Axis.horizontal,
                            itemCount: _content.length,
                            itemBuilder: (context, index) {
                              return CategoryContentItem(
                                items: _content[index],
                                onTap: () {
                                  setState(() {
                                    if (!isClickContent) {
                                      isClickContent = true;
                                    }
                                    search =
                                        AppBloc.filterExpertBloc.searchExpert;

                                    print(search?.sortType ?? 0);

                                    searchExpert = SearchModel(
                                      category: widget.categoryModel.slug,
                                      specialty: _content[index].slug,
                                      position: search?.position ?? null,
                                      province: search?.province ?? null,
                                      experienceYears:
                                          search?.experienceYears ?? null,
                                      companyName: search?.companyName ?? null,
                                      contactType: search?.contactType ?? null,
                                      cost: search?.cost ?? null,
                                      ratings: search?.ratings ?? null,
                                      sortType: search?.sortType ?? null,
                                    );
                                  });
                                  AppBloc.filterExpertBloc
                                      .add(GetFilterExpertEvent(
                                    searchModel: searchExpert!,
                                  ));
                                },
                              );
                            },
                            childShimmer: CategoryContentShimmerCard(),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                            color: backgroundTimelineDone,
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(16.sp),
                                topRight: Radius.circular(16.sp)),
                          ),
                          child: !isClickContent
                              ? _experts.isEmpty
                                  ? Empty(
                                      image: Image.asset(
                                        imageCategoryEmpty,
                                        width: 120.sp,
                                        height: 80.sp,
                                      ),
                                      text: Strings
                                          .unfortunatelyNoExpertInThisSectorSuggestion
                                          .i18n,
                                      backgroundColor: Colors.transparent,
                                    )
                                  : PaginationListView(
                                      padding: EdgeInsets.symmetric(
                                        vertical: 16.sp,
                                        horizontal: 16.sp,
                                      ),
                                      itemCount: _experts.length,
                                      itemBuilder: (context, index) {
                                        return ExpertCard(
                                          expertModel: _experts[index],
                                        );
                                      },
                                      callBackLoadMore: () =>
                                          AppBloc.contentBloc.add(
                                        GetExpertCategoryEvent(
                                          slug: widget.categoryModel.slug,
                                        ),
                                      ),
                                      callBackRefresh: null,
                                      isLoadMore:
                                          state is GettingExpertCategory,
                                      childShimmer: SearchShimmerCard(),
                                    )
                              : BlocBuilder<FilterExpertBloc,
                                  FilterExpertState>(
                                  builder: (context, state) {
                                    if (state is GettingFilterExpert ||
                                        state is GetDoneFilterExpert) {
                                      List<ExpertModel> _expertFilter =
                                          state.props[0] as List<ExpertModel>;
                                      searchExpert =
                                          AppBloc.filterExpertBloc.searchExpert;
                                      return _expertFilter.isEmpty
                                          ? Empty(
                                              image: Image.asset(
                                                imageSkillEmpty,
                                                width: 120.sp,
                                                height: 80.sp,
                                              ),
                                              text: Strings
                                                  .noExpertInThisSectorSuggestion
                                                  .i18n,
                                              backgroundColor:
                                                  Colors.transparent,
                                            )
                                          : PaginationListView(
                                              padding: EdgeInsets.symmetric(
                                                vertical: 16.sp,
                                                horizontal: 16.sp,
                                              ),
                                              itemCount: _expertFilter.length,
                                              itemBuilder: (context, index) {
                                                return ExpertCard(
                                                  expertModel:
                                                      _expertFilter[index],
                                                );
                                              },
                                              callBackLoadMore: () =>
                                                  AppBloc.filterExpertBloc.add(
                                                GetMoreFilterExpertEvent(),
                                              ),
                                              callBackRefresh: null,
                                              isLoadMore:
                                                  state is GettingFilterExpert,
                                              childShimmer: SearchShimmerCard(),
                                            );
                                    }
                                    return SearchListShimmer();
                                  },
                                ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            );
          } else {
            return Scaffold(
              body: DetailsCategoryShimmerScreen(),
            );
          }
        },
      ),
    );
  }
}
