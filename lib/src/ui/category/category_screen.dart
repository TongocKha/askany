import 'package:askany/src/bloc/category/category_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/models/category_model.dart';
import 'package:askany/src/ui/category/widgets/category_shimmer_card.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_transparent.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_message.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:askany/src/ui/category/widgets/category_item.dart';

class CategoryScreen extends StatefulWidget {
  CategoryScreen({Key? key}) : super(key: key);

  @override
  _CategoryScreenState createState() => _CategoryScreenState();
}

class _CategoryScreenState extends State<CategoryScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarCategory(title: Strings.categoryAppbartext.i18n, actions: [
        ButtonMessage(
          iconHeader: iconMessageHeaderBlackLine,
          padding: EdgeInsets.only(right: 16.sp),
        ),
      ]),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 16.sp),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 45.sp,
              child: Text(
                Strings.findAnExpertInEverySectorsWhoCanResolveYourProblems.i18n,
                style: categorySloganStyle,
              ),
            ),
            Expanded(
              child: BlocBuilder<CategoryBloc, CategoryState>(
                builder: (context, state) {
                  if (state is GettingCategoryInfo || state is GetDoneCategoryInfo) {
                    List<CategoryInfoModel> categories =
                        (state.props[0] as List<CategoryInfoModel>).toList();
                    return Container(
                      child: GridView.builder(
                        padding: EdgeInsets.only(bottom: 16.sp),
                        physics: BouncingScrollPhysics(),
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 0.itemCountGridViewCalendar,
                          crossAxisSpacing: 10.sp,
                          mainAxisSpacing: 10.sp,
                          childAspectRatio: 1.2,
                        ),
                        itemCount: categories.length,
                        itemBuilder: (context, index) {
                          return CategoryItem(
                            categories: categories[index],
                          );
                        },
                      ),
                    );
                  }
                  return Container(
                    child: GridView.builder(
                      padding: EdgeInsets.only(bottom: 16.sp),
                      physics: BouncingScrollPhysics(),
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        crossAxisSpacing: 10.sp,
                        mainAxisSpacing: 10.sp,
                        childAspectRatio: 1.2,
                      ),
                      itemCount: 6,
                      itemBuilder: (context, index) {
                        return CategoryShimmerCard();
                      },
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
