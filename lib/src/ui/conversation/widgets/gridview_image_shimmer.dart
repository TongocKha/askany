import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:askany/src/ui/common/widgets/shimmers/fade_simmer.dart';
import 'package:flutter/material.dart';

class GridViewImageShimmer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      addRepaintBoundaries: false,
      physics: BouncingScrollPhysics(),
      addAutomaticKeepAlives: true,
      shrinkWrap: true,
      gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 0.itemCountGridViewMoney,
        crossAxisSpacing: 2.sp,
        mainAxisSpacing: 2.sp,
      ),
      itemCount: 12,
      itemBuilder: (context, index) {
        return FadeShimmer(
          fadeTheme: FadeTheme.lightReverse,
        );
      },
    );
  }
}
