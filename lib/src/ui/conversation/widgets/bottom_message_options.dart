import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/message/message_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/helpers/clipboard.dart';
import 'package:askany/src/models/message_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/ui/common/widgets/bottom_sheet/bottom_delete.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class BottomMessageOptions extends StatefulWidget {
  final MessageModel message;
  const BottomMessageOptions({required this.message});
  @override
  State<StatefulWidget> createState() => _BottomMessageOptionsState();
}

class _BottomMessageOptionsState extends State<BottomMessageOptions> {
  late List<String> messageOptions = [];

  @override
  void initState() {
    super.initState();
    messageOptions.addAll(widget.message.isMe ? MY_MESSAGE_OPTIONS : FRIEND_MESSAGE_OPTIONS);

    if (widget.message.isMe && (widget.message.isSending || widget.message.isError)) {
      messageOptions.removeAt(1);
    }

    if (widget.message.data.isEmpty) {
      messageOptions = [MY_MESSAGE_OPTIONS[4]];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 12.sp, horizontal: 10.sp),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            decoration: BoxDecoration(
              color: Colors.grey.shade100,
              borderRadius: BorderRadius.circular(8.sp),
            ),
            child: ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              padding: EdgeInsets.zero,
              shrinkWrap: true,
              itemCount: messageOptions.length,
              itemBuilder: (context, index) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    index > 0 ? dividerChat : SizedBox(),
                    _buildOption(
                      text: messageOptions[index],
                      isDanger: index == messageOptions.length - 1,
                      handlePressed: () {
                        if (messageOptions[index] == MY_MESSAGE_OPTIONS[0]) {
                          CustomClipboard.copy(widget.message.data);
                        } else if (messageOptions[index] == MY_MESSAGE_OPTIONS[1]) {
                          AppBloc.messageBloc.add(
                            PickEditMessageEvent(
                              message: widget.message,
                            ),
                          );
                        } else if (messageOptions[index] == MY_MESSAGE_OPTIONS[2]) {
                          AppBloc.messageBloc.add(
                            QuoteMessageEvent(
                              message: widget.message,
                            ),
                          );
                        } else if (messageOptions[index] == MY_MESSAGE_OPTIONS[3]) {
                          AppBloc.messageBloc.add(
                            SelectMessageEvent(message: widget.message),
                          );
                        } else if (messageOptions[index] == MY_MESSAGE_OPTIONS[4]) {
                          showModalBottomSheet(
                            context: context,
                            backgroundColor: Colors.transparent,
                            isDismissible: true,
                            isScrollControlled: true,
                            barrierColor: Colors.black38,
                            builder: (context) {
                              return BottomDelete(
                                text: Strings.areYourSureToDeleteThisMessage.i18n,
                                handlePressed: () {
                                  AppBloc.messageBloc.add(
                                    DeleteMessageEvent(message: widget.message),
                                  );
                                },
                              );
                            },
                          );
                        }
                      },
                    ),
                  ],
                );
              },
            ),
          ),
          SizedBox(height: 4.sp),
          Container(
            height: 40.sp,
            decoration: BoxDecoration(
              color: Colors.grey.shade100,
              borderRadius: BorderRadius.circular(8.sp),
            ),
            child: _buildOption(
              text: Strings.cancel.i18n,
              isCancel: true,
              handlePressed: () {},
            ),
          ),
          SizedBox(height: 4.sp),
        ],
      ),
    );
  }

  Widget _buildOption({
    required String text,
    required Function handlePressed,
    bool isDanger = false,
    bool isCancel = false,
  }) {
    return TouchableOpacity(
      onTap: () {
        AppNavigator.pop();
        handlePressed();
      },
      child: Container(
        height: 42.sp,
        alignment: Alignment.center,
        color: Colors.transparent,
        child: Text(
          text,
          style: TextStyle(
            color: isDanger ? Colors.red.shade700 : colorBlack2,
            fontSize: 13.sp,
            fontWeight: isCancel ? FontWeight.w700 : FontWeight.w600,
          ),
        ),
      ),
    );
  }
}
