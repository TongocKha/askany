import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/notification/notification_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/models/message_model.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
// import 'package:intl/intl.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:intl/intl.dart';

class AppointmentChatCard extends StatefulWidget {
  final MessageModel message;
  AppointmentChatCard({required this.message});
  @override
  State<StatefulWidget> createState() => _AppointmentChatCardState();
}

class _AppointmentChatCardState extends State<AppointmentChatCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100.w,
      padding: EdgeInsets.fromLTRB(
        widget.message.isMe ? 18.sp : 12.sp,
        12.sp,
        widget.message.isMe ? 12.sp : 18.sp,
        14.sp,
      ),
      decoration: BoxDecoration(
        color: colorGreen2.withOpacity(.96),
        borderRadius: BorderRadius.circular(10.sp),
        boxShadow: [
          BoxShadow(
            color: Color(0xFF35524B).withOpacity(.3),
            blurRadius: 4,
            offset: Offset(2, 2), // changes position of shadow
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: widget.message.isMe
            ? CrossAxisAlignment.end
            : CrossAxisAlignment.start,
        children: [
          Text(
            widget.message.request!.status == OFFER_CANCELED
                ? '${Strings.servicePackage.i18n} “${widget.message.request!.requestTitle}” ${Strings.hasBeenCanceled.i18n}'
                : '${widget.message.fullName}  ${Strings.hasOrderedTheServicePackageNamed.i18n}\n“${widget.message.request!.requestTitle}”',
            textAlign: widget.message.isMe ? TextAlign.end : TextAlign.start,
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              height: 1.2.sp,
              fontSize: 12.4.sp,
              color: colorTextAppointmentCard,
              fontWeight: FontWeight.w600,
            ),
          ),
          SizedBox(height: 10.sp),
          widget.message.request!.status == OFFER_CANCELED
              ? Column(
                  crossAxisAlignment: widget.message.isMe
                      ? CrossAxisAlignment.end
                      : CrossAxisAlignment.start,
                  children: [
                    Text(
                      Strings
                          .thisServiceHasBennCanceledBecauseOfExpertIsBusyAtThatTime
                          .i18n,
                      textAlign:
                          widget.message.isMe ? TextAlign.end : TextAlign.start,
                      style: TextStyle(
                        fontSize: 11.sp,
                        color: colorTextAppointmentCard,
                      ),
                    ),
                  ],
                )
              : Column(
                  crossAxisAlignment: widget.message.isMe
                      ? CrossAxisAlignment.end
                      : CrossAxisAlignment.start,
                  children: [
                    Text(
                      '${Strings.time.i18n}: ${DateFormat('HH:mm - dd/MM/yyyy').format(widget.message.request!.startTime)}',
                      textAlign:
                          widget.message.isMe ? TextAlign.end : TextAlign.start,
                      style: TextStyle(
                        fontSize: 11.sp,
                        color: colorTextAppointmentCard,
                      ),
                    ),
                    SizedBox(height: 8.sp),
                    TouchableOpacity(
                      onTap: () {
                        AppBloc.notificationBloc.add(
                          TouchNotificationEvent(
                            offerId: widget.message.request!.id,
                          ),
                        );
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(
                            horizontal: 20.sp, vertical: 6.5.sp),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8.sp),
                          color: colorTextAppointmentCard,
                        ),
                        child: Text(
                          Strings.viewAllDetails.i18n,
                          style: TextStyle(
                            fontSize: 12.sp,
                            color: colorFontGreen,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
        ],
      ),
    );
  }
}
