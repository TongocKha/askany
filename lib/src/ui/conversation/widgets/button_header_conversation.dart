import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ButtonHeaderConversation extends StatefulWidget {
  final String iconAsset;
  final Function handlePressed;
  final double size;
  final Color? color;
  ButtonHeaderConversation({
    required this.iconAsset,
    required this.handlePressed,
    required this.size,
    this.color,
  });
  @override
  State<StatefulWidget> createState() => _ButtonHeaderConversationState();
}

class _ButtonHeaderConversationState extends State<ButtonHeaderConversation> {
  @override
  Widget build(BuildContext context) {
    return TouchableOpacity(
      onTap: () {
        widget.handlePressed();
      },
      child: Container(
        height: 35.sp,
        width: 35.sp,
        color: Colors.transparent,
        alignment: Alignment.center,
        child: Image.asset(
          widget.iconAsset,
          width: widget.size,
          color: widget.color,
        ),
      ),
    );
  }
}
