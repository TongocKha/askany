import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/message/message_bloc.dart';
import 'package:askany/src/bloc/video_call/video_call_bloc.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/helpers/permission_helper.dart';
import 'package:askany/src/models/account_model.dart';
import 'package:askany/src/models/conversation_model.dart';
import 'package:askany/src/models/slide_mode.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/cake_avatar/cake_avatar.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_input.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:askany/src/ui/conversation/widgets/bottom_sheet_search_message.dart';
import 'package:askany/src/ui/conversation/widgets/button_header_conversation.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:askany/src/helpers/extentions/string_extention.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

class ConversationHeader extends StatefulWidget {
  final ConversationModel conversationModel;
  final ItemScrollController itemScrollController;
  const ConversationHeader({
    required this.conversationModel,
    required this.itemScrollController,
  });
  @override
  State<StatefulWidget> createState() => _ConversationHeaderState();
}

class _ConversationHeaderState extends State<ConversationHeader> {
  _startCalling() async {
    bool isCameraGranted =
        await PermissionHelper().checkPermissionAndRequest(Permission.camera);
    bool isMicGranted = await PermissionHelper()
        .checkPermissionAndRequest(Permission.microphone);
    if (isCameraGranted && isMicGranted) {
      AccountModel? receiverUser = widget.conversationModel.receiverUser;
      if (receiverUser != null) {
        AppBloc.videoCallBloc.add(RequestVideoCallEvent(
          receiverId: receiverUser.id!,
          conversation: widget.conversationModel,
        ));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 4.sp, right: 12.sp),
      child: Row(
        children: [
          Expanded(
            child: Row(
              children: [
                TouchableOpacity(
                  onTap: () {
                    AppNavigator.pop();
                  },
                  child: Image.asset(
                    iconBack,
                    width: 35.sp,
                  ),
                ),
                TouchableOpacity(
                  onTap: () {
                    if (!UserLocal().getIsExpert()) {
                      AppNavigator.push(Routes.EXPERT_INFO, arguments: {
                        'expertId': widget.conversationModel.expert.id,
                      });
                    }
                  },
                  child: Row(
                    children: [
                      CakeAvatar(
                        urls: widget.conversationModel.avatarConversations,
                        size: 28.sp,
                      ),
                      SizedBox(width: 10.sp),
                      Text(
                        widget.conversationModel.conversationTitle.formatName(),
                        style: TextStyle(
                          fontSize: 13.sp,
                          fontWeight: FontWeight.w600,
                          color: colorTextChatCard,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Visibility(
            visible: UserLocal().getIsExpert(),
            child: BlocBuilder<VideoCallBloc, VideoCallState>(
                builder: (context, state) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  ButtonHeaderConversation(
                    iconAsset: iconPhone,
                    size: 15.sp,
                    handlePressed: () {
                      if (state is VideoCalling) {
                        AppNavigator.push(Routes.VIDEO_CALL, arguments: {
                          'slide': SlideMode.fade,
                          'conversationId': widget.conversationModel.id,
                        });
                      } else {
                        _startCalling();
                      }
                    },
                  ),
                  ButtonHeaderConversation(
                    iconAsset: iconVideoCall,
                    size: 20.sp,
                    handlePressed: () async {
                      if (state is VideoCalling) {
                        AppNavigator.push(Routes.VIDEO_CALL, arguments: {
                          'slide': SlideMode.fade,
                          'conversationId': widget.conversationModel.id,
                        });
                      } else {
                        _startCalling();
                      }
                    },
                  ),
                ],
              );
            }),
          ),
          ButtonHeaderConversation(
            iconAsset: iconMenuPopup,
            color: colorFontGreen,
            size: 20.sp,
            handlePressed: () async {
              _showPopupMenu(context);
            },
          ),
        ],
      ),
    );
  }

  void _showPopupMenu(contextMenu) async {
    await showMenu(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(6.sp),
      ),
      context: contextMenu,
      position: RelativeRect.fromLTRB(150, 60.sp, 20.sp, 20.sp),
      items: [
        _buildOptionMenu(
          title: 'Tìm kiếm',
          onTap: () {
            dialogAnimationWrapper(
              dismissible: true,
              slideFrom: SlideMode.bot,
              borderRadius: 9.sp,
              child: DialogInput(
                hideInputField: "Nhập từ khóa để tìm kiếm...",
                handleFinish: (input) {
                  AppBloc.messageBloc.add(SearchMessageEvent(searchKey: input));
                  showModalBottomSheet(
                    backgroundColor: colorWhite,
                    context: context,
                    isScrollControlled: true,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.vertical(
                        top: Radius.circular(20.sp),
                      ),
                    ),
                    builder: (context) => BottomSheetSearchMessage(
                      conversationModel: widget.conversationModel,
                      itemScrollController: widget.itemScrollController,
                    ),
                  );
                },
                title: "Tìm kiếm tin nhắn",
                buttonTitle: "TÌM KIẾM",
                initialValue: '',
              ),
            );
          },
        ),
        _buildOptionMenu(
          title: 'Tất cả hình ảnh',
          onTap: () {},
        ),
        _buildOptionMenu(
          title: 'Chặn',
          onTap: () {},
        ),
      ],
      elevation: 8.0,
    );
  }

  PopupMenuItem _buildOptionMenu(
      {required String title, required Function onTap}) {
    return PopupMenuItem(
      enabled: false,
      child: TouchableOpacity(
        onTap: () {
          AppNavigator.pop();
          onTap();
        },
        child: Container(
          padding: EdgeInsets.only(
              left: 4.sp, right: 6.sp, bottom: 12.sp, top: 12.sp),
          color: Colors.transparent,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                title,
                style: TextStyle(
                  color: colorBlack2,
                  fontSize: 12.sp,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
