import 'dart:io';

import 'package:askany/src/helpers/photo_helper.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:camerawesome/camerawesome_plugin.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class CameraPreview extends StatefulWidget {
  final Function? handleFinish;
  final bool isEnable;
  const CameraPreview({
    Key? key,
    this.handleFinish,
    required this.isEnable,
  }) : super(key: key);

  @override
  State<CameraPreview> createState() => _CameraPreviewState();
}

class _CameraPreviewState extends State<CameraPreview> {
  final _picker = ImagePicker();
  ValueNotifier<Size> _photoSize = ValueNotifier(Size(26, 26));
  ValueNotifier<Sensors> _sensor = ValueNotifier(Sensors.BACK);
  ValueNotifier<CaptureModes> _captureMode = ValueNotifier(CaptureModes.PHOTO);

  Future getImage({context, source = ImageSource.gallery}) async {
    return await _picker.pickImage(source: source, imageQuality: 100);
  }

  bool isCameraStarted = false;

  @override
  Widget build(BuildContext context) {
    return TouchableOpacity(
      onTap: () async {
        if (widget.isEnable && isCameraStarted) {
          try {
            AppNavigator.pop();
            XFile? image = await getImage(
              context: AppNavigator.context,
              source: ImageSource.camera,
            );
            if (widget.handleFinish != null && image != null) {
              showDialogLoading();
              File? imageReduce = await PhotoHelper().reduceSize(image.path);
              if (imageReduce != null) {
                widget.handleFinish!(imageReduce);
                AppNavigator.pop();
              }
            }
          } catch (exception) {
            print(exception);
          }
        }
      },
      child: Stack(children: [
        Container(
          color: Colors.black,
          child: Center(
            child: Container(
              height: double.infinity,
              child: CameraAwesome(
                selectDefaultSize: (_) => Size(1080, 1080),
                captureMode: _captureMode,
                photoSize: _photoSize,
                sensor: _sensor,
                onCameraStarted: () {
                  setState(() {
                    isCameraStarted = true;
                  });
                },
              ),
            ),
          ),
        ),
        Visibility(
          visible: !isCameraStarted,
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(
                color: Colors.transparent,
              ),
            ),
            height: double.infinity,
            child: Center(
              child: SizedBox(
                width: 32.sp,
                height: 32.sp,
                child: CircularProgressIndicator(
                  color: Colors.green,
                ),
              ),
            ),
          ),
        ),
      ]),
    );
  }

  @override
  void dispose() {
    debugPrint("169: dispose");
    _photoSize.dispose();
    _captureMode.dispose();
    super.dispose();
  }
}
