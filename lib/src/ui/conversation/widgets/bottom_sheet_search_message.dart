import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/message/message_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:askany/src/models/conversation_model.dart';
import 'package:askany/src/models/message_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/ui/chat/widgets/chat_list_shimmer.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:askany/src/ui/common/widgets/empty/empty.dart';
import 'package:askany/src/ui/conversation/widgets/search_message_card.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

class BottomSheetSearchMessage extends StatefulWidget {
  final ConversationModel conversationModel;
  final ItemScrollController itemScrollController;

  BottomSheetSearchMessage({
    required this.conversationModel,
    required this.itemScrollController,
  });
  @override
  State<BottomSheetSearchMessage> createState() =>
      _BottomSheetSearchMessageState();
}

class _BottomSheetSearchMessageState extends State<BottomSheetSearchMessage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        height: 80.h,
        child: Column(
          children: [
            SizedBox(
              height: 6.sp,
            ),
            Container(
              width: 60.sp,
              height: 3.sp,
              decoration: BoxDecoration(
                  color: colorGray4,
                  borderRadius: BorderRadius.circular(1.5.sp)),
            ),
            SizedBox(
              height: 21.sp,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 16.sp),
              alignment: Alignment.centerLeft,
              height: 24.sp,
              child: Text(
                'Kết quả tìm kiếm',
                style: text15w700cBlack2,
              ),
            ),
            SizedBox(height: 12.sp),
            Divider(
              color: colorGray2,
              thickness: 0.5.sp,
            ),
            SizedBox(height: 12.sp),
            BlocBuilder<MessageBloc, MessageState>(
              builder: (context, state) {
                if (state is GettingMessage || state is GetDoneMessage) {
                  List<MessageModel> searchMessages = state.props[6];
                  return searchMessages.isNotEmpty
                      ? Expanded(
                          child: RawScrollbar(
                            isAlwaysShown: true,
                            thumbColor: colorGreen4,
                            radius: Radius.circular(30.sp),
                            thickness: 4.sp,
                            child: ListView.builder(
                              shrinkWrap: true,
                              padding: EdgeInsets.zero,
                              itemCount: searchMessages.length,
                              itemBuilder: (context, index) {
                                return TouchableOpacity(
                                  onTap: () {
                                    print(widget.conversationModel.id);
                                    showDialogLoading();
                                    AppBloc.messageBloc.add(
                                      ScrollMessageSearchEvent(
                                        index: searchMessages[index].index ?? 0,
                                        itemScrollController:
                                            widget.itemScrollController,
                                        messageId: searchMessages[index].id,
                                      ),
                                    );
                                    AppNavigator.pop();
                                    AppNavigator.pop();
                                  },
                                  child: SearchMessageCard(
                                    conversationModel: widget.conversationModel,
                                    isTyping: false,
                                    messageModel: searchMessages[index],
                                  ),
                                );
                              },
                            ),
                          ),
                        )
                      : Empty(
                          image: Image.asset(
                            imageCategoryEmpty,
                            width: 120.sp,
                            height: 80.sp,
                          ),
                          text: 'Không có kết quả nào được tìm kiếm!!',
                          backgroundColor: Colors.transparent,
                        );
                }
                return ChatListShimmer();
              },
            ),
            SizedBox(height: 12.sp),
          ],
        ),
      ),
    );
  }
}
