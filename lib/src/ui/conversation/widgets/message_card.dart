import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/message/message_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/helpers/date_time_helper.dart';
import 'package:askany/src/helpers/swipe_to_helper.dart';
import 'package:askany/src/helpers/url_launcher_helper.dart';
import 'package:askany/src/models/conversation_model.dart';
import 'package:askany/src/models/message_model.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/custom_image/network_image/cached_image.dart';
import 'package:askany/src/ui/conversation/widgets/appointment_chat_card.dart';
import 'package:askany/src/ui/conversation/widgets/bottom_message_options.dart';
import 'package:askany/src/ui/conversation/widgets/image_body_message_card.dart';
import 'package:askany/src/ui/conversation/widgets/link_preview_card.dart';
import 'package:askany/src/ui/conversation/widgets/slide_animation_container.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

class MessageCard extends StatefulWidget {
  final MessageModel messageModel;
  final MessageModel? prevModel;
  final MessageModel? nextModel;
  final bool isShowAvatar;
  final ConversationModel? conversationModel;
  final bool isChatInCall;
  final ItemScrollController controller;
  final AnimationController animationController;

  MessageCard({
    required this.messageModel,
    required this.conversationModel,
    this.nextModel,
    this.prevModel,
    this.isShowAvatar = false,
    this.isChatInCall = false,
    required this.controller,
    required this.animationController,
  });

  @override
  State<StatefulWidget> createState() => _MessageCardState();
}

class _MessageCardState extends State<MessageCard> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        widget.prevModel == null ||
                isShowTime(widget.messageModel.createdAt, widget.prevModel!.createdAt)
            ? Container(
                padding: EdgeInsets.only(top: 20.sp, bottom: 12.sp),
                child: Text(
                  DateFormat('dd/MM/yyyy HH:mm').format(widget.messageModel.createdAt),
                  style: TextStyle(
                    color: colorTimeChat,
                    fontSize: 10.sp,
                  ),
                ),
              )
            : SizedBox(),
        SwipeTo(
          onLeftSwipe: (widget.messageModel.isMe && widget.messageModel.request == null)
              ? () {
                  AppBloc.messageBloc.add(
                    QuoteMessageEvent(
                      message: widget.messageModel,
                    ),
                  );
                }
              : null,
          onRightSwipe: (!widget.messageModel.isMe && widget.messageModel.request == null)
              ? () {
                  AppBloc.messageBloc.add(
                    QuoteMessageEvent(
                      message: widget.messageModel,
                    ),
                  );
                }
              : null,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment:
                widget.messageModel.isMe ? MainAxisAlignment.end : MainAxisAlignment.start,
            children: [
              SizedBox(width: 5.sp),
              !widget.messageModel.isMe && widget.isShowAvatar
                  ? CustomNetworkImage(
                      height: 20.sp,
                      width: 20.sp,
                      urlToImage: widget.conversationModel!
                          .avatarByUserSender(widget.messageModel.userSender)
                          ?.urlToImage,
                    )
                  : SizedBox(height: 20.sp, width: 20.sp),
              widget.messageModel.request != null
                  ? Container(
                      margin: EdgeInsets.only(
                        right: 4.sp,
                        left: 4.sp,
                        bottom: 10.sp,
                      ),
                      constraints: BoxConstraints(
                        maxWidth: 78.5.w,
                      ),
                      child: AppointmentChatCard(
                        message: widget.messageModel,
                      )
                      // : MessageCallCard(
                      //     messageCall: widget.messageModel.call!,
                      //   ),
                      )
                  : BlocBuilder<MessageBloc, MessageState>(
                      builder: (context, state) {
                        List<MessageModel>? selectedMessages = state.props[3];
                        bool isSelectedThisMessage = false;
                        bool isCannotSelected = false;
                        Color colorSelectedBox =
                            widget.messageModel.isMe ? colorGray1 : colorGreen3;

                        if (!widget.messageModel.isDeleted &&
                            !widget.messageModel.isSending &&
                            !widget.messageModel.isError) {
                          if (selectedMessages != null) {
                            isSelectedThisMessage = selectedMessages.contains(widget.messageModel);
                          }
                        } else {
                          isCannotSelected = true;
                        }

                        return selectedMessages == null
                            ? CustomSlideAnimationWidget(
                                isMyMessage: widget.messageModel.isMe,
                                animationController:
                                    widget.messageModel.animationController ?? null,
                                child: senderLayout(),
                              )
                            : CustomSlideAnimationWidget(
                                isMyMessage: widget.messageModel.isMe,
                                animationController:
                                    widget.messageModel.animationController ?? null,
                                child: Expanded(
                                  child: TouchableOpacity(
                                    onTap: () => AppBloc.messageBloc.add(
                                      SelectMessageEvent(
                                        message: widget.messageModel,
                                      ),
                                    ),
                                    child: Container(
                                      color: Colors.transparent,
                                      child: Row(
                                        children: [
                                          Expanded(child: senderLayout(canPressed: false)),
                                          isCannotSelected
                                              ? SizedBox(
                                                  height: 15.sp,
                                                  width: 15.sp,
                                                )
                                              : Container(
                                                  height: 15.sp,
                                                  width: 15.sp,
                                                  padding: EdgeInsets.all(3.sp),
                                                  alignment: Alignment.center,
                                                  child: Image.asset(
                                                    iconCheckInBox,
                                                    width: 6.5.sp,
                                                    color: Colors.white,
                                                  ),
                                                  decoration: BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    color: isSelectedThisMessage
                                                        ? colorSelectedBox
                                                        : Colors.transparent,
                                                    border: isSelectedThisMessage
                                                        ? null
                                                        : Border.all(
                                                            color: colorSelectedBox,
                                                            width: 0.85.sp,
                                                          ),
                                                  ),
                                                ),
                                          Container(
                                            width: 6.sp,
                                            color: Colors.transparent,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              );
                      },
                    ),
            ],
          ),
        ),
      ],
    );
  }

  Widget senderLayout({bool canPressed = true}) {
    return Column(
      crossAxisAlignment:
          widget.messageModel.isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
      children: [
        widget.messageModel.isSending
            ? Padding(
                padding: EdgeInsets.only(bottom: 2.sp, right: 12.sp),
                child: Text(
                  Strings.sending.i18n,
                  textAlign: TextAlign.end,
                  style: TextStyle(
                    color: widget.isChatInCall ? colorGrey3 : colorGray2,
                    fontSize: 9.5.sp,
                  ),
                ),
              )
            : widget.messageModel.isError
                ? TouchableOpacity(
                    onTap: () => AppBloc.messageBloc.add(
                      RetrySendMessageEvent(message: widget.messageModel, files: null),
                    ),
                    child: Container(
                      padding: EdgeInsets.only(bottom: 4.sp, right: 12.sp, top: 6.sp),
                      color: Colors.transparent,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text(
                            Strings.failedRetry.i18n,
                            textAlign: TextAlign.end,
                            style: TextStyle(
                              color: colorFinished,
                              fontSize: 9.5.sp,
                            ),
                          ),
                          SizedBox(width: 2.sp),
                          Icon(
                            Icons.refresh,
                            color: colorFinished,
                            size: 11.5.sp,
                          ),
                        ],
                      ),
                    ),
                  )
                : SizedBox(),
        TouchableOpacity(
          onTap: () async {
            if (!canPressed) {
              AppBloc.messageBloc.add(
                SelectMessageEvent(message: widget.messageModel),
              );
            } else if (widget.messageModel.parent != null) {
              AppBloc.messageBloc.add(
                ScrollToMessageEvent(
                  animationController: widget.animationController,
                  parentMessageId: widget.messageModel.parent!.id,
                  controller: widget.controller,
                ),
              );
            }
          },
          onLongPress: () {
            if (canPressed && !widget.messageModel.isDeleted) {
              showModalBottomSheet(
                context: context,
                backgroundColor: Colors.transparent,
                isDismissible: true,
                barrierColor: Colors.black38,
                isScrollControlled: true,
                builder: (context) {
                  return BottomMessageOptions(
                    message: widget.messageModel,
                  );
                },
              );
            }
          },
          child: Column(
            crossAxisAlignment:
                widget.messageModel.isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
            children: [
              Visibility(
                visible: getImages.isNotEmpty && !widget.messageModel.isDeleted,
                child: Container(
                  margin: EdgeInsets.only(
                    right: 8.sp,
                    left: 5.sp,
                    bottom: 2.sp,
                  ),
                  constraints: BoxConstraints(
                    maxWidth: 65.w,
                    maxHeight: 55.w,
                  ),
                  child: ImageBodyMessage(
                    onLongTap: () {
                      if (canPressed &&
                          !widget.messageModel.isDeleted &&
                          widget.messageModel.isMe) {
                        showModalBottomSheet(
                          context: context,
                          backgroundColor: Colors.transparent,
                          isDismissible: true,
                          barrierColor: Colors.black38,
                          isScrollControlled: true,
                          builder: (context) {
                            return BottomMessageOptions(
                              message: widget.messageModel,
                            );
                          },
                        );
                      }
                    },
                    images: getImages,
                    urlToImages: widget.messageModel.images == null
                        ? null
                        : widget.messageModel.images!.map((e) => e.urlToImage).toList(),
                    messageModel: widget.messageModel,
                  ),
                ),
              ),
              widget.messageModel.data.isEmpty
                  ? SizedBox(height: 16.sp)
                  : Container(
                      margin: EdgeInsets.only(
                        right: 8.sp,
                        left: 5.sp,
                        bottom: widget.nextModel == null ||
                                (widget.nextModel!.userSender == widget.messageModel.userSender)
                            ? 8.sp
                            : 16.sp,
                      ),
                      constraints: BoxConstraints(
                        maxWidth: 65.w,
                      ),
                      decoration: BoxDecoration(
                        border: widget.messageModel.isDeleted
                            ? Border.all(
                                color: colorGray2,
                                width: .5.sp,
                              )
                            : null,
                        color: widget.messageModel.isDeleted
                            ? Colors.white
                            : widget.messageModel.isMe
                                ? (widget.isChatInCall ? colorGreen3 : colorBackgroundSender)
                                : (widget.isChatInCall
                                    ? colorBackgroundReceiverInCall
                                    : colorBackgroundReceiver),
                        borderRadius: BorderRadius.all(Radius.circular(10.sp)),
                      ),
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 16.sp, vertical: 12.sp),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Visibility(
                              visible: widget.messageModel.parent != null,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    '❝ ${widget.messageModel.parent?.data} ❞',
                                    style: TextStyle(
                                      fontStyle: FontStyle.italic,
                                      fontSize: 10.sp,
                                      color: colorGray2,
                                    ),
                                  ),
                                  Text(
                                    '――――――',
                                    style: TextStyle(
                                      fontStyle: FontStyle.italic,
                                      fontSize: 10.sp,
                                      color: colorGray4,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            getMessage(),
                          ],
                        ),
                      ),
                    ),
            ],
          ),
        ),
        ...buildLinkPreview().map(
          (url) => LinkPreviewCard(
            url: url,
          ),
        ),
      ],
    );
  }

  getMessage({String? text}) {
    return RichText(
      textAlign: widget.messageModel.isMe ? TextAlign.right : TextAlign.left,
      text: TextSpan(
        style: TextStyle(
          color: widget.isChatInCall
              ? colorGrey3
              : widget.messageModel.isDeleted
                  ? colorGray2
                  : widget.messageModel.isMe
                      ? colorTextSender
                      : colorBlack2,
          fontSize: widget.messageModel.isDeleted ? 10.sp : 12.sp,
          fontStyle: widget.messageModel.isDeleted ? FontStyle.italic : FontStyle.normal,
        ),
        children: [
          ...(text ?? widget.messageModel.data)
              .replaceAll('\n', ' \n')
              .split(" ")
              .asMap()
              .map(
                (index, word) {
                  String formatWord =
                      word + (index < widget.messageModel.data.split(' ').length - 1 ? ' ' : '');

                  return MapEntry(
                    index,
                    !UrlLauncherHelper().isWebsite(word)
                        ? TextSpan(text: formatWord)
                        : TextSpan(
                            recognizer: TapGestureRecognizer()
                              ..onTap = () => UrlLauncherHelper.launchUrl(word),
                            text: formatWord,
                            style: TextStyle(
                              color: widget.isChatInCall ? colorGreen5 : colorFontGreen,
                              decoration: TextDecoration.underline,
                            ),
                          ),
                  );
                },
              )
              .values
              .toList(),
        ],
      ),
    );
  }

  List<String> buildLinkPreview({String? text}) {
    List<String> listUrl = [];
    List<String> words =
        (text ?? widget.messageModel.data).replaceAll('\n', ' \n').split(" ").toList();
    words.forEach((word) {
      if (UrlLauncherHelper().isWebsite(word)) {
        listUrl.add(word);
      }
    });
    return listUrl;
  }

  List<ImageProvider> get getImages {
    if (widget.messageModel.localImages != null && widget.messageModel.localImages!.isNotEmpty) {
      return widget.messageModel.localImages!.map((e) => FileImage(e)).toList();
    }
    if (widget.messageModel.images != null && widget.messageModel.images!.isNotEmpty) {
      return widget.messageModel.images!.map((e) => NetworkImage(e.urlToImage)).toList();
    }

    return [];
  }
}
