import 'dart:io';

import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

// ignore: must_be_immutable
class FeatureFilterButton extends StatefulWidget {
  File image;
  final Key key1;
  Function handlePressed;
  Function handlePressedRevert;
  FeatureFilterButton({
    Key? key,
    required this.handlePressedRevert,
    required this.key1,
    required this.image,
    required this.handlePressed,
  }) : super(key: key);

  @override
  FeatureFilterButtonState createState() => FeatureFilterButtonState();
}

class FeatureFilterButtonState extends State<FeatureFilterButton> {
  int? _index;

  @override
  void initState() {
    super.initState();
    print("filter");
    print(widget.image);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: Container(
            width: 100.w,
            child: _index != null
                ? RepaintBoundary(
                    key: widget.key1,
                    child: ColorFiltered(
                      colorFilter: ColorFilter.matrix(
                        filters[_index ?? 0],
                      ),
                      child: Image.file(
                        widget.image,
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  )
                : RepaintBoundary(
                    key: widget.key1,
                    child: Image.file(
                      widget.image,
                      fit: BoxFit.fitWidth,
                    ),
                  ),
          ),
        ),
        SizedBox(height: 5.sp),
        Container(
          height: 20.h,
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: 16.sp),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    TouchableOpacity(
                      onTap: widget.handlePressedRevert,
                      child: Container(
                        height: 18.sp,
                        width: 18.sp,
                        color: Colors.transparent,
                        margin: EdgeInsets.all(7.sp),
                        child: Image.asset(
                          iconRevertImage,
                          color: colorWhite,
                          fit: BoxFit.fitWidth,
                        ),
                      ),
                    ),
                    Container(
                      child: Text(
                        'Filter',
                        style: TextStyle(color: colorWhite, fontSize: 13.sp),
                      ),
                    ),
                    TouchableOpacity(
                      onTap: widget.handlePressed,
                      child: Container(
                        height: 18.sp,
                        width: 18.sp,
                        color: Colors.transparent,
                        margin: EdgeInsets.all(7.sp),
                        child: Image.asset(
                          iconCheckInBox,
                          color: colorWhite,
                          fit: BoxFit.fitWidth,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Center(
                  child: Container(
                    height: 80.sp,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: filters.length,
                      padding: EdgeInsets.symmetric(horizontal: 16.sp),
                      shrinkWrap: true,
                      itemBuilder: (context, index) {
                        return Row(
                          children: [
                            TouchableOpacity(
                              onTap: () {
                                setState(() {
                                  _index = index;
                                });
                              },
                              child: ColorFiltered(
                                colorFilter: ColorFilter.matrix(filters[index]),
                                child: Container(
                                  height: 60.sp,
                                  width: 50.sp,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15.sp),
                                  ),
                                  child: Image.file(
                                    widget.image,
                                    height: 60.sp,
                                    width: 50.sp,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(width: 7.sp),
                          ],
                        );
                      },
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ],
    );
  }

  void reset() {
    setState(() {});
  }
}
