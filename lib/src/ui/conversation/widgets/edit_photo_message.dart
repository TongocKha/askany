import 'dart:io';
import 'dart:math';

import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/message/message_bloc.dart';
import 'package:askany/src/helpers/drawImage/image_painter.dart';
import 'package:askany/src/configs/themes/app_colors.dart';
import 'package:askany/src/helpers/photo_helper.dart';
import 'package:askany/src/helpers/slider_custom/slider_custom.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/ui/common/screens/edit_photo_screen.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:askany/src/ui/conversation/widgets/filter_button.dart';
import 'package:askany/src/routes/app_navigator_observer.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';

import 'package:path_provider/path_provider.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:image/image.dart' as img;

import 'dart:typed_data';
import 'package:flutter/rendering.dart';
import 'dart:ui' as ui;

class EditPhotoMessageScreen extends StatefulWidget {
  final File file;
  final bool editImage;
  final Function(File)? handleFinish;
  const EditPhotoMessageScreen({
    Key? key,
    required this.file,
    this.editImage = false,
    this.handleFinish,
  }) : super(key: key);

  @override
  State<EditPhotoMessageScreen> createState() => _EditPhotoMessageScreenState();
}

class _EditPhotoMessageScreenState extends State<EditPhotoMessageScreen> {
  final GlobalKey<ImagePainterState> _imageKey = GlobalKey<ImagePainterState>();

  final GlobalKey<ExtendedImageEditorState> editorKey =
      GlobalKey<ExtendedImageEditorState>();
  late EditorCropLayerPainter _cropLayerPainter;
  AspectRatioItem _aspectRatio =
      AspectRatioItem(text: '1*1', value: CropAspectRatios.ratio1_1);

  final GlobalKey<FeatureFilterButtonState> globalKey =
      GlobalKey<FeatureFilterButtonState>();

  RulerPickerController? _rulerPickerController;

  List<bool> listIcon = [true, false, false, false];
  List<String> listRatioString = [
    "1 : 1",
    '3 : 4',
    '4 : 3',
    '9 : 16',
    '16 : 9'
  ];

  double _rotateParamater = 0.0;
  double _memoryRotate = 0.0;
  Future<void> _updateAvatar() async {
    if (AppNavigatorObserver.routeNames.contains(Routes.EDIT_PROFILE)) {
      showDialogLoading();
      File? imageReduce = await PhotoHelper().reduceSize(_images!.path);
      AppNavigator.pop();
      if (imageReduce != null && widget.handleFinish != null) {
        widget.handleFinish!(imageReduce);
        AppNavigator.pop();
      }
    }
  }

  void _cleanRotateParameters() {
    setState(() {
      _rotateParamater = 0.0;
      _buildRotatedImage();
      _buildSlider();
    });
  }

  Widget _buildRotatedImage() {
    return Container(
      width: 100.w,
      child: Transform(
        transform: Matrix4.identity()
          ..setEntry(3, 2, 0.001)
          ..rotateZ(_rotateParamater / 180 * pi),
        alignment: Alignment.center,
        child: Transform.rotate(
          angle: 0.0,
          origin: Offset(0, 0),
          child: Image.file(
            _images!,
            fit: BoxFit.fitWidth,
          ),
        ),
      ),
    );
  }

  Widget _buildSlider() {
    return RulerPicker(
      controller: _rulerPickerController!,
      beginValue: -90,
      endValue: 90,
      initValue: _memoryRotate.toInt(),
      scaleLineStyleList: [
        ScaleLineStyle(color: Colors.grey, width: 1.5, height: 30, scale: 0),
        ScaleLineStyle(color: Colors.grey, width: 1, height: 25, scale: 5),
        ScaleLineStyle(color: Colors.grey, width: 1, height: 15, scale: -1)
      ],
      onValueChange: (value) {
        _changeRotateParam(double.parse(value.toString()));
      },
      width: MediaQuery.of(context).size.width,
      height: 80,
      rulerMarginTop: 8,
    );
  }

  void _changeRotateParam(double value) {
    setState(() {
      _rotateParamater = value;
    });
  }

  void clearItem(int index) {
    listIcon[index] = true;
    listIcon.asMap().forEach((i, bool) {
      if (i != index) {
        listIcon[i] = false;
      }
    });
  }

  late List<bool> listRatio;

  List<bool> avatarCropOptions = [true];
  List<bool> messageCropOptions = [true, false, false, false, false];

  void clearRatio(int index) {
    if (!listRatio[index]) {
      listRatio[index] = !listRatio[index];
    }

    listRatio.asMap().forEach((i, bool) {
      if (i != index) {
        listRatio[i] = false;
      }
    });
  }

  File? _images;
  int? index;

  @override
  void initState() {
    _cropLayerPainter = widget.editImage
        ? const CircleEditorCropLayerPainter()
        : const CustomCropLayerPainter();

    _images = widget.file;

    super.initState();
    if (AppNavigatorObserver.routeNames.contains(Routes.EDIT_PROFILE)) {
      listRatio = avatarCropOptions;
    } else {
      listRatio = messageCropOptions;
    }

    _rulerPickerController = RulerPickerController(value: 0);
  }

  Future<void> _rotateImage() async {
    try {
      List<int> imageBytes = await _images!.readAsBytes();
      final originalImage = img.decodeImage(imageBytes);
      img.Image _fixedImage;

      if (_memoryRotate == 0.0) {
        _fixedImage = img.copyRotate(originalImage!, _rotateParamater);
        _memoryRotate = _rotateParamater;
      } else {
        if (_memoryRotate != _rotateParamater) {
          _fixedImage =
              img.copyRotate(originalImage!, _rotateParamater - _memoryRotate);
          _memoryRotate = _rotateParamater;
        } else
          _fixedImage = img.copyRotate(originalImage!, 0.0);
      }

      File fixedFile = await _images!.writeAsBytes(img.encodeJpg(_fixedImage));
      setState(() {
        _images = fixedFile;
        print("rotate");
        print(fixedFile);
      });
    } catch (e) {
      print(e);
    }
  }

  Future<void> _capturePng() async {
    showDialogLoading();

    final RenderRepaintBoundary boundary =
        globalKey.currentContext!.findRenderObject()! as RenderRepaintBoundary;
    ui.Image boxImage = await boundary.toImage(pixelRatio: 1);
    ByteData? byteData =
        await boxImage.toByteData(format: ui.ImageByteFormat.png);
    Uint8List pngBytes = byteData!.buffer.asUint8List();
    final directory = (await getApplicationDocumentsDirectory()).path;
    await Directory('$directory/sample').create(recursive: true);
    final fullPath =
        '$directory/sample/${DateTime.now().millisecondsSinceEpoch}.png';
    setState(() {
      File file = File(fullPath);
      file.writeAsBytesSync(pngBytes);
      _images = file;
    });
    AppNavigator.pop();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarTitleBack(
        context,
        '',
        backgroundColor: colorBlack,
        actions: [
          Visibility(
            visible:
                AppNavigatorObserver.routeNames.contains(Routes.PHOTO_VIEWER),
            child: TouchableOpacity(
              onTap: () {
                List<File> file = [];
                setState(() {
                  file.insert(0, _images!);
                });
                AppBloc.messageBloc.add(
                  SendMessageEvent(
                    message: '',
                    files: file,
                  ),
                );
                AppNavigator.pop();
                AppNavigator.pop();
              },
              child: Container(
                alignment: Alignment.center,
                color: Colors.transparent,
                padding: EdgeInsets.only(
                  right: 10.sp,
                  // top: 12.sp,
                ),
                child: Text(
                  'Send',
                  style: TextStyle(
                    color: colorWhite,
                    fontSize: 12.sp,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ),
          ),
          SizedBox(width: 20.sp),
          TouchableOpacity(
            onTap: () async {
              if (widget.handleFinish != null) {
                _updateAvatar();
              }
            },
            child: Container(
              alignment: Alignment.center,
              color: Colors.transparent,
              padding: EdgeInsets.only(
                right: 10.sp,
                // top: 12.sp,
              ),
              child: Text(
                'Export',
                style: TextStyle(
                  color: colorWhite,
                  fontSize: 12.sp,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
          ),
        ],
      ),
      body: Container(
        color: colorBlack,
        child: SafeArea(
          child: Column(
            children: [
              Expanded(
                child: Container(
                  width: 100.w,
                  color: Colors.transparent,
                  // padding: EdgeInsets.only(top: 12.sp),
                  child: listIcon[0]
                      ? FeatureFilterButton(
                          key1: globalKey,
                          handlePressed: _capturePng,
                          image: _images!,
                          handlePressedRevert: () {
                            setState(() {
                              globalKey.currentState!.reset();
                            });
                          },
                        )
                      : listIcon[1]
                          ? ExtendedImage.file(
                              _images!,
                              constraints: BoxConstraints(
                                  minWidth: 100.w, minHeight: 100.h),
                              fit: BoxFit.contain,
                              mode: ExtendedImageMode.editor,
                              enableLoadState: true,
                              extendedImageEditorKey: editorKey,
                              cacheRawData: true,
                              initEditorConfigHandler: (state) => EditorConfig(
                                editorMaskColorHandler:
                                    (context, pointerDown) =>
                                        Colors.black.withOpacity(.5),
                                maxScale: 8.0,
                                cropRectPadding: const EdgeInsets.all(0.0),
                                cropLayerPainter: _cropLayerPainter,
                                initCropRectType: InitCropRectType.imageRect,
                                cropAspectRatio: _aspectRatio.value,
                                cornerColor: Colors.white,
                                cornerSize: Size(30.0, 5.0),
                                lineHeight: 0.75.sp,
                              ),
                            )
                          : listIcon[3]
                              ? ImagePainter.file(
                                  _images!,
                                  key: _imageKey,
                                  scalable: true,
                                  initialStrokeWidth: 2,
                                  initialColor: Colors.white,
                                  initialPaintMode: PaintMode.freeStyle,
                                  handlePress: () async {
                                    showDialogLoading();
                                    final image = await _imageKey.currentState!
                                        .exportImage();
                                    final directory =
                                        (await getApplicationDocumentsDirectory())
                                            .path;
                                    await Directory('$directory/sample')
                                        .create(recursive: true);
                                    final fullPath =
                                        '$directory/sample/${DateTime.now().millisecondsSinceEpoch}.png';
                                    File imgFile = File('$fullPath');
                                    imgFile.writeAsBytesSync(image!);
                                    setState(() {
                                      _images = imgFile;
                                    });

                                    AppNavigator.pop();
                                  },
                                )
                              : listIcon[2]
                                  ? _buildRotatedImage()
                                  : Image.file(
                                      _images!,
                                      fit: BoxFit.fitWidth,
                                    ),
                ),
              ),
              SizedBox(height: (listIcon[0] || listIcon[3]) ? 0.sp : 5.sp),
              Container(
                child: Column(
                  children: [
                    featureCropButton(visible: listIcon[1]),
                    featureRotateButton(visible: listIcon[2]),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          ButtonIconEdit(
                            icon: imgFilterImage,
                            forcus: listIcon[0],
                            handlePressed: () {
                              setState(() {
                                clearItem(0);
                              });
                            },
                          ),
                          ButtonIconEdit(
                            icon: imgCrop,
                            forcus: listIcon[1],
                            handlePressed: () {
                              setState(() {
                                clearItem(1);
                              });
                            },
                          ),
                          ButtonIconEdit(
                            icon: imgRotate,
                            forcus: listIcon[2],
                            handlePressed: () {
                              setState(() {
                                clearItem(2);
                              });
                            },
                          ),
                          ButtonIconEdit(
                            icon: imgPaint,
                            forcus: listIcon[3],
                            handlePressed: () {
                              setState(
                                () {
                                  clearItem(3);
                                },
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 16.sp),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget featureRotateButton({required bool visible}) {
    return Visibility(
      visible: visible,
      child: Container(
        height: 20.h,
        color: colorBlack,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 16.sp),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  TouchableOpacity(
                    onTap: () {
                      _cleanRotateParameters();
                    },
                    child: Container(
                      height: 18.sp,
                      width: 18.sp,
                      color: Colors.transparent,
                      margin: EdgeInsets.all(7.sp),
                      child: Image.asset(
                        iconRevertImage,
                        color: colorWhite,
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ),
                  Container(
                    child: Text(
                      'Rotate',
                      style: TextStyle(color: colorWhite, fontSize: 13.sp),
                    ),
                  ),
                  TouchableOpacity(
                    onTap: () {
                      showDialogLoading();
                      _rotateImage();
                      AppNavigator.pop();
                    },
                    child: Container(
                      height: 18.sp,
                      width: 18.sp,
                      color: Colors.transparent,
                      margin: EdgeInsets.all(7.sp),
                      child: Image.asset(
                        iconCheckInBox,
                        color: colorWhite,
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 16.sp,
                ),
                child: Center(
                  child: Container(
                    child: _buildSlider(),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget featureCropButton({required bool visible}) {
    return Visibility(
      visible: visible,
      child: Container(
        height: 20.h,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 16.sp),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  TouchableOpacity(
                    onTap: () {
                      setState(() {
                        editorKey.currentState!.reset();
                      });
                    },
                    child: Container(
                      height: 18.sp,
                      width: 18.sp,
                      color: Colors.transparent,
                      margin: EdgeInsets.all(7.sp),
                      child: Image.asset(
                        iconRevertImage,
                        color: colorWhite,
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ),
                  Container(
                    child: Text(
                      'Cropper',
                      style: TextStyle(color: colorWhite, fontSize: 13.sp),
                    ),
                  ),
                  TouchableOpacity(
                    onTap: () async {
                      showDialogLoading();
                      File image =
                          await PhotoHelper().cropImageDataWithDartLibrary(
                        state: editorKey.currentState!,
                      );
                      File? imageReduce =
                          await PhotoHelper().reduceSize(image.path);

                      if (imageReduce != null) {
                        setState(() {
                          _images = imageReduce;
                        });

                        AppNavigator.pop();
                      }
                    },
                    child: Container(
                      height: 18.sp,
                      width: 18.sp,
                      color: Colors.transparent,
                      margin: EdgeInsets.all(7.sp),
                      child: Image.asset(
                        iconCheckInBox,
                        color: colorWhite,
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Center(
                child: Container(
                    height: 80.sp,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        ...listRatio
                            .asMap()
                            .map((index, item) {
                              return MapEntry(
                                index,
                                TouchableOpacity(
                                  onTap: () {
                                    setState(() {
                                      if (!listRatio[index]) {
                                        _aspectRatio = AspectRatioItem(
                                            text: listRatioString[index],
                                            value:
                                                ratio[listRatioString[index]]!);
                                        clearRatio(index);
                                      }
                                    });
                                  },
                                  child: Row(
                                    children: [
                                      Column(
                                        children: [
                                          index == 0
                                              ? Container(
                                                  height: 25.sp,
                                                  width: 25.sp,
                                                  decoration: BoxDecoration(
                                                    border: Border.all(
                                                      width: 1.sp,
                                                      color: !listRatio[index]
                                                          ? colorWhite
                                                          : Colors.amber,
                                                    ),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            5.sp),
                                                  ),
                                                )
                                              : Container(
                                                  height: index % 2 == 0
                                                      ? 25.sp
                                                      : 32.sp,
                                                  width: index % 2 != 0
                                                      ? 25.sp
                                                      : 32.sp,
                                                  decoration: BoxDecoration(
                                                    border: Border.all(
                                                      width: 1.sp,
                                                      color: !listRatio[index]
                                                          ? colorWhite
                                                          : Colors.amber,
                                                    ),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            5.sp),
                                                  ),
                                                ),
                                          SizedBox(
                                            height:
                                                index % 2 != 0 ? 4.sp : 11.sp,
                                          ),
                                          Text(
                                            listRatioString[index],
                                            style: TextStyle(
                                              color: !listRatio[index]
                                                  ? colorWhite
                                                  : Colors.amber,
                                              fontSize: 10.sp,
                                            ),
                                          ),
                                        ],
                                      ),
                                      index != listRatio.length - 1
                                          ? SizedBox(width: 32.sp)
                                          : SizedBox(width: 0.sp),
                                    ],
                                  ),
                                ),
                              );
                            })
                            .values
                            .toList(),
                      ],
                    )),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class ButtonIconEdit extends StatelessWidget {
  final String icon;
  final Function handlePressed;
  final bool forcus;
  const ButtonIconEdit({
    Key? key,
    this.forcus = false,
    required this.icon,
    required this.handlePressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TouchableOpacity(
          onTap: handlePressed,
          child: Container(
            height: 37.sp,
            width: 37.sp,
            padding: EdgeInsets.all(4.sp),
            decoration: BoxDecoration(
              color: !forcus ? Colors.transparent : colorGreen4,
              borderRadius: BorderRadius.circular(4.sp),
            ),
            child: Image.asset(
              icon,
              fit: BoxFit.contain,
              color: !forcus ? Colors.white : Colors.black,
            ),
          ),
        ),
      ],
    );
  }
}
