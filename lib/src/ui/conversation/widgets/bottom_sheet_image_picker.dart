import 'dart:io';
import 'dart:typed_data';
import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/photo_manager/photo_manager_bloc.dart';
import 'package:askany/src/helpers/utils/logger.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/conversation/widgets/photo_gird_view.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ImagePickerBottomSheet extends StatefulWidget {
  const ImagePickerBottomSheet({Key? key}) : super(key: key);
  @override
  State<ImagePickerBottomSheet> createState() => _ImagePickerBottomSheetState();
}

class _ImagePickerBottomSheetState extends State<ImagePickerBottomSheet>
    with SingleTickerProviderStateMixin {
  late double min = 0.45, initial = 0.455, max = 0.9;
  double heightRate = 0;
  bool isGetMoreWhenScrollUp = false;

  @override
  void dispose() {
    AppBloc.photoManagerBloc.add(DisposePhotoEvent());
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return NotificationListener<DraggableScrollableNotification>(
      onNotification: (notification) {
        UtilLogger.log("${notification.runtimeType}");
        setState(() {
          heightRate = notification.extent;
        });

        if (!isGetMoreWhenScrollUp && heightRate > initial) {
          AppBloc.photoManagerBloc.add(GetPhotoManagerEvent());
        }
        return false;
      },
      child: BlocBuilder<PhotoManagerBloc, PhotoManagerState>(
        builder: (context, state) {
          List<Uint8List> imagesChoosen = (state.props[1] as List).cast();
          List<String> images = (state.props[1] as List).cast();
          return Stack(
            children: [
              makeDismissible(
                child: DraggableScrollableSheet(
                  initialChildSize: initial,
                  maxChildSize: max,
                  minChildSize: min,
                  builder: (_, controller) {
                    return Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                      ),
                      padding: EdgeInsets.fromLTRB(4.sp, 4.sp, 4.sp, 0),
                      child: Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 10.sp),
                            child: Column(
                              children: [
                                SizedBox(height: 10.sp),
                                Center(
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      TouchableOpacity(
                                        onTap: () {
                                          AppBloc.photoManagerBloc
                                              .add(DisposePhotoEvent());
                                          AppBloc.photoManagerBloc
                                              .add(CleanPhotoPickerEvent());
                                          AppNavigator.pop();
                                        },
                                        child: Align(
                                          alignment: Alignment.centerRight,
                                          child: Container(
                                            color: Colors.transparent,
                                            child: Image.asset(
                                              iconRemove,
                                              width: 14.sp,
                                              height: 14.sp,
                                            ),
                                          ),
                                        ),
                                      ),
                                      Column(
                                        children: [
                                          Text(
                                            'Tất cả hình ảnh',
                                            style: TextStyle(
                                              fontSize: 14.sp,
                                              fontWeight: FontWeight.w400,
                                            ),
                                          ),
                                          Text(
                                            "${imagesChoosen.length}/6",
                                            style: TextStyle(
                                              fontSize: 12.sp,
                                              color: Colors.grey,
                                            ),
                                          ),
                                        ],
                                      ),
                                      TouchableOpacity(
                                        onTap: () {
                                          AppNavigator.pop();
                                        },
                                        child: Align(
                                          alignment: Alignment.centerRight,
                                          child: Container(
                                            color: Colors.transparent,
                                            child: Image.asset(
                                              iconCheckGreen,
                                              width: 16.sp,
                                              height: 16.sp,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 8.sp),
                          Expanded(
                            child: PhotosGridView(
                              scrollController: controller,
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                ),
              ),
              Visibility(
                visible: imagesChoosen.length == 1,
                child: Container(
                  margin:
                      EdgeInsets.symmetric(vertical: 16.sp, horizontal: 16.sp),
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: ButtonPrimary(
                      onPressed: () {
                        AppNavigator.push(Routes.EDIT_PHOTO_MESSAGE,
                            arguments: {
                              'file': File(images[0]),
                            });
                      },
                      text: 'Chỉnh sửa',
                    ),
                  ),
                ),
              ),
            ],
          );
        },
      ),
    );
  }

  Widget makeDismissible({required Widget child}) => GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          heightRate = 0;
          AppBloc.photoManagerBloc.add(DisposePhotoEvent());
          AppBloc.photoManagerBloc.add(CleanPhotoPickerEvent());
        },
        child: GestureDetector(
          onTap: () {},
          child: child,
        ),
      );
}
