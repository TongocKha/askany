import 'package:askany/src/models/message_model.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:askany/src/helpers/extentions/string_extention.dart';

class ButtonScrollDown extends StatelessWidget {
  final MessageModel? message;

  const ButtonScrollDown({this.message});

  @override
  Widget build(BuildContext context) {
    return message == null
        ? Container(
            height: 30.sp,
            width: 30.sp,
            decoration: BoxDecoration(
              color: colorGreen5.withOpacity(.75),
              shape: BoxShape.circle,
            ),
            alignment: Alignment.center,
            child: Icon(
              Icons.arrow_downward,
              color: colorFontGreen,
              size: 15.sp,
            ),
          )
        : Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                decoration: BoxDecoration(
                  color: colorGreen5.withOpacity(.75),
                  borderRadius: BorderRadius.circular(30.sp),
                ),
                padding: EdgeInsets.symmetric(horizontal: 16.sp, vertical: 6.sp),
                alignment: Alignment.center,
                child: Text(
                  message!.data.limitString(limit: 20),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                  style: TextStyle(
                    fontSize: 10.sp,
                    color: colorFontGreen,
                  ),
                ),
              ),
            ],
          );
  }
}
