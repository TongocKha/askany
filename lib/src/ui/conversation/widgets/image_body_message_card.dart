import 'package:askany/src/models/message_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/conversation/widgets/cache_image_message.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ImageBodyMessage extends StatefulWidget {
  final List<ImageProvider> images;
  final List<String>? urlToImages;
  final Function onLongTap;
  final MessageModel messageModel;

  const ImageBodyMessage({
    required this.images,
    required this.urlToImages,
    required this.onLongTap,
    required this.messageModel,
  });
  @override
  State<StatefulWidget> createState() => _ImageBodyPostCard();
}

class _ImageBodyPostCard extends State<ImageBodyMessage> {
  _navigationToPhotoViewer(int currentIndex) {
    AppNavigator.push(Routes.PHOTO_VIEWER, arguments: {
      'images': widget.images,
      'initialIndex': currentIndex,
      'indexListImage': widget.messageModel,
      'urlToImages': widget.urlToImages,
    });
  }

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.horizontal(
        left: Radius.circular(6.sp),
        right: Radius.circular(4.sp),
      ),
      child: widget.images.length == 1
          ? _buildSingleImage(context)
          : widget.images.length == 2
              ? _buildDoubleImage(context)
              : _buildTripleImage(context),
    );
  }

  Widget _buildSingleImage(context) {
    return TouchableOpacity(
      onTap: () => _navigationToPhotoViewer(0),
      onLongPress: widget.onLongTap,
      child: CachedImageMessage(
        urlToImage: widget.urlToImages?.first ?? '',
        radius: BorderRadius.circular(0.sp),
      ),
    );
  }

  Widget _buildDoubleImage(context) {
    return Row(
      children: [
        Expanded(
          child: TouchableOpacity(
            onLongPress: widget.onLongTap,
            onTap: () => _navigationToPhotoViewer(0),
            child: CachedImageMessage(
              urlToImage: widget.urlToImages?.first ?? '',
              radius: BorderRadius.circular(0.sp),
            ),
          ),
        ),
        SizedBox(width: .5.sp),
        Expanded(
          child: TouchableOpacity(
            onLongPress: widget.onLongTap,
            onTap: () => _navigationToPhotoViewer(1),
            child: CachedImageMessage(
              urlToImage: widget.urlToImages?[1] ?? '',
              radius: BorderRadius.circular(0.sp),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildTripleImage(context) {
    return Row(
      children: [
        Expanded(
          child: TouchableOpacity(
            onLongPress: widget.onLongTap,
            onTap: () => _navigationToPhotoViewer(0),
            child: CachedImageMessage(
              urlToImage: widget.urlToImages?.first ?? '',
              radius: BorderRadius.circular(0.sp),
            ),
          ),
        ),
        SizedBox(width: .5.sp),
        Expanded(
          child: Container(
            child: Column(
              children: [
                Expanded(
                  child: TouchableOpacity(
                    onLongPress: widget.onLongTap,
                    onTap: () => _navigationToPhotoViewer(1),
                    child: CachedImageMessage(
                      urlToImage: widget.urlToImages?[1] ?? '',
                      radius: BorderRadius.circular(0.sp),
                    ),
                  ),
                ),
                SizedBox(height: .5.sp),
                Expanded(
                  child: TouchableOpacity(
                    onLongPress: widget.onLongTap,
                    onTap: () => _navigationToPhotoViewer(2),
                    child: Stack(
                      children: [
                        CachedImageMessage(
                          urlToImage: widget.urlToImages?[2] ?? '',
                          radius: BorderRadius.circular(0.sp),
                        ),
                        Visibility(
                          visible: widget.images.length > 3,
                          child: Container(
                            decoration: BoxDecoration(
                              color: Colors.black26,
                            ),
                            alignment: Alignment.center,
                            child: Text(
                              '${widget.images.length - 3}+',
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w400,
                                fontSize: 12.sp,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
