import 'package:askany/src/helpers/device_helper.dart';
import 'package:askany/src/models/conversation_model.dart';
import 'package:askany/src/ui/common/widgets/custom_image/network_image/cached_image.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:just_audio/just_audio.dart';

class TypingWidget extends StatefulWidget {
  final ConversationModel conversationModel;

  const TypingWidget({required this.conversationModel});

  @override
  State<TypingWidget> createState() => _TypingWidgetState();
}

class _TypingWidgetState extends State<TypingWidget> {
  final AudioPlayer _player = AudioPlayer();
  @override
  void initState() {
    super.initState();
    DeviceHelper().typingSound(_player, true);
  }

  @override
  void deactivate() {
    super.deactivate();
    DeviceHelper().typingSound(_player, false);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            SizedBox(width: 5.sp),
            CustomNetworkImage(
              height: 20.sp,
              width: 20.sp,
              urlToImage:
                  widget.conversationModel.avatarByReceiver()?.urlToImage,
            ),
            Container(
              margin: EdgeInsets.only(
                right: 8.sp,
                left: 5.sp,
                bottom: 8.sp,
              ),
              padding: EdgeInsets.symmetric(horizontal: 10.sp, vertical: 8.sp),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(18.sp),
                color: colorBackgroundReceiver,
              ),
              child: Image.asset(
                imgTyping,
                width: 24.sp,
                height: 12.sp,
                color: colorGreen3,
                fit: BoxFit.fitWidth,
              ),
            ),
          ],
        ),
      ],
    );
  }
}
