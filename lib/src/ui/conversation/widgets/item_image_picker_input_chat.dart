import 'dart:io';
import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/photo_manager/photo_manager_bloc.dart';
import 'package:askany/src/configs/themes/app_colors.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';

class ItemImagePickerInputChat extends StatelessWidget {
  final File image;
  const ItemImagePickerInputChat(
      {

      // this.textOver,
      required this.image});
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          height: 58.sp,
          width: 58.sp,
          margin: EdgeInsets.only(
            right: 4.sp,
          ),
          child: Container(
            margin: EdgeInsets.only(
              right: 10.sp,
              top: 10.sp,
            ),
            decoration: BoxDecoration(
              border: Border.all(color: colorBlack4, width: .25),
              borderRadius: BorderRadius.circular(4.sp),
              image: DecorationImage(
                image: FileImage(image),
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
        Container(
          height: 58.sp,
          width: 58.sp,
          margin: EdgeInsets.only(right: 4.sp),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              TouchableOpacity(
                onTap: () {
                  AppBloc.photoManagerBloc
                      .add(TogglePhotoEvent(path: image.path));
                },
                child: Container(
                  width: 21.sp,
                  height: 21.sp,
                  decoration: BoxDecoration(
                    color: colorBlack.withOpacity(0.1),
                    border: Border.all(color: colorBlack4, width: .25),
                    borderRadius: BorderRadius.circular(6.sp),
                  ),
                  child: Center(
                    child: Image.asset(
                      iconRemove,
                      fit: BoxFit.fill,
                      width: 12.sp,
                      height: 12.sp,
                      color: colorBlack.withOpacity(0.3),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
