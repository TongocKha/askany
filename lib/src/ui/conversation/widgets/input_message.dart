import 'dart:io';

import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/chat/chat_bloc.dart';
import 'package:askany/src/bloc/message/message_bloc.dart';
import 'package:askany/src/bloc/photo_manager/photo_manager_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/helpers/input_formatter/message_formatter.dart';
import 'package:askany/src/models/message_model.dart';
import 'package:askany/src/services/socket/socket_emit.dart';
import 'package:askany/src/ui/conversation/widgets/button_header_conversation.dart';
import 'package:askany/src/ui/conversation/widgets/item_image_picker_input_chat.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class InputMessage extends StatefulWidget {
  final bool isChatInCall;
  final String conversationId;
  final String receiverId;
  final Function handleScrollWhenSendMessage;
  const InputMessage({
    required this.conversationId,
    required this.receiverId,
    required this.handleScrollWhenSendMessage,
    this.isChatInCall = false,
  });
  @override
  _InputMessageState createState() => _InputMessageState();
}

class _InputMessageState extends State<InputMessage> {
  TextEditingController msgController = TextEditingController();
  FocusNode focusNode = FocusNode();
  bool flagEdit = false;
  bool isTyping = false;
  // Timer? _debounce;

  @override
  void dispose() {
    // _debounce?.cancel();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    String? draftMessage =
        AppBloc.chatBloc.draftMessages[widget.conversationId];
    if (draftMessage != null && draftMessage.isNotEmpty) {
      msgController.text =
          AppBloc.chatBloc.draftMessages[widget.conversationId]!;
      _requestFocus();
      isTyping = true;
      SocketEmit().sendTyping(
        receiverId: widget.conversationId,
        conversationId: widget.conversationId,
      );
    }

    msgController.addListener(() {
      if (msgController.text.isEmpty) {
        if (isTyping) {
          SocketEmit().sendStopTyping(
            receiverId: widget.conversationId,
            conversationId: widget.conversationId,
          );
          isTyping = false;
        }
      } else {
        if (!isTyping) {
          SocketEmit().sendTyping(
            receiverId: widget.conversationId,
            conversationId: widget.conversationId,
          );
          isTyping = true;
        }
      }
    });
  }

  _handleSendMessage({MessageModel? chosenMessage}) {
    String message = msgController.text.trim();
    if (message.isNotEmpty ||
        AppBloc.photoManagerBloc.imagesChoosen.isNotEmpty) {
      AppBloc.chatBloc.add(
        UpdateDraftMessageEvent(
          data: '',
          conversationId: widget.conversationId,
        ),
      );
      if (chosenMessage != null) {
        AppBloc.messageBloc.add(
          SendEditMessageEvent(message: message),
        );
      } else {
        AppBloc.messageBloc.add(
          SendMessageEvent(message: message, files: null),
        );
        widget.handleScrollWhenSendMessage();
      }
      msgController.text = '';
    }
  }

  _requestFocus({bool isFocus = true}) {
    if (isFocus) {
      if (!focusNode.hasFocus) {
        focusNode.requestFocus();
      }
    } else {
      if (focusNode.hasFocus) {
        focusNode.unfocus();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MessageBloc, MessageState>(
      builder: (context, state) {
        MessageModel? chosenMessage = state.props[1] as MessageModel?;
        if (chosenMessage != null) {
          if (!flagEdit) {
            msgController.text = chosenMessage.data;
            flagEdit = true;
            _requestFocus();
          }
        } else {
          if (flagEdit) {
            flagEdit = false;
          }
        }

        return Container(
          decoration: null,
          margin: EdgeInsets.only(top: 4.sp, bottom: 18.sp),
          child: Column(
            children: <Widget>[
              BlocBuilder<PhotoManagerBloc, PhotoManagerState>(
                builder: (context, state) {
                  List<String> imagesChoosen = (state.props[1] as List).cast();
                  return Visibility(
                    visible: imagesChoosen.isNotEmpty,
                    child: Column(
                      children: [
                        Container(
                          height: 58.sp,
                          child: ListView.builder(
                            padding: EdgeInsets.symmetric(horizontal: 16.sp),
                            itemCount: imagesChoosen.length,
                            scrollDirection: Axis.horizontal,
                            itemBuilder: (context, index) {
                              return ItemImagePickerInputChat(
                                image: File(imagesChoosen[index]),
                              );
                            },
                          ),
                        ),
                        SizedBox(height: 8.sp),
                      ],
                    ),
                  );
                },
              ),
              Container(
                margin: EdgeInsets.symmetric(
                  horizontal: 12.sp,
                ),
                child: chatControls(chosenMessage: chosenMessage),
              ),
            ],
          ),
        );
      },
    );
  }

  Widget chatControls({MessageModel? chosenMessage}) {
    return Container(
      child: Row(
        children: <Widget>[
          SizedBox(width: 4.sp),
          ButtonHeaderConversation(
            iconAsset: iconPhotoChat,
            size: 22.sp,
            handlePressed: () =>
                AppBloc.photoManagerBloc.add(OnPhotoManagerEvent()),
          ),
          SizedBox(width: 4.sp),
          Expanded(
            child: TextFormField(
              focusNode: focusNode,
              controller: msgController,
              style: TextStyle(
                color: widget.isChatInCall ? Colors.white : colorTextChatCard,
                fontSize: 12.sp,
              ),
              keyboardType: TextInputType.multiline,
              inputFormatters: [
                MessageFormatter(),
              ],
              minLines: 1,
              maxLines: 2,
              onFieldSubmitted: (val) => _handleSendMessage(),
              decoration: InputDecoration(
                contentPadding: EdgeInsets.only(
                  left: 16.sp,
                  bottom: 3.sp,
                  top: 3.sp,
                  right: 10.sp,
                ),
                hintText: Strings.yourMessageHintext.i18n,
                hintStyle: TextStyle(
                  color:
                      widget.isChatInCall ? colorHintTextInCall : colorTimeChat,
                  fontSize: 12.sp,
                  fontWeight: FontWeight.w400,
                ),
                filled: true,
                fillColor: widget.isChatInCall
                    ? colorBackgroundReceiverInCall
                    : colorBackgroundSender,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(40.sp)),
                  borderSide: BorderSide.none,
                ),
              ),
              onChanged: (val) {
                if (!flagEdit) {
                  AppBloc.chatBloc.add(
                    UpdateDraftMessageEvent(
                      data: val.trim(),
                      conversationId: widget.conversationId,
                    ),
                  );
                }
                setState(() {
                  flagEdit = flagEdit;
                });
              },
            ),
          ),
          SizedBox(width: 6.sp),
          chosenMessage != null &&
                  (chosenMessage.data == msgController.text.trim() ||
                      msgController.text.isEmpty)
              ? IconButton(
                  onPressed: () {
                    msgController.text = '';
                    _requestFocus(isFocus: false);
                    AppBloc.messageBloc.add(CancelEditMessageEvent());
                  },
                  icon: Icon(
                    Icons.close,
                    color: Color(0xFFA4A4A4),
                  ),
                )
              : ButtonHeaderConversation(
                  iconAsset: iconSendMessage,
                  size: 18.5.sp,
                  handlePressed: () =>
                      _handleSendMessage(chosenMessage: chosenMessage),
                  color: msgController.text.isEmpty ? colorGreen3 : null,
                ),
        ],
      ),
    );
  }
}
