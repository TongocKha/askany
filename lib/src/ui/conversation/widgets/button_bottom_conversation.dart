import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class BottomOptionsSelectedConversation extends StatelessWidget {
  final String imageAsset;
  final String title;
  final Function handlePressed;
  final bool enable;

  BottomOptionsSelectedConversation({
    required this.imageAsset,
    required this.title,
    required this.handlePressed,
    required this.enable,
  });

  @override
  Widget build(BuildContext context) {
    return TouchableOpacity(
      onTap: enable ? handlePressed : null,
      child: Container(
        color: Colors.transparent,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset(
              imageAsset,
              width: 18.sp,
              height: 18.sp,
              color: enable ? colorBlack3 : colorBlack4,
            ),
            SizedBox(height: 6.sp),
            Text(
              title,
              style: TextStyle(
                color: enable ? colorBlack3 : colorBlack4,
                fontSize: 11.5.sp,
              ),
            )
          ],
        ),
      ),
    );
  }
}
