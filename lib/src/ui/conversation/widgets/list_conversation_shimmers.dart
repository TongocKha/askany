import 'package:askany/src/ui/common/widgets/shimmers/fade_simmer.dart';
import 'package:askany/src/ui/style/chat_style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'dart:math';

class ListConversationShimmers extends StatelessWidget {
  const ListConversationShimmers({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Random random = Random();
    bool isMe = random.nextBool();
    return ListView.builder(
      padding: EdgeInsets.only(top: 30.sp),
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      reverse: true,
      itemCount: 10,
      itemBuilder: (context, index) {
        int numberOfRow = random.nextInt(2) + 1;
        isMe = !isMe;
        return Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: isMe ? MainAxisAlignment.end : MainAxisAlignment.start,
          children: [
            SizedBox(width: 10.sp),
            !isMe
                ? FadeShimmer.round(
                    size: 40.sp,
                    fadeTheme: FadeTheme.lightReverse,
                  )
                : SizedBox(height: 20.sp, width: 20.sp),
            Container(
              margin: EdgeInsets.only(
                right: 12.sp,
                left: 8.sp,
                bottom: 16.sp,
              ),
              decoration: BoxDecoration(
                color: colorBackgroundSender,
                borderRadius: BorderRadius.all(Radius.circular(10.sp)),
              ),
              constraints: BoxConstraints(
                maxWidth: 65.w,
              ),
              padding: EdgeInsets.symmetric(horizontal: 16.sp, vertical: 12.sp),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  FadeShimmer(
                    width: 100.w,
                    height: 12.sp,
                    fadeTheme: FadeTheme.lightReverse,
                  ),
                  numberOfRow == 2 ? SizedBox(height: 8.sp) : Container(),
                  numberOfRow == 2
                      ? FadeShimmer(
                          width: 35.w,
                          height: 12.sp,
                          fadeTheme: FadeTheme.lightReverse,
                        )
                      : Container(),
                ],
              ),
            )
          ],
        );
      },
    );
  }
}
