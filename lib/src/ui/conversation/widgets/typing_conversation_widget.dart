import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class TypingConversationWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(4.sp),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(18.sp),
        color: colorBackgroundReceiver,
      ),
      child: Image.asset(
        imgTyping,
        width: 22.sp,
        height: 10.sp,
        color: colorGreen3,
        fit: BoxFit.fitWidth,
      ),
    );
  }
}
