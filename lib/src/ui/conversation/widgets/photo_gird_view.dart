import 'dart:io';
import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/photo_manager/photo_manager_bloc.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/conversation/widgets/camera_preview.dart';
import 'package:askany/src/ui/conversation/widgets/gridview_image_shimmer.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PhotosGridView extends StatefulWidget {
  final ScrollController scrollController;
  const PhotosGridView({required this.scrollController});
  @override
  State<StatefulWidget> createState() => _PhotosGridViewState();
}

class _PhotosGridViewState extends State<PhotosGridView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PhotoManagerBloc, PhotoManagerState>(
      builder: (context, state) {
        if (state is PhotoManagerInitial) {
          return GridViewImageShimmer();
        }

        List<File> images = (state.props[0] as List).cast();
        List<String> imagesChoosen = (state.props[1] as List).cast();
        bool isCameraGranted = state.props[2] as bool;
        return NotificationListener<DraggableScrollableNotification>(
          onNotification: (notification) {
            if (notification.extent > 0.5 &&
                AppBloc.photoManagerBloc.images.length <=
                    0.itemCountGridViewMoney * 3 &&
                !AppBloc.photoManagerBloc.isOver) {
              AppBloc.photoManagerBloc.add(GetPhotoManagerEvent());
            }
            return false;
          },
          child: GridView.builder(
            addRepaintBoundaries: false,
            physics: BouncingScrollPhysics(),
            controller: widget.scrollController,
            addAutomaticKeepAlives: true,
            gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 0.itemCountGridViewMoney,
              crossAxisSpacing: 2.sp,
              mainAxisSpacing: 2.sp,
            ),
            itemCount: isCameraGranted ? images.length + 1 : images.length,
            itemBuilder: (context, index) {
              int indexOfChoosen;
              int indexOfFile;
              if (isCameraGranted) {
                indexOfChoosen = ((index != 0))
                    ? imagesChoosen
                        .indexWhere((path) => path == images[index - 1].path)
                    : -1;
                indexOfFile = index - 1;
              } else {
                indexOfChoosen = ((index != 0))
                    ? imagesChoosen
                        .indexWhere((path) => path == images[index].path)
                    : -1;
                indexOfFile = index;
              }

              return (index == 0 && isCameraGranted)
                  ? CameraPreview(
                      isEnable: (imagesChoosen.length < 6),
                      handleFinish: (File? file) {
                        AppBloc.photoManagerBloc
                            .add(TogglePhotoEvent(path: file!.path));
                      },
                    )
                  : TouchableOpacity(
                      onTap: () {
                        AppBloc.photoManagerBloc.add(
                          TogglePhotoEvent(
                            // path: images[index - 1].path,
                            path: images[indexOfFile].path,
                          ),
                        );
                      },
                      child: Stack(
                        children: [
                          Container(
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                // image: FileImage(images[index - 1]),
                                image: FileImage(images[indexOfFile]),
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          Visibility(
                            visible: indexOfChoosen != -1,
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.black38,
                              ),
                              alignment: Alignment.center,
                              child: Container(
                                height: 26.sp,
                                width: 26.sp,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: colorGreen5,
                                ),
                                alignment: Alignment.center,
                                child: Text(
                                  '${indexOfChoosen + 1}',
                                  style: TextStyle(
                                    color: colorGreen2,
                                    fontSize: 10.sp,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
            },
          ),
        );
      },
    );
  }
}
