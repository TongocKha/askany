import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/message/message_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/account_style.dart';
import 'package:askany/src/ui/style/calendar_style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ConversationHeaderSelectedMessage extends StatelessWidget {
  final int quantity;

  ConversationHeaderSelectedMessage({
    required this.quantity,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100.w,
      height: 40.sp,
      padding: EdgeInsets.only(left: 4.sp, right: 12.sp),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          TouchableOpacity(
            onTap: () => AppBloc.messageBloc.add(SelectMessageEvent()),
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16.sp),
              color: Colors.transparent,
              child: Image.asset(
                iconRemove,
                width: 12.sp,
                color: colorGray1,
              ),
            ),
          ),
          SizedBox(width: 10.sp),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Text(
                    '$quantity ' +
                        (quantity >= 2
                            ? Strings.selectedMessages.i18n
                            : Strings.selectedMessage.i18n),
                    style: TextStyle(
                      color: colorBlack1,
                      fontSize: 13.sp,
                      fontWeight: FontWeight.w700,
                    ),
                    textAlign: TextAlign.start,
                  ),
                ),
                Expanded(
                  child: Row(
                    children: [
                      Icon(
                        Icons.message_outlined,
                        color: colorGray1,
                        size: 12.sp,
                      ),
                      SizedBox(
                        width: 5.sp,
                      ),
                      Text(
                        Strings.thisChatOnAskAny.i18n,
                        style: TextStyle(
                          color: colorGray1,
                          fontSize: 10.sp,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
