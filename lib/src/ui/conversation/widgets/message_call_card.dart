import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/helpers/date_time_helper.dart';
import 'package:askany/src/models/message_call_model.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class MessageCallCard extends StatefulWidget {
  final MessageCallModel messageCall;
  MessageCallCard({required this.messageCall});
  @override
  State<StatefulWidget> createState() => _MessageCardState();
}

class _MessageCardState extends State<MessageCallCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(12.sp, 16.sp, 10.sp, 16.sp),
      decoration: BoxDecoration(
        color: colorGreen5,
        borderRadius: BorderRadius.circular(10.sp),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(
                  widget.messageCall.type == MessageCallType.EndCall
                      ? Strings.thisCallIsEnd.i18n
                      : Strings.youMissedThisCall.i18n,
                  style: TextStyle(
                    color: colorTextChatCard,
                    fontSize: 12.5.sp,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                SizedBox(height: 6.sp),
                Text(
                  getDurationCallHistory(widget.messageCall.startTime,
                          widget.messageCall.endTime) +
                      ', ' +
                      DateFormat('dd/MM/yyyy')
                          .format(widget.messageCall.startTime) +
                      ' ${Strings.atTime.i18n} ${DateFormat('HH:mm').format(widget.messageCall.startTime)}',
                  style: TextStyle(
                    color: colorTextChatCard,
                    fontSize: 10.sp,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(width: 8.sp),
          Container(
            height: 32.sp,
            width: 32.sp,
            decoration: BoxDecoration(
              color: colorGreen3,
              shape: BoxShape.circle,
            ),
            alignment: Alignment.center,
            child: Image.asset(
              iconPhoneMessage,
              width: 16.sp,
            ),
          ),
        ],
      ),
    );
  }
}
