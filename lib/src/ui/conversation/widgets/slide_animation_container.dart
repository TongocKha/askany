import 'package:flutter/material.dart';

class CustomSlideAnimationWidget extends StatefulWidget {
  final bool isMyMessage;
  final Widget child;
  final AnimationController? animationController;
  const CustomSlideAnimationWidget({
    Key? key,
    required this.child,
    required this.animationController,
    required this.isMyMessage,
  }) : super(key: key);

  @override
  State<CustomSlideAnimationWidget> createState() =>
      _CustomSlideAnimationWidgetState();
}

class _CustomSlideAnimationWidgetState extends State<CustomSlideAnimationWidget>
    with TickerProviderStateMixin {
  late Animation<Offset> _offsetAnimation;
  @override
  void initState() {
    super.initState();
    _offsetAnimation = Tween<Offset>(
      begin: Offset.zero,
      end: widget.isMyMessage ? Offset(-0.1, 0) : Offset(0.1, 0),
    ).animate(
      CurvedAnimation(
        parent: widget.animationController ?? AnimationController(vsync: this),
        curve: Curves.easeInOut,
      ),
    );
  }

  @override
  void deactivate() {
    super.deactivate();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.animationController == null) {
      return widget.child;
    } else {
      return SlideTransition(
        position: _offsetAnimation,
        child: widget.child,
      );
    }
  }
}
