import 'package:askany/src/models/conversation_model.dart';
import 'package:askany/src/models/message_model.dart';
import 'package:askany/src/ui/common/widgets/cake_avatar/cake_avatar.dart';
import 'package:askany/src/ui/conversation/widgets/typing_conversation_widget.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class SearchMessageCard extends StatefulWidget {
  final ConversationModel conversationModel;
  final MessageModel messageModel;
  final bool isTyping;
  SearchMessageCard({
    required this.isTyping,
    required this.conversationModel,
    required this.messageModel,
  });
  @override
  State<StatefulWidget> createState() => _SearchMessageCardState();
}

class _SearchMessageCardState extends State<SearchMessageCard> {
  bool get _isSeenLatestMessage =>
      !(widget.conversationModel.latestMessage.isSeen);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16.sp),
      color: Theme.of(context).scaffoldBackgroundColor,
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(vertical: 12.0),
            child: Row(
              children: [
                Expanded(
                  child: Row(
                    children: [
                      CakeAvatar(
                        urls: widget.conversationModel.avatarConversations,
                        size: 42.sp,
                      ),
                      SizedBox(width: 12.0),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              widget.conversationModel.conversationTitle,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1,
                              style: TextStyle(
                                fontSize: 12.sp,
                                fontWeight: _isSeenLatestMessage
                                    ? FontWeight.w700
                                    : FontWeight.w600,
                                color: _isSeenLatestMessage
                                    ? colorBlack2
                                    : colorTextChatCard,
                              ),
                            ),
                            SizedBox(height: 3.5.sp),
                            widget.isTyping
                                ? TypingConversationWidget()
                                : Text(
                                    widget.messageModel.data,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                    style: TextStyle(
                                      fontSize: 11.sp,
                                      color: _isSeenLatestMessage
                                          ? colorBlack2
                                          : colorTextChatCard,
                                      fontWeight: _isSeenLatestMessage
                                          ? FontWeight.w700
                                          : FontWeight.w400,
                                    ),
                                  ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(width: 10.sp),
                Text(
                  DateFormat('hh:mm a').format(widget.messageModel.createdAt),
                  style: TextStyle(
                    color: colorTimeChat,
                    fontSize: 9.sp,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  // String get _latestMessageData {
  //   if (widget.conversationModel.latestMessage.data.isEmpty) {
  //     return (widget.conversationModel.latestMessage.isMe
  //             ? 'Bạn'
  //             : '${widget.conversationModel.receiverUser?.fullname ?? ''}') +
  //         ' đã gửi hình ảnh.';
  //   }

  //   return widget.conversationModel.latestMessage.data;
  // }
}
