import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:askany/src/helpers/url_launcher_helper.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_chat_types/flutter_chat_types.dart' show PreviewData;
import 'package:flutter_link_previewer/flutter_link_previewer.dart';

class LinkPreviewCard extends StatefulWidget {
  final String url;
  const LinkPreviewCard({
    Key? key,
    required this.url,
  }) : super(key: key);

  @override
  State<LinkPreviewCard> createState() => _LinkPreviewCardState();
}

class _LinkPreviewCardState extends State<LinkPreviewCard> {
  PreviewData? _previewData;
  @override
  Widget build(BuildContext context) {
    return TouchableOpacity(
      onTap: () {
        UrlLauncherHelper.launchUrl(widget.url);
      },
      child: Align(
        alignment: Alignment.centerLeft,
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 5.sp),
          key: ValueKey(widget.url),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(
              Radius.circular(20),
            ),
            color: Color(0xfff7f7f8),
          ),
          child: ClipRRect(
            borderRadius: const BorderRadius.all(
              Radius.circular(20),
            ),
            child: LinkPreview(
              onLinkPressed: (url) {},
              enableAnimation: true,
              onPreviewDataFetched: (data) {
                setState(() {
                  _previewData = data;
                });
              },
              previewData: _previewData,
              text: widget.url,
              width: 250.sp,
            ),
          ),
        ),
      ),
    );
  }
}
