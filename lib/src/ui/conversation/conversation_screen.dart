import 'package:askany/src/bloc/photo_manager/photo_manager_bloc.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/chat/chat_bloc.dart';
import 'package:askany/src/bloc/message/message_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/helpers/date_time_helper.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:askany/src/models/conversation_model.dart';
import 'package:askany/src/models/message_model.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_none.dart';
import 'package:askany/src/ui/common/widgets/bottom_sheet/bottom_delete.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/conversation/widgets/button_bottom_conversation.dart';
import 'package:askany/src/ui/conversation/widgets/button_scroll_down.dart';
import 'package:askany/src/ui/conversation/widgets/conversation_header.dart';
import 'package:askany/src/ui/conversation/widgets/conversation_header_selected_message.dart';
import 'package:askany/src/ui/conversation/widgets/input_message.dart';
import 'package:askany/src/ui/conversation/widgets/list_conversation_shimmers.dart';
import 'package:askany/src/ui/conversation/widgets/message_card.dart';
import 'package:askany/src/ui/conversation/widgets/typing_widget.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

class ImageDiviceProvider extends StatelessWidget {
  final ConversationModel conversationModel;

  const ImageDiviceProvider({
    Key? key,
    required this.conversationModel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ConversationScreen(
      conversationModel: conversationModel,
    );
  }
}

class ConversationScreen extends StatefulWidget {
  final ConversationModel conversationModel;
  ConversationScreen({required this.conversationModel});
  @override
  State<StatefulWidget> createState() => _ConversationScreenState();
}

class _ConversationScreenState extends State<ConversationScreen> with TickerProviderStateMixin {
  late final AnimationController _controller = AnimationController(
    duration: const Duration(milliseconds: DURATION_DEFAULT_ANIMATION * 2),
    vsync: this,
  )..repeat(reverse: true);

  final ItemScrollController itemScrollController = ItemScrollController();
  final ItemPositionsListener itemPositionsListener = ItemPositionsListener.create();
  // ScrollController _scrollController = ScrollController();
  bool _flagButtonScrollDown = false;

  @override
  void initState() {
    super.initState();
    AppBloc.messageBloc.add(
      OnMessageEvent(
        conversationId: widget.conversationModel.id,
        isSeen: widget.conversationModel.latestMessage.isSeen,
      ),
    );

    itemPositionsListener.itemPositions.addListener(
      () {
        List<ItemPosition> itemPositions = itemPositionsListener.itemPositions.value.toList();
        List<MessageModel> messages = AppBloc.messageBloc.messagesMap[widget.conversationModel.id]?[MESSAGE_FIELD] ?? [];
        if (itemPositions.last.index == messages.length - 2) {
          AppBloc.messageBloc.add(GetMessageEvent(conversationId: widget.conversationModel.id));
        }

        if (itemPositions.first.index > 4) {
          if (!_flagButtonScrollDown) {
            setState(() {
              _flagButtonScrollDown = true;
            });
          }
        } else {
          if (_flagButtonScrollDown) {
            setState(() {
              _flagButtonScrollDown = false;
            });
          }
        }
      },
    );
  }

  @override
  void dispose() {
    AppBloc.messageBloc.add(DisposeMessageEvent());
    AppBloc.photoManagerBloc.add(DisposePhotoEvent());
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: appBarBrighnessDark(brightness: Theme.of(context).brightness),
      body: SafeArea(
        bottom: false,
        child: Container(
          child: Column(
            children: [
              BlocBuilder<MessageBloc, MessageState>(
                builder: (context, state) {
                  if (state is MessageInitial) {
                    return ConversationHeader(
                      conversationModel: widget.conversationModel,
                      itemScrollController: itemScrollController,
                    );
                  }
                  List<MessageModel>? selectedMessages = state.props[3];

                  return selectedMessages == null
                      ? ConversationHeader(
                          conversationModel: widget.conversationModel,
                          itemScrollController: itemScrollController,
                        )
                      : ConversationHeaderSelectedMessage(
                          quantity: selectedMessages.length,
                        );
                },
              ),
              SizedBox(height: 4.sp),
              dividerChat,
              SizedBox(height: 4.sp),
              Expanded(
                child: BlocBuilder<MessageBloc, MessageState>(
                  builder: (context, state) {
                    if (state is MessageInitial) {
                      return ListConversationShimmers();
                    }

                    List<MessageModel> messages = state.props[0];
                    List<MessageModel> newMessages = state.props[2];

                    return Stack(
                      children: [
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: ScrollablePositionedList.builder(
                            // controller: _scrollController,
                            itemScrollController: itemScrollController,
                            itemPositionsListener: itemPositionsListener,
                            padding: EdgeInsets.only(top: 30.sp),
                            physics: BouncingScrollPhysics(),
                            shrinkWrap: true,
                            reverse: true,
                            itemCount: messages.length + 1,
                            itemBuilder: (context, index) {
                              int formatIndex = index - 1;
                              return index == 0
                                  ? BlocBuilder<ChatBloc, ChatState>(
                                      builder: (context, state) {
                                        List<String> isTypings = (state.props[1] as List).cast();

                                        return Visibility(
                                          visible: isTypings.contains(widget.conversationModel.id),
                                          child: TypingWidget(
                                            conversationModel: widget.conversationModel,
                                          ),
                                        );
                                      },
                                    )
                                  : MessageCard(
                                      animationController: _controller,
                                      controller: itemScrollController,
                                      messageModel: messages[formatIndex],
                                      conversationModel: widget.conversationModel,
                                      isShowAvatar: formatIndex == messages.length - 1 ||
                                          (!messages[formatIndex].isMe && (messages[formatIndex + 1].isMe || isShowTime(messages[formatIndex].createdAt, (formatIndex < messages.length - 1 ? messages[formatIndex + 1] : null)!.createdAt))),
                                      prevModel: formatIndex < messages.length - 1 ? messages[formatIndex + 1] : null,
                                      nextModel: formatIndex > 0 ? messages[formatIndex - 1] : null,
                                    );
                            },
                          ),
                        ),
                        Visibility(
                          visible: _flagButtonScrollDown,
                          child: Positioned(
                            bottom: 12.sp,
                            left: 0,
                            right: 0,
                            child: Align(
                              alignment: Alignment.center,
                              child: TouchableOpacity(
                                onTap: () => _scrollDown(),
                                child: ButtonScrollDown(
                                  message: newMessages.isEmpty ? null : newMessages.first,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    );
                  },
                ),
              ),
              BlocBuilder<MessageBloc, MessageState>(
                builder: (context, state) {
                  if (state is MessageInitial) {
                    return InputMessage(
                      receiverId: widget.conversationModel.receiverUser?.id ?? '',
                      conversationId: widget.conversationModel.id,
                      handleScrollWhenSendMessage: () {
                        _scrollDown();
                      },
                    );
                  }
                  List<MessageModel>? selectedMessages = state.props[3];
                  MessageModel? parentMessage = state.props[5] as MessageModel?;

                  return selectedMessages == null
                      ? Column(
                          children: [
                            dividerChat,
                            Visibility(
                              visible: parentMessage != null,
                              child: Container(
                                padding: EdgeInsets.symmetric(horizontal: 16.sp),
                                margin: EdgeInsets.only(top: 8.sp),
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'Đang trả lời ${widget.conversationModel.userSender(parentMessage?.userSender ?? '')?.fullname}:',
                                            style: TextStyle(
                                              fontSize: 10.sp,
                                              color: colorBlack2,
                                            ),
                                          ),
                                          SizedBox(height: 2.sp),
                                          Text(
                                            '“${parentMessage?.data}”',
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                              fontStyle: FontStyle.italic,
                                              fontSize: 10.sp,
                                              color: colorGray2,
                                            ),
                                          ),
                                          Text(
                                            '――――――',
                                            style: TextStyle(
                                              fontStyle: FontStyle.italic,
                                              fontSize: 10.sp,
                                              color: colorGray4,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    TouchableOpacity(
                                      onTap: () {
                                        AppBloc.messageBloc.add(
                                          QuoteMessageEvent(message: null),
                                        );
                                      },
                                      child: Align(
                                        alignment: Alignment.centerRight,
                                        child: Container(
                                          decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: colorGreen5,
                                          ),
                                          padding: EdgeInsets.all(8.sp),
                                          child: Image.asset(
                                            iconRemove,
                                            width: 9.sp,
                                            height: 9.sp,
                                            color: colorFontGreen,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            InputMessage(
                              receiverId: widget.conversationModel.receiverUser?.id ?? '',
                              conversationId: widget.conversationModel.id,
                              handleScrollWhenSendMessage: () {
                                _scrollDown();
                              },
                            ),
                          ],
                        )
                      : Container(
                          width: 100.w,
                          padding: EdgeInsets.symmetric(vertical: 16.sp),
                          child: Row(
                            children: [
                              Expanded(
                                child: BottomOptionsSelectedConversation(
                                  enable: selectedMessages.isNotEmpty,
                                  imageAsset: iconCopy,
                                  title: Strings.copy.i18n,
                                  handlePressed: () => AppBloc.messageBloc.add(CopyMessageEvent()),
                                ),
                              ),
                              Expanded(
                                child: BottomOptionsSelectedConversation(
                                  enable: selectedMessages.length == 1 && selectedMessages.first.isMe,
                                  imageAsset: iconDelete,
                                  title: Strings.delete.i18n,
                                  handlePressed: () {
                                    showModalBottomSheet(
                                      context: context,
                                      backgroundColor: Colors.transparent,
                                      isDismissible: true,
                                      isScrollControlled: true,
                                      barrierColor: Colors.black38,
                                      builder: (context) {
                                        return BottomDelete(
                                          text: Strings.areYourSureToDeleteThisMessage.i18n,
                                          handlePressed: () {
                                            AppBloc.messageBloc.add(
                                              DeleteMessageEvent(message: selectedMessages.first),
                                            );
                                          },
                                        );
                                      },
                                    );
                                  },
                                ),
                              ),
                            ],
                          ),
                        );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _scrollDown() {
    itemScrollController.scrollTo(
      index: 0,
      duration: Duration(milliseconds: DURATION_DEFAULT_ANIMATION),
    );
  }
}
