import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_message.dart';
import 'package:askany/src/ui/home/widget/search_home.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';

class HomeSmallHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: headerCalendarColor,
      alignment: Alignment.bottomCenter,
      padding: EdgeInsets.only(bottom: 10.sp, top: 4.sp),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
            child: SearchHome(),
          ),
          SizedBox(width: 6.sp),
          ButtonMessage(
            iconHeader: iconMessageHeader,
            padding: EdgeInsets.only(right: 16.sp),
          ),
        ],
      ),
    );
  }
}
