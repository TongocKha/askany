import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class HomeHeader extends StatelessWidget {
  const HomeHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 140.sp,
      color: headerCalendarColor,
      padding: EdgeInsets.fromLTRB(20.sp, 10.sp, 20.sp, 0.sp),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.end,
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(height: 8.sp),
              Text(
                Strings.plentifulAndProfessionalExpertCommunity.i18n,
                style: TextStyle(
                  color: colorWhiteCard,
                  fontWeight: FontWeight.w700,
                  fontSize: 13.sp,
                  height: 1.15.sp,
                ),
              ),
              SizedBox(height: 6.sp),
              Text(
                Strings.solutionForAllSector.i18n,
                style: TextStyle(
                  color: colorWhiteCard,
                  fontSize: 11.sp,
                ),
              ),
            ],
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.only(left: 3.sp, top: 5.sp),
              alignment: Alignment.bottomRight,
              color: Colors.transparent,
              child: Image.asset(
                imageAdmin,
                height: 100.sp,
                width: 100.sp,
                fit: BoxFit.fitWidth,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
