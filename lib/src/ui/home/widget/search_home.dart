import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class SearchHome extends StatefulWidget {
  SearchHome({Key? key}) : super(key: key);

  @override
  State<SearchHome> createState() => _SearchHomeState();
}

class _SearchHomeState extends State<SearchHome> {
  @override
  Widget build(BuildContext context) {
    return TouchableOpacity(
      onTap: () {
        if (UserLocal().getAccessToken() == '') {
          AppNavigator.push(Routes.CHOOSE_ACCOUNT);
        } else {
          AppNavigator.push(Routes.SEARCH);
        }
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 16.sp),
        margin: EdgeInsets.symmetric(horizontal: 16.sp),
        alignment: Alignment.center,
        height: 35.sp,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8),
          border: Border.all(color: colorGrayBorder, width: 0.5.sp),
        ),
        child: Row(
          children: <Widget>[
            Container(
              margin: EdgeInsets.symmetric(horizontal: 2.sp),
              alignment: Alignment.center,
              child: Image.asset(
                iconSearchHome,
                fit: BoxFit.cover,
                height: 14.sp,
                width: 14.sp,
              ),
            ),
            SizedBox(
              width: 8.sp,
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.only(top: 1.sp),
                child: Text(
                  Strings.findSomething.i18n,
                  style: TextStyle(
                    color: colorGray2,
                    fontSize: 12.5.sp,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
