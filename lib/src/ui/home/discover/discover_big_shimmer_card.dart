import 'package:askany/src/ui/common/widgets/shimmers/fade_simmer.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class DiscoverBigShimmerCard extends StatelessWidget {
  const DiscoverBigShimmerCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 36.sp),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          FadeShimmer(
            radius: 12.sp,
            width: 280.sp,
            height: 180.sp,
            fadeTheme: FadeTheme.lightReverse,
          ),
          Container(
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.vertical(
                bottom: Radius.circular(10.sp),
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 10.sp),
                FadeShimmer(
                  height: 16.sp,
                  width: 100.w,
                  fadeTheme: FadeTheme.lightReverse,
                ),
                SizedBox(height: 8.sp),
                FadeShimmer(
                  height: 16.sp,
                  width: 130.sp,
                  fadeTheme: FadeTheme.lightReverse,
                ),
                SizedBox(height: 10.sp),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      alignment: Alignment.centerLeft,
                      child: FadeShimmer(
                        height: 9.sp,
                        width: 100.sp,
                        fadeTheme: FadeTheme.lightReverse,
                      ),
                    ),
                    Container(
                      child: FadeShimmer(
                        height: 9.sp,
                        width: 70.sp,
                        fadeTheme: FadeTheme.lightReverse,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
