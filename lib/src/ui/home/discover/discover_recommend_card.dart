import 'package:askany/src/helpers/date_time_helper.dart';
import 'package:askany/src/models/discover_model.dart';
import 'package:askany/src/routes/app_navigator_observer.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/service_image/cached_image_custom.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class DiscoverRecommendCard extends StatelessWidget {
  final DiscoverConvertedModel discoverModel;
  const DiscoverRecommendCard({required this.discoverModel});
  @override
  Widget build(BuildContext context) {
    return TouchableOpacity(
      onTap: () {
        if (AppNavigatorObserver.routeNames
                .map((routeName) => routeName == Routes.DETAIL_DISCOVER)
                .toList()
                .length >
            2) {
          AppNavigator.replaceWith(Routes.DETAIL_DISCOVER, arguments: {
            'discoverModel': discoverModel,
          });
        } else {
          AppNavigator.push(Routes.DETAIL_DISCOVER, arguments: {
            'discoverModel': discoverModel,
          });
        }
      },
      child: Container(
        margin: EdgeInsets.only(bottom: 36.sp),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            CachedImageCustom(
              urlToImage: discoverModel.image,
              height: (100.w - 32.sp) / (16 / 9),
              width: 100.w - 32.sp,
              radius: BorderRadius.circular(12.sp),
              alignment: Alignment.center,
              padding: EdgeInsets.only(bottom: 10.sp, right: 10.sp),
              childCache: _timeDiscover(),
            ),
            Container(
              alignment: Alignment.centerLeft,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.vertical(
                  bottom: Radius.circular(10.sp),
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 10.sp),
                  Text(
                    discoverModel.descriptions,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontSize: 15.sp,
                      fontWeight: FontWeight.w700,
                      color: colorBlack2,
                    ),
                  ),
                  SizedBox(height: 6.sp),
                  Text(
                    getDateString(discoverModel.date.toLocal()),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontSize: 10.sp,
                      color: colorGray1,
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _timeDiscover() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        _buttonPlay(),
        // Spacer(),
        // Container(
        //   padding: EdgeInsets.symmetric(horizontal: 8.sp, vertical: 3.sp),
        //   decoration: BoxDecoration(
        //     color: Colors.black.withOpacity(0.5.sp),
        //     borderRadius: BorderRadius.all(
        //       Radius.circular(5.sp),
        //     ),
        //   ),
        //   child: Text(
        //     getDurationCalling(durationSeconds: discoverModel.duration, isShowHours: false),
        //     style: TextStyle(
        //       fontSize: 9.sp,
        //       fontWeight: FontWeight.w600,
        //       color: colorWhiteCard,
        //     ),
        //   ),
        // ),
      ],
    );
  }

  Widget _buttonPlay() {
    return Image.asset(
      iconPlay,
      width: 50.sp,
      height: 50.sp,
    );
  }
}
