import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

import 'package:askany/src/models/discover_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/service_image/cached_image_custom.dart';
import 'package:askany/src/ui/style/style.dart';

class DiscoverCard extends StatelessWidget {
  final DiscoverConvertedModel discoverModel;
  const DiscoverCard({
    Key? key,
    required this.discoverModel,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return TouchableOpacity(
      onTap: () {
        AppNavigator.push(
          Routes.DETAIL_DISCOVER,
          arguments: {
            'discoverModel': discoverModel,
          },
        );
      },
      child: Container(
        width: 150.sp,
        margin: EdgeInsets.only(right: 10.sp, bottom: 5.sp),
        child: Column(
          children: [
            CachedImageCustom(
              urlToImage: discoverModel.image,
              height: 100.sp,
              width: 150.sp,
              radius: BorderRadius.circular(12.sp),
              childCache: _buttonPlay(),
            ),
            Expanded(
              child: Container(
                alignment: Alignment.centerLeft,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.vertical(
                    bottom: Radius.circular(10.sp),
                  ),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 10.sp),
                    Text(
                      discoverModel.descriptions,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontSize: 12.sp,
                        fontWeight: FontWeight.w600,
                        color: colorBlack2,
                      ),
                    ),
                    SizedBox(height: 6.sp),
                    Text(
                      discoverModel.content.fullname,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontSize: 9.sp,
                        color: colorGray1,
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buttonPlay() {
    return Center(
      child: Image.asset(
        iconPlay,
        width: 40.sp,
        height: 40.sp,
      ),
    );
  }
}
