import 'package:askany/src/ui/common/widgets/shimmers/fade_simmer.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class DiscoverShimmerCard extends StatelessWidget {
  const DiscoverShimmerCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 150.sp,
      margin: EdgeInsets.only(right: 10.sp, bottom: 5.sp),
      child: Column(
        children: [
          FadeShimmer(
            radius: 12.sp,
            width: 150.sp,
            height: 100.sp,
            fadeTheme: FadeTheme.lightReverse,
          ),
          Expanded(
            child: Container(
              alignment: Alignment.centerLeft,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.vertical(
                  bottom: Radius.circular(10.sp),
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 12.sp),
                  FadeShimmer(
                    height: 14.sp,
                    width: 150.sp,
                    fadeTheme: FadeTheme.lightReverse,
                  ),
                  SizedBox(height: 4.sp),
                  FadeShimmer(
                    height: 14.sp,
                    width: 60.sp,
                    fadeTheme: FadeTheme.lightReverse,
                  ),
                  SizedBox(height: 6.sp),
                  FadeShimmer(
                    height: 12.sp,
                    width: 100.sp,
                    fadeTheme: FadeTheme.lightReverse,
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
