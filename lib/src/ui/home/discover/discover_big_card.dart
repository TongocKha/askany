import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

import 'package:askany/src/helpers/date_time_helper.dart';
import 'package:askany/src/models/discover_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/service_image/cached_image_custom.dart';
import 'package:askany/src/ui/style/style.dart';

class DiscoverBigCard extends StatelessWidget {
  final DiscoverConvertedModel discoverModel;
  const DiscoverBigCard({
    Key? key,
    required this.discoverModel,
  }) : super(key: key);

  Widget _buttonPlay() {
    return Image.asset(
      iconPlay,
      width: 50.sp,
      height: 50.sp,
    );
  }

  @override
  Widget build(BuildContext context) {
    return TouchableOpacity(
      onTap: () {
        AppNavigator.push(Routes.DETAIL_DISCOVER, arguments: {
          'discoverModel': discoverModel,
        });
      },
      child: Container(
        margin: EdgeInsets.only(bottom: 36.sp),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            CachedImageCustom(
              urlToImage: discoverModel.image,
              height: (100.w - 32.sp) / (16 / 9),
              width: 100.w - 32.sp,
              radius: BorderRadius.circular(12.sp),
              childCache: _buttonPlay(),
              alignment: Alignment.center,
            ),
            Container(
              alignment: Alignment.centerLeft,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.vertical(
                  bottom: Radius.circular(10.sp),
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 10.sp),
                  Text(
                    discoverModel.descriptions,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontSize: 14.sp,
                      fontWeight: FontWeight.w600,
                      color: colorBlack2,
                    ),
                  ),
                  SizedBox(height: 6.sp),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        discoverModel.content.fullname,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontSize: 10.sp,
                          color: colorGray1,
                        ),
                      ),
                      Text(
                        getDateString(discoverModel.date.toLocal()),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontSize: 10.sp,
                          color: colorGray1,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
