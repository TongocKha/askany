import 'package:askany/src/bloc/discover/discover_bloc.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

import 'package:askany/src/ui/common/widgets/pagination_list_view.dart';
import 'package:askany/src/ui/home/discover/discover_big_card.dart';
import 'package:askany/src/ui/home/discover/discover_big_shimmer_card.dart';

class DiscoverBigList extends StatelessWidget {
  const DiscoverBigList({
    Key? key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DiscoverBloc, DiscoverState>(
      builder: (context, state) {
        if (state is GetDoneDiscoverItems) {
          return PaginationListView(
            itemBuilder: (context, index) {
              return DiscoverBigCard(
                discoverModel: state.listItem[index],
              );
            },
            childShimmer: DiscoverBigShimmerCard(),
            itemCount: state.listItem.length,
            padding: EdgeInsets.symmetric(horizontal: 16.sp),
          );
        }
        return PaginationListView(
          itemBuilder: (context, index) {
            return DiscoverBigShimmerCard();
          },
          childShimmer: DiscoverBigShimmerCard(),
          itemCount: ITEM_COUNT_SHIMMER,
          padding: EdgeInsets.symmetric(horizontal: 16.sp),
        );
      },
    );
  }
}
