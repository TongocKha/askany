import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/discover/discover_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/pagination_list_view.dart';
import 'package:askany/src/ui/common/widgets/text_ui/title_and_seemore.dart';
import 'package:askany/src/ui/home/discover/discover_card.dart';
import 'package:askany/src/ui/home/discover/discover_shimmer_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class DiscoverList extends StatefulWidget {
  @override
  _DiscoverListState createState() => _DiscoverListState();
}

class _DiscoverListState extends State<DiscoverList> {
  @override
  void initState() {
    super.initState();
    AppBloc.discoverBloc.add(OnDiscoverEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.only(left: 16.sp, right: 10.sp),
            child: TitleAndSeeMore(
              title: Strings.discover.i18n,
              handlePressed: () async {
                await AppNavigator.push(
                  Routes.DISCOVER,
                );
              },
            ),
          ),
          SizedBox(height: 18.sp),
          BlocBuilder<DiscoverBloc, DiscoverState>(
            builder: (context, state) {
              if (state is GetDoneDiscoverItems) {
                final listItem = state.listItem;
                return Container(
                  height: 175.sp,
                  child: PaginationListView(
                    physics: BouncingScrollPhysics(),
                    padding: EdgeInsets.symmetric(horizontal: 16.sp),
                    scrollDirection: Axis.horizontal,
                    childShimmer: Container(
                      height: 175.sp,
                      child: ListView.builder(
                        padding: EdgeInsets.symmetric(horizontal: 16.sp),
                        scrollDirection: Axis.horizontal,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: ITEM_COUNT_SHIMMER,
                        itemBuilder: (context, index) {
                          return DiscoverShimmerCard();
                        },
                      ),
                    ),
                    itemCount: listItem.length,
                    itemBuilder: (context, index) {
                      return DiscoverCard(discoverModel: listItem[index]);
                    },
                  ),
                );
              }
              return Container(
                height: 175.sp,
                child: ListView.builder(
                  padding: EdgeInsets.symmetric(horizontal: 16.sp),
                  scrollDirection: Axis.horizontal,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: ITEM_COUNT_SHIMMER,
                  itemBuilder: (context, index) {
                    return DiscoverShimmerCard();
                  },
                ),
              );
            },
          )
        ],
      ),
    );
  }
}
