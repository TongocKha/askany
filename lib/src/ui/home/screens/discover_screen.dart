import 'package:askany/src/configs/lang/localization.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/home/discover/discover_big_list.dart';
import 'package:askany/src/ui/style/calendar_style.dart';

class DiscoverScreen extends StatefulWidget {
  DiscoverScreen({
    Key? key,
  }) : super(key: key);

  @override
  State<DiscoverScreen> createState() => _DiscoverScreenState();
}

class _DiscoverScreenState extends State<DiscoverScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: appBarTitleBack(
        context,
        'Khám phá',
      ),
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 16.sp),
              child: Text(
                Strings
                    .findAnExpertInEverySectorsWhoCanResolveYourProblems.i18n,
                style: TextStyle(
                  fontSize: 13.sp,
                  color: colorGray1,
                  height: 1.3.sp,
                ),
              ),
            ),
            SizedBox(height: 18.sp),
            Expanded(
              child: DiscoverBigList(),
            )
          ],
        ),
      ),
    );
  }
}
