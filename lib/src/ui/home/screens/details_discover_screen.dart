import 'package:askany/src/bloc/discover/discover_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/helpers/date_time_helper.dart';
import 'package:askany/src/ui/home/discover/discover_big_shimmer_card.dart';
import 'package:askany/src/ui/home/discover/discover_recommend_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/video_player/video_player_bloc.dart';
import 'package:askany/src/models/discover_model.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

class DetailsDiscoverScreen extends StatefulWidget {
  final DiscoverConvertedModel discoverModel;
  const DetailsDiscoverScreen({
    Key? key,
    required this.discoverModel,
  }) : super(key: key);

  @override
  _DetailsDiscoverScreenState createState() => _DetailsDiscoverScreenState();
}

class _DetailsDiscoverScreenState extends State<DetailsDiscoverScreen> {
  @override
  void initState() {
    super.initState();
    AppBloc.discoverBloc.add(OnDiscoverEvent());

    AppBloc.videoPlayerBloc
      ..add(
        PlayVideoEvent(
          urlToVideo: widget.discoverModel.title,
        ),
      );
  }

  @override
  void dispose() {
    AppBloc.videoPlayerBloc.add(DisposeVideoEvent());
    super.dispose();
  }

  _youtubePlayer(YoutubePlayerController controller) {
    return AspectRatio(
      aspectRatio: controller.value.isFullScreen ? MediaQuery.of(context).size.aspectRatio : 16 / 9,
      child: YoutubePlayerControllerProvider(
        controller: controller,
        child: YoutubePlayerIFrame(
          aspectRatio: 16 / 9,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<VideoPlayerBloc, VideoPlayerState>(
      buildWhen: (previous, current) => previous != current,
      builder: (context, state) {
        return Scaffold(
          resizeToAvoidBottomInset: false,
          extendBodyBehindAppBar: state is VideoPlaying
              ? state.videos[widget.discoverModel.title]?.value.isFullScreen ?? false
              : false,
          appBar: (state is VideoPlaying
                  ? state.videos[widget.discoverModel.title]?.value.isFullScreen ?? false
                  : false)
              ? null
              : appBarTitleBack(
                  context,
                  '',
                ),
          body: Container(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  _detailsHeader(state),
                  Visibility(
                    visible: !(state is VideoPlaying
                        ? state.videos[widget.discoverModel.title]?.value.isFullScreen ?? false
                        : false),
                    child: BlocBuilder<DiscoverBloc, DiscoverState>(
                      builder: (context, state) {
                        if (state is GetDoneDiscoverItems) {
                          final List<DiscoverConvertedModel> listItems = [];
                          listItems.addAll(state.listItem);
                          final int headderIndex = state.listItem
                              .indexWhere((item) => item.title == widget.discoverModel.title);
                          if (headderIndex != -1) {
                            listItems.removeAt(headderIndex);
                          }
                          return ListView.builder(
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: listItems.length,
                            itemBuilder: (context, index) {
                              return Padding(
                                padding: EdgeInsets.symmetric(horizontal: 16.sp),
                                child: DiscoverRecommendCard(
                                  discoverModel: listItems[index],
                                ),
                              );
                            },
                          );
                        }
                        return ListView.builder(
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: ITEM_COUNT_SHIMMER,
                          itemBuilder: (context, index) {
                            return Padding(
                              padding: EdgeInsets.symmetric(horizontal: 16.sp),
                              child: DiscoverBigShimmerCard(),
                            );
                          },
                        );
                      },
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _detailsHeader(VideoPlayerState state) {
    return Column(
      children: [
        _discoverCard(state),
        Visibility(
          visible: !(state is VideoPlaying
              ? state.videos[widget.discoverModel.title]?.value.isFullScreen ?? false
              : false),
          child: Column(
            children: [
              SizedBox(height: 16.sp),
              dividerThinkness6NotMargin,
              SizedBox(height: 16.sp),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 16.sp),
                alignment: Alignment.centerLeft,
                child: Text(
                  Strings.seeMore.i18n,
                  style:
                      TextStyle(color: colorBlack1, fontSize: 18.sp, fontWeight: FontWeight.w700),
                ),
              ),
              SizedBox(height: 20.sp),
            ],
          ),
        ),
      ],
    );
  }

  Widget _discoverCard(VideoPlayerState state) {
    return Container(
      child: Column(
        children: [
          Container(
            alignment: Alignment.center,
            color: Colors.black,
            child: Stack(
              children: [
                if (state is VideoPlaying && state.videos[widget.discoverModel.title] != null)
                  _youtubePlayer(state.videos[widget.discoverModel.title]!)
                else
                  Container(
                    height: 180.sp,
                    child: Center(
                      child: CupertinoActivityIndicator(
                        radius: 15.sp,
                      ),
                    ),
                  ),
              ],
            ),
          ),
          Visibility(
            visible: !(state is VideoPlaying
                ? state.videos[widget.discoverModel.title]?.value.isFullScreen ?? false
                : false),
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16.sp),
              alignment: Alignment.centerLeft,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.vertical(
                  bottom: Radius.circular(10.sp),
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 10.sp),
                  Text(
                    widget.discoverModel.descriptions,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontSize: 14.sp,
                      fontWeight: FontWeight.w700,
                      color: colorBlack2,
                    ),
                  ),
                  SizedBox(height: 6.sp),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        widget.discoverModel.content.fullname,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontSize: 10.sp,
                          color: colorGray1,
                        ),
                      ),
                      Text(
                        getDateString(widget.discoverModel.date.toLocal()),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontSize: 10.sp,
                          color: colorGray1,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
