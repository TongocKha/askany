import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/home/home_bloc.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_none.dart';
import 'package:askany/src/ui/home/discover/discover_list.dart';
import 'package:askany/src/ui/home/group_expert/group_expert_list.dart';
import 'package:askany/src/ui/home/hot_expert/hot_expert_list.dart';
import 'package:askany/src/ui/home/services/consulting_service_list.dart';
import 'package:askany/src/ui/home/widget/home_header.dart';
import 'package:askany/src/ui/home/widget/home_small_header.dart';
import 'package:askany/src/ui/home/widget/search_home.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin {
  final Key linkKey = GlobalKey();
  RefreshController _refreshController = RefreshController(initialRefresh: false);
  ScrollController _scrollController = ScrollController();
  bool _isBigHeader = true;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels > 114.sp) {
        if (_isBigHeader) {
          setState(() {
            _isBigHeader = false;
          });
        }
      } else if (_scrollController.position.pixels <= 262.sp) {
        if (!_isBigHeader) {
          setState(() {
            _isBigHeader = true;
          });
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: appBarBrighnessDark(
        backgroundColor: _isBigHeader ? null : headerCalendarColor,
      ),
      body: SafeArea(
        top: !_isBigHeader,
        child: Container(
          color: Colors.transparent,
          child: Column(
            children: [
              AnimatedContainer(
                height: !_isBigHeader ? 60.sp : 0.0,
                duration: const Duration(milliseconds: DURATION_DEFAULT_ANIMATION),
                curve: Curves.decelerate,
                child: HomeSmallHeader(),
              ),
              Expanded(
                child: SmartRefresher(
                  physics: BouncingScrollPhysics(),
                  scrollController: _scrollController,
                  controller: _refreshController,
                  enablePullUp: false,
                  enablePullDown: true,
                  header: MaterialClassicHeader(
                    offset: 0,
                    height: 44.sp,
                    distance: 20.sp,
                    backgroundColor: colorGreen2,
                    color: Colors.white,
                  ),
                  onRefresh: () async {
                    AppBloc.homeBloc.add(
                      RefreshHomeEvent(
                        handleFinished: () => _refreshController.refreshCompleted(),
                      ),
                    );
                  },
                  onLoading: () => null,
                  child: SingleChildScrollView(
                    physics: BouncingScrollPhysics(),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        AnimatedContainer(
                          height: _isBigHeader ? 158.sp : 114.sp,
                          duration: const Duration(milliseconds: DURATION_DEFAULT_ANIMATION),
                          curve: Curves.decelerate,
                          child: Visibility(
                            visible: _isBigHeader,
                            child: Container(
                              height: 158.sp,
                              child: Stack(
                                children: [
                                  HomeHeader(),
                                  Positioned(
                                    bottom: 0,
                                    left: 0,
                                    right: 0,
                                    child: SearchHome(),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 22.sp),
                        ConsultingServiceList(),
                        SizedBox(height: 18.sp),
                        GroupExpertList(),
                        SizedBox(height: 18.sp),
                        DiscoverList(),
                        SizedBox(height: 18.sp),
                        HotExpertList(),
                        SizedBox(height: 18.sp),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
