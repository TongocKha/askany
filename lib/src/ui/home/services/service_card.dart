import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/content/content_bloc.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/models/category_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/service_image/cached_image_custom.dart';
import 'package:askany/src/ui/style/calendar_style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ServiceCard extends StatelessWidget {
  final CategoryModel categoryModel;
  ServiceCard({required this.categoryModel});
  @override
  Widget build(BuildContext context) {
    return TouchableOpacity(
      onTap: () {
        if (UserLocal().getAccessToken() == '') {
          AppNavigator.push(Routes.CHOOSE_ACCOUNT);
        } else {
          AppBloc.contentBloc.add(
            GetExpertCategoryEvent(
              slug: categoryModel.slug,
            ),
          );
          AppNavigator.push(
            Routes.DETAILS_CATEGORY,
            arguments: {
              'categoryModel': categoryModel,
            },
          );
        }
      },
      child: Container(
        margin: EdgeInsets.only(right: 10.sp, bottom: 5.sp),
        width: 115.sp,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10.sp)),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.08),
              blurRadius: 4.sp,
              offset: Offset(1, 1),
            ),
          ],
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CachedImageCustom(
              urlToImage: categoryModel.thumbnail?.urlToImage,
              height: 112.sp,
              width: 115.sp,
              radius: BorderRadius.vertical(
                top: Radius.circular(10.sp),
              ),
            ),
            Expanded(
              child: Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.symmetric(horizontal: 8.sp),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.vertical(
                    bottom: Radius.circular(10.sp),
                  ),
                ),
                child: Text(
                  categoryModel.name,
                  style: TextStyle(
                    fontSize: 12.sp,
                    fontWeight: FontWeight.w600,
                    color: colorBlack2,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
