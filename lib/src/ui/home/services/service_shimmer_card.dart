import 'package:askany/src/ui/common/widgets/shimmers/fade_simmer.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ServiceShimmerCard extends StatelessWidget {
  const ServiceShimmerCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 10.sp, bottom: 5.sp),
      width: 115.sp,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10.sp)),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.08),
            blurRadius: 4.sp,
            offset: Offset(1, 1),
          ),
        ],
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          FadeShimmer(
            borderRadius: BorderRadius.vertical(
              top: Radius.circular(10.sp),
            ),
            height: 112.sp,
            width: 115.sp,
            fadeTheme: FadeTheme.lightReverse,
          ),
          Expanded(
            child: Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.symmetric(horizontal: 8.sp),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.vertical(
                  bottom: Radius.circular(10.sp),
                ),
              ),
              child: FadeShimmer(
                height: 14.sp,
                width: 80.sp,
                fadeTheme: FadeTheme.lightReverse,
              ),
            ),
          )
        ],
      ),
    );
  }
}
