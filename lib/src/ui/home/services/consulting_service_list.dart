import 'package:askany/src/bloc/category_home/category_home_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/models/category_model.dart';
import 'package:askany/src/ui/common/widgets/text_ui/title_and_seemore.dart';
import 'package:askany/src/ui/home/services/service_card.dart';
import 'package:askany/src/ui/home/services/service_shimmer_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ConsultingServiceList extends StatelessWidget {
  const ConsultingServiceList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.sp),
            child: TitleAndSeeMore(
              title: Strings.popularServices.i18n,
              checkSeeMore: false,
            ),
          ),
          SizedBox(height: 16.sp),
          BlocBuilder<CategoryHomeBloc, CategoryHomeState>(
            builder: (context, state) {
              List<CategoryModel> categories = (state.props[0] as List).cast();
              return state is CategoryHomeInitial
                  ? ServiceShimmerList()
                  : Container(
                      height: 162.sp,
                      child: ListView.builder(
                        padding: EdgeInsets.symmetric(horizontal: 16.sp),
                        scrollDirection: Axis.horizontal,
                        physics: BouncingScrollPhysics(),
                        itemCount: categories.length,
                        itemBuilder: (context, index) {
                          return ServiceCard(categoryModel: categories[index]);
                        },
                      ),
                    );
            },
          )
        ],
      ),
    );
  }
}
