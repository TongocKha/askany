import 'package:askany/src/configs/lang/language_service.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/models/category_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class GroupExpertCard extends StatelessWidget {
  final CategoryInfoModel categories;
  GroupExpertCard({required this.categories});
  @override
  Widget build(BuildContext context) {
    return TouchableOpacity(
      onTap: () {
        if (UserLocal().getAccessToken() == '') {
          AppNavigator.push(Routes.CHOOSE_ACCOUNT);
        } else {
          AppNavigator.push(Routes.SUB_CATEGORY, arguments: {
            'categoryInfo': categories,
          });
        }
      },
      child: Container(
        margin: EdgeInsets.only(right: 10.sp, bottom: .5.sp),
        padding: EdgeInsets.symmetric(horizontal: 10.sp),
        decoration: BoxDecoration(
          border: Border.all(
            color: colorGray2,
            width: 0.5,
          ),
          borderRadius: BorderRadius.all(
            Radius.circular(8.sp),
          ),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              padding: EdgeInsets.zero,
              alignment: Alignment.centerLeft,
              child: Image.network(
                LanguageService.getIsLanguage('vi')
                    ? (categories.categoryInfo.icon?.urlToImage ?? '')
                    : (categories.categoryInfo.thumbnail?.urlToImage ?? ''),
                width: 22.5.sp,
                height: 22.5.sp,
              ),
            ),
            SizedBox(width: 12.sp),
            Container(
              width: 60.sp,
              alignment: Alignment.centerLeft,
              child: Text(
                categories.categoryInfo.name,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontSize: 11.25.sp,
                  fontWeight: FontWeight.w600,
                  color: colorBlack2,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
