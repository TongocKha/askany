import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/ui/home/group_expert/group_expert_shimmer_card.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class GroupExpertShimmerList extends StatelessWidget {
  const GroupExpertShimmerList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50.sp,
      child: ListView.builder(
        padding: EdgeInsets.symmetric(horizontal: 16.sp),
        scrollDirection: Axis.horizontal,
        physics: BouncingScrollPhysics(),
        shrinkWrap: true,
        itemCount: ITEM_COUNT_SHIMMER,
        itemBuilder: (context, index) {
          return GroupExpertShimmerCard();
        },
      ),
    );
  }
}
