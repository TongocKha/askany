import 'package:askany/src/ui/common/widgets/shimmers/fade_simmer.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class GroupExpertShimmerCard extends StatelessWidget {
  const GroupExpertShimmerCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 10.sp, bottom: .5.sp),
      padding: EdgeInsets.symmetric(horizontal: 16.sp),
      decoration: BoxDecoration(
        border: Border.all(
          color: colorGray2,
          width: 0.5,
        ),
        borderRadius: BorderRadius.all(
          Radius.circular(8.sp),
        ),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            padding: EdgeInsets.zero,
            alignment: Alignment.center,
            child: FadeShimmer(
              height: 25.sp,
              width: 25.sp,
              fadeTheme: FadeTheme.lightReverse,
            ),
          ),
          Container(
              width: 70.sp,
              alignment: Alignment.center,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  FadeShimmer(
                    height: 13.sp,
                    width: 60.sp,
                    fadeTheme: FadeTheme.lightReverse,
                  ),
                  SizedBox(height: 3.sp),
                  FadeShimmer(
                    height: 13.sp,
                    width: 40.sp,
                    fadeTheme: FadeTheme.lightReverse,
                  ),
                ],
              )),
        ],
      ),
    );
  }
}
