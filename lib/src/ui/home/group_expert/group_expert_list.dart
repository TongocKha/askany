import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/category/category_bloc.dart';
import 'package:askany/src/bloc/home/home_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/models/category_model.dart';
import 'package:askany/src/ui/common/widgets/pagination_list_view.dart';
import 'package:askany/src/ui/common/widgets/text_ui/title_and_seemore.dart';
import 'package:askany/src/ui/home/group_expert/group_expert_card.dart';
import 'package:askany/src/ui/home/group_expert/group_expert_shimmer_list.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class GroupExpertList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.only(left: 16.sp, right: 10.sp),
            child: TitleAndSeeMore(
              title: Strings.expertCommunity.i18n,
              handlePressed: () {
                AppBloc.homeBloc.add(OnChangeIndexEvent(index: 2));
              },
            ),
          ),
          SizedBox(height: 18.sp),
          BlocBuilder<CategoryBloc, CategoryState>(
            builder: (context, state) {
              List<CategoryInfoModel> categories =
                  (state.props[0] as List).cast();
              return state is CategoryInitial
                  ? GroupExpertShimmerList()
                  : Container(
                      height: 50.sp,
                      child: PaginationListView(
                        physics: BouncingScrollPhysics(),
                        padding: EdgeInsets.symmetric(horizontal: 16.sp),
                        scrollDirection: Axis.horizontal,
                        childShimmer: GroupExpertShimmerList(),
                        itemCount: categories.length,
                        itemBuilder: (context, index) {
                          return GroupExpertCard(
                            categories: categories[index],
                          );
                        },
                      ),
                    );
            },
          ),
        ],
      ),
    );
  }
}
