import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/models/expert_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/cake_avatar/cached_cake_image.dart';

import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class HotExpertCard extends StatelessWidget {
  final ExpertModel hotExpertModel;
  HotExpertCard({required this.hotExpertModel});

  @override
  Widget build(BuildContext context) {
    return TouchableOpacity(
      onTap: () {
        AppNavigator.push(Routes.DETAILS_SPECIALIST, arguments: {
          'expertId': hotExpertModel.id,
          'specialtyId': hotExpertModel.specialtyId == null ||
                  hotExpertModel.specialtyId!.isEmpty
              ? null
              : hotExpertModel.specialtyId!.first,
        });
      },
      child: Container(
        margin: EdgeInsets.only(right: 10.sp, bottom: 5.sp),
        width: 120.sp,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10.sp)),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.08),
              blurRadius: 4.sp,
              offset: Offset(1, 1),
            ),
          ],
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 90.sp,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.vertical(
                  top: Radius.circular(10.sp),
                ),
              ),
              child: Stack(
                children: [
                  Container(
                    height: 80.sp,
                    decoration: BoxDecoration(
                      color: backgroundNotification,
                      borderRadius: BorderRadius.vertical(
                        top: Radius.circular(10.sp),
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    child: Align(
                      alignment: Alignment.center,
                      child: CachedCakeImage(
                        urlToImage: hotExpertModel.avatar?.urlToImage,
                        size: 65.sp,
                        radius: BorderRadius.all(
                          Radius.circular(65.sp),
                        ),
                        fit: BoxFit.cover,
                      ),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: 11.sp),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 7.sp),
              alignment: Alignment.center,
              child: Text(
                hotExpertModel.fullname,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 12.sp,
                  color: colorBlack1,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            SizedBox(height: 4.sp),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 7.sp),
              alignment: Alignment.center,
              child: Text(
                '${Strings.anExpertIn.i18n} ${hotExpertModel.expertSpecialist}',
                maxLines: 2,
                textAlign: TextAlign.center,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(fontSize: 11.sp, color: colorGray1),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
