import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/hot_expert_home/hot_expert_home_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/models/expert_model.dart';
import 'package:askany/src/ui/common/widgets/text_ui/title_and_seemore.dart';
import 'package:askany/src/ui/home/hot_expert/hot_expert_card.dart';
import 'package:askany/src/ui/home/hot_expert/hot_expert_shimmer_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class HotExpertList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.sp),
            child: TitleAndSeeMore(
              title: Strings.monthExpert.i18n,
              checkSeeMore: false,
            ),
          ),
          SizedBox(height: 18.sp),
          BlocBuilder<HotExpertHomeBloc, HotExpertHomeState>(
            builder: (context, state) {
              List<ExpertModel> experts = (state.props[0] as List).cast();
              experts = experts
                  .where(
                    (expert) => expert.id != AppBloc.userBloc.getAccount.id,
                  )
                  .toList();

              return state is HotExpertHomeInitial
                  ? HotExpertShimmerList()
                  : Container(
                      height: 175.sp,
                      child: ListView.builder(
                        padding: EdgeInsets.symmetric(horizontal: 16.sp),
                        scrollDirection: Axis.horizontal,
                        physics: BouncingScrollPhysics(),
                        itemCount: experts.length,
                        itemBuilder: (context, index) {
                          return HotExpertCard(
                            hotExpertModel: experts[index],
                          );
                        },
                      ),
                    );
            },
          )
        ],
      ),
    );
  }
}
