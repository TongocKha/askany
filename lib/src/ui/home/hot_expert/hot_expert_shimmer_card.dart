import 'package:askany/src/ui/common/widgets/shimmers/fade_simmer.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class HotExpertShimmerCard extends StatelessWidget {
  const HotExpertShimmerCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 10.sp, bottom: 5.sp),
      width: 120.sp,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10.sp)),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.08),
            blurRadius: 4.sp,
            offset: Offset(1, 1),
          ),
        ],
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 90.sp,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.vertical(
                top: Radius.circular(10.sp),
              ),
            ),
            child: Stack(
              children: [
                Container(
                  height: 80.sp,
                  decoration: BoxDecoration(
                    color: backgroundNotification,
                    borderRadius: BorderRadius.vertical(
                      top: Radius.circular(10.sp),
                    ),
                  ),
                ),
                Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: Align(
                    alignment: Alignment.center,
                    child: FadeShimmer.round(
                      size: 65.sp,
                      fadeTheme: FadeTheme.lightReverse,
                    ),
                  ),
                )
              ],
            ),
          ),
          SizedBox(height: 12.sp),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 7.sp),
            alignment: Alignment.center,
            child: FadeShimmer(
              height: 13.sp,
              width: 100.sp,
              fadeTheme: FadeTheme.lightReverse,
            ),
          ),
          SizedBox(height: 6.sp),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 7.sp),
            alignment: Alignment.center,
            child: FadeShimmer(
              height: 13.sp,
              width: 110.sp,
              fadeTheme: FadeTheme.lightReverse,
            ),
          ),
          SizedBox(height: 4.sp),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 7.sp),
            alignment: Alignment.center,
            child: FadeShimmer(
              height: 12.sp,
              width: 50.sp,
              fadeTheme: FadeTheme.lightReverse,
            ),
          ),
        ],
      ),
    );
  }
}
