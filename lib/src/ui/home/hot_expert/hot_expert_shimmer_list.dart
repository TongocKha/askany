import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/ui/home/hot_expert/hot_expert_shimmer_card.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class HotExpertShimmerList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 175.sp,
      child: ListView.builder(
        padding: EdgeInsets.symmetric(horizontal: 16.sp),
        scrollDirection: Axis.horizontal,
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: ITEM_COUNT_SHIMMER,
        itemBuilder: (context, index) => HotExpertShimmerCard(),
      ),
    );
  }
}
