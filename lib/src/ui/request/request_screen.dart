import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/request/request_bloc.dart';
import 'package:askany/src/bloc/video_call/video_call_bloc.dart';
import 'package:askany/src/configs/lang/language_service.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/routes/scaffold_wrapper.dart';
import 'package:askany/src/ui/common/widgets/animations/custom_animations/src/open_container.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_transparent.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_message.dart';
import 'package:askany/src/ui/common/widgets/highlight_coachmark/highlight_coachmark.dart';
import 'package:askany/src/ui/request/screens/create_request_screen.dart';
import 'package:askany/src/ui/request/screens/my_request_screen.dart';
import 'package:askany/src/ui/request/screens/request_service_screen.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class RequestScreen extends StatefulWidget {
  const RequestScreen({
    Key? key,
  }) : super(key: key);

  @override
  State<RequestScreen> createState() => _RequestPage();
}

class _RequestPage extends State<RequestScreen> with TickerProviderStateMixin {
  late TabController _tabController;
  final bool _isExpert = AppBloc.userBloc.getAccount.isExpert ?? false;
  final _floatButtonKey = GlobalKey(debugLabel: "floatButton");

  @override
  void initState() {
    super.initState();

    AppBloc.requestBloc.add(OnCommonEvent());

    _tabController = TabController(length: 2, vsync: this);
    _tabController.addListener(() {
      if (!_tabController.indexIsChanging &&
          !_tabController.animation!.isDismissed) {
        if (_tabController.index == 0) {
          AppBloc.requestBloc.add(OnCommonEvent());
        } else {
          AppBloc.requestBloc.add(CleanRequestEvent());
          AppBloc.requestBloc.add(OnHappeningEvent());
        }
      }
    });
    if (UserLocal().getShouldShowTutorial()) {
      if (!AppBloc.userBloc.getAccount.isExpert!) {
        WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {
          _showFloatingButtonTutorial();
        });
      }
    }
  }

  @override
  void dispose() {
    if (!AppBloc.requestBloc.isNotCondition) {
      AppBloc.requestBloc.add(CleanFilterRequestEvent());
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarCategory(
        title: Strings.requestsAppbartext.i18n,
        actions: [
          ButtonMessage(
            iconHeader: iconMessageHeaderBlackLine,
            padding: EdgeInsets.only(right: 16.sp),
          ),
        ],
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(42.sp),
          child: Padding(
            padding: EdgeInsets.only(left: 4.sp),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TabBar(
                    isScrollable: true,
                    physics: NeverScrollableScrollPhysics(),
                    labelStyle: TextStyle(
                      fontSize: 12.5.sp,
                      color: colorGreen2,
                      fontWeight: FontWeight.w700,
                    ),
                    indicatorColor: colorGreen2,
                    indicatorSize: TabBarIndicatorSize.label,
                    indicatorPadding: LanguageService.getIsLanguage('vi')
                        ? EdgeInsets.symmetric(horizontal: 30.sp)
                        : EdgeInsets.symmetric(horizontal: 15.sp),
                    indicatorWeight: 1.5.sp,
                    controller: _tabController,
                    labelColor: colorGreen2,
                    unselectedLabelColor: colorGray1,
                    unselectedLabelStyle: TextStyle(
                      fontSize: 12.5.sp,
                      color: colorGray1,
                    ),
                    tabs: <Widget>[
                      Tab(
                        child: Text(Strings.requests.i18n),
                      ),
                      Tab(
                        child: Text(
                          AppBloc.userBloc.getAccount.isExpert!
                              ? Strings.bidRequests.i18n
                              : Strings.myRequests.i18n,
                        ),
                      ),
                    ],
                  ),
                  dividerChat,
                ],
              ),
            ),
          ),
        ),
      ),
      body: Container(
        color: colorWhite,
        child: TabBarView(
          physics: ClampingScrollPhysics(),
          controller: _tabController,
          children: <Widget>[
            RequestServiceScreen(),
            MyRequestScreen(),
          ],
        ),
      ),
      floatingActionButton: UserLocal().getAccessToken() == '' || _isExpert
          ? null
          : BlocBuilder<VideoCallBloc, VideoCallState>(
              builder: (context, state) {
                return Container(
                  margin: EdgeInsets.only(
                    bottom: state is VideoCalling ? 18.sp : 10.sp,
                  ),
                  child: OpenContainer(
                    closedShape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(23.sp),
                      ),
                    ),
                    closedElevation: 0.0,
                    closedColor: colorAddButton,
                    openElevation: 0.0,
                    openColor: colorAddButton,
                    transitionType: ContainerTransitionType.fadeThrough,
                    transitionDuration:
                        Duration(milliseconds: DELAY_HALF_SECOND),
                    openBuilder: (_, callBack) {
                      return ScaffoldWrapper(
                          child: CreateRequestScreen(request: null));
                    },
                    closedBuilder: (_, callBack) {
                      return Container(
                        height: 46.sp,
                        width: 46.sp,
                        child: FloatingActionButton(
                          key: _floatButtonKey,
                          onPressed: callBack,
                          elevation: 1.0,
                          child: Image.asset(
                            iconCreateRequest,
                            width: 20.sp,
                          ),
                          backgroundColor: colorAddButton,
                        ),
                      );
                    },
                  ),
                );
              },
            ),
    );
  }

  void _showFloatingButtonTutorial() {
    CoachMark coachMark = CoachMark();
    RenderBox? target =
        _floatButtonKey.currentContext?.findRenderObject() as RenderBox?;
    if (target == null) return;
    Rect markRect = target.localToGlobal(Offset.zero) & target.size;
    markRect = Rect.fromCircle(
      center: markRect.center,
      radius: markRect.height / 2,
    );
    coachMark.show(
      targetContext: _floatButtonKey.currentContext!,
      children: [
        Positioned(
          top: markRect.top,
          right: (100.w - markRect.center.dx) + markRect.width / 2,
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 18.sp),
            height: 43.sp,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  width: 46.4.w,
                  child: Text(
                    Strings.pressToCreateNewRequest.i18n,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 13.sp,
                      fontWeight: FontWeight.w600,
                      height: 1.625,
                      color: colorWhiteCard,
                    ),
                  ),
                ),
                SizedBox(width: 13.sp),
                Image.asset(imageArrowTutorial, width: 12.w),
              ],
            ),
          ),
        ),
      ],
      markRect: markRect,
      onClose: () {
        UserLocal().showedTutorial();
      },
    );
  }
}
