import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/request/request_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/models/request_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/empty/empty.dart';
import 'package:askany/src/ui/common/widgets/pagination_list_view.dart';
import 'package:askany/src/ui/request/widgets/request_shimmer_card.dart';
import 'package:askany/src/ui/request/widgets/request_shimmers_list.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:askany/src/ui/request/widgets/request_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class HappeningList extends StatefulWidget {
  @override
  _HappeningList createState() => _HappeningList();
}

class _HappeningList extends State<HappeningList> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RequestBloc, RequestState>(
      builder: (context, state) {
        if (state is GettingRequestHappening ||
            state is GetDoneRequestHappening) {
          List<RequestModel> requests = state.props[0] as List<RequestModel>;

          return requests.length > 0
              ? PaginationListView(
                  padding: EdgeInsets.only(bottom: 24.sp, top: 6.sp),
                  itemCount: requests.length,
                  callBackLoadMore: () =>
                      AppBloc.requestBloc.add(GetRequestHappeningEvent()),
                  callBackRefresh: (Function handleFinished) {
                    AppBloc.requestBloc.add(
                      RefreshRequestHappeningEvent(
                        handleFinished: handleFinished,
                      ),
                    );
                  },
                  isLoadMore: state is GettingRequestHappening,
                  childShimmer: RequestShimmerCard(index: -1),
                  itemBuilder: (context, index) {
                    return Column(
                      children: [
                        TouchableOpacity(
                          onTap: () {
                            AppNavigator.push(Routes.DETAILS_REQUEST,
                                arguments: {
                                  'requestModel': requests[index],
                                });
                          },
                          child: RequestCard(request: requests[index]),
                        ),
                        index < requests.length - 1
                            ? dividerThinkness6NotMargin
                            : SizedBox(),
                      ],
                    );
                  },
                )
              : Empty(
                  image: Image.asset(
                    imageRequestEmpty,
                    width: 140.sp,
                    height: 82.sp,
                  ),
                  text: Strings.youDontHavenAnyRequest.i18n,
                );
        }

        return RequestsShimmersList();
      },
    );
  }
}
