import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_date_picker.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class BottomSheetCalendar extends StatelessWidget {
  final DateTime selected;
  final Function(DateTime) onDateSelected;
  BottomSheetCalendar({
    required this.selected,
    required this.onDateSelected,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 350.sp,
        decoration: BoxDecoration(
          color: colorWhite,
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(18.sp),
          ),
        ),
        child: Column(
          children: [
            SizedBox(height: 5.sp),
            Align(
              alignment: Alignment.center,
              child: Container(
                height: 3.sp,
                width: 60.sp,
                decoration: BoxDecoration(
                  color: colorGray4,
                  borderRadius: BorderRadius.circular(30),
                ),
              ),
            ),
            SizedBox(height: 15.sp),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 16.sp),
              alignment: Alignment.centerLeft,
              child: Text(
                Strings.bidingClosingDate.i18n,
                style: TextStyle(
                  color: colorBlack2,
                  fontSize: 15.sp,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
            SizedBox(height: 10.sp),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.sp),
              child: dividerChat,
            ),
            DialogDatePicker(
              selectedDate: selected,
              onDateSelected: onDateSelected,
            ),
          ],
        ));
  }
}
