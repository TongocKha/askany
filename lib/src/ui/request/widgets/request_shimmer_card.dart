import 'package:flutter/material.dart';
import 'package:askany/src/ui/common/widgets/shimmers/fade_simmer.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class RequestShimmerCard extends StatelessWidget {
  final int index;
  const RequestShimmerCard({required this.index});
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.symmetric(horizontal: 16.sp, vertical: 18.sp),
          color: colorRequestCard,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Container(
                        alignment: Alignment.centerLeft,
                        height: 20.sp,
                        child: FadeShimmer(
                          width: 60.w,
                          height: 13.sp,
                          fadeTheme: FadeTheme.lightReverse,
                        ),
                      ),
                      Container(
                        height: 20.sp,
                        alignment: Alignment.centerLeft,
                        child: FadeShimmer(
                          width: 50.w,
                          height: 13.sp,
                          fadeTheme: FadeTheme.lightReverse,
                        ),
                      ),
                    ],
                  ),
                  Container(
                    height: 20.sp,
                    alignment: Alignment.centerLeft,
                    child: FadeShimmer(
                      width: 20.w,
                      height: 11.sp,
                      fadeTheme: FadeTheme.lightReverse,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 10.sp),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: 20.sp,
                    alignment: Alignment.centerLeft,
                    child: FadeShimmer(
                      width: 40.w,
                      height: 11.sp,
                      fadeTheme: FadeTheme.lightReverse,
                    ),
                  ),
                  Container(
                    height: 20.sp,
                    alignment: Alignment.centerLeft,
                    child: FadeShimmer(
                      width: 40.w,
                      height: 11.sp,
                      fadeTheme: FadeTheme.lightReverse,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 10.sp),
              Container(
                height: 20.sp,
                alignment: Alignment.centerLeft,
                child: FadeShimmer(
                  width: 60.sp,
                  height: 11.sp,
                  fadeTheme: FadeTheme.lightReverse,
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                  top: 14.sp,
                  bottom: 10.sp,
                ),
                child: dividerChat,
              ),
              Column(
                children: [
                  Container(
                    alignment: Alignment.centerLeft,
                    height: 20.sp,
                    child: FadeShimmer(
                      height: 11.sp,
                      width: 100.w,
                      fadeTheme: FadeTheme.lightReverse,
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    height: 20.sp,
                    child: FadeShimmer(
                      height: 11.sp,
                      width: 100.w,
                      fadeTheme: FadeTheme.lightReverse,
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    height: 20.sp,
                    child: FadeShimmer(
                      height: 11.sp,
                      width: 100.sp,
                      fadeTheme: FadeTheme.lightReverse,
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
        index < 2 ? dividerThinkness6NotMargin : SizedBox(),
      ],
    );
  }
}
