import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/request/request_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/helpers/currency_helper.dart';
import 'package:askany/src/helpers/extentions/string_extention.dart';
import 'package:askany/src/models/budget_model.dart';
import 'package:askany/src/models/request_model.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_form.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_form_request.dart';
import 'package:askany/src/ui/request/widgets/request_dropdown_button.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class CreateOffer extends StatefulWidget {
  final RequestModel requestModel;
  CreateOffer({required this.requestModel});

  @override
  State<CreateOffer> createState() => _CreateOfferState();
}

class _CreateOfferState extends State<CreateOffer> {
  TextEditingController _priceController = TextEditingController();
  TextEditingController _contentController = TextEditingController();
  TextEditingController _timeController = TextEditingController();
  String _price = '0';
  String _content = '';
  String _timeInput = '0';
  String inputTypeTime = '';
  String _currency = CURRENCY_VND;
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  List<String> _buildTimeItems() {
    if ((int.tryParse(_timeController.text) ?? 0) > 1) {
      return [
        Strings.hoursUperCase.i18n,
        Strings.minutesUperCase.i18n,
      ];
    }
    return [
      Strings.hourUperCase.i18n,
      Strings.minuteUperCase.i18n,
    ];
  }

  String _buildTimeHintext() {
    if ((int.tryParse(_timeController.text) ?? 0) > 1) {
      return Strings.hoursUperCase.i18n;
    }
    return Strings.hourUperCase.i18n;
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 16.sp, vertical: 25.sp),
        color: colorAddButton,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              Strings.offerForThisRequest.i18n,
              style: TextStyle(
                color: colorBlack1,
                fontSize: 16.sp,
                fontWeight: FontWeight.w700,
              ),
            ),
            SizedBox(height: 10.sp),
            Text(
              Strings.yourOfferForThisRequest.i18n,
              style: TextStyle(
                color: colorBlack2,
                fontSize: 13.sp,
              ),
            ),
            SizedBox(height: 8.sp),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  child: Container(
                    height: 40.sp,
                    padding: EdgeInsets.symmetric(horizontal: 9.sp),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(6.5.sp),
                      border: Border.all(
                        color: colorBorderTextField,
                        width: 0.5.sp,
                      ),
                    ),
                    child: TextFormField(
                      controller: _priceController,
                      cursorColor: headerCalendarColor,
                      maxLines: 1,
                      keyboardType: TextInputType.number,
                      inputFormatters: _currency == CURRENCY_VND
                          ? CurrencyHelper.vndInputFormat
                          : CurrencyHelper.usdInputFormat,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                      ),
                      onChanged: (val) {
                        _price = val.trim();
                      },
                    ),
                  ),
                ),
                SizedBox(width: 9.sp),
                TouchableOpacity(
                  onTap: () {
                    setState(() {
                      if (_currency == CURRENCY_VND) {
                        _currency = CURRENCY_USD;
                      } else {
                        _currency = CURRENCY_VND;
                      }
                    });

                    if (_priceController.text.isNotEmpty) {
                      if (_currency == CURRENCY_VND) {
                        _priceController.text = double.parse(_priceController.text)
                            .toStringAsFixed(0)
                            .formatMoney(splitBy: ',');
                      } else {
                        _priceController.text = _priceController.text.replaceAll(',', '');
                      }
                    }
                  },
                  child: Container(
                    alignment: Alignment.centerRight,
                    child: Text(
                      _currency == CURRENCY_VND
                          ? Strings.vnd.i18n.toUpperCase()
                          : Strings.usd.i18n.toUpperCase(),
                      style: TextStyle(
                        fontWeight: FontWeight.w400,
                        color: colorBlack2,
                        fontSize: 12.5.sp,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 20.sp),
            Text(
              Strings.adviseDuration.i18n,
              style: TextStyle(
                color: colorBlack2,
                fontSize: 13.sp,
              ),
            ),
            Container(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Expanded(
                    child: TextFieldForm(
                      hintText: Strings.timeHintext.i18n,
                      margin: EdgeInsets.only(top: 7.sp),
                      controller: _timeController,
                      textInputType: TextInputType.number,
                      onChanged: (val) {
                        _timeInput = val.trim();
                        setState(() {});
                      },
                      validatorForm: (val){
                        if (val != '' && double.parse(val!) > 0){
                        if (inputTypeTime == Strings.minutesUperCase.i18n){
                          if (double.parse(val) % 15 != 0){
                            return Strings.adviseDurationMustBeDivisibleBy15Errortext.i18n;
                            
                          }
                          if (double.parse(val) > 240){
                            return Strings.adviseDurationMustBeLessThan240m.i18n;
                          }
                          return null;


                        }else{
                          if (double.parse(val) > 4){
                            return  Strings.adviseDurationMustBeLessThan4h.i18n;
                          }
                          return null;

                        }
                        }
                        return Strings.adviseDurationCannotBeLeftBlankErrortext.i18n;


                     
                      }
                    ),
                  ),
                  SizedBox(width: 9.sp),
                  Container(
                    margin: EdgeInsets.only(top: 5.sp),
                    width: 85.sp,
                    padding: EdgeInsets.zero,
                    child: RequestDropdownButton(
                      buttonHeight: 40.sp,
                      hintText: _buildTimeHintext(),
                      items: _buildTimeItems(),
                      onChanged: (val) => inputTypeTime = val.toString().trim(),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 20.sp),
            Text(
              Strings.convinceCustomers.i18n,
              style: TextStyle(
                color: colorBlack2,
                fontSize: 13.sp,
              ),
            ),
            TextFieldFormRequest(
              hintText: Strings.enterDescriptionAboutYouHintext.i18n,
              maxLines: 4,
              validatorForm: (val) => val!.isEmpty || val.trim().length == 0
                  ? Strings.requestTitleCannotBeLeftBlankErrortext.i18n
                  : null,
              colorTextField: colorWhite,
              controller: _contentController,
              onChanged: (val) => _content = val.trim(),
            ),
            SizedBox(height: 20.sp),
            ButtonPrimary(
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  showDialogLoading();
                  AppBloc.requestBloc.add(
                    ExpertCreateOfferEvent(
                      requestModel: widget.requestModel,
                      budgetModel: BudgetModel(
                        currency: _currency,
                        cost: double.parse(_price.replaceAll(',', '')),
                        totalMinutes: inputTypeTime == Strings.minutesUperCase.i18n
                            ? int.parse(_timeInput)
                            : int.parse(_timeInput) * 60,
                      ),
                      content: _content,
                    ),
                  );
                }
              },
              text: Strings.postOffer.i18n,
              fontWeight: FontWeight.w700,
              width: 135.sp,
            )
          ],
        ),
      ),
    );
  }
}
