import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class BottomSheetString extends StatelessWidget {
  final List<String> listString;
  final double sizeBottomSheet;
  final String title;
  final Function(String) handlePressed;
  const BottomSheetString({
    required this.listString,
    required this.sizeBottomSheet,
    required this.title,
    required this.handlePressed,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: sizeBottomSheet,
      padding: EdgeInsets.symmetric(horizontal: 16.sp),
      decoration: BoxDecoration(
        color: colorWhite,
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(18.sp),
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 5.sp),
          Align(
            alignment: Alignment.center,
            child: Container(
              height: 3.sp,
              width: 60.sp,
              decoration: BoxDecoration(
                color: colorGray4,
                borderRadius: BorderRadius.circular(30),
              ),
            ),
          ),
          SizedBox(height: 15.sp),
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              title,
              style: TextStyle(
                color: colorBlack2,
                fontSize: 15.sp,
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
          SizedBox(height: 10.sp),
          dividerChat,
          Expanded(
            child: ListView.builder(
              physics: BouncingScrollPhysics(),
              itemCount: listString.length,
              itemBuilder: (context, index) {
                return TouchableOpacity(
                  onTap: () {
                    handlePressed(listString[index]);
                    AppNavigator.pop();
                  },
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 12.sp),
                    color: Colors.transparent,
                    child: Text(
                      listString[index],
                      style: TextStyle(
                        fontSize: 13.sp,
                        color: colorBlack2,
                      ),
                    ),
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
