import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

import 'package:askany/src/ui/style/style.dart';

class RatingCard extends StatelessWidget {
  final double rating;
  final bool? isFeedback;
  final String textBehindStar;
  final double widthStar;

  const RatingCard({
    Key? key,
    required this.rating,
    this.isFeedback,
    this.textBehindStar = '',
    this.widthStar = 11,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 14.sp,
      alignment: Alignment.center,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ListView.builder(
            padding: EdgeInsets.all(0),
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemCount: 5,
            itemBuilder: (context, index) {
              return Padding(
                padding: EdgeInsets.only(right: (widthStar * 30 / 100).sp),
                child: Image.asset(
                  index + 1 <= rating.round() ? iconStar : iconStarEmpty,
                  width: widthStar.sp,
                  color: index + 1 <= rating.round() ? colorStar : Colors.grey,
                ),
              );
            },
          ),
          SizedBox(width: 5.sp),
          Padding(
            padding: EdgeInsets.only(top: 2.sp),
            child: isFeedback == true
                ? Container()
                : Text(
                    textBehindStar.isEmpty ? rating.toStringAsFixed(1) : textBehindStar,
                    style: TextStyle(
                      color: colorGray1,
                      fontSize: 11.sp,
                    ),
                  ),
          ),
        ],
      ),
    );
  }
}
