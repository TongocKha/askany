import 'package:askany/src/ui/common/widgets/custom_dropdown_button/dropdown_button_2.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class RequestDropdownButton extends StatelessWidget {
  const RequestDropdownButton(
      {Key? key,
      required String hintText,
      required List<String> items,
      required Function(String?)? onChanged,
      this.centerTextItem,
      this.buttonHeight,
      this.validatorDropDown})
      : _hintText = hintText,
        _items = items,
        _onChanged = onChanged,
        super(key: key);
  final bool? centerTextItem;
  final String _hintText;
  final List<String> _items;
  final Function(String?)? _onChanged;
  final String? Function(String?)? validatorDropDown;
  final double? buttonHeight;

  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField2(
      buttonHeight: buttonHeight ?? 36.sp,
      dropdownElevation: 0,
      scrollbarAlwaysShow: true,
      offset: Offset(0, -3.sp),
      icon: Image.asset(iconArrowDown, width: 10.sp),
      buttonWidth: 100.w,
      dropdownMaxHeight: 150.sp,
      decoration: InputDecoration(
        border: InputBorder.none,
        contentPadding: EdgeInsets.zero,
      ),
      buttonDecoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(6.5.sp),
        border: Border.all(
          color: colorBorderTextField,
          width: 0.5.sp,
        ),
      ),
      buttonPadding: EdgeInsets.symmetric(horizontal: 10.sp),
      dropdownPadding: EdgeInsets.zero,
      dropdownDecoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: colorBorderTextField),
        borderRadius: BorderRadius.circular(8.sp),
      ),
      validator: validatorDropDown,
      hint: Text(
        _hintText,
        style: TextStyle(
          color: colorBlack2,
          fontSize: 12.sp,
        ),
      ),
      items: _items
          .map(
            (value) => DropdownMenuItem<String>(
              value: value,
              child: centerTextItem != null
                  ? Center(
                      child: Text(
                        value,
                        style: TextStyle(
                          color: colorBlack2,
                          fontSize: 12.sp,
                        ),
                      ),
                    )
                  : Text(
                      value,
                      style: TextStyle(
                        color: colorBlack2,
                        fontSize: 12.sp,
                      ),
                    ),
            ),
          )
          .toList(),
      onChanged: _onChanged,
    );
  }
}
