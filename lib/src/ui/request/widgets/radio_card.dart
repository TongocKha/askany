import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class RadioCard<T> extends StatelessWidget {
  const RadioCard({
    Key? key,
    required this.paymentMethod,
    required this.value,
    required this.onChanged,
    required this.title,
    this.subTitle,
    this.disable = false,
    this.onDisableTap,
  }) : super(key: key);

  final T? paymentMethod;
  final T value;
  final Function(T) onChanged;
  final String title;
  final String? subTitle;
  final bool disable;
  final Function(T)? onDisableTap;

  @override
  Widget build(BuildContext context) {
    return TouchableOpacity(
      onTap: () {
        if (!disable) {
          onChanged(value);
        } else {
          if (onDisableTap != null) {
            onDisableTap!(value);
          }
        }
      },
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 10.sp, horizontal: 14.sp),
        decoration: BoxDecoration(
          color: colorWhite,
          border: Border.all(
            color: paymentMethod == value ? colorGreen2 : colorGray2,
            width: .5.sp,
          ),
          borderRadius: BorderRadius.circular(10.sp),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 16.sp,
              width: 16.sp,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                  width: 1.sp,
                  color: disable
                      ? colorGray2
                      : paymentMethod == value
                          ? colorGreen2
                          : colorGray1,
                ),
              ),
              child: Visibility(
                visible: paymentMethod == value,
                child: Container(
                  height: 8.sp,
                  width: 8.sp,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: colorGreen2,
                  ),
                ),
              ),
            ),
            SizedBox(width: 10.sp),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  style: TextStyle(
                    fontSize: 13.sp,
                    color: disable ? colorGray2 : colorBlack1,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                SizedBox(height: 2.sp),
                subTitle != null
                    ? Text(
                        subTitle!,
                        style: TextStyle(
                          fontSize: 11.sp,
                          color: disable ? colorGray2 : colorGray1,
                          fontWeight: FontWeight.w400,
                          height: 1.57,
                        ),
                      )
                    : SizedBox(),
              ],
            )
          ],
        ),
      ),
    );
  }
}
