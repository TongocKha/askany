import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/request/request_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/models/request_model.dart';
import 'package:askany/src/models/slide_mode.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_confirm_cancel.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:askany/src/ui/common/widgets/text_ui/text_ui.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class RequestDetailsCard extends StatelessWidget {
  final RequestModel _requestModel;
  final bool _isCommonRequest;
  const RequestDetailsCard(this._requestModel, this._isCommonRequest);
  String _buildOfferCount() {
    if (_requestModel.offerers.length + _requestModel.selectedOffers.length >
        1) {
      return '${Strings.thereAre.i18n}' +
          ' ${_requestModel.offerers.length + _requestModel.selectedOffers.length} ' +
          Strings.offers.i18n.toLowerCase();
    }
    return '${Strings.thereIs.i18n}' +
        ' ${_requestModel.offerers.length + _requestModel.selectedOffers.length} ' +
        Strings.offer.i18n.toLowerCase();
  }

  @override
  Widget build(BuildContext context) {
    final bool _isExpert = AppBloc.userBloc.getAccount.isExpert ?? false;
    final bool _isChosen = _requestModel.selectedOffers.indexWhere(
          (element) =>
              element.authorExpert.id == AppBloc.userBloc.getAccount.id! &&
              element.status >= OFFER_CHOSEN,
        ) !=
        -1;
    final bool _isRequestDone = _requestModel.status == REQUEST_FINISHED;
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 16.sp,
        vertical: 4.sp,
      ),
      width: 100.w,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: TextUI(
                  _requestModel.title,
                  fontSize: 21.sp,
                  fontWeight: FontWeight.w700,
                  color: colorBlack1,
                ),
              ),
              SizedBox(width: 12.sp),
              UserLocal().getAccessToken() != '' &&
                      !AppBloc.userBloc.getAccount.isExpert! &&
                      _requestModel.status == REQUEST_HAPPENING &&
                      !_isCommonRequest
                  ? TouchableOpacity(
                      onTap: () {
                        if (_requestModel.offerers.isEmpty &&
                            _requestModel.selectedOffers.isEmpty) {
                          AppNavigator.push(Routes.CREATE_REQUEST, arguments: {
                            'requestModel': _requestModel,
                          });
                        } else {
                          dialogAnimationWrapper(
                            slideFrom: SlideMode.bot,
                            borderRadius: 10.sp,
                            child: DialogConfirmCancel(
                              bodyBefore: Strings.requestHasBidNoti.i18n,
                              confirmText: Strings.delete.i18n,
                              cancelText: Strings.cancel.i18n,
                              onConfirmed: () {
                                showDialogLoading();
                                AppBloc.requestBloc.add(
                                  DeleteRequestEvent(
                                      requestId: _requestModel.id!),
                                );
                              },
                            ),
                          );
                        }
                      },
                      child: Container(
                        height: 30.sp,
                        width: 24.sp,
                        color: Colors.transparent,
                        alignment: Alignment.center,
                        child: Image.asset(
                          'assets/icons/ic_pen_edit2.png',
                          width: 12.5.sp,
                        ),
                      ),
                    )
                  : SizedBox(),
            ],
          ),
          SizedBox(height: 12.sp),
          TextUI(
            _requestModel.content,
            fontSize: 13.sp,
            color: colorBlack2,
            fontWeight: FontWeight.w400,
            textAlign: TextAlign.justify,
            height: 1.7,
          ),
          SizedBox(height: 12.sp),
          dividerColorGrey2,
          SizedBox(height: 18.sp),
          TextUI(
            Strings.deepInfor.i18n,
            fontSize: 14.sp,
            fontWeight: FontWeight.w700,
            color: colorBlack1,
          ),
          SizedBox(height: 12.sp),
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: 20.sp,
              vertical: 6.sp,
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8.sp),
              border: Border.all(
                color: getColorByStatus(
                  request: _requestModel,
                  isExpert: _isExpert,
                  isExpertChosen: _isChosen,
                  isCommonRequest: _isCommonRequest,
                ),
                width: 1.0,
              ),
            ),
            child: buildRequestStatus(
              request: _requestModel,
              isExpert: _isExpert,
              isExpertChosen: _isChosen,
              isCommonRequest: _isCommonRequest,
            ),
          ),
          SizedBox(height: 14.sp),
          Container(
            width: double.infinity,
            padding: EdgeInsets.all(10.sp),
            decoration: BoxDecoration(
              color: colorAddButton,
            ),
            child: TextUI(
              _requestModel.offerers.isEmpty &&
                      _requestModel.selectedOffers.isEmpty
                  ? Strings.noOffersNoti.i18n
                  : _buildOfferCount(),
              color: colorFontGreen,
              fontWeight: FontWeight.w600,
              fontSize: 12.5.sp,
            ),
          ),
          SizedBox(height: 15),
          TextUI(
            '${Strings.expirationDate.i18n}: ${DateFormat("dd/MM/yyyy").format(_requestModel.dateExpired)}',
            fontSize: 12.5.sp,
            color: colorBlack2,
          ),
          SizedBox(height: 12.sp),
          Container(
            child: Row(
              children: [
                TextUI(
                  '${Strings.contactForm.i18n}: ${_getStringContactBy()}',
                  fontSize: 12.5.sp,
                  color: colorBlack2,
                ),
              ],
            ),
          ),
          SizedBox(height: 12.sp),
          TextUI(
            '${Strings.participants.i18n}: ${_requestModel.participantsCount}',
            fontSize: 12.5.sp,
            color: colorBlack2,
          ),
          SizedBox(height: 12.sp),
          Visibility(
            visible: _requestModel.contactForm == CONTACT_MEET,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextUI(
                  '${Strings.place.i18n}: ${_requestModel.locationAddress}',
                  fontSize: 12.5.sp,
                  color: colorBlack2,
                ),
                SizedBox(height: 10.sp),
              ],
            ),
          ),
          TextUI(
            '${Strings.budget.i18n}: ${_requestModel.budget!.costString}',
            fontSize: 12.5.sp,
            color: colorBlack2,
          ),
          SizedBox(height: 16.sp),
          UserLocal().getAccessToken() != '' &&
                  !_isExpert &&
                  !_isRequestDone &&
                  !_isCommonRequest
              ? ButtonPrimary(
                  onPressed: () {
                    dialogAnimationWrapper(
                      slideFrom: SlideMode.bot,
                      child: DialogConfirmCancel(
                        onConfirmed: () {
                          AppBloc.requestBloc.add(
                            UserEndRequestEvent(requestId: _requestModel.id!),
                          );
                        },
                        title: Strings.endRequest.i18n,
                        bodyBefore: Strings.youDecidedToEndRequest.i18n,
                        cancelText: Strings.cancel.i18n,
                      ),
                    );
                  },
                  text: Strings.endRequest.i18n,
                )
              : SizedBox(),
          UserLocal().getAccessToken() != '' && !_isExpert && !_isRequestDone
              ? SizedBox(height: 20.sp)
              : SizedBox(),
        ],
      ),
    );
  }

  String _getStringContactBy() {
    switch (_requestModel.contactForm) {
      case 'call':
        return Strings.makeACall.i18n;
      case 'meet':
        return Strings.makeAAppointment.i18n;
      default:
        return Strings.notUpdated.i18n;
    }
  }
}
