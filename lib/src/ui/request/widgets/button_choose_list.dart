import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ButtonChooseList extends StatelessWidget {
  final bool isChoose;
  final Function onChanged;
  final String title;
  const ButtonChooseList({
    required this.isChoose,
    required this.onChanged,
    required this.title,
  });

  @override
  Widget build(BuildContext context) {
    return TouchableOpacity(
      onTap: () {
        onChanged();
      },
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: 12.sp,
          vertical: 7.sp,
        ),
        decoration: BoxDecoration(
          color: isChoose ? colorGreen3 : Colors.transparent,
          borderRadius: BorderRadius.all(
            Radius.circular(47.sp),
          ),
        ),
        child: Text(
          title,
          style: TextStyle(
            fontSize: 11.sp,
            color: isChoose ? colorWhiteCard : colorGray1,
          ),
        ),
      ),
    );
  }
}
