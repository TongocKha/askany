import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/models/request_model.dart';
import 'package:askany/src/ui/common/widgets/text_ui/text_ui.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class RequestCommonCard extends StatelessWidget {
  final RequestModel request;

  RequestCommonCard({
    required this.request,
  });

  @override
  Widget build(BuildContext context) {
    final bool _isExpert = AppBloc.userBloc.getAccount.isExpert ?? false;

    return Container(
      padding: EdgeInsets.all(16.sp),
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 2.sp),
          Row(
            children: [
              Expanded(
                child: Text(
                  request.title,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 13.sp,
                    color: colorBlack1,
                  ),
                ),
              ),
              SizedBox(width: 12.w),
              TextUI(
                Strings.beingInprogress.i18n,
                color: colorGreen2,
                fontWeight: FontWeight.w600,
                fontSize: 11.sp,
              ),
            ],
          ),
          SizedBox(height: 10.sp),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Image.asset(
                    iconDot,
                    width: 4.5.sp,
                  ),
                  SizedBox(width: 2.5.sp),
                  TextUI(
                    '${Strings.budget.i18n}: ${request.budget!.costString}',
                    fontSize: 10.5.sp,
                    fontWeight: FontWeight.w600,
                    color: colorBlack2,
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Image.asset(
                    iconDot,
                    width: 4.5.sp,
                  ),
                  SizedBox(width: 2.5.sp),
                  TextUI(
                    '${Strings.expirationTime.i18n}: ${DateFormat("dd/MM/yyyy").format(request.dateExpired)}',
                    fontSize: 10.5.sp,
                    fontWeight: FontWeight.w600,
                    color: colorBlack2,
                  ),
                ],
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 5.sp),
            child: Text(
              '${Strings.customer.i18n}: ${request.authorUser!.fullname}',
              style: TextStyle(
                fontWeight: FontWeight.w400,
                color: colorBlack2,
                fontSize: 11.sp,
                height: 1.57,
              ),
            ),
          ),
          RichText(
            text: TextSpan(
              style: TextStyle(
                fontSize: 11.sp,
                height: 1.57,
              ),
              children: [
                TextSpan(
                  text: request.offerers.isEmpty &&
                          request.selectedOffers.isEmpty
                      ? Strings.noOffersNoti.i18n
                      : '${request.offerers.length + request.selectedOffers.length} ${Strings.offers.i18n}',
                  style: TextStyle(
                    fontWeight: FontWeight.w400,
                    color: colorBlack2,
                  ),
                ),
                _isExpert && _isExpertChosen()
                    ? TextSpan(
                        text: ' | ${Strings.selected.i18n}',
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          color: colorChosen,
                        ),
                      )
                    : TextSpan(),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              top: 14.sp,
              bottom: 10.sp,
            ),
            child: dividerColorGrey2,
          ),
          Container(
            child: Text(
              request.content,
              textAlign: TextAlign.justify,
              maxLines: 3,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                color: colorGray1,
                fontSize: 11.sp,
                height: 1.25.sp,
              ),
            ),
          ),
        ],
      ),
    );
  }

  bool _isExpertChosen() {
    if (request.selectedOffers.isNotEmpty) {
      return request.selectedOffers.indexWhere((element) =>
              element.authorExpert.id == AppBloc.userBloc.getAccount.id) !=
          -1;
    } else {
      return false;
    }
  }
}
