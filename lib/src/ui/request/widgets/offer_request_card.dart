import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/request/request_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/constants/provinces.dart';
import 'package:askany/src/models/offer_model.dart';
import 'package:askany/src/models/request_model.dart';
import 'package:askany/src/models/slide_mode.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/custom_image/network_image/cached_image.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_confirm_cancel.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:askany/src/ui/common/widgets/text_ui/read_more.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:askany/src/ui/request/widgets/rating_card.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:askany/src/helpers/extentions/string_extention.dart';

class OfferRequestCard extends StatefulWidget {
  final OfferModel offer;
  final RequestModel request;
  OfferRequestCard({
    required this.request,
    required this.offer,
  });
  @override
  State<StatefulWidget> createState() => _OfferRequestCardState();
}

class _OfferRequestCardState extends State<OfferRequestCard> {
  @override
  Widget build(BuildContext context) {
    final bool _isChosen = widget.offer.status >= OFFER_CHOSEN &&
        widget.request.status == REQUEST_HAPPENING;
    final bool _canStillPickOffer = widget.request.status != REQUEST_FINISHED;
    return Container(
      padding: EdgeInsets.fromLTRB(0.02 * 100.w + 14.sp, 20.sp, 10.sp, 20.sp),
      margin: EdgeInsets.only(bottom: 16.sp),
      decoration: BoxDecoration(
        color: _isChosen || _canStillPickOffer
            ? Colors.white
            : backgroundTimelineDone,
        borderRadius: BorderRadius.circular(8.sp),
        gradient: _isChosen
            ? LinearGradient(
                stops: [0.02, 0.02],
                colors: [
                  headerCalendarColor,
                  Colors.white,
                ],
              )
            : null,
        boxShadow: _isChosen || _canStillPickOffer
            ? [
                BoxShadow(
                  color: Colors.black12,
                  spreadRadius: 0.5,
                  blurRadius: 4.0,
                  offset: Offset(1.0, 1.0),
                ),
              ]
            : null,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: _ExpertNameAndAvatar(
                  offer: widget.offer,
                ),
              ),
              _canStillPickOffer || _isChosen
                  ? _OfferStatusTag(
                      isChosen: _isChosen,
                      request: widget.request,
                      offer: widget.offer,
                    )
                  : SizedBox(),
            ],
          ),
          SizedBox(height: 14.sp),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                children: [
                  Image.asset(
                    iconLocation,
                    width: 7.5.sp,
                  ),
                  SizedBox(width: 4.sp),
                  Text(
                    Province()
                        .getProvinceByCode(widget.offer.authorExpert.province)!,
                    maxLines: 1,
                    style: TextStyle(
                      fontSize: 11.sp,
                      fontWeight: FontWeight.w400,
                      color: colorGray1,
                    ),
                  ),
                ],
              ),
              SizedBox(width: 16.sp),
              Expanded(
                child: Row(
                  children: [
                    Image.asset(
                      iconDot,
                      width: 4.sp,
                    ),
                    SizedBox(width: 4.sp),
                    Expanded(
                      child: Text(
                        '${Strings.anExpertIn.i18n} ${widget.request.specialtyModel?.getLocaleTitle?.toLowerCase()}',
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontSize: 12.sp,
                          fontWeight: FontWeight.w400,
                          color: colorGray1,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(height: 14.sp),
          dividerChat,
          SizedBox(height: 14.sp),
          Text(
            Strings.offerContent.i18n,
            style: TextStyle(
              fontSize: 12.5.sp,
              fontWeight: FontWeight.w600,
              color: colorBlack1,
            ),
          ),
          SizedBox(height: 8.sp),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                '${Strings.offerCost.i18n}: ${widget.offer.price.costString}',
                maxLines: 1,
                style: TextStyle(
                  fontSize: 11.5.sp,
                  fontWeight: FontWeight.w600,
                  color: colorBlack1,
                ),
              ),
              SizedBox(width: 16.sp),
              Expanded(
                child: Text(
                  '${Strings.adviseDuration.i18n}: ${widget.offer.price.totalMinutes} ${Strings.minutes.i18n}',
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: 11.5.sp,
                    fontWeight: FontWeight.w600,
                    color: colorBlack1,
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: 8.sp),
          ReadMoreText(
            widget.offer.content,
            textAlign: TextAlign.justify,
            trimLines: 4,
            colorClickableText: headerCalendarColor,
            trimMode: TrimMode.Line,
            trimCollapsedText: Strings.showMore.i18n,
            trimExpandedText: Strings.showLess.i18n,
            style: TextStyle(
              fontSize: 11.5.sp,
              fontWeight: FontWeight.w400,
              color: colorBlack2,
              height: 1.15.sp,
            ),
          ),
        ],
      ),
    );
  }
}

class _OfferStatusTag extends StatelessWidget {
  const _OfferStatusTag({
    Key? key,
    required this.isChosen,
    this.offer,
    this.request,
  }) : super(key: key);
  final RequestModel? request;
  final OfferModel? offer;
  final bool isChosen;
  @override
  Widget build(BuildContext context) {
    return TouchableOpacity(
      onTap: () {
        if (offer != null && offer!.status < OFFER_CHOSEN) {
          dialogAnimationWrapper(
            slideFrom: SlideMode.bot,
            borderRadius: 8.5.sp,
            paddingHorizontal: 18.sp,
            child: DialogConfirmCancel(
              title: Strings.chooseExpert.i18n,
              bodyBefore: Strings.yourDecidedToChoose.i18n,
              highlightText: offer!.authorExpert.fullname,
              bodyAfter: Strings.forThisRequest.i18n,
              bodyColor: colorBlack2,
              cancelText: Strings.cancel.i18n,
              onConfirmed: () {
                AppBloc.requestBloc.add(
                  UserChooseOfferEvent(
                    requestId: request!.id!,
                    offerId: offer!.id,
                  ),
                );
                AppNavigator.pop();
                AppNavigator.push(
                  Routes.ORDER_SERVICE,
                  arguments: {
                    'request': request,
                    'offer': offer,
                    'contactForm': request!.contactForm,
                  },
                );
              },
            ),
          );
        }
        if (offer!.status == OFFER_CHOSEN) {
          AppBloc.requestBloc.add(
            ContinuePaymentOfferEvent(request: request!, offer: offer!),
          );
        }
      },
      child: Container(
        padding: EdgeInsets.symmetric(
          vertical: 6.5.sp,
          horizontal: 16.sp,
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(6.5.sp),
          color: isChosen ? Colors.transparent : colorFontGreen,
          border: Border.all(
            color: colorFontGreen,
            width: 1,
          ),
        ),
        child: Text(
          isChosen
              ? (offer!.status == OFFER_CHOSEN
                  ? Strings.payment.i18n
                  : Strings.selected.i18n)
              : Strings.select.i18n,
          style: TextStyle(
            fontSize: 11.sp,
            fontWeight: FontWeight.w600,
            color: isChosen ? colorFontGreen : colorWhiteCard,
          ),
        ),
      ),
    );
  }
}

class _ExpertNameAndAvatar extends StatelessWidget {
  const _ExpertNameAndAvatar({
    Key? key,
    required this.offer,
  }) : super(key: key);
  final OfferModel offer;
  @override
  Widget build(BuildContext context) {
    return TouchableOpacity(
      onTap: () {
        AppNavigator.push(
          Routes.DETAILS_SPECIALIST,
          arguments: {
            'expertId': offer.authorExpert.id,
          },
        );
      },
      child: Container(
        color: Colors.transparent,
        child: Row(
          children: [
            CustomNetworkImage(
              width: 36.sp,
              urlToImage: offer.authorExpert.avatar?.urlToImage,
            ),
            SizedBox(width: 6.sp),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  offer.authorExpert.fullname.formatName(limit: 12),
                  style: TextStyle(
                    fontSize: 12.5.sp,
                    fontWeight: FontWeight.w600,
                    color: colorBlack1,
                  ),
                ),
                SizedBox(height: 4.sp),
                offer.authorExpert.ratingCounts == 0
                    ? Text(
                        Strings.noReviews.i18n,
                        style: TextStyle(
                          color: colorGray1,
                          fontSize: 11.sp,
                        ),
                      )
                    : RatingCard(rating: offer.authorExpert.stars),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
