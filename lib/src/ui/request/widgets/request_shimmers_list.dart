import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/ui/request/widgets/request_shimmer_card.dart';
import 'package:flutter/material.dart';

class RequestsShimmersList extends StatelessWidget {
  const RequestsShimmersList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      itemCount: ITEM_COUNT_SHIMMER,
      itemBuilder: (context, index) => RequestShimmerCard(index: index),
    );
  }
}
