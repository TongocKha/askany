import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/request/request_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/helpers/currency_helper.dart';
import 'package:askany/src/models/category_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class FilterRequestScreen extends StatefulWidget {
  FilterRequestScreen({Key? key}) : super(key: key);

  @override
  State<FilterRequestScreen> createState() => _FilterRequestScreenState();
}

class _FilterRequestScreenState extends State<FilterRequestScreen> {
  TextEditingController searchKeyController = TextEditingController();
  String minMoney = MIN_MONEY;
  String maxMoney = MAX_MONEY;
  TextEditingController _minMoneyController = TextEditingController();
  TextEditingController _maxMoneyController = TextEditingController();

  @override
  void initState() {
    super.initState();
    if (AppBloc.requestBloc.searchKey.isNotEmpty) {
      searchKeyController.text = AppBloc.requestBloc.searchKey;
    }
    minMoney = AppBloc.requestBloc.cost.split('-')[0];
    maxMoney = AppBloc.requestBloc.cost.split('-')[1];
    if (minMoney.length > 2) {
      var value = minMoney;
      value = value.replaceAll(RegExp(r'\D'), '');
      value = value.replaceAll(RegExp(r'\B(?=(\d{3})+(?!\d))'), ',');
      _minMoneyController.text = minMoney == MIN_MONEY ? '' : value;
    }

    if (maxMoney.length > 2) {
      var value = maxMoney;
      value = value.replaceAll(RegExp(r'\D'), '');
      value = value.replaceAll(RegExp(r'\B(?=(\d{3})+(?!\d))'), ',');
      _maxMoneyController.text = maxMoney == MAX_MONEY ? '' : maxMoney;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        bottom: false,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 16.sp),
              width: 100.w,
              height: 50.sp,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: TouchableOpacity(
                      onTap: () {
                        searchKeyController.text = '';
                        minMoney = MIN_MONEY;
                        maxMoney = MAX_MONEY;
                        _minMoneyController.text = '';
                        _maxMoneyController.text = '';
                        AppBloc.requestBloc.add(FilterRequestEvent(
                          searchKey: '',
                          cost: '$minMoney - $maxMoney',
                        ));
                      },
                      child: Container(
                        color: Colors.transparent,
                        child: Text(
                          Strings.clearAll.i18n,
                          style: TextStyle(
                            color: colorGray1,
                            fontSize: 13.sp,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      alignment: Alignment.center,
                      child: Text(
                        Strings.filter.i18n,
                        style: TextStyle(
                          color: colorBlack2,
                          fontSize: 15.sp,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: TouchableOpacity(
                      onTap: () {
                        AppNavigator.pop();
                      },
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Container(
                          color: Colors.transparent,
                          child: Image.asset(
                            iconRemove,
                            width: 13.sp,
                            height: 13.sp,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            dividerChat,
            SizedBox(height: 20.sp),
            Expanded(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 16.sp),
                child: Column(
                  children: [
                    _textTitle(
                      title: Strings.searchRequest.i18n,
                    ),
                    SizedBox(height: 10.sp),
                    Container(
                      height: 33.sp,
                      decoration: BoxDecoration(
                        border: Border.all(color: colorGrayBorder, width: .5.sp),
                        borderRadius: BorderRadius.circular(8.sp),
                      ),
                      child: TextFormField(
                        controller: searchKeyController,
                        style: TextStyle(
                          color: colorCaptionSearch,
                          fontSize: 12.sp,
                        ),
                        keyboardType: TextInputType.multiline,
                        maxLines: 1,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.only(
                            top: 0.sp,
                            right: 10.sp,
                          ),
                          hintText: Strings.searchHintext.i18n,
                          hintStyle: TextStyle(
                            color: colorCaptionSearch,
                            fontSize: 12.sp,
                            fontWeight: FontWeight.w400,
                          ),
                          filled: true,
                          fillColor: Colors.transparent,
                          border: OutlineInputBorder(
                            borderSide: BorderSide.none,
                          ),
                          prefixIcon: Container(
                            margin: EdgeInsets.all(10.sp),
                            child: Image.asset(
                              iconSearch,
                            ),
                          ),
                          suffixIcon: IconButton(
                            onPressed: () {
                              searchKeyController.text = '';
                            },
                            icon: Icon(
                              Icons.close,
                              color: Color(0xFFA4A4A4),
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 17.sp),
                    _textTitle(title: Strings.price.i18n),
                    SizedBox(height: 10.sp),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                          child: _textFieldInputMoneyCard(
                            controller: _minMoneyController,
                            onChanged: (val) {
                              setState(
                                () {
                                  minMoney = val.replaceAll(',', '');
                                },
                              );
                            },
                          ),
                        ),
                        Container(
                          alignment: Alignment.center,
                          width: 18.sp,
                          child: Text(
                            ' - ',
                            style: TextStyle(
                              color: colorBlack2,
                              fontSize: 14.sp,
                            ),
                          ),
                        ),
                        Expanded(
                          child: _textFieldInputMoneyCard(
                            controller: _maxMoneyController,
                            onChanged: (val) {
                              setState(
                                () {
                                  maxMoney = val.replaceAll(',', '');
                                },
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            dividerChat,
            SizedBox(height: 7.sp),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 16.sp),
              margin: EdgeInsets.only(bottom: 18.sp),
              child: ButtonPrimary(
                onPressed: () {
                  CategoryModel? category = AppBloc.requestBloc.category;
                  AppBloc.requestBloc.add(
                    FilterRequestEvent(
                      searchKey: searchKeyController.text,
                      cost: minMoney + '-' + maxMoney,
                      category: category != null ? category : null,
                    ),
                  );
                  AppNavigator.pop();
                },
                text: Strings.filter.i18n,
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _textTitle({required String title}) {
    return Container(
      alignment: Alignment.centerLeft,
      child: Text(
        title,
        style: TextStyle(
          color: colorBlack2,
          fontSize: 13.sp,
          fontWeight: FontWeight.w600,
        ),
      ),
    );
  }

  Widget _textFieldInputMoneyCard({
    required Function(String)? onChanged,
    required TextEditingController controller,
  }) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 9.sp,
        vertical: 0.sp,
      ),
      height: 34.sp,
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(
          color: colorBorderTextField,
          width: 0.5.sp,
        ),
        borderRadius: BorderRadius.all(
          Radius.circular(
            5.sp,
          ),
        ),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            Strings.currency.i18n,
            style: TextStyle(
              color: colorGray2,
              fontSize: 12.sp,
            ),
          ),
          SizedBox(width: 5.sp),
          Expanded(
            child: Container(
              margin: EdgeInsets.only(bottom: 6.5.sp),
              child: TextFormField(
                controller: controller,
                keyboardType: TextInputType.number,
                cursorColor: headerCalendarColor,
                inputFormatters: CurrencyHelper.vndInputFormat,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 12.sp,
                ),
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintStyle: TextStyle(
                    color: colorGray2,
                    fontSize: 12.sp,
                  ),
                ),
                onChanged: onChanged,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
