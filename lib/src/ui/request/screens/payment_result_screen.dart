import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/home/home_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class PaymentResultScreen extends StatelessWidget {
  const PaymentResultScreen({
    Key? key,
    required this.isSuccess,
    this.isCheckChatScreen = false,
  }) : super(key: key);

  final bool isSuccess;
  final bool isCheckChatScreen;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        appBar: AppBar(
          systemOverlayStyle: SystemUiOverlayStyle(
            statusBarBrightness: Theme.of(context).brightness,
          ),
          elevation: 1,
          backgroundColor: Colors.white,
          automaticallyImplyLeading: false,
          centerTitle: true,
          title: Text(
            Strings.paymentResult.i18n,
            style: TextStyle(
              fontSize: 15.sp,
              fontWeight: FontWeight.w700,
              color: colorBlack2,
            ),
          ),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 26.sp),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      imagePaymentSuccess,
                      height: 82.sp,
                      width: 125.sp,
                    ),
                    SizedBox(height: 4.93.h),
                    Text(
                      isSuccess ? Strings.paymentSuccess.i18n : Strings.paymentFailure.i18n,
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 17.sp,
                        height: 1.364,
                        color: colorBlack1,
                      ),
                    ),
                    SizedBox(height: 2.46.h),
                    Text(
                      isSuccess ? Strings.paymentSuccessNoti.i18n : Strings.paymentFailureNoti.i18n,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 12.sp,
                        height: 1.6,
                        color: colorBlack2,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 28.sp, left: 16.sp, right: 16.sp),
              child: ButtonPrimary(
                onPressed: () {
                  if (isCheckChatScreen) {
                    AppNavigator.popUntil(Routes.CONVERSATION);
                  } else {
                    AppBloc.homeBloc.add(OnChangeIndexEvent());
                    AppNavigator.pushNamedAndRemoveUntil(Routes.ROOT);
                  }
                },
                text: Strings.backToHomePage.i18n,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
