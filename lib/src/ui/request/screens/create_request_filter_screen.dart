import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/category/category_bloc.dart';
import 'package:askany/src/bloc/request/request_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/models/category_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';

class CreateRequestFilterScreen extends StatelessWidget {
  const CreateRequestFilterScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarTitleBack(
        context,
        Strings.category.i18n,
        leading: Container(),
        actions: [
          TouchableOpacity(
            onTap: () {
              AppNavigator.pop();
            },
            child: Image.asset(
              iconRemove,
              width: 14.sp,
              height: 14.sp,
            ),
          ),
          SizedBox(
            width: 17.sp,
          )
        ],
      ),
      body: Column(
        children: [
          Divider(),
          SizedBox(
            height: 20.sp,
          ),
          Expanded(
            child: BlocBuilder<CategoryBloc, CategoryState>(
              builder: (context, state) {
                if (state is GetDoneCategoryInfo) {
                  return Container(
                    padding: EdgeInsets.only(left: 18.sp),
                    child: ListView.builder(
                      physics: BouncingScrollPhysics(),
                      itemCount: state.categories.length + 1,
                      itemBuilder: (context, index) {
                        if (index == 0) {
                          return TouchableOpacity(
                            onTap: () {
                              AppBloc.requestBloc
                                  .add(CleanFilterRequestEvent());
                              AppNavigator.pop();
                            },
                            child: Container(
                              alignment: Alignment.centerLeft,
                              height: 40.sp,
                              color: colorWhite,
                              child: Row(
                                children: [
                                  Text(
                                    Strings.all.i18n,
                                    style: text13Grey1Weight400,
                                  ),
                                ],
                              ),
                            ),
                          );
                        }
                        return RequestFilterItem(
                          content:
                              state.categories[index - 1].categoryInfo.name,
                          listContent: state.categories[index - 1].listSubCates,
                        );
                      },
                    ),
                  );
                }
                return Container();
              },
            ),
          )
        ],
      ),
    );
  }
}

class RequestFilterItem extends StatefulWidget {
  final List<CategoryModel> listContent;
  final String content;
  const RequestFilterItem({
    Key? key,
    required this.listContent,
    required this.content,
  }) : super(key: key);

  @override
  _RequestFilterItemState createState() => _RequestFilterItemState();
}

class _RequestFilterItemState extends State<RequestFilterItem> {
  bool isShow = false;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TouchableOpacity(
          onTap: () {
            setState(() {
              isShow = !isShow;
            });
          },
          child: Container(
            height: 40.sp,
            color: colorWhite,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  widget.content,
                  style: text13Grey1Weight400,
                ),
                Spacer(),
                Container(
                  height: 40.sp,
                  width: 50.sp,
                  child: isShow
                      ? Center(
                          child: Image.asset(
                            iconArrowUp2,
                            height: 28.sp,
                            width: 12.sp,
                          ),
                        )
                      : Center(
                          child: Image.asset(
                            iconArrowDown,
                            height: 28.sp,
                            width: 12.sp,
                          ),
                        ),
                )
              ],
            ),
          ),
        ),
        Visibility(
          visible: isShow,
          child: Container(
            height: widget.listContent.length * 40.sp,
            padding: EdgeInsets.only(
              left: 18.sp,
            ),
            width: double.infinity,
            child: ListView.builder(
                shrinkWrap: true,
                itemCount: widget.listContent.length,
                physics: BouncingScrollPhysics(),
                itemBuilder: (context, index) {
                  return TouchableOpacity(
                    onTap: () {
                      String searchKey = AppBloc.requestBloc.searchKey;
                      String cost = AppBloc.requestBloc.cost;

                      AppBloc.requestBloc.add(
                        FilterRequestEvent(
                          category: widget.listContent[index],
                          cost: cost,
                          searchKey: searchKey,
                        ),
                      );
                      AppNavigator.pop();
                    },
                    child: Container(
                      color: colorWhite,
                      alignment: Alignment.centerLeft,
                      height: 40.sp,
                      child: Text(
                        ' ${widget.listContent[index].name}',
                        style: text13Grey1Weight400,
                      ),
                    ),
                  );
                }),
          ),
        )
      ],
    );
  }
}
