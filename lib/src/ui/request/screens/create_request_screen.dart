import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/request/request_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/helpers/currency_helper.dart';
import 'package:askany/src/models/budget_model.dart';
import 'package:askany/src/models/request_model.dart';
import 'package:askany/src/models/slide_mode.dart';
import 'package:askany/src/models/specialty_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/scaffold_wrapper.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_confirm_cancel.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_form.dart';
import 'package:askany/src/ui/common/widgets/text_field/text_field_form_request.dart';
import 'package:askany/src/ui/request/widgets/bottom_sheet_calendar.dart';
import 'package:askany/src/ui/request/widgets/bottom_sheet_string.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:askany/src/helpers/extentions/string_extention.dart';

class CreateRequestScreen extends StatefulWidget {
  final RequestModel? request;
  final bool isNavigationFromSearch;
  CreateRequestScreen(
      {required this.request, this.isNavigationFromSearch = false});
  @override
  _CreateRequestScreenState createState() => _CreateRequestScreenState();
}

class _CreateRequestScreenState extends State<CreateRequestScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _titleController = TextEditingController();
  TextEditingController _contentController = TextEditingController();
  TextEditingController _priceController = TextEditingController();
  TextEditingController _quantityController = TextEditingController();
  TextEditingController _locationNameController = TextEditingController();
  TextEditingController _locationAddressController = TextEditingController();
  TextEditingController _contactFormController = TextEditingController();
  TextEditingController _specialtyControler = TextEditingController();
  FocusNode _budgetFocus = FocusNode();
  bool _isCreating = true;
  String _title = '';
  String _content = '';
  String _currency = CURRENCY_VND;
  String _price = '';
  String _quantity = '';
  String _contactForm = 'meet';
  String _locationName = '';
  String _locationAddress = '';
  String _specialty = '';
  String? requestField;
  DateTime _selectedDate = DateTime.now().add(Duration(days: 3));
  List<String> specialtiesTitle = [];
  // String? _contactFormString;
  List<String> _contactForms = [Strings.call.i18n, Strings.meet.i18n];

  @override
  void initState() {
    super.initState();
    _isCreating = widget.request == null;
    _specialtyControler.text = Strings.sectorOfRequestAppbartext.i18n;
    // if (_isCreating) {
    //   _contactFormString = _contactForms[0];
    // }

    // Initial specialties
    List<SpecialtyModel> filterSpecialties = AppBloc.requestBloc.specialties
        .where((item) => item.getLocaleTitle != null)
        .toList();

    specialtiesTitle =
        filterSpecialties.map((item) => item.getLocaleTitle!).toSet().toList();

    // Initial value for edit
    if (widget.request != null) {
      int indexOfSpecialty = filterSpecialties
          .indexWhere((item) => item.id == widget.request!.specialty);

      if (indexOfSpecialty != -1) {
        _specialty = filterSpecialties[indexOfSpecialty].getLocaleTitle!;
      } else {
        _specialty = specialtiesTitle[0];
      }

      _currency = widget.request!.budget!.currency;

      _selectedDate = widget.request!.dateExpired;
      _titleController.text = widget.request!.title;
      _contentController.text = widget.request!.content;
      _priceController.text = widget.request!.budget!.costStringWithoutCurrency;
      _quantityController.text = widget.request!.participantsCount.toString();

      _title = widget.request!.title;
      _content = widget.request!.content;
      _price = widget.request!.budget!.costStringWithoutCurrency;
      _quantity = widget.request!.participantsCount.toString();
      _locationName = widget.request!.locationName == EMPTY_STRING
          ? ''
          : widget.request!.locationName;
      _locationAddress = widget.request!.locationAddress == EMPTY_STRING
          ? ''
          : widget.request!.locationAddress;

      _locationNameController.text = _locationName;
      _locationAddressController.text = _locationAddress;
      _contactForm = widget.request!.contactForm;

      _specialtyControler.text = _specialty;
    }

    _contactFormController.text =
        _contactForm == CONTACT_MEET ? _contactForms[1] : _contactForms[0];
  }

  Future<void> onBackPressed() {
    return dialogAnimationWrapper(
      slideFrom: SlideMode.bot,
      child: DialogConfirmCancel(
        cancelText: 'Huỷ',
        bodyBefore:
            'Bạn có chắc chắn muốn rời khỏi màn hình này, thông tin bạn nhập sẽ không được lưu lại?',
        onConfirmed: () {
          AppNavigator.pop();
          AppNavigator.pop();
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ScaffoldWrapper(
      child: Scaffold(
        appBar: appBarTitleBack(
          context,
          _isCreating
              ? Strings.createNewRequest.i18n
              : Strings.editRequestDetails.i18n,
          onBackPressed: onBackPressed,
        ),
        body: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Container(
            color: backgroundDetails,
            padding: EdgeInsets.only(top: 6.sp, bottom: 60.sp),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  Container(
                    color: Colors.white,
                    padding: EdgeInsets.all(16.sp),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Image.asset(
                              'assets/images/ic_form_request.png',
                              width: 20.sp,
                            ),
                            SizedBox(
                              width: 10.sp,
                            ),
                            Text(
                              Strings.request.i18n,
                              style: TextStyle(
                                color: colorFontGreen,
                                fontSize: 14.sp,
                                fontWeight: FontWeight.w600,
                              ),
                            )
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 15.sp),
                          child: Divider(
                            thickness: 0.5,
                            height: 0.5,
                            color: Color(0xffC5D0CF),
                          ),
                        ),
                        Text(
                          Strings.myRequest.i18n,
                          style: TextStyle(
                            fontSize: 12.sp,
                            fontWeight: FontWeight.w600,
                            color: colorBlack2,
                          ),
                        ),
                        TextFieldFormRequest(
                          controller: _titleController,
                          hintText: Strings.requestTitleHintext.i18n,
                          maxLines: 4,
                          validatorForm: (val) =>
                              val!.isEmpty || val.trim().length == 0
                                  ? Strings
                                      .requestTitleCannotBeLeftBlankErrortext
                                      .i18n
                                  : null,
                          onChanged: (val) => _title = val.trim(),
                        ),
                        SizedBox(
                          height: 20.sp,
                        ),
                        Text(
                          Strings.requestContent.i18n,
                          style: TextStyle(
                            fontSize: 12.sp,
                            fontWeight: FontWeight.w600,
                            color: colorBlack2,
                          ),
                        ),
                        TextFieldFormRequest(
                          controller: _contentController,
                          hintText: Strings.requestContent.i18n,
                          maxLines: 6,
                          validatorForm: (val) =>
                              val!.isEmpty || val.trim().length == 0
                                  ? Strings
                                      .requestContentCannotBeLeftBlankErrortext
                                      .i18n
                                  : null,
                          onChanged: (val) => _content = val.trim(),
                        ),
                        SizedBox(
                          height: 20.sp,
                        ),
                        Text(
                          Strings.sectorOfRequestAppbartext.i18n,
                          style: TextStyle(
                            fontSize: 12.sp,
                            fontWeight: FontWeight.w600,
                            color: colorBlack2,
                          ),
                        ),
                        SizedBox(height: 10.sp),
                        TouchableOpacity(
                          onTap: () {
                            showModalBottomSheet(
                              context: context,
                              backgroundColor: Colors.transparent,
                              isScrollControlled: true,
                              builder: (context) {
                                return BottomSheetString(
                                  listString: specialtiesTitle,
                                  sizeBottomSheet: 80.h,
                                  title: Strings.sectorOfRequestAppbartext.i18n,
                                  handlePressed: (val) {
                                    setState(() {
                                      _specialty = val;
                                    });
                                    _specialtyControler.text = val;
                                  },
                                );
                              },
                            );
                            FocusScope.of(context).requestFocus(_budgetFocus);
                          },
                          child: TextFieldForm(
                            controller: _specialtyControler,
                            autovalidateMode: AutovalidateMode.always,
                            isActive: false,
                            contentPadding: EdgeInsets.fromLTRB(
                              10.sp,
                              20.sp,
                              10.sp,
                              0.sp,
                            ),
                            fontSize: 13.sp,
                            suffixIcon: TouchableOpacity(
                              onTap: () {},
                              child: Image.asset(
                                iconArrowDown,
                                width: 10.sp,
                              ),
                            ),
                            validatorForm: (val) => null,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 6.sp),
                  Container(
                    padding: EdgeInsets.all(16.sp),
                    color: Colors.white,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Image.asset(
                              'assets/images/ic_form_budget.png',
                              width: 20.sp,
                            ),
                            SizedBox(
                              width: 10.sp,
                            ),
                            Text(
                              Strings.budget.i18n,
                              style: TextStyle(
                                color: colorFontGreen,
                                fontSize: 14.sp,
                                fontWeight: FontWeight.w600,
                              ),
                            )
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 15.sp),
                          child: dividerChat,
                        ),
                        Text(
                          Strings.availableBudget.i18n,
                          style: TextStyle(
                            fontSize: 12.sp,
                            fontWeight: FontWeight.w600,
                            color: colorBlack2,
                          ),
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Expanded(
                              child: TextFieldFormRequest(
                                controller: _priceController,
                                hintText: Strings.priceHintext.i18n,
                                maxLines: 1,
                                textInputType: TextInputType.multiline,
                                inputFormatters: _currency == CURRENCY_VND
                                    ? CurrencyHelper.vndInputFormat
                                    : CurrencyHelper.usdInputFormat,
                                focusNode: _budgetFocus,
                                validatorForm: (val) =>
                                    val!.isEmpty || val.trim().length == 0
                                        ? Strings
                                            .budgetCannotBeLeftBlankErrortext
                                            .i18n
                                        : null,
                                onChanged: (val) {
                                  _price = val.trim();
                                },
                              ),
                            ),
                            SizedBox(width: 12.sp),
                            TouchableOpacity(
                              onTap: () {
                                setState(() {
                                  if (_currency == CURRENCY_VND) {
                                    _currency = CURRENCY_USD;
                                  } else {
                                    _currency = CURRENCY_VND;
                                  }
                                });

                                if (_priceController.text.isNotEmpty) {
                                  if (_currency == CURRENCY_VND) {
                                    _priceController.text =
                                        double.parse(_priceController.text)
                                            .toStringAsFixed(0)
                                            .formatMoney(splitBy: ',');
                                  } else {
                                    _priceController.text = _priceController
                                        .text
                                        .replaceAll(',', '');
                                  }
                                }
                              },
                              child: Container(
                                padding: EdgeInsets.only(top: 10.sp),
                                alignment: Alignment.centerRight,
                                child: Text(
                                  _currency == CURRENCY_VND
                                      ? Strings.vnd.i18n.toUpperCase()
                                      : Strings.usd.i18n.toUpperCase(),
                                  style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    color: colorBlack2,
                                    fontSize: 12.5.sp,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 15.sp),
                        Text(
                          Strings.participants.i18n,
                          style: TextStyle(
                            fontSize: 12.5.sp,
                            fontWeight: FontWeight.w600,
                            color: colorBlack2,
                          ),
                        ),
                        TextFieldFormRequest(
                          controller: _quantityController,
                          hintText: Strings.participantHintext.i18n,
                          maxLines: 1,
                          inputFormatters: [
                            FilteringTextInputFormatter.digitsOnly,
                          ],
                          validatorForm: (val) => val!.isEmpty ||
                                  val.trim().length == 0
                              ? Strings
                                  .participantsCannotBeLeftBlankErrortext.i18n
                              : (int.tryParse(val.trim().toString())! > 0 &&
                                      int.tryParse(val.trim().toString())! <=
                                          1000)
                                  ? null
                                  : "Số lượng người phải <=1000",
                          onChanged: (val) => _quantity = val.trim(),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 6.sp),
                  Container(
                    padding: EdgeInsets.all(16.sp),
                    color: Colors.white,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Image.asset(
                              'assets/images/ic_form_contact.png',
                              width: 20.sp,
                            ),
                            SizedBox(
                              width: 10.sp,
                            ),
                            Text(
                              Strings.contactForm.i18n,
                              style: TextStyle(
                                color: colorFontGreen,
                                fontSize: 14.sp,
                                fontWeight: FontWeight.w600,
                              ),
                            )
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 15.sp),
                          child: dividerChat,
                        ),
                        Text(
                          Strings.contactForm.i18n,
                          style: TextStyle(
                            fontSize: 12.sp,
                            fontWeight: FontWeight.w600,
                            color: colorBlack2,
                          ),
                        ),
                        SizedBox(height: 10.sp),
                        TouchableOpacity(
                          onTap: () {
                            showModalBottomSheet(
                              context: context,
                              backgroundColor: Colors.transparent,
                              isScrollControlled: true,
                              builder: (context) {
                                return BottomSheetString(
                                  listString: _contactForms,
                                  sizeBottomSheet: 25.h,
                                  title: Strings.contactForm.i18n,
                                  handlePressed: (val) {
                                    setState(() {
                                      _contactForm = val == _contactForms[1]
                                          ? CONTACT_MEET
                                          : CONTACT_CALL;
                                    });
                                    _contactFormController.text = val;
                                  },
                                );
                              },
                            );
                          },
                          child: TextFieldForm(
                            controller: _contactFormController,
                            autovalidateMode: AutovalidateMode.always,
                            isActive: false,
                            contentPadding: EdgeInsets.fromLTRB(
                              10.sp,
                              20.sp,
                              10.sp,
                              0.sp,
                            ),
                            fontSize: 13.sp,
                            suffixIcon: TouchableOpacity(
                              onTap: () {},
                              child: Image.asset(
                                iconArrowDown,
                                width: 10.sp,
                              ),
                            ),
                            validatorForm: (val) => null,
                          ),
                        ),
                        SizedBox(
                          height: 15.sp,
                        ),
                        Text(
                          Strings.bidingClosingDate.i18n,
                          style: TextStyle(
                            fontSize: 12.sp,
                            fontWeight: FontWeight.w600,
                            color: colorBlack2,
                          ),
                        ),
                        TouchableOpacity(
                          onTap: () {
                            showModalBottomSheet(
                              context: context,
                              backgroundColor: Colors.transparent,
                              isScrollControlled: true,
                              builder: (context) {
                                return BottomSheetCalendar(
                                  selected: _selectedDate,
                                  onDateSelected: (date) {
                                    setState(() {
                                      _selectedDate = date;
                                      AppNavigator.pop();
                                    });
                                  },
                                );
                              },
                            );
                          },
                          child: TextFieldForm(
                            initialValue:
                                DateFormat('dd/MM/yyyy').format(_selectedDate),
                            autovalidateMode: AutovalidateMode.always,
                            isActive: false,
                            contentPadding: EdgeInsets.fromLTRB(
                              10.sp,
                              20.sp,
                              10.sp,
                              0.sp,
                            ),
                            fontSize: 13.sp,
                            suffixIcon: TouchableOpacity(
                              onTap: () {},
                              child: Image.asset(
                                iconArrowDown,
                                width: 10.sp,
                              ),
                            ),
                            validatorForm: (val) => null,
                          ),
                        ),
                        AnimatedSwitcher(
                          duration: const Duration(
                              milliseconds: DURATION_DEFAULT_ANIMATION),
                          transitionBuilder:
                              (Widget child, Animation<double> animation) {
                            return ScaleTransition(
                                scale: animation, child: child);
                          },
                          child: _contactForm == CONTACT_MEET
                              ? Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(height: 15.sp),
                                    Text(
                                      Strings.place.i18n,
                                      style: TextStyle(
                                        fontSize: 12.sp,
                                        fontWeight: FontWeight.w600,
                                        color: colorBlack2,
                                      ),
                                    ),
                                    TextFieldFormRequest(
                                      controller: _locationNameController,
                                      hintText: Strings.place.i18n,
                                      maxLines: 1,
                                      validatorForm: (val) => val!.isEmpty ||
                                              val.trim().length == 0
                                          ? Strings
                                              .placeCannotBeLeftBlankErrortext
                                              .i18n
                                              .i18n
                                          : null,
                                      onChanged: (val) =>
                                          _locationName = val.trim(),
                                    ),
                                    TextFieldFormRequest(
                                      controller: _locationAddressController,
                                      hintText: Strings.placeNameHintext.i18n,
                                      maxLines: 3,
                                      validatorForm: (val) => val!.isEmpty ||
                                              val.trim().length == 0
                                          ? Strings
                                              .placeNameCannotBeLeftBlankErrortext
                                              .i18n
                                          : null,
                                      onChanged: (val) =>
                                          _locationAddress = val.trim(),
                                    ),
                                  ],
                                )
                              : SizedBox(),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 18.sp),
                  Container(
                    margin: EdgeInsets.symmetric(
                      horizontal: 16.sp,
                    ),
                    child: ButtonPrimary(
                      onPressed: () {
                        if (!_formKey.currentState!.validate())
                          return;
                        else {
                          showDialogLoading();
                          int indexSpecialty = AppBloc.requestBloc.specialties
                              .indexWhere(
                                  (spec) => spec.getLocaleTitle == _specialty);

                          SpecialtyModel specialtyModel =
                              AppBloc.requestBloc.specialties[indexSpecialty];

                          RequestModel request = RequestModel(
                            title: _title,
                            content: _content,
                            specialty: specialtyModel.id,
                            budget: BudgetModel(
                              cost: double.parse(_price.replaceAll(',', '')),
                              totalMinutes: 60,
                              currency: _currency,
                            ),
                            contactForm: _contactForm,
                            locationName: _contactForm == CONTACT_CALL
                                ? EMPTY_STRING
                                : _locationName,
                            locationAddress: _contactForm == CONTACT_CALL
                                ? EMPTY_STRING
                                : _locationAddress,
                            offerers: [],
                            participantsCount: int.parse(_quantity),
                            dateExpired: _selectedDate,
                            selectedOffers: [],
                          );
                          if (_isCreating) {
                            AppBloc.requestBloc.add(
                              CreateRequestEvent(
                                request: request,
                                isNavigationFromSearch:
                                    widget.isNavigationFromSearch,
                              ),
                            );
                          } else {
                            request.id = widget.request!.id;
                            AppBloc.requestBloc
                                .add(UpdateRequestEvent(request: request));
                          }
                        }
                      },
                      text: _isCreating
                          ? Strings.postRequest.i18n
                          : Strings.save.i18n,
                    ),
                  ),
                  !_isCreating
                      ? TouchableOpacity(
                          onTap: () => AppNavigator.pop(),
                          child: Container(
                            padding: EdgeInsets.symmetric(
                              vertical: 12.sp,
                            ),
                            color: Colors.transparent,
                            width: 100.w,
                            alignment: Alignment.center,
                            child: Text(
                              Strings.cancel.i18n,
                              style: TextStyle(
                                color: colorGray1,
                                fontSize: 13.sp,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        )
                      : SizedBox(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
