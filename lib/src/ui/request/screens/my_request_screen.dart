import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/request/request_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/ui/request/widgets/button_choose_list.dart';
import 'package:askany/src/ui/request/widgets/finished_list.dart';
import 'package:askany/src/ui/request/widgets/happening_list.dart';

import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class MyRequestScreen extends StatefulWidget {
  MyRequestScreen({Key? key}) : super(key: key);

  @override
  State<MyRequestScreen> createState() => _MyRequestScreenState();
}

class _MyRequestScreenState extends State<MyRequestScreen> {
  bool _isCheckHappening = true;

  @override
  void initState() {
    super.initState();
    _isCheckHappening = true;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          SizedBox(height: 11.sp),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 16.sp),
            child: Row(
              children: [
                ButtonChooseList(
                  isChoose: _isCheckHappening,
                  onChanged: () {
                    if (!_isCheckHappening) {
                      setState(() {
                        _isCheckHappening = true;
                      });
                      AppBloc.requestBloc.add(CleanRequestEvent());
                      AppBloc.requestBloc.add(OnHappeningEvent());
                    }
                  },
                  title: Strings.beingInprogress.i18n,
                ),
                SizedBox(width: 15.sp),
                ButtonChooseList(
                  isChoose: !_isCheckHappening,
                  onChanged: () {
                    if (_isCheckHappening) {
                      setState(() {
                        _isCheckHappening = false;
                      });
                      AppBloc.requestBloc.add(CleanRequestEvent());
                      AppBloc.requestBloc.add(OnFinishedEvent());
                    }
                  },
                  title: Strings.closed.i18n,
                ),
              ],
            ),
          ),
          Expanded(
            child: _isCheckHappening ? HappeningList() : FinishedList(),
          ),
        ],
      ),
    );
  }
}
