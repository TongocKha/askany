import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/request/request_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/models/author_model.dart';
import 'package:askany/src/models/offer_model.dart';
import 'package:askany/src/models/request_model.dart';
import 'package:askany/src/models/slide_mode.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/custom_image/network_image/cached_image.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_confirm_cancel.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:askany/src/ui/request/widgets/create_offer.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:askany/src/ui/request/widgets/offer_request_card.dart';
import 'package:askany/src/ui/request/widgets/request_details_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class DetailsRequestScreen extends StatefulWidget {
  final RequestModel request;
  final bool isCommonRequest;
  DetailsRequestScreen({
    required this.request,
    this.isCommonRequest = false,
  });
  @override
  _DetailsRequestScreenState createState() => _DetailsRequestScreenState();
}

class _DetailsRequestScreenState extends State<DetailsRequestScreen> {
  late RequestModel _request;
  late bool _isExpert;
  late bool _isCommonRequest;

  @override
  void initState() {
    super.initState();
    _request = widget.request;
    _isCommonRequest = widget.isCommonRequest;
    _isExpert = UserLocal().getIsExpert();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<RequestBloc, RequestState>(
      listener: (context, state) {
        if (state is GetDoneRequestHappening || state is GetDoneRequestCommon) {
          int indexOfRequest = (state.props[0] as List<RequestModel>).indexWhere(
            (item) => item.id == _request.id,
          );
          if (indexOfRequest != -1) {
            setState(() {
              _request = state.props[0][indexOfRequest];
              _isCommonRequest = !_request.isMyAuthor;
            });
          } else {
            setState(() {
              int indexOfOffer = _request.offerers.indexWhere(
                (offer) => offer.authorExpert.id == AppBloc.userBloc.getAccount.id,
              );
              if (indexOfOffer != -1) {
                _request.offerers.removeAt(indexOfOffer);
                _isCommonRequest = true;
              }
            });
          }
        }
      },
      child: Scaffold(
        appBar: appBarTitleBack(context, ''),
        body: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            children: [
              RequestDetailsCard(_request, _isCommonRequest),
              _isExpert
                  ? Padding(
                      padding: EdgeInsets.symmetric(horizontal: 16.sp),
                      child: Container(
                        color: colorGray2,
                        width: 100.w,
                        height: 1.sp,
                      ),
                    )
                  : dividerThinkness6,
              _isExpert
                  ? _CustomerInfo(
                      author: _request.authorUser!,
                    )
                  : SizedBox(),
              UserLocal().getAccessToken() == ''
                  ? SizedBox()
                  : _isCommonRequest
                      ? (_isExpert
                          ? (_request.status == REQUEST_FINISHED ||
                                  _request.selectedOffers.indexWhere((element) =>
                                          element.authorExpert.id ==
                                          AppBloc.userBloc.getAccount.id) !=
                                      -1
                              ? _MyOffer(
                                  offer:
                                      [..._request.offerers, ..._request.selectedOffers].firstWhere(
                                    (element) =>
                                        element.authorExpert.id == AppBloc.userBloc.getAccount.id,
                                  ),
                                  request: _request,
                                )
                              : CreateOffer(
                                  requestModel: _request,
                                ))
                          : SizedBox())
                      : _isExpert
                          ? (_request.status == REQUEST_FINISHED ||
                                  _request.selectedOffers.indexWhere((element) =>
                                          element.authorExpert.id ==
                                          AppBloc.userBloc.getAccount.id) !=
                                      -1
                              ? _MyOfferResult(
                                  isChosen: _request.selectedOffers.indexWhere((element) =>
                                          element.authorExpert.id ==
                                          AppBloc.userBloc.getAccount.id) !=
                                      -1,
                                )
                              : _MyOffer(
                                  offer:
                                      [..._request.offerers, ..._request.selectedOffers].firstWhere(
                                    (element) =>
                                        element.authorExpert.id == AppBloc.userBloc.getAccount.id,
                                  ),
                                  request: _request,
                                ))
                          : (_request.offerers.isEmpty && _request.selectedOffers.isEmpty
                              ? Container(
                                  alignment: Alignment.topLeft,
                                  constraints: BoxConstraints(
                                    minHeight: 30.h,
                                  ),
                                  margin: EdgeInsets.all(16.sp),
                                  child: Text(
                                    Strings.noBidsYet.i18n,
                                    style: TextStyle(
                                      fontSize: 15.sp,
                                      fontWeight: FontWeight.w600,
                                      color: colorBlack1,
                                    ),
                                  ),
                                )
                              : _ListOffers(request: _request)),
            ],
          ),
        ),
      ),
    );
  }
}

class _MyOfferResult extends StatelessWidget {
  const _MyOfferResult({Key? key, required this.isChosen}) : super(key: key);

  final bool isChosen;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100.w,
      padding: EdgeInsets.symmetric(
        vertical: 22.5.sp,
        horizontal: 16.sp,
      ),
      color: isChosen ? colorChosenCard : colorNotChosenCard,
      child: Text(
        '${Strings.bidingEnd.i18n}, ' +
            (isChosen
                ? Strings.congratulationYouAreTheChosenOne.i18n
                : Strings.unfortunalyYouAreNotTheChosen.i18n),
        textAlign: TextAlign.left,
        style: TextStyle(
          fontWeight: FontWeight.w600,
          fontSize: 15.sp,
          color: colorBlack1,
          height: 1.36,
        ),
      ),
    );
  }
}

class _ListOffers extends StatelessWidget {
  const _ListOffers({Key? key, required this.request}) : super(key: key);

  final RequestModel request;

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.topLeft,
      padding: EdgeInsets.only(
        left: 16.sp,
        top: 32.sp,
        right: 16.sp,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          request.selectedOffers.isNotEmpty
              ? Text(
                  Strings.selectedExpert.i18n,
                  style: TextStyle(
                    fontSize: 17.sp,
                    fontWeight: FontWeight.w700,
                    color: colorBlack1,
                  ),
                )
              : SizedBox(),
          SizedBox(height: request.selectedOffers.isNotEmpty ? 22.sp : 0),
          request.selectedOffers.isNotEmpty
              ? ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  padding: EdgeInsets.only(bottom: 30.sp),
                  shrinkWrap: true,
                  itemCount: request.selectedOffers.length,
                  itemBuilder: (context, index) {
                    return OfferRequestCard(
                      offer: request.selectedOffers[index],
                      request: request,
                    );
                  },
                )
              : SizedBox(),
          request.offerers.isNotEmpty
              ? Text(
                  Strings.bidExperts.i18n,
                  style: TextStyle(
                    fontSize: 17.sp,
                    fontWeight: FontWeight.w700,
                    color: colorBlack1,
                  ),
                )
              : SizedBox(),
          SizedBox(height: request.offerers.isNotEmpty ? 22.sp : 0),
          request.offerers.isNotEmpty
              ? ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  padding: EdgeInsets.only(bottom: 30.sp),
                  shrinkWrap: true,
                  itemCount: request.offerers.length,
                  itemBuilder: (context, index) {
                    return OfferRequestCard(
                      offer: request.offerers[index],
                      request: request,
                    );
                  },
                )
              : SizedBox(height: 30.sp),
        ],
      ),
    );
  }
}

class _MyOffer extends StatelessWidget {
  const _MyOffer({
    Key? key,
    required this.offer,
    required this.request,
  }) : super(key: key);
  final OfferModel offer;
  final RequestModel request;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100.w,
      padding: EdgeInsets.only(
        left: 16.sp,
        top: 28.sp,
        right: 16.sp,
        bottom: 50.sp,
      ),
      color: colorAddButton,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            Strings.yourBids.i18n,
            style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 17.sp,
              color: colorBlack1,
              height: 1.35,
            ),
          ),
          SizedBox(height: 12.sp),
          RichText(
            textAlign: TextAlign.left,
            text: TextSpan(
              style: TextStyle(
                fontSize: 13.sp,
                color: colorBlack2,
              ),
              children: [
                TextSpan(
                  text:
                      "${Strings.offer.i18n}: ${offer.price.costString}\n${Strings.adviseDuration.i18n}: ${offer.price.totalMinutes}p\n${Strings.contentHintext.i18n}:\n",
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    height: 2.25,
                  ),
                ),
                TextSpan(
                  text: offer.content,
                  style: TextStyle(
                    fontWeight: FontWeight.w400,
                    height: 1.875,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 28.sp),
          TouchableOpacity(
            onTap: () {
              dialogAnimationWrapper(
                slideFrom: SlideMode.bot,
                child: DialogConfirmCancel(
                  title: Strings.cancelOffer.i18n,
                  bodyBefore:
                      '${Strings.areYouSureToCancelP1.i18n} "${request.title}" ${Strings.areYouSureToCancelP2.i18n}',
                  confirmText: Strings.confirm.i18n,
                  cancelText: Strings.cancel.i18n,
                  onConfirmed: () {
                    AppNavigator.pop();
                    showDialogLoading();
                    AppBloc.requestBloc.add(
                      ExpertCancelOfferEvent(
                        offerId: offer.id,
                        requestModel: request,
                      ),
                    );
                  },
                ),
              );
            },
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 9.sp),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: colorChosenCard,
                borderRadius: BorderRadius.circular(8.sp),
                border: Border.all(color: colorGray2, width: 1.sp),
              ),
              child: Text(
                Strings.cancelOffer.i18n,
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 13.sp,
                  color: colorGray1,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class _CustomerInfo extends StatelessWidget {
  const _CustomerInfo({
    Key? key,
    required this.author,
  }) : super(key: key);

  final AuthorModel author;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        left: 16.sp,
        top: 18.sp,
        right: 16.sp,
        bottom: 38.sp,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            Strings.customerInformation.i18n,
            style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 14.sp,
              color: colorBlack1,
              height: 1.47,
            ),
          ),
          SizedBox(height: 14.sp),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              CustomNetworkImage(
                width: 44.sp,
                urlToImage: author.avatar?.urlToImage,
              ),
              SizedBox(width: 10.sp),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    author.fullname,
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 13.sp,
                      color: colorBlack2,
                      height: 1.5,
                    ),
                  ),
                  SizedBox(height: 2.sp),
                  Text(
                    'Hồ Chí Minh',
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 11.sp,
                      color: colorGray1,
                      height: 1.57,
                    ),
                  ),
                ],
              )
            ],
          ),
        ],
      ),
    );
  }
}
