import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/request/request_bloc.dart';
import 'package:askany/src/bloc/timeline/timeline_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/models/offer_model.dart';
import 'package:askany/src/models/request_model.dart';
import 'package:askany/src/models/slide_mode.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_with_text_and_pop_button.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:askany/src/ui/common/widgets/step_order_service.dart';
import 'package:askany/src/ui/request/widgets/order_service_step_one.dart';
import 'package:askany/src/ui/request/widgets/order_service_step_three.dart';
import 'package:askany/src/ui/request/widgets/order_service_step_two.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class OrderServiceScreen extends StatefulWidget {
  const OrderServiceScreen({
    Key? key,
    this.request,
    this.offer,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _OrderServiceScreenState();
  final RequestModel? request;
  final OfferModel? offer;
}

class _OrderServiceScreenState extends State<OrderServiceScreen> {
  late TimelineBloc _timelineBloc;
  late PageController _pageController;
  late List<Widget> _steps;
  int _currentIndex = 0;
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _authorNameController =
      TextEditingController(text: AppBloc.userBloc.getAccount.fullname);
  final TextEditingController _authorPhoneController =
      TextEditingController(text: AppBloc.userBloc.getAccount.phone);
  final TextEditingController _noteController = TextEditingController();
  DateTime? _startTime;
  DateTime? _endTime;
  DateTime _selectedDate = DateTime.now();
  PaymentMethod _paymentMethod = PaymentMethod.byVNPay;
  int _adviseDuration = 0;
  @override
  void initState() {
    super.initState();
    _timelineBloc = TimelineBloc();

    if (widget.request!.endTime!.difference(widget.request!.startTime!).inMinutes != 0) {
      _adviseDuration = widget.request!.endTime!.difference(widget.request!.startTime!).inMinutes;
    } else {
      _adviseDuration = widget.offer!.price.totalMinutes;
    }
    if (widget.offer != null) {
      _initialValue();
    }

    _pageController = PageController(
      initialPage: _currentIndex,
    );
    _steps = [
      OrderServiceStepOne(
        formKey: _formKey,
        authorNameController: _authorNameController,
        authorPhoneController: _authorPhoneController,
        noteController: _noteController,
      ),
      OrderServiceStepTwo(
        totalMinus: _adviseDuration,
        initStartTime: _startTime,
        initEndTime: _endTime,
        onTimeSelected: (startTime, endTime) {
          setState(() {
            _startTime = DateTime(
              _selectedDate.year,
              _selectedDate.month,
              _selectedDate.day,
              startTime.hour,
              startTime.minute,
            );
            _endTime = DateTime(
              _selectedDate.year,
              _selectedDate.month,
              _selectedDate.day,
              endTime.hour,
              endTime.minute,
            );
          });
        },
        selectedDate: _selectedDate,
        onDateSelected: (val) {
          setState(() {
            _startTime = DateTime.now();
            _endTime = null;
            _selectedDate = val;
            _update(val);
          });
        },
      ),
      OrderServiceStepThree(
        request: widget.request!,
        offer: widget.offer!,
        onSelectedValueChanged: (val) {
          _paymentMethod = val;
        },
        adviseDuration: _adviseDuration,
      ),
    ];
  }

  _initialValue() {
    if (widget.offer!.authorName != null && widget.offer!.authorName!.isNotEmpty) {
      _authorNameController.text = widget.offer!.authorName!;
      _authorPhoneController.text = widget.offer!.authorPhone!;
      _noteController.text = widget.offer!.note!;

      _currentIndex = 1;
    }

    if (widget.offer?.isPickTime != null && widget.offer!.isPickTime) {
      _startTime = widget.offer!.startTime;
      if (_startTime != null) {
        _selectedDate = DateTime(
          _startTime!.year,
          _startTime!.month,
          _startTime!.day,
        );
      }

      _endTime = widget.offer!.endTime;
      _currentIndex = 2;
    }
  }

  _update(DateTime startTime) {
    _steps = [
      OrderServiceStepOne(
        formKey: _formKey,
        authorNameController: _authorNameController,
        authorPhoneController: _authorPhoneController,
        noteController: _noteController,
      ),
      OrderServiceStepTwo(
        totalMinus: _adviseDuration,
        initStartTime: _startTime,
        initEndTime: _endTime,
        onTimeSelected: (startTime, endTime) {
          setState(() {
            _startTime = DateTime(
              _selectedDate.year,
              _selectedDate.month,
              _selectedDate.day,
              startTime.hour,
              startTime.minute,
            );
            _endTime = DateTime(
              _selectedDate.year,
              _selectedDate.month,
              _selectedDate.day,
              endTime.hour,
              endTime.minute,
            );
          });
        },
        selectedDate: _selectedDate,
        onDateSelected: (val) {
          setState(() {
            _startTime = DateTime.now();
            _endTime = null;
            _selectedDate = val;
          });
        },
      ),
      OrderServiceStepThree(
        request: widget.request!,
        offer: widget.offer!,
        onSelectedValueChanged: (val) {
          _paymentMethod = val;
        },
        adviseDuration: _adviseDuration,
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      lazy: false,
      create: (context) => _timelineBloc
        ..add(
          GetDateAvailable(
            selectedDate: DateTime.now(),
            contactType: widget.request!.contactForm == CONTACT_MEET ? 0 : 1,
            expertId: widget.offer!.authorExpert.id,
          ),
        ),
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        appBar: appBarTitleBack(
          context,
          Strings.orderingService.i18n,
          onBackPressed: () {
            if (_currentIndex != 0) {
              _update(_startTime ?? DateTime.now());

              _pageController.previousPage(
                duration: const Duration(milliseconds: LIMIT_RESPONSE_TIME),
                curve: Curves.easeInOut,
              );
            } else {
              AppNavigator.pop();
            }
          },
        ),
        body: Container(
          child: Column(
            children: [
              Container(
                width: 100.w,
                height: 48.sp,
                margin: EdgeInsets.only(top: 18.sp, bottom: 14.sp),
                padding: EdgeInsets.symmetric(horizontal: 4.sp),
                child: StepOrderService(
                  steps: [
                    Strings.enteringInformation.i18n,
                    Strings.selectingTime.i18n,
                    Strings.payment,
                  ],
                  currentStep: _currentIndex,
                  onStepChanged: (index) => _pageController.jumpToPage(index),
                ),
              ),
              dividerThinkness6NotMargin,
              Expanded(
                child: PageView(
                  physics: NeverScrollableScrollPhysics(),
                  controller: _pageController,
                  children: _steps,
                  onPageChanged: (index) {
                    setState(() {
                      _currentIndex = index;
                    });
                  },
                ),
              ),
              dividerChat,
              Visibility(
                visible: MediaQuery.of(context).viewInsets.bottom == 0,
                child: Container(
                  width: 100.w,
                  padding: EdgeInsets.only(
                    left: 16.sp,
                    right: 16.sp,
                    bottom: 16.sp,
                    top: _currentIndex == 2 ? 8.sp : 16.sp,
                  ),
                  child: Column(
                    children: [
                      _currentIndex == 2
                          ? Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text(
                                  Strings.totalPrice.i18n,
                                  style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 13.sp,
                                    color: colorBlack2,
                                    height: 1.625,
                                  ),
                                ),
                                Text(
                                  convertMoneyToString(
                                    calculateTotal(
                                      widget.request!,
                                      widget.offer!,
                                      _adviseDuration,
                                    ),
                                    widget.request!.budget!.currency,
                                  ),
                                  style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 20.sp,
                                    color: colorFontGreen,
                                  ),
                                ),
                              ],
                            )
                          : SizedBox(),
                      _currentIndex == 2 ? SizedBox(height: 12.sp) : SizedBox(),
                      ButtonPrimary(
                        onPressed: _buttonOnPressedHandle,
                        fontWeight: FontWeight.w700,
                        text: _currentIndex == _steps.length - 1
                            ? Strings.payment.i18n
                            : Strings.nextStep.i18n,
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  _buttonOnPressedHandle() async {
    switch (_currentIndex) {
      case 0:
        final _isValid = _formKey.currentState!.validate();
        if (_isValid) {
          showDialogLoading();
          AppBloc.requestBloc.add(
            UserDoneStep1Event(
                offer: widget.offer!,
                request: widget.request!,
                authorName: _authorNameController.text,
                authorPhone: _authorPhoneController.text,
                note: _noteController.text,
                handleFinished: () {
                  _pageController.nextPage(
                    duration: Duration(milliseconds: LIMIT_RESPONSE_TIME),
                    curve: Curves.easeIn,
                  );
                }),
          );
        }
        break;
      case 1:
        if (_startTime == null || _endTime == null) {
          dialogAnimationWrapper(
            slideFrom: SlideMode.fade,
            child: DialogWithTextAndPopButton(
              bodyBefore: Strings.youHaveNotSelectedAnyTimeErrortext.i18n,
              bodyAlign: TextAlign.center,
              bodyFontSize: 13.sp,
              bodyColor: colorBlack1,
            ),
          );
          return;
        }

        showDialogLoading();
        _update(_startTime ?? DateTime.now());
        AppBloc.requestBloc.add(
          UserDoneStep2Event(
              offer: widget.offer!,
              request: widget.request!,
              startTime: _startTime!,
              endTime: _endTime!,
              handleFinished: () {
                _pageController.nextPage(
                  duration: Duration(milliseconds: LIMIT_RESPONSE_TIME),
                  curve: Curves.easeIn,
                );
              }),
        );

        break;
      case 2:
        final _amount = calculateTotal(widget.request!, widget.offer!, _adviseDuration);
        showDialogLoading();
        AppBloc.requestBloc.add(
          UserPayOfferEvent(
            paymentMethod: _paymentMethod,
            amount: _amount,
            offerId: widget.offer!.id,
            expertId: widget.offer!.authorExpert.id,
            request: widget.request!,
            updatedWallet: AppBloc.userBloc.getAccount.wallet! - _amount,
          ),
        );
        break;
      default:
        break;
    }
  }
}
