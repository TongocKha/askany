import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/request/request_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/configs/themes/app_colors.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/request/widgets/common_request_list.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class RequestServiceScreen extends StatefulWidget {
  const RequestServiceScreen({Key? key}) : super(key: key);

  @override
  State<RequestServiceScreen> createState() => _RequestServiceScreenState();
}

class _RequestServiceScreenState extends State<RequestServiceScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 10.sp),
          Container(
            child: Row(
              children: [
                Expanded(
                  child: TouchableOpacity(
                    onTap: () {
                      AppNavigator.push(Routes.CREATE_REQUEST_FILTER);
                    },
                    child: Container(
                      height: 33.sp,
                      margin: EdgeInsets.only(
                          left: 16.sp, right: AppBloc.userBloc.getAccount.isExpert! ? 8.sp : 16.sp),
                      decoration: BoxDecoration(
                        border: Border.all(color: colorGrayBorder, width: .5.sp),
                        borderRadius: BorderRadius.circular(8.sp),
                      ),
                      child: BlocBuilder<RequestBloc, RequestState>(
                        builder: (context, state) {
                          final String _hinText = AppBloc.requestBloc.category != null
                              ? AppBloc.requestBloc.category!.name
                              : Strings.requestSearchingHintext.i18n;
                          return TextFormField(
                            enabled: false,
                            style: TextStyle(
                              color: colorCaptionSearch,
                              fontSize: 12.sp,
                            ),
                            keyboardType: TextInputType.multiline,
                            maxLines: 1,
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.only(
                                top: 0.sp,
                                right: 10.sp,
                              ),
                              hintText: _hinText,
                              hintStyle: TextStyle(
                                color: colorCaptionSearch,
                                fontSize: 12.sp,
                                fontWeight: FontWeight.w400,
                              ),
                              filled: true,
                              fillColor: Colors.transparent,
                              border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                              ),
                              prefixIcon: Container(
                                margin: EdgeInsets.all(10.sp),
                                child: Image.asset(
                                  iconSearch,
                                ),
                              ),
                            ),
                            onChanged: (val) {
                              setState(() {});
                            },
                          );
                        },
                      ),
                    ),
                  ),
                ),
                Visibility(
                  visible: UserLocal().getIsExpert(),
                  child: TouchableOpacity(
                    onTap: () {
                      AppNavigator.push(Routes.FILTER_REQUEST);
                    },
                    child: Container(
                      margin: EdgeInsets.only(right: 16.sp),
                      decoration: BoxDecoration(
                        border: Border.all(width: 0.5.sp, color: colorGray2),
                        borderRadius: BorderRadius.all(
                          Radius.circular(8.sp),
                        ),
                      ),
                      padding: EdgeInsets.all(9.sp),
                      width: 33.sp,
                      height: 33.sp,
                      child: Image.asset(
                        iconFilter,
                        fit: BoxFit.cover,
                        color: colorwhiteborder,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(child: CommonRequestList()),
        ],
      ),
    );
  }
}
