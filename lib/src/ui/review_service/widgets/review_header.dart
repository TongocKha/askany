import 'package:askany/src/ui/common/widgets/cake_avatar/cached_cake_image.dart';
import 'package:askany/src/ui/review_service/widgets/rating_and_text_review.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ReviewHeader extends StatelessWidget {
  final String? urlImage;
  final String username;
  final int? ratingCount;
  final int sizeUrlImage;
  final double? sizeStar;
  final Color colorUsername;
  final EdgeInsetsGeometry? marginStar;
  final double distanceTextAndStar;
  final double stars;
  final String? descriptionBehindStar;
  const ReviewHeader({
    this.urlImage,
    required this.username,
    this.ratingCount,
    this.sizeUrlImage = 35,
    this.sizeStar,
    this.colorUsername = colorBlack2,
    this.marginStar,
    this.distanceTextAndStar = 0,
    required this.stars,
    this.descriptionBehindStar,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CachedCakeImage(
            fit: BoxFit.cover,
            urlToImage: urlImage ?? '',
            size: sizeUrlImage.sp,
            radius: BorderRadius.all(
              Radius.circular(sizeUrlImage.sp),
            ),
          ),
          SizedBox(width: 6.5.sp),
          Expanded(
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 1.sp),
                    child: Text(
                      username,
                      style: TextStyle(
                        fontSize: 13.sp,
                        fontWeight: FontWeight.w600,
                        color: colorUsername,
                      ),
                    ),
                  ),
                  RatingAndTextReview(
                    stars: stars,
                    ratingCount: ratingCount ?? 0,
                    descriptionBehindStar: descriptionBehindStar,
                    sizeStar: sizeStar,
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
