import 'package:askany/src/ui/common/widgets/shimmers/fade_simmer.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ReviewShimmerCard extends StatelessWidget {
  const ReviewShimmerCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 18.sp),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                FadeShimmer.round(
                  size: 35.sp,
                  fadeTheme: FadeTheme.lightReverse,
                ),
                SizedBox(width: 6.5.sp),
                Expanded(
                  child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        FadeShimmer(
                          height: 14.sp,
                          width: 130.sp,
                          fadeTheme: FadeTheme.lightReverse,
                        ),
                        SizedBox(height: 5.sp),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            FadeShimmer(
                              height: 13.sp,
                              width: 80.sp,
                              fadeTheme: FadeTheme.lightReverse,
                            ),
                            SizedBox(width: 6.sp),
                            FadeShimmer(
                              height: 10.sp,
                              width: 90.sp,
                              fadeTheme: FadeTheme.lightReverse,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 10.sp),
          Container(
            margin: EdgeInsets.only(left: 43.sp),
            child: FadeShimmer(
              height: 13.sp,
              width: 90.sp,
              fadeTheme: FadeTheme.lightReverse,
            ),
          ),
        ],
      ),
    );
  }
}
