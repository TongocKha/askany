import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class InfoExpertCard extends StatelessWidget {
  final String icon;
  final String title;
  final String description;

  InfoExpertCard({
    required this.icon,
    required this.title,
    required this.description,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            child: Image.asset(
              icon,
              fit: BoxFit.cover,
              color: colorGray1,
            ),
            height: 22.sp,
          ),
          SizedBox(width: 13.sp),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  style: TextStyle(
                    color: colorBlack2,
                    fontSize: 12.sp,
                  ),
                ),
                Text(
                  description,
                  style: TextStyle(
                    color: colorBlack2,
                    fontSize: 12.sp,
                    fontWeight: FontWeight.w600,
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
