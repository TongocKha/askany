import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class RatingReviewCard extends StatelessWidget {
  final int countStar;
  final String textBehindStar;
  RatingReviewCard({
    required this.countStar,
    required this.textBehindStar,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 14.sp,
      alignment: Alignment.center,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ListView.builder(
            padding: EdgeInsets.all(0),
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemCount: countStar,
            itemBuilder: (context, index) {
              return Padding(
                padding: index == countStar - 1
                    ? EdgeInsets.only(right: 5.sp)
                    : EdgeInsets.only(right: 2.sp),
                child: Image.asset(
                  iconStar,
                  width: 7.sp,
                  color: colorStar,
                ),
              );
            },
          ),
          Padding(
            padding: EdgeInsets.only(top: 2.sp),
            child: Text(
              textBehindStar,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: colorBlack2,
                fontSize: 9.sp,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
