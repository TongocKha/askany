import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/rating/rating_bloc.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/review_service/widgets/rating_review_card.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class FilterRatingSkill extends StatelessWidget {
  final List<int> ratingStar;
  final Function(int) handlePressed;
  const FilterRatingSkill({required this.ratingStar, required this.handlePressed});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 27.sp,
      child: ListView.builder(
        padding: EdgeInsets.symmetric(horizontal: 16.sp),
        scrollDirection: Axis.horizontal,
        physics: BouncingScrollPhysics(),
        itemCount: 5,
        itemBuilder: (context, index) {
          return TouchableOpacity(
            onTap: () {
              AppBloc.ratingBloc.add(
                FilterRatingStarSkillEvent(
                  stars: 5 - index,
                ),
              );
              handlePressed(5 - index);
            },
            child: Container(
              padding: EdgeInsets.symmetric(
                horizontal: 8.sp,
              ),
              decoration: BoxDecoration(
                color: colorDisalbeTimeButton,
                borderRadius: BorderRadius.all(
                  Radius.circular(
                    40.sp,
                  ),
                ),
              ),
              margin: EdgeInsets.only(right: 8.sp),
              child: RatingReviewCard(
                countStar: 5 - index,
                textBehindStar: "(${ratingStar[4 - index]})",
              ),
            ),
          );
        },
      ),
    );
  }
}
