import 'package:askany/src/models/rating_model.dart';
import 'package:askany/src/ui/review_service/widgets/answer_card.dart';
import 'package:askany/src/ui/review_service/widgets/review_header.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ReviewCard extends StatelessWidget {
  final RatingModel ratingModel;
  final bool isCheckLastReview;

  ReviewCard({
    required this.ratingModel,
    this.isCheckLastReview = false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: isCheckLastReview ? 8.sp : 18.sp),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ReviewHeader(
            descriptionBehindStar:
                DateFormat('dd/MM/yyyy hh:mm').format(ratingModel.createdAt!).toString(),
            urlImage: ratingModel.authorUser?.avatar?.urlToImage,
            username: ratingModel.authorUser!.fullname,
            marginStar: EdgeInsets.only(right: 3.sp),
            stars: ratingModel.stars.toDouble(),
            sizeStar: 12.sp,
          ),
          SizedBox(height: 5.sp),
          Visibility(
            visible: ratingModel.content.isNotEmpty,
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(left: 43.sp),
                  child: Text(
                    ratingModel.content,
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                      fontSize: 12.sp,
                      color: colorBlack2,
                      height: 1.25.sp,
                    ),
                  ),
                ),
                SizedBox(height: 10.sp),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 43.sp),
            color: Colors.transparent,
            child: ratingModel.reply != null
                ? AnswerCard(
                    replyModel: ratingModel.reply!,
                    expert: ratingModel.expert!,
                  )
                : Container(),
          ),
          SizedBox(height: isCheckLastReview ? 0 : 6.sp),
          !isCheckLastReview ? dividerChat : Container(),
        ],
      ),
    );
  }
}
