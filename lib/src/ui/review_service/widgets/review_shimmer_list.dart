import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/ui/review_service/widgets/review_shimmer_card.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ReviewShimmerList extends StatelessWidget {
  const ReviewShimmerList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      scrollDirection: Axis.vertical,
      padding: EdgeInsets.symmetric(horizontal: 16.sp),
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: ITEM_COUNT_SHIMMER,
      itemBuilder: (context, index) => ReviewShimmerCard(),
    );
  }
}
