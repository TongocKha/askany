import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/helpers/province_helper.dart';
import 'package:askany/src/models/expert_model.dart';
import 'package:askany/src/models/slide_mode.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/cake_avatar/cached_cake_image.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_with_text_and_pop_button.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';

import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class BottomSheetContactExpert extends StatelessWidget {
  final ExpertModel expertModel;
  final bool isCheckChatScreen;
  const BottomSheetContactExpert({
    required this.expertModel,
    this.isCheckChatScreen = false,
  });

  _onTapContact({required String contactForm}) {
    AppNavigator.pop();
    if (AppBloc.userBloc.getAccount.isVerifyPhone != null &&
        AppBloc.userBloc.getAccount.isVerifyPhone!) {
      if (contactForm == CONTACT_MEET &&
          (expertModel.meetPrice == null || expertModel.meetPrice!.cost == 0)) {
        // Show dialog can't book
        dialogAnimationWrapper(
          slideFrom: SlideMode.bot,
          child: DialogWithTextAndPopButton(
            bodyBefore: Strings.youCannotMakeAAppointmentNoti.i18n,
            bodyFontSize: 12.sp,
            bodyColor: colorBlack1,
          ),
        );
      } else if (contactForm == CONTACT_CALL &&
          expertModel.callPrice!.cost == 0) {
        dialogAnimationWrapper(
          slideFrom: SlideMode.bot,
          child: DialogWithTextAndPopButton(
            bodyBefore: Strings.youCannotMakeACallNoti.i18n,
            bodyFontSize: 12.sp,
            bodyColor: colorBlack1,
          ),
        );
      } else {
        AppNavigator.push(
          Routes.ORDER_EXPERT,
          arguments: {
            'expertModel': expertModel,
            'contactForm': contactForm,
            'isCheckChatScreen': isCheckChatScreen,
          },
        );
      }
    } else {
      AppNavigator.push(Routes.VERIFICATION_OTP, arguments: {
        'phone': AppBloc.userBloc.getAccount.phone,
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100.w,
      padding: EdgeInsets.fromLTRB(16.sp, 0.0, 16.sp, 18.sp),
      decoration: BoxDecoration(
        color: colorWhite,
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(18.sp),
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(height: 5.sp),
          Align(
            alignment: Alignment.center,
            child: Container(
              height: 3.sp,
              width: 60.sp,
              decoration: BoxDecoration(
                color: colorGray4,
                borderRadius: BorderRadius.circular(30),
              ),
            ),
          ),
          SizedBox(height: 15.sp),
          Row(
            children: [
              CachedCakeImage(
                urlToImage: expertModel.avatar?.urlToImage,
                size: 50.sp,
                fit: BoxFit.cover,
                radius: BorderRadius.all(
                  Radius.circular(50.sp),
                ),
              ),
              SizedBox(width: 9.sp),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: Text(
                      expertModel.fullname,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: colorBlack1,
                        fontSize: 13.sp,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  SizedBox(height: 4.sp),
                  Row(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 2.5.sp),
                            child: Image.asset(
                              iconLocation,
                              height: 8.sp,
                              color: colorGray1,
                              width: 10.sp,
                            ),
                          ),
                          SizedBox(width: 4.sp),
                          Text(
                            '${ProvinceHelper().getProvinceByCode(expertModel.province)}',
                            style: TextStyle(
                              color: colorGray2,
                              fontSize: 11.sp,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(height: 4.sp),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      expertModel.stars != 0
                          ? Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Image.asset(
                                  iconStar,
                                  color: colorStar,
                                  width: 11.sp,
                                  height: 11.sp,
                                ),
                                SizedBox(width: 4.sp),
                                Text(
                                  expertModel.stars.toString(),
                                  style: TextStyle(
                                    color: colorGray1,
                                    fontSize: 10.sp,
                                  ),
                                ),
                                SizedBox(width: 3.sp),
                                Text(
                                  '(${expertModel.ratingCount} ${Strings.reviewsCount.i18n})',
                                  style: TextStyle(
                                    color: colorGray2,
                                    fontSize: 11.sp,
                                  ),
                                )
                              ],
                            )
                          : Text(
                              'Chưa có đánh giá',
                              style: TextStyle(
                                color: colorGray2,
                                fontSize: 11.sp,
                              ),
                            )
                    ],
                  )
                ],
              ),
            ],
          ),
          SizedBox(height: 28.sp),
          Row(
            children: [
              Expanded(
                child: TouchableOpacity(
                  onTap: () {
                    _onTapContact(contactForm: CONTACT_CALL);
                  },
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 7.sp),
                    decoration: BoxDecoration(
                      color: colorWhite,
                      border: Border.all(color: colorGray2, width: 1.sp),
                      borderRadius: BorderRadius.all(
                        Radius.circular(
                          7.sp,
                        ),
                      ),
                    ),
                    alignment: Alignment.center,
                    child: Text(
                      Strings.call.i18n,
                      style: TextStyle(
                        color: colorGray2,
                        fontSize: 13.sp,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(width: 15.sp),
              Expanded(
                child: TouchableOpacity(
                  onTap: () {
                    _onTapContact(contactForm: CONTACT_MEET);
                  },
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 9.sp),
                    decoration: BoxDecoration(
                      color: colorFontGreen,
                      borderRadius: BorderRadius.all(
                        Radius.circular(
                          7.sp,
                        ),
                      ),
                    ),
                    alignment: Alignment.center,
                    child: Text(
                      Strings.meet.i18n,
                      style: TextStyle(
                        color: colorWhite,
                        fontSize: 13.sp,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
