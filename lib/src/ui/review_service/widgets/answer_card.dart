import 'package:askany/src/models/author_model.dart';
import 'package:askany/src/models/reply_model.dart';
import 'package:askany/src/ui/common/widgets/cake_avatar/cached_cake_image.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class AnswerCard extends StatelessWidget {
  final ReplyModel replyModel;
  final AuthorModel expert;
  AnswerCard({required this.replyModel, required this.expert});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 8.sp),
      decoration: BoxDecoration(
        color: colorDisalbeTimeButton,
        borderRadius: BorderRadius.all(
          Radius.circular(4.sp),
        ),
      ),
      padding: EdgeInsets.symmetric(
        horizontal: 8.sp,
        vertical: 8.sp,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              CachedCakeImage(
                urlToImage: expert.avatar!.urlToImage,
                size: 23.sp,
                fit: BoxFit.cover,
                radius: BorderRadius.all(
                  Radius.circular(26.sp),
                ),
              ),
              SizedBox(width: 8.sp),
              Text(
                expert.fullname,
                style: TextStyle(
                  color: colorBlack2,
                  fontWeight: FontWeight.w600,
                  fontSize: 13.sp,
                ),
              )
            ],
          ),
          SizedBox(height: 3.sp),
          Container(
            margin: EdgeInsets.only(left: 28.sp),
            child: Text(
              replyModel.content,
              textAlign: TextAlign.justify,
              style: TextStyle(
                color: colorBlack2,
                fontSize: 12.sp,
                height: 1.2.sp,
              ),
            ),
          )
        ],
      ),
    );
  }
}
