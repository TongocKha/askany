import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/rating/rating_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/models/rating_management_model.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/empty/empty.dart';
import 'package:askany/src/ui/common/widgets/pagination_list_view.dart';
import 'package:askany/src/ui/review_service/widgets/filter_rating_skill.dart';
import 'package:askany/src/ui/review_service/widgets/review_card.dart';
import 'package:askany/src/ui/review_service/widgets/review_shimmer_card.dart';
import 'package:askany/src/ui/review_service/widgets/review_shimmer_list.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ReviewSkillScreen extends StatefulWidget {
  final String skillId;
  ReviewSkillScreen({required this.skillId});
  @override
  State<ReviewSkillScreen> createState() => _ReviewSkillScreenState();
}

class _ReviewSkillScreenState extends State<ReviewSkillScreen> {
  final List<int> ratingSkillFake = [1, 2, 3, 4, 5];
  int _stars = -1;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarTitleBack(context, Strings.reviewTabbartext.i18n),
      body: SafeArea(
        bottom: false,
        child: Container(
          child: Column(
            children: [
              dividerChat,
              SizedBox(height: 16.sp),
              FilterRatingSkill(
                ratingStar: ratingSkillFake,
                handlePressed: (val) {
                  setState(() {
                    _stars = val;
                  });
                },
              ),
              SizedBox(height: 16.sp),
              BlocBuilder<RatingBloc, RatingState>(
                buildWhen: (previous, current) => previous != current,
                builder: (context, state) {
                  if (state is GetDoneRatingSkill ||
                      state is GettingRatingSkill) {
                    Map<String, RatingStoreModel>? listRating =
                        state.props[0] as Map<String, RatingStoreModel>?;
                    return Expanded(
                      child: listRating?[widget.skillId] != null
                          ? Container(
                              color: Colors.transparent,
                              padding: EdgeInsets.symmetric(horizontal: 16.sp),
                              child: PaginationListView(
                                padding: EdgeInsets.only(bottom: 18.sp),
                                physics: BouncingScrollPhysics(),
                                itemCount: listRating?[widget.skillId]
                                        ?.getRatingsByStars(_stars)
                                        .ratings
                                        .length ??
                                    0,
                                itemBuilder: (context, index) {
                                  return ReviewCard(
                                    ratingModel: listRating![widget.skillId]!
                                        .getRatingsByStars(_stars)
                                        .ratings[index],
                                    isCheckLastReview: index ==
                                        listRating[widget.skillId]!
                                                .getRatingsByStars(_stars)
                                                .ratings
                                                .length -
                                            1,
                                  );
                                },
                                callBackLoadMore: () {
                                  AppBloc.ratingBloc.add(
                                    GetRatingSkillEvent(),
                                  );
                                },
                                callBackRefresh: (Function handleFinished) {
                                  AppBloc.ratingBloc.add(
                                    RefreshRatingSkillEvent(
                                      handleFinished: handleFinished,
                                    ),
                                  );
                                },
                                isLoadMore: state is GettingRatingSkill,
                                childShimmer: ReviewShimmerCard(),
                              ),
                            )
                          : Empty(
                              image: Image.asset(
                                imageSkillEmpty,
                                width: 120.sp,
                                height: 80.sp,
                              ),
                              text: Strings.thereNoReviewYet.i18n,
                              backgroundColor: Colors.transparent,
                            ),
                    );
                  }
                  return ReviewShimmerList();
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
