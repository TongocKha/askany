import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/rating/rating_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/models/expert_model.dart';
import 'package:askany/src/models/rating_management_model.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/empty/empty.dart';
import 'package:askany/src/ui/common/widgets/pagination_list_view.dart';
import 'package:askany/src/ui/review_service/widgets/filter_rating.dart';
import 'package:askany/src/ui/review_service/widgets/review_card.dart';
import 'package:askany/src/ui/review_service/widgets/review_shimmer_card.dart';
import 'package:askany/src/ui/review_service/widgets/review_shimmer_list.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ReviewExpertScreen extends StatefulWidget {
  final ExpertModel expertModel;
  final List<int> ratingSingleCounts;
  ReviewExpertScreen({required this.expertModel, required this.ratingSingleCounts});
  @override
  State<ReviewExpertScreen> createState() => _ReviewExpertScreenState();
}

class _ReviewExpertScreenState extends State<ReviewExpertScreen> {
  int _stars = -1;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarTitleBack(context, Strings.reviewsAppbartext.i18n),
      body: SafeArea(
        bottom: false,
        child: Container(
          child: Column(
            children: [
              dividerChat,
              SizedBox(height: 16.sp),
              FilterRating(
                ratingStar: widget.ratingSingleCounts,
                handlePressed: (val) {
                  setState(() {
                    _stars = val;
                  });
                },
              ),
              SizedBox(height: 16.sp),
              BlocBuilder<RatingBloc, RatingState>(
                buildWhen: (previous, current) => previous != current,
                builder: (context, state) {
                  if (state is GetDoneRatingExpert || state is GettingRatingExpert) {
                    Map<String, RatingStoreModel>? listRating =
                        state.props[0] as Map<String, RatingStoreModel>?;
                    return Expanded(
                      child: listRating?[widget.expertModel.id] != null &&
                              (listRating?[widget.expertModel.id]
                                          ?.getRatingsByStars(_stars)
                                          .ratings
                                          .length ??
                                      0) >
                                  0
                          ? Container(
                              color: Colors.transparent,
                              padding: EdgeInsets.symmetric(horizontal: 16.sp),
                              child: PaginationListView(
                                padding: EdgeInsets.only(bottom: 18.sp),
                                physics: BouncingScrollPhysics(),
                                itemCount: listRating?[widget.expertModel.id]
                                        ?.getRatingsByStars(_stars)
                                        .ratings
                                        .length ??
                                    0,
                                itemBuilder: (context, index) {
                                  return ReviewCard(
                                    ratingModel: listRating![widget.expertModel.id]!
                                        .getRatingsByStars(_stars)
                                        .ratings[index],
                                    isCheckLastReview: index ==
                                        listRating[widget.expertModel.id]!
                                                .getRatingsByStars(_stars)
                                                .ratings
                                                .length -
                                            1,
                                  );
                                },
                                callBackLoadMore: () {
                                  AppBloc.ratingBloc.add(
                                    GetRatingExpertEvent(),
                                  );
                                },
                                callBackRefresh: (Function handleFinished) {
                                  AppBloc.ratingBloc.add(
                                    RefreshRatingExpertEvent(
                                      handleFinished: handleFinished,
                                    ),
                                  );
                                },
                                isLoadMore: state is GettingRatingExpert,
                                childShimmer: ReviewShimmerCard(),
                              ),
                            )
                          : Empty(
                              image: Image.asset(
                                imageSkillEmpty,
                                width: 120.sp,
                                height: 80.sp,
                              ),
                              text: Strings.thereNoReviewYet.i18n,
                              backgroundColor: Colors.transparent,
                            ),
                    );
                  }
                  return ReviewShimmerList();
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
