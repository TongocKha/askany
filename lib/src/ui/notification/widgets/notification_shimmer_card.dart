import 'package:askany/src/ui/common/widgets/shimmers/fade_simmer.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class NotificationShimmerCard extends StatelessWidget {
  const NotificationShimmerCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16.sp, vertical: 12.5.sp),
      decoration: BoxDecoration(color: Colors.white),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          FadeShimmer.round(
            size: 40.sp,
            fadeTheme: FadeTheme.lightReverse,
          ),
          SizedBox(width: 12.sp),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  alignment: Alignment.centerLeft,
                  child: FadeShimmer(
                    height: 13.sp,
                    width: 165.w,
                    fadeTheme: FadeTheme.lightReverse,
                  ),
                ),
                SizedBox(height: 2.sp),
                Container(
                  alignment: Alignment.centerLeft,
                  child: FadeShimmer(
                    height: 13.sp,
                    width: 15.w,
                    fadeTheme: FadeTheme.lightReverse,
                  ),
                ),
                SizedBox(height: 4.sp),
                Container(
                  alignment: Alignment.centerLeft,
                  child: FadeShimmer(
                    height: 11.sp,
                    width: 30.w,
                    fadeTheme: FadeTheme.lightReverse,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
