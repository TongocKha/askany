import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/ui/notification/widgets/notification_shimmer_card.dart';
import 'package:flutter/material.dart';

class NotificationsShimmersList extends StatelessWidget {
  const NotificationsShimmersList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      itemCount: ITEM_COUNT_SHIMMER * 3,
      itemBuilder: (context, index) => NotificationShimmerCard(),
    );
  }
}
