import 'package:askany/src/helpers/extentions/string_extention.dart';
import 'package:askany/src/models/notification_model.dart';
import 'package:askany/src/ui/common/widgets/custom_image/network_image/cached_image.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class NotificationCard extends StatefulWidget {
  final NotificationModel notificationModel;
  NotificationCard({required this.notificationModel});
  @override
  State<StatefulWidget> createState() => _NotificationCardState();
}

class _NotificationCardState extends State<NotificationCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16.sp, vertical: 12.5.sp),
      decoration: BoxDecoration(
        color: widget.notificationModel.isSeen ? Colors.white : backgroundNotification,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          CustomNetworkImage(
            height: 40.sp,
            width: 40.sp,
            urlToImage: widget.notificationModel.userNotification.avatar?.urlToImage,
          ),
          SizedBox(width: 12.sp),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                RichText(
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                  text: TextSpan(
                    style: TextStyle(
                      fontSize: 12.sp,
                      color: colorBlack1,
                    ),
                    children: [
                      ...widget.notificationModel.message
                          .getTitleNotification(widget.notificationModel)
                          .map((element) {
                        return TextSpan(
                          text: element.text,
                          style: TextStyle(
                            fontWeight: element.fontWeight,
                          ),
                        );
                      }),
                    ],
                  ),
                ),
                SizedBox(height: 4.sp),
                Text(
                  DateFormat('HH:mm dd/MM/yyyy').format(widget.notificationModel.createdAt),
                  style: TextStyle(
                    fontSize: 10.sp,
                    fontWeight: FontWeight.w400,
                    color: colorGray2,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
