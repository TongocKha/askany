import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/notification/notification_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/models/notification_model.dart';
import 'package:askany/src/models/slide_mode.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_slidable.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_confirm_cancel.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_loading.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_with_text_and_pop_button.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:askany/src/ui/common/widgets/empty/empty.dart';
import 'package:askany/src/ui/common/widgets/pagination_list_view.dart';
import 'package:askany/src/ui/notification/widgets/notification_card.dart';
import 'package:askany/src/ui/notification/widgets/notification_shimmer_card.dart';
import 'package:askany/src/ui/notification/widgets/notification_shimmer_list.dart';
import 'package:askany/src/ui/style/account_style.dart';
import 'package:askany/src/ui/style/calendar_style.dart';
import 'package:askany/src/ui/style/chat_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class NotificationScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  final SlidableController slidableController = new SlidableController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarBrightness: Theme.of(context).brightness,
        ),
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        automaticallyImplyLeading: false,
        title: Text(
          Strings.notification.i18n,
          style: TextStyle(
            fontSize: 17.sp,
            fontWeight: FontWeight.w700,
            color: colorBlack2,
          ),
        ),
        centerTitle: false,
        actions: [
          Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.only(
              right: 14.sp,
              top: 14.sp,
              bottom: 14.sp,
            ),
            child: TouchableOpacity(
              onTap: () {
                dialogAnimationWrapper(
                  borderRadius: 10.sp,
                  slideFrom: SlideMode.bot,
                  child: DialogConfirmCancel(
                    bodyBefore: Strings.markAllAsReadNoti.i18n,
                    confirmText: Strings.confirm.i18n,
                    cancelText: Strings.cancel.i18n,
                    onConfirmed: () {
                      AppNavigator.pop();
                      AppBloc.notificationBloc.add(MarkSeenAllNotificationEvent());
                    },
                  ),
                );
              },
              child: Text(
                Strings.readAll.i18n,
                style: TextStyle(
                  fontSize: 12.sp,
                  fontWeight: FontWeight.w400,
                  color: colorGray2,
                ),
              ),
            ),
          ),
        ],
      ),
      body: Container(
        child: BlocBuilder<NotificationBloc, NotificationState>(
          builder: (context, state) {
            List<NotificationModel> notifications = (state.props[0] as List).cast();

            return Column(
              children: [
                dividerChat,
                Expanded(
                  child: state is NotificationInitial
                      ? NotificationsShimmersList()
                      : notifications.length > 0
                          ? PaginationListView(
                              childShimmer: NotificationShimmerCard(),
                              callBackLoadMore: () {
                                AppBloc.notificationBloc.add(GetNotificationEvent());
                              },
                              callBackRefresh: (Function handleFinished) {
                                AppBloc.notificationBloc.add(RefreshNotificationEvent(
                                  handleFinished: handleFinished,
                                ));
                              },
                              isLoadMore: state is GettingNotification,
                              itemCount: notifications.length,
                              itemBuilder: (context, index) {
                                return Slidable(
                                  actionPane: SlidableScrollActionPane(),
                                  actionExtentRatio: 0.22,
                                  controller: slidableController,
                                  closeOnScroll: true,
                                  actions: [
                                    ButtonSlidable(
                                      imageAsset: iconDelete,
                                      title: Strings.delete.i18n,
                                      handlePressed: () {
                                        slidableController.activeState?.close();
                                        showDialogLoading();
                                        AppBloc.notificationBloc.add(
                                          DeleteNotificationEvent(
                                            notificationId: notifications[index].id,
                                          ),
                                        );
                                      },
                                      backgroundcolor: Colors.redAccent,
                                      colorIcon: Colors.white,
                                    ),
                                  ],
                                  child: TouchableOpacity(
                                    onTap: () {
                                      slidableController.activeState?.close();
                                      if (notifications[index].offer?.id != null) {
                                        AppBloc.notificationBloc.add(
                                          TouchNotificationEvent(
                                            offerId: notifications[index].offer!.id,
                                          ),
                                        );
                                      } else {
                                        dialogAnimationWrapper(
                                          slideFrom: SlideMode.bot,
                                          child: DialogWithTextAndPopButton(
                                            title: Strings.errorOccurredNoti.i18n,
                                            bodyAfter:
                                                'Chào giá đã bị huỷ bởi chuyên gia, bây giờ bạn không thể xem chi tiết dịch vụ này.',
                                          ),
                                        );
                                      }
                                      if (notifications[index].status == 0) {
                                        AppBloc.notificationBloc.add(
                                          MarkSeenNotificationEvent(
                                            notificationId: notifications[index].id,
                                          ),
                                        );
                                      }
                                    },
                                    child: NotificationCard(
                                      notificationModel: notifications[index],
                                    ),
                                  ),
                                );
                              },
                            )
                          : Empty(
                              image: Image.asset(
                                imageNotificationEmpty,
                                height: 84.sp,
                                width: 124.sp,
                              ),
                              text: Strings.youDontHaveAnyNoti.i18n,
                            ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
