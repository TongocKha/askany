import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class SearchManyCard extends StatelessWidget {
  final String keySearch;
  final bool isHotSearch;
  final bool isTop;
  final Function handlePressed;
  SearchManyCard({
    required this.keySearch,
    required this.isHotSearch,
    required this.isTop,
    required this.handlePressed,
  });

  @override
  Widget build(BuildContext context) {
    return TouchableOpacity(
      onTap: handlePressed,
      child: Container(
        color: Colors.transparent,
        padding: EdgeInsets.symmetric(horizontal: 16.sp),
        child: Column(
          children: [
            Container(
              width: 100.w,
              color: colorBorderTextField,
              height: 0.5.sp,
            ),
            Row(
              children: [
                Container(
                  height: 45.sp,
                  alignment: Alignment.centerLeft,
                  child: Text(
                    keySearch,
                    style: TextStyle(
                      fontSize: 12.sp,
                      color: colorGray1,
                    ),
                  ),
                ),
                SizedBox(width: 9.sp),
                Visibility(
                  visible: isHotSearch,
                  child: Image.asset(
                    iconFire,
                    width: 10.sp,
                    height: 13.sp,
                    color: isTop ? colorBackgroundBadges : null,
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
