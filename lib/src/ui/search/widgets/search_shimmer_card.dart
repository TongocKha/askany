import 'package:askany/src/ui/common/widgets/shimmers/fade_simmer.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class SearchShimmerCard extends StatelessWidget {
  const SearchShimmerCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.sp, vertical: 20.sp),
      margin: EdgeInsets.only(bottom: 10.sp),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(
            Radius.circular(10.sp),
          ),
          boxShadow: [
            BoxShadow(
              offset: Offset(1, 1),
              color: Colors.black.withOpacity(.08),
              blurRadius: 4,
            )
          ]),
      child: Column(
        children: [
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                FadeShimmer.round(
                  size: 36.sp,
                  fadeTheme: FadeTheme.lightReverse,
                ),
                SizedBox(
                  width: 10.sp,
                ),
                FadeShimmer(
                  width: 100.sp,
                  height: 15.sp,
                  fadeTheme: FadeTheme.lightReverse,
                ),
                SizedBox(width: 25.sp),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.only(top: 1.sp),
                    alignment: Alignment.topRight,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        FadeShimmer(
                          width: 40.sp,
                          height: 12.sp,
                          fadeTheme: FadeTheme.lightReverse,
                        ),
                        SizedBox(height: 5.sp),
                        FadeShimmer(
                          width: 35.sp,
                          height: 10.sp,
                          fadeTheme: FadeTheme.lightReverse,
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
          SizedBox(height: 10.sp),
          Container(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      FadeShimmer(
                        width: 120.sp,
                        height: 10.sp,
                        fadeTheme: FadeTheme.lightReverse,
                      ),
                      SizedBox(height: 7.sp),
                      FadeShimmer(
                        width: 100.sp,
                        height: 10.sp,
                        fadeTheme: FadeTheme.lightReverse,
                      ),
                      SizedBox(height: 7.sp),
                      FadeShimmer(
                        width: 120.sp,
                        height: 10.sp,
                        fadeTheme: FadeTheme.lightReverse,
                      ),
                    ],
                  ),
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    FadeShimmer(
                      width: 25.sp,
                      height: 20.sp,
                      fadeTheme: FadeTheme.lightReverse,
                    ),
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
