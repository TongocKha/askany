import 'dart:core';

import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/data/local_data_source/search_local_data.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class NearbySearch extends StatefulWidget {
  final List<String> listTagsSearch;
  final Function(bool) handlePressedClear;
  final Function(String) handlePressed;
  NearbySearch({
    required this.listTagsSearch,
    required this.handlePressedClear,
    required this.handlePressed,
  });

  @override
  _NearbySearchState createState() => _NearbySearchState();
}

class _NearbySearchState extends State<NearbySearch> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.symmetric(
              vertical: 17.sp,
              horizontal: 16.sp,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  Strings.recentSearching.i18n,
                  style: TextStyle(
                    color: colorBlack2,
                    fontSize: 14.sp,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                TouchableOpacity(
                  onTap: () {
                    SearchLocal().removeSearch();
                    setState(() {
                      widget.listTagsSearch.clear();
                      widget.handlePressedClear(false);
                    });
                  },
                  child: Text(
                    Strings.clearAll.i18n,
                    style: TextStyle(
                      color: colorGreen2,
                      fontSize: 12.sp,
                    ),
                  ),
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.zero,
            padding: EdgeInsets.symmetric(horizontal: 16.sp),
            child: Wrap(
              spacing: 14.0.sp,
              runSpacing: 14.0.sp,
              alignment: WrapAlignment.start,
              crossAxisAlignment: WrapCrossAlignment.start,
              children: List<Widget>.generate(
                  widget.listTagsSearch.length > 6
                      ? 6
                      : widget.listTagsSearch.length, (index) {
                return TouchableOpacity(
                  onTap: () {
                    widget.handlePressed(widget.listTagsSearch[index]);
                  },
                  child: Container(
                    child: Chip(
                      padding: EdgeInsets.all(8.sp),
                      backgroundColor: colorAddButton,
                      label: Text(
                        widget.listTagsSearch[index],
                        style: TextStyle(fontSize: 12.sp),
                      ),
                      onDeleted: () {
                        SearchLocal()
                            .removeSearchKey(widget.listTagsSearch[index]);
                        setState(() {
                          widget.listTagsSearch.removeAt(index);
                          widget.handlePressedClear(
                              widget.listTagsSearch.isNotEmpty);
                        });
                      },
                      deleteIcon: ImageIcon(
                        AssetImage(iconRemove),
                        size: 8.sp,
                        color: colorGray1,
                      ),
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    ),
                  ),
                );
              }),
            ),
          ),
          SizedBox(height: 17.sp),
          dividerThinkness6NotMargin,
        ],
      ),
    );
  }
}
