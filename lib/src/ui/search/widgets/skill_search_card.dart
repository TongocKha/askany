import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/data/local_data_source/search_local_data.dart';
import 'package:askany/src/helpers/province_helper.dart';
import 'package:askany/src/models/expert_model.dart';
import 'package:askany/src/models/skill_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/cake_avatar/cached_cake_image.dart';
import 'package:askany/src/ui/common/widgets/text_ui/text_ui.dart';
import 'package:askany/src/ui/style/calendar_style.dart';
import 'package:askany/src/ui/style/home_style.dart';
import 'package:askany/src/ui/style/request_style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class SkillSearchCard extends StatelessWidget {
  final SkillModel skillModel;
  final ExpertModel? expertModel;
  SkillSearchCard({required this.skillModel, this.expertModel});

  @override
  Widget build(BuildContext context) {
    return TouchableOpacity(
      onTap: () {
        if (AppBloc.skillBloc.searchSkill != null &&
            AppBloc.skillBloc.searchSkill!.search.length > SINGLE_CHARACTER) {
          SearchLocal().saveSearch(AppBloc.skillBloc.searchSkill!.search);
        }
        AppNavigator.push(Routes.DETAIL_SKILL_SEARCH, arguments: {
          'skillModel': skillModel,
          'expertModel': expertModel,
        });
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10.sp, vertical: 20.sp),
        margin: EdgeInsets.only(bottom: 10.sp),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(
              Radius.circular(10.sp),
            ),
            boxShadow: [
              BoxShadow(
                offset: Offset(1, 1),
                color: Colors.black.withOpacity(.08),
                blurRadius: 4,
              )
            ]),
        child: Column(
          children: [
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  CachedCakeImage(
                    fit: BoxFit.cover,
                    urlToImage: expertModel?.avatar?.urlToImage ??
                        skillModel.author?.avatar?.urlToImage,
                    size: 36.sp,
                    radius: BorderRadius.all(
                      Radius.circular(20.sp),
                    ),
                  ),
                  SizedBox(
                    width: 10.sp,
                  ),
                  Expanded(
                    child: Text(
                      skillModel.name,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: colorBlack1,
                        fontSize: 13.sp,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  SizedBox(width: 8.sp),
                  Container(
                    padding: EdgeInsets.only(top: 1.sp),
                    alignment: Alignment.topRight,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          skillModel.callPriceCost != null
                              ? skillModel.callPrice!.costString
                              : skillModel.meetPrice!.costString,
                          style: TextStyle(
                            color: colorBlack1,
                            fontSize: 11.sp,
                            fontWeight: FontWeight.w600,
                            height: 1.4,
                          ),
                        ),
                        Text(
                          '/${skillModel.callPriceCost != null ? skillModel.callPrice!.totalMinutes : skillModel.meetPrice!.totalMinutes} ${Strings.minutes.i18n}',
                          style: TextStyle(
                            color: colorGray1,
                            fontSize: 9.sp,
                            fontWeight: FontWeight.w400,
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: 10.sp),
            Container(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        _infoServiceCard(
                            text:
                                '${ProvinceHelper().getProvinceByCode(expertModel?.province ?? skillModel.author?.province ?? 0)}'),
                        SizedBox(height: 5.sp),
                        _infoServiceCard(
                            text: '${skillModel.expExperience} ' +
                                (skillModel.expExperience > 1
                                    ? '${Strings.yearsOfExperience.i18n}'
                                    : '${Strings.yearOfExperience.i18n}')),
                        SizedBox(height: 5.sp),
                        _infoServiceCard(text: 'Chuyên viên tư vấn du học'),
                      ],
                    ),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(bottom: 2.5.sp),
                        child: Image.asset(
                          iconStar,
                          height: 12.5.sp,
                          width: 12.5.sp,
                          color: colorStar,
                        ),
                      ),
                      SizedBox(width: 5.sp),
                      Container(
                        margin: EdgeInsets.only(top: 2.sp),
                        child: Text(
                          '${(skillModel.stars ?? 0.0)}',
                          style: TextStyle(
                            color: colorGray2,
                            fontSize: 11.sp,
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _infoServiceCard({required String text}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Image.asset(
          iconDot,
          width: 4.5.sp,
          height: 4.5.sp,
          color: colorGray1,
        ),
        SizedBox(width: 2.5.sp),
        TextUI(
          text,
          fontSize: 11.sp,
          fontWeight: FontWeight.w400,
          color: colorGray1,
        ),
      ],
    );
  }
}
