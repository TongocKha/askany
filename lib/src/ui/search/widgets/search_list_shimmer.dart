import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/ui/search/widgets/search_shimmer_card.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class SearchListShimmer extends StatelessWidget {
  const SearchListShimmer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: EdgeInsets.symmetric(horizontal: 16.sp, vertical: 17.sp),
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: ITEM_COUNT_SHIMMER,
      itemBuilder: (BuildContext context, int index) {
        return SearchShimmerCard();
      },
    );
  }
}
