import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/helpers/province_helper.dart';
import 'package:askany/src/models/expert_model.dart';
import 'package:askany/src/routes/app_navigator_observer.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/common/widgets/divider_bottom_sheet.dart';
import 'package:askany/src/ui/review_service/widgets/info_expert_card.dart';
import 'package:askany/src/ui/review_service/widgets/review_header.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class BottomSheetInfoExpert extends StatelessWidget {
  final ExpertModel expert;
  BottomSheetInfoExpert({required this.expert});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100.w,
      padding: EdgeInsets.symmetric(horizontal: 16.sp),
      decoration: BoxDecoration(
        color: colorWhite,
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(18.sp),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(height: 5.sp),
          Align(
            alignment: Alignment.center,
            child: DividerBottomSheet(),
          ),
          SizedBox(height: 25.sp),
          ReviewHeader(
            distanceTextAndStar: 8.sp,
            urlImage: expert.avatar?.urlToImage,
            username: expert.fullname,
            ratingCount: expert.ratingCount,
            sizeUrlImage: 45,
            sizeStar: 13,
            colorUsername: colorBlack1,
            marginStar: EdgeInsets.only(right: 5.sp),
            stars: expert.stars,
          ),
          SizedBox(height: 15.sp),
          InfoExpertCard(
            icon: iconLocationRounded,
            title: '${Strings.addressTitle.i18n}:',
            description:
                '${ProvinceHelper().getProvinceByCode(expert.province)}',
          ),
          SizedBox(height: 15.sp),
          InfoExpertCard(
            icon: iconDictionary,
            title: '${Strings.languages.i18n}:',
            description: expert.languagesTile,
          ),
          SizedBox(height: 15.sp),
          InfoExpertCard(
            icon: iconExperience,
            title: '${Strings.yearsCountOfExperienceHinttext.i18n}:',
            description: '${expert.experienceYears}',
          ),
          SizedBox(height: 15.sp),
          InfoExpertCard(
            icon: iconPosition,
            title: '${Strings.currentPosition.i18n}:',
            description:
                '${Strings.anExpertIn.i18n} ${expert.expertSpecialist}',
          ),
          SizedBox(height: 15.sp),
          InfoExpertCard(
            icon: iconCompany,
            title: '${Strings.currentCompany.i18n}:',
            description: expert.companyName,
          ),
          SizedBox(height: 15.sp),
          dividerChat,
          SizedBox(height: 15.sp),
          Text(
            expert.introduce,
            style: TextStyle(
              color: colorBlack2,
              fontSize: 12.sp,
              height: 1.28.sp,
            ),
          ),
          SizedBox(height: 18.sp),
          ButtonPrimary(
            onPressed: () {
              if (AppNavigatorObserver.routeNames
                      .contains(Routes.DETAILS_SPECIALIST) &&
                  AppNavigatorObserver.routeNames
                          .where((route) => route == Routes.DETAIL_SKILL_SEARCH)
                          .length ==
                      1) {
                AppNavigator.popUntil(Routes.DETAILS_SPECIALIST);
              } else {
                AppNavigator.push(Routes.DETAILS_SPECIALIST, arguments: {
                  'expertId': expert.id,
                  'specialtyId': expert.specialties.isNotEmpty
                      ? expert.specialties.first.id
                      : null,
                });
              }
            },
            text: Strings.viewAll.i18n,
            fontWeight: FontWeight.w700,
          ),
          SizedBox(height: 18.sp),
        ],
      ),
    );
  }
}
