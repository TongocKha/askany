import 'dart:async';

import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/hot_searched/hot_searched_bloc.dart';
import 'package:askany/src/bloc/skill/skill_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/data/local_data_source/search_local_data.dart';
import 'package:askany/src/models/search_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_none.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/search/widgets/nearby_search.dart';
import 'package:askany/src/ui/search/screens/result_screen.dart';
import 'package:askany/src/ui/search/widgets/search_many_card.dart';

import 'package:askany/src/ui/style/style.dart';

import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SearchScreen extends StatefulWidget {
  SearchScreen({Key? key}) : super(key: key);

  @override
  State<SearchScreen> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  final _searchController = TextEditingController();
  String _searchKey = '';
  Timer? _debounce;

  List<String> _listTagsSearch = [];

  bool isShowHistorySearch = false;
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _debounce?.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: true,
        resizeToAvoidBottomInset: false,
        appBar: appBarBrighnessDark(brightness: Brightness.light),
        body: SafeArea(
          bottom: false,
          child: Column(
            children: [
              _appBarInput(),
              dividerChat,
              Expanded(
                  child: _searchKey.isEmpty
                      ? _emptySearchScreen()
                      : ResultScreen()),
            ],
          ),
        ));
  }

  Widget _appBarInput() {
    return Container(
      width: 100.w,
      decoration: BoxDecoration(color: Colors.white),
      alignment: Alignment.bottomCenter,
      padding: EdgeInsets.only(right: 16.sp),
      child: Row(
        children: [
          Expanded(
            child: TextFormField(
              controller: _searchController,
              cursorColor: headerCalendarColor,
              style: TextStyle(
                color: Colors.black,
                fontSize: 12.sp,
              ),
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: Strings.whatAreYouLookingFor.i18n,
                hintStyle: TextStyle(
                  color: colorGray2,
                  fontSize: 12.sp,
                ),
                contentPadding: EdgeInsets.only(
                  top: 8.25.sp,
                ),
                prefixIconConstraints: BoxConstraints(
                  maxHeight: 33.sp,
                  maxWidth: 33.sp,
                ),
                prefixIcon: TouchableOpacity(
                  onTap: () {
                    AppNavigator.pop();
                  },
                  child: Image.asset(
                    iconBack,
                    width: 33.sp,
                    height: 33.sp,
                    color: colorGray1,
                  ),
                ),
              ),
              onChanged: (value) {
                if (_debounce?.isActive ?? false) _debounce?.cancel();
                _debounce = Timer(
                  const Duration(milliseconds: DELAY_HALF_SECOND),
                  () {
                    AppBloc.skillBloc.add(
                      SearchSkillEvent(
                        searchModel: SearchModel(
                          search: value.trim(),
                        ),
                      ),
                    );

                    setState(() {
                      _searchKey = value;
                      _listTagsSearch.add(value);
                    });
                  },
                );
              },
            ),
          ),
          TouchableOpacity(
            child: _searchController.text.isNotEmpty
                ? Image.asset(
                    iconClear,
                    width: 15.sp,
                    height: 15.sp,
                  )
                : Container(),
            onTap: () {
              setState(() {
                _searchController.text = '';
                _searchKey = '';
              });
            },
          )
        ],
      ),
    );
  }

  Widget _emptySearchScreen() {
    return Container(
      height: 100.h,
      child: Column(
        children: [
          Visibility(
            visible: SearchLocal().listSearch.isNotEmpty || isShowHistorySearch,
            child: NearbySearch(
              listTagsSearch: SearchLocal()
                  .listSearch
                  .map((search) => search.searchKey)
                  .toList(),
              handlePressedClear: (bool) {
                setState(() {
                  isShowHistorySearch = bool;
                });
              },
              handlePressed: (value) {
                setState(() {
                  _searchKey = value;
                  _listTagsSearch.add(value);
                });

                _searchController.value = TextEditingValue(
                  text: value,
                  selection: TextSelection.fromPosition(
                    TextPosition(offset: value.length),
                  ),
                );

                AppBloc.skillBloc.add(
                  SearchSkillEvent(
                    searchModel: SearchModel(
                      search: value.trim(),
                    ),
                  ),
                );
              },
            ),
          ),
          SizedBox(height: 17.sp),
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: 16.sp,
            ),
            alignment: Alignment.centerLeft,
            child: Text(
              Strings.trending.i18n,
              style: TextStyle(
                color: colorBlack2,
                fontSize: 14.sp,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          SizedBox(height: 17.sp),
          Expanded(
            child: BlocBuilder<HotSearchedBloc, HotSearchedState>(
              builder: (context, state) {
                List<String> _listSearchMany = state.props[0] as List<String>;
                return state is HotSearchedInitial
                    ? Container()
                    : ListView.builder(
                        physics: BouncingScrollPhysics(),
                        itemCount: _listSearchMany.length,
                        itemBuilder: (context, index) {
                          return SearchManyCard(
                            handlePressed: () {
                              AppBloc.skillBloc.add(
                                SearchSkillEvent(
                                  searchModel: SearchModel(
                                    search: _listSearchMany[index].trim(),
                                  ),
                                ),
                              );
                              _searchController.value = TextEditingValue(
                                text: _listSearchMany[index],
                                selection: TextSelection.fromPosition(
                                  TextPosition(
                                      offset: _listSearchMany[index].length),
                                ),
                              );
                              setState(() {
                                _searchKey = _listSearchMany[index];
                                _listTagsSearch.add(_listSearchMany[index]);
                              });
                            },
                            keySearch: _listSearchMany[index],
                            isHotSearch: index < 3,
                            isTop: index == 0,
                          );
                        },
                      );
              },
            ),
          ),
        ],
      ),
    );
  }
}
