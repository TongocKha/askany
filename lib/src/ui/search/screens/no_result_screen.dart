import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class NoResultScreen extends StatelessWidget {
  const NoResultScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
            height: 70.w,
            padding: EdgeInsets.only(
              top: 70.sp,
              left: 50.sp,
              right: 50.sp,
            ),
            alignment: Alignment.bottomCenter,
            child: Image.asset(
              imageSearchDefault,
              width: 160.sp,
              height: 80.sp,
            ),
          ),
          SizedBox(height: 20.sp),
          Container(
            height: 50.sp,
            alignment: Alignment.center,
            child: Text(
              Strings.thereNoResultNoti.i18n,
              style: TextStyle(color: colorGray1, fontSize: 12.sp),
            ),
          ),
          Expanded(
            child: Visibility(
              visible: !AppBloc.userBloc.getAccount.isExpert!,
              child: Container(
                alignment: Alignment.bottomCenter,
                margin: EdgeInsets.only(bottom: 90.sp),
                child: RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                    text: Strings
                        .youWantToFindSomeAdviseForMoreSpecialProblem.i18n,
                    style: TextStyle(
                      color: colorGray2,
                      fontSize: 12.sp,
                      fontWeight: FontWeight.w400,
                      height: 1.6,
                    ),
                    children: [
                      TextSpan(
                        text: Strings.let.i18n,
                        style: TextStyle(
                          color: colorGray2,
                          fontSize: 12.sp,
                          fontWeight: FontWeight.w400,
                          height: 1.6,
                        ),
                      ),
                      TextSpan(
                        text: Strings.createNewRequest.i18n.toLowerCase(),
                        recognizer: new TapGestureRecognizer()
                          ..onTap = () => AppNavigator.push(
                                Routes.CREATE_REQUEST,
                                arguments: {
                                  'isNavigationFromSearch': true,
                                },
                              ),
                        style: TextStyle(
                          color: colorGreen2,
                          fontSize: 12.sp,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      TextSpan(
                        text: Strings.now.i18n,
                        style: TextStyle(
                          color: colorGray2,
                          fontSize: 12.sp,
                          fontWeight: FontWeight.w400,
                          height: 1.6,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
