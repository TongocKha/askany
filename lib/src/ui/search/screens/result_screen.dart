import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/skill/skill_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/models/skill_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/pagination_list_view.dart';
import 'package:askany/src/ui/search/screens/no_result_screen.dart';
import 'package:askany/src/ui/search/widgets/search_list_shimmer.dart';
import 'package:askany/src/ui/search/widgets/search_shimmer_card.dart';
import 'package:askany/src/ui/search/widgets/skill_search_card.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ResultScreen extends StatelessWidget {
  ResultScreen();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SkillBloc, SkillState>(builder: (context, state) {
      List<SkillModel> skills = [];
      int totalResult = 0;
      if (state is SearchingSkill || state is SearchDoneSkill) {
        skills = state.props[0];
        totalResult = state.props[1];
      }

      return Container(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: 16.sp,
                vertical: 20.sp,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    '$totalResult' +
                        (totalResult > 1
                            ? ' ${Strings.readyServicesCount.i18n}'
                            : ' ${Strings.readyServiceCount.i18n}'),
                    style: TextStyle(
                      color: colorGray1,
                      fontSize: 12.sp,
                    ),
                  ),
                  TouchableOpacity(
                    onTap: () async {
                      await AppNavigator.push(Routes.FILTER);
                    },
                    child: Container(
                      color: Colors.transparent,
                      height: 15.sp,
                      width: 15.sp,
                      child: Image.asset(iconFilter),
                    ),
                  )
                ],
              ),
            ),
            Expanded(
              child: Container(
                alignment: Alignment.topCenter,
                decoration: BoxDecoration(
                  color: backgroundTimelineDone,
                  borderRadius: BorderRadius.horizontal(
                      left: Radius.circular(17.sp),
                      right: Radius.circular(17.sp)),
                ),
                child: state is SkillInitial
                    ? SearchListShimmer()
                    : skills.isEmpty
                        ? NoResultScreen()
                        : PaginationListView(
                            padding: EdgeInsets.symmetric(
                                horizontal: 16.sp, vertical: 17.sp),
                            physics: BouncingScrollPhysics(),
                            itemCount: skills.length,
                            itemBuilder: (context, index) {
                              return SkillSearchCard(skillModel: skills[index]);
                            },
                            childShimmer: SearchShimmerCard(),
                            callBackLoadMore: () {
                              AppBloc.skillBloc.add(GetMoreSearchEvent());
                            },
                            callBackRefresh: (Function handleFinished) {
                              AppBloc.skillBloc.add(
                                RefreshSkillSearchEvent(
                                  handleFinished: handleFinished,
                                ),
                              );
                            },
                            isLoadMore: state is SearchingSkill,
                          ),
              ),
            ),
          ],
        ),
      );
    });
  }
}
