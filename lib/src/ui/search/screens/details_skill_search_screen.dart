import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/expert_info/expert_info_bloc.dart';
import 'package:askany/src/bloc/rating/rating_bloc.dart';
import 'package:askany/src/bloc/recommend_skill/recommend_skill_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/helpers/province_helper.dart';
import 'package:askany/src/models/expert_model.dart';
import 'package:askany/src/models/rating_management_model.dart';
import 'package:askany/src/models/skill_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/routes/app_routes.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_title_back.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/button_primary.dart';
import 'package:askany/src/ui/common/widgets/button_widgets/touchable_opacity.dart';
import 'package:askany/src/ui/common/widgets/cake_avatar/cached_cake_image.dart';
import 'package:askany/src/ui/common/widgets/empty/empty.dart';
import 'package:askany/src/ui/review_service/widgets/rating_and_text_review.dart';
import 'package:askany/src/ui/review_service/widgets/review_card.dart';
import 'package:askany/src/ui/search/widgets/bottom_sheeet_contact_skill.dart';
import 'package:askany/src/ui/search/widgets/search_list_shimmer.dart';
import 'package:askany/src/ui/search/widgets/skill_search_card.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class DetailsSkillSearchScreen extends StatefulWidget {
  final SkillModel skillModel;
  final ExpertModel? expertModel;
  const DetailsSkillSearchScreen({required this.skillModel, this.expertModel});
  @override
  State<DetailsSkillSearchScreen> createState() => _DetailsSkillSearchScreenState();
}

class _DetailsSkillSearchScreenState extends State<DetailsSkillSearchScreen> {
  @override
  void initState() {
    super.initState();
    AppBloc.ratingBloc.add(OnRatingSkillEvent(skillId: widget.skillModel.id!));

    AppBloc.recommendSkillBloc
        .add(GetListSkillRecommendEvent(keyWord: widget.skillModel.keywords[0]));
    AppBloc.expertInfoBloc.add(
      GetInfoExpertEvent(
        expertId: widget.skillModel.expertId ?? widget.skillModel.author?.id ?? '',
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final int time = widget.skillModel.callPriceCost != null
        ? widget.skillModel.callPrice!.totalMinutes
        : widget.skillModel.meetPrice!.totalMinutes;
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: appBarTitleBack(context, ''),
      body: SafeArea(
          bottom: false,
          child: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                  physics: BouncingScrollPhysics(),
                  child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 10.sp),
                          padding: EdgeInsets.symmetric(horizontal: 16.sp),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text(
                                widget.skillModel.name,
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  color: colorBlack1,
                                  fontSize: 17.sp,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                              SizedBox(height: 13.sp),
                              Container(
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Text(
                                      widget.skillModel.callPrice != null
                                          ? widget.skillModel.callPrice!.costString
                                          : widget.skillModel.meetPrice!.costString,
                                      style: TextStyle(
                                        color: colorBlack1,
                                        fontSize: 13.sp,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                    Text(
                                      ' /$time ${Strings.minutes.i18n} (${Strings.minimum.i18n} $time ${Strings.minutes.i18n})',
                                      style: TextStyle(
                                        color: colorBlack2,
                                        fontSize: 11.sp,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              SizedBox(height: 5.sp),
                              TouchableOpacity(
                                onTap: () {},
                                child: RatingAndTextReview(
                                  ratingCount: widget.skillModel.ratingCount ?? 0,
                                  stars: widget.skillModel.stars ?? 0.0,
                                ),
                              ),
                              SizedBox(height: 20.sp),
                              dividerChat,
                              SizedBox(height: 16.sp),
                              Text(
                                widget.skillModel.description,
                                textAlign: TextAlign.justify,
                                style: TextStyle(
                                  fontSize: 12.sp,
                                  color: colorBlack2,
                                  height: 1.4.sp,
                                ),
                              ),
                              SizedBox(height: 26.sp),
                            ],
                          ),
                        ),
                        dividerThinkness6NotMargin,
                        SizedBox(
                          height: 16.sp,
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 16.sp),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                Strings.expert.i18n,
                                style: TextStyle(
                                  color: colorBlack1,
                                  fontSize: 14.sp,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                              SizedBox(height: 14.sp),
                              TouchableOpacity(
                                onTap: () {
                                  AppBloc.expertInfoBloc.add(
                                    ShowBottomSheetInfoEvent(
                                      context: context,
                                      expertId: widget.expertModel?.id ??
                                          widget.skillModel.author?.id ??
                                          '',
                                    ),
                                  );
                                },
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    CachedCakeImage(
                                      fit: BoxFit.cover,
                                      urlToImage: widget.expertModel?.avatar?.urlToImage ??
                                          widget.skillModel.author?.avatar?.urlToImage,
                                      size: 42.sp,
                                      radius: BorderRadius.all(
                                        Radius.circular(45.sp),
                                      ),
                                    ),
                                    SizedBox(width: 8.sp),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          Container(
                                            margin: EdgeInsets.only(top: 5.sp),
                                            child: Row(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              children: [
                                                SizedBox(width: 2.sp),
                                                Text(
                                                  widget.expertModel?.fullname ??
                                                      widget.skillModel.author!.fullname,
                                                  style: TextStyle(
                                                    color: colorBlack1,
                                                    fontSize: 13.sp,
                                                    fontWeight: FontWeight.w600,
                                                  ),
                                                ),
                                                SizedBox(width: 8.sp),
                                                Padding(
                                                  padding: EdgeInsets.only(top: 5.sp),
                                                  child: Image.asset(
                                                    iconCheckConfirm,
                                                    height: 10.sp,
                                                    width: 10.sp,
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                          SizedBox(height: 4.sp),
                                          Row(
                                            children: [
                                              Image.asset(
                                                iconLocation,
                                                height: 10.sp,
                                                color: colorGray1,
                                                width: 10.sp,
                                              ),
                                              SizedBox(width: 4.sp),
                                              Text(
                                                '${ProvinceHelper().getProvinceByCode(widget.expertModel?.province ?? widget.skillModel.author?.province ?? 0)}',
                                                style: TextStyle(
                                                  color: colorGray2,
                                                  fontSize: 11.sp,
                                                  fontWeight: FontWeight.w600,
                                                ),
                                              ),
                                            ],
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 18.sp),
                        TouchableOpacity(
                          onTap: () {
                            if (widget.skillModel.ratingCount != 0) {
                              AppNavigator.push(
                                Routes.REVIEWSSKILL,
                                arguments: {
                                  'skillId': widget.skillModel.id,
                                },
                              );
                            }
                          },
                          child: RatingAndTextReview(
                            padding: EdgeInsets.symmetric(horizontal: 16.sp),
                            ratingCount: widget.skillModel.ratingCount ?? 0,
                            stars: widget.skillModel.stars ?? 0.0,
                          ),
                        ),
                        SizedBox(height: 26.sp),
                        BlocBuilder<RatingBloc, RatingState>(
                          buildWhen: (previous, current) => previous != current,
                          builder: (context, state) {
                            if (state is GetDoneRatingSkill &&
                                state.ratings[widget.skillModel.id] != null) {
                              RatingStoreModel listRating =
                                  state.ratings[widget.skillModel.id] as RatingStoreModel;

                              return listRating.joinRatings.ratings.isNotEmpty
                                  ? Container(
                                      color: Colors.transparent,
                                      child: ListView.builder(
                                        padding: EdgeInsets.symmetric(horizontal: 16.sp),
                                        physics: NeverScrollableScrollPhysics(),
                                        itemCount: listRating.joinRatings.ratings.length >= 3
                                            ? 3
                                            : listRating.joinRatings.ratings.length,
                                        shrinkWrap: true,
                                        itemBuilder: (context, index) {
                                          return ReviewCard(
                                            ratingModel: listRating.joinRatings.ratings[index],
                                            isCheckLastReview: index ==
                                                (listRating.joinRatings.ratings.length >= 3
                                                    ? 2
                                                    : listRating.joinRatings.ratings.length - 1),
                                          );
                                        },
                                      ),
                                    )
                                  : Container();
                            }
                            return Container();
                          },
                        ),
                        dividerThinkness6NotMargin,
                        SizedBox(
                          height: 16.sp,
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 16.sp),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                Strings.otherServices.i18n,
                                style: TextStyle(
                                  color: colorBlack1,
                                  fontSize: 14.sp,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                              SizedBox(height: 16.sp),
                              BlocBuilder<RecommendSkillBloc, RecommendSkillState>(
                                builder: (context, state) {
                                  if (state is GettingSkillRecommend ||
                                      state is GetDoneSkillRecommend) {
                                    List<SkillModel> _recommendSkill =
                                        state.props[0] as List<SkillModel>;

                                    _recommendSkill = _recommendSkill
                                        .where((skill) => skill.id != widget.skillModel.id)
                                        .toList();
                                    return _recommendSkill.isNotEmpty
                                        ? Container(
                                            child: ListView.builder(
                                              physics: NeverScrollableScrollPhysics(),
                                              shrinkWrap: true,
                                              itemCount: _recommendSkill.length,
                                              itemBuilder: (context, index) {
                                                return SkillSearchCard(
                                                  skillModel: _recommendSkill[index],
                                                );
                                              },
                                            ),
                                          )
                                        : Empty(
                                            image: Image.asset(
                                              imageSkillEmpty,
                                              width: 120.sp,
                                              height: 80.sp,
                                            ),
                                            text: Strings.thereNoRecommendServicesNoti.i18n,
                                            backgroundColor: Colors.transparent,
                                          );
                                  }
                                  return SearchListShimmer();
                                },
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 20.sp),
                      ],
                    ),
                  ),
                ),
              ),
              dividerChat,
              Visibility(
                visible: UserLocal().getAccessToken() != '' && !UserLocal().getIsExpert(),
                child: Column(
                  children: [
                    dividerChat,
                    Container(
                      margin: EdgeInsets.only(top: 13.sp, bottom: 18.sp),
                      padding: EdgeInsets.symmetric(horizontal: 16.sp),
                      child: ButtonPrimary(
                        onPressed: () {
                          showModalBottomSheet(
                            context: context,
                            backgroundColor: Colors.transparent,
                            isScrollControlled: true,
                            builder: (context) {
                              return BottomSheetContactSkill(
                                skillModel: widget.skillModel,
                              );
                            },
                          );
                        },
                        text: Strings.contactNow.i18n,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          )),
    );
  }
}
