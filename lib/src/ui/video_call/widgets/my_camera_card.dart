import 'dart:math' as math;

import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/configs/themes/app_colors.dart';
import 'package:askany/src/ui/common/widgets/custom_image/network_image/cached_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart' as RTC;
import 'package:flutter_webrtc/flutter_webrtc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class MyCameraCard extends StatefulWidget {
  final RTC.RTCVideoRenderer localRenderer;
  final bool cameraEnable;
  final EdgeInsets margin;
  final double width;
  final double height;

  // status == 1 ( width: size.width / 3.75,)
  // status == 2  ( width: 40.w,height: 25.w)
  MyCameraCard({
    required this.localRenderer,
    required this.cameraEnable,
    required this.margin,
    required this.width,
    required this.height,
  });

  @override
  State<StatefulWidget> createState() => _MyCameraCardState();
}

class _MyCameraCardState extends State<MyCameraCard> {
  @override
  void initState() {
    super.initState();
    // widget.localRenderer.muted = true;
  }

  @override
  Widget build(BuildContext context) {
    if (widget.localRenderer.textureId == null || !widget.cameraEnable) {
      return Container(
        width: widget.width,
        height: widget.height,
        margin: widget.margin,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.sp),
          color: colorBlueGrey,
          boxShadow: [
            BoxShadow(color: colorBlack.withOpacity(0.1), spreadRadius: 3),
          ],
        ),
        child: Center(
          child: CustomNetworkImage(
            height: 50.sp,
            width: 50.sp,
            urlToImage: AppBloc.userBloc.getAccount.avatar?.urlToImage,
          ),
        ),
      );
    } else {
      return Container(
        // width: size.width / 3.75,
        width: widget.width,
        height: widget.height,
        margin: widget.margin,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.sp),
          color: Colors.black,
          boxShadow: [
            BoxShadow(color: colorBlack.withOpacity(0.1), spreadRadius: 3),
          ],
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10.sp),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.sp),
            ),
            child: new Transform(
              transform: Matrix4.identity()
                ..rotateY(
                  math.pi,
                ),
              alignment: FractionalOffset.center,
              child: new RTCVideoView(
                widget.localRenderer,
                objectFit: RTC.RTCVideoViewObjectFit.RTCVideoViewObjectFitCover,
              ),
            ),
          ),
        ),
      );
    }
  }
}
