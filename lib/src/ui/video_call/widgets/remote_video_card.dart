import 'dart:math' as math;

import 'package:askany/src/configs/themes/app_colors.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/models/conversation_model.dart';
import 'package:askany/src/ui/common/widgets/custom_image/network_image/cached_image.dart';
import 'package:askany/src/ui/style/call_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart' as RTC;
import 'package:flutter_webrtc/flutter_webrtc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class RemoteVideoCard extends StatefulWidget {
  final RTC.RTCVideoRenderer remoteRenderer;
  final int status;
  final bool remoteOnCamera;
  final EdgeInsets margin;
  final double width;
  final double height;
  final BorderRadius? borderRadius;
  final ConversationModel conversation;
  RemoteVideoCard({
    required this.remoteRenderer,
    required this.status,
    required this.remoteOnCamera,
    required this.margin,
    required this.width,
    required this.height,
    required this.conversation,
    this.borderRadius,
  });

  @override
  State<StatefulWidget> createState() => _RemoteVideoCardState();
}

class _RemoteVideoCardState extends State<RemoteVideoCard> {
  get borderRadius => widget.borderRadius;
  RTCVideoViewObjectFit remoteObjectFit = RTC.RTCVideoViewObjectFit.RTCVideoViewObjectFitCover;

  @override
  void initState() {
    super.initState();
    widget.remoteRenderer.onResize = () {
      if (this.mounted) {
        setState(() {
          remoteObjectFit = _getOpjectFit(renderer: widget.remoteRenderer);
        });
      } else {
        remoteObjectFit = _getOpjectFit(renderer: widget.remoteRenderer);
      }
    };
  }

  @override
  Widget build(BuildContext context) {
    if (widget.remoteRenderer.textureId == null || !widget.remoteOnCamera) {
      if (widget.status == 1)
        return Container(
          width: widget.width,
          height: widget.height,
          margin: widget.margin,
          decoration: BoxDecoration(
            borderRadius: borderRadius != null ? borderRadius : BorderRadius.circular(10.sp),
            color: colorBackgroundRemoteCamera,
            boxShadow: [
              BoxShadow(color: colorBlack.withOpacity(0.3), spreadRadius: 1),
            ],
          ),
          // colorPrimary`Black,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CustomNetworkImage(
                height: 100.sp,
                width: 100.sp,
                urlToImage: UserLocal().getIsExpert() ? (widget.conversation.users.isEmpty ? null : widget.conversation.users.first.avatar?.urlToImage) : widget.conversation.expert.avatar?.urlToImage,
              ),
              SizedBox(
                height: 13.sp,
              ),
              Text(
                UserLocal().getIsExpert() ? (widget.conversation.users.isEmpty ? widget.conversation.conversationTitle : (widget.conversation.users.first.fullname ?? '')) : (widget.conversation.expert.fullname ?? ''),
                style: TextStyle(color: Colors.white, fontSize: 15.sp),
              ),
            ],
          ),
        );
      else if (widget.status == 2) {
        return Container(
          //       width: size.width / 3.75,
          width: widget.width,
          height: widget.height,
          margin: widget.margin,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.sp),
            color: colorBlueGrey,
            boxShadow: [
              BoxShadow(color: colorBlack.withOpacity(0.1), spreadRadius: 3),
            ],
          ),

          child: Center(
            child: CustomNetworkImage(
              height: 60.sp,
              width: 60.sp,
              urlToImage: UserLocal().getIsExpert() ? (widget.conversation.users.isEmpty ? null : widget.conversation.users.first.avatar?.urlToImage) : widget.conversation.expert.avatar?.urlToImage,
            ),
          ),
        );
      }
    } else {
      if (widget.status == 1) {
        return Container(
          width: widget.width,
          height: widget.height,
          margin: widget.margin,
          decoration: BoxDecoration(
            borderRadius: borderRadius != null ? borderRadius : BorderRadius.circular(10.sp),
            // BorderRadius.circular(10.sp),
            color: colorBlueGrey,
          ),
          // colorPrimary`Black,
          child: ClipRRect(
            borderRadius: borderRadius != null ? borderRadius : BorderRadius.circular(10.sp),
            child: Container(
              child: new Transform(
                transform: Matrix4.identity()
                  ..rotateY(
                    math.pi,
                  ),
                alignment: FractionalOffset.center,
                child: new RTCVideoView(
                  widget.remoteRenderer,
                  objectFit: remoteObjectFit,
                ),
              ),
            ),
          ),
        );
      } else if (widget.status == 2) {
        return Container(
          width: widget.width,
          height: widget.height,
          margin: widget.margin,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.sp),
            color: colorBlueGrey,
            boxShadow: [
              BoxShadow(color: colorBlack.withOpacity(0.1), spreadRadius: 3),
            ],
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(10.sp),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.sp),
              ),
              child: Transform(
                transform: Matrix4.identity()
                  ..rotateY(
                    math.pi,
                  ),
                alignment: FractionalOffset.center,
                child: new RTCVideoView(
                  widget.remoteRenderer,
                  objectFit: remoteObjectFit,
                ),
              ),
            ),
          ),
        );
      }
    }
    return Container(
      child: new Transform(
        transform: Matrix4.identity()
          ..rotateY(
            math.pi,
          ),
        alignment: FractionalOffset.center,
        child: new RTCVideoView(
          widget.remoteRenderer,
          objectFit: remoteObjectFit,
        ),
      ),
    );
  }

  RTCVideoViewObjectFit _getOpjectFit({required RTCVideoRenderer renderer}) {
    return renderer.videoHeight < renderer.videoWidth ? RTC.RTCVideoViewObjectFit.RTCVideoViewObjectFitCover : RTC.RTCVideoViewObjectFit.RTCVideoViewObjectFitContain;
  }
}
