import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/video_call/video_call_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/models/slide_mode.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_confirm_cancel.dart';
import 'package:askany/src/ui/common/widgets/dialogs/dialog_wrapper.dart';
import 'package:askany/src/ui/common/widgets/text_ui/text_ui.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class TabMenuVertical extends StatefulWidget {
  final bool cameraEnabled;
  final bool micEnabled;
  final bool sharescreenEnabled;
  final AnimationController controller;

  TabMenuVertical(
      {required this.cameraEnabled,
      required this.micEnabled,
      required this.sharescreenEnabled,
      required this.controller});

  @override
  State<StatefulWidget> createState() => _TabMenuVerticalState();
}

class _TabMenuVerticalState extends State<TabMenuVertical> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(10.sp, 10.sp, 10.sp, 10.sp),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
              width: 45.sp,
              height: 45.sp,
              child: widget.micEnabled
                  ? ElevatedButton(
                      onPressed: () {
                        AppBloc.videoCallBloc.add(ToggleMicEvent());
                      },
                      child: Image.asset(iconEnableRecord, height: 60.sp, width: 60.sp),
                      style: ElevatedButton.styleFrom(
                        shape: CircleBorder(),
                        padding: EdgeInsets.all(5),
                        primary: colorBlack2, // <-- Button color
                        onPrimary: Colors.red, // <-- Splash color
                      ),
                    )
                  : ElevatedButton(
                      onPressed: () {
                        AppBloc.videoCallBloc.add(ToggleMicEvent());
                      },
                      child: Image.asset(iconDeniedRecord, height: 60.sp, width: 60.sp),
                      style: ElevatedButton.styleFrom(
                        shape: CircleBorder(),
                        padding: EdgeInsets.all(5),
                        primary: colorBlack2, // <-- Button color
                        onPrimary: Colors.red, // <-- Splash color
                      ),
                    )),
          SizedBox(width: 15.sp),
          SizedBox(
              width: 45.sp,
              height: 45.sp,
              child: widget.cameraEnabled
                  ? ElevatedButton(
                      onPressed: () {
                        AppBloc.videoCallBloc.add(ToggleVideoEvent());
                      },
                      child: Image.asset(iconEnableVideo, height: 60.sp, width: 60.sp),
                      style: ElevatedButton.styleFrom(
                        shape: CircleBorder(),
                        padding: EdgeInsets.all(5),
                        primary: colorBlack2, // <-- Button color
                        onPrimary: Colors.red, // <-- Splash color
                      ),
                    )
                  : ElevatedButton(
                      onPressed: () {
                        AppBloc.videoCallBloc.add(ToggleVideoEvent());
                      },
                      child: Image.asset(iconDeniedVideo, height: 60.sp, width: 60.sp),
                      style: ElevatedButton.styleFrom(
                        shape: CircleBorder(),
                        padding: EdgeInsets.all(5),
                        primary: colorBlack2, // <-- Button color
                        onPrimary: Colors.red, // <-- Splash color
                      ),
                    )),
          SizedBox(width: 15.sp),
          // SizedBox(
          //     width: 45.sp,
          //     height: 45.sp,
          //     child: ElevatedButton(
          //       onPressed: () {
          //         widget.sharescreenEnabled == false
          //             ? showAlertDialogFullScreen(
          //                 title:
          //                     "Ask Any sẽ bắt đầu chia sẻ mọi thứ hiển thị trên màn hình của bạn.",
          //                 content:
          //                     "Ask Any sẽ bắt đầu chia sẻ mọi thứ hiển thị trên màn hình của bạn.",
          //                 textLeft: "HỦY",
          //                 textRight: "BẮT ĐẦU CHIA SẺ",
          //                 onPressedButtonLeft: () {
          //                   AppNavigator.pop();
          //                 },
          //                 onPressedButtonRight: () {
          //                   AppBloc.videoCallBloc.add(StartShareScreenEvent());
          //                   AppNavigator.pop();
          //                 },
          //                 context: context)
          //             : showAlertDialogFullScreen(
          //                 title:
          //                     "Ask Any sẽ kết thúc chia sẻ mọi thứ hiển thị trên màn hình của bạn.",
          //                 content:
          //                     "Ask Any sẽ kết thúc chia sẻ mọi thứ hiển thị trên màn hình của bạn.",
          //                 textLeft: "HỦY",
          //                 textRight: "KẾT THÚC CHIA SẺ",
          //                 onPressedButtonLeft: () {
          //                   AppNavigator.pop();
          //                 },
          //                 onPressedButtonRight: () {
          //                   widget.controller
          //                       .reverse()
          //                       .then((value) => AppBloc.videoCallBloc.add(StopShareScreenEvent()));
          //                   AppNavigator.pop();
          //                 },
          //                 context: context);
          //       },
          //       child: widget.sharescreenEnabled == false
          //           ? Image.asset(iconMenuShareScreen, height: 60.sp, width: 60.sp)
          //           : Image.asset(iconEnableShareScreen, height: 60.sp, width: 60.sp),
          //       style: ElevatedButton.styleFrom(
          //         shape: CircleBorder(),
          //         padding: EdgeInsets.all(5),
          //         primary: colorBlack2, // <-- Button color
          //         onPrimary: Colors.red, // <-- Splash color
          //       ),
          //     )),
          SizedBox(
              width: 45.sp,
              height: 45.sp,
              child: GestureDetector(
                onTap: () {
                  showModalBottomSheet<void>(
                    context: context,
                    backgroundColor: Colors.transparent,
                    builder: (BuildContext context) {
                      return Container(
                        clipBehavior: Clip.antiAlias,
                        height: 140.sp,
                        decoration: BoxDecoration(
                          color: colorBackgroundBottomSheet,
                          borderRadius: BorderRadius.vertical(
                            top: Radius.circular(18.sp),
                          ),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            SizedBox(
                              height: 5.sp,
                            ),
                            Container(
                              height: 2.sp,
                              width: 60.sp,
                              decoration: BoxDecoration(
                                color: colorBackgroundGrey,
                                borderRadius: BorderRadius.circular(30),
                              ),
                            ),
                            SizedBox(
                              height: 20.sp,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Expanded(
                                  child: Column(
                                    children: [
                                      ElevatedButton(
                                        onPressed: () {
                                          AppNavigator.pop();
                                          widget.sharescreenEnabled == false
                                              ? dialogAnimationWrapper(
                                                  // title:
                                                  //     "Ask Any sẽ bắt đầu chia sẻ mọi thứ hiển thị trên màn hình của bạn.",
                                                  // content:
                                                  //     "Ask Any sẽ bắt đầu chia sẻ mọi thứ hiển thị trên màn hình của bạn.",
                                                  // textLeft: "HỦY",
                                                  // textRight: "BẮT ĐẦU CHIA SẺ",
                                                  // onPressedButtonLeft: () {
                                                  //   AppNavigator.pop();
                                                  // },
                                                  // onPressedButtonRight: () {
                                                  //   AppBloc.videoCallBloc
                                                  //       .add(StartShareScreenEvent());
                                                  //   AppNavigator.pop();
                                                  // },
                                                  // context: context)
                                                  slideFrom: SlideMode.bot,
                                                  child: DialogConfirmCancel(
                                                    bodyBefore: Strings.startSharingNoti.i18n,
                                                    bodyColor: colorGray1,
                                                    cancelText: Strings.cancel.i18n.toUpperCase(),
                                                    confirmText:
                                                        Strings.startSharing.i18n.toUpperCase(),
                                                    onConfirmed: () {
                                                      AppBloc.videoCallBloc
                                                          .add(StartShareScreenEvent());
                                                      AppNavigator.pop();
                                                    },
                                                  ),
                                                )
                                              : dialogAnimationWrapper(
                                                  // title:
                                                  //     "Ask Any sẽ kết thúc chia sẻ mọi thứ hiển thị trên màn hình của bạn.",
                                                  // content:
                                                  //     "Ask Any sẽ kết thúc chia sẻ mọi thứ hiển thị trên màn hình của bạn.",
                                                  // textLeft: "HỦY",
                                                  // textRight: "KẾT THÚC CHIA SẺ",
                                                  // onPressedButtonLeft: () {
                                                  //   AppNavigator.pop();
                                                  // },
                                                  // onPressedButtonRight: () {
                                                  //   widget.controller.reverse().then((value) =>
                                                  //       AppBloc.videoCallBloc
                                                  //           .add(StopShareScreenEvent()));
                                                  //   AppNavigator.pop();
                                                  // },
                                                  // context: context);
                                                  slideFrom: SlideMode.bot,
                                                  child: DialogConfirmCancel(
                                                    bodyBefore: Strings.startSharingNoti.i18n,
                                                    bodyColor: colorGray1,
                                                    cancelText: Strings.cancel.i18n.toUpperCase(),
                                                    confirmText:
                                                        Strings.stopSharing.i18n.toUpperCase(),
                                                    onConfirmed: () {
                                                      widget.controller.reverse().then((value) =>
                                                          AppBloc.videoCallBloc
                                                              .add(StopShareScreenEvent()));
                                                      AppNavigator.pop();
                                                    },
                                                  ),
                                                );
                                        },
                                        child: widget.sharescreenEnabled == false
                                            ? Image.asset(
                                                iconShareScreen,
                                                height: 40.sp,
                                                width: 40.sp,
                                              )
                                            : Image.asset(
                                                iconEnableShareScreen,
                                                height: 40.sp,
                                                width: 40.sp,
                                              ),
                                        style: ElevatedButton.styleFrom(
                                          shape: CircleBorder(),
                                          padding: EdgeInsets.all(5),
                                          primary: colorBackgroundGrey2, // <-- Button color
                                          onPrimary: Colors.red, // <-- Splash color
                                        ),
                                      ),
                                      SizedBox(
                                        height: 15.sp,
                                      ),
                                      widget.sharescreenEnabled == false
                                          ? TextUI(Strings.shareMyScreen.i18n,
                                              color: colorWhiteShareScreen,
                                              fontSize: 11.sp,
                                              fontWeight: FontWeight.w400)
                                          : TextUI(Strings.stopSharingMyScreen.i18n,
                                              color: colorWhiteShareScreen,
                                              fontSize: 11.sp,
                                              fontWeight: FontWeight.w400)
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: Column(
                                    children: [
                                      ElevatedButton(
                                        onPressed: () {
                                          AppBloc.videoCallBloc.add(ToggleMyRaiseHandEvent());
                                          AppNavigator.pop();
                                          Future.delayed(const Duration(seconds: 3), () {
                                            if (this.mounted) {
                                              setState(() {
                                                AppBloc.videoCallBloc.add(ToggleMyRaiseHandEvent());
                                              });
                                            }
                                          });
                                        },
                                        child: Image.asset(
                                          iconRaiseHand,
                                          height: 40.sp,
                                          width: 40.sp,
                                        ),
                                        style: ElevatedButton.styleFrom(
                                          shape: CircleBorder(),
                                          padding: EdgeInsets.all(5),
                                          primary: colorBackgroundGrey2, // <-- Button color
                                          onPrimary: Colors.red, // <-- Splash color
                                        ),
                                      ),
                                      SizedBox(
                                        height: 15.sp,
                                      ),
                                      TextUI(
                                        Strings.raiseYourHand.i18n,
                                        color: colorBackgroundGrey,
                                        fontSize: 12.sp,
                                        fontWeight: FontWeight.w400,
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      );
                    },
                  );
                },
                child: Container(
                  // width: 45.sp,
                  // height: 45.sp,
                  // padding: EdgeInsets.all(10.sp),
                  child: CircleAvatar(
                      backgroundColor: colorBlack2,
                      child: Image.asset(
                        iconMenuShareScreen,
                        height: 25.sp,
                        width: 25.sp,
                        color: colorBackgroundGrey,
                      )),
                ),
              )),
          SizedBox(width: 15.sp),
          SizedBox(
              width: 45.sp,
              height: 45.sp,
              child: GestureDetector(
                onTap: () {
                  AppBloc.videoCallBloc.add(EndVideoCallEvent());
                },
                child: Container(
                  // width: 45.sp,
                  // height: 45.sp,
                  // padding: EdgeInsets.all(10.sp),
                  child: CircleAvatar(
                      backgroundColor: Colors.red,
                      child: Image.asset(iconEndCall, height: 25.sp, width: 25.sp)),
                ),
              )

              // ElevatedButton(
              //   onPressed: () {
              //     AppBloc.videoCallBloc.add(EndVideoCallEvent());
              //   },
              //   child: Icon(
              //     Icons.call_end,
              //     color: Colors.white,
              //     size: 25.sp,
              //   ),
              //   style: ElevatedButton.styleFrom(
              //     shape: CircleBorder(),
              //     padding: EdgeInsets.all(5),
              //     primary: Colors.red, // <-- Button color
              //     onPrimary: Colors.red, // <-- Splash color
              //   ),
              // )
              ),
        ],
      ),
    );
  }
}
