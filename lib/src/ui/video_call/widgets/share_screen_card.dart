import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/video_call/video_call_bloc.dart';
import 'package:askany/src/configs/themes/app_colors.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/models/video_call_model.dart';
import 'package:askany/src/ui/style/call_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart' as RTC;
import 'package:flutter_webrtc/flutter_webrtc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';

class ShareScreenCard extends StatefulWidget {
  final VideoCallModel videoCallModel;
  final int status;
  final AnimationController animationController;

  ShareScreenCard(
      {required this.videoCallModel,
      required this.status,
      required this.animationController});

  @override
  State<StatefulWidget> createState() => _ShareScreenCardState();
}

class _ShareScreenCardState extends State<ShareScreenCard> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    if (widget.videoCallModel.localShareRenderer.textureId == null &&
        widget.videoCallModel.remoteShareRenderer.textureId == null &&
        widget.videoCallModel.remoteRenderer.textureId == null) {
      if (widget.status == 1)
        return Container();
      else if (widget.status == 2) {
        return _getContainerShare;
      } else if (widget.status == 3) {
        return _getContainerShare;
      }
    } else {
      if (widget.status == 1)
        return Container(
          width: size.width / 3.25,
          child: AspectRatio(
            aspectRatio: size.aspectRatio < 0.6 ? 0.6 : size.aspectRatio,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5.sp),
                border: Border.all(
                    color: Theme.of(context).primaryColor, width: 2.25.sp),
              ),
              child: new Transform(
                transform: Matrix4.identity()
                  ..rotateY(
                    0.0,
                  ),
                alignment: FractionalOffset.center,
                child: new RTCVideoView(
                  widget.videoCallModel.isMyShare
                      ? widget.videoCallModel.localShareRenderer
                      : (widget.videoCallModel.remoteShareRenderer.srcObject ==
                              null
                          ? widget.videoCallModel.remoteRenderer
                          : widget.videoCallModel.remoteShareRenderer),
                  objectFit:
                      RTC.RTCVideoViewObjectFit.RTCVideoViewObjectFitContain,
                ),
              ),
            ),
          ),
        );
      else if (widget.status == 2) {
        return widget.videoCallModel.isMyShare
            ? _getContainerShare
            : Container(
                width: size.width,
                height: size.height,
                margin: EdgeInsets.fromLTRB(10.sp, 5.sp, 10.sp, 5.sp),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: colorBackgroundRemoteCamera,
                ),
                child: ClipRRect(
                    borderRadius: BorderRadius.circular(10.0),
                    child: Transform(
                      transform: Matrix4.identity()
                        ..rotateY(
                          0,
                        ),
                      alignment: FractionalOffset.center,
                      child: new RTCVideoView(
                        widget.videoCallModel.isMyShare
                            ? widget.videoCallModel.localShareRenderer
                            : (widget.videoCallModel.remoteShareRenderer
                                        .srcObject ==
                                    null
                                ? widget.videoCallModel.remoteRenderer
                                : widget.videoCallModel.remoteShareRenderer),
                        objectFit: RTC
                            .RTCVideoViewObjectFit.RTCVideoViewObjectFitContain,
                      ),
                    )));
      } else if (widget.status == 3) {
        return widget.videoCallModel.isMyShare
            ? _getContainerShare
            : Container(
                width: 100.h,
                height: 100.h,
                margin: EdgeInsets.fromLTRB(5.sp, 5.sp, 5.sp, 5.sp),
                decoration: BoxDecoration(
                  // borderRadius: BorderRadius.circular(10),
                  color: colorBlueGrey,
                ),
                child: Container(
                  width: 70.h,
                  height: 70.h,
                  child: ClipRRect(
                      borderRadius: BorderRadius.circular(10.0),
                      child: Transform(
                        transform: Matrix4.identity()
                          ..rotateY(
                            0,
                          ),
                        alignment: FractionalOffset.center,
                        child: new RTCVideoView(
                          widget.videoCallModel.isMyShare
                              ? widget.videoCallModel.localShareRenderer
                              : (widget.videoCallModel.remoteShareRenderer
                                          .srcObject ==
                                      null
                                  ? widget.videoCallModel.remoteRenderer
                                  : widget.videoCallModel.remoteShareRenderer),
                          objectFit: RTC.RTCVideoViewObjectFit
                              .RTCVideoViewObjectFitContain,
                        ),
                      )),
                ));
      }
      return Container();
    }
    return Container();
  }

  Widget get _getContainerShare => Container(
        width: 100.h,
        height: 100.h,
        margin: EdgeInsets.zero,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.zero,
          color: colorBackgroundRemoteCamera,
          boxShadow: [
            BoxShadow(color: Colors.black12, spreadRadius: 1),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              Strings.sharingScreenNoti.i18n,
              style: TextStyle(
                  color: colorGreywhite2,
                  fontSize: 13.sp,
                  fontWeight: FontWeight.w400),
            ),
            ElevatedButton(
              onPressed: () {
                widget.animationController.reverse().then((value) =>
                    AppBloc.videoCallBloc.add(StopShareScreenEvent()));
              },
              style: ElevatedButton.styleFrom(
                elevation: 0.0,
                primary: Colors.red.withOpacity(0),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(8),
                  ),
                  side: BorderSide(color: colorwhiteborder, width: 1.sp),
                ),
              ),
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 10.sp),
                child: Text(
                  Strings.stopSharing.i18n,
                  style: TextStyle(
                    color: colorGreywhite2,
                    fontSize: 12.sp,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
            ),
          ],
        ),
      );
}
