import 'dart:async';
import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/timer/timer_bloc.dart';
import 'package:askany/src/bloc/video_call/video_call_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/configs/themes/app_colors.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/data/local_data_source/user_local_data.dart';
import 'package:askany/src/helpers/date_time_helper.dart';
import 'package:askany/src/helpers/device_orientation_helper.dart';
import 'package:askany/src/models/conversation_model.dart';
import 'package:askany/src/models/location_widget_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/services/vibrate/vibrate_incoming_call.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_start_call.dart';
import 'package:askany/src/ui/common/widgets/text_ui/text_ui.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:askany/src/ui/video_call/screens/chat_in_call_screen.dart';
import 'package:askany/src/ui/video_call/screens/incoming_call_screen.dart';
import 'package:askany/src/ui/video_call/screens/participant_screen.dart';
import 'package:askany/src/ui/video_call/widgets/my_camera_card.dart';
import 'package:askany/src/ui/video_call/widgets/remote_video_card.dart';
import 'package:askany/src/ui/video_call/widgets/share_screen_card.dart';
import 'package:askany/src/ui/video_call/widgets/tab_menu_vertical.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:wakelock/wakelock.dart';

class VideoCallScreen extends StatefulWidget {
  final ConversationModel conversation;
  const VideoCallScreen({required this.conversation});
  @override
  State<StatefulWidget> createState() => _VideoCallScreenState();
}

class _VideoCallScreenState extends State<VideoCallScreen> with TickerProviderStateMixin {
  // Animation Controllers
  late Animation<double> _animation;
  AnimationController? _controller;
  LocationWidgetModel? _locationWidgetModel = LocationWidgetModel(
      posittionX: 47.w,
      posittionY: 0,
      posittionLeft: 0,
      posittionTop: 0,
      posittionRight: 50.w,
      posittionBottom: 75.h - 30.w);

  LocationWidgetState locationWidgetState = LocationWidgetState();
  bool isTimer = false;
  bool isRemoteCam = true;
  bool isMyCam = true;

  @override
  void initState() {
    super.initState();
    // Initial ConversationModel

    Wakelock.enable();
    _controller = AnimationController(
      duration: const Duration(milliseconds: DELAY_HALF_SECOND),
      vsync: this,
    );
    _animation = Tween<double>(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(parent: _controller!, curve: Curves.fastOutSlowIn),
    );
    _locationWidgetModel!.setLocationWidgetModel(1, 100.h, 100.w);
    DeviceOrientationHelper().releaseOrientation();
    AppBloc.timerBloc.add(StartTimerEvent());
  }

  @override
  void dispose() {
    _controller!.dispose();
    locationWidgetState.dispose();
    Wakelock.disable();
    DeviceOrientationHelper().setPortrait();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<VideoCallBloc, VideoCallState>(builder: (context, state) {
      if (state is VideoCallIncoming) {
        return IncomingCallScreen(state: state, conversation: widget.conversation);
      }
      if (state is VideoCalling) {
        stopRingAndVibrateIncomingCall();
        if (state.videoCallModel.isVidCallFullScreen == false) {
          if (state.videoCallModel.isSharingScreen == false) {
            // video call not work sharescreen
            return buildVideoCallingScreen(state);
          } else {
            _controller!.forward();
            // video call work sharescreem
            return buildVideoCallingShareScreen(state);
          }
        } else {
          _controller!.forward();
          return buildVideoCallFullScreen(state);
        }
      }
      return Scaffold(
        body: Container(),
      );
    });
  }

  Widget buildVideoCallingShareScreen(VideoCalling state) {
    DeviceOrientationHelper().releaseOrientation();
    return OrientationBuilder(builder: (context, orientation) {
      if (state.videoCallModel.isShareFullScreen == false) {
        isMyCam = true;
        isRemoteCam = true;
        return ScaleTransition(
          scale: _animation,
          child: Scaffold(
            extendBodyBehindAppBar: orientation == Orientation.portrait ? false : true,
            backgroundColor: colorBackgroundVideoCall,
            appBar: buildAppBarParent(orientation, state),
            body: Container(
              height: 100.h,
              width: 100.h,
              color: colorBackgroundVideoCall,
              child: orientation == Orientation.portrait
                  ? buildPortraitShareScreen(state)
                  : buildLandScapeShareScreen(state),
            ),
          ),
        );
      } else {
        _controller!.forward();
        return buildStatusFullShareScreen(state);
      }
    });
  }

  Widget buildVideoCallingScreen(
    VideoCalling state,
  ) {
    DeviceOrientationHelper().releaseOrientation();
    return OrientationBuilder(builder: (context, orientation) {
      orientation == Orientation.portrait
          ? _locationWidgetModel!.setLocationWidgetModel(1, 100.h, 100.w)
          : _locationWidgetModel!.setLocationWidgetModel(2, 100.h, 100.w);
      return orientation == Orientation.portrait
          ? buildPortraitVideoCallingScreen(state)
          : buildLandScapeVideoCallingScreen(state);
    });
  }

  Widget buildPortraitVideoCallingScreen(VideoCalling state) {
    return Scaffold(
      extendBodyBehindAppBar: false,
      backgroundColor: colorBackgroundVideoCall,
      appBar: buildAppBarParent(Orientation.portrait, state),
      body: SafeArea(
        child: Container(
          height: 100.h,
          width: 100.w,
          color: colorBackgroundVideoCall,
          child: Column(
            children: [
              Expanded(
                  child: Stack(
                alignment: Alignment.bottomCenter,
                children: [
                  RemoteVideoCard(
                    width: 100.w,
                    height: 100.h,
                    remoteOnCamera: state.videoCallModel.isRemoteOnCamera,
                    remoteRenderer: state.videoCallModel.remoteRenderer,
                    margin: EdgeInsets.all(10.sp),
                    status: 1,
                    conversation: widget.conversation,
                  ),
                  buildRaiseHandContainer(state),
                  buildDraggerStreamMyCamera(state, 1, 25.w, 40.w),
                ],
              )),
              handleLogicTabMenuVertical(state),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildLandScapeVideoCallingScreen(VideoCalling state) {
    // _locationWidgetModel!.setLocationWidgetModel(4, 100.h, 100.w);
    return Scaffold(
        extendBodyBehindAppBar: true,
        backgroundColor: colorBackgroundVideoCall,
        // appBar: buildAppBarParent(Orientation.portrait, state),
        body: Container(
          height: 100.h,
          width: 100.h,
          color: colorBackgroundVideoCall,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: 15.sp),
              Expanded(
                  child: Stack(
                alignment: Alignment.topCenter,
                children: [
                  Container(
                    margin: EdgeInsets.fromLTRB(70.sp, 10.sp, 70.sp, 0.sp),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.sp),
                      color: colorBackgroundRemoteCamera,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        RemoteVideoCard(
                          width: 76.h,
                          height: 50.h,
                          remoteOnCamera: state.videoCallModel.isRemoteOnCamera,
                          remoteRenderer: state.videoCallModel.remoteRenderer,
                          margin: EdgeInsets.fromLTRB(0.sp, 0.sp, 0.sp, 0.sp),
                          borderRadius: BorderRadius.circular(10.sp),
                          status: 1,
                          conversation: widget.conversation,
                        ),
                      ],
                    ),
                  ),
                  buildRaiseHandContainer(state),
                  buildDraggerStreamMyCamera(state, 2, 40.w, 25.w),
                  buildAppBarLanscapeVideoCall(state),
                ],
              )),
              handleLogicTabMenuVertical(state)
            ],
          ),
        ));
  }

  Widget buildPortraitShareScreen(VideoCalling state) {
    return SafeArea(
      child: Column(
        children: [
          Expanded(
            child: ShareScreenCard(
              videoCallModel: state.videoCallModel,
              animationController: _controller!,
              status: 2,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Stack(
                children: [
                  RemoteVideoCard(
                    width: 45.w,
                    height: 25.w,
                    remoteOnCamera: state.videoCallModel.isRemoteOnCamera,
                    remoteRenderer: state.videoCallModel.remoteRenderer,
                    margin: EdgeInsets.fromLTRB(0.sp, 10.sp, 0.sp, 10.sp),
                    status: 2,
                    conversation: widget.conversation,
                  ),
                  Positioned(
                    top: 0,
                    left: 0,
                    child: buildContainerRaiseHand(),
                  ),
                  Positioned(
                    bottom: 14.sp,
                    right: 4.sp,
                    child: state.videoCallModel.isRemoteOnMic
                        ? buildContainerIcon(iconEnableRecord, EdgeInsets.zero)
                        : buildContainerIcon(iconDeniedRecord, EdgeInsets.zero),
                  )
                ],
              ),
              SizedBox(
                width: 13.sp,
              ),
              Stack(
                children: [
                  MyCameraCard(
                    width: 45.w,
                    height: 25.w,
                    cameraEnable: state.videoCallModel.cameraEnabled,
                    localRenderer: state.videoCallModel.localRenderer,
                    margin: EdgeInsets.fromLTRB(0.sp, 10.sp, 0.sp, 10.sp),
                  ),
                  Positioned(
                    top: 0,
                    left: 0,
                    child: buildContainerRaiseHand(),
                  ),
                  Positioned(
                    bottom: 14.sp,
                    right: 4.sp,
                    child: state.videoCallModel.micEnabled
                        ? buildContainerIcon(iconEnableRecord, EdgeInsets.zero)
                        : buildContainerIcon(iconDeniedRecord, EdgeInsets.zero),
                  )
                ],
              )
            ],
          ),
          handleLogicTabMenuVertical(state)
        ],
      ),
    );
  }

  Widget buildLandScapeShareScreen(VideoCalling state) {
    return Column(
      children: [
        SizedBox(
          height: 25.sp,
          width: 25.sp,
        ),
        Expanded(
          child: Row(
            children: [
              Expanded(
                  child: ShareScreenCard(
                videoCallModel: state.videoCallModel,
                animationController: _controller!,
                status: 2,
              )),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Stack(
                    alignment: Alignment.bottomRight,
                    children: [
                      RemoteVideoCard(
                        width: 45.w,
                        height: 30.w,
                        remoteOnCamera: state.videoCallModel.isRemoteOnCamera,
                        remoteRenderer: state.videoCallModel.remoteRenderer,
                        margin: EdgeInsets.fromLTRB(0.sp, 2.sp, 10.sp, 8.sp),
                        status: 2,
                        conversation: widget.conversation,
                      ),
                      buildContainerRaiseHand(),
                      state.videoCallModel.isRemoteOnMic
                          ? buildContainerIcon(iconEnableRecord, EdgeInsets.all(20))
                          : buildContainerIcon(iconDeniedRecord, EdgeInsets.all(20))
                    ],
                  ),
                  Stack(
                    alignment: Alignment.bottomRight,
                    children: [
                      MyCameraCard(
                        width: 45.w,
                        height: 30.w,
                        cameraEnable: state.videoCallModel.cameraEnabled,
                        localRenderer: state.videoCallModel.localRenderer,
                        margin: EdgeInsets.fromLTRB(0.sp, 0.sp, 10.sp, 0.sp),
                      ),
                      // buildContainerRaiseHand(),
                      state.videoCallModel.micEnabled
                          ? buildContainerIcon(iconEnableRecord, EdgeInsets.all(20))
                          : buildContainerIcon(iconDeniedRecord, EdgeInsets.all(20))
                    ],
                  )
                ],
              ),
            ],
          ),
        ),
        SizedBox(
          height: 5.sp,
        ),
        handleLogicTabMenuVertical(state)
      ],
    );
  }

  Widget buildStatusFullShareScreen(VideoCalling state) {
    return ScaleTransition(
      scale: _animation,
      child: Scaffold(
        extendBodyBehindAppBar: true,
        backgroundColor: colorBackgroundVideoCall,
        body: SafeArea(
          child: Stack(
            alignment: Alignment.bottomRight,
            children: [
              Stack(
                alignment: Alignment.topRight,
                children: [
                  ShareScreenCard(
                    videoCallModel: state.videoCallModel,
                    animationController: _controller!,
                    status: 3,
                  ),
                  Stack(
                    children: [
                      Column(
                        children: [
                          Visibility(
                              visible: isRemoteCam,
                              child: Stack(
                                alignment: Alignment.centerRight,
                                children: [
                                  RemoteVideoCard(
                                      conversation: widget.conversation,
                                      width: 45.w,
                                      height: 25.w,
                                      remoteOnCamera: state.videoCallModel.isRemoteOnCamera,
                                      margin: EdgeInsets.fromLTRB(15.sp, 0.sp, 15.sp, 0.sp),
                                      remoteRenderer: state.videoCallModel.remoteRenderer,
                                      status: 2),
                                  Column(
                                    children: [
                                      GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            isRemoteCam = false;
                                          });
                                        },
                                        child: Container(
                                            margin: EdgeInsets.fromLTRB(0, 0, 20.sp, 0),
                                            child: CircleAvatar(
                                              backgroundColor: colorBlack.withOpacity(0.4),
                                              child: Icon(Icons.clear),
                                            )),
                                      ),
                                      SizedBox(
                                        height: 5.sp,
                                        width: 1.w,
                                      ),
                                      state.videoCallModel.micEnabled
                                          ? buildContainerIcon(
                                              iconEnableRecord, EdgeInsets.fromLTRB(0, 0, 20.sp, 0))
                                          : buildContainerIcon(
                                              iconDeniedRecord, EdgeInsets.fromLTRB(0, 0, 20.sp, 0))
                                    ],
                                  )
                                ],
                              )),
                          SizedBox(
                            height: 5.sp,
                          ),
                          Visibility(
                              visible: isMyCam,
                              child: Stack(
                                alignment: Alignment.centerRight,
                                children: [
                                  MyCameraCard(
                                    width: 45.w,
                                    height: 25.w,
                                    cameraEnable: state.videoCallModel.cameraEnabled,
                                    localRenderer: state.videoCallModel.localRenderer,
                                    margin: EdgeInsets.fromLTRB(15.sp, 0.sp, 15.sp, 0.sp),
                                  ),
                                  Column(
                                    children: [
                                      GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            isMyCam = false;
                                          });
                                        },
                                        child: Container(
                                            margin: EdgeInsets.fromLTRB(0, 0, 20.sp, 0),
                                            child: CircleAvatar(
                                              backgroundColor: colorBlack.withOpacity(0.4),
                                              child: Icon(Icons.clear),
                                            )),
                                      ),
                                      SizedBox(
                                        height: 5.sp,
                                        width: 1.w,
                                      ),
                                      state.videoCallModel.micEnabled
                                          ? buildContainerIcon(
                                              iconEnableRecord, EdgeInsets.fromLTRB(0, 0, 20.sp, 0))
                                          : buildContainerIcon(
                                              iconDeniedRecord, EdgeInsets.fromLTRB(0, 0, 20.sp, 0))
                                    ],
                                  )
                                ],
                              ))
                        ],
                      ),
                    ],
                  )
                ],
              ),
              GestureDetector(
                onTap: () {
                  _controller!
                      .reverse()
                      .then((value) => AppBloc.videoCallBloc.add(ShareFullScreenEvent()));
                },
                child: Container(
                  width: 60.sp,
                  height: 60.sp,
                  padding: EdgeInsets.all(10.sp),
                  child: CircleAvatar(
                      backgroundColor: colorBlack.withOpacity(0.4),
                      child: Image.asset(iconDeniedFullScreen, height: 20.sp, width: 20.sp)),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget buildVideoCallFullScreen(VideoCalling state) {
    DeviceOrientationHelper().releaseOrientation();
    return ScaleTransition(
        scale: _animation,
        child: OrientationBuilder(builder: (context, orientation) {
          return orientation == Orientation.portrait
              ? buildPortraitVideoCallFullScreen(state)
              : buildLanscapeVideoCallFullScreen(state);
        }));
  }

  Widget buildPortraitVideoCallFullScreen(VideoCalling state) {
    _locationWidgetModel!.setLocationWidgetModel(3, 100.h, 100.w);
    return Scaffold(
        body: Stack(
      alignment: Alignment.bottomRight,
      children: [
        Stack(
          children: [
            RemoteVideoCard(
              borderRadius: BorderRadius.zero,
              conversation: widget.conversation,
              width: 100.w,
              height: 100.h,
              remoteOnCamera: state.videoCallModel.isRemoteOnCamera,
              remoteRenderer: state.videoCallModel.remoteRenderer,
              margin: EdgeInsets.all(0.sp),
              status: 1,
            ),
            buildDraggerStreamMyCamera(state, 2, 25.w, 40.w),
          ],
        ),
        GestureDetector(
          onTap: () {
            _locationWidgetModel!.setLocationWidgetModel(1, 100.h, 100.w);
            _controller!
                .reverse()
                .then((value) => AppBloc.videoCallBloc.add(VideoCallingFullScreenEvent()));
          },
          child: Container(
            width: 60.sp,
            height: 60.sp,
            padding: EdgeInsets.all(10.sp),
            child: CircleAvatar(
                backgroundColor: colorBlack.withOpacity(0.4),
                child: Image.asset(iconDeniedFullScreen, height: 20.sp, width: 20.sp)),
          ),
        )
      ],
    ));
  }

  Widget buildLanscapeVideoCallFullScreen(VideoCalling state) {
    _locationWidgetModel!.setLocationWidgetModel(4, 100.h, 100.w);
    return Scaffold(
        body: Stack(
      alignment: Alignment.bottomRight,
      children: [
        Stack(
          children: [
            RemoteVideoCard(
              width: 100.h,
              height: 100.h,
              conversation: widget.conversation,
              borderRadius: BorderRadius.circular(0.sp),
              remoteOnCamera: state.videoCallModel.isRemoteOnCamera,
              remoteRenderer: state.videoCallModel.remoteRenderer,
              margin: EdgeInsets.all(0.sp),
              status: 1,
            ),
            buildDraggerStreamMyCamera(state, 2, 40.w, 25.w),
          ],
        ),
        GestureDetector(
          onTap: () {
            _locationWidgetModel!.setLocationWidgetModel(1, 100.h, 100.w);
            _controller!
                .reverse()
                .then((value) => AppBloc.videoCallBloc.add(VideoCallingFullScreenEvent()));
          },
          child: Container(
            width: 60.sp,
            height: 60.sp,
            padding: EdgeInsets.all(10.sp),
            child: CircleAvatar(
                backgroundColor: colorBlack.withOpacity(0.4),
                child: Image.asset(iconDeniedFullScreen, height: 20.sp, width: 20.sp)),
          ),
        )
      ],
    ));
  }

  Widget buildDraggerStreamMyCamera(
      VideoCalling state, int status, double widthCamera, double heightCamera) {
    //status = 1  video calling
    // status = 2 full video calling
    return StreamBuilder<LocationWidgetModel?>(
      stream: locationWidgetState.locationStream,
      builder: (context, snapshot) => Positioned(
        left: _locationWidgetModel!.posittionX,
        top: (_locationWidgetModel!.posittionY + ((status == 1) ? -90 : 0)) < 0
            ? 0
            : (_locationWidgetModel!.posittionY + ((status == 1) ? -90 : 0)) >
                    _locationWidgetModel!.posittionBottom
                ? _locationWidgetModel!.posittionBottom
                : (_locationWidgetModel!.posittionY + ((status == 1) ? -90 : 0)),
        child: Draggable(
          child: Stack(
            alignment: Alignment.bottomRight,
            children: [
              MyCameraCard(
                cameraEnable: state.videoCallModel.cameraEnabled,
                localRenderer: state.videoCallModel.localRenderer,
                margin: EdgeInsets.all(15.sp),
                width: widthCamera,
                height: heightCamera,
              ),
              state.videoCallModel.micEnabled
                  ? buildContainerIcon(iconEnableRecord, EdgeInsets.all(16.sp))
                  : buildContainerIcon(iconDeniedRecord, EdgeInsets.all(16.sp)),
            ],
          ),
          feedback: Stack(
            alignment: Alignment.bottomRight,
            children: [
              MyCameraCard(
                cameraEnable: state.videoCallModel.cameraEnabled,
                localRenderer: state.videoCallModel.localRenderer,
                margin: EdgeInsets.all(15.sp),
                width: widthCamera,
                height: heightCamera,
              ),
              // widget.micEnabled
              state.videoCallModel.isRemoteOnMic
                  ? buildContainerIcon(iconEnableRecord, EdgeInsets.all(16.sp))
                  : buildContainerIcon(iconDeniedRecord, EdgeInsets.all(16.sp))
            ],
          ),
          childWhenDragging: Container(),
          onDragUpdate: (details) => {},
          onDraggableCanceled: (Velocity velocity, Offset offset) {
            locationWidgetState.changePosittion(offset, _locationWidgetModel!);
          },
        ),
      ),
    );
  }

  Widget buildRaiseHandContainer(VideoCalling state) {
    return Container(
      alignment: Alignment.bottomCenter,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          state.videoCallModel.isMyRaisingHand || state.videoCallModel.isRemoteRaisingHand
              ? buildRaisingHandIcon(
                  iconRaiseHand2,
                  state.videoCallModel.isMyRaisingHand
                      ? AppBloc.userBloc.getAccount.fullname
                      : UserLocal().getIsExpert()
                          ? (widget.conversation.users.isEmpty
                              ? widget.conversation.conversationTitle
                              : (widget.conversation.users.first.fullname ?? ''))
                          : (widget.conversation.expert.fullname ?? ''),
                  EdgeInsets.all(10.sp))
              : buildRaisingHandIcon(iconRaiseHand2, null, EdgeInsets.all(10.sp)),
        ],
      ),
    );
  }

  Widget buildContainerIcon(String icon, EdgeInsets edgeInsets) {
    return Container(
      margin: edgeInsets,
      child: CircleAvatar(
        backgroundColor: colorBlack.withOpacity(0.4),
        child: Image.asset(
          icon,
          height: 30.sp,
          width: 30.sp,
        ),
      ),
    );
  }

  Widget buildContainerRaiseHand() {
    return Container(
        // margin: EdgeInsets.only(left: 2.sp, top: 12.sp),
        // child: Image.asset(
        //   iconRaiseHand2,
        //   height: 20.sp,
        //   width: 20.sp,
        // ),
        );
  }

  Widget buildRaisingHandIcon(String icon, String? name, EdgeInsets edgeInsets) {
    return Visibility(
      visible: name != null,
      child: Container(
        height: 30.sp,
        margin: EdgeInsets.only(bottom: 20.sp),
        // color: colorGreyWhite,
        decoration: BoxDecoration(
          color: colorGreyWhite,
          borderRadius: BorderRadius.all(
            Radius.circular(8.sp),
          ),
        ),

        child: Row(
          children: [
            Image.asset(icon, height: 30.sp, width: 30.sp),
            TextUI(
              '$name   ',
              color: Colors.white,
              fontSize: 13.sp,
              fontWeight: FontWeight.w600,
            ),
          ],
        ),
      ),
    );
  }

  Widget handleLogicTabMenuVertical(VideoCalling state) {
    return Padding(
      padding: EdgeInsets.only(bottom: state.videoCallModel.isSharingScreen ? 0 : 15.sp),
      child: TabMenuVertical(
        cameraEnabled: state.videoCallModel.cameraEnabled,
        micEnabled: state.videoCallModel.micEnabled,
        sharescreenEnabled: state.videoCallModel.isSharingScreen,
        controller: _controller!,
      ),
    );
  }

  AppBar buildAppBarParent(Orientation orientation, VideoCalling state) {
    return appbarStartCall(
        context: context,
        onPressedBackChat: () {
          showModalBottomSheet<void>(
            isScrollControlled: true,
            context: context,
            backgroundColor: colorBackgroundVideoCall,
            builder: (BuildContext context) {
              return ChatInCallScreen(
                conversationModel: widget.conversation,
              );
            },
          );
        },
        onPressedFullScreen: () {
          _locationWidgetModel!.setLocationWidgetModel(3, 100.h, 100.w);
          if (state.videoCallModel.isSharingScreen) {
            _controller!
                .reverse()
                .then((value) => AppBloc.videoCallBloc.add(ShareFullScreenEvent()));
          } else {
            AppBloc.videoCallBloc.add(VideoCallingFullScreenEvent());
          }
        },
        onPressedPeopleInvited: () {
          showModalBottomSheet<void>(
            isScrollControlled: true,
            context: context,
            backgroundColor: colorBackgroundVideoCall,
            builder: (BuildContext context) {
              return ParticipantScreen(
                conversationModel: widget.conversation,
              );
            },
          );
        },
        title: "Tư vấn xin visa du học Mỹ Tho OK khum mấy bạn",
        onPressedBack: () {
          AppNavigator.pop();
        },
        onPressedSwitchCamera: () {
          AppBloc.videoCallBloc.add(SwitchCameraEvent());
        },
        orientation: orientation);
  }

  Widget buildAppBarLanscapeVideoCall(VideoCalling stateVideo) {
    return Container(
      width: 100.h - 100.sp,
      margin: EdgeInsets.only(top: 10.sp),
      decoration: BoxDecoration(
          color: colorBlack1.withOpacity(0.1),
          borderRadius:
              BorderRadius.only(topLeft: Radius.circular(10.sp), topRight: Radius.circular(10.sp))),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          IconButton(
              padding: EdgeInsets.zero,
              icon: ImageIcon(
                AssetImage(iconArrowBack),
                color: Colors.white,
                size: 30.sp,
              ),
              // Center(child: Image.asset(iconArrowBack, height: 60.sp, width: 60.sp, color: colorBackgroundGrey,),),
              onPressed: () {
                AppNavigator.pop();
              }),
          Container(
            height: 40.sp,
            width: 100.h - 90.sp - 140.sp - 30.sp,
            margin: EdgeInsets.only(top: 10.sp),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Tư vấn xin visa du học Mỹ Tho OK khum mấy bạn",
                  style: TextStyle(fontSize: 15.sp, color: Colors.grey.withOpacity(0.3)),
                  overflow: TextOverflow.ellipsis,
                ),
                BlocBuilder<TimerBloc, TimerState>(
                  builder: (context, state) {
                    return Text(
                      state is TimerRunInProgress
                          ? getDurationCalling(
                              durationSeconds: state.duration,
                            )
                          : "00:00:00",
                      style: TextStyle(fontSize: 9.sp, color: Colors.grey),
                      overflow: TextOverflow.ellipsis,
                    );
                  },
                )
              ],
            ),
          ),
          IconButton(
              icon: ImageIcon(AssetImage(iconFlipCamera), size: 20.sp, color: colorBackgroundGrey),
              onPressed: () {
                AppBloc.videoCallBloc.add(SwitchCameraEvent());
              }),
          PopupMenuButton(
            offset: const Offset(-25.0, 45.0),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(15.0))),
            icon: ImageIcon(AssetImage(iconMenuVertical), size: 15.sp, color: colorBackgroundGrey),
            padding: EdgeInsets.zero,
            color: colorBlack2,
            onSelected: (value) {
              if (value == 0) {
                // onPressedBackChat();
                showModalBottomSheet<void>(
                  isScrollControlled: true,
                  context: context,
                  backgroundColor: colorBackgroundVideoCall,
                  builder: (BuildContext context) {
                    return ChatInCallScreen(
                      conversationModel: widget.conversation,
                    );
                  },
                );
              } else if (value == 1) {
                // onPressedPeopleInvited();
                showModalBottomSheet<void>(
                  isScrollControlled: true,
                  context: context,
                  builder: (BuildContext context) {
                    return ParticipantScreen(
                      conversationModel: widget.conversation,
                    );
                  },
                );
              } else if (value == 2) {
                // onPressedFullScreen();
                _locationWidgetModel!.setLocationWidgetModel(3, 100.h, 100.w);
                if (stateVideo.videoCallModel.isSharingScreen) {
                  _controller!
                      .reverse()
                      .then((value) => AppBloc.videoCallBloc.add(ShareFullScreenEvent()));
                } else {
                  AppBloc.videoCallBloc.add(VideoCallingFullScreenEvent());
                }
              }
            },
            itemBuilder: (context) {
              return [
                PopupMenuItem(
                    value: 0,
                    child: Row(
                      children: [
                        Center(
                          child: Image.asset(
                            iconBubbleChat,
                            height: 20.sp,
                            width: 20.sp,
                            color: colorBackgroundGrey,
                          ),
                        ),
                        Text("    ${Strings.chatInCall.i18n}")
                      ],
                    )),
                PopupMenuItem(
                    value: 1,
                    child: Row(
                      children: [
                        Center(
                          child: Image.asset(
                            iconPersonOutline,
                            height: 20.sp,
                            width: 20.sp,
                            color: colorBackgroundGrey,
                          ),
                        ),
                        Text("    ${Strings.callParticipants.i18n}")
                      ],
                    )),
                PopupMenuItem(
                  value: 2,
                  child: Row(
                    children: [
                      Center(
                        child: Image.asset(
                          iconFullScreen,
                          height: 20.sp,
                          width: 20.sp,
                          color: colorBackgroundGrey,
                        ),
                      ),
                      Text("    ${Strings.fullScreen.i18n}")
                    ],
                  ),
                ),
                PopupMenuItem(
                  value: 2,
                  child: Row(
                    children: [
                      Center(
                        child: Image.asset(
                          iconCamera,
                          height: 20.sp,
                          width: 20.sp,
                          color: colorBackgroundGrey,
                        ),
                      ),
                      Text("    ${Strings.record.i18n}")
                    ],
                  ),
                ),
              ];
            },
          )
        ],
      ),
    );
  }
}

class LocationWidgetState {
  StreamController<LocationWidgetModel> streamLocationController =
      StreamController<LocationWidgetModel>.broadcast();

// sink
  Sink get locationSink => streamLocationController.sink;

// stream
  Stream<LocationWidgetModel>? get locationStream => streamLocationController.stream;

// function to change the posittion
  changePosittion(Offset offset, LocationWidgetModel _locationWidgetModel) {
    locationSink.add(getPosittionWidget(offset, _locationWidgetModel));
  }

  void dispose() {
    streamLocationController.close();
  }

  LocationWidgetModel getPosittionWidget(Offset offset, LocationWidgetModel _locationWidgetModel) {
    if (offset.dy >= _locationWidgetModel.posittionTop &&
        offset.dy <= _locationWidgetModel.posittionBottom &&
        offset.dx <= _locationWidgetModel.posittionRight &&
        offset.dx >= _locationWidgetModel.posittionLeft) {
      _locationWidgetModel.posittionX = offset.dx;
      _locationWidgetModel.posittionY = offset.dy;
    } else {
      if (offset.dy < _locationWidgetModel.posittionTop) {
        _locationWidgetModel.posittionY = _locationWidgetModel.posittionTop;
      } else if (offset.dy > _locationWidgetModel.posittionBottom) {
        _locationWidgetModel.posittionY =
            _locationWidgetModel.posittionBottom - (65.sp - 20.w + 6.sp);
      } else {
        _locationWidgetModel.posittionY = offset.dy;
      }
      if (offset.dx > _locationWidgetModel.posittionRight) {
        _locationWidgetModel.posittionX = _locationWidgetModel.posittionRight;
      } else if (offset.dx < _locationWidgetModel.posittionLeft) {
        _locationWidgetModel.posittionX = _locationWidgetModel.posittionLeft;
      } else {
        _locationWidgetModel.posittionX = offset.dx;
      }
    }
    return _locationWidgetModel;
  }
}
