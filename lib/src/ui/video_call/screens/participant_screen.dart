import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/helpers/province_helper.dart';
import 'package:askany/src/models/account_model.dart';
import 'package:askany/src/models/conversation_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:askany/src/ui/common/widgets/custom_image/network_image/cached_image.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:askany/src/helpers/extentions/string_extention.dart';

class ParticipantScreen extends StatefulWidget {
  final ConversationModel conversationModel;
  const ParticipantScreen({required this.conversationModel});

  @override
  _ParticipantScreenState createState() => _ParticipantScreenState();
}

class _ParticipantScreenState extends State<ParticipantScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(
      builder: (context, orientation) => SafeArea(
        child: Container(
          color: colorBackgroundVideoCall,
          height: 100.h,
          child: Column(
            children: [
              SizedBox(height: orientation.index == 0 ? 5.h : 1.h),
              Container(
                padding: EdgeInsets.only(left: 4.sp, right: 12.sp),
                color: Colors.transparent,
                child: Stack(
                  alignment: Alignment.centerRight,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          Strings.callParticipants.i18n,
                          style: TextStyle(
                            color: colorTextAppointmentCard,
                            fontWeight: FontWeight.w700,
                            fontSize: 14.sp,
                          ),
                        )
                      ],
                    ),
                    IconButton(
                      icon: Icon(
                        Icons.clear,
                        color: Colors.white,
                        size: 23.sp,
                      ),
                      onPressed: () {
                        AppNavigator.pop();
                        // showAlert(context);
                      },
                    )
                  ],
                ),
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 16.sp),
                  child: ListView.builder(
                    physics: BouncingScrollPhysics(),
                    itemCount: widget.conversationModel.allMembers.length,
                    itemBuilder: (context, index) => _buildParticipantCard(
                      widget.conversationModel.allMembers[index],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildParticipantCard(AccountModel user) {
    return Container(
      margin: EdgeInsets.only(top: 18.sp),
      child: Row(
        children: [
          CustomNetworkImage(
            height: 44.sp,
            width: 44.sp,
            urlToImage: user.avatar?.urlToImage,
          ),
          SizedBox(width: 10.sp),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                (user.fullname ?? '').formatName(),
                style: TextStyle(
                  color: colorGrey3,
                  fontWeight: FontWeight.w600,
                  fontSize: 13.sp,
                ),
              ),
              SizedBox(height: 6.sp),
              Text(
                ProvinceHelper().getProvinceByCode(user.province ?? 0) ?? '',
                style: TextStyle(
                  color: colorGray2,
                  fontSize: 11.sp,
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
