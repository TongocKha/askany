import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/message/message_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/constants/constants.dart';
import 'package:askany/src/helpers/date_time_helper.dart';
import 'package:askany/src/models/conversation_model.dart';
import 'package:askany/src/models/message_model.dart';
import 'package:askany/src/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:askany/src/ui/conversation/widgets/input_message.dart';
import 'package:askany/src/ui/conversation/widgets/message_card.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

class ChatInCallScreen extends StatefulWidget {
  final ConversationModel conversationModel;
  const ChatInCallScreen({required this.conversationModel});
  @override
  _ChatInCallScreenState createState() => _ChatInCallScreenState();
}

class _ChatInCallScreenState extends State<ChatInCallScreen>
    with TickerProviderStateMixin {
  late final AnimationController _controller = AnimationController(
    duration: const Duration(milliseconds: DURATION_DEFAULT_ANIMATION * 2),
    lowerBound: 0.8,
    vsync: this,
  )..repeat(reverse: true);
  ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    AppBloc.messageBloc.add(
      OnMessageEvent(
        conversationId: widget.conversationModel.id,
        isSeen: widget.conversationModel.latestMessage.isSeen,
      ),
    );

    _scrollController.addListener(
      () {
        if (_scrollController.position.pixels >=
            _scrollController.position.maxScrollExtent - 2.sp) {
          AppBloc.messageBloc.add(
              GetMessageEvent(conversationId: widget.conversationModel.id));
        }
      },
    );
  }

  @override
  void dispose() {
    AppBloc.messageBloc.add(DisposeMessageEvent());
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(
      builder: (context, orientation) => SafeArea(
        bottom: false,
        child: Container(
          color: colorBackgroundVideoCall,
          height: 100.h,
          child: Column(
            children: [
              SizedBox(height: orientation.index == 0 ? 5.h : 1.h),
              Container(
                padding: EdgeInsets.only(left: 4.sp, right: 12.sp),
                child: Stack(
                  alignment: Alignment.centerRight,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          Strings.chatInCall.i18n,
                          style: TextStyle(
                            color: colorTextAppointmentCard,
                            fontWeight: FontWeight.w700,
                            fontSize: 14.sp,
                          ),
                        )
                      ],
                    ),
                    IconButton(
                        icon: Icon(
                          Icons.clear,
                          color: Colors.white,
                          size: 23.sp,
                        ),
                        onPressed: () {
                          AppNavigator.pop();
                          // showAlert(context);
                        })
                  ],
                ),
              ),
              SizedBox(height: 12.sp),
              Expanded(
                child: BlocBuilder<MessageBloc, MessageState>(
                  builder: (context, state) {
                    if (state is MessageInitial) {
                      return Container(
                        child: Center(
                          child: CircularProgressIndicator(),
                        ),
                      );
                    }

                    List<MessageModel> messages = state.props[0];

                    return Align(
                      alignment: Alignment.bottomCenter,
                      child: ListView.builder(
                        controller: _scrollController,
                        padding: EdgeInsets.only(top: 30.sp),
                        physics: BouncingScrollPhysics(),
                        shrinkWrap: true,
                        reverse: true,
                        itemCount: messages.length,
                        itemBuilder: (context, index) {
                          return MessageCard(
                            animationController: _controller,
                            controller: ItemScrollController(),
                            isChatInCall: true,
                            messageModel: messages[index],
                            conversationModel: widget.conversationModel,
                            isShowAvatar: index == messages.length - 1 ||
                                (!messages[index].isMe &&
                                    (messages[index + 1].isMe ||
                                        isShowTime(
                                            messages[index].createdAt,
                                            (index < messages.length - 1
                                                    ? messages[index + 1]
                                                    : null)!
                                                .createdAt))),
                            prevModel: index < messages.length - 1
                                ? messages[index + 1]
                                : null,
                            nextModel: index > 0 ? messages[index - 1] : null,
                          );
                        },
                      ),
                    );
                  },
                ),
              ),
              dividerChat,
              InputMessage(
                receiverId: widget.conversationModel.receiverUser?.id ?? '',
                isChatInCall: true,
                conversationId: widget.conversationModel.id,
                handleScrollWhenSendMessage: () {
                  _scrollDown();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _scrollDown() {
    _scrollController.animateTo(
      0.0,
      duration: Duration(milliseconds: DELAY_200_MS),
      curve: Curves.bounceInOut,
    );
  }
}
