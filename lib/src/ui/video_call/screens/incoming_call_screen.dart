import 'package:askany/src/bloc/app_bloc.dart';
import 'package:askany/src/bloc/video_call/video_call_bloc.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/models/conversation_model.dart';
import 'package:askany/src/services/vibrate/vibrate_incoming_call.dart';
import 'package:askany/src/ui/common/widgets/appbars/appbar_none.dart';
import 'package:askany/src/ui/common/widgets/cake_avatar/cake_avatar.dart';
import 'package:askany/src/ui/style/style.dart';
import 'package:flutter/material.dart';
import 'package:askany/src/helpers/sizer_custom/sizer.dart';
import 'package:askany/src/helpers/extentions/string_extention.dart';

class IncomingCallScreen extends StatefulWidget {
  final VideoCallIncoming state;
  final ConversationModel conversation;
  const IncomingCallScreen({required this.state, required this.conversation});
  @override
  _IncomingCallScreenState createState() => _IncomingCallScreenState();
}

class _IncomingCallScreenState extends State<IncomingCallScreen> {
  @override
  void initState() {
    super.initState();

    startRingAndVibrateIncomingCall();
  }

  @override
  void dispose() {
    stopRingAndVibrateIncomingCall();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: appBarBrighnessDark(brightness: Brightness.dark),
        body: Stack(alignment: Alignment.bottomCenter, children: <Widget>[
          Container(
            height: 100.h,
            width: 100.w,
            color: colorBackgroundVideoCall,
            child: Container(
              padding: EdgeInsets.fromLTRB(0, 20.h, 0, 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    Strings.incomingCall.i18n,
                    style: TextStyle(color: Colors.white, fontSize: 20.sp),
                  ),
                  SizedBox(
                    height: 50.sp,
                  ),
                  CakeAvatar(
                    urls: widget.conversation.avatarConversations,
                    size: 100.sp,
                  ),
                  SizedBox(
                    height: 13.sp,
                  ),
                  Text(
                    widget.conversation.conversationTitle.formatName(),
                    style: TextStyle(color: Colors.white, fontSize: 15.sp),
                  ),
                ],
              ),
            ),
          ),
          Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                height: 100.sp,
                width: 100.h,
                margin: EdgeInsets.fromLTRB(0, 0, 0, 15.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      margin: EdgeInsets.fromLTRB(20.w, 0, 0, 0),
                      child: Column(
                        children: [
                          SizedBox(
                              width: 50.sp,
                              height: 50.sp,
                              child: ElevatedButton(
                                onPressed: () async {
                                  stopRingAndVibrateIncomingCall();
                                  AppBloc.videoCallBloc
                                      .add(EndVideoCallEvent());
                                },
                                child: Icon(
                                  Icons.close,
                                  color: Colors.white,
                                  size: 30.sp,
                                ),
                                style: ElevatedButton.styleFrom(
                                  shape: CircleBorder(),
                                  padding: EdgeInsets.all(5),
                                  primary: Colors.red, // <-- Button color
                                  onPrimary: Colors.red, // <-- Splash color
                                ),
                              )),
                          SizedBox(
                            height: 10.sp,
                          ),
                          Text(
                            Strings.reject.i18n,
                            style:
                                TextStyle(color: Colors.white, fontSize: 13.sp),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 0, 20.w, 0),
                      child: Column(
                        children: [
                          SizedBox(
                              width: 50.sp,
                              height: 50.sp,
                              child: ElevatedButton(
                                onPressed: () {
                                  stopRingAndVibrateIncomingCall();

                                  AppBloc.videoCallBloc.add(
                                    StartVideoCallEvent(
                                      incomingCallModel:
                                          widget.state.incomingCallModel,
                                    ),
                                  );
                                },
                                child: Image.asset(iconVideoCamera,
                                    height: 60.sp, width: 60.sp),
                                style: ElevatedButton.styleFrom(
                                  shape: CircleBorder(),
                                  padding: EdgeInsets.all(5),
                                  primary: Colors.green,
                                  // <-- Button color
                                  onPrimary: Colors.red, // <-- Splash color
                                ),
                              )),
                          SizedBox(
                            height: 10.sp,
                          ),
                          Text(
                            Strings.answer.i18n,
                            style:
                                TextStyle(color: Colors.white, fontSize: 13.sp),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              )),
        ]),
      ),
    );
  }
}
