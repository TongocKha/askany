class Endpoints {
  // Authentication
  static const String LOGIN = "login/user";
  static const String REGISTER = "register/user";
  static const String CHANGE_PASSWORD = 'reset-pass';
  static const String REFRESH_TOKEN = 'refresh-token';
  static const String FORGOT_PASSWORD = 'forgot-pass';
  static const String SWITCH_TOKEN = 'switch-token';
  static const String INFO_BY_TOKEN = 'get-info-by-token';
  static const String USER_REGISTER_EXPERT = 'users/register/expert';

  // Request
  static const String REQUESTS = 'requests';
  static const String GLOBAL_REQUESTS = 'requests/list-mobile';
  static const String GLOBAL_REQUEST_SEARCH_BY_CATEGORY =
      'requests/list-mobile/happening';
  static const String MY_REQUEST = 'requests/my-requests';
  static const String MY_OFFERED_REQUEST =
      'requests/my-offered-requests/offers';
  static const String EXPERT_CANCEL_OFFER = 'offers/expert-cancel';
  static const String USER_END_REQUEST = 'requests/end-of-request';
  static const String DETAILS_OFFER = 'offers';
  static const String EXPERT_TIMELINE = 'time-lines/expert';

  static const String USER_SERVICE_MANAGAMENT =
      'requests/my-requests/offers/customise-status';
  static const String EXPERT_SERVICE_MANAGAMENT =
      'requests/my-offered-requests/offers/customise-status';
  static const String USER_CANCEL_OFFER = 'offers/user-cancel';
  static const String EXPERT_CONFIRM_OFFER = 'offers/confirm';
  static const String USER_COMPLETE_OFFER = 'offers/complete';
  static const String USER_RATING_OFFER = 'offers/rating';
  static const String REPORT_OFFER = 'offers/report';
  static const String EXPERT_REPLY_FEEDBACK = 'experts/rating';
  static const String BOOKING_EXPERT = 'requests/expert';

  // Booking
  static const String UPDATE_TIMELINE_AND_USER_INFO = 'offers/time-line';
  static const String CHOOSE_OFFER = 'requests/choose-offer';
  static const String PAY_BY_PERSONAL_WALLET = 'users/pay-askany';
  static const String PAY_BY_VNPAY = 'users/pay-vnpay';
  static const String BOOKING_TARGET_EXPERT = 'requests/expert/direct';

  // Calendar
  static const String USER_SCHEDULE_REQUEST =
      'requests/my-schedule-requests/offers';
  static const String EXPERT_SCHEDULE_REQUEST =
      'requests/my-schedule-offered-requests/offers';

  // Conversation
  static const String CONVERSATION = 'conversations';
  static const String BADGES_CONVERSATION =
      'conversations/countUnseenConversations';
  static const String USER_CONVERSATION = 'conversations/user';
  static const String EXPERT_CONVERSATION = 'conversations/expert';
  static const String MARK_SEEN_ALL_MESSAGE = 'conversations/seen';
  static const String GET_MESSAGE_BY_CONVERSATION = 'messages/conversation';
  static const String MESSAGES = 'messages';
  static const String EXISTS_CONVERSATION = 'conversations/exist-conversation';
  static const String IGNORE_CONVERSATION = 'conversations/ignore';
  static const String UNIGNORE_CONVERSATION = 'conversations/unignore';

  // User
  static const String GET_USER_LINK_RECHARGE = 'users/pay-vnpay';
  static const String GET_USER_BY_TOKEN = 'users/api/info';
  static const String UPDATE_INFO_USER = 'users';
  static const String BLOCK_EXPERT = 'users/block-experts';
  static const String WITHDRAW_USER = 'users/withdraw';

  // Expert
  static const String GET_EXPERT_LINK_RECHARGE = 'experts/pay-vnpay';
  static const String GET_EXPERT_BY_TOKEN = 'experts/api/info';
  static const String EXPERTS = 'experts';
  static const String RECOMMEND_EXPERTS = 'experts/recommend-experts';
  static const String SEARCH_EXPERT = 'experts/search';
  static const String HOT_EXPERTS = 'components/home/experts';
  static const String WITHDRAW_EXPERT = 'experts/withdraw';

  // Skill
  static const String GET_SKILL = 'skills';
  static const String POSITIONS = 'positions';
  static const String SEARCH_SKILL = 'skills/search';
  static const String RECOMMEND_SKILLS = 'skills/recommend-skills';
  static const String INFO_SKILLS = 'skills/info';

  // Category
  static const String CATEGORY = 'categories';
  static const String CATEGORY_PARENT = 'categories/parent';
  static const String CATEGORY_SPECIALTY = 'categories/specialty';
  static const String CATEGORY_HOME = 'components/home/categories';

  // Specialty
  static const String SPECIALTY = 'specialties';
  static const String MY_SPECIALTY = 'experts/specialties/mine';
  static const String EXPERT_SPECIALTY = 'specialties/experts';

  //update email
  static const String UPDATE_EMAIL = 'update-email';

  //update phone
  static const String UPDATE_PHONE = 'update-phone';

  // verify otp
  static const String VERIFY_OTP = 'verify-update-phone';

  //verify phone
  static const String VERIFY_PHONE = 'xac-thuc-phone';

  //verify company
  static const String VERIFY_COMPANY = 'experts/experience-confirm';

  // Video Call
  static const String CALL = 'calls';

  // Notifications
  static const String NOTIFICATIONS = 'notifications';
  static const String NOTIFICATIONS_LIST = 'notifications/list';
  static const String NOTIFICATIONS_SEEN_ALL = 'notifications/seen-all';
  static const String GET_OFFER_BY_ID = 'offers/v2';

  // Transactions
  static const String USER_TRANSACITONS = 'users/transactions/history';
  static const String EXPERT_TRANSACITONS = 'experts/transactions/history';

  // Rating
  static const String RATING_SKILL = 'skills/rating';
  static const String RATING_EXPERT = 'experts/rating';

  // Experience
  static const String EXPERIENCE = 'experiences';
  static const String GET_EXPERIENCE = 'experts/my-experience';

  // Language
  static const String LANGUAGES = 'languages';

  // Service Prices
  static const String SERVICE_PRICE = 'prices/expert';
  static const String CREATE_SERVICE_PRICE = 'price/create';

  // Discover
  static const String DISCOVER_ADVISES = 'components/discover/advises';

  //Search
  static const String HOT_SEARCH = 'components/home/hot_search';

  //Wallet
  static const String BANKS = 'banks';
}
