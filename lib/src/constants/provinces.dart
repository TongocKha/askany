const provinces = {
  "1": {
    "slug": "thanh-pho-can-tho",
    "name": "Thành phố Cần Thơ",
    "code": 1,
    "districts": {
      "66": {"slug": "huyen-co-do", "name": "Huyện Cờ Đỏ", "code": 66},
      "67": {"slug": "huyen-phong-dien", "name": "Huyện Phong Điền", "code": 67},
      "68": {"slug": "huyen-thoi-lai", "name": "Huyện Thới Lai", "code": 68},
      "69": {"slug": "huyen-vinh-thanh", "name": "Huyện Vĩnh Thạnh", "code": 69},
      "70": {"slug": "quan-binh-thuy", "name": "Quận Bình Thủy", "code": 70},
      "71": {"slug": "quan-cai-rang", "name": "Quận Cái Răng", "code": 71},
      "72": {"slug": "quan-ninh-kieu", "name": "Quận Ninh Kiều", "code": 72},
      "73": {"slug": "quan-o-mon", "name": "Quận Ô Môn", "code": 73},
      "74": {"slug": "quan-thot-not", "name": "Quận Thốt Nốt", "code": 74}
    }
  },
  "2": {
    "slug": "thanh-pho-da-nang",
    "name": "Thành phố Đà Nẵng",
    "code": 2,
    "districts": {
      "76": {"slug": "huyen-hoa-vang", "name": "Huyện Hòa Vang", "code": 76},
      "77": {"slug": "huyen-hoang-sa", "name": "Huyện Hoàng Sa", "code": 77},
      "78": {"slug": "quan-cam-le", "name": "Quận Cẩm Lệ", "code": 78},
      "79": {"slug": "quan-hai-chau", "name": "Quận Hải Châu", "code": 79},
      "80": {"slug": "quan-lien-chieu", "name": "Quận Liên Chiểu", "code": 80},
      "81": {"slug": "quan-ngu-hanh-son", "name": "Quận Ngũ Hành Sơn", "code": 81},
      "82": {"slug": "quan-son-tra", "name": "Quận Sơn Trà", "code": 82},
      "83": {"slug": "quan-thanh-khe", "name": "Quận Thanh Khê", "code": 83}
    }
  },
  "3": {
    "slug": "thanh-pho-ha-noi",
    "name": "Thành phố Hà Nội",
    "code": 3,
    "districts": {
      "85": {"slug": "huyen-ba-vi", "name": "Huyện Ba Vì", "code": 85},
      "86": {"slug": "huyen-chuong-my", "name": "Huyện Chương Mỹ", "code": 86},
      "87": {"slug": "huyen-dan-phuong", "name": "Huyện Đan Phượng", "code": 87},
      "88": {"slug": "huyen-dong-anh", "name": "Huyện Đông Anh", "code": 88},
      "89": {"slug": "huyen-gia-lam", "name": "Huyện Gia Lâm", "code": 89},
      "90": {"slug": "huyen-hoai-duc", "name": "Huyện Hoài Đức", "code": 90},
      "91": {"slug": "huyen-me-linh", "name": "Huyện Mê Linh", "code": 91},
      "92": {"slug": "huyen-my-duc", "name": "Huyện Mỹ Đức", "code": 92},
      "93": {"slug": "huyen-phu-xuyen", "name": "Huyện Phú Xuyên", "code": 93},
      "94": {"slug": "huyen-phuc-tho", "name": "Huyện Phúc Thọ", "code": 94},
      "95": {"slug": "huyen-quoc-oai", "name": "Huyện Quốc Oai", "code": 95},
      "96": {"slug": "huyen-soc-son", "name": "Huyện Sóc Sơn", "code": 96},
      "97": {"slug": "huyen-thach-that", "name": "Huyện Thạch Thất", "code": 97},
      "98": {"slug": "huyen-thanh-oai", "name": "Huyện Thanh Oai", "code": 98},
      "99": {"slug": "huyen-thanh-tri", "name": "Huyện Thanh Trì", "code": 99},
      "100": {"slug": "huyen-thuong-tin", "name": "Huyện Thường Tín", "code": 100},
      "101": {"slug": "huyen-tu-liem", "name": "Huyện Từ Liêm", "code": 101},
      "102": {"slug": "huyen-ung-hoa", "name": "Huyện ứng Hòa", "code": 102},
      "103": {"slug": "quan-ba-dinh", "name": "Quận Ba Đình", "code": 103},
      "104": {"slug": "quan-cau-giay", "name": "Quận Cầu Giấy", "code": 104},
      "105": {"slug": "quan-dong-da", "name": "Quận Đống Đa", "code": 105},
      "106": {"slug": "quan-ha-dong", "name": "Quận Hà Đông", "code": 106},
      "107": {"slug": "quan-hai-ba-trung", "name": "Quận Hai Bà Trưng", "code": 107},
      "108": {"slug": "quan-hoan-kiem", "name": "Quận Hoàn Kiếm", "code": 108},
      "109": {"slug": "quan-hoang-mai", "name": "Quận Hoàng Mai", "code": 109},
      "110": {"slug": "quan-long-bien", "name": "Quận Long Biên", "code": 110},
      "111": {"slug": "quan-tay-ho", "name": "Quận Tây Hồ", "code": 111},
      "112": {"slug": "quan-thanh-xuan", "name": "Quận Thanh Xuân", "code": 112},
      "113": {"slug": "thi-xa-son-tay", "name": "Thị xã Sơn Tây", "code": 113}
    }
  },
  "4": {
    "slug": "thanh-pho-hai-phong",
    "name": "Thành phố Hải Phòng",
    "code": 4,
    "districts": {
      "115": {"slug": "huyen-an-duong", "name": "Huyện An Dương", "code": 115},
      "116": {"slug": "huyen-an-lao", "name": "Huyện An Lão", "code": 116},
      "117": {"slug": "huyen-bach-long-vi", "name": "Huyện Bạch Long Vĩ", "code": 117},
      "118": {"slug": "huyen-cat-hai", "name": "Huyện Cát Hải", "code": 118},
      "119": {"slug": "huyen-kien-thuy", "name": "Huyện Kiến Thụy", "code": 119},
      "120": {"slug": "huyen-thuy-nguyen", "name": "Huyện Thủy Nguyên", "code": 120},
      "121": {"slug": "huyen-tien-lang", "name": "Huyện Tiên Lãng", "code": 121},
      "122": {"slug": "huyen-vinh-bao", "name": "Huyện Vĩnh Bảo", "code": 122},
      "123": {"slug": "quan-do-son", "name": "Quận Đồ Sơn", "code": 123},
      "124": {"slug": "quan-duong-kinh", "name": "Quận Dương Kinh", "code": 124},
      "125": {"slug": "quan-hai-an", "name": "Quận Hải An", "code": 125},
      "126": {"slug": "quan-hong-bang", "name": "Quận Hồng Bàng", "code": 126},
      "127": {"slug": "quan-kien-an", "name": "Quận Kiến An", "code": 127},
      "128": {"slug": "quan-le-chan", "name": "Quận Lê Chân", "code": 128},
      "129": {"slug": "quan-ngo-quyen", "name": "Quận Ngô Quyền", "code": 129}
    }
  },
  "5": {
    "slug": "thanh-pho-ho-chi-minh",
    "name": "Thành phố Hồ Chí Minh",
    "code": 5,
    "districts": {
      "131": {"slug": "huyen-binh-chanh", "name": "Huyện Bình Chánh", "code": 131},
      "132": {"slug": "huyen-can-gio", "name": "Huyện Cần Giờ", "code": 132},
      "133": {"slug": "huyen-cu-chi", "name": "Huyện Củ Chi", "code": 133},
      "134": {"slug": "huyen-hoc-mon", "name": "Huyện Hóc Môn", "code": 134},
      "135": {"slug": "huyen-nha-be", "name": "Huyện Nhà Bè", "code": 135},
      "136": {"slug": "quan-1", "name": "Quận 1", "code": 136},
      "137": {"slug": "quan-10", "name": "Quận 10", "code": 137},
      "138": {"slug": "quan-11", "name": "Quận 11", "code": 138},
      "139": {"slug": "quan-12", "name": "Quận 12", "code": 139},
      "140": {"slug": "quan-2", "name": "Quận 2", "code": 140},
      "141": {"slug": "quan-3", "name": "Quận 3", "code": 141},
      "142": {"slug": "quan-4", "name": "Quận 4", "code": 142},
      "143": {"slug": "quan-5", "name": "Quận 5", "code": 143},
      "144": {"slug": "quan-6", "name": "Quận 6", "code": 144},
      "145": {"slug": "quan-7", "name": "Quận 7", "code": 145},
      "146": {"slug": "quan-8", "name": "Quận 8", "code": 146},
      "147": {"slug": "quan-9", "name": "Quận 9", "code": 147},
      "148": {"slug": "quan-binh-tan", "name": "Quận Bình Tân", "code": 148},
      "149": {"slug": "quan-binh-thanh", "name": "Quận Bình Thạnh", "code": 149},
      "150": {"slug": "quan-go-vap", "name": "Quận Gò Vấp", "code": 150},
      "151": {"slug": "quan-phu-nhuan", "name": "Quận Phú Nhuận", "code": 151},
      "152": {"slug": "quan-tan-binh", "name": "Quận Tân Bình", "code": 152},
      "153": {"slug": "quan-tan-phu", "name": "Quận Tân Phú", "code": 153},
      "154": {"slug": "quan-thu-duc", "name": "Quận Thủ Đức", "code": 154}
    }
  },
  "6": {
    "slug": "tinh-an-giang",
    "name": "Tỉnh An Giang",
    "code": 6,
    "districts": {
      "155": {"slug": "huyen-an-phu", "name": "Huyện An Phú", "code": 155},
      "156": {"slug": "huyen-chau-phu", "name": "Huyện Châu Phú", "code": 156},
      "157": {"slug": "huyen-chau-thanh", "name": "Huyện Châu Thành", "code": 157},
      "158": {"slug": "huyen-cho-moi", "name": "Huyện Chợ Mới", "code": 158},
      "159": {"slug": "huyen-phu-tan", "name": "Huyện Phú Tân", "code": 159},
      "160": {"slug": "huyen-thoai-son", "name": "Huyện Thoại Sơn", "code": 160},
      "161": {"slug": "huyen-tinh-bien", "name": "Huyện Tịnh Biên", "code": 161},
      "162": {"slug": "huyen-tri-ton", "name": "Huyện Tri Tôn", "code": 162},
      "163": {"slug": "thanh-pho-long-xuyen", "name": "Thành phố Long Xuyên", "code": 163},
      "164": {"slug": "thi-xa-chau-doc", "name": "Thị xã Châu Đốc", "code": 164},
      "165": {"slug": "thi-xa-tan-chau", "name": "Thị xã Tân Châu", "code": 165}
    }
  },
  "7": {
    "slug": "tinh-ba-ria-vung-tau",
    "name": "Tỉnh Bà Rịa-Vũng Tàu",
    "code": 7,
    "districts": {
      "166": {"slug": "huyen-chau-duc", "name": "Huyện Châu Đức", "code": 166},
      "167": {"slug": "huyen-con-dao", "name": "Huyện Côn Đảo", "code": 167},
      "168": {"slug": "huyen-dat-do", "name": "Huyện Đất Đỏ", "code": 168},
      "169": {"slug": "huyen-long-dien", "name": "Huyện Long Điền", "code": 169},
      "170": {"slug": "huyen-tan-thanh", "name": "Huyện Tân Thành", "code": 170},
      "171": {"slug": "huyen-xuyen-moc", "name": "Huyện Xuyên Mộc", "code": 171},
      "172": {"slug": "thanh-pho-vung-tau", "name": "Thành phố Vũng Tàu", "code": 172},
      "173": {"slug": "thi-xa-ba-ria", "name": "Thị xã Bà Rịa", "code": 173},
      "174": {"slug": "thi-xa-phu-my", "name": "Thị xã Phú Mỹ", "code": 174}
    }
  },
  "8": {
    "slug": "tinh-bac-giang",
    "name": "Tỉnh Bắc Giang",
    "code": 8,
    "districts": {
      "174": {"slug": "huyen-hiep-hoa", "name": "Huyện Hiệp Hòa", "code": 174},
      "175": {"slug": "huyen-lang-giang", "name": "Huyện Lạng Giang", "code": 175},
      "176": {"slug": "huyen-luc-nam", "name": "Huyện Lục Nam", "code": 176},
      "177": {"slug": "huyen-luc-ngan", "name": "Huyện Lục Ngạn", "code": 177},
      "178": {"slug": "huyen-son-dong", "name": "Huyện Sơn Động", "code": 178},
      "179": {"slug": "huyen-tan-yen", "name": "Huyện Tân Yên", "code": 179},
      "180": {"slug": "huyen-viet-yen", "name": "Huyện Việt Yên", "code": 180},
      "181": {"slug": "huyen-yen-dung", "name": "Huyện Yên Dũng", "code": 181},
      "182": {"slug": "huyen-yen-the", "name": "Huyện Yên Thế", "code": 182},
      "183": {"slug": "thanh-pho-bac-giang", "name": "Thành phố Bắc Giang", "code": 183}
    }
  },
  "9": {
    "slug": "tinh-bac-kan",
    "name": "Tỉnh Bắc Kạn",
    "code": 9,
    "districts": {
      "184": {"slug": "huyen-ba-be", "name": "Huyện Ba Bể", "code": 184},
      "185": {"slug": "huyen-bach-thong", "name": "Huyện Bạch Thông", "code": 185},
      "186": {"slug": "huyen-cho-don", "name": "Huyện Chợ Đồn", "code": 186},
      "187": {"slug": "huyen-cho-moi", "name": "Huyện Chợ Mới", "code": 187},
      "188": {"slug": "huyen-na-ri", "name": "Huyện Na Rì", "code": 188},
      "189": {"slug": "huyen-ngan-son", "name": "Huyện Ngân Sơn", "code": 189},
      "190": {"slug": "huyen-pac-nam", "name": "Huyện Pác Nặm", "code": 190},
      "191": {"slug": "thi-xa-bac-kan", "name": "Thị xã Bắc Kạn", "code": 191}
    }
  },
  "10": {
    "slug": "tinh-bac-lieu",
    "name": "Tỉnh Bạc Liêu",
    "code": 10,
    "districts": {
      "192": {"slug": "huyen-dong-hai", "name": "Huyện Đông Hải", "code": 192},
      "193": {"slug": "huyen-gia-rai", "name": "Huyện Giá Rai", "code": 193},
      "194": {"slug": "huyen-hoa-binh", "name": "Huyện Hòa Bình", "code": 194},
      "195": {"slug": "huyen-hong-dan", "name": "Huyện Hồng Dân", "code": 195},
      "196": {"slug": "huyen-phuoc-long", "name": "Huyện Phước Long", "code": 196},
      "197": {"slug": "huyen-vinh-loi", "name": "Huyện Vĩnh Lợi", "code": 197},
      "198": {"slug": "thanh-pho-bac-lieu", "name": "Thành Phố Bạc Liêu", "code": 198}
    }
  },
  "11": {
    "slug": "tinh-bac-ninh",
    "name": "Tỉnh Bắc Ninh",
    "code": 11,
    "districts": {
      "199": {"slug": "huyen-gia-binh", "name": "Huyện Gia Bình", "code": 199},
      "200": {"slug": "huyen-luong-tai", "name": "Huyện Lương Tài", "code": 200},
      "201": {"slug": "huyen-que-vo", "name": "Huyện Quế Võ", "code": 201},
      "202": {"slug": "huyen-thuan-thanh", "name": "Huyện Thuận Thành", "code": 202},
      "203": {"slug": "huyen-tien-du", "name": "Huyện Tiên Du", "code": 203},
      "204": {"slug": "huyen-yen-phong", "name": "Huyện Yên Phong", "code": 204},
      "205": {"slug": "thanh-pho-bac-ninh", "name": "Thành phố Bắc Ninh", "code": 205},
      "206": {"slug": "thi-xa-tu-son", "name": "Thị xã Từ Sơn", "code": 206}
    }
  },
  "12": {
    "slug": "tinh-ben-tre",
    "name": "Tỉnh Bến Tre",
    "code": 12,
    "districts": {
      "207": {"slug": "huyen-ba-tri", "name": "Huyện Ba Tri", "code": 207},
      "208": {"slug": "huyen-binh-dai", "name": "Huyện Bình Đại", "code": 208},
      "209": {"slug": "huyen-chau-thanh", "name": "Huyện Châu Thành", "code": 209},
      "210": {"slug": "huyen-cho-lach", "name": "Huyện Chợ Lách", "code": 210},
      "211": {"slug": "huyen-giong-trom", "name": "Huyện Giồng Trôm", "code": 211},
      "212": {"slug": "huyen-mo-cay-bac", "name": "Huyện Mỏ Cày Bắc", "code": 212},
      "213": {"slug": "huyen-mo-cay-nam", "name": "Huyện Mỏ Cày Nam", "code": 213},
      "214": {"slug": "huyen-thanh-phu", "name": "Huyện Thạnh Phú", "code": 214},
      "215": {"slug": "thanh-pho-ben-tre", "name": "Thành phố Bến Tre", "code": 215}
    }
  },
  "13": {
    "slug": "tinh-binh-dinh",
    "name": "Tỉnh Bình Định",
    "code": 13,
    "districts": {
      "216": {"slug": "huyen-an-lao", "name": "Huyện An Lão", "code": 216},
      "217": {"slug": "huyen-an-nhon", "name": "Huyện An Nhơn", "code": 217},
      "218": {"slug": "huyen-hoai-an", "name": "Huyện Hoài  Ân", "code": 218},
      "219": {"slug": "huyen-hoai-nhon", "name": "Huyện Hoài Nhơn", "code": 219},
      "220": {"slug": "huyen-phu-my", "name": "Huyện Phù Mỹ", "code": 220},
      "221": {"slug": "huyen-phu-cat", "name": "Huyện Phù cát", "code": 221},
      "222": {"slug": "huyen-tay-son", "name": "Huyện Tây Sơn", "code": 222},
      "223": {"slug": "huyen-tuy-phuoc", "name": "Huyện Tuy Phước", "code": 223},
      "224": {"slug": "huyen-van-canh", "name": "Huyện Vân Canh", "code": 224},
      "225": {"slug": "huyen-vinh-thanh", "name": "Huyện Vĩnh Thạnh", "code": 225},
      "226": {"slug": "thanh-pho-quy-nhon", "name": "Thành phố Quy Nhơn", "code": 226}
    }
  },
  "14": {
    "slug": "tinh-binh-duong",
    "name": "Tỉnh Bình Dương",
    "code": 14,
    "districts": {
      "227": {"slug": "huyen-ben-cat", "name": "Huyện Bến Cát", "code": 227},
      "228": {"slug": "huyen-dau-tieng", "name": "Huyện Dầu Tiếng", "code": 228},
      "229": {"slug": "huyen-di-an", "name": "Huyện Dĩ An", "code": 229},
      "230": {"slug": "huyen-phu-giao", "name": "Huyện Phú Giáo", "code": 230},
      "231": {"slug": "huyen-tan-uyen", "name": "Huyện Tân Uyên", "code": 231},
      "232": {"slug": "huyen-thuan-an", "name": "Huyện Thuận An", "code": 232},
      "233": {"slug": "thi-xa-thu-dau-mot", "name": "Thị xã Thủ Dầu Một", "code": 233}
    }
  },
  "15": {
    "slug": "tinh-binh-phuoc",
    "name": "Tỉnh Bình Phước",
    "code": 15,
    "districts": {
      "234": {"slug": "huyen-bu-dang", "name": "Huyện Bù Đăng", "code": 234},
      "235": {"slug": "huyen-bu-dop", "name": "Huyện Bù Đốp", "code": 235},
      "236": {"slug": "huyen-bu-gia-map", "name": "Huyện Bù Gia Mập", "code": 236},
      "237": {"slug": "huyen-chon-thanh", "name": "Huyện Chơn Thành", "code": 237},
      "238": {"slug": "huyen-dong-phu", "name": "Huyện Đồng Phú", "code": 238},
      "239": {"slug": "huyen-hon-quan", "name": "Huyện Hớn Quản", "code": 239},
      "240": {"slug": "huyen-loc-ninh", "name": "Huyện Lộc Ninh", "code": 240},
      "241": {"slug": "thi-xa-binh-long", "name": "Thị xã Bình Long", "code": 241},
      "242": {"slug": "thi-xa-dong-xoai", "name": "Thị xã Đồng Xoài", "code": 242},
      "243": {"slug": "thi-xa-phuoc-long", "name": "Thị xã Phước Long", "code": 243}
    }
  },
  "16": {
    "slug": "tinh-binh-thuan",
    "name": "Tỉnh Bình Thuận",
    "code": 16,
    "districts": {
      "244": {"slug": "huyen-duc-linh", "name": "Huyện  Đức Linh", "code": 244},
      "245": {"slug": "huyen-bac-binh", "name": "Huyện Bắc Bình", "code": 245},
      "246": {"slug": "huyen-ham-tan", "name": "Huyện Hàm Tân", "code": 246},
      "247": {"slug": "huyen-ham-thuan-bac", "name": "Huyện Hàm Thuận Bắc", "code": 247},
      "248": {"slug": "huyen-ham-thuan-nam", "name": "Huyện Hàm Thuận Nam", "code": 248},
      "249": {"slug": "huyen-phu-qui", "name": "Huyện Phú Qúi", "code": 249},
      "250": {"slug": "huyen-tanh-linh", "name": "Huyện Tánh Linh", "code": 250},
      "251": {"slug": "huyen-tuy-phong", "name": "Huyện Tuy Phong", "code": 251},
      "252": {"slug": "thanh-pho-phan-thiet", "name": "Thành phố Phan Thiết", "code": 252},
      "253": {"slug": "thi-xa-la-gi", "name": "Thị xã La Gi", "code": 253}
    }
  },
  "17": {
    "slug": "tinh-ca-mau",
    "name": "Tỉnh Cà Mau",
    "code": 17,
    "districts": {
      "254": {"slug": "huyen-cai-nuoc", "name": "Huyện Cái Nước", "code": 254},
      "255": {"slug": "huyen-dam-doi", "name": "Huyện Đầm Dơi", "code": 255},
      "256": {"slug": "huyen-nam-can", "name": "Huyện Năm Căn", "code": 256},
      "257": {"slug": "huyen-ngoc-hien", "name": "Huyện Ngọc Hiển", "code": 257},
      "258": {"slug": "huyen-phu-tan", "name": "Huyện Phú Tân", "code": 258},
      "259": {"slug": "huyen-thoi-binh", "name": "Huyện Thới Bình", "code": 259},
      "260": {"slug": "huyen-tran-van-thoi", "name": "Huyện Trần Văn Thời", "code": 260},
      "261": {"slug": "huyen-u-minh", "name": "Huyện U Minh", "code": 261},
      "262": {"slug": "thanh-pho-ca-mau", "name": "Thành phố Cà Mau", "code": 262}
    }
  },
  "18": {
    "slug": "tinh-cao-bang",
    "name": "Tỉnh Cao Bằng",
    "code": 18,
    "districts": {
      "263": {"slug": "huyen-bao-lac", "name": "Huyện Bảo Lạc", "code": 263},
      "264": {"slug": "huyen-bao-lam", "name": "Huyện Bảo Lâm", "code": 264},
      "265": {"slug": "huyen-ha-lang", "name": "Huyện Hạ Lang", "code": 265},
      "266": {"slug": "huyen-ha-quang", "name": "Huyện Hà Quảng", "code": 266},
      "267": {"slug": "huyen-hoa-an", "name": "Huyện Hòa An", "code": 267},
      "268": {"slug": "huyen-nguyen-binh", "name": "Huyện Nguyên Bình", "code": 268},
      "269": {"slug": "huyen-phuc-hoa", "name": "Huyện Phục Hòa", "code": 269},
      "270": {"slug": "huyen-quang-uyen", "name": "Huyện Quảng Uyên", "code": 270},
      "271": {"slug": "huyen-thach-an", "name": "Huyện Thạch An", "code": 271},
      "272": {"slug": "huyen-thong-nong", "name": "Huyện Thông Nông", "code": 272},
      "273": {"slug": "huyen-tra-linh", "name": "Huyện Trà Lĩnh", "code": 273},
      "274": {"slug": "huyen-trung-khanh", "name": "Huyện Trùng Khánh", "code": 274},
      "275": {"slug": "thi-xa-cao-bang", "name": "Thị xã Cao Bằng", "code": 275}
    }
  },
  "19": {
    "slug": "tinh-dak-lak",
    "name": "Tỉnh Đắk Lắk",
    "code": 19,
    "districts": {
      "276": {"slug": "huyen-buon-don", "name": "Huyện Buôn Đôn", "code": 276},
      "277": {"slug": "huyen-cu-kuin", "name": "Huyện Cư Kuin", "code": 277},
      "278": {"slug": "huyen-cu-mgar", "name": "Huyện Cư MGar", "code": 278},
      "279": {"slug": "huyen-ea-kar", "name": "Huyện Ea Kar", "code": 279},
      "280": {"slug": "huyen-ea-sup", "name": "Huyện Ea Súp", "code": 280},
      "281": {"slug": "huyen-eahleo", "name": "Huyện EaHLeo", "code": 281},
      "282": {"slug": "huyen-krong-ana", "name": "Huyện Krông Ana", "code": 282},
      "283": {"slug": "huyen-krong-bong", "name": "Huyện Krông Bông", "code": 283},
      "284": {"slug": "huyen-krong-buk", "name": "Huyện Krông Búk", "code": 284},
      "285": {"slug": "huyen-krong-nang", "name": "Huyện Krông Năng", "code": 285},
      "286": {"slug": "huyen-krong-pac", "name": "Huyện Krông Pắc", "code": 286},
      "287": {"slug": "huyen-lak", "name": "Huyện Lắk", "code": 287},
      "288": {"slug": "huyen-mdrak", "name": "Huyện MDrắk", "code": 288},
      "289": {"slug": "thanh-pho-buon-ma-thuot", "name": "Thành phố Buôn Ma Thuột", "code": 289},
      "290": {"slug": "thi-xa-buon-ho", "name": "Thị xã Buôn Hồ", "code": 290}
    }
  },
  "20": {
    "slug": "tinh-dak-nong",
    "name": "Tỉnh Đắk Nông",
    "code": 20,
    "districts": {
      "291": {"slug": "huyen-cu-jut", "name": "Huyện Cư Jút", "code": 291},
      "292": {"slug": "huyen-dak-glong", "name": "Huyện Đắk GLong", "code": 292},
      "293": {"slug": "huyen-dak-mil", "name": "Huyện Đắk Mil", "code": 293},
      "294": {"slug": "huyen-dak-rlap", "name": "Huyện Đắk RLấp", "code": 294},
      "295": {"slug": "huyen-dak-song", "name": "Huyện Đắk Song", "code": 295},
      "296": {"slug": "huyen-krong-no", "name": "Huyện KRông Nô", "code": 296},
      "297": {"slug": "huyen-tuy-duc", "name": "Huyện Tuy Đức", "code": 297},
      "298": {"slug": "thi-xa-gia-nghia", "name": "Thị xã Gia Nghĩa", "code": 298}
    }
  },
  "21": {
    "slug": "tinh-dien-bien",
    "name": "Tỉnh Điện Biên",
    "code": 21,
    "districts": {
      "299": {"slug": "huyen-dien-bien", "name": "Huyện Điện Biên", "code": 299},
      "300": {"slug": "huyen-dien-bien-dong", "name": "Huyện Điện Biên Đông", "code": 300},
      "301": {"slug": "huyen-muong-cha", "name": "Huyện Mường Chà", "code": 301},
      "302": {"slug": "huyen-muong-nhe", "name": "Huyện Mương Nhé", "code": 302},
      "303": {"slug": "huyen-muong-ang", "name": "Huyện Mường ảng", "code": 303},
      "304": {"slug": "huyen-tua-chua", "name": "Huyện Tủa Chùa", "code": 304},
      "305": {"slug": "huyen-tuan-giao", "name": "Huyện Tuần Giáo", "code": 305},
      "306": {"slug": "thanh-pho-dien-bien-phu", "name": "Thành phố Điện Biên phủ", "code": 306},
      "307": {"slug": "thi-xa-muong-lay", "name": "Thị xã Mường Lay", "code": 307}
    }
  },
  "22": {
    "slug": "tinh-dong-nai",
    "name": "Tỉnh Đồng Nai",
    "code": 22,
    "districts": {
      "308": {"slug": "huyen-cam-my", "name": "Huyện Cẩm Mỹ", "code": 308},
      "309": {"slug": "huyen-dinh-quan", "name": "Huyện Định Quán", "code": 309},
      "310": {"slug": "huyen-long-thanh", "name": "Huyện Long Thành", "code": 310},
      "311": {"slug": "huyen-nhon-trach", "name": "Huyện Nhơn Trạch", "code": 311},
      "312": {"slug": "huyen-tan-phu", "name": "Huyện Tân Phú", "code": 312},
      "313": {"slug": "huyen-thong-nhat", "name": "Huyện Thống Nhất", "code": 313},
      "314": {"slug": "huyen-trang-bom", "name": "Huyện Trảng Bom", "code": 314},
      "315": {"slug": "huyen-vinh-cuu", "name": "Huyện Vĩnh Cửu", "code": 315},
      "316": {"slug": "huyen-xuan-loc", "name": "Huyện Xuân Lộc", "code": 316},
      "317": {"slug": "thanh-pho-bien-hoa", "name": "Thành phố Biên Hòa", "code": 317},
      "318": {"slug": "thi-xa-long-khanh", "name": "Thị xã Long Khánh", "code": 318}
    }
  },
  "23": {
    "slug": "tinh-dong-thap",
    "name": "Tỉnh Đồng Tháp",
    "code": 23,
    "districts": {
      "319": {"slug": "huyen-cao-lanh", "name": "Huyện Cao Lãnh", "code": 319},
      "320": {"slug": "huyen-chau-thanh", "name": "Huyện Châu Thành", "code": 320},
      "321": {"slug": "huyen-hong-ngu", "name": "Huyện Hồng Ngự", "code": 321},
      "322": {"slug": "huyen-lai-vung", "name": "Huyện Lai Vung", "code": 322},
      "323": {"slug": "huyen-lap-vo", "name": "Huyện Lấp Vò", "code": 323},
      "324": {"slug": "huyen-tam-nong", "name": "Huyện Tam Nông", "code": 324},
      "325": {"slug": "huyen-tan-hong", "name": "Huyện Tân Hồng", "code": 325},
      "326": {"slug": "huyen-thanh-binh", "name": "Huyện Thanh Bình", "code": 326},
      "327": {"slug": "huyen-thap-muoi", "name": "Huyện Tháp Mười", "code": 327},
      "328": {"slug": "thanh-pho-cao-lanh", "name": "Thành phố Cao Lãnh", "code": 328},
      "329": {"slug": "thi-xa-hong-ngu", "name": "Thị xã Hồng Ngự", "code": 329},
      "330": {"slug": "thi-xa-sa-dec", "name": "Thị xã Sa Đéc", "code": 330}
    }
  },
  "24": {
    "slug": "tinh-gia-lai",
    "name": "Tỉnh Gia Lai",
    "code": 24,
    "districts": {
      "331": {"slug": "huyen-chu-pah", "name": "Huyện Chư Păh", "code": 331},
      "332": {"slug": "huyen-chu-puh", "name": "Huyện Chư Pưh", "code": 332},
      "333": {"slug": "huyen-chu-se", "name": "Huyện Chư Sê", "code": 333},
      "334": {"slug": "huyen-chuprong", "name": "Huyện ChưPRông", "code": 334},
      "335": {"slug": "huyen-dak-doa", "name": "Huyện Đăk Đoa", "code": 335},
      "336": {"slug": "huyen-dak-po", "name": "Huyện Đăk Pơ", "code": 336},
      "337": {"slug": "huyen-duc-co", "name": "Huyện Đức Cơ", "code": 337},
      "338": {"slug": "huyen-ia-grai", "name": "Huyện Ia Grai", "code": 338},
      "339": {"slug": "huyen-ia-pa", "name": "Huyện Ia Pa", "code": 339},
      "340": {"slug": "huyen-kbang", "name": "Huyện KBang", "code": 340},
      "341": {"slug": "huyen-kbang", "name": "Huyện KBang", "code": 341},
      "342": {"slug": "huyen-kong-chro", "name": "Huyện Kông Chro", "code": 342},
      "343": {"slug": "huyen-krong-pa", "name": "Huyện Krông Pa", "code": 343},
      "344": {"slug": "huyen-mang-yang", "name": "Huyện Mang Yang", "code": 344},
      "345": {"slug": "huyen-phu-thien", "name": "Huyện Phú Thiện", "code": 345},
      "346": {"slug": "thanh-pho-plei-ku", "name": "Thành phố Plei Ku", "code": 346},
      "347": {"slug": "thi-xa-ayun-pa", "name": "Thị xã AYun Pa", "code": 347},
      "348": {"slug": "thi-xa-an-khe", "name": "Thị xã An Khê", "code": 348}
    }
  },
  "25": {
    "slug": "tinh-ha-giang",
    "name": "Tỉnh Hà Giang",
    "code": 25,
    "districts": {
      "349": {"slug": "huyen-bac-me", "name": "Huyện Bắc Mê", "code": 349},
      "350": {"slug": "huyen-bac-quang", "name": "Huyện Bắc Quang", "code": 350},
      "351": {"slug": "huyen-dong-van", "name": "Huyện Đồng Văn", "code": 351},
      "352": {"slug": "huyen-hoang-su-phi", "name": "Huyện Hoàng Su Phì", "code": 352},
      "353": {"slug": "huyen-meo-vac", "name": "Huyện Mèo Vạc", "code": 353},
      "354": {"slug": "huyen-quan-ba", "name": "Huyện Quản Bạ", "code": 354},
      "355": {"slug": "huyen-quang-binh", "name": "Huyện Quang Bình", "code": 355},
      "356": {"slug": "huyen-vi-xuyen", "name": "Huyện Vị Xuyên", "code": 356},
      "357": {"slug": "huyen-xin-man", "name": "Huyện Xín Mần", "code": 357},
      "358": {"slug": "huyen-yen-minh", "name": "Huyện Yên Minh", "code": 358},
      "359": {"slug": "thanh-pho-ha-giang", "name": "Thành Phố Hà Giang", "code": 359}
    }
  },
  "26": {
    "slug": "tinh-ha-nam",
    "name": "Tỉnh Hà Nam",
    "code": 26,
    "districts": {
      "360": {"slug": "huyen-binh-luc", "name": "Huyện Bình Lục", "code": 360},
      "361": {"slug": "huyen-duy-tien", "name": "Huyện Duy Tiên", "code": 361},
      "362": {"slug": "huyen-kim-bang", "name": "Huyện Kim Bảng", "code": 362},
      "363": {"slug": "huyen-ly-nhan", "name": "Huyện Lý Nhân", "code": 363},
      "364": {"slug": "huyen-thanh-liem", "name": "Huyện Thanh Liêm", "code": 364},
      "365": {"slug": "thanh-pho-phu-ly", "name": "Thành phố Phủ Lý", "code": 365}
    }
  },
  "27": {
    "slug": "tinh-ha-tinh",
    "name": "Tỉnh Hà Tĩnh",
    "code": 27,
    "districts": {
      "366": {"slug": "huyen-cam-xuyen", "name": "Huyện Cẩm Xuyên", "code": 366},
      "367": {"slug": "huyen-can-loc", "name": "Huyện Can Lộc", "code": 367},
      "368": {"slug": "huyen-duc-tho", "name": "Huyện Đức Thọ", "code": 368},
      "369": {"slug": "huyen-huong-khe", "name": "Huyện Hương Khê", "code": 369},
      "370": {"slug": "huyen-huong-son", "name": "Huyện Hương Sơn", "code": 370},
      "371": {"slug": "huyen-ky-anh", "name": "Huyện Kỳ Anh", "code": 371},
      "372": {"slug": "huyen-loc-ha", "name": "Huyện Lộc Hà", "code": 372},
      "373": {"slug": "huyen-nghi-xuan", "name": "Huyện Nghi Xuân", "code": 373},
      "374": {"slug": "huyen-thach-ha", "name": "Huyện Thạch Hà", "code": 374},
      "375": {"slug": "huyen-vu-quang", "name": "Huyện Vũ Quang", "code": 375},
      "376": {"slug": "thanh-pho-ha-tinh", "name": "Thành phố Hà Tĩnh", "code": 376},
      "377": {"slug": "thi-xa-hong-linh", "name": "Thị xã Hồng Lĩnh", "code": 377}
    }
  },
  "28": {
    "slug": "tinh-hai-duong",
    "name": "Tỉnh Hải Dương",
    "code": 28,
    "districts": {
      "378": {"slug": "huyen-binh-giang", "name": "Huyện Bình Giang", "code": 378},
      "379": {"slug": "huyen-cam-giang", "name": "Huyện Cẩm Giàng", "code": 379},
      "380": {"slug": "huyen-gia-loc", "name": "Huyện Gia Lộc", "code": 380},
      "381": {"slug": "huyen-kim-thanh", "name": "Huyện Kim Thành", "code": 381},
      "382": {"slug": "huyen-kinh-mon", "name": "Huyện Kinh Môn", "code": 382},
      "383": {"slug": "huyen-nam-sach", "name": "Huyện Nam Sách", "code": 383},
      "384": {"slug": "huyen-ninh-giang", "name": "Huyện Ninh Giang", "code": 384},
      "385": {"slug": "huyen-thanh-ha", "name": "Huyện Thanh Hà", "code": 385},
      "386": {"slug": "huyen-thanh-mien", "name": "Huyện Thanh Miện", "code": 386},
      "387": {"slug": "huyen-tu-ky", "name": "Huyện Tứ Kỳ", "code": 387},
      "388": {"slug": "thanh-pho-hai-duong", "name": "Thành phố Hải Dương", "code": 388},
      "389": {"slug": "thi-xa-chi-linh", "name": "Thị xã Chí Linh", "code": 389}
    }
  },
  "29": {
    "slug": "tinh-hau-giang",
    "name": "Tỉnh Hậu Giang",
    "code": 29,
    "districts": {
      "390": {"slug": "huyen-chau-thanh", "name": "Huyện Châu Thành", "code": 390},
      "391": {"slug": "huyen-chau-thanh-a", "name": "Huyện Châu Thành A", "code": 391},
      "392": {"slug": "huyen-long-my", "name": "Huyện Long Mỹ", "code": 392},
      "393": {"slug": "huyen-phung-hiep", "name": "Huyện Phụng Hiệp", "code": 393},
      "394": {"slug": "huyen-vi-thuy", "name": "Huyện Vị Thủy", "code": 394},
      "395": {"slug": "thanh-pho-vi-thanh", "name": "Thành Phố Vị Thanh", "code": 395},
      "396": {"slug": "thi-xa-nga-bay", "name": "Thị xã Ngã Bảy", "code": 396}
    }
  },
  "30": {
    "slug": "tinh-hoa-binh",
    "name": "Tỉnh Hòa Bình",
    "code": 30,
    "districts": {
      "397": {"slug": "huyen-cao-phong", "name": "Huyện Cao Phong", "code": 397},
      "398": {"slug": "huyen-da-bac", "name": "Huyện Đà Bắc", "code": 398},
      "399": {"slug": "huyen-kim-boi", "name": "Huyện Kim Bôi", "code": 399},
      "400": {"slug": "huyen-ky-son", "name": "Huyện Kỳ Sơn", "code": 400},
      "401": {"slug": "huyen-lac-son", "name": "Huyện Lạc Sơn", "code": 401},
      "402": {"slug": "huyen-lac-thuy", "name": "Huyện Lạc Thủy", "code": 402},
      "403": {"slug": "huyen-luong-son", "name": "Huyện Lương Sơn", "code": 403},
      "404": {"slug": "huyen-mai-chau", "name": "Huyện Mai Châu", "code": 404},
      "405": {"slug": "huyen-tan-lac", "name": "Huyện Tân Lạc", "code": 405},
      "406": {"slug": "huyen-yen-thuy", "name": "Huyện Yên Thủy", "code": 406},
      "407": {"slug": "thanh-pho-hoa-binh", "name": "Thành phố Hòa Bình", "code": 407}
    }
  },
  "31": {
    "slug": "tinh-hung-yen",
    "name": "Tỉnh Hưng Yên",
    "code": 31,
    "districts": {
      "408": {"slug": "huyen-an-thi", "name": "Huyện Ân Thi", "code": 408},
      "409": {"slug": "huyen-khoai-chau", "name": "Huyện Khoái Châu", "code": 409},
      "410": {"slug": "huyen-kim-dong", "name": "Huyện Kim Động", "code": 410},
      "411": {"slug": "huyen-my-hao", "name": "Huyện Mỹ Hào", "code": 411},
      "412": {"slug": "huyen-phu-cu", "name": "Huyện Phù Cừ", "code": 412},
      "413": {"slug": "huyen-tien-lu", "name": "Huyện Tiên Lữ", "code": 413},
      "414": {"slug": "huyen-van-giang", "name": "Huyện Văn Giang", "code": 414},
      "415": {"slug": "huyen-van-lam", "name": "Huyện Văn Lâm", "code": 415},
      "416": {"slug": "huyen-yen-my", "name": "Huyện Yên Mỹ", "code": 416},
      "417": {"slug": "thanh-pho-hung-yen", "name": "Thành phố Hưng Yên", "code": 417}
    }
  },
  "32": {
    "slug": "tinh-khanh-hoa",
    "name": "Tỉnh Khánh Hòa",
    "code": 32,
    "districts": {
      "418": {"slug": "huyen-cam-lam", "name": "Huyện Cam Lâm", "code": 418},
      "419": {"slug": "huyen-dien-khanh", "name": "Huyện Diên Khánh", "code": 419},
      "420": {"slug": "huyen-khanh-son", "name": "Huyện Khánh Sơn", "code": 420},
      "421": {"slug": "huyen-khanh-vinh", "name": "Huyện Khánh Vĩnh", "code": 421},
      "422": {"slug": "huyen-ninh-hoa", "name": "Huyện Ninh Hòa", "code": 422},
      "423": {"slug": "huyen-truong-sa", "name": "Huyện Trường Sa", "code": 423},
      "424": {"slug": "huyen-van-ninh", "name": "Huyện Vạn Ninh", "code": 424},
      "425": {"slug": "thanh-pho-nha-trang", "name": "Thành phố Nha Trang", "code": 425},
      "426": {"slug": "thi-xa-cam-ranh", "name": "Thị xã Cam Ranh", "code": 426}
    }
  },
  "33": {
    "slug": "tinh-kien-giang",
    "name": "Tỉnh Kiên Giang",
    "code": 33,
    "districts": {
      "427": {"slug": "huyen-an-bien", "name": "Huyện An Biên", "code": 427},
      "428": {"slug": "huyen-an-minh", "name": "Huyện An Minh", "code": 428},
      "429": {"slug": "huyen-chau-thanh", "name": "Huyện Châu Thành", "code": 429},
      "430": {"slug": "huyen-giang-thanh", "name": "Huyện Giang Thành", "code": 430},
      "431": {"slug": "huyen-giong-rieng", "name": "Huyện Giồng Riềng", "code": 431},
      "432": {"slug": "huyen-go-quao", "name": "Huyện Gò Quao", "code": 432},
      "433": {"slug": "huyen-hon-dat", "name": "Huyện Hòn Đất", "code": 433},
      "434": {"slug": "huyen-kien-hai", "name": "Huyện Kiên Hải", "code": 434},
      "435": {"slug": "huyen-kien-luong", "name": "Huyện Kiên Lương", "code": 435},
      "436": {"slug": "huyen-phu-quoc", "name": "Huyện Phú Quốc", "code": 436},
      "437": {"slug": "huyen-tan-hiep", "name": "Huyện Tân Hiệp", "code": 437},
      "438": {"slug": "huyen-u-minh-thuong", "name": "Huyện U Minh Thượng", "code": 438},
      "439": {"slug": "huyen-vinh-thuan", "name": "Huyện Vĩnh Thuận", "code": 439},
      "440": {"slug": "thanh-pho-rach-gia", "name": "Thành phố Rạch Giá", "code": 440},
      "441": {"slug": "thi-xa-ha-tien", "name": "Thị xã Hà Tiên", "code": 441}
    }
  },
  "34": {
    "slug": "tinh-kon-tum",
    "name": "Tỉnh Kon Tum",
    "code": 34,
    "districts": {
      "442": {"slug": "huyen-dak-glei", "name": "Huyện Đắk Glei", "code": 442},
      "443": {"slug": "huyen-dak-ha", "name": "Huyện Đắk Hà", "code": 443},
      "444": {"slug": "huyen-dak-to", "name": "Huyện Đắk Tô", "code": 444},
      "445": {"slug": "huyen-kon-plong", "name": "Huyện Kon Plông", "code": 445},
      "446": {"slug": "huyen-kon-ray", "name": "Huyện Kon Rẫy", "code": 446},
      "447": {"slug": "huyen-ngoc-hoi", "name": "Huyện Ngọc Hồi", "code": 447},
      "448": {"slug": "huyen-sa-thay", "name": "Huyện Sa Thầy", "code": 448},
      "449": {"slug": "huyen-tu-mo-rong", "name": "Huyện Tu Mơ Rông", "code": 449},
      "450": {"slug": "thanh-pho-kon-tum", "name": "Thành phố Kon Tum", "code": 450}
    }
  },
  "35": {
    "slug": "tinh-lai-chau",
    "name": "Tỉnh Lai Châu",
    "code": 35,
    "districts": {
      "451": {"slug": "huyen-muong-te", "name": "Huyện Mường Tè", "code": 451},
      "452": {"slug": "huyen-phong-tho", "name": "Huyện Phong Thổ", "code": 452},
      "453": {"slug": "huyen-sin-ho", "name": "Huyện Sìn Hồ", "code": 453},
      "454": {"slug": "huyen-tam-duong", "name": "Huyện Tam Đường", "code": 454},
      "455": {"slug": "huyen-tan-uyen", "name": "Huyện Tân Uyên", "code": 455},
      "456": {"slug": "huyen-than-uyen", "name": "Huyện Than Uyên", "code": 456},
      "457": {"slug": "thi-xa-lai-chau", "name": "Thị xã Lai Châu", "code": 457}
    }
  },
  "36": {
    "slug": "tinh-lam-dong",
    "name": "Tỉnh Lâm Đồng",
    "code": 36,
    "districts": {
      "458": {"slug": "huyen-bao-lam", "name": "Huyện Bảo Lâm", "code": 458},
      "459": {"slug": "huyen-cat-tien", "name": "Huyện Cát Tiên", "code": 459},
      "460": {"slug": "huyen-da-huoai", "name": "Huyện Đạ Huoai", "code": 460},
      "461": {"slug": "huyen-da-teh", "name": "Huyện Đạ Tẻh", "code": 461},
      "462": {"slug": "huyen-dam-rong", "name": "Huyện Đam Rông", "code": 462},
      "463": {"slug": "huyen-di-linh", "name": "Huyện Di Linh", "code": 463},
      "464": {"slug": "huyen-don-duong", "name": "Huyện Đơn Dương", "code": 464},
      "465": {"slug": "huyen-duc-trong", "name": "Huyện Đức Trọng", "code": 465},
      "466": {"slug": "huyen-lac-duong", "name": "Huyện Lạc Dương", "code": 466},
      "467": {"slug": "huyen-lam-ha", "name": "Huyện Lâm Hà", "code": 467},
      "468": {"slug": "thanh-pho-bao-loc", "name": "Thành phố Bảo Lộc", "code": 468},
      "469": {"slug": "thanh-pho-da-lat", "name": "Thành phố Đà Lạt", "code": 469}
    }
  },
  "37": {
    "slug": "tinh-lang-son",
    "name": "Tỉnh Lạng Sơn",
    "code": 37,
    "districts": {
      "470": {"slug": "huyen-bac-son", "name": "Huyện Bắc Sơn", "code": 470},
      "471": {"slug": "huyen-binh-gia", "name": "Huyện Bình Gia", "code": 471},
      "472": {"slug": "huyen-cao-loc", "name": "Huyện Cao Lộc", "code": 472},
      "473": {"slug": "huyen-chi-lang", "name": "Huyện Chi Lăng", "code": 473},
      "474": {"slug": "huyen-dinh-lap", "name": "Huyện Đình Lập", "code": 474},
      "475": {"slug": "huyen-huu-lung", "name": "Huyện Hữu Lũng", "code": 475},
      "476": {"slug": "huyen-loc-binh", "name": "Huyện Lộc Bình", "code": 476},
      "477": {"slug": "huyen-trang-dinh", "name": "Huyện Tràng Định", "code": 477},
      "478": {"slug": "huyen-van-lang", "name": "Huyện Văn Lãng", "code": 478},
      "479": {"slug": "huyen-van-quan", "name": "Huyện Văn Quan", "code": 479},
      "480": {"slug": "thanh-pho-lang-son", "name": "Thành phố Lạng Sơn", "code": 480}
    }
  },
  "38": {
    "slug": "tinh-lao-cai",
    "name": "Tỉnh Lào Cai",
    "code": 38,
    "districts": {
      "481": {"slug": "huyen-bac-ha", "name": "Huyện Bắc Hà", "code": 481},
      "482": {"slug": "huyen-bao-thang", "name": "Huyện Bảo Thắng", "code": 482},
      "483": {"slug": "huyen-bao-yen", "name": "Huyện Bảo Yên", "code": 483},
      "484": {"slug": "huyen-bat-xat", "name": "Huyện Bát Xát", "code": 484},
      "485": {"slug": "huyen-muong-khuong", "name": "Huyện Mường Khương", "code": 485},
      "486": {"slug": "huyen-sa-pa", "name": "Huyện Sa Pa", "code": 486},
      "487": {"slug": "huyen-si-ma-cai", "name": "Huyện Si Ma Cai", "code": 487},
      "488": {"slug": "huyen-van-ban", "name": "Huyện Văn Bàn", "code": 488},
      "489": {"slug": "thanh-pho-lao-cai", "name": "Thành phố Lào Cai", "code": 489}
    }
  },
  "39": {
    "slug": "tinh-long-an",
    "name": "Tỉnh Long An",
    "code": 39,
    "districts": {
      "490": {"slug": "huyen-ben-luc", "name": "Huyện Bến Lức", "code": 490},
      "491": {"slug": "huyen-can-duoc", "name": "Huyện Cần Đước", "code": 491},
      "492": {"slug": "huyen-can-giuoc", "name": "Huyện Cần Giuộc", "code": 492},
      "493": {"slug": "huyen-chau-thanh", "name": "Huyện Châu Thành", "code": 493},
      "494": {"slug": "huyen-duc-hoa", "name": "Huyện Đức Hòa", "code": 494},
      "495": {"slug": "huyen-duc-hue", "name": "Huyện Đức Huệ", "code": 495},
      "496": {"slug": "huyen-moc-hoa", "name": "Huyện Mộc Hóa", "code": 496},
      "497": {"slug": "huyen-tan-hung", "name": "Huyện Tân Hưng", "code": 497},
      "498": {"slug": "huyen-tan-thanh", "name": "Huyện Tân Thạnh", "code": 498},
      "499": {"slug": "huyen-tan-tru", "name": "Huyện Tân Trụ", "code": 499},
      "500": {"slug": "huyen-thanh-hoa", "name": "Huyện Thạnh Hóa", "code": 500},
      "501": {"slug": "huyen-thu-thua", "name": "Huyện Thủ Thừa", "code": 501},
      "502": {"slug": "huyen-vinh-hung", "name": "Huyện Vĩnh Hưng", "code": 502},
      "503": {"slug": "thanh-pho-tan-an", "name": "Thành phố Tân An", "code": 503}
    }
  },
  "40": {
    "slug": "tinh-nam-dinh",
    "name": "Tỉnh Nam Định",
    "code": 40,
    "districts": {
      "504": {"slug": "huyen-giao-thuy", "name": "Huyện Giao Thủy", "code": 504},
      "505": {"slug": "huyen-hai-hau", "name": "Huyện Hải Hậu", "code": 505},
      "506": {"slug": "huyen-my-loc", "name": "Huyện Mỹ Lộc", "code": 506},
      "507": {"slug": "huyen-nam-truc", "name": "Huyện Nam Trực", "code": 507},
      "508": {"slug": "huyen-nghia-hung", "name": "Huyện Nghĩa Hưng", "code": 508},
      "509": {"slug": "huyen-truc-ninh", "name": "Huyện Trực Ninh", "code": 509},
      "510": {"slug": "huyen-vu-ban", "name": "Huyện Vụ Bản", "code": 510},
      "511": {"slug": "huyen-xuan-truong", "name": "Huyện Xuân Trường", "code": 511},
      "512": {"slug": "huyen-y-yen", "name": "Huyện ý Yên", "code": 512},
      "513": {"slug": "thanh-pho-nam-dinh", "name": "Thành phố Nam Định", "code": 513}
    }
  },
  "41": {
    "slug": "tinh-nghe-an",
    "name": "Tỉnh Nghệ An",
    "code": 41,
    "districts": {
      "514": {"slug": "huyen-anh-son", "name": "Huyện Anh Sơn", "code": 514},
      "515": {"slug": "huyen-con-cuong", "name": "Huyện Con Cuông", "code": 515},
      "516": {"slug": "huyen-dien-chau", "name": "Huyện Diễn Châu", "code": 516},
      "517": {"slug": "huyen-do-luong", "name": "Huyện Đô Lương", "code": 517},
      "518": {"slug": "huyen-hung-nguyen", "name": "Huyện Hưng Nguyên", "code": 518},
      "519": {"slug": "huyen-ky-son", "name": "Huyện Kỳ Sơn", "code": 519},
      "520": {"slug": "huyen-nam-dan", "name": "Huyện Nam Đàn", "code": 520},
      "521": {"slug": "huyen-nghi-loc", "name": "Huyện Nghi Lộc", "code": 521},
      "522": {"slug": "huyen-nghia-dan", "name": "Huyện Nghĩa Đàn", "code": 522},
      "523": {"slug": "huyen-que-phong", "name": "Huyện Quế Phong", "code": 523},
      "524": {"slug": "huyen-quy-chau", "name": "Huyện Quỳ Châu", "code": 524},
      "525": {"slug": "huyen-quy-hop", "name": "Huyện Quỳ Hợp", "code": 525},
      "526": {"slug": "huyen-quynh-luu", "name": "Huyện Quỳnh Lưu", "code": 526},
      "527": {"slug": "huyen-tan-ky", "name": "Huyện Tân Kỳ", "code": 527},
      "528": {"slug": "huyen-thanh-chuong", "name": "Huyện Thanh Chương", "code": 528},
      "529": {"slug": "huyen-tuong-duong", "name": "Huyện Tương Dương", "code": 529},
      "530": {"slug": "huyen-yen-thanh", "name": "Huyện Yên Thành", "code": 530},
      "531": {"slug": "thanh-pho-vinh", "name": "Thành phố Vinh", "code": 531},
      "532": {"slug": "thi-xa-cua-lo", "name": "Thị xã Cửa Lò", "code": 532},
      "533": {"slug": "thi-xa-thai-hoa", "name": "Thị xã Thái Hòa", "code": 533}
    }
  },
  "42": {
    "slug": "tinh-ninh-binh",
    "name": "Tỉnh Ninh Bình",
    "code": 42,
    "districts": {
      "534": {"slug": "huyen-gia-vien", "name": "Huyện Gia Viễn", "code": 534},
      "535": {"slug": "huyen-hoa-lu", "name": "Huyện Hoa Lư", "code": 535},
      "536": {"slug": "huyen-kim-son", "name": "Huyện Kim Sơn", "code": 536},
      "537": {"slug": "huyen-nho-quan", "name": "Huyện Nho Quan", "code": 537},
      "538": {"slug": "huyen-yen-khanh", "name": "Huyện Yên Khánh", "code": 538},
      "539": {"slug": "huyen-yen-mo", "name": "Huyện Yên Mô", "code": 539},
      "540": {"slug": "thanh-pho-ninh-binh", "name": "Thành phố Ninh Bình", "code": 540},
      "541": {"slug": "thi-xa-tam-diep", "name": "Thị xã Tam Điệp", "code": 541}
    }
  },
  "43": {
    "slug": "tinh-ninh-thuan",
    "name": "Tỉnh Ninh Thuận",
    "code": 43,
    "districts": {
      "542": {"slug": "huyen-bac-ai", "name": "Huyên Bác ái", "code": 542},
      "543": {"slug": "huyen-ninh-hai", "name": "Huyện Ninh Hải", "code": 543},
      "544": {"slug": "huyen-ninh-phuoc", "name": "Huyện Ninh Phước", "code": 544},
      "545": {"slug": "huyen-ninh-son", "name": "Huyện Ninh Sơn", "code": 545},
      "546": {"slug": "huyen-thuan-bac", "name": "Huyện Thuận Bắc", "code": 546},
      "547": {"slug": "huyen-thuan-nam", "name": "Huyện Thuận Nam", "code": 547},
      "548": {
        "slug": "thanh-pho-phan-rang-thap-cham",
        "name": "Thành phố Phan Rang-Tháp Chàm",
        "code": 548
      }
    }
  },
  "44": {
    "slug": "tinh-phu-tho",
    "name": "Tỉnh Phú Thọ",
    "code": 44,
    "districts": {
      "549": {"slug": "huyen-cam-khe", "name": "Huyện Cẩm Khê", "code": 549},
      "550": {"slug": "huyen-doan-hung", "name": "Huyện Đoan Hùng", "code": 550},
      "551": {"slug": "huyen-ha-hoa", "name": "Huyện Hạ Hòa", "code": 551},
      "552": {"slug": "huyen-lam-thao", "name": "Huyện Lâm Thao", "code": 552},
      "553": {"slug": "huyen-phu-ninh", "name": "Huyện Phù Ninh", "code": 553},
      "554": {"slug": "huyen-tam-nong", "name": "Huyện Tam Nông", "code": 554},
      "555": {"slug": "huyen-tan-son", "name": "Huyện Tân Sơn", "code": 555},
      "556": {"slug": "huyen-thanh-ba", "name": "Huyện Thanh Ba", "code": 556},
      "557": {"slug": "huyen-thanh-son", "name": "Huyện Thanh Sơn", "code": 557},
      "558": {"slug": "huyen-thanh-thuy", "name": "Huyện Thanh Thủy", "code": 558},
      "559": {"slug": "huyen-yen-lap", "name": "Huyện Yên Lập", "code": 559},
      "560": {"slug": "thanh-pho-viet-tri", "name": "Thành phố Việt Trì", "code": 560},
      "561": {"slug": "thi-xa-phu-tho", "name": "Thị xã Phú Thọ", "code": 561}
    }
  },
  "45": {
    "slug": "tinh-phu-yen",
    "name": "Tỉnh Phú Yên",
    "code": 45,
    "districts": {
      "562": {"slug": "huyen-dong-hoa", "name": "Huyện Đông Hòa", "code": 562},
      "563": {"slug": "huyen-dong-xuan", "name": "Huyện Đồng Xuân", "code": 563},
      "564": {"slug": "huyen-phu-hoa", "name": "Huyện Phú Hòa", "code": 564},
      "565": {"slug": "huyen-son-hoa", "name": "Huyện Sơn Hòa", "code": 565},
      "566": {"slug": "huyen-song-hinh", "name": "Huyện Sông Hinh", "code": 566},
      "567": {"slug": "huyen-tay-hoa", "name": "Huyện Tây Hòa", "code": 567},
      "568": {"slug": "huyen-tuy-an", "name": "Huyện Tuy An", "code": 568},
      "569": {"slug": "thanh-pho-tuy-hoa", "name": "Thành phố Tuy Hòa", "code": 569},
      "570": {"slug": "thi-xa-song-cau", "name": "Thị xã Sông Cầu", "code": 570}
    }
  },
  "46": {
    "slug": "tinh-quang-binh",
    "name": "Tỉnh Quảng Bình",
    "code": 46,
    "districts": {
      "571": {"slug": "huyen-bo-trach", "name": "Huyện Bố Trạch", "code": 571},
      "572": {"slug": "huyen-le-thuy", "name": "Huyện Lệ Thủy", "code": 572},
      "573": {"slug": "huyen-minhhoa", "name": "Huyện MinhHoá", "code": 573},
      "574": {"slug": "huyen-quang-ninh", "name": "Huyện Quảng Ninh", "code": 574},
      "575": {"slug": "huyen-quang-trach", "name": "Huyện Quảng Trạch", "code": 575},
      "576": {"slug": "huyen-tuyen-hoa", "name": "Huyện Tuyên Hoá", "code": 576},
      "577": {"slug": "thanh-pho-dong-hoi", "name": "Thành phố Đồng Hới", "code": 577}
    }
  },
  "47": {
    "slug": "tinh-quang-nam",
    "name": "Tỉnh Quảng Nam",
    "code": 47,
    "districts": {
      "578": {"slug": "huyen-bac-tra-my", "name": "Huyện Bắc Trà My", "code": 578},
      "579": {"slug": "huyen-dai-loc", "name": "Huyện Đại Lộc", "code": 579},
      "580": {"slug": "huyen-dien-ban", "name": "Huyện Điện Bàn", "code": 580},
      "581": {"slug": "huyen-dong-giang", "name": "Huyện Đông Giang", "code": 581},
      "582": {"slug": "huyen-duy-xuyen", "name": "Huyện Duy Xuyên", "code": 582},
      "583": {"slug": "huyen-hiep-duc", "name": "Huyện Hiệp Đức", "code": 583},
      "584": {"slug": "huyen-nam-giang", "name": "Huyện Nam Giang", "code": 584},
      "585": {"slug": "huyen-nam-tra-my", "name": "Huyện Nam Trà My", "code": 585},
      "586": {"slug": "huyen-nong-son", "name": "Huyện Nông Sơn", "code": 586},
      "587": {"slug": "huyen-nui-thanh", "name": "Huyện Núi Thành", "code": 587},
      "588": {"slug": "huyen-phu-ninh", "name": "Huyện Phú Ninh", "code": 588},
      "589": {"slug": "huyen-phuoc-son", "name": "Huyện Phước Sơn", "code": 589},
      "590": {"slug": "huyen-que-son", "name": "Huyện Quế Sơn", "code": 590},
      "591": {"slug": "huyen-tay-giang", "name": "Huyện Tây Giang", "code": 591},
      "592": {"slug": "huyen-thang-binh", "name": "Huyện Thăng Bình", "code": 592},
      "593": {"slug": "huyen-tien-phuoc", "name": "Huyện Tiên Phước", "code": 593},
      "594": {"slug": "thanh-pho-hoi-an", "name": "Thành phố Hội An", "code": 594},
      "595": {"slug": "thanh-pho-tam-ky", "name": "Thành phố Tam Kỳ", "code": 595}
    }
  },
  "48": {
    "slug": "tinh-quang-ngai",
    "name": "Tỉnh Quảng Ngãi",
    "code": 48,
    "districts": {
      "596": {"slug": "huyen-ba-to", "name": "Huyện Ba Tơ", "code": 596},
      "597": {"slug": "huyen-binh-son", "name": "Huyện Bình Sơn", "code": 597},
      "598": {"slug": "huyen-duc-pho", "name": "Huyện Đức Phổ", "code": 598},
      "599": {"slug": "huyen-ly-son", "name": "Huyện Lý sơn", "code": 599},
      "600": {"slug": "huyen-minh-long", "name": "Huyện Minh Long", "code": 600},
      "601": {"slug": "huyen-mo-duc", "name": "Huyện Mộ Đức", "code": 601},
      "602": {"slug": "huyen-nghia-hanh", "name": "Huyện Nghĩa Hành", "code": 602},
      "603": {"slug": "huyen-son-ha", "name": "Huyện Sơn Hà", "code": 603},
      "604": {"slug": "huyen-son-tay", "name": "Huyện Sơn Tây", "code": 604},
      "605": {"slug": "huyen-son-tinh", "name": "Huyện Sơn Tịnh", "code": 605},
      "606": {"slug": "huyen-tay-tra", "name": "Huyện Tây Trà", "code": 606},
      "607": {"slug": "huyen-tra-bong", "name": "Huyện Trà Bồng", "code": 607},
      "608": {"slug": "huyen-tu-nghia", "name": "Huyện Tư Nghĩa", "code": 608},
      "609": {"slug": "thanh-pho-quang-ngai", "name": "Thành phố Quảng Ngãi", "code": 609}
    }
  },
  "49": {
    "slug": "tinh-quang-ninh",
    "name": "Tỉnh Quảng Ninh",
    "code": 49,
    "districts": {
      "610": {"slug": "huyen-ba-che", "name": "Huyện Ba Chẽ", "code": 610},
      "611": {"slug": "huyen-binh-lieu", "name": "Huyện Bình Liêu", "code": 611},
      "612": {"slug": "huyen-co-to", "name": "Huyện Cô Tô", "code": 612},
      "613": {"slug": "huyen-dam-ha", "name": "Huyện Đầm Hà", "code": 613},
      "614": {"slug": "huyen-dong-trieu", "name": "Huyện Đông Triều", "code": 614},
      "615": {"slug": "huyen-hai-ha", "name": "Huyện Hải Hà", "code": 615},
      "616": {"slug": "huyen-hoanh-bo", "name": "Huyện Hoành Bồ", "code": 616},
      "617": {"slug": "huyen-tien-yen", "name": "Huyện Tiên Yên", "code": 617},
      "618": {"slug": "huyen-van-don", "name": "Huyện Vân Đồn", "code": 618},
      "619": {"slug": "huyen-yen-hung", "name": "Huyện Yên Hưng", "code": 619},
      "620": {"slug": "thanh-pho-ha-long", "name": "Thành phố Hạ Long", "code": 620},
      "621": {"slug": "thanh-pho-mong-cai", "name": "Thành phố Móng Cái", "code": 621},
      "622": {"slug": "thi-xa-cam-pha", "name": "Thị xã Cẩm Phả", "code": 622},
      "623": {"slug": "thi-xa-uong-bi", "name": "Thị xã Uông Bí", "code": 623}
    }
  },
  "50": {
    "slug": "tinh-quang-tri",
    "name": "Tỉnh Quảng Trị",
    "code": 50,
    "districts": {
      "624": {"slug": "huyen-cam-lo", "name": "Huyện Cam Lộ", "code": 624},
      "625": {"slug": "huyen-con-co", "name": "Huyện Cồn Cỏ", "code": 625},
      "626": {"slug": "huyen-da-krong", "name": "Huyện Đa Krông", "code": 626},
      "627": {"slug": "huyen-gio-linh", "name": "Huyện Gio Linh", "code": 627},
      "628": {"slug": "huyen-hai-lang", "name": "Huyện Hải Lăng", "code": 628},
      "629": {"slug": "huyen-huong-hoa", "name": "Huyện Hướng Hóa", "code": 629},
      "630": {"slug": "huyen-trieu-phong", "name": "Huyện Triệu Phong", "code": 630},
      "631": {"slug": "huyen-vinh-linh", "name": "Huyện Vính Linh", "code": 631},
      "632": {"slug": "thanh-pho-dong-ha", "name": "Thành phố Đông Hà", "code": 632},
      "633": {"slug": "thi-xa-quang-tri", "name": "Thị xã Quảng Trị", "code": 633}
    }
  },
  "51": {
    "slug": "tinh-soc-trang",
    "name": "Tỉnh Sóc Trăng",
    "code": 51,
    "districts": {
      "634": {"slug": "huyen-chau-thanh", "name": "Huyện Châu Thành", "code": 634},
      "635": {"slug": "huyen-cu-lao-dung", "name": "Huyện Cù Lao Dung", "code": 635},
      "636": {"slug": "huyen-ke-sach", "name": "Huyện Kế Sách", "code": 636},
      "637": {"slug": "huyen-long-phu", "name": "Huyện Long Phú", "code": 637},
      "638": {"slug": "huyen-my-tu", "name": "Huyện Mỹ Tú", "code": 638},
      "639": {"slug": "huyen-my-xuyen", "name": "Huyện Mỹ Xuyên", "code": 639},
      "640": {"slug": "huyen-nga-nam", "name": "Huyện Ngã Năm", "code": 640},
      "641": {"slug": "huyen-thanh-tri", "name": "Huyện Thạnh Trị", "code": 641},
      "642": {"slug": "huyen-tran-de", "name": "Huyện Trần Đề", "code": 642},
      "643": {"slug": "huyen-vinh-chau", "name": "Huyện Vĩnh Châu", "code": 643},
      "644": {"slug": "thanh-pho-soc-trang", "name": "Thành phố Sóc Trăng", "code": 644}
    }
  },
  "52": {
    "slug": "tinh-son-la",
    "name": "Tỉnh Sơn La",
    "code": 52,
    "districts": {
      "645": {"slug": "huyen-bac-yen", "name": "Huyện Bắc Yên", "code": 645},
      "646": {"slug": "huyen-mai-son", "name": "Huyện Mai Sơn", "code": 646},
      "647": {"slug": "huyen-moc-chau", "name": "Huyện Mộc Châu", "code": 647},
      "648": {"slug": "huyen-muong-la", "name": "Huyện Mường La", "code": 648},
      "649": {"slug": "huyen-phu-yen", "name": "Huyện Phù Yên", "code": 649},
      "650": {"slug": "huyen-quynh-nhai", "name": "Huyện Quỳnh Nhai", "code": 650},
      "651": {"slug": "huyen-song-ma", "name": "Huyện Sông Mã", "code": 651},
      "652": {"slug": "huyen-sop-cop", "name": "Huyện Sốp Cộp", "code": 652},
      "653": {"slug": "huyen-thuan-chau", "name": "Huyện Thuận Châu", "code": 653},
      "654": {"slug": "huyen-yen-chau", "name": "Huyện Yên Châu", "code": 654},
      "655": {"slug": "thanh-pho-son-la", "name": "Thành phố Sơn La", "code": 655}
    }
  },
  "53": {
    "slug": "tinh-tay-ninh",
    "name": "Tỉnh Tây Ninh",
    "code": 53,
    "districts": {
      "656": {"slug": "huyen-ben-cau", "name": "Huyện Bến Cầu", "code": 656},
      "657": {"slug": "huyen-chau-thanh", "name": "Huyện Châu Thành", "code": 657},
      "658": {"slug": "huyen-duong-minh-chau", "name": "Huyện Dương Minh Châu", "code": 658},
      "659": {"slug": "huyen-go-dau", "name": "Huyện Gò Dầu", "code": 659},
      "660": {"slug": "huyen-hoa-thanh", "name": "Huyện Hòa Thành", "code": 660},
      "661": {"slug": "huyen-tan-bien", "name": "Huyện Tân Biên", "code": 661},
      "662": {"slug": "huyen-tan-chau", "name": "Huyện Tân Châu", "code": 662},
      "663": {"slug": "huyen-trang-bang", "name": "Huyện Trảng Bàng", "code": 663},
      "664": {"slug": "thi-xa-tay-ninh", "name": "Thị xã Tây Ninh", "code": 664}
    }
  },
  "54": {
    "slug": "tinh-thai-binh",
    "name": "Tỉnh Thái Bình",
    "code": 54,
    "districts": {
      "665": {"slug": "huyen-dong-hung", "name": "Huyện Đông Hưng", "code": 665},
      "666": {"slug": "huyen-hung-ha", "name": "Huyện Hưng Hà", "code": 666},
      "667": {"slug": "huyen-kien-xuong", "name": "Huyện Kiến Xương", "code": 667},
      "668": {"slug": "huyen-quynh-phu", "name": "Huyện Quỳnh Phụ", "code": 668},
      "669": {"slug": "huyen-thai-thuy", "name": "Huyện Thái Thụy", "code": 669},
      "670": {"slug": "huyen-tien-hai", "name": "Huyện Tiền Hải", "code": 670},
      "671": {"slug": "huyen-vu-thu", "name": "Huyện Vũ Thư", "code": 671},
      "672": {"slug": "thanh-pho-thai-binh", "name": "Thành phố Thái Bình", "code": 672}
    }
  },
  "55": {
    "slug": "tinh-thai-nguyen",
    "name": "Tỉnh Thái Nguyên",
    "code": 55,
    "districts": {
      "673": {"slug": "huyen-dai-tu", "name": "Huyện Đại Từ", "code": 673},
      "674": {"slug": "huyen-dinh-hoa", "name": "Huyện Định Hóa", "code": 674},
      "675": {"slug": "huyen-dong-hy", "name": "Huyện Đồng Hỷ", "code": 675},
      "676": {"slug": "huyen-pho-yen", "name": "Huyện Phổ Yên", "code": 676},
      "677": {"slug": "huyen-phu-binh", "name": "Huyện Phú Bình", "code": 677},
      "678": {"slug": "huyen-phu-luong", "name": "Huyện Phú Lương", "code": 678},
      "679": {"slug": "huyen-vo-nhai", "name": "Huyện Võ Nhai", "code": 679},
      "680": {"slug": "thanh-pho-thai-nguyen", "name": "Thành phố Thái Nguyên", "code": 680},
      "681": {"slug": "thi-xa-song-cong", "name": "Thị xã Sông Công", "code": 681}
    }
  },
  "56": {
    "slug": "tinh-thanh-hoa",
    "name": "Tỉnh Thanh Hóa",
    "code": 56,
    "districts": {
      "682": {"slug": "huyen-ba-thuoc", "name": "Huyện Bá Thước", "code": 682},
      "683": {"slug": "huyen-cam-thuy", "name": "Huyện Cẩm Thủy", "code": 683},
      "684": {"slug": "huyen-dong-son", "name": "Huyện Đông Sơn", "code": 684},
      "685": {"slug": "huyen-ha-trung", "name": "Huyện Hà Trung", "code": 685},
      "686": {"slug": "huyen-hau-loc", "name": "Huyện Hậu Lộc", "code": 686},
      "687": {"slug": "huyen-hoang-hoa", "name": "Huyện Hoằng Hóa", "code": 687},
      "688": {"slug": "huyen-lang-chanh", "name": "Huyện Lang Chánh", "code": 688},
      "689": {"slug": "huyen-muong-lat", "name": "Huyện Mường Lát", "code": 689},
      "690": {"slug": "huyen-nga-son", "name": "Huyện Nga Sơn", "code": 690},
      "691": {"slug": "huyen-ngoc-lac", "name": "Huyện Ngọc Lặc", "code": 691},
      "692": {"slug": "huyen-nhu-thanh", "name": "Huyện Như Thanh", "code": 692},
      "693": {"slug": "huyen-nhu-xuan", "name": "Huyện Như Xuân", "code": 693},
      "694": {"slug": "huyen-nong-cong", "name": "Huyện Nông Cống", "code": 694},
      "695": {"slug": "huyen-quan-hoa", "name": "Huyện Quan Hóa", "code": 695},
      "696": {"slug": "huyen-quan-son", "name": "Huyện Quan Sơn", "code": 696},
      "697": {"slug": "huyen-quang-xuong", "name": "Huyện Quảng Xương", "code": 697},
      "698": {"slug": "huyen-thach-thanh", "name": "Huyện Thạch Thành", "code": 698},
      "699": {"slug": "huyen-thieu-hoa", "name": "Huyện Thiệu Hóa", "code": 699},
      "700": {"slug": "huyen-tho-xuan", "name": "Huyện Thọ Xuân", "code": 700},
      "701": {"slug": "huyen-thuong-xuan", "name": "Huyện Thường Xuân", "code": 701},
      "702": {"slug": "huyen-tinh-gia", "name": "Huyện Tĩnh Gia", "code": 702},
      "703": {"slug": "huyen-trieu-son", "name": "Huyện Triệu Sơn", "code": 703},
      "704": {"slug": "huyen-vinh-loc", "name": "Huyện Vĩnh Lộc", "code": 704},
      "705": {"slug": "huyen-yen-dinh", "name": "Huyện Yên Định", "code": 705},
      "706": {"slug": "thanh-pho-thanh-hoa", "name": "Thành phố Thanh Hóa", "code": 706},
      "707": {"slug": "thi-xa-bim-son", "name": "Thị xã Bỉm Sơn", "code": 707},
      "708": {"slug": "thi-xa-sam-son", "name": "Thị xã Sầm Sơn", "code": 708}
    }
  },
  "57": {
    "slug": "tinh-thua-thien-hue",
    "name": "Tỉnh Thừa Thiên Huế",
    "code": 57,
    "districts": {
      "709": {"slug": "huyen-a-luoi", "name": "Huyện A Lưới", "code": 709},
      "710": {"slug": "huyen-huong-tra", "name": "Huyện Hương Trà", "code": 710},
      "711": {"slug": "huyen-nam-dong", "name": "Huyện Nam Dông", "code": 711},
      "712": {"slug": "huyen-phong-dien", "name": "Huyện Phong Điền", "code": 712},
      "713": {"slug": "huyen-phu-loc", "name": "Huyện Phú Lộc", "code": 713},
      "714": {"slug": "huyen-phu-vang", "name": "Huyện Phú Vang", "code": 714},
      "715": {"slug": "huyen-quang-dien", "name": "Huyện Quảng Điền", "code": 715},
      "716": {"slug": "thanh-pho-hue", "name": "Thành phố Huế", "code": 716},
      "717": {"slug": "thi-xa-huong-thuy", "name": "thị xã Hương Thủy", "code": 717}
    }
  },
  "58": {
    "slug": "tinh-tien-giang",
    "name": "Tỉnh Tiền Giang",
    "code": 58,
    "districts": {
      "718": {"slug": "huyen-cai-be", "name": "Huyện Cái Bè", "code": 718},
      "719": {"slug": "huyen-cai-lay", "name": "Huyện Cai Lậy", "code": 719},
      "720": {"slug": "huyen-chau-thanh", "name": "Huyện Châu Thành", "code": 720},
      "721": {"slug": "huyen-cho-gao", "name": "Huyện Chợ Gạo", "code": 721},
      "722": {"slug": "huyen-go-cong-dong", "name": "Huyện Gò Công Đông", "code": 722},
      "723": {"slug": "huyen-go-cong-tay", "name": "Huyện Gò Công Tây", "code": 723},
      "724": {"slug": "huyen-tan-phu-dong", "name": "Huyện Tân Phú Đông", "code": 724},
      "725": {"slug": "huyen-tan-phuoc", "name": "Huyện Tân Phước", "code": 725},
      "726": {"slug": "thanh-pho-my-tho", "name": "Thành phố Mỹ Tho", "code": 726},
      "727": {"slug": "thi-xa-go-cong", "name": "Thị xã Gò Công", "code": 727}
    }
  },
  "59": {
    "slug": "tinh-tra-vinh",
    "name": "Tỉnh Trà Vinh",
    "code": 59,
    "districts": {
      "728": {"slug": "huyen-cang-long", "name": "Huyện Càng Long", "code": 728},
      "729": {"slug": "huyen-cau-ke", "name": "Huyện Cầu Kè", "code": 729},
      "730": {"slug": "huyen-cau-ngang", "name": "Huyện Cầu Ngang", "code": 730},
      "731": {"slug": "huyen-chau-thanh", "name": "Huyện Châu Thành", "code": 731},
      "732": {"slug": "huyen-duyen-hai", "name": "Huyện Duyên Hải", "code": 732},
      "733": {"slug": "huyen-tieu-can", "name": "Huyện Tiểu Cần", "code": 733},
      "734": {"slug": "huyen-tra-cu", "name": "Huyện Trà Cú", "code": 734},
      "735": {"slug": "thanh-pho-tra-vinh", "name": "Thành phố Trà Vinh", "code": 735}
    }
  },
  "60": {
    "slug": "tinh-tuyen-quang",
    "name": "Tỉnh Tuyên Quang",
    "code": 60,
    "districts": {
      "736": {"slug": "huyen-chiem-hoa", "name": "Huyện Chiêm Hóa", "code": 736},
      "737": {"slug": "huyen-ham-yen", "name": "Huyện Hàm Yên", "code": 737},
      "738": {"slug": "huyen-na-hang", "name": "Huyện Na hang", "code": 738},
      "739": {"slug": "huyen-son-duong", "name": "Huyện Sơn Dương", "code": 739},
      "740": {"slug": "huyen-yen-son", "name": "Huyện Yên Sơn", "code": 740},
      "741": {"slug": "thanh-pho-tuyen-quang", "name": "Thành phố Tuyên Quang", "code": 741}
    }
  },
  "61": {
    "slug": "tinh-vinh-long",
    "name": "Tỉnh Vĩnh Long",
    "code": 61,
    "districts": {
      "742": {"slug": "huyen-binh-minh", "name": "Huyện Bình Minh", "code": 742},
      "743": {"slug": "huyen-binh-tan", "name": "Huyện Bình Tân", "code": 743},
      "744": {"slug": "huyen-long-ho", "name": "Huyện Long Hồ", "code": 744},
      "745": {"slug": "huyen-mang-thit", "name": "Huyện Mang Thít", "code": 745},
      "746": {"slug": "huyen-tam-binh", "name": "Huyện Tam Bình", "code": 746},
      "747": {"slug": "huyen-tra-on", "name": "Huyện Trà Ôn", "code": 747},
      "748": {"slug": "huyen-vung-liem", "name": "Huyện Vũng Liêm", "code": 748},
      "749": {"slug": "thanh-pho-vinh-long", "name": "Thành phố Vĩnh Long", "code": 749}
    }
  },
  "62": {
    "slug": "tinh-vinh-phuc",
    "name": "Tỉnh Vĩnh Phúc",
    "code": 62,
    "districts": {
      "750": {"slug": "huyen-binh-xuyen", "name": "Huyện Bình Xuyên", "code": 750},
      "751": {"slug": "huyen-lap-thach", "name": "Huyện Lập Thạch", "code": 751},
      "752": {"slug": "huyen-song-lo", "name": "Huyện Sông Lô", "code": 752},
      "753": {"slug": "huyen-tam-dao", "name": "Huyện Tam Đảo", "code": 753},
      "754": {"slug": "huyen-tam-duong", "name": "Huyện Tam Dương", "code": 754},
      "755": {"slug": "huyen-vinh-tuong", "name": "Huyện Vĩnh Tường", "code": 755},
      "756": {"slug": "huyen-yen-lac", "name": "Huyện Yên Lạc", "code": 756},
      "757": {"slug": "thanh-pho-vinh-yen", "name": "Thành phố Vĩnh Yên", "code": 757},
      "758": {"slug": "thi-xa-phuc-yen", "name": "Thị xã Phúc Yên", "code": 758}
    }
  },
  "63": {
    "slug": "tinh-yen-bai",
    "name": "Tỉnh Yên Bái",
    "code": 63,
    "districts": {
      "759": {"slug": "huyen-luc-yen", "name": "Huyện Lục Yên", "code": 759},
      "760": {"slug": "huyen-mu-cang-chai", "name": "Huyện Mù Cang Chải", "code": 760},
      "761": {"slug": "huyen-tram-tau", "name": "Huyện Trạm Tấu", "code": 761},
      "762": {"slug": "huyen-tran-yen", "name": "Huyện Trấn Yên", "code": 762},
      "763": {"slug": "huyen-van-chan", "name": "Huyện Văn Chấn", "code": 763},
      "764": {"slug": "huyen-van-yen", "name": "Huyện Văn Yên", "code": 764},
      "765": {"slug": "huyen-yen-binh", "name": "Huyện Yên Bình", "code": 765},
      "766": {"slug": "thanh-pho-yen-bai", "name": "Thành phố Yên Bái", "code": 766},
      "767": {"slug": "thi-xa-nghia-lo", "name": "Thị xã Nghĩa Lộ", "code": 767}
    }
  }
};

class Province {
  String? getProvinceByCode(int code) {
    if (code.toString().length == 1) {
      return provinces[code.toString()] == null
          ? provinces['1']!['name'].toString()
          : provinces[code.toString()]!['name'].toString();
    }
    return provinces[code.toString()] == null
        ? provinces['1']!['name'].toString()
        : provinces[code.toString()]!['name'].toString();
  }

  int getCodeOfProvince(String name) {
    List fullProvinceList = provinces.values.map((item) => item).toList();
    return fullProvinceList.where((province) => province['name'] == name).toList()[0]['code'];
  }

  static const _prioritizedProvinces = [
    "Thành phố Cần Thơ",
    "Thành phố Đà Nẵng",
    "Thành phố Hải Phòng",
    "Thành phố Hà Nội",
    "Thành phố Hồ Chí Minh",
  ];

  List renderProvinceArray() {
    List initialProvinceList;
    initialProvinceList = provinces.entries.map((entry) => entry.value['name']).toList();
    initialProvinceList.sort();
    _prioritizedProvinces.forEach((element) {
      initialProvinceList.remove(element);
      initialProvinceList.insert(0, element);
    });

    return initialProvinceList;
  }
}
