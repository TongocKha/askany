class SocketEvents {
  // CCU: Active - Inactive: inactive is app running on foreground for iOS
  static const String SET_ACTIVE_CSS = "SET_ACTIVE_CSS";
  static const String SET_ACTIVE_SSC = "SET_ACTIVE_SSC";

  static const String SET_INACTIVE_CSS = "SET_INACTIVE_CSS";
  static const String SET_INACTIVE_SSC = "SET_INACTIVE_SSC";

  // Room
  static const String JOIN_ROOM_CSS = "JOIN_ROOM_CSS";
  static const String JOIN_ROOM_SSC = "JOIN_ROOM_SSC";
  static const String LEAVE_ROOM_CSS = "LEAVE_ROOM_CSS";
  static const String LEAVE_ROOM_SSC = "LEAVE_ROOM_SSC";

  // Message
  static const String SEND_MESSAGE_CSS = 'SEND_MESSAGE_CSS';
  static const String SEND_MESSAGE_SSC = 'SEND_MESSAGE_SSC';
  static const String MESSAGE_UPDATED_CSS = 'MESSAGE_UPDATED_CSS';
  static const String MESSAGE_UPDATED_SSC = 'MESSAGE_UPDATED_SSC';
  static const String MESSAGE_REMOVED_CSS = 'MESSAGE_REMOVED_CSS';
  static const String MESSAGE_REMOVED_SSC = 'MESSAGE_REMOVED_SSC';

  // Calling
  static const String CALLING_CSS = "CALLING_CSS";
  static const String CALLING_SSC = "CALLING_SSC";

  static const String IGNORE_CALL_SSC = "IGNORE_CALL_SSC";

  // End call
  static const String END_CALL_CSS = "END_CALL_CSS";
  static const String END_CALL_SSC = "END_CALL_SSC";

  // Answer
  static const String ANSWER_CSS = "ANSWER_CSS";
  static const String ANSWER_SSC = "ANSWER_SSC";

  // Candidate
  static const String SEND_CANDIDATE_CSS = "SEND_CANDIDATE_CSS";
  static const String SEND_CANDIDATE_SSC = "SEND_CANDIDATE_SSC";

  // Raise hand
  static const String RAISE_HAND_CSS = "SEND_RASE_HAND_CSS";
  static const String RAISE_HAND_SSC = "SEND_RASE_HAND_SSC";

  // Share screen
  static const String SHARE_SCREEN_CSS = "SHARE_SCREEN_CSS";
  static const String SHARE_SCREEN_SSC = "SHARE_SCREEN_SSC";

  static const String ANSWER_SHARE_SCREEN_CSS = "ANSWER_SHARE_SCREEN_CSS";
  static const String ANSWER_SHARE_SCREEN_SSC = "ANSWER_SHARE_SCREEN_SSC";

  static const String SEND_CANDIDATE_SHARE_SCREEN_CSS = "SEND_CANDIDATE_SHARE_SCREEN_CSS";
  static const String SEND_CANDIDATE_SHARE_SCREEN_SSC = "SEND_CANDIDATE_SHARE_SCREEN_SSC";

  static const String STOP_SHARE_SCREEN_CSS = "STOP_SHARE_SCREEN_CSS";
  static const String STOP_SHARE_SCREEN_SSC = "STOP_SHARE_SCREEN_SSC";

  // Toggle mic
  static const String TOGGLE_MIC_CSS = "TOGGLE_MIC_CSS";
  static const String TOGGLE_MIC_SSC = "TOGGLE_MIC_SSC";

  // Toggle camera
  static const String TOGGLE_CAMERA_CSS = "TOGGLE_CAMERA_CSS";
  static const String TOGGLE_CAMERA_SSC = "TOGGLE_CAMERA_SSC";

  // Send device
  static const String SEND_DEVICE_CSS = "SEND_INFO_DEVICE_CSS";

  // Logout
  static const String LOGOUT_CSS = "LOGOUT_CSS";
  static const String LOGOUT_SSC = "LOGOUT_SSC";

  // Notifications
  static const String SEND_NOTI_SSC = 'SEND_NOTIFICATION_SSC';
  static const String SEND_NOTI_CSS = 'SEND_NOTIFICATION_CSS';

  // Typing
  static const String TYPING_CSS = 'TYPING_CSS';
  static const String TYPING_SSC = 'TYPING_SSC';

  // Stop Typing
  static const String STOP_TYPING_CSS = 'STOP_TYPING_CSS';
  static const String STOP_TYPING_SSC = 'STOP_TYPING_SSC';
}
