import 'package:askany/src/configs/application.dart';
import 'package:askany/src/configs/lang/localization.dart';
import 'package:askany/src/models/conversation_model.dart';

const String AUTHOR_PROJECT = 'lambiengcode';
const String APPLICATION_ANDROID_ICON = 'ic_notification';
const String NOTIFICATION_CHANNEL_ID = 'askany_notification_normal';

const String UNKNOWN = 'unknown';

const String STRIPE_PUBLISH_KEY_ID =
    "pk_test_51I5uPoBxowVJskY2Efe6hjnouC7C87pdTlRu4ROIwYyXKHFryyviRMQvhnPVuK5TRDuia9Isw1utNHwOoKzjERm500vO0j6xel";

const String STRIPE_SECRET_ID =
    "sk_test_51I5uPoBxowVJskY20pniLILSj1bV1kmsoiZBfzwZW9XYLmlcMNeg5FP6NQoYuUNtwWnfgJufxvvFIoVU8CiAyoGb00BCrfdp1P";

const String APPLE_MERCHANT_ID = 'merchant.com.askany.ndn';

const Map<String, dynamic> configuration = {
  'iceServers': [
    {'urls': 'stun:stun.ducanhzed.com:3478'},
    {
      'urls': 'turn:turn.ducanhzed.com:3478',
      'username': 'root',
      'credential': 'abc@123',
    },
  ],
};

const Map<String, dynamic> configurationShare = {
  'iceServers': [
    {'urls': 'stun:stun.ducanhzed.com:3478'},
    {
      'urls': 'turn:turn.ducanhzed.com:3478',
      'username': 'root',
      'credential': 'abc@123',
    },
  ],
  'sdpSemantics': "unified-plan",
};

List<String> calendarTitle = [
  'T2',
  'T3',
  'T4',
  'T5',
  'T6',
  'T7',
  'CN',
];

List<String> thisWeek = [
  '30',
  '31',
  '1',
  '2',
  '3',
  '4',
  '5',
];

List<String> statusTimeline = [
  'Đang diễn ra',
  'Hoàn thành',
  'Sắp diễn ra',
];

// Enum
enum PaymentMethod { byVNPay, byAppWallet }

// 'url'
const HTTP_URL = 'http://';
const HTTPS_URL = 'https://';
const URL_VNPAY = 'https://askanydev.tk';
const VNPAY_SUCCESS = 'thanh-toan/thanh-cong';

// Methods
const POST = 'POST';
const GET = 'GET';
const PUT = 'PUT';
const DELETE = 'DELETE';
const PATCH = 'PATCH';

const EMPTY_STRING = 'EMPTY';

// Numbers
const THUMBNAIL_IMAGE_SIZE = 350;
const SINGLE_CHARACTER = 1;
const CONNECT_TIME_OUT = 5000;
const RECEIVE_TIME_OUT = 5000;
const LIMIT_RESPONSE_TIME = 200;
const DELAY_50_MS = 50;
const DELAY_100_MS = 100;
const DELAY_200_MS = 200;
const DELAY_250_MS = 250;
const DELAY_HALF_SECOND = 500;
const DURATION_DEFAULT_ANIMATION = 300;
const SPACE_TIME_MESSAGE = 10;
const DELAY_A_MINUTE = 1;
const DELAY_TWO_SECONDS = 2;
const LIMIT_API_5 = 5;
const LIMIT_API_10 = 10;
const LIMIT_API_15 = 15;
const UNLIMITED_API = 1000;
const UNLIMITED_QUANTITY = 99999;
const INCH_TO_DP = 160;
const String MIN_MONEY = '0';
const String MAX_MONEY = '1000000000';

// Quantity Constants
const ITEM_COUNT_SHIMMER = 5;
const ITEM_GRID_VIEW = 3;
const PERCENT_BATTERY_WARNING = 10;
const PARTICIPANTS_COUNT = 1;

// Status Request
const REQUEST_FINISHED = -1;
const REQUEST_HAPPENING = 1;

// Status Offer
const OFFER_CONFIRMED = 4;
const OFFER_PAID = 3;
const OFFER_CHOSEN = 2;
const OFFER_ACTIVE = 1;
const OFFER_BLOCKED = 0;
const OFFER_DELETED = -1;
const OFFER_CANCELED = -2;
const OFFER_REPORTED = -3;
const OFFER_COMPLETED = 5;

// Service Managament Stataus (tabsNumbers)
const SERVICE_CONFIRMING = 1;
const SERVICE_CONFIRMED = 2;
const SERVICE_COMPLETED = 3;
const SERVICE_CANCELED = 4;
const SERVICE_COMPLAINED = 5;
const SERVICE_ADVISED = 6;

// Status ContactType
const CONTACT_CALL_TYPE = 1;
const CONTACT_MEET_TYPE = 2;

// Status isSeen message
const IS_SEEN = 1;
const NOT_SEEN = 0;

// String Constants
const MODE_DEV = 'DEV';
const MODE_PRODUCTION = 'PRODUCTION';
const SDP_SHARE_IPHONE = 'IPHONE';
const DATE_TIME_DEFAULT = '2022-01-10T08:36:56.514Z';
const CONTACT_MEET = 'meet';
const CONTACT_CALL = 'call';
const ROLE_EXPERT = 'expert';
const ROLE_USER = 'user';

const MESSAGE_FIELD = 'messages';
const IS_OVER_FIELD = 'isOver';

// Font Family
const NUNITO_SANS = 'Nunito';

const REQUEST_HAPPENING_STRING = 'happening';
const REQUEST_FINISHED_STRING = 'over';

const CURRENCY_VND = 'vnd';
const CURRENCY_USD = 'usd';

// 'url' to website
const URL_REGISTER_USER = '${Application.websiteUrl}dang-ky';

// Error Message String
const WRONG_PASSWORD = 'wrong_password';
const UNCONFIRMED_ACCOUNT = 'unconfirmed_account';
const PHONE_EXISTS = 'phone_exists';
const SOCIAL_EXISTS = 'social_exists';
const EMAIL_EXISTS = 'email_exists';

//Error Bloc Expert String
const BLOC_EXPERT_EXISTS = 'already_blocked';

// Method Channels
const INITIAL_APP_CHANNEL = 'com.askany.startArguments';
const SHARE_SCREEN_CHANNEL = 'com.askany.shareScreen';

// Options message
// ignore: non_constant_identifier_names
final MY_MESSAGE_OPTIONS = [
  Strings.MY_MESSAGE_OPTIONS[0].i18n,
  Strings.MY_MESSAGE_OPTIONS[1].i18n,
  Strings.MY_MESSAGE_OPTIONS[2].i18n,
  Strings.MY_MESSAGE_OPTIONS[3].i18n,
  Strings.MY_MESSAGE_OPTIONS[4].i18n,
];

// ignore: non_constant_identifier_names
final FRIEND_MESSAGE_OPTIONS = [
  Strings.FRIEND_MESSAGE_OPTIONS[0].i18n,
  Strings.FRIEND_MESSAGE_OPTIONS[1].i18n,
  Strings.FRIEND_MESSAGE_OPTIONS[2].i18n,
  Strings.FRIEND_MESSAGE_OPTIONS[3].i18n,
];

// ignore: non_constant_identifier_names
final CHAT_OPTIONS = [
  Strings.CHAT_OPTIONS[0].i18n,
  Strings.CHAT_OPTIONS[1].i18n,
  Strings.CHAT_OPTIONS[2].i18n,
  Strings.CHAT_OPTIONS[3].i18n,
  Strings.CHAT_OPTIONS[4].i18n,
];

// Message
LatestMessage get defaultLatestMessage => LatestMessage(
      id: '',
      expertSender: '',
      data: 'Chưa có tin nhắn nào!',
      createdAt: DateTime.parse(DATE_TIME_DEFAULT).toLocal(),
      isSeenStatus: IS_SEEN,
    );

// Video Call Status
const int MISSING = -1;
const int IN_COMMING = 0;
const int CALLING = 1;
const int END_CALL = 2;

// Notification message type
const int OFFERED_REQUEST = 1;
const int PICKED_EXPERT = 2;
const int CONFIRM_ADVISE = 3;
const int CANCEL_SERVICE = 4;
const int CANCEL_OFFER = 5;
const int USER_PAID_EXPERT = 6;
const int USER_COMPLAIN_REQUEST = 7;
const int USER_RATE_REQUEST = 8;

// Notification type
const int USER_SEND_EXPERT = 0;
const int EXPERT_SEND_USER = 1;
const int ADMIN_SEND_USER = 2;
const int ADMIN_SEND_EXPERT = 3;

// Transaction status
const int PAYMENT_FAILED = -1;
const int WAITING_FOR_PROGRESSING = 0;
const int PAYMENT_SUCCESSFUL = 1;
const int USER_REPORT = 2;

// Transaction type
const int USER_PAYMENT_WALLET_VNPAY = 0;
const int USER_PAYMENT_WALLET_ASKANY = 1;
const int ADMIN_SEND_MONEY_EXPERT = 2;
const int ADMIN_SEND_MONEY_USER = 3;
const int USER_RECHARGE_VNPAY = 4;
const int EXPERT_RECHAGE_VNPAY = 5;
const int USER_WITHDRAW_MONEY_BANK = 6;
const int EXPERT_WITHDRAW_MONEY_VNPAY = 7;

// Price Service
const int IS_REPEAT = 1;

//Deep-link
const String DetailsExpert = 'chi-tiet-chuyen-gia';
const String DetailsService = 'chi-tiet-dich-vu';
const String DetailsSkill = 'chi-tiet-hop-dong';
const String Category = 'danh-muc';
const String RequestService = 'yeu-cau-dich-vu';
